﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// Vuforia.QCARRuntimeUtilities/WebCamUsed
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitie_0.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo WebCamUsed_t807_il2cpp_TypeInfo;
// Vuforia.QCARRuntimeUtilities/WebCamUsed
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitie_0MethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// Metadata Definition Vuforia.QCARRuntimeUtilities/WebCamUsed
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo WebCamUsed_t807____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &WebCamUsed_t807_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamUsed_t807, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType WebCamUsed_t807_0_0_32854;
FieldInfo WebCamUsed_t807____UNKNOWN_2_FieldInfo = 
{
	"UNKNOWN"/* name */
	, &WebCamUsed_t807_0_0_32854/* type */
	, &WebCamUsed_t807_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType WebCamUsed_t807_0_0_32854;
FieldInfo WebCamUsed_t807____TRUE_3_FieldInfo = 
{
	"TRUE"/* name */
	, &WebCamUsed_t807_0_0_32854/* type */
	, &WebCamUsed_t807_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType WebCamUsed_t807_0_0_32854;
FieldInfo WebCamUsed_t807____FALSE_4_FieldInfo = 
{
	"FALSE"/* name */
	, &WebCamUsed_t807_0_0_32854/* type */
	, &WebCamUsed_t807_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* WebCamUsed_t807_FieldInfos[] =
{
	&WebCamUsed_t807____value___1_FieldInfo,
	&WebCamUsed_t807____UNKNOWN_2_FieldInfo,
	&WebCamUsed_t807____TRUE_3_FieldInfo,
	&WebCamUsed_t807____FALSE_4_FieldInfo,
	NULL
};
static const int32_t WebCamUsed_t807____UNKNOWN_2_DefaultValueData = 0;
extern Il2CppType Int32_t123_0_0_0;
static Il2CppFieldDefaultValueEntry WebCamUsed_t807____UNKNOWN_2_DefaultValue = 
{
	&WebCamUsed_t807____UNKNOWN_2_FieldInfo/* field */
	, { (char*)&WebCamUsed_t807____UNKNOWN_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t WebCamUsed_t807____TRUE_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry WebCamUsed_t807____TRUE_3_DefaultValue = 
{
	&WebCamUsed_t807____TRUE_3_FieldInfo/* field */
	, { (char*)&WebCamUsed_t807____TRUE_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t WebCamUsed_t807____FALSE_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry WebCamUsed_t807____FALSE_4_DefaultValue = 
{
	&WebCamUsed_t807____FALSE_4_FieldInfo/* field */
	, { (char*)&WebCamUsed_t807____FALSE_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* WebCamUsed_t807_FieldDefaultValues[] = 
{
	&WebCamUsed_t807____UNKNOWN_2_DefaultValue,
	&WebCamUsed_t807____TRUE_3_DefaultValue,
	&WebCamUsed_t807____FALSE_4_DefaultValue,
	NULL
};
static MethodInfo* WebCamUsed_t807_MethodInfos[] =
{
	NULL
};
extern MethodInfo Enum_Equals_m587_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo Enum_GetHashCode_m588_MethodInfo;
extern MethodInfo Enum_ToString_m589_MethodInfo;
extern MethodInfo Enum_ToString_m590_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToBoolean_m591_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToByte_m592_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToChar_m593_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDateTime_m594_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDecimal_m595_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDouble_m596_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt16_m597_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt32_m598_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt64_m599_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSByte_m600_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSingle_m601_MethodInfo;
extern MethodInfo Enum_ToString_m602_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToType_m603_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt16_m604_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt32_m605_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt64_m606_MethodInfo;
extern MethodInfo Enum_CompareTo_m607_MethodInfo;
extern MethodInfo Enum_GetTypeCode_m608_MethodInfo;
static MethodInfo* WebCamUsed_t807_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
extern TypeInfo IFormattable_t182_il2cpp_TypeInfo;
extern TypeInfo IConvertible_t183_il2cpp_TypeInfo;
extern TypeInfo IComparable_t184_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair WebCamUsed_t807_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WebCamUsed_t807_0_0_0;
extern Il2CppType WebCamUsed_t807_1_0_0;
extern TypeInfo Enum_t185_il2cpp_TypeInfo;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t123_il2cpp_TypeInfo;
extern TypeInfo QCARRuntimeUtilities_t156_il2cpp_TypeInfo;
TypeInfo WebCamUsed_t807_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebCamUsed"/* name */
	, ""/* namespaze */
	, WebCamUsed_t807_MethodInfos/* methods */
	, NULL/* properties */
	, WebCamUsed_t807_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, WebCamUsed_t807_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &WebCamUsed_t807_0_0_0/* byval_arg */
	, &WebCamUsed_t807_1_0_0/* this_arg */
	, WebCamUsed_t807_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, WebCamUsed_t807_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebCamUsed_t807)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Vuforia.QCARRuntimeUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitie.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARRuntimeUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitieMethodDeclarations.h"

// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.String
#include "mscorlib_System_String.h"
#include "mscorlib_ArrayTypes.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.Void
#include "mscorlib_System_Void.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehav.h"
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"
// System.Single
#include "mscorlib_System_Single.h"
// Vuforia.OrientedBoundingBox
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox.h"
extern TypeInfo SurfaceUtilities_t136_il2cpp_TypeInfo;
extern TypeInfo CharU5BU5D_t378_il2cpp_TypeInfo;
extern TypeInfo Char_t371_il2cpp_TypeInfo;
extern TypeInfo TrackableBehaviour_t17_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo TrackableBehaviourU5BU5D_t857_il2cpp_TypeInfo;
extern TypeInfo WebCamAbstractBehaviour_t63_il2cpp_TypeInfo;
extern TypeInfo VideoModeData_t602_il2cpp_TypeInfo;
extern TypeInfo Vector2_t99_il2cpp_TypeInfo;
extern TypeInfo Mathf_t179_il2cpp_TypeInfo;
extern TypeInfo OrientedBoundingBox_t634_il2cpp_TypeInfo;
// Vuforia.SurfaceUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUtilitiesMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehavMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2IMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// Vuforia.OrientedBoundingBox
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBoxMethodDeclarations.h"
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern Il2CppType TrackableBehaviour_t17_0_0_0;
extern Il2CppType WebCamAbstractBehaviour_t63_0_0_0;
extern MethodInfo SurfaceUtilities_GetSurfaceOrientation_m4337_MethodInfo;
extern MethodInfo QCARRuntimeUtilities_get_ScreenOrientation_m4321_MethodInfo;
extern MethodInfo QCARRuntimeUtilities_get_IsLandscapeOrientation_m4322_MethodInfo;
extern MethodInfo String_Split_m5524_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Object_FindObjectsOfType_m812_MethodInfo;
extern MethodInfo Behaviour_set_enabled_m547_MethodInfo;
extern MethodInfo Application_get_isEditor_m2532_MethodInfo;
extern MethodInfo QCARRuntimeUtilities_IsPlayMode_m416_MethodInfo;
extern MethodInfo Object_FindObjectOfType_m256_MethodInfo;
extern MethodInfo WebCamAbstractBehaviour_IsWebCamUsed_m4405_MethodInfo;
extern MethodInfo Rect_get_xMin_m2425_MethodInfo;
extern MethodInfo Rect_get_yMin_m2424_MethodInfo;
extern MethodInfo Rect_get_width_m679_MethodInfo;
extern MethodInfo Rect_get_height_m680_MethodInfo;
extern MethodInfo QCARRuntimeUtilities_PrepareCoordinateConversion_m4332_MethodInfo;
extern MethodInfo Mathf_RoundToInt_m2379_MethodInfo;
extern MethodInfo Vec2I__ctor_m3174_MethodInfo;
extern MethodInfo Vector2__ctor_m636_MethodInfo;
extern MethodInfo OrientedBoundingBox_get_Center_m2958_MethodInfo;
extern MethodInfo QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4326_MethodInfo;
extern MethodInfo OrientedBoundingBox_get_HalfExtents_m2960_MethodInfo;
extern MethodInfo OrientedBoundingBox_get_Rotation_m2962_MethodInfo;
extern MethodInfo OrientedBoundingBox__ctor_m2957_MethodInfo;
extern MethodInfo Rect_get_xMax_m2399_MethodInfo;
extern MethodInfo Rect_get_yMax_m2401_MethodInfo;
extern MethodInfo Rect__ctor_m262_MethodInfo;
extern MethodInfo Screen_set_sleepTimeout_m5502_MethodInfo;
extern MethodInfo Object__ctor_m312_MethodInfo;


// System.String Vuforia.QCARRuntimeUtilities::StripFileNameFromPath(System.String)
extern MethodInfo QCARRuntimeUtilities_StripFileNameFromPath_m4319_MethodInfo;
 String_t* QCARRuntimeUtilities_StripFileNameFromPath_m4319 (Object_t * __this/* static, unused */, String_t* ___fullPath, MethodInfo* method){
	StringU5BU5D_t862* V_0 = {0};
	String_t* V_1 = {0};
	CharU5BU5D_t378* V_2 = {0};
	{
		V_2 = ((CharU5BU5D_t378*)SZArrayNew(InitializedTypeInfo(&CharU5BU5D_t378_il2cpp_TypeInfo), 1));
		NullCheck(V_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_2, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(V_2, 0)) = (uint16_t)((int32_t)47);
		NullCheck(___fullPath);
		StringU5BU5D_t862* L_0 = String_Split_m5524(___fullPath, V_2, /*hidden argument*/&String_Split_m5524_MethodInfo);
		V_0 = L_0;
		NullCheck(V_0);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ((int32_t)((((int32_t)(((Array_t *)V_0)->max_length)))-1)));
		int32_t L_1 = ((int32_t)((((int32_t)(((Array_t *)V_0)->max_length)))-1));
		V_1 = (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_1));
		return V_1;
	}
}
// System.String Vuforia.QCARRuntimeUtilities::StripExtensionFromPath(System.String)
extern MethodInfo QCARRuntimeUtilities_StripExtensionFromPath_m4320_MethodInfo;
 String_t* QCARRuntimeUtilities_StripExtensionFromPath_m4320 (Object_t * __this/* static, unused */, String_t* ___fullPath, MethodInfo* method){
	StringU5BU5D_t862* V_0 = {0};
	String_t* V_1 = {0};
	CharU5BU5D_t378* V_2 = {0};
	{
		V_2 = ((CharU5BU5D_t378*)SZArrayNew(InitializedTypeInfo(&CharU5BU5D_t378_il2cpp_TypeInfo), 1));
		NullCheck(V_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_2, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(V_2, 0)) = (uint16_t)((int32_t)46);
		NullCheck(___fullPath);
		StringU5BU5D_t862* L_0 = String_Split_m5524(___fullPath, V_2, /*hidden argument*/&String_Split_m5524_MethodInfo);
		V_0 = L_0;
		NullCheck(V_0);
		if ((((int32_t)(((int32_t)(((Array_t *)V_0)->max_length)))) > ((int32_t)1)))
		{
			goto IL_0020;
		}
	}
	{
		return (String_t*) &_stringLiteral141;
	}

IL_0020:
	{
		NullCheck(V_0);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ((int32_t)((((int32_t)(((Array_t *)V_0)->max_length)))-1)));
		int32_t L_1 = ((int32_t)((((int32_t)(((Array_t *)V_0)->max_length)))-1));
		V_1 = (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_1));
		return V_1;
	}
}
// UnityEngine.ScreenOrientation Vuforia.QCARRuntimeUtilities::get_ScreenOrientation()
 int32_t QCARRuntimeUtilities_get_ScreenOrientation_m4321 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SurfaceUtilities_t136_il2cpp_TypeInfo));
		int32_t L_0 = SurfaceUtilities_GetSurfaceOrientation_m4337(NULL /*static, unused*/, /*hidden argument*/&SurfaceUtilities_GetSurfaceOrientation_m4337_MethodInfo);
		return L_0;
	}
}
// System.Boolean Vuforia.QCARRuntimeUtilities::get_IsLandscapeOrientation()
 bool QCARRuntimeUtilities_get_IsLandscapeOrientation_m4322 (Object_t * __this/* static, unused */, MethodInfo* method){
	int32_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		int32_t L_0 = QCARRuntimeUtilities_get_ScreenOrientation_m4321(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_ScreenOrientation_m4321_MethodInfo);
		V_0 = L_0;
		if ((((int32_t)V_0) == ((int32_t)3)))
		{
			goto IL_0013;
		}
	}
	{
		if ((((int32_t)V_0) == ((int32_t)3)))
		{
			goto IL_0013;
		}
	}
	{
		return ((((int32_t)V_0) == ((int32_t)4))? 1 : 0);
	}

IL_0013:
	{
		return 1;
	}
}
// System.Boolean Vuforia.QCARRuntimeUtilities::get_IsPortraitOrientation()
extern MethodInfo QCARRuntimeUtilities_get_IsPortraitOrientation_m4323_MethodInfo;
 bool QCARRuntimeUtilities_get_IsPortraitOrientation_m4323 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_get_IsLandscapeOrientation_m4322(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_IsLandscapeOrientation_m4322_MethodInfo);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Vuforia.QCARRuntimeUtilities::ForceDisableTrackables()
extern MethodInfo QCARRuntimeUtilities_ForceDisableTrackables_m4324_MethodInfo;
 void QCARRuntimeUtilities_ForceDisableTrackables_m4324 (Object_t * __this/* static, unused */, MethodInfo* method){
	TrackableBehaviourU5BU5D_t857* V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&TrackableBehaviour_t17_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		ObjectU5BU5D_t227* L_1 = Object_FindObjectsOfType_m812(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_FindObjectsOfType_m812_MethodInfo);
		V_0 = ((TrackableBehaviourU5BU5D_t857*)Castclass(L_1, InitializedTypeInfo(&TrackableBehaviourU5BU5D_t857_il2cpp_TypeInfo)));
		if (!V_0)
		{
			goto IL_002f;
		}
	}
	{
		V_1 = 0;
		goto IL_0029;
	}

IL_001c:
	{
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, V_1);
		int32_t L_2 = V_1;
		NullCheck((*(TrackableBehaviour_t17 **)(TrackableBehaviour_t17 **)SZArrayLdElema(V_0, L_2)));
		Behaviour_set_enabled_m547((*(TrackableBehaviour_t17 **)(TrackableBehaviour_t17 **)SZArrayLdElema(V_0, L_2)), 0, /*hidden argument*/&Behaviour_set_enabled_m547_MethodInfo);
		V_1 = ((int32_t)(V_1+1));
	}

IL_0029:
	{
		NullCheck(V_0);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((Array_t *)V_0)->max_length))))))
		{
			goto IL_001c;
		}
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean Vuforia.QCARRuntimeUtilities::IsPlayMode()
 bool QCARRuntimeUtilities_IsPlayMode_m416 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		bool L_0 = Application_get_isEditor_m2532(NULL /*static, unused*/, /*hidden argument*/&Application_get_isEditor_m2532_MethodInfo);
		return L_0;
	}
}
// System.Boolean Vuforia.QCARRuntimeUtilities::IsQCAREnabled()
extern MethodInfo QCARRuntimeUtilities_IsQCAREnabled_m361_MethodInfo;
 bool QCARRuntimeUtilities_IsQCAREnabled_m361 (Object_t * __this/* static, unused */, MethodInfo* method){
	WebCamAbstractBehaviour_t63 * V_0 = {0};
	int32_t G_B5_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m416(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsPlayMode_m416_MethodInfo);
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		if ((((QCARRuntimeUtilities_t156_StaticFields*)InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo)->static_fields)->___sWebCamUsed_0))
		{
			goto IL_0034;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&WebCamAbstractBehaviour_t63_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Object_t120 * L_2 = Object_FindObjectOfType_m256(NULL /*static, unused*/, L_1, /*hidden argument*/&Object_FindObjectOfType_m256_MethodInfo);
		V_0 = ((WebCamAbstractBehaviour_t63 *)Castclass(L_2, InitializedTypeInfo(&WebCamAbstractBehaviour_t63_il2cpp_TypeInfo)));
		NullCheck(V_0);
		bool L_3 = WebCamAbstractBehaviour_IsWebCamUsed_m4405(V_0, /*hidden argument*/&WebCamAbstractBehaviour_IsWebCamUsed_m4405_MethodInfo);
		if (L_3)
		{
			goto IL_002e;
		}
	}
	{
		G_B5_0 = 2;
		goto IL_002f;
	}

IL_002e:
	{
		G_B5_0 = 1;
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		((QCARRuntimeUtilities_t156_StaticFields*)InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo)->static_fields)->___sWebCamUsed_0 = G_B5_0;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		return ((((int32_t)(((QCARRuntimeUtilities_t156_StaticFields*)InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo)->static_fields)->___sWebCamUsed_0)) == ((int32_t)1))? 1 : 0);
	}

IL_003d:
	{
		return 1;
	}
}
// Vuforia.QCARRenderer/Vec2I Vuforia.QCARRuntimeUtilities::ScreenSpaceToCameraFrameCoordinates(UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
extern MethodInfo QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4325_MethodInfo;
 Vec2I_t675  QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4325 (Object_t * __this/* static, unused */, Vector2_t99  ___screenSpaceCoordinate, Rect_t103  ___bgTextureViewPortRect, bool ___isTextureMirrored, VideoModeData_t602  ___videoModeData, MethodInfo* method){
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	Vec2I_t675  V_13 = {0};
	{
		float L_0 = Rect_get_xMin_m2425((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_xMin_m2425_MethodInfo);
		V_0 = L_0;
		float L_1 = Rect_get_yMin_m2424((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_yMin_m2424_MethodInfo);
		V_1 = L_1;
		float L_2 = Rect_get_width_m679((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_width_m679_MethodInfo);
		V_2 = L_2;
		float L_3 = Rect_get_height_m680((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_height_m680_MethodInfo);
		V_3 = L_3;
		V_4 = 0;
		NullCheck((&___videoModeData));
		int32_t L_4 = ((&___videoModeData)->___width_0);
		V_5 = (((float)L_4));
		NullCheck((&___videoModeData));
		int32_t L_5 = ((&___videoModeData)->___height_1);
		V_6 = (((float)L_5));
		V_7 = (0.0f);
		V_8 = (0.0f);
		V_9 = (0.0f);
		V_10 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		QCARRuntimeUtilities_PrepareCoordinateConversion_m4332(NULL /*static, unused*/, ___isTextureMirrored, (&V_7), (&V_8), (&V_9), (&V_10), (&V_4), /*hidden argument*/&QCARRuntimeUtilities_PrepareCoordinateConversion_m4332_MethodInfo);
		NullCheck((&___screenSpaceCoordinate));
		float L_6 = ((&___screenSpaceCoordinate)->___x_1);
		V_11 = ((float)((float)((float)(L_6-V_0))/(float)V_2));
		NullCheck((&___screenSpaceCoordinate));
		float L_7 = ((&___screenSpaceCoordinate)->___y_2);
		V_12 = ((float)((float)((float)(L_7-V_1))/(float)V_3));
		if (!V_4)
		{
			goto IL_00aa;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		int32_t L_8 = Mathf_RoundToInt_m2379(NULL /*static, unused*/, ((float)((float)((float)(V_7+((float)((float)V_9*(float)V_12))))*(float)V_5)), /*hidden argument*/&Mathf_RoundToInt_m2379_MethodInfo);
		int32_t L_9 = Mathf_RoundToInt_m2379(NULL /*static, unused*/, ((float)((float)((float)(V_8+((float)((float)V_10*(float)V_11))))*(float)V_6)), /*hidden argument*/&Mathf_RoundToInt_m2379_MethodInfo);
		Vec2I__ctor_m3174((&V_13), L_8, L_9, /*hidden argument*/&Vec2I__ctor_m3174_MethodInfo);
		goto IL_00d1;
	}

IL_00aa:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		int32_t L_10 = Mathf_RoundToInt_m2379(NULL /*static, unused*/, ((float)((float)((float)(V_7+((float)((float)V_9*(float)V_11))))*(float)V_5)), /*hidden argument*/&Mathf_RoundToInt_m2379_MethodInfo);
		int32_t L_11 = Mathf_RoundToInt_m2379(NULL /*static, unused*/, ((float)((float)((float)(V_8+((float)((float)V_10*(float)V_12))))*(float)V_6)), /*hidden argument*/&Mathf_RoundToInt_m2379_MethodInfo);
		Vec2I__ctor_m3174((&V_13), L_10, L_11, /*hidden argument*/&Vec2I__ctor_m3174_MethodInfo);
	}

IL_00d1:
	{
		return V_13;
	}
}
// UnityEngine.Vector2 Vuforia.QCARRuntimeUtilities::CameraFrameToScreenSpaceCoordinates(UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
 Vector2_t99  QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4326 (Object_t * __this/* static, unused */, Vector2_t99  ___cameraFrameCoordinate, Rect_t103  ___bgTextureViewPortRect, bool ___isTextureMirrored, VideoModeData_t602  ___videoModeData, MethodInfo* method){
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	Vector2_t99  V_13 = {0};
	{
		float L_0 = Rect_get_xMin_m2425((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_xMin_m2425_MethodInfo);
		V_0 = L_0;
		float L_1 = Rect_get_yMin_m2424((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_yMin_m2424_MethodInfo);
		V_1 = L_1;
		float L_2 = Rect_get_width_m679((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_width_m679_MethodInfo);
		V_2 = L_2;
		float L_3 = Rect_get_height_m680((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_height_m680_MethodInfo);
		V_3 = L_3;
		V_4 = 0;
		NullCheck((&___videoModeData));
		int32_t L_4 = ((&___videoModeData)->___width_0);
		V_5 = (((float)L_4));
		NullCheck((&___videoModeData));
		int32_t L_5 = ((&___videoModeData)->___height_1);
		V_6 = (((float)L_5));
		V_7 = (0.0f);
		V_8 = (0.0f);
		V_9 = (0.0f);
		V_10 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		QCARRuntimeUtilities_PrepareCoordinateConversion_m4332(NULL /*static, unused*/, ___isTextureMirrored, (&V_7), (&V_8), (&V_9), (&V_10), (&V_4), /*hidden argument*/&QCARRuntimeUtilities_PrepareCoordinateConversion_m4332_MethodInfo);
		NullCheck((&___cameraFrameCoordinate));
		float L_6 = ((&___cameraFrameCoordinate)->___x_1);
		V_11 = ((float)((float)((float)(((float)((float)L_6/(float)V_5))-V_7))/(float)V_9));
		NullCheck((&___cameraFrameCoordinate));
		float L_7 = ((&___cameraFrameCoordinate)->___y_2);
		V_12 = ((float)((float)((float)(((float)((float)L_7/(float)V_6))-V_8))/(float)V_10));
		if (!V_4)
		{
			goto IL_00a0;
		}
	}
	{
		Vector2__ctor_m636((&V_13), ((float)(((float)((float)V_2*(float)V_12))+V_0)), ((float)(((float)((float)V_3*(float)V_11))+V_1)), /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		goto IL_00b3;
	}

IL_00a0:
	{
		Vector2__ctor_m636((&V_13), ((float)(((float)((float)V_2*(float)V_11))+V_0)), ((float)(((float)((float)V_3*(float)V_12))+V_1)), /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
	}

IL_00b3:
	{
		return V_13;
	}
}
// Vuforia.OrientedBoundingBox Vuforia.QCARRuntimeUtilities::CameraFrameToScreenSpaceCoordinates(Vuforia.OrientedBoundingBox,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
extern MethodInfo QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4327_MethodInfo;
 OrientedBoundingBox_t634  QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4327 (Object_t * __this/* static, unused */, OrientedBoundingBox_t634  ___cameraFrameObb, Rect_t103  ___bgTextureViewPortRect, bool ___isTextureMirrored, VideoModeData_t602  ___videoModeData, MethodInfo* method){
	bool V_0 = false;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector2_t99  V_4 = {0};
	Vector2_t99  V_5 = {0};
	float V_6 = 0.0f;
	int32_t V_7 = {0};
	float G_B7_0 = 0.0f;
	float G_B6_0 = 0.0f;
	int32_t G_B8_0 = 0;
	float G_B8_1 = 0.0f;
	float G_B10_0 = 0.0f;
	float G_B9_0 = 0.0f;
	int32_t G_B11_0 = 0;
	float G_B11_1 = 0.0f;
	{
		V_0 = 0;
		V_1 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		int32_t L_0 = QCARRuntimeUtilities_get_ScreenOrientation_m4321(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_ScreenOrientation_m4321_MethodInfo);
		V_7 = L_0;
		if (((int32_t)(V_7-1)) == 0)
		{
			goto IL_002a;
		}
		if (((int32_t)(V_7-1)) == 1)
		{
			goto IL_0040;
		}
		if (((int32_t)(V_7-1)) == 2)
		{
			goto IL_004a;
		}
		if (((int32_t)(V_7-1)) == 3)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_004a;
	}

IL_002a:
	{
		V_1 = ((float)(V_1+(90.0f)));
		V_0 = 1;
		goto IL_004a;
	}

IL_0036:
	{
		V_1 = ((float)(V_1+(180.0f)));
		goto IL_004a;
	}

IL_0040:
	{
		V_1 = ((float)(V_1+(270.0f)));
		V_0 = 1;
	}

IL_004a:
	{
		float L_1 = Rect_get_width_m679((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_width_m679_MethodInfo);
		G_B6_0 = L_1;
		if (V_0)
		{
			G_B7_0 = L_1;
			goto IL_005d;
		}
	}
	{
		NullCheck((&___videoModeData));
		int32_t L_2 = ((&___videoModeData)->___width_0);
		G_B8_0 = L_2;
		G_B8_1 = G_B6_0;
		goto IL_0064;
	}

IL_005d:
	{
		NullCheck((&___videoModeData));
		int32_t L_3 = ((&___videoModeData)->___height_1);
		G_B8_0 = L_3;
		G_B8_1 = G_B7_0;
	}

IL_0064:
	{
		V_2 = ((float)((float)G_B8_1/(float)(((float)G_B8_0))));
		float L_4 = Rect_get_height_m680((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_height_m680_MethodInfo);
		G_B9_0 = L_4;
		if (V_0)
		{
			G_B10_0 = L_4;
			goto IL_007a;
		}
	}
	{
		NullCheck((&___videoModeData));
		int32_t L_5 = ((&___videoModeData)->___height_1);
		G_B11_0 = L_5;
		G_B11_1 = G_B9_0;
		goto IL_0081;
	}

IL_007a:
	{
		NullCheck((&___videoModeData));
		int32_t L_6 = ((&___videoModeData)->___width_0);
		G_B11_0 = L_6;
		G_B11_1 = G_B10_0;
	}

IL_0081:
	{
		V_3 = ((float)((float)G_B11_1/(float)(((float)G_B11_0))));
		Vector2_t99  L_7 = OrientedBoundingBox_get_Center_m2958((&___cameraFrameObb), /*hidden argument*/&OrientedBoundingBox_get_Center_m2958_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		Vector2_t99  L_8 = QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4326(NULL /*static, unused*/, L_7, ___bgTextureViewPortRect, ___isTextureMirrored, ___videoModeData, /*hidden argument*/&QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4326_MethodInfo);
		V_4 = L_8;
		Vector2_t99  L_9 = OrientedBoundingBox_get_HalfExtents_m2960((&___cameraFrameObb), /*hidden argument*/&OrientedBoundingBox_get_HalfExtents_m2960_MethodInfo);
		;
		float L_10 = (L_9.___x_1);
		Vector2_t99  L_11 = OrientedBoundingBox_get_HalfExtents_m2960((&___cameraFrameObb), /*hidden argument*/&OrientedBoundingBox_get_HalfExtents_m2960_MethodInfo);
		;
		float L_12 = (L_11.___y_2);
		Vector2__ctor_m636((&V_5), ((float)((float)L_10*(float)V_2)), ((float)((float)L_12*(float)V_3)), /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		float L_13 = OrientedBoundingBox_get_Rotation_m2962((&___cameraFrameObb), /*hidden argument*/&OrientedBoundingBox_get_Rotation_m2962_MethodInfo);
		V_6 = L_13;
		if (!___isTextureMirrored)
		{
			goto IL_00c9;
		}
	}
	{
		V_6 = ((-V_6));
	}

IL_00c9:
	{
		V_6 = ((float)(((float)((float)((float)((float)V_6*(float)(180.0f)))/(float)(3.14159274f)))+V_1));
		OrientedBoundingBox_t634  L_14 = {0};
		OrientedBoundingBox__ctor_m2957(&L_14, V_4, V_5, V_6, /*hidden argument*/&OrientedBoundingBox__ctor_m2957_MethodInfo);
		return L_14;
	}
}
// System.Void Vuforia.QCARRuntimeUtilities::SelectRectTopLeftAndBottomRightForLandscapeLeft(UnityEngine.Rect,System.Boolean,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern MethodInfo QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4328_MethodInfo;
 void QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4328 (Object_t * __this/* static, unused */, Rect_t103  ___screenSpaceRect, bool ___isMirrored, Vector2_t99 * ___topLeft, Vector2_t99 * ___bottomRight, MethodInfo* method){
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	{
		if (___isMirrored)
		{
			goto IL_00f5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		int32_t L_0 = QCARRuntimeUtilities_get_ScreenOrientation_m4321(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_ScreenOrientation_m4321_MethodInfo);
		V_0 = L_0;
		if (((int32_t)(V_0-1)) == 0)
		{
			goto IL_005c;
		}
		if (((int32_t)(V_0-1)) == 1)
		{
			goto IL_008f;
		}
		if (((int32_t)(V_0-1)) == 2)
		{
			goto IL_00c2;
		}
		if (((int32_t)(V_0-1)) == 3)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_00c2;
	}

IL_0029:
	{
		float L_1 = Rect_get_xMax_m2399((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2399_MethodInfo);
		float L_2 = Rect_get_yMax_m2401((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2401_MethodInfo);
		Vector2_t99  L_3 = {0};
		Vector2__ctor_m636(&L_3, L_1, L_2, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___topLeft = L_3;
		float L_4 = Rect_get_xMin_m2425((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2425_MethodInfo);
		float L_5 = Rect_get_yMin_m2424((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2424_MethodInfo);
		Vector2_t99  L_6 = {0};
		Vector2__ctor_m636(&L_6, L_4, L_5, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___bottomRight = L_6;
		return;
	}

IL_005c:
	{
		float L_7 = Rect_get_xMax_m2399((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2399_MethodInfo);
		float L_8 = Rect_get_yMin_m2424((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2424_MethodInfo);
		Vector2_t99  L_9 = {0};
		Vector2__ctor_m636(&L_9, L_7, L_8, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___topLeft = L_9;
		float L_10 = Rect_get_xMin_m2425((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2425_MethodInfo);
		float L_11 = Rect_get_yMax_m2401((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2401_MethodInfo);
		Vector2_t99  L_12 = {0};
		Vector2__ctor_m636(&L_12, L_10, L_11, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___bottomRight = L_12;
		return;
	}

IL_008f:
	{
		float L_13 = Rect_get_xMin_m2425((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2425_MethodInfo);
		float L_14 = Rect_get_yMax_m2401((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2401_MethodInfo);
		Vector2_t99  L_15 = {0};
		Vector2__ctor_m636(&L_15, L_13, L_14, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___topLeft = L_15;
		float L_16 = Rect_get_xMax_m2399((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2399_MethodInfo);
		float L_17 = Rect_get_yMin_m2424((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2424_MethodInfo);
		Vector2_t99  L_18 = {0};
		Vector2__ctor_m636(&L_18, L_16, L_17, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___bottomRight = L_18;
		return;
	}

IL_00c2:
	{
		float L_19 = Rect_get_xMin_m2425((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2425_MethodInfo);
		float L_20 = Rect_get_yMin_m2424((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2424_MethodInfo);
		Vector2_t99  L_21 = {0};
		Vector2__ctor_m636(&L_21, L_19, L_20, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___topLeft = L_21;
		float L_22 = Rect_get_xMax_m2399((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2399_MethodInfo);
		float L_23 = Rect_get_yMax_m2401((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2401_MethodInfo);
		Vector2_t99  L_24 = {0};
		Vector2__ctor_m636(&L_24, L_22, L_23, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___bottomRight = L_24;
		return;
	}

IL_00f5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		int32_t L_25 = QCARRuntimeUtilities_get_ScreenOrientation_m4321(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_ScreenOrientation_m4321_MethodInfo);
		V_1 = L_25;
		if (((int32_t)(V_1-1)) == 0)
		{
			goto IL_014b;
		}
		if (((int32_t)(V_1-1)) == 1)
		{
			goto IL_017e;
		}
		if (((int32_t)(V_1-1)) == 2)
		{
			goto IL_01b1;
		}
		if (((int32_t)(V_1-1)) == 3)
		{
			goto IL_0118;
		}
	}
	{
		goto IL_01b1;
	}

IL_0118:
	{
		float L_26 = Rect_get_xMin_m2425((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2425_MethodInfo);
		float L_27 = Rect_get_yMax_m2401((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2401_MethodInfo);
		Vector2_t99  L_28 = {0};
		Vector2__ctor_m636(&L_28, L_26, L_27, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___topLeft = L_28;
		float L_29 = Rect_get_xMax_m2399((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2399_MethodInfo);
		float L_30 = Rect_get_yMin_m2424((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2424_MethodInfo);
		Vector2_t99  L_31 = {0};
		Vector2__ctor_m636(&L_31, L_29, L_30, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___bottomRight = L_31;
		return;
	}

IL_014b:
	{
		float L_32 = Rect_get_xMax_m2399((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2399_MethodInfo);
		float L_33 = Rect_get_yMax_m2401((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2401_MethodInfo);
		Vector2_t99  L_34 = {0};
		Vector2__ctor_m636(&L_34, L_32, L_33, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___topLeft = L_34;
		float L_35 = Rect_get_xMin_m2425((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2425_MethodInfo);
		float L_36 = Rect_get_yMin_m2424((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2424_MethodInfo);
		Vector2_t99  L_37 = {0};
		Vector2__ctor_m636(&L_37, L_35, L_36, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___bottomRight = L_37;
		return;
	}

IL_017e:
	{
		float L_38 = Rect_get_xMin_m2425((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2425_MethodInfo);
		float L_39 = Rect_get_yMin_m2424((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2424_MethodInfo);
		Vector2_t99  L_40 = {0};
		Vector2__ctor_m636(&L_40, L_38, L_39, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___topLeft = L_40;
		float L_41 = Rect_get_xMax_m2399((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2399_MethodInfo);
		float L_42 = Rect_get_yMax_m2401((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2401_MethodInfo);
		Vector2_t99  L_43 = {0};
		Vector2__ctor_m636(&L_43, L_41, L_42, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___bottomRight = L_43;
		return;
	}

IL_01b1:
	{
		float L_44 = Rect_get_xMax_m2399((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2399_MethodInfo);
		float L_45 = Rect_get_yMin_m2424((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2424_MethodInfo);
		Vector2_t99  L_46 = {0};
		Vector2__ctor_m636(&L_46, L_44, L_45, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___topLeft = L_46;
		float L_47 = Rect_get_xMin_m2425((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2425_MethodInfo);
		float L_48 = Rect_get_yMax_m2401((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2401_MethodInfo);
		Vector2_t99  L_49 = {0};
		Vector2__ctor_m636(&L_49, L_47, L_48, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		*___bottomRight = L_49;
		return;
	}
}
// UnityEngine.Rect Vuforia.QCARRuntimeUtilities::CalculateRectFromLandscapeLeftCorners(UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern MethodInfo QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4329_MethodInfo;
 Rect_t103  QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4329 (Object_t * __this/* static, unused */, Vector2_t99  ___topLeft, Vector2_t99  ___bottomRight, bool ___isMirrored, MethodInfo* method){
	Rect_t103  V_0 = {0};
	int32_t V_1 = {0};
	int32_t V_2 = {0};
	{
		if (___isMirrored)
		{
			goto IL_0109;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		int32_t L_0 = QCARRuntimeUtilities_get_ScreenOrientation_m4321(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_ScreenOrientation_m4321_MethodInfo);
		V_1 = L_0;
		if (((int32_t)(V_1-1)) == 0)
		{
			goto IL_0061;
		}
		if (((int32_t)(V_1-1)) == 1)
		{
			goto IL_0099;
		}
		if (((int32_t)(V_1-1)) == 2)
		{
			goto IL_00d1;
		}
		if (((int32_t)(V_1-1)) == 3)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_00d1;
	}

IL_0029:
	{
		NullCheck((&___bottomRight));
		float L_1 = ((&___bottomRight)->___x_1);
		NullCheck((&___bottomRight));
		float L_2 = ((&___bottomRight)->___y_2);
		NullCheck((&___topLeft));
		float L_3 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_4 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_5 = ((&___topLeft)->___y_2);
		NullCheck((&___bottomRight));
		float L_6 = ((&___bottomRight)->___y_2);
		Rect__ctor_m262((&V_0), L_1, L_2, ((float)(L_3-L_4)), ((float)(L_5-L_6)), /*hidden argument*/&Rect__ctor_m262_MethodInfo);
		goto IL_0201;
	}

IL_0061:
	{
		NullCheck((&___bottomRight));
		float L_7 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_8 = ((&___topLeft)->___y_2);
		NullCheck((&___topLeft));
		float L_9 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_10 = ((&___bottomRight)->___x_1);
		NullCheck((&___bottomRight));
		float L_11 = ((&___bottomRight)->___y_2);
		NullCheck((&___topLeft));
		float L_12 = ((&___topLeft)->___y_2);
		Rect__ctor_m262((&V_0), L_7, L_8, ((float)(L_9-L_10)), ((float)(L_11-L_12)), /*hidden argument*/&Rect__ctor_m262_MethodInfo);
		goto IL_0201;
	}

IL_0099:
	{
		NullCheck((&___topLeft));
		float L_13 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_14 = ((&___bottomRight)->___y_2);
		NullCheck((&___bottomRight));
		float L_15 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_16 = ((&___topLeft)->___x_1);
		NullCheck((&___topLeft));
		float L_17 = ((&___topLeft)->___y_2);
		NullCheck((&___bottomRight));
		float L_18 = ((&___bottomRight)->___y_2);
		Rect__ctor_m262((&V_0), L_13, L_14, ((float)(L_15-L_16)), ((float)(L_17-L_18)), /*hidden argument*/&Rect__ctor_m262_MethodInfo);
		goto IL_0201;
	}

IL_00d1:
	{
		NullCheck((&___topLeft));
		float L_19 = ((&___topLeft)->___x_1);
		NullCheck((&___topLeft));
		float L_20 = ((&___topLeft)->___y_2);
		NullCheck((&___bottomRight));
		float L_21 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_22 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_23 = ((&___bottomRight)->___y_2);
		NullCheck((&___topLeft));
		float L_24 = ((&___topLeft)->___y_2);
		Rect__ctor_m262((&V_0), L_19, L_20, ((float)(L_21-L_22)), ((float)(L_23-L_24)), /*hidden argument*/&Rect__ctor_m262_MethodInfo);
		goto IL_0201;
	}

IL_0109:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		int32_t L_25 = QCARRuntimeUtilities_get_ScreenOrientation_m4321(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_ScreenOrientation_m4321_MethodInfo);
		V_2 = L_25;
		if (((int32_t)(V_2-1)) == 0)
		{
			goto IL_0164;
		}
		if (((int32_t)(V_2-1)) == 1)
		{
			goto IL_0199;
		}
		if (((int32_t)(V_2-1)) == 2)
		{
			goto IL_01ce;
		}
		if (((int32_t)(V_2-1)) == 3)
		{
			goto IL_012c;
		}
	}
	{
		goto IL_01ce;
	}

IL_012c:
	{
		NullCheck((&___topLeft));
		float L_26 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_27 = ((&___bottomRight)->___y_2);
		NullCheck((&___bottomRight));
		float L_28 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_29 = ((&___topLeft)->___x_1);
		NullCheck((&___topLeft));
		float L_30 = ((&___topLeft)->___y_2);
		NullCheck((&___bottomRight));
		float L_31 = ((&___bottomRight)->___y_2);
		Rect__ctor_m262((&V_0), L_26, L_27, ((float)(L_28-L_29)), ((float)(L_30-L_31)), /*hidden argument*/&Rect__ctor_m262_MethodInfo);
		goto IL_0201;
	}

IL_0164:
	{
		NullCheck((&___bottomRight));
		float L_32 = ((&___bottomRight)->___x_1);
		NullCheck((&___bottomRight));
		float L_33 = ((&___bottomRight)->___y_2);
		NullCheck((&___topLeft));
		float L_34 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_35 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_36 = ((&___topLeft)->___y_2);
		NullCheck((&___bottomRight));
		float L_37 = ((&___bottomRight)->___y_2);
		Rect__ctor_m262((&V_0), L_32, L_33, ((float)(L_34-L_35)), ((float)(L_36-L_37)), /*hidden argument*/&Rect__ctor_m262_MethodInfo);
		goto IL_0201;
	}

IL_0199:
	{
		NullCheck((&___topLeft));
		float L_38 = ((&___topLeft)->___x_1);
		NullCheck((&___topLeft));
		float L_39 = ((&___topLeft)->___y_2);
		NullCheck((&___bottomRight));
		float L_40 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_41 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_42 = ((&___bottomRight)->___y_2);
		NullCheck((&___topLeft));
		float L_43 = ((&___topLeft)->___y_2);
		Rect__ctor_m262((&V_0), L_38, L_39, ((float)(L_40-L_41)), ((float)(L_42-L_43)), /*hidden argument*/&Rect__ctor_m262_MethodInfo);
		goto IL_0201;
	}

IL_01ce:
	{
		NullCheck((&___bottomRight));
		float L_44 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_45 = ((&___topLeft)->___y_2);
		NullCheck((&___topLeft));
		float L_46 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_47 = ((&___bottomRight)->___x_1);
		NullCheck((&___bottomRight));
		float L_48 = ((&___bottomRight)->___y_2);
		NullCheck((&___topLeft));
		float L_49 = ((&___topLeft)->___y_2);
		Rect__ctor_m262((&V_0), L_44, L_45, ((float)(L_46-L_47)), ((float)(L_48-L_49)), /*hidden argument*/&Rect__ctor_m262_MethodInfo);
	}

IL_0201:
	{
		return V_0;
	}
}
// System.Void Vuforia.QCARRuntimeUtilities::DisableSleepMode()
extern MethodInfo QCARRuntimeUtilities_DisableSleepMode_m4330_MethodInfo;
 void QCARRuntimeUtilities_DisableSleepMode_m4330 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		Screen_set_sleepTimeout_m5502(NULL /*static, unused*/, (-1), /*hidden argument*/&Screen_set_sleepTimeout_m5502_MethodInfo);
		return;
	}
}
// System.Void Vuforia.QCARRuntimeUtilities::ResetSleepMode()
extern MethodInfo QCARRuntimeUtilities_ResetSleepMode_m4331_MethodInfo;
 void QCARRuntimeUtilities_ResetSleepMode_m4331 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		Screen_set_sleepTimeout_m5502(NULL /*static, unused*/, ((int32_t)-2), /*hidden argument*/&Screen_set_sleepTimeout_m5502_MethodInfo);
		return;
	}
}
// System.Void Vuforia.QCARRuntimeUtilities::PrepareCoordinateConversion(System.Boolean,System.Single&,System.Single&,System.Single&,System.Single&,System.Boolean&)
 void QCARRuntimeUtilities_PrepareCoordinateConversion_m4332 (Object_t * __this/* static, unused */, bool ___isTextureMirrored, float* ___prefixX, float* ___prefixY, float* ___inversionMultiplierX, float* ___inversionMultiplierY, bool* ___isPortrait, MethodInfo* method){
	int32_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		int32_t L_0 = QCARRuntimeUtilities_get_ScreenOrientation_m4321(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_ScreenOrientation_m4321_MethodInfo);
		V_0 = L_0;
		if (((int32_t)(V_0-1)) == 0)
		{
			goto IL_0023;
		}
		if (((int32_t)(V_0-1)) == 1)
		{
			goto IL_0066;
		}
		if (((int32_t)(V_0-1)) == 2)
		{
			goto IL_00ec;
		}
		if (((int32_t)(V_0-1)) == 3)
		{
			goto IL_00a9;
		}
	}
	{
		goto IL_00ec;
	}

IL_0023:
	{
		*((int8_t*)(___isPortrait)) = (int8_t)1;
		if (___isTextureMirrored)
		{
			goto IL_0048;
		}
	}
	{
		*((float*)(___prefixX)) = (float)(0.0f);
		*((float*)(___prefixY)) = (float)(1.0f);
		*((float*)(___inversionMultiplierX)) = (float)(1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(-1.0f);
		return;
	}

IL_0048:
	{
		*((float*)(___prefixX)) = (float)(1.0f);
		*((float*)(___prefixY)) = (float)(1.0f);
		*((float*)(___inversionMultiplierX)) = (float)(-1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(-1.0f);
		return;
	}

IL_0066:
	{
		*((int8_t*)(___isPortrait)) = (int8_t)1;
		if (___isTextureMirrored)
		{
			goto IL_008b;
		}
	}
	{
		*((float*)(___prefixX)) = (float)(1.0f);
		*((float*)(___prefixY)) = (float)(0.0f);
		*((float*)(___inversionMultiplierX)) = (float)(-1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(1.0f);
		return;
	}

IL_008b:
	{
		*((float*)(___prefixX)) = (float)(0.0f);
		*((float*)(___prefixY)) = (float)(0.0f);
		*((float*)(___inversionMultiplierX)) = (float)(1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(1.0f);
		return;
	}

IL_00a9:
	{
		*((int8_t*)(___isPortrait)) = (int8_t)0;
		if (___isTextureMirrored)
		{
			goto IL_00ce;
		}
	}
	{
		*((float*)(___prefixX)) = (float)(1.0f);
		*((float*)(___prefixY)) = (float)(1.0f);
		*((float*)(___inversionMultiplierX)) = (float)(-1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(-1.0f);
		return;
	}

IL_00ce:
	{
		*((float*)(___prefixX)) = (float)(0.0f);
		*((float*)(___prefixY)) = (float)(1.0f);
		*((float*)(___inversionMultiplierX)) = (float)(1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(-1.0f);
		return;
	}

IL_00ec:
	{
		*((int8_t*)(___isPortrait)) = (int8_t)0;
		if (___isTextureMirrored)
		{
			goto IL_0111;
		}
	}
	{
		*((float*)(___prefixX)) = (float)(0.0f);
		*((float*)(___prefixY)) = (float)(0.0f);
		*((float*)(___inversionMultiplierX)) = (float)(1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(1.0f);
		return;
	}

IL_0111:
	{
		*((float*)(___prefixX)) = (float)(1.0f);
		*((float*)(___prefixY)) = (float)(0.0f);
		*((float*)(___inversionMultiplierX)) = (float)(-1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(1.0f);
		return;
	}
}
// System.Void Vuforia.QCARRuntimeUtilities::.ctor()
extern MethodInfo QCARRuntimeUtilities__ctor_m4333_MethodInfo;
 void QCARRuntimeUtilities__ctor_m4333 (QCARRuntimeUtilities_t156 * __this, MethodInfo* method){
	{
		Object__ctor_m312(__this, /*hidden argument*/&Object__ctor_m312_MethodInfo);
		return;
	}
}
// System.Void Vuforia.QCARRuntimeUtilities::.cctor()
extern MethodInfo QCARRuntimeUtilities__cctor_m4334_MethodInfo;
 void QCARRuntimeUtilities__cctor_m4334 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition Vuforia.QCARRuntimeUtilities
extern Il2CppType WebCamUsed_t807_0_0_17;
FieldInfo QCARRuntimeUtilities_t156____sWebCamUsed_0_FieldInfo = 
{
	"sWebCamUsed"/* name */
	, &WebCamUsed_t807_0_0_17/* type */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* parent */
	, offsetof(QCARRuntimeUtilities_t156_StaticFields, ___sWebCamUsed_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* QCARRuntimeUtilities_t156_FieldInfos[] =
{
	&QCARRuntimeUtilities_t156____sWebCamUsed_0_FieldInfo,
	NULL
};
static PropertyInfo QCARRuntimeUtilities_t156____ScreenOrientation_PropertyInfo = 
{
	&QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* parent */
	, "ScreenOrientation"/* name */
	, &QCARRuntimeUtilities_get_ScreenOrientation_m4321_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo QCARRuntimeUtilities_t156____IsLandscapeOrientation_PropertyInfo = 
{
	&QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* parent */
	, "IsLandscapeOrientation"/* name */
	, &QCARRuntimeUtilities_get_IsLandscapeOrientation_m4322_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo QCARRuntimeUtilities_t156____IsPortraitOrientation_PropertyInfo = 
{
	&QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* parent */
	, "IsPortraitOrientation"/* name */
	, &QCARRuntimeUtilities_get_IsPortraitOrientation_m4323_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* QCARRuntimeUtilities_t156_PropertyInfos[] =
{
	&QCARRuntimeUtilities_t156____ScreenOrientation_PropertyInfo,
	&QCARRuntimeUtilities_t156____IsLandscapeOrientation_PropertyInfo,
	&QCARRuntimeUtilities_t156____IsPortraitOrientation_PropertyInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo QCARRuntimeUtilities_t156_QCARRuntimeUtilities_StripFileNameFromPath_m4319_ParameterInfos[] = 
{
	{"fullPath", 0, 134219982, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.QCARRuntimeUtilities::StripFileNameFromPath(System.String)
MethodInfo QCARRuntimeUtilities_StripFileNameFromPath_m4319_MethodInfo = 
{
	"StripFileNameFromPath"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_StripFileNameFromPath_m4319/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, QCARRuntimeUtilities_t156_QCARRuntimeUtilities_StripFileNameFromPath_m4319_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2340/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo QCARRuntimeUtilities_t156_QCARRuntimeUtilities_StripExtensionFromPath_m4320_ParameterInfos[] = 
{
	{"fullPath", 0, 134219983, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.QCARRuntimeUtilities::StripExtensionFromPath(System.String)
MethodInfo QCARRuntimeUtilities_StripExtensionFromPath_m4320_MethodInfo = 
{
	"StripExtensionFromPath"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_StripExtensionFromPath_m4320/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, QCARRuntimeUtilities_t156_QCARRuntimeUtilities_StripExtensionFromPath_m4320_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2341/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ScreenOrientation_t137_0_0_0;
extern void* RuntimeInvoker_ScreenOrientation_t137 (MethodInfo* method, void* obj, void** args);
// UnityEngine.ScreenOrientation Vuforia.QCARRuntimeUtilities::get_ScreenOrientation()
MethodInfo QCARRuntimeUtilities_get_ScreenOrientation_m4321_MethodInfo = 
{
	"get_ScreenOrientation"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_get_ScreenOrientation_m4321/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &ScreenOrientation_t137_0_0_0/* return_type */
	, RuntimeInvoker_ScreenOrientation_t137/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2342/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.QCARRuntimeUtilities::get_IsLandscapeOrientation()
MethodInfo QCARRuntimeUtilities_get_IsLandscapeOrientation_m4322_MethodInfo = 
{
	"get_IsLandscapeOrientation"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_get_IsLandscapeOrientation_m4322/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2343/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.QCARRuntimeUtilities::get_IsPortraitOrientation()
MethodInfo QCARRuntimeUtilities_get_IsPortraitOrientation_m4323_MethodInfo = 
{
	"get_IsPortraitOrientation"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_get_IsPortraitOrientation_m4323/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2344/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::ForceDisableTrackables()
MethodInfo QCARRuntimeUtilities_ForceDisableTrackables_m4324_MethodInfo = 
{
	"ForceDisableTrackables"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_ForceDisableTrackables_m4324/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2345/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.QCARRuntimeUtilities::IsPlayMode()
MethodInfo QCARRuntimeUtilities_IsPlayMode_m416_MethodInfo = 
{
	"IsPlayMode"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_IsPlayMode_m416/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2346/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.QCARRuntimeUtilities::IsQCAREnabled()
MethodInfo QCARRuntimeUtilities_IsQCAREnabled_m361_MethodInfo = 
{
	"IsQCAREnabled"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_IsQCAREnabled_m361/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2347/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t99_0_0_0;
extern Il2CppType Vector2_t99_0_0_0;
extern Il2CppType Rect_t103_0_0_0;
extern Il2CppType Rect_t103_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppType VideoModeData_t602_0_0_0;
extern Il2CppType VideoModeData_t602_0_0_0;
static ParameterInfo QCARRuntimeUtilities_t156_QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4325_ParameterInfos[] = 
{
	{"screenSpaceCoordinate", 0, 134219984, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
	{"bgTextureViewPortRect", 1, 134219985, &EmptyCustomAttributesCache, &Rect_t103_0_0_0},
	{"isTextureMirrored", 2, 134219986, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
	{"videoModeData", 3, 134219987, &EmptyCustomAttributesCache, &VideoModeData_t602_0_0_0},
};
extern Il2CppType Vec2I_t675_0_0_0;
extern void* RuntimeInvoker_Vec2I_t675_Vector2_t99_Rect_t103_SByte_t125_VideoModeData_t602 (MethodInfo* method, void* obj, void** args);
// Vuforia.QCARRenderer/Vec2I Vuforia.QCARRuntimeUtilities::ScreenSpaceToCameraFrameCoordinates(UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
MethodInfo QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4325_MethodInfo = 
{
	"ScreenSpaceToCameraFrameCoordinates"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4325/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &Vec2I_t675_0_0_0/* return_type */
	, RuntimeInvoker_Vec2I_t675_Vector2_t99_Rect_t103_SByte_t125_VideoModeData_t602/* invoker_method */
	, QCARRuntimeUtilities_t156_QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4325_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2348/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t99_0_0_0;
extern Il2CppType Rect_t103_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppType VideoModeData_t602_0_0_0;
static ParameterInfo QCARRuntimeUtilities_t156_QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4326_ParameterInfos[] = 
{
	{"cameraFrameCoordinate", 0, 134219988, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
	{"bgTextureViewPortRect", 1, 134219989, &EmptyCustomAttributesCache, &Rect_t103_0_0_0},
	{"isTextureMirrored", 2, 134219990, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
	{"videoModeData", 3, 134219991, &EmptyCustomAttributesCache, &VideoModeData_t602_0_0_0},
};
extern Il2CppType Vector2_t99_0_0_0;
extern void* RuntimeInvoker_Vector2_t99_Vector2_t99_Rect_t103_SByte_t125_VideoModeData_t602 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Vuforia.QCARRuntimeUtilities::CameraFrameToScreenSpaceCoordinates(UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
MethodInfo QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4326_MethodInfo = 
{
	"CameraFrameToScreenSpaceCoordinates"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4326/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t99_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t99_Vector2_t99_Rect_t103_SByte_t125_VideoModeData_t602/* invoker_method */
	, QCARRuntimeUtilities_t156_QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4326_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2349/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType OrientedBoundingBox_t634_0_0_0;
extern Il2CppType OrientedBoundingBox_t634_0_0_0;
extern Il2CppType Rect_t103_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppType VideoModeData_t602_0_0_0;
static ParameterInfo QCARRuntimeUtilities_t156_QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4327_ParameterInfos[] = 
{
	{"cameraFrameObb", 0, 134219992, &EmptyCustomAttributesCache, &OrientedBoundingBox_t634_0_0_0},
	{"bgTextureViewPortRect", 1, 134219993, &EmptyCustomAttributesCache, &Rect_t103_0_0_0},
	{"isTextureMirrored", 2, 134219994, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
	{"videoModeData", 3, 134219995, &EmptyCustomAttributesCache, &VideoModeData_t602_0_0_0},
};
extern Il2CppType OrientedBoundingBox_t634_0_0_0;
extern void* RuntimeInvoker_OrientedBoundingBox_t634_OrientedBoundingBox_t634_Rect_t103_SByte_t125_VideoModeData_t602 (MethodInfo* method, void* obj, void** args);
// Vuforia.OrientedBoundingBox Vuforia.QCARRuntimeUtilities::CameraFrameToScreenSpaceCoordinates(Vuforia.OrientedBoundingBox,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
MethodInfo QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4327_MethodInfo = 
{
	"CameraFrameToScreenSpaceCoordinates"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4327/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &OrientedBoundingBox_t634_0_0_0/* return_type */
	, RuntimeInvoker_OrientedBoundingBox_t634_OrientedBoundingBox_t634_Rect_t103_SByte_t125_VideoModeData_t602/* invoker_method */
	, QCARRuntimeUtilities_t156_QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4327_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2350/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Rect_t103_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppType Vector2_t99_1_0_2;
extern Il2CppType Vector2_t99_1_0_0;
extern Il2CppType Vector2_t99_1_0_2;
static ParameterInfo QCARRuntimeUtilities_t156_QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4328_ParameterInfos[] = 
{
	{"screenSpaceRect", 0, 134219996, &EmptyCustomAttributesCache, &Rect_t103_0_0_0},
	{"isMirrored", 1, 134219997, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
	{"topLeft", 2, 134219998, &EmptyCustomAttributesCache, &Vector2_t99_1_0_2},
	{"bottomRight", 3, 134219999, &EmptyCustomAttributesCache, &Vector2_t99_1_0_2},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Rect_t103_SByte_t125_Vector2U26_t940_Vector2U26_t940 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::SelectRectTopLeftAndBottomRightForLandscapeLeft(UnityEngine.Rect,System.Boolean,UnityEngine.Vector2&,UnityEngine.Vector2&)
MethodInfo QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4328_MethodInfo = 
{
	"SelectRectTopLeftAndBottomRightForLandscapeLeft"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4328/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Rect_t103_SByte_t125_Vector2U26_t940_Vector2U26_t940/* invoker_method */
	, QCARRuntimeUtilities_t156_QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4328_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2351/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t99_0_0_0;
extern Il2CppType Vector2_t99_0_0_0;
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo QCARRuntimeUtilities_t156_QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4329_ParameterInfos[] = 
{
	{"topLeft", 0, 134220000, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
	{"bottomRight", 1, 134220001, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
	{"isMirrored", 2, 134220002, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Rect_t103_0_0_0;
extern void* RuntimeInvoker_Rect_t103_Vector2_t99_Vector2_t99_SByte_t125 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Rect Vuforia.QCARRuntimeUtilities::CalculateRectFromLandscapeLeftCorners(UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
MethodInfo QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4329_MethodInfo = 
{
	"CalculateRectFromLandscapeLeftCorners"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4329/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &Rect_t103_0_0_0/* return_type */
	, RuntimeInvoker_Rect_t103_Vector2_t99_Vector2_t99_SByte_t125/* invoker_method */
	, QCARRuntimeUtilities_t156_QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4329_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2352/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::DisableSleepMode()
MethodInfo QCARRuntimeUtilities_DisableSleepMode_m4330_MethodInfo = 
{
	"DisableSleepMode"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_DisableSleepMode_m4330/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2353/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::ResetSleepMode()
MethodInfo QCARRuntimeUtilities_ResetSleepMode_m4331_MethodInfo = 
{
	"ResetSleepMode"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_ResetSleepMode_m4331/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2354/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppType Single_t170_1_0_0;
extern Il2CppType Single_t170_1_0_0;
extern Il2CppType Single_t170_1_0_0;
extern Il2CppType Single_t170_1_0_0;
extern Il2CppType Single_t170_1_0_0;
extern Il2CppType Boolean_t122_1_0_0;
extern Il2CppType Boolean_t122_1_0_0;
static ParameterInfo QCARRuntimeUtilities_t156_QCARRuntimeUtilities_PrepareCoordinateConversion_m4332_ParameterInfos[] = 
{
	{"isTextureMirrored", 0, 134220003, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
	{"prefixX", 1, 134220004, &EmptyCustomAttributesCache, &Single_t170_1_0_0},
	{"prefixY", 2, 134220005, &EmptyCustomAttributesCache, &Single_t170_1_0_0},
	{"inversionMultiplierX", 3, 134220006, &EmptyCustomAttributesCache, &Single_t170_1_0_0},
	{"inversionMultiplierY", 4, 134220007, &EmptyCustomAttributesCache, &Single_t170_1_0_0},
	{"isPortrait", 5, 134220008, &EmptyCustomAttributesCache, &Boolean_t122_1_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125_SingleU26_t941_SingleU26_t941_SingleU26_t941_SingleU26_t941_BooleanU26_t490 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::PrepareCoordinateConversion(System.Boolean,System.Single&,System.Single&,System.Single&,System.Single&,System.Boolean&)
MethodInfo QCARRuntimeUtilities_PrepareCoordinateConversion_m4332_MethodInfo = 
{
	"PrepareCoordinateConversion"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_PrepareCoordinateConversion_m4332/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125_SingleU26_t941_SingleU26_t941_SingleU26_t941_SingleU26_t941_BooleanU26_t490/* invoker_method */
	, QCARRuntimeUtilities_t156_QCARRuntimeUtilities_PrepareCoordinateConversion_m4332_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2355/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::.ctor()
MethodInfo QCARRuntimeUtilities__ctor_m4333_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&QCARRuntimeUtilities__ctor_m4333/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2356/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::.cctor()
MethodInfo QCARRuntimeUtilities__cctor_m4334_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&QCARRuntimeUtilities__cctor_m4334/* method */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2357/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* QCARRuntimeUtilities_t156_MethodInfos[] =
{
	&QCARRuntimeUtilities_StripFileNameFromPath_m4319_MethodInfo,
	&QCARRuntimeUtilities_StripExtensionFromPath_m4320_MethodInfo,
	&QCARRuntimeUtilities_get_ScreenOrientation_m4321_MethodInfo,
	&QCARRuntimeUtilities_get_IsLandscapeOrientation_m4322_MethodInfo,
	&QCARRuntimeUtilities_get_IsPortraitOrientation_m4323_MethodInfo,
	&QCARRuntimeUtilities_ForceDisableTrackables_m4324_MethodInfo,
	&QCARRuntimeUtilities_IsPlayMode_m416_MethodInfo,
	&QCARRuntimeUtilities_IsQCAREnabled_m361_MethodInfo,
	&QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4325_MethodInfo,
	&QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4326_MethodInfo,
	&QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4327_MethodInfo,
	&QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4328_MethodInfo,
	&QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4329_MethodInfo,
	&QCARRuntimeUtilities_DisableSleepMode_m4330_MethodInfo,
	&QCARRuntimeUtilities_ResetSleepMode_m4331_MethodInfo,
	&QCARRuntimeUtilities_PrepareCoordinateConversion_m4332_MethodInfo,
	&QCARRuntimeUtilities__ctor_m4333_MethodInfo,
	&QCARRuntimeUtilities__cctor_m4334_MethodInfo,
	NULL
};
extern TypeInfo WebCamUsed_t807_il2cpp_TypeInfo;
static TypeInfo* QCARRuntimeUtilities_t156_il2cpp_TypeInfo__nestedTypes[2] =
{
	&WebCamUsed_t807_il2cpp_TypeInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
static MethodInfo* QCARRuntimeUtilities_t156_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType QCARRuntimeUtilities_t156_0_0_0;
extern Il2CppType QCARRuntimeUtilities_t156_1_0_0;
extern TypeInfo Object_t_il2cpp_TypeInfo;
struct QCARRuntimeUtilities_t156;
TypeInfo QCARRuntimeUtilities_t156_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "QCARRuntimeUtilities"/* name */
	, "Vuforia"/* namespaze */
	, QCARRuntimeUtilities_t156_MethodInfos/* methods */
	, QCARRuntimeUtilities_t156_PropertyInfos/* properties */
	, QCARRuntimeUtilities_t156_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, QCARRuntimeUtilities_t156_il2cpp_TypeInfo__nestedTypes/* nested_types */
	, NULL/* nested_in */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, QCARRuntimeUtilities_t156_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &QCARRuntimeUtilities_t156_il2cpp_TypeInfo/* cast_class */
	, &QCARRuntimeUtilities_t156_0_0_0/* byval_arg */
	, &QCARRuntimeUtilities_t156_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (QCARRuntimeUtilities_t156)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(QCARRuntimeUtilities_t156_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 18/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.SurfaceUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUtilities.h"
#ifndef _MSC_VER
#else
#endif

// Vuforia.QCARRendererImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl.h"
// Vuforia.QCARRendererImpl/RenderEvent
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl_Re.h"
extern TypeInfo QCARRenderer_t706_il2cpp_TypeInfo;
extern TypeInfo QCARWrapper_t754_il2cpp_TypeInfo;
extern TypeInfo IQCARWrapper_t753_il2cpp_TypeInfo;
extern TypeInfo Boolean_t122_il2cpp_TypeInfo;
extern TypeInfo QCARUnityImpl_t709_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
// Vuforia.QCARRenderer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererMethodDeclarations.h"
// Vuforia.QCARRendererImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImplMethodDeclarations.h"
// Vuforia.QCARWrapper
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARWrapperMethodDeclarations.h"
// Vuforia.QCARUnityImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnityImplMethodDeclarations.h"
extern MethodInfo QCARRenderer_get_InternalInstance_m3176_MethodInfo;
extern MethodInfo QCARRendererImpl_UnityRenderEvent_m3187_MethodInfo;
extern MethodInfo QCARWrapper_get_Instance_m4090_MethodInfo;
extern MethodInfo IQCARWrapper_HasSurfaceBeenRecreated_m5235_MethodInfo;
extern MethodInfo QCARUnityImpl_SetRendererDirty_m3197_MethodInfo;
extern MethodInfo IQCARWrapper_OnSurfaceChanged_m5234_MethodInfo;


// System.Void Vuforia.SurfaceUtilities::OnSurfaceCreated()
extern MethodInfo SurfaceUtilities_OnSurfaceCreated_m318_MethodInfo;
 void SurfaceUtilities_OnSurfaceCreated_m318 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRenderer_t706_il2cpp_TypeInfo));
		QCARRendererImpl_t707 * L_0 = QCARRenderer_get_InternalInstance_m3176(NULL /*static, unused*/, /*hidden argument*/&QCARRenderer_get_InternalInstance_m3176_MethodInfo);
		NullCheck(L_0);
		QCARRendererImpl_UnityRenderEvent_m3187(L_0, ((int32_t)101), /*hidden argument*/&QCARRendererImpl_UnityRenderEvent_m3187_MethodInfo);
		return;
	}
}
// System.Void Vuforia.SurfaceUtilities::OnSurfaceDeinit()
extern MethodInfo SurfaceUtilities_OnSurfaceDeinit_m4335_MethodInfo;
 void SurfaceUtilities_OnSurfaceDeinit_m4335 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRenderer_t706_il2cpp_TypeInfo));
		QCARRendererImpl_t707 * L_0 = QCARRenderer_get_InternalInstance_m3176(NULL /*static, unused*/, /*hidden argument*/&QCARRenderer_get_InternalInstance_m3176_MethodInfo);
		NullCheck(L_0);
		QCARRendererImpl_UnityRenderEvent_m3187(L_0, ((int32_t)105), /*hidden argument*/&QCARRendererImpl_UnityRenderEvent_m3187_MethodInfo);
		return;
	}
}
// System.Boolean Vuforia.SurfaceUtilities::HasSurfaceBeenRecreated()
extern MethodInfo SurfaceUtilities_HasSurfaceBeenRecreated_m313_MethodInfo;
 bool SurfaceUtilities_HasSurfaceBeenRecreated_m313 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARWrapper_t754_il2cpp_TypeInfo));
		Object_t * L_0 = QCARWrapper_get_Instance_m4090(NULL /*static, unused*/, /*hidden argument*/&QCARWrapper_get_Instance_m4090_MethodInfo);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&IQCARWrapper_HasSurfaceBeenRecreated_m5235_MethodInfo, L_0);
		return L_1;
	}
}
// System.Void Vuforia.SurfaceUtilities::OnSurfaceChanged(System.Int32,System.Int32)
extern MethodInfo SurfaceUtilities_OnSurfaceChanged_m4336_MethodInfo;
 void SurfaceUtilities_OnSurfaceChanged_m4336 (Object_t * __this/* static, unused */, int32_t ___screenWidth, int32_t ___screenHeight, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARUnityImpl_t709_il2cpp_TypeInfo));
		QCARUnityImpl_SetRendererDirty_m3197(NULL /*static, unused*/, /*hidden argument*/&QCARUnityImpl_SetRendererDirty_m3197_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARWrapper_t754_il2cpp_TypeInfo));
		Object_t * L_0 = QCARWrapper_get_Instance_m4090(NULL /*static, unused*/, /*hidden argument*/&QCARWrapper_get_Instance_m4090_MethodInfo);
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, int32_t >::Invoke(&IQCARWrapper_OnSurfaceChanged_m5234_MethodInfo, L_0, ___screenWidth, ___screenHeight);
		return;
	}
}
// System.Void Vuforia.SurfaceUtilities::SetSurfaceOrientation(UnityEngine.ScreenOrientation)
extern MethodInfo SurfaceUtilities_SetSurfaceOrientation_m319_MethodInfo;
 void SurfaceUtilities_SetSurfaceOrientation_m319 (Object_t * __this/* static, unused */, int32_t ___screenOrientation, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SurfaceUtilities_t136_il2cpp_TypeInfo));
		((SurfaceUtilities_t136_StaticFields*)InitializedTypeInfo(&SurfaceUtilities_t136_il2cpp_TypeInfo)->static_fields)->___mScreenOrientation_0 = ___screenOrientation;
		return;
	}
}
// UnityEngine.ScreenOrientation Vuforia.SurfaceUtilities::GetSurfaceOrientation()
 int32_t SurfaceUtilities_GetSurfaceOrientation_m4337 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SurfaceUtilities_t136_il2cpp_TypeInfo));
		return (((SurfaceUtilities_t136_StaticFields*)InitializedTypeInfo(&SurfaceUtilities_t136_il2cpp_TypeInfo)->static_fields)->___mScreenOrientation_0);
	}
}
// System.Void Vuforia.SurfaceUtilities::.cctor()
extern MethodInfo SurfaceUtilities__cctor_m4338_MethodInfo;
 void SurfaceUtilities__cctor_m4338 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition Vuforia.SurfaceUtilities
extern Il2CppType ScreenOrientation_t137_0_0_17;
FieldInfo SurfaceUtilities_t136____mScreenOrientation_0_FieldInfo = 
{
	"mScreenOrientation"/* name */
	, &ScreenOrientation_t137_0_0_17/* type */
	, &SurfaceUtilities_t136_il2cpp_TypeInfo/* parent */
	, offsetof(SurfaceUtilities_t136_StaticFields, ___mScreenOrientation_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* SurfaceUtilities_t136_FieldInfos[] =
{
	&SurfaceUtilities_t136____mScreenOrientation_0_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::OnSurfaceCreated()
MethodInfo SurfaceUtilities_OnSurfaceCreated_m318_MethodInfo = 
{
	"OnSurfaceCreated"/* name */
	, (methodPointerType)&SurfaceUtilities_OnSurfaceCreated_m318/* method */
	, &SurfaceUtilities_t136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2358/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::OnSurfaceDeinit()
MethodInfo SurfaceUtilities_OnSurfaceDeinit_m4335_MethodInfo = 
{
	"OnSurfaceDeinit"/* name */
	, (methodPointerType)&SurfaceUtilities_OnSurfaceDeinit_m4335/* method */
	, &SurfaceUtilities_t136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2359/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.SurfaceUtilities::HasSurfaceBeenRecreated()
MethodInfo SurfaceUtilities_HasSurfaceBeenRecreated_m313_MethodInfo = 
{
	"HasSurfaceBeenRecreated"/* name */
	, (methodPointerType)&SurfaceUtilities_HasSurfaceBeenRecreated_m313/* method */
	, &SurfaceUtilities_t136_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2360/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo SurfaceUtilities_t136_SurfaceUtilities_OnSurfaceChanged_m4336_ParameterInfos[] = 
{
	{"screenWidth", 0, 134220009, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"screenHeight", 1, 134220010, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::OnSurfaceChanged(System.Int32,System.Int32)
MethodInfo SurfaceUtilities_OnSurfaceChanged_m4336_MethodInfo = 
{
	"OnSurfaceChanged"/* name */
	, (methodPointerType)&SurfaceUtilities_OnSurfaceChanged_m4336/* method */
	, &SurfaceUtilities_t136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, SurfaceUtilities_t136_SurfaceUtilities_OnSurfaceChanged_m4336_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2361/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ScreenOrientation_t137_0_0_0;
extern Il2CppType ScreenOrientation_t137_0_0_0;
static ParameterInfo SurfaceUtilities_t136_SurfaceUtilities_SetSurfaceOrientation_m319_ParameterInfos[] = 
{
	{"screenOrientation", 0, 134220011, &EmptyCustomAttributesCache, &ScreenOrientation_t137_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::SetSurfaceOrientation(UnityEngine.ScreenOrientation)
MethodInfo SurfaceUtilities_SetSurfaceOrientation_m319_MethodInfo = 
{
	"SetSurfaceOrientation"/* name */
	, (methodPointerType)&SurfaceUtilities_SetSurfaceOrientation_m319/* method */
	, &SurfaceUtilities_t136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, SurfaceUtilities_t136_SurfaceUtilities_SetSurfaceOrientation_m319_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2362/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ScreenOrientation_t137_0_0_0;
extern void* RuntimeInvoker_ScreenOrientation_t137 (MethodInfo* method, void* obj, void** args);
// UnityEngine.ScreenOrientation Vuforia.SurfaceUtilities::GetSurfaceOrientation()
MethodInfo SurfaceUtilities_GetSurfaceOrientation_m4337_MethodInfo = 
{
	"GetSurfaceOrientation"/* name */
	, (methodPointerType)&SurfaceUtilities_GetSurfaceOrientation_m4337/* method */
	, &SurfaceUtilities_t136_il2cpp_TypeInfo/* declaring_type */
	, &ScreenOrientation_t137_0_0_0/* return_type */
	, RuntimeInvoker_ScreenOrientation_t137/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2363/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::.cctor()
MethodInfo SurfaceUtilities__cctor_m4338_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&SurfaceUtilities__cctor_m4338/* method */
	, &SurfaceUtilities_t136_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2364/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SurfaceUtilities_t136_MethodInfos[] =
{
	&SurfaceUtilities_OnSurfaceCreated_m318_MethodInfo,
	&SurfaceUtilities_OnSurfaceDeinit_m4335_MethodInfo,
	&SurfaceUtilities_HasSurfaceBeenRecreated_m313_MethodInfo,
	&SurfaceUtilities_OnSurfaceChanged_m4336_MethodInfo,
	&SurfaceUtilities_SetSurfaceOrientation_m319_MethodInfo,
	&SurfaceUtilities_GetSurfaceOrientation_m4337_MethodInfo,
	&SurfaceUtilities__cctor_m4338_MethodInfo,
	NULL
};
static MethodInfo* SurfaceUtilities_t136_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType SurfaceUtilities_t136_0_0_0;
extern Il2CppType SurfaceUtilities_t136_1_0_0;
struct SurfaceUtilities_t136;
TypeInfo SurfaceUtilities_t136_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "SurfaceUtilities"/* name */
	, "Vuforia"/* namespaze */
	, SurfaceUtilities_t136_MethodInfos/* methods */
	, NULL/* properties */
	, SurfaceUtilities_t136_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SurfaceUtilities_t136_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SurfaceUtilities_t136_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SurfaceUtilities_t136_il2cpp_TypeInfo/* cast_class */
	, &SurfaceUtilities_t136_0_0_0/* byval_arg */
	, &SurfaceUtilities_t136_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SurfaceUtilities_t136)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SurfaceUtilities_t136_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBeh.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo;
// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBehMethodDeclarations.h"

// Vuforia.WordFilterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilterMode.h"
// Vuforia.WordPrefabCreationMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordPrefabCreationM.h"
// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio.h"
// System.Action
#include "System_Core_System_Action.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3.h"
// Vuforia.KeepAliveAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBe.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// Vuforia.TextTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTracker.h"
// Vuforia.TrackerManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManager.h"
// Vuforia.Tracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Tracker.h"
// Vuforia.WordList
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordList.h"
// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_45.h"
// Vuforia.WordResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordResult.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_20.h"
// Vuforia.WordManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordManager.h"
// Vuforia.StateManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateManager.h"
// Vuforia.WordManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordManagerImpl.h"
extern TypeInfo QCARAbstractBehaviour_t45_il2cpp_TypeInfo;
extern TypeInfo Action_t147_il2cpp_TypeInfo;
extern TypeInfo Action_1_t802_il2cpp_TypeInfo;
extern TypeInfo TrackerManager_t787_il2cpp_TypeInfo;
extern TypeInfo TextTracker_t722_il2cpp_TypeInfo;
extern TypeInfo Tracker_t659_il2cpp_TypeInfo;
extern TypeInfo WordList_t723_il2cpp_TypeInfo;
extern TypeInfo List_1_t808_il2cpp_TypeInfo;
extern TypeInfo ITextRecoEventHandler_t809_il2cpp_TypeInfo;
extern TypeInfo String_t_il2cpp_TypeInfo;
extern TypeInfo WordFilterMode_t816_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t735_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t942_il2cpp_TypeInfo;
extern TypeInfo Word_t736_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t943_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t734_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t944_il2cpp_TypeInfo;
extern TypeInfo WordResult_t747_il2cpp_TypeInfo;
extern TypeInfo StateManager_t768_il2cpp_TypeInfo;
extern TypeInfo WordManager_t733_il2cpp_TypeInfo;
extern TypeInfo WordManagerImpl_t744_il2cpp_TypeInfo;
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavioMethodDeclarations.h"
// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3MethodDeclarations.h"
// Vuforia.KeepAliveAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBeMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// Vuforia.TrackerManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManagerMethodDeclarations.h"
// Vuforia.Tracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerMethodDeclarations.h"
// Vuforia.TextTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrackerMethodDeclarations.h"
// Vuforia.WordList
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordListMethodDeclarations.h"
// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_45MethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_20MethodDeclarations.h"
// Vuforia.StateManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateManagerMethodDeclarations.h"
// Vuforia.WordManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordManagerImplMethodDeclarations.h"
// Vuforia.WordManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordManagerMethodDeclarations.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern Il2CppType QCARAbstractBehaviour_t45_0_0_0;
extern MethodInfo Object_op_Implicit_m257_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_OnQCARInitialized_m4351_MethodInfo;
extern MethodInfo Action__ctor_m4584_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_RegisterQCARInitializedCallback_m4279_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_OnQCARStarted_m4352_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_RegisterQCARStartedCallback_m4281_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_OnTrackablesUpdated_m4353_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_RegisterTrackablesUpdatedCallback_m4283_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_OnPause_m4354_MethodInfo;
extern MethodInfo Action_1__ctor_m4672_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_RegisterOnPauseCallback_m4285_MethodInfo;
extern MethodInfo KeepAliveAbstractBehaviour_get_Instance_m4106_MethodInfo;
extern MethodInfo Object_op_Inequality_m489_MethodInfo;
extern MethodInfo KeepAliveAbstractBehaviour_get_KeepTextRecoBehaviourAlive_m4098_MethodInfo;
extern MethodInfo Component_get_gameObject_m419_MethodInfo;
extern MethodInfo Object_DontDestroyOnLoad_m4535_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_StartTextTracker_m4347_MethodInfo;
extern MethodInfo TrackerManager_get_Instance_m4207_MethodInfo;
extern MethodInfo TrackerManager_GetTracker_TisTextTracker_t722_m5508_MethodInfo;
extern MethodInfo Tracker_get_IsActive_m3087_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_StopTextTracker_m4348_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_UnregisterQCARInitializedCallback_m4280_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4282_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_UnregisterTrackablesUpdatedCallback_m4284_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_UnregisterOnPauseCallback_m4286_MethodInfo;
extern MethodInfo TextTracker_get_WordList_m5084_MethodInfo;
extern MethodInfo WordList_UnloadAllLists_m5206_MethodInfo;
extern MethodInfo List_1_Add_m5525_MethodInfo;
extern MethodInfo ITextRecoEventHandler_OnInitialized_m5266_MethodInfo;
extern MethodInfo List_1_Remove_m5526_MethodInfo;
extern MethodInfo Debug_Log_m288_MethodInfo;
extern MethodInfo Tracker_Start_m4674_MethodInfo;
extern MethodInfo Tracker_Stop_m4675_MethodInfo;
extern MethodInfo WordList_LoadWordListFile_m5197_MethodInfo;
extern MethodInfo String_op_Inequality_m658_MethodInfo;
extern MethodInfo WordList_AddWordsFromFile_m5200_MethodInfo;
extern MethodInfo String_get_Length_m2431_MethodInfo;
extern MethodInfo WordList_AddWord_m5203_MethodInfo;
extern MethodInfo WordList_SetFilterMode_m5208_MethodInfo;
extern MethodInfo WordList_LoadFilterListFile_m5209_MethodInfo;
extern MethodInfo WordList_AddWordToFilterList_m5212_MethodInfo;
extern MethodInfo IEnumerable_1_GetEnumerator_m5527_MethodInfo;
extern MethodInfo IEnumerator_1_get_Current_m5528_MethodInfo;
extern MethodInfo List_1_GetEnumerator_m5529_MethodInfo;
extern MethodInfo Enumerator_get_Current_m5530_MethodInfo;
extern MethodInfo ITextRecoEventHandler_OnWordLost_m5268_MethodInfo;
extern MethodInfo Enumerator_MoveNext_m5531_MethodInfo;
extern MethodInfo IDisposable_Dispose_m333_MethodInfo;
extern MethodInfo IEnumerator_MoveNext_m4590_MethodInfo;
extern MethodInfo IEnumerable_1_GetEnumerator_m5532_MethodInfo;
extern MethodInfo IEnumerator_1_get_Current_m5533_MethodInfo;
extern MethodInfo ITextRecoEventHandler_OnWordDetected_m5267_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_get_HasStarted_m4270_MethodInfo;
extern MethodInfo TrackerManager_InitTracker_TisTextTracker_t722_m5534_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_SetupWordList_m4349_MethodInfo;
extern MethodInfo TrackerManager_GetStateManager_m4767_MethodInfo;
extern MethodInfo StateManager_GetWordManager_m5089_MethodInfo;
extern MethodInfo WordManagerImpl_InitializeWordBehaviourTemplates_m3270_MethodInfo;
extern MethodInfo WordManager_GetNewWords_m5111_MethodInfo;
extern MethodInfo WordManager_GetLostWords_m5112_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4350_MethodInfo;
extern MethodInfo List_1__ctor_m5535_MethodInfo;
extern MethodInfo MonoBehaviour__ctor_m254_MethodInfo;
struct TrackerManager_t787;
// Declaration T Vuforia.TrackerManager::GetTracker<Vuforia.TextTracker>()
// T Vuforia.TrackerManager::GetTracker<Vuforia.TextTracker>()
struct TrackerManager_t787;
// Declaration T Vuforia.TrackerManager::InitTracker<Vuforia.TextTracker>()
// T Vuforia.TrackerManager::InitTracker<Vuforia.TextTracker>()


// System.Boolean Vuforia.TextRecoAbstractBehaviour::get_IsInitialized()
extern MethodInfo TextRecoAbstractBehaviour_get_IsInitialized_m4339_MethodInfo;
 bool TextRecoAbstractBehaviour_get_IsInitialized_m4339 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mHasInitialized_2);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Awake()
extern MethodInfo TextRecoAbstractBehaviour_Awake_m4340_MethodInfo;
 void TextRecoAbstractBehaviour_Awake_m4340 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	QCARAbstractBehaviour_t45 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m361(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsQCAREnabled_m361_MethodInfo);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&QCARAbstractBehaviour_t45_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Object_t120 * L_2 = Object_FindObjectOfType_m256(NULL /*static, unused*/, L_1, /*hidden argument*/&Object_FindObjectOfType_m256_MethodInfo);
		V_0 = ((QCARAbstractBehaviour_t45 *)Castclass(L_2, InitializedTypeInfo(&QCARAbstractBehaviour_t45_il2cpp_TypeInfo)));
		bool L_3 = Object_op_Implicit_m257(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_3)
		{
			goto IL_006d;
		}
	}
	{
		IntPtr_t121 L_4 = { &TextRecoAbstractBehaviour_OnQCARInitialized_m4351_MethodInfo };
		Action_t147 * L_5 = (Action_t147 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t147_il2cpp_TypeInfo));
		Action__ctor_m4584(L_5, __this, L_4, /*hidden argument*/&Action__ctor_m4584_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterQCARInitializedCallback_m4279(V_0, L_5, /*hidden argument*/&QCARAbstractBehaviour_RegisterQCARInitializedCallback_m4279_MethodInfo);
		IntPtr_t121 L_6 = { &TextRecoAbstractBehaviour_OnQCARStarted_m4352_MethodInfo };
		Action_t147 * L_7 = (Action_t147 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t147_il2cpp_TypeInfo));
		Action__ctor_m4584(L_7, __this, L_6, /*hidden argument*/&Action__ctor_m4584_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterQCARStartedCallback_m4281(V_0, L_7, /*hidden argument*/&QCARAbstractBehaviour_RegisterQCARStartedCallback_m4281_MethodInfo);
		IntPtr_t121 L_8 = { &TextRecoAbstractBehaviour_OnTrackablesUpdated_m4353_MethodInfo };
		Action_t147 * L_9 = (Action_t147 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t147_il2cpp_TypeInfo));
		Action__ctor_m4584(L_9, __this, L_8, /*hidden argument*/&Action__ctor_m4584_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterTrackablesUpdatedCallback_m4283(V_0, L_9, /*hidden argument*/&QCARAbstractBehaviour_RegisterTrackablesUpdatedCallback_m4283_MethodInfo);
		IntPtr_t121 L_10 = { &TextRecoAbstractBehaviour_OnPause_m4354_MethodInfo };
		Action_1_t802 * L_11 = (Action_1_t802 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_1_t802_il2cpp_TypeInfo));
		Action_1__ctor_m4672(L_11, __this, L_10, /*hidden argument*/&Action_1__ctor_m4672_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterOnPauseCallback_m4285(V_0, L_11, /*hidden argument*/&QCARAbstractBehaviour_RegisterOnPauseCallback_m4285_MethodInfo);
	}

IL_006d:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Start()
extern MethodInfo TextRecoAbstractBehaviour_Start_m4341_MethodInfo;
 void TextRecoAbstractBehaviour_Start_m4341 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	{
		KeepAliveAbstractBehaviour_t38 * L_0 = KeepAliveAbstractBehaviour_get_Instance_m4106(NULL /*static, unused*/, /*hidden argument*/&KeepAliveAbstractBehaviour_get_Instance_m4106_MethodInfo);
		bool L_1 = Object_op_Inequality_m489(NULL /*static, unused*/, L_0, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		KeepAliveAbstractBehaviour_t38 * L_2 = KeepAliveAbstractBehaviour_get_Instance_m4106(NULL /*static, unused*/, /*hidden argument*/&KeepAliveAbstractBehaviour_get_Instance_m4106_MethodInfo);
		NullCheck(L_2);
		bool L_3 = KeepAliveAbstractBehaviour_get_KeepTextRecoBehaviourAlive_m4098(L_2, /*hidden argument*/&KeepAliveAbstractBehaviour_get_KeepTextRecoBehaviourAlive_m4098_MethodInfo);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		GameObject_t29 * L_4 = Component_get_gameObject_m419(__this, /*hidden argument*/&Component_get_gameObject_m419_MethodInfo);
		Object_DontDestroyOnLoad_m4535(NULL /*static, unused*/, L_4, /*hidden argument*/&Object_DontDestroyOnLoad_m4535_MethodInfo);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::OnEnable()
extern MethodInfo TextRecoAbstractBehaviour_OnEnable_m4342_MethodInfo;
 void TextRecoAbstractBehaviour_OnEnable_m4342 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mTrackerWasActiveBeforeDisabling_4);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		TextRecoAbstractBehaviour_StartTextTracker_m4347(__this, /*hidden argument*/&TextRecoAbstractBehaviour_StartTextTracker_m4347_MethodInfo);
	}

IL_000e:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::OnDisable()
extern MethodInfo TextRecoAbstractBehaviour_OnDisable_m4343_MethodInfo;
 void TextRecoAbstractBehaviour_OnDisable_m4343 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	TextTracker_t722 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t787_il2cpp_TypeInfo));
		TrackerManager_t787 * L_0 = TrackerManager_get_Instance_m4207(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4207_MethodInfo);
		NullCheck(L_0);
		TextTracker_t722 * L_1 = (TextTracker_t722 *)GenericVirtFuncInvoker0< TextTracker_t722 * >::Invoke(&TrackerManager_GetTracker_TisTextTracker_t722_m5508_MethodInfo, L_0);
		V_0 = L_1;
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		NullCheck(V_0);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&Tracker_get_IsActive_m3087_MethodInfo, V_0);
		__this->___mTrackerWasActiveBeforeDisabling_4 = L_2;
		NullCheck(V_0);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&Tracker_get_IsActive_m3087_MethodInfo, V_0);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		TextRecoAbstractBehaviour_StopTextTracker_m4348(__this, /*hidden argument*/&TextRecoAbstractBehaviour_StopTextTracker_m4348_MethodInfo);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::OnDestroy()
extern MethodInfo TextRecoAbstractBehaviour_OnDestroy_m4344_MethodInfo;
 void TextRecoAbstractBehaviour_OnDestroy_m4344 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	QCARAbstractBehaviour_t45 * V_0 = {0};
	TextTracker_t722 * V_1 = {0};
	WordList_t723 * V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&QCARAbstractBehaviour_t45_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Object_t120 * L_1 = Object_FindObjectOfType_m256(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_FindObjectOfType_m256_MethodInfo);
		V_0 = ((QCARAbstractBehaviour_t45 *)Castclass(L_1, InitializedTypeInfo(&QCARAbstractBehaviour_t45_il2cpp_TypeInfo)));
		bool L_2 = Object_op_Implicit_m257(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_2)
		{
			goto IL_0065;
		}
	}
	{
		IntPtr_t121 L_3 = { &TextRecoAbstractBehaviour_OnQCARInitialized_m4351_MethodInfo };
		Action_t147 * L_4 = (Action_t147 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t147_il2cpp_TypeInfo));
		Action__ctor_m4584(L_4, __this, L_3, /*hidden argument*/&Action__ctor_m4584_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_UnregisterQCARInitializedCallback_m4280(V_0, L_4, /*hidden argument*/&QCARAbstractBehaviour_UnregisterQCARInitializedCallback_m4280_MethodInfo);
		IntPtr_t121 L_5 = { &TextRecoAbstractBehaviour_OnQCARStarted_m4352_MethodInfo };
		Action_t147 * L_6 = (Action_t147 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t147_il2cpp_TypeInfo));
		Action__ctor_m4584(L_6, __this, L_5, /*hidden argument*/&Action__ctor_m4584_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4282(V_0, L_6, /*hidden argument*/&QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4282_MethodInfo);
		IntPtr_t121 L_7 = { &TextRecoAbstractBehaviour_OnTrackablesUpdated_m4353_MethodInfo };
		Action_t147 * L_8 = (Action_t147 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t147_il2cpp_TypeInfo));
		Action__ctor_m4584(L_8, __this, L_7, /*hidden argument*/&Action__ctor_m4584_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_UnregisterTrackablesUpdatedCallback_m4284(V_0, L_8, /*hidden argument*/&QCARAbstractBehaviour_UnregisterTrackablesUpdatedCallback_m4284_MethodInfo);
		IntPtr_t121 L_9 = { &TextRecoAbstractBehaviour_OnPause_m4354_MethodInfo };
		Action_1_t802 * L_10 = (Action_1_t802 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_1_t802_il2cpp_TypeInfo));
		Action_1__ctor_m4672(L_10, __this, L_9, /*hidden argument*/&Action_1__ctor_m4672_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_UnregisterOnPauseCallback_m4286(V_0, L_10, /*hidden argument*/&QCARAbstractBehaviour_UnregisterOnPauseCallback_m4286_MethodInfo);
	}

IL_0065:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t787_il2cpp_TypeInfo));
		TrackerManager_t787 * L_11 = TrackerManager_get_Instance_m4207(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4207_MethodInfo);
		NullCheck(L_11);
		TextTracker_t722 * L_12 = (TextTracker_t722 *)GenericVirtFuncInvoker0< TextTracker_t722 * >::Invoke(&TrackerManager_GetTracker_TisTextTracker_t722_m5508_MethodInfo, L_11);
		V_1 = L_12;
		if (!V_1)
		{
			goto IL_0081;
		}
	}
	{
		NullCheck(V_1);
		WordList_t723 * L_13 = (WordList_t723 *)VirtFuncInvoker0< WordList_t723 * >::Invoke(&TextTracker_get_WordList_m5084_MethodInfo, V_1);
		V_2 = L_13;
		NullCheck(V_2);
		VirtFuncInvoker0< bool >::Invoke(&WordList_UnloadAllLists_m5206_MethodInfo, V_2);
	}

IL_0081:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::RegisterTextRecoEventHandler(Vuforia.ITextRecoEventHandler)
extern MethodInfo TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4345_MethodInfo;
 void TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4345 (TextRecoAbstractBehaviour_t35 * __this, Object_t * ___trackableEventHandler, MethodInfo* method){
	{
		List_1_t808 * L_0 = (__this->___mTextRecoEventHandlers_13);
		NullCheck(L_0);
		VirtActionInvoker1< Object_t * >::Invoke(&List_1_Add_m5525_MethodInfo, L_0, ___trackableEventHandler);
		bool L_1 = (__this->___mHasInitialized_2);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		NullCheck(___trackableEventHandler);
		InterfaceActionInvoker0::Invoke(&ITextRecoEventHandler_OnInitialized_m5266_MethodInfo, ___trackableEventHandler);
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean Vuforia.TextRecoAbstractBehaviour::UnregisterTextRecoEventHandler(Vuforia.ITextRecoEventHandler)
extern MethodInfo TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4346_MethodInfo;
 bool TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4346 (TextRecoAbstractBehaviour_t35 * __this, Object_t * ___trackableEventHandler, MethodInfo* method){
	{
		List_1_t808 * L_0 = (__this->___mTextRecoEventHandlers_13);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&List_1_Remove_m5526_MethodInfo, L_0, ___trackableEventHandler);
		return L_1;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::StartTextTracker()
 void TextRecoAbstractBehaviour_StartTextTracker_m4347 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	TextTracker_t722 * V_0 = {0};
	{
		Debug_Log_m288(NULL /*static, unused*/, (String_t*) &_stringLiteral315, /*hidden argument*/&Debug_Log_m288_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t787_il2cpp_TypeInfo));
		TrackerManager_t787 * L_0 = TrackerManager_get_Instance_m4207(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4207_MethodInfo);
		NullCheck(L_0);
		TextTracker_t722 * L_1 = (TextTracker_t722 *)GenericVirtFuncInvoker0< TextTracker_t722 * >::Invoke(&TrackerManager_GetTracker_TisTextTracker_t722_m5508_MethodInfo, L_0);
		V_0 = L_1;
		if (!V_0)
		{
			goto IL_001f;
		}
	}
	{
		NullCheck(V_0);
		VirtFuncInvoker0< bool >::Invoke(&Tracker_Start_m4674_MethodInfo, V_0);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::StopTextTracker()
 void TextRecoAbstractBehaviour_StopTextTracker_m4348 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	TextTracker_t722 * V_0 = {0};
	{
		Debug_Log_m288(NULL /*static, unused*/, (String_t*) &_stringLiteral316, /*hidden argument*/&Debug_Log_m288_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t787_il2cpp_TypeInfo));
		TrackerManager_t787 * L_0 = TrackerManager_get_Instance_m4207(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4207_MethodInfo);
		NullCheck(L_0);
		TextTracker_t722 * L_1 = (TextTracker_t722 *)GenericVirtFuncInvoker0< TextTracker_t722 * >::Invoke(&TrackerManager_GetTracker_TisTextTracker_t722_m5508_MethodInfo, L_0);
		V_0 = L_1;
		if (!V_0)
		{
			goto IL_001e;
		}
	}
	{
		NullCheck(V_0);
		VirtActionInvoker0::Invoke(&Tracker_Stop_m4675_MethodInfo, V_0);
	}

IL_001e:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::SetupWordList()
 void TextRecoAbstractBehaviour_SetupWordList_m4349 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	TextTracker_t722 * V_0 = {0};
	WordList_t723 * V_1 = {0};
	StringU5BU5D_t862* V_2 = {0};
	String_t* V_3 = {0};
	StringU5BU5D_t862* V_4 = {0};
	String_t* V_5 = {0};
	CharU5BU5D_t378* V_6 = {0};
	StringU5BU5D_t862* V_7 = {0};
	int32_t V_8 = 0;
	CharU5BU5D_t378* V_9 = {0};
	StringU5BU5D_t862* V_10 = {0};
	int32_t V_11 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t787_il2cpp_TypeInfo));
		TrackerManager_t787 * L_0 = TrackerManager_get_Instance_m4207(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4207_MethodInfo);
		NullCheck(L_0);
		TextTracker_t722 * L_1 = (TextTracker_t722 *)GenericVirtFuncInvoker0< TextTracker_t722 * >::Invoke(&TrackerManager_GetTracker_TisTextTracker_t722_m5508_MethodInfo, L_0);
		V_0 = L_1;
		if (!V_0)
		{
			goto IL_0125;
		}
	}
	{
		NullCheck(V_0);
		WordList_t723 * L_2 = (WordList_t723 *)VirtFuncInvoker0< WordList_t723 * >::Invoke(&TextTracker_get_WordList_m5084_MethodInfo, V_0);
		V_1 = L_2;
		String_t* L_3 = (__this->___mWordListFile_5);
		NullCheck(V_1);
		VirtFuncInvoker1< bool, String_t* >::Invoke(&WordList_LoadWordListFile_m5197_MethodInfo, V_1, L_3);
		String_t* L_4 = (__this->___mCustomWordListFile_6);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_5 = String_op_Inequality_m658(NULL /*static, unused*/, L_4, (String_t*) &_stringLiteral141, /*hidden argument*/&String_op_Inequality_m658_MethodInfo);
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		String_t* L_6 = (__this->___mCustomWordListFile_6);
		NullCheck(V_1);
		VirtFuncInvoker1< int32_t, String_t* >::Invoke(&WordList_AddWordsFromFile_m5200_MethodInfo, V_1, L_6);
	}

IL_0044:
	{
		String_t* L_7 = (__this->___mAdditionalCustomWords_7);
		if (!L_7)
		{
			goto IL_009b;
		}
	}
	{
		String_t* L_8 = (__this->___mAdditionalCustomWords_7);
		V_6 = ((CharU5BU5D_t378*)SZArrayNew(InitializedTypeInfo(&CharU5BU5D_t378_il2cpp_TypeInfo), 2));
		NullCheck(V_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_6, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(V_6, 0)) = (uint16_t)((int32_t)13);
		NullCheck(V_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_6, 1);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(V_6, 1)) = (uint16_t)((int32_t)10);
		NullCheck(L_8);
		StringU5BU5D_t862* L_9 = String_Split_m5524(L_8, V_6, /*hidden argument*/&String_Split_m5524_MethodInfo);
		V_2 = L_9;
		V_7 = V_2;
		V_8 = 0;
		goto IL_0093;
	}

IL_0076:
	{
		NullCheck(V_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_7, V_8);
		int32_t L_10 = V_8;
		V_3 = (*(String_t**)(String_t**)SZArrayLdElema(V_7, L_10));
		NullCheck(V_3);
		int32_t L_11 = String_get_Length_m2431(V_3, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		NullCheck(V_1);
		VirtFuncInvoker1< bool, String_t* >::Invoke(&WordList_AddWord_m5203_MethodInfo, V_1, V_3);
	}

IL_008d:
	{
		V_8 = ((int32_t)(V_8+1));
	}

IL_0093:
	{
		NullCheck(V_7);
		if ((((int32_t)V_8) < ((int32_t)(((int32_t)(((Array_t *)V_7)->max_length))))))
		{
			goto IL_0076;
		}
	}

IL_009b:
	{
		int32_t L_12 = (__this->___mFilterMode_8);
		NullCheck(V_1);
		VirtFuncInvoker1< bool, int32_t >::Invoke(&WordList_SetFilterMode_m5208_MethodInfo, V_1, L_12);
		int32_t L_13 = (__this->___mFilterMode_8);
		if (!L_13)
		{
			goto IL_0125;
		}
	}
	{
		String_t* L_14 = (__this->___mFilterListFile_9);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_15 = String_op_Inequality_m658(NULL /*static, unused*/, L_14, (String_t*) &_stringLiteral141, /*hidden argument*/&String_op_Inequality_m658_MethodInfo);
		if (!L_15)
		{
			goto IL_00cf;
		}
	}
	{
		String_t* L_16 = (__this->___mFilterListFile_9);
		NullCheck(V_1);
		VirtFuncInvoker1< bool, String_t* >::Invoke(&WordList_LoadFilterListFile_m5209_MethodInfo, V_1, L_16);
	}

IL_00cf:
	{
		String_t* L_17 = (__this->___mAdditionalFilterWords_10);
		if (!L_17)
		{
			goto IL_0125;
		}
	}
	{
		String_t* L_18 = (__this->___mAdditionalFilterWords_10);
		V_9 = ((CharU5BU5D_t378*)SZArrayNew(InitializedTypeInfo(&CharU5BU5D_t378_il2cpp_TypeInfo), 1));
		NullCheck(V_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_9, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(V_9, 0)) = (uint16_t)((int32_t)10);
		NullCheck(L_18);
		StringU5BU5D_t862* L_19 = String_Split_m5524(L_18, V_9, /*hidden argument*/&String_Split_m5524_MethodInfo);
		V_4 = L_19;
		V_10 = V_4;
		V_11 = 0;
		goto IL_011d;
	}

IL_00fd:
	{
		NullCheck(V_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_10, V_11);
		int32_t L_20 = V_11;
		V_5 = (*(String_t**)(String_t**)SZArrayLdElema(V_10, L_20));
		NullCheck(V_5);
		int32_t L_21 = String_get_Length_m2431(V_5, /*hidden argument*/&String_get_Length_m2431_MethodInfo);
		if ((((int32_t)L_21) <= ((int32_t)0)))
		{
			goto IL_0117;
		}
	}
	{
		NullCheck(V_1);
		VirtFuncInvoker1< bool, String_t* >::Invoke(&WordList_AddWordToFilterList_m5212_MethodInfo, V_1, V_5);
	}

IL_0117:
	{
		V_11 = ((int32_t)(V_11+1));
	}

IL_011d:
	{
		NullCheck(V_10);
		if ((((int32_t)V_11) < ((int32_t)(((int32_t)(((Array_t *)V_10)->max_length))))))
		{
			goto IL_00fd;
		}
	}

IL_0125:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::NotifyEventHandlersOfChanges(System.Collections.Generic.IEnumerable`1<Vuforia.Word>,System.Collections.Generic.IEnumerable`1<Vuforia.WordResult>)
 void TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4350 (TextRecoAbstractBehaviour_t35 * __this, Object_t* ___lostWords, Object_t* ___newWords, MethodInfo* method){
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	WordResult_t747 * V_2 = {0};
	Object_t * V_3 = {0};
	Object_t* V_4 = {0};
	Enumerator_t943  V_5 = {0};
	Object_t* V_6 = {0};
	Enumerator_t943  V_7 = {0};
	int32_t leaveInstructions[2] = {0};
	Exception_t151 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t151 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		NullCheck(___lostWords);
		Object_t* L_0 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(&IEnumerable_1_GetEnumerator_m5527_MethodInfo, ___lostWords);
		V_4 = L_0;
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0049;
		}

IL_000a:
		{
			NullCheck(V_4);
			Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(&IEnumerator_1_get_Current_m5528_MethodInfo, V_4);
			V_0 = L_1;
			List_1_t808 * L_2 = (__this->___mTextRecoEventHandlers_13);
			NullCheck(L_2);
			Enumerator_t943  L_3 = List_1_GetEnumerator_m5529(L_2, /*hidden argument*/&List_1_GetEnumerator_m5529_MethodInfo);
			V_5 = L_3;
		}

IL_001f:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0030;
			}

IL_0021:
			{
				Object_t * L_4 = Enumerator_get_Current_m5530((&V_5), /*hidden argument*/&Enumerator_get_Current_m5530_MethodInfo);
				V_1 = L_4;
				NullCheck(V_1);
				InterfaceActionInvoker1< Object_t * >::Invoke(&ITextRecoEventHandler_OnWordLost_m5268_MethodInfo, V_1, V_0);
			}

IL_0030:
			{
				bool L_5 = Enumerator_MoveNext_m5531((&V_5), /*hidden argument*/&Enumerator_MoveNext_m5531_MethodInfo);
				if (L_5)
				{
					goto IL_0021;
				}
			}

IL_0039:
			{
				// IL_0039: leave.s IL_0049
				leaveInstructions[1] = 0x49; // 2
				THROW_SENTINEL(IL_0049);
				// finally target depth: 2
			}
		} // end try (depth: 2)
		catch(Il2CppFinallySentinel& e)
		{
			goto IL_003b;
		}
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t151 *)e.ex;
			goto IL_003b;
		}

IL_003b:
		{ // begin finally (depth: 2)
			NullCheck(Box(InitializedTypeInfo(&Enumerator_t943_il2cpp_TypeInfo), &(*(&V_5))));
			InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m333_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t943_il2cpp_TypeInfo), &(*(&V_5))));
			// finally node depth: 2
			switch (leaveInstructions[1])
			{
				case 0x49:
					goto IL_0049;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 2, __last_unhandled_exception has not been set");
					#endif
					Exception_t151 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		} // end finally (depth: 2)

IL_0049:
		{
			NullCheck(V_4);
			bool L_6 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&IEnumerator_MoveNext_m4590_MethodInfo, V_4);
			if (L_6)
			{
				goto IL_000a;
			}
		}

IL_0052:
		{
			// IL_0052: leave.s IL_0060
			leaveInstructions[0] = 0x60; // 1
			THROW_SENTINEL(IL_0060);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0054;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t151 *)e.ex;
		goto IL_0054;
	}

IL_0054:
	{ // begin finally (depth: 1)
		{
			if (!V_4)
			{
				goto IL_005f;
			}
		}

IL_0058:
		{
			NullCheck(V_4);
			InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m333_MethodInfo, V_4);
		}

IL_005f:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x60:
					goto IL_0060;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					Exception_t151 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0060:
	{
		NullCheck(___newWords);
		Object_t* L_7 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(&IEnumerable_1_GetEnumerator_m5532_MethodInfo, ___newWords);
		V_6 = L_7;
	}

IL_0068:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a9;
		}

IL_006a:
		{
			NullCheck(V_6);
			WordResult_t747 * L_8 = (WordResult_t747 *)InterfaceFuncInvoker0< WordResult_t747 * >::Invoke(&IEnumerator_1_get_Current_m5533_MethodInfo, V_6);
			V_2 = L_8;
			List_1_t808 * L_9 = (__this->___mTextRecoEventHandlers_13);
			NullCheck(L_9);
			Enumerator_t943  L_10 = List_1_GetEnumerator_m5529(L_9, /*hidden argument*/&List_1_GetEnumerator_m5529_MethodInfo);
			V_7 = L_10;
		}

IL_007f:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0090;
			}

IL_0081:
			{
				Object_t * L_11 = Enumerator_get_Current_m5530((&V_7), /*hidden argument*/&Enumerator_get_Current_m5530_MethodInfo);
				V_3 = L_11;
				NullCheck(V_3);
				InterfaceActionInvoker1< WordResult_t747 * >::Invoke(&ITextRecoEventHandler_OnWordDetected_m5267_MethodInfo, V_3, V_2);
			}

IL_0090:
			{
				bool L_12 = Enumerator_MoveNext_m5531((&V_7), /*hidden argument*/&Enumerator_MoveNext_m5531_MethodInfo);
				if (L_12)
				{
					goto IL_0081;
				}
			}

IL_0099:
			{
				// IL_0099: leave.s IL_00a9
				leaveInstructions[1] = 0xA9; // 2
				THROW_SENTINEL(IL_00a9);
				// finally target depth: 2
			}
		} // end try (depth: 2)
		catch(Il2CppFinallySentinel& e)
		{
			goto IL_009b;
		}
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t151 *)e.ex;
			goto IL_009b;
		}

IL_009b:
		{ // begin finally (depth: 2)
			NullCheck(Box(InitializedTypeInfo(&Enumerator_t943_il2cpp_TypeInfo), &(*(&V_7))));
			InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m333_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t943_il2cpp_TypeInfo), &(*(&V_7))));
			// finally node depth: 2
			switch (leaveInstructions[1])
			{
				case 0xA9:
					goto IL_00a9;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 2, __last_unhandled_exception has not been set");
					#endif
					Exception_t151 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		} // end finally (depth: 2)

IL_00a9:
		{
			NullCheck(V_6);
			bool L_13 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&IEnumerator_MoveNext_m4590_MethodInfo, V_6);
			if (L_13)
			{
				goto IL_006a;
			}
		}

IL_00b2:
		{
			// IL_00b2: leave.s IL_00c0
			leaveInstructions[0] = 0xC0; // 1
			THROW_SENTINEL(IL_00c0);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_00b4;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t151 *)e.ex;
		goto IL_00b4;
	}

IL_00b4:
	{ // begin finally (depth: 1)
		{
			if (!V_6)
			{
				goto IL_00bf;
			}
		}

IL_00b8:
		{
			NullCheck(V_6);
			InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m333_MethodInfo, V_6);
		}

IL_00bf:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0xC0:
					goto IL_00c0;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					Exception_t151 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_00c0:
	{
		return;
	}
}
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_WordListFile()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m467_MethodInfo;
 String_t* TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m467 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mWordListFile_5);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_WordListFile(System.String)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m468_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m468 (TextRecoAbstractBehaviour_t35 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___mWordListFile_5 = ___value;
		return;
	}
}
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_CustomWordListFile()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m469_MethodInfo;
 String_t* TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m469 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mCustomWordListFile_6);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_CustomWordListFile(System.String)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m470_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m470 (TextRecoAbstractBehaviour_t35 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___mCustomWordListFile_6 = ___value;
		return;
	}
}
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_AdditionalCustomWords()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m471_MethodInfo;
 String_t* TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m471 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mAdditionalCustomWords_7);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_AdditionalCustomWords(System.String)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m472_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m472 (TextRecoAbstractBehaviour_t35 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___mAdditionalCustomWords_7 = ___value;
		return;
	}
}
// Vuforia.WordFilterMode Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_FilterMode()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m473_MethodInfo;
 int32_t TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m473 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___mFilterMode_8);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_FilterMode(Vuforia.WordFilterMode)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m474_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m474 (TextRecoAbstractBehaviour_t35 * __this, int32_t ___value, MethodInfo* method){
	{
		__this->___mFilterMode_8 = ___value;
		return;
	}
}
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_FilterListFile()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m475_MethodInfo;
 String_t* TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m475 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mFilterListFile_9);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_FilterListFile(System.String)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m476_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m476 (TextRecoAbstractBehaviour_t35 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___mFilterListFile_9 = ___value;
		return;
	}
}
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_AdditionalFilterWords()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m477_MethodInfo;
 String_t* TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m477 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mAdditionalFilterWords_10);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_AdditionalFilterWords(System.String)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m478_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m478 (TextRecoAbstractBehaviour_t35 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___mAdditionalFilterWords_10 = ___value;
		return;
	}
}
// Vuforia.WordPrefabCreationMode Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_WordPrefabCreationMode()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m479_MethodInfo;
 int32_t TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m479 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___mWordPrefabCreationMode_11);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_WordPrefabCreationMode(Vuforia.WordPrefabCreationMode)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m480_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m480 (TextRecoAbstractBehaviour_t35 * __this, int32_t ___value, MethodInfo* method){
	{
		__this->___mWordPrefabCreationMode_11 = ___value;
		return;
	}
}
// System.Int32 Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_MaximumWordInstances()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m481_MethodInfo;
 int32_t TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m481 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___mMaximumWordInstances_12);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_MaximumWordInstances(System.Int32)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m482_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m482 (TextRecoAbstractBehaviour_t35 * __this, int32_t ___value, MethodInfo* method){
	{
		__this->___mMaximumWordInstances_12 = ___value;
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::OnQCARInitialized()
 void TextRecoAbstractBehaviour_OnQCARInitialized_m4351 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	bool V_0 = false;
	QCARAbstractBehaviour_t45 * V_1 = {0};
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&QCARAbstractBehaviour_t45_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Object_t120 * L_1 = Object_FindObjectOfType_m256(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_FindObjectOfType_m256_MethodInfo);
		V_1 = ((QCARAbstractBehaviour_t45 *)Castclass(L_1, InitializedTypeInfo(&QCARAbstractBehaviour_t45_il2cpp_TypeInfo)));
		bool L_2 = Object_op_Implicit_m257(NULL /*static, unused*/, V_1, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		NullCheck(V_1);
		bool L_3 = QCARAbstractBehaviour_get_HasStarted_m4270(V_1, /*hidden argument*/&QCARAbstractBehaviour_get_HasStarted_m4270_MethodInfo);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		NullCheck(V_1);
		Behaviour_set_enabled_m547(V_1, 0, /*hidden argument*/&Behaviour_set_enabled_m547_MethodInfo);
		V_0 = 1;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t787_il2cpp_TypeInfo));
		TrackerManager_t787 * L_4 = TrackerManager_get_Instance_m4207(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4207_MethodInfo);
		NullCheck(L_4);
		TextTracker_t722 * L_5 = (TextTracker_t722 *)GenericVirtFuncInvoker0< TextTracker_t722 * >::Invoke(&TrackerManager_GetTracker_TisTextTracker_t722_m5508_MethodInfo, L_4);
		if (L_5)
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t787_il2cpp_TypeInfo));
		TrackerManager_t787 * L_6 = TrackerManager_get_Instance_m4207(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4207_MethodInfo);
		NullCheck(L_6);
		GenericVirtFuncInvoker0< TextTracker_t722 * >::Invoke(&TrackerManager_InitTracker_TisTextTracker_t722_m5534_MethodInfo, L_6);
	}

IL_0047:
	{
		if (!V_0)
		{
			goto IL_0051;
		}
	}
	{
		NullCheck(V_1);
		Behaviour_set_enabled_m547(V_1, 1, /*hidden argument*/&Behaviour_set_enabled_m547_MethodInfo);
	}

IL_0051:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::OnQCARStarted()
 void TextRecoAbstractBehaviour_OnQCARStarted_m4352 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	WordManager_t733 * V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t943  V_2 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t151 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t151 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		TextRecoAbstractBehaviour_SetupWordList_m4349(__this, /*hidden argument*/&TextRecoAbstractBehaviour_SetupWordList_m4349_MethodInfo);
		TextRecoAbstractBehaviour_StartTextTracker_m4347(__this, /*hidden argument*/&TextRecoAbstractBehaviour_StartTextTracker_m4347_MethodInfo);
		__this->___mHasInitialized_2 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t787_il2cpp_TypeInfo));
		TrackerManager_t787 * L_0 = TrackerManager_get_Instance_m4207(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4207_MethodInfo);
		NullCheck(L_0);
		StateManager_t768 * L_1 = (StateManager_t768 *)VirtFuncInvoker0< StateManager_t768 * >::Invoke(&TrackerManager_GetStateManager_m4767_MethodInfo, L_0);
		NullCheck(L_1);
		WordManager_t733 * L_2 = (WordManager_t733 *)VirtFuncInvoker0< WordManager_t733 * >::Invoke(&StateManager_GetWordManager_m5089_MethodInfo, L_1);
		V_0 = L_2;
		int32_t L_3 = (__this->___mWordPrefabCreationMode_11);
		int32_t L_4 = (__this->___mMaximumWordInstances_12);
		NullCheck(((WordManagerImpl_t744 *)Castclass(V_0, InitializedTypeInfo(&WordManagerImpl_t744_il2cpp_TypeInfo))));
		WordManagerImpl_InitializeWordBehaviourTemplates_m3270(((WordManagerImpl_t744 *)Castclass(V_0, InitializedTypeInfo(&WordManagerImpl_t744_il2cpp_TypeInfo))), L_3, L_4, /*hidden argument*/&WordManagerImpl_InitializeWordBehaviourTemplates_m3270_MethodInfo);
		List_1_t808 * L_5 = (__this->___mTextRecoEventHandlers_13);
		NullCheck(L_5);
		Enumerator_t943  L_6 = List_1_GetEnumerator_m5529(L_5, /*hidden argument*/&List_1_GetEnumerator_m5529_MethodInfo);
		V_2 = L_6;
	}

IL_0046:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0056;
		}

IL_0048:
		{
			Object_t * L_7 = Enumerator_get_Current_m5530((&V_2), /*hidden argument*/&Enumerator_get_Current_m5530_MethodInfo);
			V_1 = L_7;
			NullCheck(V_1);
			InterfaceActionInvoker0::Invoke(&ITextRecoEventHandler_OnInitialized_m5266_MethodInfo, V_1);
		}

IL_0056:
		{
			bool L_8 = Enumerator_MoveNext_m5531((&V_2), /*hidden argument*/&Enumerator_MoveNext_m5531_MethodInfo);
			if (L_8)
			{
				goto IL_0048;
			}
		}

IL_005f:
		{
			// IL_005f: leave.s IL_006f
			leaveInstructions[0] = 0x6F; // 1
			THROW_SENTINEL(IL_006f);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0061;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t151 *)e.ex;
		goto IL_0061;
	}

IL_0061:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t943_il2cpp_TypeInfo), &(*(&V_2))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m333_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t943_il2cpp_TypeInfo), &(*(&V_2))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x6F:
				goto IL_006f;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t151 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_006f:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::OnTrackablesUpdated()
 void TextRecoAbstractBehaviour_OnTrackablesUpdated_m4353 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	WordManagerImpl_t744 * V_0 = {0};
	Object_t* V_1 = {0};
	Object_t* V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t787_il2cpp_TypeInfo));
		TrackerManager_t787 * L_0 = TrackerManager_get_Instance_m4207(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4207_MethodInfo);
		NullCheck(L_0);
		StateManager_t768 * L_1 = (StateManager_t768 *)VirtFuncInvoker0< StateManager_t768 * >::Invoke(&TrackerManager_GetStateManager_m4767_MethodInfo, L_0);
		NullCheck(L_1);
		WordManager_t733 * L_2 = (WordManager_t733 *)VirtFuncInvoker0< WordManager_t733 * >::Invoke(&StateManager_GetWordManager_m5089_MethodInfo, L_1);
		V_0 = ((WordManagerImpl_t744 *)Castclass(L_2, InitializedTypeInfo(&WordManagerImpl_t744_il2cpp_TypeInfo)));
		NullCheck(V_0);
		Object_t* L_3 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(&WordManager_GetNewWords_m5111_MethodInfo, V_0);
		V_1 = L_3;
		NullCheck(V_0);
		Object_t* L_4 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(&WordManager_GetLostWords_m5112_MethodInfo, V_0);
		V_2 = L_4;
		TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4350(__this, V_2, V_1, /*hidden argument*/&TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4350_MethodInfo);
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::OnPause(System.Boolean)
 void TextRecoAbstractBehaviour_OnPause_m4354 (TextRecoAbstractBehaviour_t35 * __this, bool ___pause, MethodInfo* method){
	TextTracker_t722 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t787_il2cpp_TypeInfo));
		TrackerManager_t787 * L_0 = TrackerManager_get_Instance_m4207(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4207_MethodInfo);
		NullCheck(L_0);
		TextTracker_t722 * L_1 = (TextTracker_t722 *)GenericVirtFuncInvoker0< TextTracker_t722 * >::Invoke(&TrackerManager_GetTracker_TisTextTracker_t722_m5508_MethodInfo, L_0);
		V_0 = L_1;
		if (!V_0)
		{
			goto IL_003a;
		}
	}
	{
		if (!___pause)
		{
			goto IL_002c;
		}
	}
	{
		NullCheck(V_0);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&Tracker_get_IsActive_m3087_MethodInfo, V_0);
		__this->___mTrackerWasActiveBeforePause_3 = L_2;
		NullCheck(V_0);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&Tracker_get_IsActive_m3087_MethodInfo, V_0);
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		TextRecoAbstractBehaviour_StopTextTracker_m4348(__this, /*hidden argument*/&TextRecoAbstractBehaviour_StopTextTracker_m4348_MethodInfo);
		return;
	}

IL_002c:
	{
		bool L_4 = (__this->___mTrackerWasActiveBeforePause_3);
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		TextRecoAbstractBehaviour_StartTextTracker_m4347(__this, /*hidden argument*/&TextRecoAbstractBehaviour_StartTextTracker_m4347_MethodInfo);
	}

IL_003a:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::.ctor()
extern MethodInfo TextRecoAbstractBehaviour__ctor_m466_MethodInfo;
 void TextRecoAbstractBehaviour__ctor_m466 (TextRecoAbstractBehaviour_t35 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&List_1_t808_il2cpp_TypeInfo));
		List_1_t808 * L_0 = (List_1_t808 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t808_il2cpp_TypeInfo));
		List_1__ctor_m5535(L_0, /*hidden argument*/&List_1__ctor_m5535_MethodInfo);
		__this->___mTextRecoEventHandlers_13 = L_0;
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.TextRecoAbstractBehaviour
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo TextRecoAbstractBehaviour_t35____mHasInitialized_2_FieldInfo = 
{
	"mHasInitialized"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t35, ___mHasInitialized_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo TextRecoAbstractBehaviour_t35____mTrackerWasActiveBeforePause_3_FieldInfo = 
{
	"mTrackerWasActiveBeforePause"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t35, ___mTrackerWasActiveBeforePause_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo TextRecoAbstractBehaviour_t35____mTrackerWasActiveBeforeDisabling_4_FieldInfo = 
{
	"mTrackerWasActiveBeforeDisabling"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t35, ___mTrackerWasActiveBeforeDisabling_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mWordListFile;
FieldInfo TextRecoAbstractBehaviour_t35____mWordListFile_5_FieldInfo = 
{
	"mWordListFile"/* name */
	, &String_t_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t35, ___mWordListFile_5)/* data */
	, &TextRecoAbstractBehaviour_t35__CustomAttributeCache_mWordListFile/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mCustomWordListFile;
FieldInfo TextRecoAbstractBehaviour_t35____mCustomWordListFile_6_FieldInfo = 
{
	"mCustomWordListFile"/* name */
	, &String_t_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t35, ___mCustomWordListFile_6)/* data */
	, &TextRecoAbstractBehaviour_t35__CustomAttributeCache_mCustomWordListFile/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mAdditionalCustomWords;
FieldInfo TextRecoAbstractBehaviour_t35____mAdditionalCustomWords_7_FieldInfo = 
{
	"mAdditionalCustomWords"/* name */
	, &String_t_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t35, ___mAdditionalCustomWords_7)/* data */
	, &TextRecoAbstractBehaviour_t35__CustomAttributeCache_mAdditionalCustomWords/* custom_attributes_cache */

};
extern Il2CppType WordFilterMode_t816_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mFilterMode;
FieldInfo TextRecoAbstractBehaviour_t35____mFilterMode_8_FieldInfo = 
{
	"mFilterMode"/* name */
	, &WordFilterMode_t816_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t35, ___mFilterMode_8)/* data */
	, &TextRecoAbstractBehaviour_t35__CustomAttributeCache_mFilterMode/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mFilterListFile;
FieldInfo TextRecoAbstractBehaviour_t35____mFilterListFile_9_FieldInfo = 
{
	"mFilterListFile"/* name */
	, &String_t_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t35, ___mFilterListFile_9)/* data */
	, &TextRecoAbstractBehaviour_t35__CustomAttributeCache_mFilterListFile/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mAdditionalFilterWords;
FieldInfo TextRecoAbstractBehaviour_t35____mAdditionalFilterWords_10_FieldInfo = 
{
	"mAdditionalFilterWords"/* name */
	, &String_t_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t35, ___mAdditionalFilterWords_10)/* data */
	, &TextRecoAbstractBehaviour_t35__CustomAttributeCache_mAdditionalFilterWords/* custom_attributes_cache */

};
extern Il2CppType WordPrefabCreationMode_t732_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mWordPrefabCreationMode;
FieldInfo TextRecoAbstractBehaviour_t35____mWordPrefabCreationMode_11_FieldInfo = 
{
	"mWordPrefabCreationMode"/* name */
	, &WordPrefabCreationMode_t732_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t35, ___mWordPrefabCreationMode_11)/* data */
	, &TextRecoAbstractBehaviour_t35__CustomAttributeCache_mWordPrefabCreationMode/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mMaximumWordInstances;
FieldInfo TextRecoAbstractBehaviour_t35____mMaximumWordInstances_12_FieldInfo = 
{
	"mMaximumWordInstances"/* name */
	, &Int32_t123_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t35, ___mMaximumWordInstances_12)/* data */
	, &TextRecoAbstractBehaviour_t35__CustomAttributeCache_mMaximumWordInstances/* custom_attributes_cache */

};
extern Il2CppType List_1_t808_0_0_1;
FieldInfo TextRecoAbstractBehaviour_t35____mTextRecoEventHandlers_13_FieldInfo = 
{
	"mTextRecoEventHandlers"/* name */
	, &List_1_t808_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t35, ___mTextRecoEventHandlers_13)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TextRecoAbstractBehaviour_t35_FieldInfos[] =
{
	&TextRecoAbstractBehaviour_t35____mHasInitialized_2_FieldInfo,
	&TextRecoAbstractBehaviour_t35____mTrackerWasActiveBeforePause_3_FieldInfo,
	&TextRecoAbstractBehaviour_t35____mTrackerWasActiveBeforeDisabling_4_FieldInfo,
	&TextRecoAbstractBehaviour_t35____mWordListFile_5_FieldInfo,
	&TextRecoAbstractBehaviour_t35____mCustomWordListFile_6_FieldInfo,
	&TextRecoAbstractBehaviour_t35____mAdditionalCustomWords_7_FieldInfo,
	&TextRecoAbstractBehaviour_t35____mFilterMode_8_FieldInfo,
	&TextRecoAbstractBehaviour_t35____mFilterListFile_9_FieldInfo,
	&TextRecoAbstractBehaviour_t35____mAdditionalFilterWords_10_FieldInfo,
	&TextRecoAbstractBehaviour_t35____mWordPrefabCreationMode_11_FieldInfo,
	&TextRecoAbstractBehaviour_t35____mMaximumWordInstances_12_FieldInfo,
	&TextRecoAbstractBehaviour_t35____mTextRecoEventHandlers_13_FieldInfo,
	NULL
};
static PropertyInfo TextRecoAbstractBehaviour_t35____IsInitialized_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, "IsInitialized"/* name */
	, &TextRecoAbstractBehaviour_get_IsInitialized_m4339_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_WordListFile_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.WordListFile"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m467_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m468_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_CustomWordListFile_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.CustomWordListFile"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m469_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m470_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_AdditionalCustomWords_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.AdditionalCustomWords"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m471_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m472_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_FilterMode_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.FilterMode"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m473_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m474_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_FilterListFile_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.FilterListFile"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m475_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m476_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_AdditionalFilterWords_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.AdditionalFilterWords"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m477_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m478_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_WordPrefabCreationMode_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.WordPrefabCreationMode"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m479_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m480_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_MaximumWordInstances_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.MaximumWordInstances"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m481_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m482_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* TextRecoAbstractBehaviour_t35_PropertyInfos[] =
{
	&TextRecoAbstractBehaviour_t35____IsInitialized_PropertyInfo,
	&TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_WordListFile_PropertyInfo,
	&TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_CustomWordListFile_PropertyInfo,
	&TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_AdditionalCustomWords_PropertyInfo,
	&TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_FilterMode_PropertyInfo,
	&TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_FilterListFile_PropertyInfo,
	&TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_AdditionalFilterWords_PropertyInfo,
	&TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_WordPrefabCreationMode_PropertyInfo,
	&TextRecoAbstractBehaviour_t35____Vuforia_IEditorTextRecoBehaviour_MaximumWordInstances_PropertyInfo,
	NULL
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.TextRecoAbstractBehaviour::get_IsInitialized()
MethodInfo TextRecoAbstractBehaviour_get_IsInitialized_m4339_MethodInfo = 
{
	"get_IsInitialized"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_get_IsInitialized_m4339/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2365/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Awake()
MethodInfo TextRecoAbstractBehaviour_Awake_m4340_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Awake_m4340/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2366/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Start()
MethodInfo TextRecoAbstractBehaviour_Start_m4341_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Start_m4341/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2367/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnEnable()
MethodInfo TextRecoAbstractBehaviour_OnEnable_m4342_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnEnable_m4342/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2368/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnDisable()
MethodInfo TextRecoAbstractBehaviour_OnDisable_m4343_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnDisable_m4343/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2369/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnDestroy()
MethodInfo TextRecoAbstractBehaviour_OnDestroy_m4344_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnDestroy_m4344/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2370/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ITextRecoEventHandler_t809_0_0_0;
extern Il2CppType ITextRecoEventHandler_t809_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4345_ParameterInfos[] = 
{
	{"trackableEventHandler", 0, 134220012, &EmptyCustomAttributesCache, &ITextRecoEventHandler_t809_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::RegisterTextRecoEventHandler(Vuforia.ITextRecoEventHandler)
MethodInfo TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4345_MethodInfo = 
{
	"RegisterTextRecoEventHandler"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4345/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4345_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2371/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ITextRecoEventHandler_t809_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4346_ParameterInfos[] = 
{
	{"trackableEventHandler", 0, 134220013, &EmptyCustomAttributesCache, &ITextRecoEventHandler_t809_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.TextRecoAbstractBehaviour::UnregisterTextRecoEventHandler(Vuforia.ITextRecoEventHandler)
MethodInfo TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4346_MethodInfo = 
{
	"UnregisterTextRecoEventHandler"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4346/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4346_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2372/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::StartTextTracker()
MethodInfo TextRecoAbstractBehaviour_StartTextTracker_m4347_MethodInfo = 
{
	"StartTextTracker"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_StartTextTracker_m4347/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2373/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::StopTextTracker()
MethodInfo TextRecoAbstractBehaviour_StopTextTracker_m4348_MethodInfo = 
{
	"StopTextTracker"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_StopTextTracker_m4348/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2374/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::SetupWordList()
MethodInfo TextRecoAbstractBehaviour_SetupWordList_m4349_MethodInfo = 
{
	"SetupWordList"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_SetupWordList_m4349/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2375/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerable_1_t735_0_0_0;
extern Il2CppType IEnumerable_1_t735_0_0_0;
extern Il2CppType IEnumerable_1_t734_0_0_0;
extern Il2CppType IEnumerable_1_t734_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4350_ParameterInfos[] = 
{
	{"lostWords", 0, 134220014, &EmptyCustomAttributesCache, &IEnumerable_1_t735_0_0_0},
	{"newWords", 1, 134220015, &EmptyCustomAttributesCache, &IEnumerable_1_t734_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::NotifyEventHandlersOfChanges(System.Collections.Generic.IEnumerable`1<Vuforia.Word>,System.Collections.Generic.IEnumerable`1<Vuforia.WordResult>)
MethodInfo TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4350_MethodInfo = 
{
	"NotifyEventHandlersOfChanges"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4350/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4350_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2376/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_WordListFile()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m467_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_WordListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m467/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2377/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m468_ParameterInfos[] = 
{
	{"value", 0, 134220016, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_WordListFile(System.String)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m468_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_WordListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m468/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m468_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2378/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_CustomWordListFile()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m469_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_CustomWordListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m469/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2379/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m470_ParameterInfos[] = 
{
	{"value", 0, 134220017, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_CustomWordListFile(System.String)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m470_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_CustomWordListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m470/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m470_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2380/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_AdditionalCustomWords()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m471_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_AdditionalCustomWords"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m471/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2381/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m472_ParameterInfos[] = 
{
	{"value", 0, 134220018, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_AdditionalCustomWords(System.String)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m472_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_AdditionalCustomWords"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m472/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m472_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2382/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WordFilterMode_t816_0_0_0;
extern void* RuntimeInvoker_WordFilterMode_t816 (MethodInfo* method, void* obj, void** args);
// Vuforia.WordFilterMode Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_FilterMode()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m473_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_FilterMode"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m473/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &WordFilterMode_t816_0_0_0/* return_type */
	, RuntimeInvoker_WordFilterMode_t816/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2383/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WordFilterMode_t816_0_0_0;
extern Il2CppType WordFilterMode_t816_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m474_ParameterInfos[] = 
{
	{"value", 0, 134220019, &EmptyCustomAttributesCache, &WordFilterMode_t816_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_FilterMode(Vuforia.WordFilterMode)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m474_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_FilterMode"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m474/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m474_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2384/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_FilterListFile()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m475_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_FilterListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m475/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2385/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m476_ParameterInfos[] = 
{
	{"value", 0, 134220020, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_FilterListFile(System.String)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m476_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_FilterListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m476/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m476_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2386/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_AdditionalFilterWords()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m477_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_AdditionalFilterWords"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m477/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2387/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m478_ParameterInfos[] = 
{
	{"value", 0, 134220021, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_AdditionalFilterWords(System.String)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m478_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_AdditionalFilterWords"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m478/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m478_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2388/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WordPrefabCreationMode_t732_0_0_0;
extern void* RuntimeInvoker_WordPrefabCreationMode_t732 (MethodInfo* method, void* obj, void** args);
// Vuforia.WordPrefabCreationMode Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_WordPrefabCreationMode()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m479_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_WordPrefabCreationMode"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m479/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &WordPrefabCreationMode_t732_0_0_0/* return_type */
	, RuntimeInvoker_WordPrefabCreationMode_t732/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2389/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WordPrefabCreationMode_t732_0_0_0;
extern Il2CppType WordPrefabCreationMode_t732_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m480_ParameterInfos[] = 
{
	{"value", 0, 134220022, &EmptyCustomAttributesCache, &WordPrefabCreationMode_t732_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_WordPrefabCreationMode(Vuforia.WordPrefabCreationMode)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m480_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_WordPrefabCreationMode"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m480/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m480_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2390/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Int32 Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_MaximumWordInstances()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m481_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_MaximumWordInstances"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m481/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2391/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m482_ParameterInfos[] = 
{
	{"value", 0, 134220023, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_MaximumWordInstances(System.Int32)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m482_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_MaximumWordInstances"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m482/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m482_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2392/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnQCARInitialized()
MethodInfo TextRecoAbstractBehaviour_OnQCARInitialized_m4351_MethodInfo = 
{
	"OnQCARInitialized"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnQCARInitialized_m4351/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2393/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnQCARStarted()
MethodInfo TextRecoAbstractBehaviour_OnQCARStarted_m4352_MethodInfo = 
{
	"OnQCARStarted"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnQCARStarted_m4352/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2394/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnTrackablesUpdated()
MethodInfo TextRecoAbstractBehaviour_OnTrackablesUpdated_m4353_MethodInfo = 
{
	"OnTrackablesUpdated"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnTrackablesUpdated_m4353/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2395/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_OnPause_m4354_ParameterInfos[] = 
{
	{"pause", 0, 134220024, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnPause(System.Boolean)
MethodInfo TextRecoAbstractBehaviour_OnPause_m4354_MethodInfo = 
{
	"OnPause"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnPause_m4354/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125/* invoker_method */
	, TextRecoAbstractBehaviour_t35_TextRecoAbstractBehaviour_OnPause_m4354_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2396/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::.ctor()
MethodInfo TextRecoAbstractBehaviour__ctor_m466_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour__ctor_m466/* method */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2397/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TextRecoAbstractBehaviour_t35_MethodInfos[] =
{
	&TextRecoAbstractBehaviour_get_IsInitialized_m4339_MethodInfo,
	&TextRecoAbstractBehaviour_Awake_m4340_MethodInfo,
	&TextRecoAbstractBehaviour_Start_m4341_MethodInfo,
	&TextRecoAbstractBehaviour_OnEnable_m4342_MethodInfo,
	&TextRecoAbstractBehaviour_OnDisable_m4343_MethodInfo,
	&TextRecoAbstractBehaviour_OnDestroy_m4344_MethodInfo,
	&TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4345_MethodInfo,
	&TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4346_MethodInfo,
	&TextRecoAbstractBehaviour_StartTextTracker_m4347_MethodInfo,
	&TextRecoAbstractBehaviour_StopTextTracker_m4348_MethodInfo,
	&TextRecoAbstractBehaviour_SetupWordList_m4349_MethodInfo,
	&TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4350_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m467_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m468_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m469_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m470_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m471_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m472_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m473_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m474_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m475_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m476_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m477_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m478_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m479_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m480_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m481_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m482_MethodInfo,
	&TextRecoAbstractBehaviour_OnQCARInitialized_m4351_MethodInfo,
	&TextRecoAbstractBehaviour_OnQCARStarted_m4352_MethodInfo,
	&TextRecoAbstractBehaviour_OnTrackablesUpdated_m4353_MethodInfo,
	&TextRecoAbstractBehaviour_OnPause_m4354_MethodInfo,
	&TextRecoAbstractBehaviour__ctor_m466_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m197_MethodInfo;
extern MethodInfo Object_GetHashCode_m199_MethodInfo;
extern MethodInfo Object_ToString_m200_MethodInfo;
static MethodInfo* TextRecoAbstractBehaviour_t35_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m467_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m468_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m469_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m470_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m471_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m472_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m473_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m474_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m475_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m476_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m477_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m478_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m479_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m480_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m481_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m482_MethodInfo,
};
extern TypeInfo IEditorTextRecoBehaviour_t166_il2cpp_TypeInfo;
static TypeInfo* TextRecoAbstractBehaviour_t35_InterfacesTypeInfos[] = 
{
	&IEditorTextRecoBehaviour_t166_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair TextRecoAbstractBehaviour_t35_InterfacesOffsets[] = 
{
	{ &IEditorTextRecoBehaviour_t166_il2cpp_TypeInfo, 4},
};
extern TypeInfo SerializeField_t469_il2cpp_TypeInfo;
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern MethodInfo SerializeField__ctor_m2084_MethodInfo;
extern TypeInfo HideInInspector_t204_il2cpp_TypeInfo;
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspector.h"
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspectorMethodDeclarations.h"
extern MethodInfo HideInInspector__ctor_m721_MethodInfo;
void TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mWordListFile(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mCustomWordListFile(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mAdditionalCustomWords(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mFilterMode(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mFilterListFile(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mAdditionalFilterWords(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mWordPrefabCreationMode(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mMaximumWordInstances(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mWordListFile = {
2,
NULL,
&TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mWordListFile
};
CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mCustomWordListFile = {
2,
NULL,
&TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mCustomWordListFile
};
CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mAdditionalCustomWords = {
2,
NULL,
&TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mAdditionalCustomWords
};
CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mFilterMode = {
2,
NULL,
&TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mFilterMode
};
CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mFilterListFile = {
2,
NULL,
&TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mFilterListFile
};
CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mAdditionalFilterWords = {
2,
NULL,
&TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mAdditionalFilterWords
};
CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mWordPrefabCreationMode = {
2,
NULL,
&TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mWordPrefabCreationMode
};
CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mMaximumWordInstances = {
2,
NULL,
&TextRecoAbstractBehaviour_t35_CustomAttributesCacheGenerator_mMaximumWordInstances
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType TextRecoAbstractBehaviour_t35_0_0_0;
extern Il2CppType TextRecoAbstractBehaviour_t35_1_0_0;
extern TypeInfo MonoBehaviour_t10_il2cpp_TypeInfo;
struct TextRecoAbstractBehaviour_t35;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mWordListFile;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mCustomWordListFile;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mAdditionalCustomWords;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mFilterMode;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mFilterListFile;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mAdditionalFilterWords;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mWordPrefabCreationMode;
extern CustomAttributesCache TextRecoAbstractBehaviour_t35__CustomAttributeCache_mMaximumWordInstances;
TypeInfo TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextRecoAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TextRecoAbstractBehaviour_t35_MethodInfos/* methods */
	, TextRecoAbstractBehaviour_t35_PropertyInfos/* properties */
	, TextRecoAbstractBehaviour_t35_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* element_class */
	, TextRecoAbstractBehaviour_t35_InterfacesTypeInfos/* implemented_interfaces */
	, TextRecoAbstractBehaviour_t35_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TextRecoAbstractBehaviour_t35_il2cpp_TypeInfo/* cast_class */
	, &TextRecoAbstractBehaviour_t35_0_0_0/* byval_arg */
	, &TextRecoAbstractBehaviour_t35_1_0_0/* this_arg */
	, TextRecoAbstractBehaviour_t35_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextRecoAbstractBehaviour_t35)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 33/* method_count */
	, 9/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 20/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.SimpleTargetData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SimpleTargetData.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SimpleTargetData_t810_il2cpp_TypeInfo;
// Vuforia.SimpleTargetData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SimpleTargetDataMethodDeclarations.h"



// Metadata Definition Vuforia.SimpleTargetData
extern Il2CppType Int32_t123_0_0_6;
FieldInfo SimpleTargetData_t810____id_0_FieldInfo = 
{
	"id"/* name */
	, &Int32_t123_0_0_6/* type */
	, &SimpleTargetData_t810_il2cpp_TypeInfo/* parent */
	, offsetof(SimpleTargetData_t810, ___id_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_3;
FieldInfo SimpleTargetData_t810____unused_1_FieldInfo = 
{
	"unused"/* name */
	, &Int32_t123_0_0_3/* type */
	, &SimpleTargetData_t810_il2cpp_TypeInfo/* parent */
	, offsetof(SimpleTargetData_t810, ___unused_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* SimpleTargetData_t810_FieldInfos[] =
{
	&SimpleTargetData_t810____id_0_FieldInfo,
	&SimpleTargetData_t810____unused_1_FieldInfo,
	NULL
};
static MethodInfo* SimpleTargetData_t810_MethodInfos[] =
{
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
static MethodInfo* SimpleTargetData_t810_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType SimpleTargetData_t810_0_0_0;
extern Il2CppType SimpleTargetData_t810_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
TypeInfo SimpleTargetData_t810_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "SimpleTargetData"/* name */
	, "Vuforia"/* namespaze */
	, SimpleTargetData_t810_MethodInfos/* methods */
	, NULL/* properties */
	, SimpleTargetData_t810_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SimpleTargetData_t810_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SimpleTargetData_t810_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SimpleTargetData_t810_il2cpp_TypeInfo/* cast_class */
	, &SimpleTargetData_t810_0_0_0/* byval_arg */
	, &SimpleTargetData_t810_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SimpleTargetData_t810)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, sizeof(SimpleTargetData_t810 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, true/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeha.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TurnOffAbstractBehaviour_t31_il2cpp_TypeInfo;
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBehaMethodDeclarations.h"



// System.Void Vuforia.TurnOffAbstractBehaviour::.ctor()
extern MethodInfo TurnOffAbstractBehaviour__ctor_m483_MethodInfo;
 void TurnOffAbstractBehaviour__ctor_m483 (TurnOffAbstractBehaviour_t31 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.TurnOffAbstractBehaviour
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffAbstractBehaviour::.ctor()
MethodInfo TurnOffAbstractBehaviour__ctor_m483_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TurnOffAbstractBehaviour__ctor_m483/* method */
	, &TurnOffAbstractBehaviour_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2398/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TurnOffAbstractBehaviour_t31_MethodInfos[] =
{
	&TurnOffAbstractBehaviour__ctor_m483_MethodInfo,
	NULL
};
static MethodInfo* TurnOffAbstractBehaviour_t31_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType TurnOffAbstractBehaviour_t31_0_0_0;
extern Il2CppType TurnOffAbstractBehaviour_t31_1_0_0;
struct TurnOffAbstractBehaviour_t31;
TypeInfo TurnOffAbstractBehaviour_t31_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "TurnOffAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TurnOffAbstractBehaviour_t31_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TurnOffAbstractBehaviour_t31_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TurnOffAbstractBehaviour_t31_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TurnOffAbstractBehaviour_t31_il2cpp_TypeInfo/* cast_class */
	, &TurnOffAbstractBehaviour_t31_0_0_0/* byval_arg */
	, &TurnOffAbstractBehaviour_t31_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TurnOffAbstractBehaviour_t31)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBu.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo;
// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBuMethodDeclarations.h"

// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_46.h"
// Vuforia.ObjectTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTracker.h"
// Vuforia.ImageTargetBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder.h"
// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_21.h"
// Vuforia.TrackableSource
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableSource.h"
extern TypeInfo List_1_t811_il2cpp_TypeInfo;
extern TypeInfo IUserDefinedTargetEventHandler_t812_il2cpp_TypeInfo;
extern TypeInfo ObjectTracker_t605_il2cpp_TypeInfo;
extern TypeInfo ImageTargetBuilder_t645_il2cpp_TypeInfo;
extern TypeInfo Single_t170_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t945_il2cpp_TypeInfo;
extern TypeInfo FrameQuality_t644_il2cpp_TypeInfo;
extern TypeInfo TrackableSource_t630_il2cpp_TypeInfo;
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_46MethodDeclarations.h"
// Vuforia.ObjectTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrackerMethodDeclarations.h"
// Vuforia.ImageTargetBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilderMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_21MethodDeclarations.h"
extern MethodInfo List_1_Add_m5536_MethodInfo;
extern MethodInfo IUserDefinedTargetEventHandler_OnInitialized_m5269_MethodInfo;
extern MethodInfo List_1_Remove_m5537_MethodInfo;
extern MethodInfo ObjectTracker_get_ImageTargetBuilder_m4914_MethodInfo;
extern MethodInfo ImageTargetBuilder_StartScan_m4736_MethodInfo;
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4360_MethodInfo;
extern MethodInfo ImageTargetBuilder_Build_m4735_MethodInfo;
extern MethodInfo ImageTargetBuilder_StopScan_m4737_MethodInfo;
extern MethodInfo List_1_GetEnumerator_m5538_MethodInfo;
extern MethodInfo Enumerator_get_Current_m5539_MethodInfo;
extern MethodInfo IUserDefinedTargetEventHandler_OnFrameQualityChanged_m5270_MethodInfo;
extern MethodInfo Enumerator_MoveNext_m5540_MethodInfo;
extern MethodInfo KeepAliveAbstractBehaviour_get_KeepUDTBuildingBehaviourAlive_m4100_MethodInfo;
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4366_MethodInfo;
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4367_MethodInfo;
extern MethodInfo ImageTargetBuilder_GetFrameQuality_m4738_MethodInfo;
extern MethodInfo ImageTargetBuilder_GetTrackableSource_m4739_MethodInfo;
extern MethodInfo IUserDefinedTargetEventHandler_OnNewTrackableSource_m5271_MethodInfo;
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4359_MethodInfo;
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4357_MethodInfo;
extern MethodInfo TrackerManager_GetTracker_TisObjectTracker_t605_m4592_MethodInfo;
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4364_MethodInfo;
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4363_MethodInfo;
extern MethodInfo List_1__ctor_m5541_MethodInfo;
struct TrackerManager_t787;
// Declaration T Vuforia.TrackerManager::GetTracker<Vuforia.ObjectTracker>()
// T Vuforia.TrackerManager::GetTracker<Vuforia.ObjectTracker>()


// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::RegisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4355_MethodInfo;
 void UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4355 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, Object_t * ___eventHandler, MethodInfo* method){
	{
		List_1_t811 * L_0 = (__this->___mHandlers_9);
		NullCheck(L_0);
		VirtActionInvoker1< Object_t * >::Invoke(&List_1_Add_m5536_MethodInfo, L_0, ___eventHandler);
		bool L_1 = (__this->___mOnInitializedCalled_8);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		NullCheck(___eventHandler);
		InterfaceActionInvoker0::Invoke(&IUserDefinedTargetEventHandler_OnInitialized_m5269_MethodInfo, ___eventHandler);
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::UnregisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4356_MethodInfo;
 bool UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4356 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, Object_t * ___eventHandler, MethodInfo* method){
	{
		List_1_t811 * L_0 = (__this->___mHandlers_9);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&List_1_Remove_m5537_MethodInfo, L_0, ___eventHandler);
		return L_1;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StartScanning()
 void UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4357 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		ObjectTracker_t605 * L_0 = (__this->___mObjectTracker_2);
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		bool L_1 = (__this->___StopTrackerWhileScanning_10);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		ObjectTracker_t605 * L_2 = (__this->___mObjectTracker_2);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(&Tracker_Stop_m4675_MethodInfo, L_2);
	}

IL_001b:
	{
		ObjectTracker_t605 * L_3 = (__this->___mObjectTracker_2);
		NullCheck(L_3);
		ImageTargetBuilder_t645 * L_4 = (ImageTargetBuilder_t645 *)VirtFuncInvoker0< ImageTargetBuilder_t645 * >::Invoke(&ObjectTracker_get_ImageTargetBuilder_m4914_MethodInfo, L_3);
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(&ImageTargetBuilder_StartScan_m4736_MethodInfo, L_4);
		__this->___mCurrentlyScanning_4 = 1;
	}

IL_0032:
	{
		UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4360(__this, 0, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4360_MethodInfo);
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::BuildNewTarget(System.String,System.Single)
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4358_MethodInfo;
 void UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4358 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, String_t* ___targetName, float ___sceenSizeWidth, MethodInfo* method){
	{
		__this->___mCurrentlyBuilding_6 = 1;
		ObjectTracker_t605 * L_0 = (__this->___mObjectTracker_2);
		NullCheck(L_0);
		ImageTargetBuilder_t645 * L_1 = (ImageTargetBuilder_t645 *)VirtFuncInvoker0< ImageTargetBuilder_t645 * >::Invoke(&ObjectTracker_get_ImageTargetBuilder_m4914_MethodInfo, L_0);
		NullCheck(L_1);
		VirtFuncInvoker2< bool, String_t*, float >::Invoke(&ImageTargetBuilder_Build_m4735_MethodInfo, L_1, ___targetName, ___sceenSizeWidth);
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StopScanning()
 void UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4359 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		__this->___mCurrentlyScanning_4 = 0;
		ObjectTracker_t605 * L_0 = (__this->___mObjectTracker_2);
		NullCheck(L_0);
		ImageTargetBuilder_t645 * L_1 = (ImageTargetBuilder_t645 *)VirtFuncInvoker0< ImageTargetBuilder_t645 * >::Invoke(&ObjectTracker_get_ImageTargetBuilder_m4914_MethodInfo, L_0);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(&ImageTargetBuilder_StopScan_m4737_MethodInfo, L_1);
		bool L_2 = (__this->___StopTrackerWhileScanning_10);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		ObjectTracker_t605 * L_3 = (__this->___mObjectTracker_2);
		NullCheck(L_3);
		VirtFuncInvoker0< bool >::Invoke(&Tracker_Start_m4674_MethodInfo, L_3);
	}

IL_002b:
	{
		UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4360(__this, (-1), /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4360_MethodInfo);
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::SetFrameQuality(Vuforia.ImageTargetBuilder/FrameQuality)
 void UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4360 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, int32_t ___frameQuality, MethodInfo* method){
	Object_t * V_0 = {0};
	Enumerator_t945  V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t151 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t151 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		int32_t L_0 = (__this->___mLastFrameQuality_3);
		if ((((int32_t)___frameQuality) == ((int32_t)L_0)))
		{
			goto IL_0046;
		}
	}
	{
		List_1_t811 * L_1 = (__this->___mHandlers_9);
		NullCheck(L_1);
		Enumerator_t945  L_2 = List_1_GetEnumerator_m5538(L_1, /*hidden argument*/&List_1_GetEnumerator_m5538_MethodInfo);
		V_1 = L_2;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0017:
		{
			Object_t * L_3 = Enumerator_get_Current_m5539((&V_1), /*hidden argument*/&Enumerator_get_Current_m5539_MethodInfo);
			V_0 = L_3;
			NullCheck(V_0);
			InterfaceActionInvoker1< int32_t >::Invoke(&IUserDefinedTargetEventHandler_OnFrameQualityChanged_m5270_MethodInfo, V_0, ___frameQuality);
		}

IL_0026:
		{
			bool L_4 = Enumerator_MoveNext_m5540((&V_1), /*hidden argument*/&Enumerator_MoveNext_m5540_MethodInfo);
			if (L_4)
			{
				goto IL_0017;
			}
		}

IL_002f:
		{
			// IL_002f: leave.s IL_003f
			leaveInstructions[0] = 0x3F; // 1
			THROW_SENTINEL(IL_003f);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0031;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t151 *)e.ex;
		goto IL_0031;
	}

IL_0031:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t945_il2cpp_TypeInfo), &(*(&V_1))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m333_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t945_il2cpp_TypeInfo), &(*(&V_1))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x3F:
				goto IL_003f;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t151 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_003f:
	{
		__this->___mLastFrameQuality_3 = ___frameQuality;
	}

IL_0046:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Start()
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_Start_m4361_MethodInfo;
 void UserDefinedTargetBuildingAbstractBehaviour_Start_m4361 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method){
	QCARAbstractBehaviour_t45 * V_0 = {0};
	{
		KeepAliveAbstractBehaviour_t38 * L_0 = KeepAliveAbstractBehaviour_get_Instance_m4106(NULL /*static, unused*/, /*hidden argument*/&KeepAliveAbstractBehaviour_get_Instance_m4106_MethodInfo);
		bool L_1 = Object_op_Inequality_m489(NULL /*static, unused*/, L_0, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		KeepAliveAbstractBehaviour_t38 * L_2 = KeepAliveAbstractBehaviour_get_Instance_m4106(NULL /*static, unused*/, /*hidden argument*/&KeepAliveAbstractBehaviour_get_Instance_m4106_MethodInfo);
		NullCheck(L_2);
		bool L_3 = KeepAliveAbstractBehaviour_get_KeepUDTBuildingBehaviourAlive_m4100(L_2, /*hidden argument*/&KeepAliveAbstractBehaviour_get_KeepUDTBuildingBehaviourAlive_m4100_MethodInfo);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		GameObject_t29 * L_4 = Component_get_gameObject_m419(__this, /*hidden argument*/&Component_get_gameObject_m419_MethodInfo);
		Object_DontDestroyOnLoad_m4535(NULL /*static, unused*/, L_4, /*hidden argument*/&Object_DontDestroyOnLoad_m4535_MethodInfo);
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_5 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&QCARAbstractBehaviour_t45_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Object_t120 * L_6 = Object_FindObjectOfType_m256(NULL /*static, unused*/, L_5, /*hidden argument*/&Object_FindObjectOfType_m256_MethodInfo);
		V_0 = ((QCARAbstractBehaviour_t45 *)Castclass(L_6, InitializedTypeInfo(&QCARAbstractBehaviour_t45_il2cpp_TypeInfo)));
		bool L_7 = Object_op_Implicit_m257(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_7)
		{
			goto IL_0065;
		}
	}
	{
		IntPtr_t121 L_8 = { &UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4366_MethodInfo };
		Action_t147 * L_9 = (Action_t147 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t147_il2cpp_TypeInfo));
		Action__ctor_m4584(L_9, __this, L_8, /*hidden argument*/&Action__ctor_m4584_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterQCARStartedCallback_m4281(V_0, L_9, /*hidden argument*/&QCARAbstractBehaviour_RegisterQCARStartedCallback_m4281_MethodInfo);
		IntPtr_t121 L_10 = { &UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4367_MethodInfo };
		Action_1_t802 * L_11 = (Action_1_t802 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_1_t802_il2cpp_TypeInfo));
		Action_1__ctor_m4672(L_11, __this, L_10, /*hidden argument*/&Action_1__ctor_m4672_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterOnPauseCallback_m4285(V_0, L_11, /*hidden argument*/&QCARAbstractBehaviour_RegisterOnPauseCallback_m4285_MethodInfo);
	}

IL_0065:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Update()
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_Update_m4362_MethodInfo;
 void UserDefinedTargetBuildingAbstractBehaviour_Update_m4362 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method){
	TrackableSource_t630 * V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t945  V_2 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t151 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t151 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		bool L_0 = (__this->___mOnInitializedCalled_8);
		if (!L_0)
		{
			goto IL_0090;
		}
	}
	{
		bool L_1 = (__this->___mCurrentlyScanning_4);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		ObjectTracker_t605 * L_2 = (__this->___mObjectTracker_2);
		NullCheck(L_2);
		ImageTargetBuilder_t645 * L_3 = (ImageTargetBuilder_t645 *)VirtFuncInvoker0< ImageTargetBuilder_t645 * >::Invoke(&ObjectTracker_get_ImageTargetBuilder_m4914_MethodInfo, L_2);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&ImageTargetBuilder_GetFrameQuality_m4738_MethodInfo, L_3);
		UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4360(__this, L_4, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4360_MethodInfo);
	}

IL_0029:
	{
		bool L_5 = (__this->___mCurrentlyBuilding_6);
		if (!L_5)
		{
			goto IL_0090;
		}
	}
	{
		ObjectTracker_t605 * L_6 = (__this->___mObjectTracker_2);
		NullCheck(L_6);
		ImageTargetBuilder_t645 * L_7 = (ImageTargetBuilder_t645 *)VirtFuncInvoker0< ImageTargetBuilder_t645 * >::Invoke(&ObjectTracker_get_ImageTargetBuilder_m4914_MethodInfo, L_6);
		NullCheck(L_7);
		TrackableSource_t630 * L_8 = (TrackableSource_t630 *)VirtFuncInvoker0< TrackableSource_t630 * >::Invoke(&ImageTargetBuilder_GetTrackableSource_m4739_MethodInfo, L_7);
		V_0 = L_8;
		if (!V_0)
		{
			goto IL_0090;
		}
	}
	{
		__this->___mCurrentlyBuilding_6 = 0;
		List_1_t811 * L_9 = (__this->___mHandlers_9);
		NullCheck(L_9);
		Enumerator_t945  L_10 = List_1_GetEnumerator_m5538(L_9, /*hidden argument*/&List_1_GetEnumerator_m5538_MethodInfo);
		V_2 = L_10;
	}

IL_0058:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0069;
		}

IL_005a:
		{
			Object_t * L_11 = Enumerator_get_Current_m5539((&V_2), /*hidden argument*/&Enumerator_get_Current_m5539_MethodInfo);
			V_1 = L_11;
			NullCheck(V_1);
			InterfaceActionInvoker1< TrackableSource_t630 * >::Invoke(&IUserDefinedTargetEventHandler_OnNewTrackableSource_m5271_MethodInfo, V_1, V_0);
		}

IL_0069:
		{
			bool L_12 = Enumerator_MoveNext_m5540((&V_2), /*hidden argument*/&Enumerator_MoveNext_m5540_MethodInfo);
			if (L_12)
			{
				goto IL_005a;
			}
		}

IL_0072:
		{
			// IL_0072: leave.s IL_0082
			leaveInstructions[0] = 0x82; // 1
			THROW_SENTINEL(IL_0082);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0074;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t151 *)e.ex;
		goto IL_0074;
	}

IL_0074:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t945_il2cpp_TypeInfo), &(*(&V_2))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m333_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t945_il2cpp_TypeInfo), &(*(&V_2))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x82:
				goto IL_0082;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t151 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0082:
	{
		bool L_13 = (__this->___StopScanningWhenFinshedBuilding_12);
		if (!L_13)
		{
			goto IL_0090;
		}
	}
	{
		UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4359(__this, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4359_MethodInfo);
	}

IL_0090:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnEnable()
 void UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4363 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mOnInitializedCalled_8);
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		bool L_1 = (__this->___mWasScanningBeforeDisable_5);
		__this->___mCurrentlyScanning_4 = L_1;
		bool L_2 = (__this->___mWasBuildingBeforeDisable_7);
		__this->___mCurrentlyBuilding_6 = L_2;
		bool L_3 = (__this->___mWasScanningBeforeDisable_5);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4357(__this, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4357_MethodInfo);
	}

IL_002e:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDisable()
 void UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4364 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mOnInitializedCalled_8);
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		bool L_1 = (__this->___mCurrentlyScanning_4);
		__this->___mWasScanningBeforeDisable_5 = L_1;
		bool L_2 = (__this->___mCurrentlyBuilding_6);
		__this->___mWasBuildingBeforeDisable_7 = L_2;
		bool L_3 = (__this->___mCurrentlyScanning_4);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4359(__this, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4359_MethodInfo);
	}

IL_002e:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDestroy()
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4365_MethodInfo;
 void UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4365 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method){
	QCARAbstractBehaviour_t45 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&QCARAbstractBehaviour_t45_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Object_t120 * L_1 = Object_FindObjectOfType_m256(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_FindObjectOfType_m256_MethodInfo);
		V_0 = ((QCARAbstractBehaviour_t45 *)Castclass(L_1, InitializedTypeInfo(&QCARAbstractBehaviour_t45_il2cpp_TypeInfo)));
		bool L_2 = Object_op_Implicit_m257(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		IntPtr_t121 L_3 = { &UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4366_MethodInfo };
		Action_t147 * L_4 = (Action_t147 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t147_il2cpp_TypeInfo));
		Action__ctor_m4584(L_4, __this, L_3, /*hidden argument*/&Action__ctor_m4584_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4282(V_0, L_4, /*hidden argument*/&QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4282_MethodInfo);
		IntPtr_t121 L_5 = { &UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4367_MethodInfo };
		Action_1_t802 * L_6 = (Action_1_t802 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_1_t802_il2cpp_TypeInfo));
		Action_1__ctor_m4672(L_6, __this, L_5, /*hidden argument*/&Action_1__ctor_m4672_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_UnregisterOnPauseCallback_m4286(V_0, L_6, /*hidden argument*/&QCARAbstractBehaviour_UnregisterOnPauseCallback_m4286_MethodInfo);
	}

IL_0041:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnQCARStarted()
 void UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4366 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method){
	Object_t * V_0 = {0};
	Enumerator_t945  V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t151 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t151 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		__this->___mOnInitializedCalled_8 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t787_il2cpp_TypeInfo));
		TrackerManager_t787 * L_0 = TrackerManager_get_Instance_m4207(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4207_MethodInfo);
		NullCheck(L_0);
		ObjectTracker_t605 * L_1 = (ObjectTracker_t605 *)GenericVirtFuncInvoker0< ObjectTracker_t605 * >::Invoke(&TrackerManager_GetTracker_TisObjectTracker_t605_m4592_MethodInfo, L_0);
		__this->___mObjectTracker_2 = L_1;
		List_1_t811 * L_2 = (__this->___mHandlers_9);
		NullCheck(L_2);
		Enumerator_t945  L_3 = List_1_GetEnumerator_m5538(L_2, /*hidden argument*/&List_1_GetEnumerator_m5538_MethodInfo);
		V_1 = L_3;
	}

IL_0023:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0033;
		}

IL_0025:
		{
			Object_t * L_4 = Enumerator_get_Current_m5539((&V_1), /*hidden argument*/&Enumerator_get_Current_m5539_MethodInfo);
			V_0 = L_4;
			NullCheck(V_0);
			InterfaceActionInvoker0::Invoke(&IUserDefinedTargetEventHandler_OnInitialized_m5269_MethodInfo, V_0);
		}

IL_0033:
		{
			bool L_5 = Enumerator_MoveNext_m5540((&V_1), /*hidden argument*/&Enumerator_MoveNext_m5540_MethodInfo);
			if (L_5)
			{
				goto IL_0025;
			}
		}

IL_003c:
		{
			// IL_003c: leave.s IL_004c
			leaveInstructions[0] = 0x4C; // 1
			THROW_SENTINEL(IL_004c);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_003e;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t151 *)e.ex;
		goto IL_003e;
	}

IL_003e:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t945_il2cpp_TypeInfo), &(*(&V_1))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m333_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t945_il2cpp_TypeInfo), &(*(&V_1))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x4C:
				goto IL_004c;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t151 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_004c:
	{
		bool L_6 = (__this->___StartScanningAutomatically_11);
		if (!L_6)
		{
			goto IL_005a;
		}
	}
	{
		UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4357(__this, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4357_MethodInfo);
	}

IL_005a:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnPause(System.Boolean)
 void UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4367 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, bool ___pause, MethodInfo* method){
	{
		if (!___pause)
		{
			goto IL_000a;
		}
	}
	{
		UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4364(__this, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4364_MethodInfo);
		return;
	}

IL_000a:
	{
		UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4363(__this, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4363_MethodInfo);
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::.ctor()
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour__ctor_m490_MethodInfo;
 void UserDefinedTargetBuildingAbstractBehaviour__ctor_m490 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		__this->___mLastFrameQuality_3 = (-1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&List_1_t811_il2cpp_TypeInfo));
		List_1_t811 * L_0 = (List_1_t811 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t811_il2cpp_TypeInfo));
		List_1__ctor_m5541(L_0, /*hidden argument*/&List_1__ctor_m5541_MethodInfo);
		__this->___mHandlers_9 = L_0;
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.UserDefinedTargetBuildingAbstractBehaviour
extern Il2CppType ObjectTracker_t605_0_0_1;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t56____mObjectTracker_2_FieldInfo = 
{
	"mObjectTracker"/* name */
	, &ObjectTracker_t605_0_0_1/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t56, ___mObjectTracker_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType FrameQuality_t644_0_0_1;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t56____mLastFrameQuality_3_FieldInfo = 
{
	"mLastFrameQuality"/* name */
	, &FrameQuality_t644_0_0_1/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t56, ___mLastFrameQuality_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t56____mCurrentlyScanning_4_FieldInfo = 
{
	"mCurrentlyScanning"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t56, ___mCurrentlyScanning_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t56____mWasScanningBeforeDisable_5_FieldInfo = 
{
	"mWasScanningBeforeDisable"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t56, ___mWasScanningBeforeDisable_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t56____mCurrentlyBuilding_6_FieldInfo = 
{
	"mCurrentlyBuilding"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t56, ___mCurrentlyBuilding_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t56____mWasBuildingBeforeDisable_7_FieldInfo = 
{
	"mWasBuildingBeforeDisable"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t56, ___mWasBuildingBeforeDisable_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t56____mOnInitializedCalled_8_FieldInfo = 
{
	"mOnInitializedCalled"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t56, ___mOnInitializedCalled_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType List_1_t811_0_0_33;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t56____mHandlers_9_FieldInfo = 
{
	"mHandlers"/* name */
	, &List_1_t811_0_0_33/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t56, ___mHandlers_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t56____StopTrackerWhileScanning_10_FieldInfo = 
{
	"StopTrackerWhileScanning"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t56, ___StopTrackerWhileScanning_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t56____StartScanningAutomatically_11_FieldInfo = 
{
	"StartScanningAutomatically"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t56, ___StartScanningAutomatically_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_6;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t56____StopScanningWhenFinshedBuilding_12_FieldInfo = 
{
	"StopScanningWhenFinshedBuilding"/* name */
	, &Boolean_t122_0_0_6/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t56, ___StopScanningWhenFinshedBuilding_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UserDefinedTargetBuildingAbstractBehaviour_t56_FieldInfos[] =
{
	&UserDefinedTargetBuildingAbstractBehaviour_t56____mObjectTracker_2_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t56____mLastFrameQuality_3_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t56____mCurrentlyScanning_4_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t56____mWasScanningBeforeDisable_5_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t56____mCurrentlyBuilding_6_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t56____mWasBuildingBeforeDisable_7_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t56____mOnInitializedCalled_8_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t56____mHandlers_9_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t56____StopTrackerWhileScanning_10_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t56____StartScanningAutomatically_11_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t56____StopScanningWhenFinshedBuilding_12_FieldInfo,
	NULL
};
extern Il2CppType IUserDefinedTargetEventHandler_t812_0_0_0;
extern Il2CppType IUserDefinedTargetEventHandler_t812_0_0_0;
static ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t56_UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4355_ParameterInfos[] = 
{
	{"eventHandler", 0, 134220025, &EmptyCustomAttributesCache, &IUserDefinedTargetEventHandler_t812_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::RegisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4355_MethodInfo = 
{
	"RegisterEventHandler"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4355/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t56_UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4355_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2399/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IUserDefinedTargetEventHandler_t812_0_0_0;
static ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t56_UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4356_ParameterInfos[] = 
{
	{"eventHandler", 0, 134220026, &EmptyCustomAttributesCache, &IUserDefinedTargetEventHandler_t812_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::UnregisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4356_MethodInfo = 
{
	"UnregisterEventHandler"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4356/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t56_UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4356_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2400/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StartScanning()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4357_MethodInfo = 
{
	"StartScanning"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4357/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2401/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Single_t170_0_0_0;
extern Il2CppType Single_t170_0_0_0;
static ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t56_UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4358_ParameterInfos[] = 
{
	{"targetName", 0, 134220027, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"sceenSizeWidth", 1, 134220028, &EmptyCustomAttributesCache, &Single_t170_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Single_t170 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::BuildNewTarget(System.String,System.Single)
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4358_MethodInfo = 
{
	"BuildNewTarget"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4358/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Single_t170/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t56_UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4358_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2402/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StopScanning()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4359_MethodInfo = 
{
	"StopScanning"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4359/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2403/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType FrameQuality_t644_0_0_0;
extern Il2CppType FrameQuality_t644_0_0_0;
static ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t56_UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4360_ParameterInfos[] = 
{
	{"frameQuality", 0, 134220029, &EmptyCustomAttributesCache, &FrameQuality_t644_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::SetFrameQuality(Vuforia.ImageTargetBuilder/FrameQuality)
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4360_MethodInfo = 
{
	"SetFrameQuality"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4360/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t56_UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4360_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2404/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Start()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_Start_m4361_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_Start_m4361/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2405/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Update()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_Update_m4362_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_Update_m4362/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2406/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnEnable()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4363_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4363/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2407/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDisable()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4364_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4364/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2408/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDestroy()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4365_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4365/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2409/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnQCARStarted()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4366_MethodInfo = 
{
	"OnQCARStarted"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4366/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2410/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t56_UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4367_ParameterInfos[] = 
{
	{"pause", 0, 134220030, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnPause(System.Boolean)
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4367_MethodInfo = 
{
	"OnPause"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4367/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t56_UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4367_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2411/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::.ctor()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour__ctor_m490_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour__ctor_m490/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2412/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UserDefinedTargetBuildingAbstractBehaviour_t56_MethodInfos[] =
{
	&UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4355_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4356_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4357_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4358_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4359_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4360_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_Start_m4361_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_Update_m4362_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4363_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4364_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4365_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4366_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4367_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour__ctor_m490_MethodInfo,
	NULL
};
static MethodInfo* UserDefinedTargetBuildingAbstractBehaviour_t56_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0;
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t56_1_0_0;
struct UserDefinedTargetBuildingAbstractBehaviour_t56;
TypeInfo UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserDefinedTargetBuildingAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, UserDefinedTargetBuildingAbstractBehaviour_t56_MethodInfos/* methods */
	, NULL/* properties */
	, UserDefinedTargetBuildingAbstractBehaviour_t56_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UserDefinedTargetBuildingAbstractBehaviour_t56_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_il2cpp_TypeInfo/* cast_class */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_0_0_0/* byval_arg */
	, &UserDefinedTargetBuildingAbstractBehaviour_t56_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserDefinedTargetBuildingAbstractBehaviour_t56)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 14/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.VideoBackgroundAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo;
// Vuforia.VideoBackgroundAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbstMethodDeclarations.h"

// UnityEngine.MeshRenderer
#include "UnityEngine_UnityEngine_MeshRenderer.h"
// System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator_.h"
// Vuforia.BackgroundPlaneAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbst.h"
// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
#include "System_Core_System_Collections_Generic_HashSet_1_gen.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Vuforia.QCARManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManager.h"
// Vuforia.QCARManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
extern TypeInfo HashSet_1_t813_il2cpp_TypeInfo;
extern TypeInfo MeshRenderer_t167_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t946_il2cpp_TypeInfo;
extern TypeInfo Vector3_t73_il2cpp_TypeInfo;
extern TypeInfo QCARManager_t171_il2cpp_TypeInfo;
extern TypeInfo QCARManagerImpl_t702_il2cpp_TypeInfo;
extern TypeInfo Color_t66_il2cpp_TypeInfo;
// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
#include "System_Core_System_Collections_Generic_HashSet_1_genMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
// System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator_MethodDeclarations.h"
// Vuforia.UnityCameraExtensions
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityCameraExtensioMethodDeclarations.h"
// Vuforia.BackgroundPlaneAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbstMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
// Vuforia.QCARManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerMethodDeclarations.h"
// Vuforia.QCARManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImplMethodDeclarations.h"
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
extern MethodInfo Component_GetComponent_TisMeshRenderer_t167_m484_MethodInfo;
extern MethodInfo HashSet_1_Contains_m5542_MethodInfo;
extern MethodInfo Renderer_set_enabled_m285_MethodInfo;
extern MethodInfo HashSet_1_Add_m5543_MethodInfo;
extern MethodInfo HashSet_1_GetEnumerator_m5544_MethodInfo;
extern MethodInfo Enumerator_get_Current_m5545_MethodInfo;
extern MethodInfo Enumerator_MoveNext_m5546_MethodInfo;
extern MethodInfo HashSet_1_Clear_m5547_MethodInfo;
extern MethodInfo UnityCameraExtensions_GetMaxDepthForVideoBackground_m2797_MethodInfo;
extern MethodInfo Mathf_Min_m2650_MethodInfo;
extern MethodInfo UnityCameraExtensions_GetMinDepthForVideoBackground_m2798_MethodInfo;
extern MethodInfo Mathf_Max_m2593_MethodInfo;
extern MethodInfo VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4372_MethodInfo;
extern MethodInfo BackgroundPlaneAbstractBehaviour_SetStereoDepth_m2777_MethodInfo;
extern MethodInfo Camera_get_projectionMatrix_m823_MethodInfo;
extern MethodInfo Component_get_transform_m487_MethodInfo;
extern MethodInfo Transform_get_localPosition_m830_MethodInfo;
extern MethodInfo Matrix4x4_get_Item_m5548_MethodInfo;
extern MethodInfo Matrix4x4_set_Item_m5549_MethodInfo;
extern MethodInfo Camera_set_projectionMatrix_m829_MethodInfo;
extern MethodInfo QCARManager_get_Instance_m520_MethodInfo;
extern MethodInfo QCARManagerImpl_StartRendering_m3161_MethodInfo;
extern MethodInfo Transform_get_parent_m858_MethodInfo;
extern MethodInfo Component_GetComponent_TisQCARAbstractBehaviour_t45_m5550_MethodInfo;
extern MethodInfo VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4373_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_RegisterRenderOnUpdateCallback_m4299_MethodInfo;
extern MethodInfo Component_GetComponent_TisCamera_t168_m822_MethodInfo;
extern MethodInfo Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t2_m4611_MethodInfo;
extern MethodInfo VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368_MethodInfo;
extern MethodInfo GL_SetRevertBackfacing_m5551_MethodInfo;
extern MethodInfo QCARManagerImpl_FinishRendering_m3162_MethodInfo;
extern MethodInfo Color__ctor_m683_MethodInfo;
extern MethodInfo GL_Clear_m4740_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_UnregisterRenderOnUpdateCallback_m4300_MethodInfo;
extern MethodInfo HashSet_1__ctor_m5552_MethodInfo;
struct Component_t128;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
// UnityEngine.CastHelper`1<UnityEngine.MeshRenderer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_3.h"
struct Component_t128;
// UnityEngine.CastHelper`1<System.Object>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_0.h"
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
 Object_t * Component_GetComponent_TisObject_t_m280_gshared (Component_t128 * __this, MethodInfo* method);
#define Component_GetComponent_TisObject_t_m280(__this, method) (Object_t *)Component_GetComponent_TisObject_t_m280_gshared((Component_t128 *)__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t167_m484(__this, method) (MeshRenderer_t167 *)Component_GetComponent_TisObject_t_m280_gshared((Component_t128 *)__this, method)
struct Component_t128;
// UnityEngine.CastHelper`1<Vuforia.QCARAbstractBehaviour>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_34.h"
// Declaration !!0 UnityEngine.Component::GetComponent<Vuforia.QCARAbstractBehaviour>()
// !!0 UnityEngine.Component::GetComponent<Vuforia.QCARAbstractBehaviour>()
#define Component_GetComponent_TisQCARAbstractBehaviour_t45_m5550(__this, method) (QCARAbstractBehaviour_t45 *)Component_GetComponent_TisObject_t_m280_gshared((Component_t128 *)__this, method)
struct Component_t128;
// UnityEngine.CastHelper`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_17.h"
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t168_m822(__this, method) (Camera_t168 *)Component_GetComponent_TisObject_t_m280_gshared((Component_t128 *)__this, method)
struct Component_t128;
struct Component_t128;
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
 Object_t * Component_GetComponentInChildren_TisObject_t_m646_gshared (Component_t128 * __this, MethodInfo* method);
#define Component_GetComponentInChildren_TisObject_t_m646(__this, method) (Object_t *)Component_GetComponentInChildren_TisObject_t_m646_gshared((Component_t128 *)__this, method)
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<Vuforia.BackgroundPlaneAbstractBehaviour>()
// !!0 UnityEngine.Component::GetComponentInChildren<Vuforia.BackgroundPlaneAbstractBehaviour>()
#define Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t2_m4611(__this, method) (BackgroundPlaneAbstractBehaviour_t2 *)Component_GetComponentInChildren_TisObject_t_m646_gshared((Component_t128 *)__this, method)


// System.Boolean Vuforia.VideoBackgroundAbstractBehaviour::get_VideoBackGroundMirrored()
 bool VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___U3CVideoBackGroundMirroredU3Ek__BackingField_9);
		return L_0;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::set_VideoBackGroundMirrored(System.Boolean)
extern MethodInfo VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369_MethodInfo;
 void VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369 (VideoBackgroundAbstractBehaviour_t58 * __this, bool ___value, MethodInfo* method){
	{
		__this->___U3CVideoBackGroundMirroredU3Ek__BackingField_9 = ___value;
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ResetBackgroundPlane(System.Boolean)
extern MethodInfo VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4370_MethodInfo;
 void VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4370 (VideoBackgroundAbstractBehaviour_t58 * __this, bool ___disable, MethodInfo* method){
	MeshRenderer_t167 * V_0 = {0};
	MeshRenderer_t167 * V_1 = {0};
	Enumerator_t946  V_2 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t151 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t151 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		if (!___disable)
		{
			goto IL_0058;
		}
	}
	{
		BackgroundPlaneAbstractBehaviour_t2 * L_0 = (__this->___mBackgroundBehaviour_6);
		bool L_1 = Object_op_Inequality_m489(NULL /*static, unused*/, L_0, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_1)
		{
			goto IL_0048;
		}
	}
	{
		BackgroundPlaneAbstractBehaviour_t2 * L_2 = (__this->___mBackgroundBehaviour_6);
		NullCheck(L_2);
		MeshRenderer_t167 * L_3 = Component_GetComponent_TisMeshRenderer_t167_m484(L_2, /*hidden argument*/&Component_GetComponent_TisMeshRenderer_t167_m484_MethodInfo);
		V_0 = L_3;
		bool L_4 = Object_op_Inequality_m489(NULL /*static, unused*/, V_0, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_4)
		{
			goto IL_0048;
		}
	}
	{
		HashSet_1_t813 * L_5 = (__this->___mDisabledMeshRenderers_8);
		NullCheck(L_5);
		bool L_6 = (bool)VirtFuncInvoker1< bool, MeshRenderer_t167 * >::Invoke(&HashSet_1_Contains_m5542_MethodInfo, L_5, V_0);
		if (L_6)
		{
			goto IL_0048;
		}
	}
	{
		NullCheck(V_0);
		Renderer_set_enabled_m285(V_0, 0, /*hidden argument*/&Renderer_set_enabled_m285_MethodInfo);
		HashSet_1_t813 * L_7 = (__this->___mDisabledMeshRenderers_8);
		NullCheck(L_7);
		HashSet_1_Add_m5543(L_7, V_0, /*hidden argument*/&HashSet_1_Add_m5543_MethodInfo);
	}

IL_0048:
	{
		__this->___mClearBuffers_2 = ((int32_t)16);
		__this->___mSkipStateUpdates_3 = 5;
		return;
	}

IL_0058:
	{
		int32_t L_8 = (__this->___mSkipStateUpdates_3);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_9 = (__this->___mSkipStateUpdates_3);
		__this->___mSkipStateUpdates_3 = ((int32_t)(L_9-1));
	}

IL_006f:
	{
		int32_t L_10 = (__this->___mSkipStateUpdates_3);
		if (L_10)
		{
			goto IL_00b8;
		}
	}
	{
		HashSet_1_t813 * L_11 = (__this->___mDisabledMeshRenderers_8);
		NullCheck(L_11);
		Enumerator_t946  L_12 = HashSet_1_GetEnumerator_m5544(L_11, /*hidden argument*/&HashSet_1_GetEnumerator_m5544_MethodInfo);
		V_2 = L_12;
	}

IL_0083:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0094;
		}

IL_0085:
		{
			MeshRenderer_t167 * L_13 = Enumerator_get_Current_m5545((&V_2), /*hidden argument*/&Enumerator_get_Current_m5545_MethodInfo);
			V_1 = L_13;
			NullCheck(V_1);
			Renderer_set_enabled_m285(V_1, 1, /*hidden argument*/&Renderer_set_enabled_m285_MethodInfo);
		}

IL_0094:
		{
			bool L_14 = Enumerator_MoveNext_m5546((&V_2), /*hidden argument*/&Enumerator_MoveNext_m5546_MethodInfo);
			if (L_14)
			{
				goto IL_0085;
			}
		}

IL_009d:
		{
			// IL_009d: leave.s IL_00ad
			leaveInstructions[0] = 0xAD; // 1
			THROW_SENTINEL(IL_00ad);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_009f;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t151 *)e.ex;
		goto IL_009f;
	}

IL_009f:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t946_il2cpp_TypeInfo), &(*(&V_2))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m333_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t946_il2cpp_TypeInfo), &(*(&V_2))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0xAD:
				goto IL_00ad;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t151 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_00ad:
	{
		HashSet_1_t813 * L_15 = (__this->___mDisabledMeshRenderers_8);
		NullCheck(L_15);
		VirtActionInvoker0::Invoke(&HashSet_1_Clear_m5547_MethodInfo, L_15);
	}

IL_00b8:
	{
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::SetStereoDepth(System.Single)
extern MethodInfo VideoBackgroundAbstractBehaviour_SetStereoDepth_m4371_MethodInfo;
 void VideoBackgroundAbstractBehaviour_SetStereoDepth_m4371 (VideoBackgroundAbstractBehaviour_t58 * __this, float ___depth, MethodInfo* method){
	{
		Camera_t168 * L_0 = (__this->___mCamera_5);
		float L_1 = UnityCameraExtensions_GetMaxDepthForVideoBackground_m2797(NULL /*static, unused*/, L_0, /*hidden argument*/&UnityCameraExtensions_GetMaxDepthForVideoBackground_m2797_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t179_il2cpp_TypeInfo));
		float L_2 = Mathf_Min_m2650(NULL /*static, unused*/, ___depth, L_1, /*hidden argument*/&Mathf_Min_m2650_MethodInfo);
		Camera_t168 * L_3 = (__this->___mCamera_5);
		float L_4 = UnityCameraExtensions_GetMinDepthForVideoBackground_m2798(NULL /*static, unused*/, L_3, /*hidden argument*/&UnityCameraExtensions_GetMinDepthForVideoBackground_m2798_MethodInfo);
		float L_5 = Mathf_Max_m2593(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/&Mathf_Max_m2593_MethodInfo);
		___depth = L_5;
		float L_6 = (__this->___mStereoDepth_7);
		if ((((float)___depth) == ((float)L_6)))
		{
			goto IL_0039;
		}
	}
	{
		__this->___mStereoDepth_7 = ___depth;
		VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4372(__this, /*hidden argument*/&VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4372_MethodInfo);
	}

IL_0039:
	{
		BackgroundPlaneAbstractBehaviour_t2 * L_7 = (__this->___mBackgroundBehaviour_6);
		bool L_8 = Object_op_Inequality_m489(NULL /*static, unused*/, L_7, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_8)
		{
			goto IL_0053;
		}
	}
	{
		BackgroundPlaneAbstractBehaviour_t2 * L_9 = (__this->___mBackgroundBehaviour_6);
		NullCheck(L_9);
		BackgroundPlaneAbstractBehaviour_SetStereoDepth_m2777(L_9, ___depth, /*hidden argument*/&BackgroundPlaneAbstractBehaviour_SetStereoDepth_m2777_MethodInfo);
	}

IL_0053:
	{
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ApplyStereoDepthToMatrices()
 void VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4372 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method){
	Matrix4x4_t176  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Camera_t168 * L_0 = (__this->___mCamera_5);
		NullCheck(L_0);
		Matrix4x4_t176  L_1 = Camera_get_projectionMatrix_m823(L_0, /*hidden argument*/&Camera_get_projectionMatrix_m823_MethodInfo);
		V_0 = L_1;
		Transform_t74 * L_2 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_2);
		Vector3_t73  L_3 = Transform_get_localPosition_m830(L_2, /*hidden argument*/&Transform_get_localPosition_m830_MethodInfo);
		;
		float L_4 = (L_3.___x_1);
		V_1 = ((-L_4));
		float L_5 = Matrix4x4_get_Item_m5548((&V_0), 0, 0, /*hidden argument*/&Matrix4x4_get_Item_m5548_MethodInfo);
		V_2 = ((float)((float)(1.0f)/(float)L_5));
		float L_6 = (__this->___mStereoDepth_7);
		V_3 = ((float)((float)L_6*(float)V_2));
		V_4 = ((float)((float)V_1/(float)V_3));
		Matrix4x4_set_Item_m5549((&V_0), 0, 2, V_4, /*hidden argument*/&Matrix4x4_set_Item_m5549_MethodInfo);
		Camera_t168 * L_7 = (__this->___mCamera_5);
		NullCheck(L_7);
		Camera_set_projectionMatrix_m829(L_7, V_0, /*hidden argument*/&Camera_set_projectionMatrix_m829_MethodInfo);
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::RenderOnUpdate()
 void VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4373 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method){
	{
		QCARAbstractBehaviour_t45 * L_0 = (__this->___mQCARAbstractBehaviour_4);
		NullCheck(L_0);
		bool L_1 = QCARAbstractBehaviour_get_HasStarted_m4270(L_0, /*hidden argument*/&QCARAbstractBehaviour_get_HasStarted_m4270_MethodInfo);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARManager_t171_il2cpp_TypeInfo));
		QCARManager_t171 * L_2 = QCARManager_get_Instance_m520(NULL /*static, unused*/, /*hidden argument*/&QCARManager_get_Instance_m520_MethodInfo);
		NullCheck(((QCARManagerImpl_t702 *)Castclass(L_2, InitializedTypeInfo(&QCARManagerImpl_t702_il2cpp_TypeInfo))));
		QCARManagerImpl_StartRendering_m3161(((QCARManagerImpl_t702 *)Castclass(L_2, InitializedTypeInfo(&QCARManagerImpl_t702_il2cpp_TypeInfo))), /*hidden argument*/&QCARManagerImpl_StartRendering_m3161_MethodInfo);
	}

IL_001c:
	{
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::Awake()
extern MethodInfo VideoBackgroundAbstractBehaviour_Awake_m4374_MethodInfo;
 void VideoBackgroundAbstractBehaviour_Awake_m4374 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method){
	{
		Transform_t74 * L_0 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_0);
		Transform_t74 * L_1 = Transform_get_parent_m858(L_0, /*hidden argument*/&Transform_get_parent_m858_MethodInfo);
		NullCheck(L_1);
		QCARAbstractBehaviour_t45 * L_2 = Component_GetComponent_TisQCARAbstractBehaviour_t45_m5550(L_1, /*hidden argument*/&Component_GetComponent_TisQCARAbstractBehaviour_t45_m5550_MethodInfo);
		__this->___mQCARAbstractBehaviour_4 = L_2;
		QCARAbstractBehaviour_t45 * L_3 = (__this->___mQCARAbstractBehaviour_4);
		bool L_4 = Object_op_Inequality_m489(NULL /*static, unused*/, L_3, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		QCARAbstractBehaviour_t45 * L_5 = (__this->___mQCARAbstractBehaviour_4);
		IntPtr_t121 L_6 = { &VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4373_MethodInfo };
		Action_t147 * L_7 = (Action_t147 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t147_il2cpp_TypeInfo));
		Action__ctor_m4584(L_7, __this, L_6, /*hidden argument*/&Action__ctor_m4584_MethodInfo);
		NullCheck(L_5);
		QCARAbstractBehaviour_RegisterRenderOnUpdateCallback_m4299(L_5, L_7, /*hidden argument*/&QCARAbstractBehaviour_RegisterRenderOnUpdateCallback_m4299_MethodInfo);
	}

IL_003b:
	{
		Camera_t168 * L_8 = Component_GetComponent_TisCamera_t168_m822(__this, /*hidden argument*/&Component_GetComponent_TisCamera_t168_m822_MethodInfo);
		__this->___mCamera_5 = L_8;
		Camera_t168 * L_9 = (__this->___mCamera_5);
		float L_10 = UnityCameraExtensions_GetMaxDepthForVideoBackground_m2797(NULL /*static, unused*/, L_9, /*hidden argument*/&UnityCameraExtensions_GetMaxDepthForVideoBackground_m2797_MethodInfo);
		__this->___mStereoDepth_7 = L_10;
		BackgroundPlaneAbstractBehaviour_t2 * L_11 = Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t2_m4611(__this, /*hidden argument*/&Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t2_m4611_MethodInfo);
		__this->___mBackgroundBehaviour_6 = L_11;
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPreRender()
extern MethodInfo VideoBackgroundAbstractBehaviour_OnPreRender_m4375_MethodInfo;
 void VideoBackgroundAbstractBehaviour_OnPreRender_m4375 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method){
	{
		bool L_0 = VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368(__this, /*hidden argument*/&VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368_MethodInfo);
		GL_SetRevertBackfacing_m5551(NULL /*static, unused*/, L_0, /*hidden argument*/&GL_SetRevertBackfacing_m5551_MethodInfo);
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPostRender()
extern MethodInfo VideoBackgroundAbstractBehaviour_OnPostRender_m4376_MethodInfo;
 void VideoBackgroundAbstractBehaviour_OnPostRender_m4376 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method){
	{
		QCARAbstractBehaviour_t45 * L_0 = (__this->___mQCARAbstractBehaviour_4);
		NullCheck(L_0);
		bool L_1 = QCARAbstractBehaviour_get_HasStarted_m4270(L_0, /*hidden argument*/&QCARAbstractBehaviour_get_HasStarted_m4270_MethodInfo);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARManager_t171_il2cpp_TypeInfo));
		QCARManager_t171 * L_2 = QCARManager_get_Instance_m520(NULL /*static, unused*/, /*hidden argument*/&QCARManager_get_Instance_m520_MethodInfo);
		NullCheck(((QCARManagerImpl_t702 *)Castclass(L_2, InitializedTypeInfo(&QCARManagerImpl_t702_il2cpp_TypeInfo))));
		QCARManagerImpl_FinishRendering_m3162(((QCARManagerImpl_t702 *)Castclass(L_2, InitializedTypeInfo(&QCARManagerImpl_t702_il2cpp_TypeInfo))), /*hidden argument*/&QCARManagerImpl_FinishRendering_m3162_MethodInfo);
	}

IL_001c:
	{
		int32_t L_3 = (__this->___mClearBuffers_2);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0053;
		}
	}
	{
		Color_t66  L_4 = {0};
		Color__ctor_m683(&L_4, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/&Color__ctor_m683_MethodInfo);
		GL_Clear_m4740(NULL /*static, unused*/, 0, 1, L_4, /*hidden argument*/&GL_Clear_m4740_MethodInfo);
		int32_t L_5 = (__this->___mClearBuffers_2);
		__this->___mClearBuffers_2 = ((int32_t)(L_5-1));
	}

IL_0053:
	{
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnDestroy()
extern MethodInfo VideoBackgroundAbstractBehaviour_OnDestroy_m4377_MethodInfo;
 void VideoBackgroundAbstractBehaviour_OnDestroy_m4377 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method){
	{
		QCARAbstractBehaviour_t45 * L_0 = (__this->___mQCARAbstractBehaviour_4);
		bool L_1 = Object_op_Inequality_m489(NULL /*static, unused*/, L_0, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		QCARAbstractBehaviour_t45 * L_2 = (__this->___mQCARAbstractBehaviour_4);
		IntPtr_t121 L_3 = { &VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4373_MethodInfo };
		Action_t147 * L_4 = (Action_t147 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t147_il2cpp_TypeInfo));
		Action__ctor_m4584(L_4, __this, L_3, /*hidden argument*/&Action__ctor_m4584_MethodInfo);
		NullCheck(L_2);
		QCARAbstractBehaviour_UnregisterRenderOnUpdateCallback_m4300(L_2, L_4, /*hidden argument*/&QCARAbstractBehaviour_UnregisterRenderOnUpdateCallback_m4300_MethodInfo);
	}

IL_0025:
	{
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::.ctor()
extern MethodInfo VideoBackgroundAbstractBehaviour__ctor_m491_MethodInfo;
 void VideoBackgroundAbstractBehaviour__ctor_m491 (VideoBackgroundAbstractBehaviour_t58 * __this, MethodInfo* method){
	{
		HashSet_1_t813 * L_0 = (HashSet_1_t813 *)il2cpp_codegen_object_new (InitializedTypeInfo(&HashSet_1_t813_il2cpp_TypeInfo));
		HashSet_1__ctor_m5552(L_0, /*hidden argument*/&HashSet_1__ctor_m5552_MethodInfo);
		__this->___mDisabledMeshRenderers_8 = L_0;
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.VideoBackgroundAbstractBehaviour
extern Il2CppType Int32_t123_0_0_1;
FieldInfo VideoBackgroundAbstractBehaviour_t58____mClearBuffers_2_FieldInfo = 
{
	"mClearBuffers"/* name */
	, &Int32_t123_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t58, ___mClearBuffers_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo VideoBackgroundAbstractBehaviour_t58____mSkipStateUpdates_3_FieldInfo = 
{
	"mSkipStateUpdates"/* name */
	, &Int32_t123_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t58, ___mSkipStateUpdates_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType QCARAbstractBehaviour_t45_0_0_1;
FieldInfo VideoBackgroundAbstractBehaviour_t58____mQCARAbstractBehaviour_4_FieldInfo = 
{
	"mQCARAbstractBehaviour"/* name */
	, &QCARAbstractBehaviour_t45_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t58, ___mQCARAbstractBehaviour_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Camera_t168_0_0_1;
FieldInfo VideoBackgroundAbstractBehaviour_t58____mCamera_5_FieldInfo = 
{
	"mCamera"/* name */
	, &Camera_t168_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t58, ___mCamera_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t2_0_0_1;
FieldInfo VideoBackgroundAbstractBehaviour_t58____mBackgroundBehaviour_6_FieldInfo = 
{
	"mBackgroundBehaviour"/* name */
	, &BackgroundPlaneAbstractBehaviour_t2_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t58, ___mBackgroundBehaviour_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_1;
FieldInfo VideoBackgroundAbstractBehaviour_t58____mStereoDepth_7_FieldInfo = 
{
	"mStereoDepth"/* name */
	, &Single_t170_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t58, ___mStereoDepth_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType HashSet_1_t813_0_0_1;
FieldInfo VideoBackgroundAbstractBehaviour_t58____mDisabledMeshRenderers_8_FieldInfo = 
{
	"mDisabledMeshRenderers"/* name */
	, &HashSet_1_t813_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t58, ___mDisabledMeshRenderers_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
extern CustomAttributesCache VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache_U3CVideoBackGroundMirroredU3Ek__BackingField;
FieldInfo VideoBackgroundAbstractBehaviour_t58____U3CVideoBackGroundMirroredU3Ek__BackingField_9_FieldInfo = 
{
	"<VideoBackGroundMirrored>k__BackingField"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t58, ___U3CVideoBackGroundMirroredU3Ek__BackingField_9)/* data */
	, &VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache_U3CVideoBackGroundMirroredU3Ek__BackingField/* custom_attributes_cache */

};
static FieldInfo* VideoBackgroundAbstractBehaviour_t58_FieldInfos[] =
{
	&VideoBackgroundAbstractBehaviour_t58____mClearBuffers_2_FieldInfo,
	&VideoBackgroundAbstractBehaviour_t58____mSkipStateUpdates_3_FieldInfo,
	&VideoBackgroundAbstractBehaviour_t58____mQCARAbstractBehaviour_4_FieldInfo,
	&VideoBackgroundAbstractBehaviour_t58____mCamera_5_FieldInfo,
	&VideoBackgroundAbstractBehaviour_t58____mBackgroundBehaviour_6_FieldInfo,
	&VideoBackgroundAbstractBehaviour_t58____mStereoDepth_7_FieldInfo,
	&VideoBackgroundAbstractBehaviour_t58____mDisabledMeshRenderers_8_FieldInfo,
	&VideoBackgroundAbstractBehaviour_t58____U3CVideoBackGroundMirroredU3Ek__BackingField_9_FieldInfo,
	NULL
};
static PropertyInfo VideoBackgroundAbstractBehaviour_t58____VideoBackGroundMirrored_PropertyInfo = 
{
	&VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* parent */
	, "VideoBackGroundMirrored"/* name */
	, &VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368_MethodInfo/* get */
	, &VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* VideoBackgroundAbstractBehaviour_t58_PropertyInfos[] =
{
	&VideoBackgroundAbstractBehaviour_t58____VideoBackGroundMirrored_PropertyInfo,
	NULL
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368;
// System.Boolean Vuforia.VideoBackgroundAbstractBehaviour::get_VideoBackGroundMirrored()
MethodInfo VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368_MethodInfo = 
{
	"get_VideoBackGroundMirrored"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368/* method */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2413/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo VideoBackgroundAbstractBehaviour_t58_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369_ParameterInfos[] = 
{
	{"value", 0, 134220031, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::set_VideoBackGroundMirrored(System.Boolean)
MethodInfo VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369_MethodInfo = 
{
	"set_VideoBackGroundMirrored"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369/* method */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125/* invoker_method */
	, VideoBackgroundAbstractBehaviour_t58_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369_ParameterInfos/* parameters */
	, &VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2414/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo VideoBackgroundAbstractBehaviour_t58_VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4370_ParameterInfos[] = 
{
	{"disable", 0, 134220032, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ResetBackgroundPlane(System.Boolean)
MethodInfo VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4370_MethodInfo = 
{
	"ResetBackgroundPlane"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4370/* method */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125/* invoker_method */
	, VideoBackgroundAbstractBehaviour_t58_VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4370_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2415/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t170_0_0_0;
static ParameterInfo VideoBackgroundAbstractBehaviour_t58_VideoBackgroundAbstractBehaviour_SetStereoDepth_m4371_ParameterInfos[] = 
{
	{"depth", 0, 134220033, &EmptyCustomAttributesCache, &Single_t170_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Single_t170 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::SetStereoDepth(System.Single)
MethodInfo VideoBackgroundAbstractBehaviour_SetStereoDepth_m4371_MethodInfo = 
{
	"SetStereoDepth"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_SetStereoDepth_m4371/* method */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Single_t170/* invoker_method */
	, VideoBackgroundAbstractBehaviour_t58_VideoBackgroundAbstractBehaviour_SetStereoDepth_m4371_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2416/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ApplyStereoDepthToMatrices()
MethodInfo VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4372_MethodInfo = 
{
	"ApplyStereoDepthToMatrices"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4372/* method */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2417/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::RenderOnUpdate()
MethodInfo VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4373_MethodInfo = 
{
	"RenderOnUpdate"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4373/* method */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2418/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::Awake()
MethodInfo VideoBackgroundAbstractBehaviour_Awake_m4374_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_Awake_m4374/* method */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2419/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPreRender()
MethodInfo VideoBackgroundAbstractBehaviour_OnPreRender_m4375_MethodInfo = 
{
	"OnPreRender"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_OnPreRender_m4375/* method */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2420/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPostRender()
MethodInfo VideoBackgroundAbstractBehaviour_OnPostRender_m4376_MethodInfo = 
{
	"OnPostRender"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_OnPostRender_m4376/* method */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2421/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnDestroy()
MethodInfo VideoBackgroundAbstractBehaviour_OnDestroy_m4377_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_OnDestroy_m4377/* method */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2422/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::.ctor()
MethodInfo VideoBackgroundAbstractBehaviour__ctor_m491_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour__ctor_m491/* method */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2423/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* VideoBackgroundAbstractBehaviour_t58_MethodInfos[] =
{
	&VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368_MethodInfo,
	&VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369_MethodInfo,
	&VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4370_MethodInfo,
	&VideoBackgroundAbstractBehaviour_SetStereoDepth_m4371_MethodInfo,
	&VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4372_MethodInfo,
	&VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4373_MethodInfo,
	&VideoBackgroundAbstractBehaviour_Awake_m4374_MethodInfo,
	&VideoBackgroundAbstractBehaviour_OnPreRender_m4375_MethodInfo,
	&VideoBackgroundAbstractBehaviour_OnPostRender_m4376_MethodInfo,
	&VideoBackgroundAbstractBehaviour_OnDestroy_m4377_MethodInfo,
	&VideoBackgroundAbstractBehaviour__ctor_m491_MethodInfo,
	NULL
};
static MethodInfo* VideoBackgroundAbstractBehaviour_t58_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
};
extern TypeInfo RequireComponent_t154_il2cpp_TypeInfo;
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
extern MethodInfo RequireComponent__ctor_m348_MethodInfo;
extern TypeInfo Camera_t168_il2cpp_TypeInfo;
void VideoBackgroundAbstractBehaviour_t58_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t154 * tmp;
		tmp = (RequireComponent_t154 *)il2cpp_codegen_object_new (&RequireComponent_t154_il2cpp_TypeInfo);
		RequireComponent__ctor_m348(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&Camera_t168_il2cpp_TypeInfo)), &RequireComponent__ctor_m348_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo CompilerGeneratedAttribute_t202_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern MethodInfo CompilerGeneratedAttribute__ctor_m701_MethodInfo;
void VideoBackgroundAbstractBehaviour_t58_CustomAttributesCacheGenerator_U3CVideoBackGroundMirroredU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void VideoBackgroundAbstractBehaviour_t58_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void VideoBackgroundAbstractBehaviour_t58_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache = {
1,
NULL,
&VideoBackgroundAbstractBehaviour_t58_CustomAttributesCacheGenerator
};
CustomAttributesCache VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache_U3CVideoBackGroundMirroredU3Ek__BackingField = {
1,
NULL,
&VideoBackgroundAbstractBehaviour_t58_CustomAttributesCacheGenerator_U3CVideoBackGroundMirroredU3Ek__BackingField
};
CustomAttributesCache VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368 = {
1,
NULL,
&VideoBackgroundAbstractBehaviour_t58_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368
};
CustomAttributesCache VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369 = {
1,
NULL,
&VideoBackgroundAbstractBehaviour_t58_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType VideoBackgroundAbstractBehaviour_t58_0_0_0;
extern Il2CppType VideoBackgroundAbstractBehaviour_t58_1_0_0;
struct VideoBackgroundAbstractBehaviour_t58;
extern CustomAttributesCache VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache;
extern CustomAttributesCache VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache_U3CVideoBackGroundMirroredU3Ek__BackingField;
extern CustomAttributesCache VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4368;
extern CustomAttributesCache VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4369;
TypeInfo VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "VideoBackgroundAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VideoBackgroundAbstractBehaviour_t58_MethodInfos/* methods */
	, VideoBackgroundAbstractBehaviour_t58_PropertyInfos/* properties */
	, VideoBackgroundAbstractBehaviour_t58_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, VideoBackgroundAbstractBehaviour_t58_VTable/* vtable */
	, &VideoBackgroundAbstractBehaviour_t58__CustomAttributeCache/* custom_attributes_cache */
	, &VideoBackgroundAbstractBehaviour_t58_il2cpp_TypeInfo/* cast_class */
	, &VideoBackgroundAbstractBehaviour_t58_0_0_0/* byval_arg */
	, &VideoBackgroundAbstractBehaviour_t58_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VideoBackgroundAbstractBehaviour_t58)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 11/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.VideoTextureRendererAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendere.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo;
// Vuforia.VideoTextureRendererAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendereMethodDeclarations.h"

// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterMode.h"
// UnityEngine.TextureWrapMode
#include "UnityEngine_UnityEngine_TextureWrapMode.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// Vuforia.QCARRenderer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer.h"
extern TypeInfo Texture2D_t196_il2cpp_TypeInfo;
extern TypeInfo BackgroundPlaneAbstractBehaviour_t2_il2cpp_TypeInfo;
extern TypeInfo BackgroundPlaneAbstractBehaviourU5BU5D_t938_il2cpp_TypeInfo;
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
extern Il2CppType BackgroundPlaneAbstractBehaviour_t2_0_0_0;
extern MethodInfo Texture2D__ctor_m5553_MethodInfo;
extern MethodInfo Texture_set_filterMode_m5554_MethodInfo;
extern MethodInfo Texture_set_wrapMode_m5555_MethodInfo;
extern MethodInfo Texture_GetNativeTextureID_m5556_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_RegisterVideoBgEventHandler_m4290_MethodInfo;
extern MethodInfo GameObject_GetComponent_TisRenderer_t129_m4609_MethodInfo;
extern MethodInfo Renderer_get_material_m5437_MethodInfo;
extern MethodInfo Material_set_mainTexture_m654_MethodInfo;
extern MethodInfo QCARRenderer_get_Instance_m3175_MethodInfo;
extern MethodInfo QCARRenderer_get_VideoBackgroundTexture_m5029_MethodInfo;
extern MethodInfo QCARRenderer_SetVideoBackgroundTexture_m5032_MethodInfo;
extern MethodInfo String_Concat_m2077_MethodInfo;
extern MethodInfo Debug_LogWarning_m4964_MethodInfo;
extern MethodInfo Object_op_Equality_m524_MethodInfo;
struct GameObject_t29;
// UnityEngine.CastHelper`1<UnityEngine.Renderer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_2.h"
struct GameObject_t29;
// Declaration !!0 UnityEngine.GameObject::GetComponent<System.Object>()
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
 Object_t * GameObject_GetComponent_TisObject_t_m564_gshared (GameObject_t29 * __this, MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m564(__this, method) (Object_t *)GameObject_GetComponent_TisObject_t_m564_gshared((GameObject_t29 *)__this, method)
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t129_m4609(__this, method) (Renderer_t129 *)GameObject_GetComponent_TisObject_t_m564_gshared((GameObject_t29 *)__this, method)


// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Awake()
extern MethodInfo VideoTextureRendererAbstractBehaviour_Awake_m4378_MethodInfo;
 void VideoTextureRendererAbstractBehaviour_Awake_m4378 (VideoTextureRendererAbstractBehaviour_t60 * __this, MethodInfo* method){
	{
		Texture2D_t196 * L_0 = (Texture2D_t196 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Texture2D_t196_il2cpp_TypeInfo));
		Texture2D__ctor_m5553(L_0, 0, 0, 7, 0, /*hidden argument*/&Texture2D__ctor_m5553_MethodInfo);
		__this->___mTexture_2 = L_0;
		Texture2D_t196 * L_1 = (__this->___mTexture_2);
		NullCheck(L_1);
		Texture_set_filterMode_m5554(L_1, 1, /*hidden argument*/&Texture_set_filterMode_m5554_MethodInfo);
		Texture2D_t196 * L_2 = (__this->___mTexture_2);
		NullCheck(L_2);
		Texture_set_wrapMode_m5555(L_2, 1, /*hidden argument*/&Texture_set_wrapMode_m5555_MethodInfo);
		Texture2D_t196 * L_3 = (__this->___mTexture_2);
		NullCheck(L_3);
		int32_t L_4 = Texture_GetNativeTextureID_m5556(L_3, /*hidden argument*/&Texture_GetNativeTextureID_m5556_MethodInfo);
		__this->___mNativeTextureID_5 = L_4;
		return;
	}
}
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Start()
extern MethodInfo VideoTextureRendererAbstractBehaviour_Start_m4379_MethodInfo;
 void VideoTextureRendererAbstractBehaviour_Start_m4379 (VideoTextureRendererAbstractBehaviour_t60 * __this, MethodInfo* method){
	QCARAbstractBehaviour_t45 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&QCARAbstractBehaviour_t45_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		Object_t120 * L_1 = Object_FindObjectOfType_m256(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_FindObjectOfType_m256_MethodInfo);
		V_0 = ((QCARAbstractBehaviour_t45 *)Castclass(L_1, InitializedTypeInfo(&QCARAbstractBehaviour_t45_il2cpp_TypeInfo)));
		bool L_2 = Object_op_Implicit_m257(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m257_MethodInfo);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterVideoBgEventHandler_m4290(V_0, __this, /*hidden argument*/&QCARAbstractBehaviour_RegisterVideoBgEventHandler_m4290_MethodInfo);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Update()
extern MethodInfo VideoTextureRendererAbstractBehaviour_Update_m4380_MethodInfo;
 void VideoTextureRendererAbstractBehaviour_Update_m4380 (VideoTextureRendererAbstractBehaviour_t60 * __this, MethodInfo* method){
	BackgroundPlaneAbstractBehaviourU5BU5D_t938* V_0 = {0};
	BackgroundPlaneAbstractBehaviour_t2 * V_1 = {0};
	bool V_2 = false;
	Texture2D_t196 * V_3 = {0};
	BackgroundPlaneAbstractBehaviourU5BU5D_t938* V_4 = {0};
	int32_t V_5 = 0;
	{
		bool L_0 = (__this->___mVideoBgConfigChanged_3);
		if (!L_0)
		{
			goto IL_00d4;
		}
	}
	{
		bool L_1 = (__this->___mTextureAppliedToMaterial_4);
		if (L_1)
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_2 = Type_GetTypeFromHandle_m255(NULL /*static, unused*/, LoadTypeToken(&BackgroundPlaneAbstractBehaviour_t2_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m255_MethodInfo);
		ObjectU5BU5D_t227* L_3 = Object_FindObjectsOfType_m812(NULL /*static, unused*/, L_2, /*hidden argument*/&Object_FindObjectsOfType_m812_MethodInfo);
		V_0 = ((BackgroundPlaneAbstractBehaviourU5BU5D_t938*)Castclass(L_3, InitializedTypeInfo(&BackgroundPlaneAbstractBehaviourU5BU5D_t938_il2cpp_TypeInfo)));
		V_4 = V_0;
		V_5 = 0;
		goto IL_0057;
	}

IL_0030:
	{
		NullCheck(V_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_4, V_5);
		int32_t L_4 = V_5;
		V_1 = (*(BackgroundPlaneAbstractBehaviour_t2 **)(BackgroundPlaneAbstractBehaviour_t2 **)SZArrayLdElema(V_4, L_4));
		NullCheck(V_1);
		GameObject_t29 * L_5 = Component_get_gameObject_m419(V_1, /*hidden argument*/&Component_get_gameObject_m419_MethodInfo);
		NullCheck(L_5);
		Renderer_t129 * L_6 = GameObject_GetComponent_TisRenderer_t129_m4609(L_5, /*hidden argument*/&GameObject_GetComponent_TisRenderer_t129_m4609_MethodInfo);
		NullCheck(L_6);
		Material_t64 * L_7 = Renderer_get_material_m5437(L_6, /*hidden argument*/&Renderer_get_material_m5437_MethodInfo);
		Texture2D_t196 * L_8 = (__this->___mTexture_2);
		NullCheck(L_7);
		Material_set_mainTexture_m654(L_7, L_8, /*hidden argument*/&Material_set_mainTexture_m654_MethodInfo);
		V_5 = ((int32_t)(V_5+1));
	}

IL_0057:
	{
		NullCheck(V_4);
		if ((((int32_t)V_5) < ((int32_t)(((int32_t)(((Array_t *)V_4)->max_length))))))
		{
			goto IL_0030;
		}
	}
	{
		__this->___mTextureAppliedToMaterial_4 = 1;
	}

IL_0066:
	{
		V_2 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRenderer_t706_il2cpp_TypeInfo));
		QCARRenderer_t706 * L_9 = QCARRenderer_get_Instance_m3175(NULL /*static, unused*/, /*hidden argument*/&QCARRenderer_get_Instance_m3175_MethodInfo);
		NullCheck(L_9);
		Texture2D_t196 * L_10 = (Texture2D_t196 *)VirtFuncInvoker0< Texture2D_t196 * >::Invoke(&QCARRenderer_get_VideoBackgroundTexture_m5029_MethodInfo, L_9);
		V_3 = L_10;
		bool L_11 = Object_op_Inequality_m489(NULL /*static, unused*/, V_3, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_11)
		{
			goto IL_008c;
		}
	}
	{
		Texture2D_t196 * L_12 = (__this->___mTexture_2);
		bool L_13 = Object_op_Inequality_m489(NULL /*static, unused*/, V_3, L_12, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_13)
		{
			goto IL_008c;
		}
	}
	{
		V_2 = 1;
	}

IL_008c:
	{
		if (V_2)
		{
			goto IL_00c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRenderer_t706_il2cpp_TypeInfo));
		QCARRenderer_t706 * L_14 = QCARRenderer_get_Instance_m3175(NULL /*static, unused*/, /*hidden argument*/&QCARRenderer_get_Instance_m3175_MethodInfo);
		Texture2D_t196 * L_15 = (__this->___mTexture_2);
		int32_t L_16 = (__this->___mNativeTextureID_5);
		NullCheck(L_14);
		bool L_17 = (bool)VirtFuncInvoker2< bool, Texture2D_t196 *, int32_t >::Invoke(&QCARRenderer_SetVideoBackgroundTexture_m5032_MethodInfo, L_14, L_15, L_16);
		if (L_17)
		{
			goto IL_00cd;
		}
	}
	{
		int32_t L_18 = (__this->___mNativeTextureID_5);
		int32_t L_19 = L_18;
		Object_t * L_20 = Box(InitializedTypeInfo(&Int32_t123_il2cpp_TypeInfo), &L_19);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_21 = String_Concat_m2077(NULL /*static, unused*/, (String_t*) &_stringLiteral317, L_20, /*hidden argument*/&String_Concat_m2077_MethodInfo);
		Debug_Log_m288(NULL /*static, unused*/, L_21, /*hidden argument*/&Debug_Log_m288_MethodInfo);
		goto IL_00cd;
	}

IL_00c3:
	{
		Debug_LogWarning_m4964(NULL /*static, unused*/, (String_t*) &_stringLiteral318, /*hidden argument*/&Debug_LogWarning_m4964_MethodInfo);
	}

IL_00cd:
	{
		__this->___mVideoBgConfigChanged_3 = 0;
	}

IL_00d4:
	{
		return;
	}
}
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnDestroy()
extern MethodInfo VideoTextureRendererAbstractBehaviour_OnDestroy_m4381_MethodInfo;
 void VideoTextureRendererAbstractBehaviour_OnDestroy_m4381 (VideoTextureRendererAbstractBehaviour_t60 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRenderer_t706_il2cpp_TypeInfo));
		QCARRenderer_t706 * L_0 = QCARRenderer_get_Instance_m3175(NULL /*static, unused*/, /*hidden argument*/&QCARRenderer_get_Instance_m3175_MethodInfo);
		NullCheck(L_0);
		Texture2D_t196 * L_1 = (Texture2D_t196 *)VirtFuncInvoker0< Texture2D_t196 * >::Invoke(&QCARRenderer_get_VideoBackgroundTexture_m5029_MethodInfo, L_0);
		Texture2D_t196 * L_2 = (__this->___mTexture_2);
		bool L_3 = Object_op_Equality_m524(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/&Object_op_Equality_m524_MethodInfo);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRenderer_t706_il2cpp_TypeInfo));
		QCARRenderer_t706 * L_4 = QCARRenderer_get_Instance_m3175(NULL /*static, unused*/, /*hidden argument*/&QCARRenderer_get_Instance_m3175_MethodInfo);
		NullCheck(L_4);
		VirtFuncInvoker2< bool, Texture2D_t196 *, int32_t >::Invoke(&QCARRenderer_SetVideoBackgroundTexture_m5032_MethodInfo, L_4, (Texture2D_t196 *)NULL, 0);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern MethodInfo VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m493_MethodInfo;
 void VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m493 (VideoTextureRendererAbstractBehaviour_t60 * __this, MethodInfo* method){
	{
		__this->___mVideoBgConfigChanged_3 = 1;
		return;
	}
}
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::.ctor()
extern MethodInfo VideoTextureRendererAbstractBehaviour__ctor_m492_MethodInfo;
 void VideoTextureRendererAbstractBehaviour__ctor_m492 (VideoTextureRendererAbstractBehaviour_t60 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.VideoTextureRendererAbstractBehaviour
extern Il2CppType Texture2D_t196_0_0_1;
FieldInfo VideoTextureRendererAbstractBehaviour_t60____mTexture_2_FieldInfo = 
{
	"mTexture"/* name */
	, &Texture2D_t196_0_0_1/* type */
	, &VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo/* parent */
	, offsetof(VideoTextureRendererAbstractBehaviour_t60, ___mTexture_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo VideoTextureRendererAbstractBehaviour_t60____mVideoBgConfigChanged_3_FieldInfo = 
{
	"mVideoBgConfigChanged"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo/* parent */
	, offsetof(VideoTextureRendererAbstractBehaviour_t60, ___mVideoBgConfigChanged_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo VideoTextureRendererAbstractBehaviour_t60____mTextureAppliedToMaterial_4_FieldInfo = 
{
	"mTextureAppliedToMaterial"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo/* parent */
	, offsetof(VideoTextureRendererAbstractBehaviour_t60, ___mTextureAppliedToMaterial_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo VideoTextureRendererAbstractBehaviour_t60____mNativeTextureID_5_FieldInfo = 
{
	"mNativeTextureID"/* name */
	, &Int32_t123_0_0_1/* type */
	, &VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo/* parent */
	, offsetof(VideoTextureRendererAbstractBehaviour_t60, ___mNativeTextureID_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* VideoTextureRendererAbstractBehaviour_t60_FieldInfos[] =
{
	&VideoTextureRendererAbstractBehaviour_t60____mTexture_2_FieldInfo,
	&VideoTextureRendererAbstractBehaviour_t60____mVideoBgConfigChanged_3_FieldInfo,
	&VideoTextureRendererAbstractBehaviour_t60____mTextureAppliedToMaterial_4_FieldInfo,
	&VideoTextureRendererAbstractBehaviour_t60____mNativeTextureID_5_FieldInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Awake()
MethodInfo VideoTextureRendererAbstractBehaviour_Awake_m4378_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_Awake_m4378/* method */
	, &VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2424/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Start()
MethodInfo VideoTextureRendererAbstractBehaviour_Start_m4379_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_Start_m4379/* method */
	, &VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2425/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Update()
MethodInfo VideoTextureRendererAbstractBehaviour_Update_m4380_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_Update_m4380/* method */
	, &VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2426/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnDestroy()
MethodInfo VideoTextureRendererAbstractBehaviour_OnDestroy_m4381_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_OnDestroy_m4381/* method */
	, &VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnVideoBackgroundConfigChanged()
MethodInfo VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m493_MethodInfo = 
{
	"OnVideoBackgroundConfigChanged"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m493/* method */
	, &VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::.ctor()
MethodInfo VideoTextureRendererAbstractBehaviour__ctor_m492_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour__ctor_m492/* method */
	, &VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* VideoTextureRendererAbstractBehaviour_t60_MethodInfos[] =
{
	&VideoTextureRendererAbstractBehaviour_Awake_m4378_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_Start_m4379_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_Update_m4380_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_OnDestroy_m4381_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m493_MethodInfo,
	&VideoTextureRendererAbstractBehaviour__ctor_m492_MethodInfo,
	NULL
};
static MethodInfo* VideoTextureRendererAbstractBehaviour_t60_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m493_MethodInfo,
};
extern TypeInfo IVideoBackgroundEventHandler_t112_il2cpp_TypeInfo;
static TypeInfo* VideoTextureRendererAbstractBehaviour_t60_InterfacesTypeInfos[] = 
{
	&IVideoBackgroundEventHandler_t112_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair VideoTextureRendererAbstractBehaviour_t60_InterfacesOffsets[] = 
{
	{ &IVideoBackgroundEventHandler_t112_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType VideoTextureRendererAbstractBehaviour_t60_0_0_0;
extern Il2CppType VideoTextureRendererAbstractBehaviour_t60_1_0_0;
struct VideoTextureRendererAbstractBehaviour_t60;
TypeInfo VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "VideoTextureRendererAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VideoTextureRendererAbstractBehaviour_t60_MethodInfos/* methods */
	, NULL/* properties */
	, VideoTextureRendererAbstractBehaviour_t60_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo/* element_class */
	, VideoTextureRendererAbstractBehaviour_t60_InterfacesTypeInfos/* implemented_interfaces */
	, VideoTextureRendererAbstractBehaviour_t60_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &VideoTextureRendererAbstractBehaviour_t60_il2cpp_TypeInfo/* cast_class */
	, &VideoTextureRendererAbstractBehaviour_t60_0_0_0/* byval_arg */
	, &VideoTextureRendererAbstractBehaviour_t60_1_0_0/* this_arg */
	, VideoTextureRendererAbstractBehaviour_t60_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VideoTextureRendererAbstractBehaviour_t60)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 6/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo;
// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstraMethodDeclarations.h"

// Vuforia.VirtualButton
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton.h"
// Vuforia.VirtualButton/Sensitivity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton_Sensi.h"
// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_47.h"
// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstract.h"
// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_22.h"
extern TypeInfo List_1_t814_il2cpp_TypeInfo;
extern TypeInfo IVirtualButtonEventHandler_t815_il2cpp_TypeInfo;
extern TypeInfo RectangleData_t632_il2cpp_TypeInfo;
extern TypeInfo VirtualButton_t639_il2cpp_TypeInfo;
extern TypeInfo Sensitivity_t789_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t947_il2cpp_TypeInfo;
extern TypeInfo ImageTarget_t616_il2cpp_TypeInfo;
// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_47MethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// Vuforia.VirtualButton
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_22MethodDeclarations.h"
// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstractMethodDeclarations.h"
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
extern MethodInfo Matrix4x4_get_zero_m5557_MethodInfo;
extern MethodInfo List_1__ctor_m5558_MethodInfo;
extern MethodInfo List_1_Add_m5559_MethodInfo;
extern MethodInfo List_1_Remove_m5560_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4391_MethodInfo;
extern MethodInfo Vector2_get_zero_m665_MethodInfo;
extern MethodInfo Transform_get_position_m538_MethodInfo;
extern MethodInfo Transform_InverseTransformPoint_m622_MethodInfo;
extern MethodInfo Transform_get_lossyScale_m540_MethodInfo;
extern MethodInfo Vector3_get_Item_m841_MethodInfo;
extern MethodInfo Vector2_op_Multiply_m808_MethodInfo;
extern MethodInfo Vector2_Scale_m2511_MethodInfo;
extern MethodInfo Vector2_op_Subtraction_m688_MethodInfo;
extern MethodInfo Vector2_op_Addition_m2391_MethodInfo;
extern MethodInfo VirtualButton_SetArea_m5453_MethodInfo;
extern MethodInfo VirtualButton_SetSensitivity_m5454_MethodInfo;
extern MethodInfo Behaviour_get_enabled_m536_MethodInfo;
extern MethodInfo VirtualButton_SetEnabled_m5455_MethodInfo;
extern MethodInfo Transform_get_localScale_m2501_MethodInfo;
extern MethodInfo Object_get_name_m2144_MethodInfo;
extern MethodInfo String_Concat_m287_MethodInfo;
extern MethodInfo Vector3__ctor_m568_MethodInfo;
extern MethodInfo Transform_set_localScale_m2507_MethodInfo;
extern MethodInfo Vector3_get_zero_m807_MethodInfo;
extern MethodInfo Transform_set_localPosition_m832_MethodInfo;
extern MethodInfo Transform_TransformPoint_m628_MethodInfo;
extern MethodInfo Transform_set_position_m570_MethodInfo;
extern MethodInfo Transform_get_rotation_m539_MethodInfo;
extern MethodInfo Transform_set_rotation_m617_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_CalculateButtonArea_m4386_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Equals_m4395_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_UpdateEnabled_m4389_MethodInfo;
extern MethodInfo List_1_GetEnumerator_m5561_MethodInfo;
extern MethodInfo Enumerator_get_Current_m5562_MethodInfo;
extern MethodInfo IVirtualButtonEventHandler_OnButtonPressed_m5471_MethodInfo;
extern MethodInfo Enumerator_MoveNext_m5563_MethodInfo;
extern MethodInfo IVirtualButtonEventHandler_OnButtonReleased_m5472_MethodInfo;
extern MethodInfo GameObject_GetComponent_TisImageTargetAbstractBehaviour_t23_m5564_MethodInfo;
extern MethodInfo GameObject_get_transform_m537_MethodInfo;
extern MethodInfo Vector2_get_Item_m2393_MethodInfo;
extern MethodInfo Vector3_op_Division_m630_MethodInfo;
extern MethodInfo Component_GetComponent_TisRenderer_t129_m362_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_UpdatePose_m508_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4387_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_UpdateSensitivity_m4388_MethodInfo;
extern MethodInfo Application_get_isPlaying_m2529_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_get_ImageTarget_m4246_MethodInfo;
extern MethodInfo ImageTarget_DestroyVirtualButton_m4728_MethodInfo;
extern MethodInfo Math_Abs_m4686_MethodInfo;
struct GameObject_t29;
// UnityEngine.CastHelper`1<Vuforia.ImageTargetAbstractBehaviour>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_35.h"
// Declaration !!0 UnityEngine.GameObject::GetComponent<Vuforia.ImageTargetAbstractBehaviour>()
// !!0 UnityEngine.GameObject::GetComponent<Vuforia.ImageTargetAbstractBehaviour>()
#define GameObject_GetComponent_TisImageTargetAbstractBehaviour_t23_m5564(__this, method) (ImageTargetAbstractBehaviour_t23 *)GameObject_GetComponent_TisObject_t_m564_gshared((GameObject_t29 *)__this, method)
struct Component_t128;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t129_m362(__this, method) (Renderer_t129 *)Component_GetComponent_TisObject_t_m280_gshared((Component_t128 *)__this, method)


// System.String Vuforia.VirtualButtonAbstractBehaviour::get_VirtualButtonName()
extern MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButtonName_m495_MethodInfo;
 String_t* VirtualButtonAbstractBehaviour_get_VirtualButtonName_m495 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mName_3);
		return L_0;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_Pressed()
extern MethodInfo VirtualButtonAbstractBehaviour_get_Pressed_m4382_MethodInfo;
 bool VirtualButtonAbstractBehaviour_get_Pressed_m4382 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mPressed_10);
		return L_0;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_HasUpdatedPose()
extern MethodInfo VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m507_MethodInfo;
 bool VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m507 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mHasUpdatedPose_5);
		return L_0;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_UnregisterOnDestroy()
extern MethodInfo VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m505_MethodInfo;
 bool VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m505 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mUnregisterOnDestroy_14);
		return L_0;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::set_UnregisterOnDestroy(System.Boolean)
extern MethodInfo VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m506_MethodInfo;
 void VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m506 (VirtualButtonAbstractBehaviour_t30 * __this, bool ___value, MethodInfo* method){
	{
		__this->___mUnregisterOnDestroy_14 = ___value;
		return;
	}
}
// Vuforia.VirtualButton Vuforia.VirtualButtonAbstractBehaviour::get_VirtualButton()
extern MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButton_m4383_MethodInfo;
 VirtualButton_t639 * VirtualButtonAbstractBehaviour_get_VirtualButton_m4383 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		VirtualButton_t639 * L_0 = (__this->___mVirtualButton_15);
		return L_0;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::.ctor()
extern MethodInfo VirtualButtonAbstractBehaviour__ctor_m494_MethodInfo;
 void VirtualButtonAbstractBehaviour__ctor_m494 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		Matrix4x4_t176  L_0 = Matrix4x4_get_zero_m5557(NULL /*static, unused*/, /*hidden argument*/&Matrix4x4_get_zero_m5557_MethodInfo);
		__this->___mPrevTransform_6 = L_0;
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		__this->___mName_3 = (String_t*) &_stringLiteral141;
		__this->___mPressed_10 = 0;
		__this->___mSensitivity_4 = 2;
		__this->___mSensitivityDirty_8 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&List_1_t814_il2cpp_TypeInfo));
		List_1_t814 * L_1 = (List_1_t814 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t814_il2cpp_TypeInfo));
		List_1__ctor_m5558(L_1, /*hidden argument*/&List_1__ctor_m5558_MethodInfo);
		__this->___mHandlers_11 = L_1;
		__this->___mHasUpdatedPose_5 = 0;
		return;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::RegisterEventHandler(Vuforia.IVirtualButtonEventHandler)
extern MethodInfo VirtualButtonAbstractBehaviour_RegisterEventHandler_m4384_MethodInfo;
 void VirtualButtonAbstractBehaviour_RegisterEventHandler_m4384 (VirtualButtonAbstractBehaviour_t30 * __this, Object_t * ___eventHandler, MethodInfo* method){
	{
		List_1_t814 * L_0 = (__this->___mHandlers_11);
		NullCheck(L_0);
		VirtActionInvoker1< Object_t * >::Invoke(&List_1_Add_m5559_MethodInfo, L_0, ___eventHandler);
		return;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UnregisterEventHandler(Vuforia.IVirtualButtonEventHandler)
extern MethodInfo VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4385_MethodInfo;
 bool VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4385 (VirtualButtonAbstractBehaviour_t30 * __this, Object_t * ___eventHandler, MethodInfo* method){
	{
		List_1_t814 * L_0 = (__this->___mHandlers_11);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&List_1_Remove_m5560_MethodInfo, L_0, ___eventHandler);
		return L_1;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::CalculateButtonArea(UnityEngine.Vector2&,UnityEngine.Vector2&)
 bool VirtualButtonAbstractBehaviour_CalculateButtonArea_m4386 (VirtualButtonAbstractBehaviour_t30 * __this, Vector2_t99 * ___topLeft, Vector2_t99 * ___bottomRight, MethodInfo* method){
	ImageTargetAbstractBehaviour_t23 * V_0 = {0};
	Vector3_t73  V_1 = {0};
	float V_2 = 0.0f;
	Vector2_t99  V_3 = {0};
	Vector2_t99  V_4 = {0};
	Vector2_t99  V_5 = {0};
	Vector2_t99  V_6 = {0};
	Vector3_t73  V_7 = {0};
	Vector3_t73  V_8 = {0};
	Vector3_t73  V_9 = {0};
	{
		ImageTargetAbstractBehaviour_t23 * L_0 = VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4391(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4391_MethodInfo);
		V_0 = L_0;
		bool L_1 = Object_op_Equality_m524(NULL /*static, unused*/, V_0, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Equality_m524_MethodInfo);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Vector2_t99  L_2 = Vector2_get_zero_m665(NULL /*static, unused*/, /*hidden argument*/&Vector2_get_zero_m665_MethodInfo);
		Vector2_t99  L_3 = L_2;
		V_6 = L_3;
		*___bottomRight = L_3;
		*___topLeft = V_6;
		return 0;
	}

IL_0028:
	{
		NullCheck(V_0);
		Transform_t74 * L_4 = Component_get_transform_m487(V_0, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		Transform_t74 * L_5 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_5);
		Vector3_t73  L_6 = Transform_get_position_m538(L_5, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		NullCheck(L_4);
		Vector3_t73  L_7 = Transform_InverseTransformPoint_m622(L_4, L_6, /*hidden argument*/&Transform_InverseTransformPoint_m622_MethodInfo);
		V_1 = L_7;
		NullCheck(V_0);
		Transform_t74 * L_8 = Component_get_transform_m487(V_0, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_8);
		Vector3_t73  L_9 = Transform_get_lossyScale_m540(L_8, /*hidden argument*/&Transform_get_lossyScale_m540_MethodInfo);
		V_7 = L_9;
		float L_10 = Vector3_get_Item_m841((&V_7), 0, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		V_2 = L_10;
		float L_11 = Vector3_get_Item_m841((&V_1), 0, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		float L_12 = Vector3_get_Item_m841((&V_1), 2, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		Vector2__ctor_m636((&V_3), ((float)((float)L_11*(float)V_2)), ((float)((float)L_12*(float)V_2)), /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		Transform_t74 * L_13 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_13);
		Vector3_t73  L_14 = Transform_get_lossyScale_m540(L_13, /*hidden argument*/&Transform_get_lossyScale_m540_MethodInfo);
		V_8 = L_14;
		float L_15 = Vector3_get_Item_m841((&V_8), 0, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		Transform_t74 * L_16 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_16);
		Vector3_t73  L_17 = Transform_get_lossyScale_m540(L_16, /*hidden argument*/&Transform_get_lossyScale_m540_MethodInfo);
		V_9 = L_17;
		float L_18 = Vector3_get_Item_m841((&V_9), 2, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		Vector2__ctor_m636((&V_4), L_15, L_18, /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		Vector2_t99  L_19 = Vector2_op_Multiply_m808(NULL /*static, unused*/, V_4, (0.5f), /*hidden argument*/&Vector2_op_Multiply_m808_MethodInfo);
		Vector2_t99  L_20 = {0};
		Vector2__ctor_m636(&L_20, (1.0f), (-1.0f), /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		Vector2_t99  L_21 = Vector2_Scale_m2511(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/&Vector2_Scale_m2511_MethodInfo);
		V_5 = L_21;
		Vector2_t99  L_22 = Vector2_op_Subtraction_m688(NULL /*static, unused*/, V_3, V_5, /*hidden argument*/&Vector2_op_Subtraction_m688_MethodInfo);
		*___topLeft = L_22;
		Vector2_t99  L_23 = Vector2_op_Addition_m2391(NULL /*static, unused*/, V_3, V_5, /*hidden argument*/&Vector2_op_Addition_m2391_MethodInfo);
		*___bottomRight = L_23;
		return 1;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateAreaRectangle()
 bool VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4387 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	RectangleData_t632  V_0 = {0};
	{
		Initobj (&RectangleData_t632_il2cpp_TypeInfo, (&V_0));
		Vector2_t99 * L_0 = &(__this->___mLeftTop_12);
		NullCheck(L_0);
		float L_1 = (L_0->___x_1);
		NullCheck((&V_0));
		(&V_0)->___leftTopX_0 = L_1;
		Vector2_t99 * L_2 = &(__this->___mLeftTop_12);
		NullCheck(L_2);
		float L_3 = (L_2->___y_2);
		NullCheck((&V_0));
		(&V_0)->___leftTopY_1 = L_3;
		Vector2_t99 * L_4 = &(__this->___mRightBottom_13);
		NullCheck(L_4);
		float L_5 = (L_4->___x_1);
		NullCheck((&V_0));
		(&V_0)->___rightBottomX_2 = L_5;
		Vector2_t99 * L_6 = &(__this->___mRightBottom_13);
		NullCheck(L_6);
		float L_7 = (L_6->___y_2);
		NullCheck((&V_0));
		(&V_0)->___rightBottomY_3 = L_7;
		VirtualButton_t639 * L_8 = (__this->___mVirtualButton_15);
		if (L_8)
		{
			goto IL_005a;
		}
	}
	{
		return 0;
	}

IL_005a:
	{
		VirtualButton_t639 * L_9 = (__this->___mVirtualButton_15);
		NullCheck(L_9);
		bool L_10 = (bool)VirtFuncInvoker1< bool, RectangleData_t632  >::Invoke(&VirtualButton_SetArea_m5453_MethodInfo, L_9, V_0);
		return L_10;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateSensitivity()
 bool VirtualButtonAbstractBehaviour_UpdateSensitivity_m4388 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		VirtualButton_t639 * L_0 = (__this->___mVirtualButton_15);
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		VirtualButton_t639 * L_1 = (__this->___mVirtualButton_15);
		int32_t L_2 = (__this->___mSensitivity_4);
		NullCheck(L_1);
		bool L_3 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(&VirtualButton_SetSensitivity_m5454_MethodInfo, L_1, L_2);
		return L_3;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateEnabled()
 bool VirtualButtonAbstractBehaviour_UpdateEnabled_m4389 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		VirtualButton_t639 * L_0 = (__this->___mVirtualButton_15);
		bool L_1 = Behaviour_get_enabled_m536(__this, /*hidden argument*/&Behaviour_get_enabled_m536_MethodInfo);
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, bool >::Invoke(&VirtualButton_SetEnabled_m5455_MethodInfo, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdatePose()
 bool VirtualButtonAbstractBehaviour_UpdatePose_m508 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	ImageTargetAbstractBehaviour_t23 * V_0 = {0};
	Transform_t74 * V_1 = {0};
	Vector3_t73  V_2 = {0};
	Vector3_t73  V_3 = {0};
	Vector2_t99  V_4 = {0};
	Vector2_t99  V_5 = {0};
	float V_6 = 0.0f;
	Vector3_t73  V_7 = {0};
	Vector3_t73  V_8 = {0};
	Vector3_t73  V_9 = {0};
	Vector3_t73  V_10 = {0};
	Vector3_t73  V_11 = {0};
	Vector3_t73  V_12 = {0};
	Vector3_t73  V_13 = {0};
	Vector3_t73  V_14 = {0};
	{
		ImageTargetAbstractBehaviour_t23 * L_0 = VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4391(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4391_MethodInfo);
		V_0 = L_0;
		bool L_1 = Object_op_Equality_m524(NULL /*static, unused*/, V_0, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Equality_m524_MethodInfo);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return 0;
	}

IL_0012:
	{
		Transform_t74 * L_2 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_2);
		Transform_t74 * L_3 = Transform_get_parent_m858(L_2, /*hidden argument*/&Transform_get_parent_m858_MethodInfo);
		V_1 = L_3;
		goto IL_00c3;
	}

IL_0023:
	{
		NullCheck(V_1);
		Vector3_t73  L_4 = Transform_get_localScale_m2501(V_1, /*hidden argument*/&Transform_get_localScale_m2501_MethodInfo);
		V_7 = L_4;
		float L_5 = Vector3_get_Item_m841((&V_7), 0, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		NullCheck(V_1);
		Vector3_t73  L_6 = Transform_get_localScale_m2501(V_1, /*hidden argument*/&Transform_get_localScale_m2501_MethodInfo);
		V_8 = L_6;
		float L_7 = Vector3_get_Item_m841((&V_8), 1, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		if ((((float)L_5) != ((float)L_7)))
		{
			goto IL_0067;
		}
	}
	{
		NullCheck(V_1);
		Vector3_t73  L_8 = Transform_get_localScale_m2501(V_1, /*hidden argument*/&Transform_get_localScale_m2501_MethodInfo);
		V_9 = L_8;
		float L_9 = Vector3_get_Item_m841((&V_9), 0, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		NullCheck(V_1);
		Vector3_t73  L_10 = Transform_get_localScale_m2501(V_1, /*hidden argument*/&Transform_get_localScale_m2501_MethodInfo);
		V_10 = L_10;
		float L_11 = Vector3_get_Item_m841((&V_10), 2, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		if ((((float)L_9) == ((float)L_11)))
		{
			goto IL_00bc;
		}
	}

IL_0067:
	{
		NullCheck(V_1);
		String_t* L_12 = Object_get_name_m2144(V_1, /*hidden argument*/&Object_get_name_m2144_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_13 = String_Concat_m287(NULL /*static, unused*/, (String_t*) &_stringLiteral319, L_12, (String_t*) &_stringLiteral320, /*hidden argument*/&String_Concat_m287_MethodInfo);
		Debug_LogWarning_m4964(NULL /*static, unused*/, L_13, /*hidden argument*/&Debug_LogWarning_m4964_MethodInfo);
		NullCheck(V_1);
		Vector3_t73  L_14 = Transform_get_localScale_m2501(V_1, /*hidden argument*/&Transform_get_localScale_m2501_MethodInfo);
		V_11 = L_14;
		float L_15 = Vector3_get_Item_m841((&V_11), 0, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		NullCheck(V_1);
		Vector3_t73  L_16 = Transform_get_localScale_m2501(V_1, /*hidden argument*/&Transform_get_localScale_m2501_MethodInfo);
		V_12 = L_16;
		float L_17 = Vector3_get_Item_m841((&V_12), 0, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		NullCheck(V_1);
		Vector3_t73  L_18 = Transform_get_localScale_m2501(V_1, /*hidden argument*/&Transform_get_localScale_m2501_MethodInfo);
		V_13 = L_18;
		float L_19 = Vector3_get_Item_m841((&V_13), 0, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		Vector3_t73  L_20 = {0};
		Vector3__ctor_m568(&L_20, L_15, L_17, L_19, /*hidden argument*/&Vector3__ctor_m568_MethodInfo);
		NullCheck(V_1);
		Transform_set_localScale_m2507(V_1, L_20, /*hidden argument*/&Transform_set_localScale_m2507_MethodInfo);
	}

IL_00bc:
	{
		NullCheck(V_1);
		Transform_t74 * L_21 = Transform_get_parent_m858(V_1, /*hidden argument*/&Transform_get_parent_m858_MethodInfo);
		V_1 = L_21;
	}

IL_00c3:
	{
		bool L_22 = Object_op_Inequality_m489(NULL /*static, unused*/, V_1, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (L_22)
		{
			goto IL_0023;
		}
	}
	{
		__this->___mHasUpdatedPose_5 = 1;
		Transform_t74 * L_23 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_23);
		Transform_t74 * L_24 = Transform_get_parent_m858(L_23, /*hidden argument*/&Transform_get_parent_m858_MethodInfo);
		bool L_25 = Object_op_Inequality_m489(NULL /*static, unused*/, L_24, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_25)
		{
			goto IL_0116;
		}
	}
	{
		Transform_t74 * L_26 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_26);
		Transform_t74 * L_27 = Transform_get_parent_m858(L_26, /*hidden argument*/&Transform_get_parent_m858_MethodInfo);
		NullCheck(L_27);
		GameObject_t29 * L_28 = Component_get_gameObject_m419(L_27, /*hidden argument*/&Component_get_gameObject_m419_MethodInfo);
		NullCheck(V_0);
		GameObject_t29 * L_29 = Component_get_gameObject_m419(V_0, /*hidden argument*/&Component_get_gameObject_m419_MethodInfo);
		bool L_30 = Object_op_Inequality_m489(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_30)
		{
			goto IL_0116;
		}
	}
	{
		Transform_t74 * L_31 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		Vector3_t73  L_32 = Vector3_get_zero_m807(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_zero_m807_MethodInfo);
		NullCheck(L_31);
		Transform_set_localPosition_m832(L_31, L_32, /*hidden argument*/&Transform_set_localPosition_m832_MethodInfo);
	}

IL_0116:
	{
		NullCheck(V_0);
		Transform_t74 * L_33 = Component_get_transform_m487(V_0, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		Transform_t74 * L_34 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_34);
		Vector3_t73  L_35 = Transform_get_position_m538(L_34, /*hidden argument*/&Transform_get_position_m538_MethodInfo);
		NullCheck(L_33);
		Vector3_t73  L_36 = Transform_InverseTransformPoint_m622(L_33, L_35, /*hidden argument*/&Transform_InverseTransformPoint_m622_MethodInfo);
		V_2 = L_36;
		NullCheck((&V_2));
		(&V_2)->___y_2 = (0.001f);
		NullCheck(V_0);
		Transform_t74 * L_37 = Component_get_transform_m487(V_0, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_37);
		Vector3_t73  L_38 = Transform_TransformPoint_m628(L_37, V_2, /*hidden argument*/&Transform_TransformPoint_m628_MethodInfo);
		V_3 = L_38;
		Transform_t74 * L_39 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_39);
		Transform_set_position_m570(L_39, V_3, /*hidden argument*/&Transform_set_position_m570_MethodInfo);
		Transform_t74 * L_40 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(V_0);
		Transform_t74 * L_41 = Component_get_transform_m487(V_0, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_41);
		Quaternion_t108  L_42 = Transform_get_rotation_m539(L_41, /*hidden argument*/&Transform_get_rotation_m539_MethodInfo);
		NullCheck(L_40);
		Transform_set_rotation_m617(L_40, L_42, /*hidden argument*/&Transform_set_rotation_m617_MethodInfo);
		VirtualButtonAbstractBehaviour_CalculateButtonArea_m4386(__this, (&V_4), (&V_5), /*hidden argument*/&VirtualButtonAbstractBehaviour_CalculateButtonArea_m4386_MethodInfo);
		NullCheck(V_0);
		Transform_t74 * L_43 = Component_get_transform_m487(V_0, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_43);
		Vector3_t73  L_44 = Transform_get_localScale_m2501(L_43, /*hidden argument*/&Transform_get_localScale_m2501_MethodInfo);
		V_14 = L_44;
		float L_45 = Vector3_get_Item_m841((&V_14), 0, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		V_6 = ((float)((float)L_45*(float)(0.001f)));
		Vector2_t99  L_46 = (__this->___mLeftTop_12);
		bool L_47 = VirtualButtonAbstractBehaviour_Equals_m4395(NULL /*static, unused*/, V_4, L_46, V_6, /*hidden argument*/&VirtualButtonAbstractBehaviour_Equals_m4395_MethodInfo);
		if (!L_47)
		{
			goto IL_01b2;
		}
	}
	{
		Vector2_t99  L_48 = (__this->___mRightBottom_13);
		bool L_49 = VirtualButtonAbstractBehaviour_Equals_m4395(NULL /*static, unused*/, V_5, L_48, V_6, /*hidden argument*/&VirtualButtonAbstractBehaviour_Equals_m4395_MethodInfo);
		if (L_49)
		{
			goto IL_01c4;
		}
	}

IL_01b2:
	{
		__this->___mLeftTop_12 = V_4;
		__this->___mRightBottom_13 = V_5;
		return 1;
	}

IL_01c4:
	{
		return 0;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnTrackerUpdated(System.Boolean)
extern MethodInfo VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4390_MethodInfo;
 void VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4390 (VirtualButtonAbstractBehaviour_t30 * __this, bool ___pressed, MethodInfo* method){
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t947  V_2 = {0};
	Enumerator_t947  V_3 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t151 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t151 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		bool L_0 = (__this->___mPreviouslyEnabled_9);
		bool L_1 = Behaviour_get_enabled_m536(__this, /*hidden argument*/&Behaviour_get_enabled_m536_MethodInfo);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0021;
		}
	}
	{
		bool L_2 = Behaviour_get_enabled_m536(__this, /*hidden argument*/&Behaviour_get_enabled_m536_MethodInfo);
		__this->___mPreviouslyEnabled_9 = L_2;
		VirtualButtonAbstractBehaviour_UpdateEnabled_m4389(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_UpdateEnabled_m4389_MethodInfo);
	}

IL_0021:
	{
		bool L_3 = Behaviour_get_enabled_m536(__this, /*hidden argument*/&Behaviour_get_enabled_m536_MethodInfo);
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		return;
	}

IL_002a:
	{
		bool L_4 = (__this->___mPressed_10);
		if ((((int32_t)L_4) == ((int32_t)___pressed)))
		{
			goto IL_00aa;
		}
	}
	{
		List_1_t814 * L_5 = (__this->___mHandlers_11);
		if (!L_5)
		{
			goto IL_00aa;
		}
	}
	{
		if (!___pressed)
		{
			goto IL_0074;
		}
	}
	{
		List_1_t814 * L_6 = (__this->___mHandlers_11);
		NullCheck(L_6);
		Enumerator_t947  L_7 = List_1_GetEnumerator_m5561(L_6, /*hidden argument*/&List_1_GetEnumerator_m5561_MethodInfo);
		V_2 = L_7;
	}

IL_004a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005b;
		}

IL_004c:
		{
			Object_t * L_8 = Enumerator_get_Current_m5562((&V_2), /*hidden argument*/&Enumerator_get_Current_m5562_MethodInfo);
			V_0 = L_8;
			NullCheck(V_0);
			InterfaceActionInvoker1< VirtualButtonAbstractBehaviour_t30 * >::Invoke(&IVirtualButtonEventHandler_OnButtonPressed_m5471_MethodInfo, V_0, __this);
		}

IL_005b:
		{
			bool L_9 = Enumerator_MoveNext_m5563((&V_2), /*hidden argument*/&Enumerator_MoveNext_m5563_MethodInfo);
			if (L_9)
			{
				goto IL_004c;
			}
		}

IL_0064:
		{
			// IL_0064: leave.s IL_00aa
			leaveInstructions[0] = 0xAA; // 1
			THROW_SENTINEL(IL_00aa);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0066;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t151 *)e.ex;
		goto IL_0066;
	}

IL_0066:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t947_il2cpp_TypeInfo), &(*(&V_2))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m333_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t947_il2cpp_TypeInfo), &(*(&V_2))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0xAA:
				goto IL_00aa;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t151 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0074:
	{
		List_1_t814 * L_10 = (__this->___mHandlers_11);
		NullCheck(L_10);
		Enumerator_t947  L_11 = List_1_GetEnumerator_m5561(L_10, /*hidden argument*/&List_1_GetEnumerator_m5561_MethodInfo);
		V_3 = L_11;
	}

IL_0080:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0091;
		}

IL_0082:
		{
			Object_t * L_12 = Enumerator_get_Current_m5562((&V_3), /*hidden argument*/&Enumerator_get_Current_m5562_MethodInfo);
			V_1 = L_12;
			NullCheck(V_1);
			InterfaceActionInvoker1< VirtualButtonAbstractBehaviour_t30 * >::Invoke(&IVirtualButtonEventHandler_OnButtonReleased_m5472_MethodInfo, V_1, __this);
		}

IL_0091:
		{
			bool L_13 = Enumerator_MoveNext_m5563((&V_3), /*hidden argument*/&Enumerator_MoveNext_m5563_MethodInfo);
			if (L_13)
			{
				goto IL_0082;
			}
		}

IL_009a:
		{
			// IL_009a: leave.s IL_00aa
			leaveInstructions[0] = 0xAA; // 1
			THROW_SENTINEL(IL_00aa);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_009c;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t151 *)e.ex;
		goto IL_009c;
	}

IL_009c:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t947_il2cpp_TypeInfo), &(*(&V_3))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m333_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t947_il2cpp_TypeInfo), &(*(&V_3))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0xAA:
				goto IL_00aa;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t151 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_00aa:
	{
		__this->___mPressed_10 = ___pressed;
		return;
	}
}
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VirtualButtonAbstractBehaviour::GetImageTargetBehaviour()
 ImageTargetAbstractBehaviour_t23 * VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4391 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	GameObject_t29 * V_0 = {0};
	ImageTargetAbstractBehaviour_t23 * V_1 = {0};
	{
		Transform_t74 * L_0 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_0);
		Transform_t74 * L_1 = Transform_get_parent_m858(L_0, /*hidden argument*/&Transform_get_parent_m858_MethodInfo);
		bool L_2 = Object_op_Equality_m524(NULL /*static, unused*/, L_1, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Equality_m524_MethodInfo);
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		return (ImageTargetAbstractBehaviour_t23 *)NULL;
	}

IL_0015:
	{
		Transform_t74 * L_3 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_3);
		Transform_t74 * L_4 = Transform_get_parent_m858(L_3, /*hidden argument*/&Transform_get_parent_m858_MethodInfo);
		NullCheck(L_4);
		GameObject_t29 * L_5 = Component_get_gameObject_m419(L_4, /*hidden argument*/&Component_get_gameObject_m419_MethodInfo);
		V_0 = L_5;
		goto IL_0060;
	}

IL_0028:
	{
		NullCheck(V_0);
		ImageTargetAbstractBehaviour_t23 * L_6 = GameObject_GetComponent_TisImageTargetAbstractBehaviour_t23_m5564(V_0, /*hidden argument*/&GameObject_GetComponent_TisImageTargetAbstractBehaviour_t23_m5564_MethodInfo);
		V_1 = L_6;
		bool L_7 = Object_op_Inequality_m489(NULL /*static, unused*/, V_1, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		return V_1;
	}

IL_003a:
	{
		NullCheck(V_0);
		Transform_t74 * L_8 = GameObject_get_transform_m537(V_0, /*hidden argument*/&GameObject_get_transform_m537_MethodInfo);
		NullCheck(L_8);
		Transform_t74 * L_9 = Transform_get_parent_m858(L_8, /*hidden argument*/&Transform_get_parent_m858_MethodInfo);
		bool L_10 = Object_op_Equality_m524(NULL /*static, unused*/, L_9, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Equality_m524_MethodInfo);
		if (!L_10)
		{
			goto IL_004f;
		}
	}
	{
		return (ImageTargetAbstractBehaviour_t23 *)NULL;
	}

IL_004f:
	{
		NullCheck(V_0);
		Transform_t74 * L_11 = GameObject_get_transform_m537(V_0, /*hidden argument*/&GameObject_get_transform_m537_MethodInfo);
		NullCheck(L_11);
		Transform_t74 * L_12 = Transform_get_parent_m858(L_11, /*hidden argument*/&Transform_get_parent_m858_MethodInfo);
		NullCheck(L_12);
		GameObject_t29 * L_13 = Component_get_gameObject_m419(L_12, /*hidden argument*/&Component_get_gameObject_m419_MethodInfo);
		V_0 = L_13;
	}

IL_0060:
	{
		bool L_14 = Object_op_Inequality_m489(NULL /*static, unused*/, V_0, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (L_14)
		{
			goto IL_0028;
		}
	}
	{
		return (ImageTargetAbstractBehaviour_t23 *)NULL;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetVirtualButtonName(System.String)
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m496_MethodInfo;
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m496 (VirtualButtonAbstractBehaviour_t30 * __this, String_t* ___virtualButtonName, MethodInfo* method){
	{
		VirtualButton_t639 * L_0 = (__this->___mVirtualButton_15);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		__this->___mName_3 = ___virtualButtonName;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_SensitivitySetting()
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m497_MethodInfo;
 int32_t VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m497 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___mSensitivity_4);
		return L_0;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetSensitivitySetting(Vuforia.VirtualButton/Sensitivity)
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m498_MethodInfo;
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m498 (VirtualButtonAbstractBehaviour_t30 * __this, int32_t ___sensibility, MethodInfo* method){
	{
		VirtualButton_t639 * L_0 = (__this->___mVirtualButton_15);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		__this->___mSensitivity_4 = ___sensibility;
		__this->___mSensitivityDirty_8 = 1;
		return 1;
	}

IL_0018:
	{
		return 0;
	}
}
// UnityEngine.Matrix4x4 Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_PreviousTransform()
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m499_MethodInfo;
 Matrix4x4_t176  VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m499 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		Matrix4x4_t176  L_0 = (__this->___mPrevTransform_6);
		return L_0;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPreviousTransform(UnityEngine.Matrix4x4)
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m500_MethodInfo;
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m500 (VirtualButtonAbstractBehaviour_t30 * __this, Matrix4x4_t176  ___transformMatrix, MethodInfo* method){
	{
		VirtualButton_t639 * L_0 = (__this->___mVirtualButton_15);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		__this->___mPrevTransform_6 = ___transformMatrix;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// UnityEngine.GameObject Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_PreviousParent()
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m501_MethodInfo;
 GameObject_t29 * VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m501 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		GameObject_t29 * L_0 = (__this->___mPrevParent_7);
		return L_0;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPreviousParent(UnityEngine.GameObject)
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m502_MethodInfo;
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m502 (VirtualButtonAbstractBehaviour_t30 * __this, GameObject_t29 * ___parent, MethodInfo* method){
	{
		VirtualButton_t639 * L_0 = (__this->___mVirtualButton_15);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		__this->___mPrevParent_7 = ___parent;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.InitializeVirtualButton(Vuforia.VirtualButton)
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m503_MethodInfo;
 void VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m503 (VirtualButtonAbstractBehaviour_t30 * __this, VirtualButton_t639 * ___virtualButton, MethodInfo* method){
	{
		__this->___mVirtualButton_15 = ___virtualButton;
		return;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPosAndScaleFromButtonArea(UnityEngine.Vector2,UnityEngine.Vector2)
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m504_MethodInfo;
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m504 (VirtualButtonAbstractBehaviour_t30 * __this, Vector2_t99  ___topLeft, Vector2_t99  ___bottomRight, MethodInfo* method){
	ImageTargetAbstractBehaviour_t23 * V_0 = {0};
	float V_1 = 0.0f;
	Vector2_t99  V_2 = {0};
	Vector2_t99  V_3 = {0};
	Vector3_t73  V_4 = {0};
	Vector3_t73  V_5 = {0};
	Vector3_t73  V_6 = {0};
	Vector3_t73  V_7 = {0};
	{
		ImageTargetAbstractBehaviour_t23 * L_0 = VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4391(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4391_MethodInfo);
		V_0 = L_0;
		bool L_1 = Object_op_Equality_m524(NULL /*static, unused*/, V_0, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Equality_m524_MethodInfo);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return 0;
	}

IL_0012:
	{
		NullCheck(V_0);
		Transform_t74 * L_2 = Component_get_transform_m487(V_0, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_2);
		Vector3_t73  L_3 = Transform_get_lossyScale_m540(L_2, /*hidden argument*/&Transform_get_lossyScale_m540_MethodInfo);
		V_6 = L_3;
		float L_4 = Vector3_get_Item_m841((&V_6), 0, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		V_1 = L_4;
		Vector2_t99  L_5 = Vector2_op_Addition_m2391(NULL /*static, unused*/, ___topLeft, ___bottomRight, /*hidden argument*/&Vector2_op_Addition_m2391_MethodInfo);
		Vector2_t99  L_6 = Vector2_op_Multiply_m808(NULL /*static, unused*/, L_5, (0.5f), /*hidden argument*/&Vector2_op_Multiply_m808_MethodInfo);
		V_2 = L_6;
		float L_7 = Vector2_get_Item_m2393((&___bottomRight), 0, /*hidden argument*/&Vector2_get_Item_m2393_MethodInfo);
		float L_8 = Vector2_get_Item_m2393((&___topLeft), 0, /*hidden argument*/&Vector2_get_Item_m2393_MethodInfo);
		float L_9 = Vector2_get_Item_m2393((&___topLeft), 1, /*hidden argument*/&Vector2_get_Item_m2393_MethodInfo);
		float L_10 = Vector2_get_Item_m2393((&___bottomRight), 1, /*hidden argument*/&Vector2_get_Item_m2393_MethodInfo);
		Vector2__ctor_m636((&V_3), ((float)(L_7-L_8)), ((float)(L_9-L_10)), /*hidden argument*/&Vector2__ctor_m636_MethodInfo);
		float L_11 = Vector2_get_Item_m2393((&V_2), 0, /*hidden argument*/&Vector2_get_Item_m2393_MethodInfo);
		float L_12 = Vector2_get_Item_m2393((&V_2), 1, /*hidden argument*/&Vector2_get_Item_m2393_MethodInfo);
		Vector3__ctor_m568((&V_4), ((float)((float)L_11/(float)V_1)), (0.001f), ((float)((float)L_12/(float)V_1)), /*hidden argument*/&Vector3__ctor_m568_MethodInfo);
		float L_13 = Vector2_get_Item_m2393((&V_3), 0, /*hidden argument*/&Vector2_get_Item_m2393_MethodInfo);
		float L_14 = Vector2_get_Item_m2393((&V_3), 0, /*hidden argument*/&Vector2_get_Item_m2393_MethodInfo);
		float L_15 = Vector2_get_Item_m2393((&V_3), 1, /*hidden argument*/&Vector2_get_Item_m2393_MethodInfo);
		float L_16 = Vector2_get_Item_m2393((&V_3), 1, /*hidden argument*/&Vector2_get_Item_m2393_MethodInfo);
		Vector3__ctor_m568((&V_5), L_13, ((float)((float)((float)(L_14+L_15))*(float)(0.5f))), L_16, /*hidden argument*/&Vector3__ctor_m568_MethodInfo);
		Transform_t74 * L_17 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(V_0);
		Transform_t74 * L_18 = Component_get_transform_m487(V_0, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_18);
		Vector3_t73  L_19 = Transform_TransformPoint_m628(L_18, V_4, /*hidden argument*/&Transform_TransformPoint_m628_MethodInfo);
		NullCheck(L_17);
		Transform_set_position_m570(L_17, L_19, /*hidden argument*/&Transform_set_position_m570_MethodInfo);
		Transform_t74 * L_20 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		Transform_t74 * L_21 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		NullCheck(L_21);
		Transform_t74 * L_22 = Transform_get_parent_m858(L_21, /*hidden argument*/&Transform_get_parent_m858_MethodInfo);
		NullCheck(L_22);
		Vector3_t73  L_23 = Transform_get_lossyScale_m540(L_22, /*hidden argument*/&Transform_get_lossyScale_m540_MethodInfo);
		V_7 = L_23;
		float L_24 = Vector3_get_Item_m841((&V_7), 0, /*hidden argument*/&Vector3_get_Item_m841_MethodInfo);
		Vector3_t73  L_25 = Vector3_op_Division_m630(NULL /*static, unused*/, V_5, L_24, /*hidden argument*/&Vector3_op_Division_m630_MethodInfo);
		NullCheck(L_20);
		Transform_set_localScale_m2507(L_20, L_25, /*hidden argument*/&Transform_set_localScale_m2507_MethodInfo);
		return 1;
	}
}
// UnityEngine.Renderer Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.GetRenderer()
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m513_MethodInfo;
 Renderer_t129 * VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m513 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		Renderer_t129 * L_0 = Component_GetComponent_TisRenderer_t129_m362(__this, /*hidden argument*/&Component_GetComponent_TisRenderer_t129_m362_MethodInfo);
		return L_0;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::LateUpdate()
extern MethodInfo VirtualButtonAbstractBehaviour_LateUpdate_m4392_MethodInfo;
 void VirtualButtonAbstractBehaviour_LateUpdate_m4392 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		bool L_0 = VirtualButtonAbstractBehaviour_UpdatePose_m508(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_UpdatePose_m508_MethodInfo);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4387(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4387_MethodInfo);
	}

IL_000f:
	{
		bool L_1 = (__this->___mSensitivityDirty_8);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = VirtualButtonAbstractBehaviour_UpdateSensitivity_m4388(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_UpdateSensitivity_m4388_MethodInfo);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		__this->___mSensitivityDirty_8 = 0;
	}

IL_0026:
	{
		return;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnDisable()
extern MethodInfo VirtualButtonAbstractBehaviour_OnDisable_m4393_MethodInfo;
 void VirtualButtonAbstractBehaviour_OnDisable_m4393 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	Object_t * V_0 = {0};
	Enumerator_t947  V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t151 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t151 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m361(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsQCAREnabled_m361_MethodInfo);
		if (!L_0)
		{
			goto IL_0075;
		}
	}
	{
		bool L_1 = (__this->___mPreviouslyEnabled_9);
		bool L_2 = Behaviour_get_enabled_m536(__this, /*hidden argument*/&Behaviour_get_enabled_m536_MethodInfo);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0028;
		}
	}
	{
		bool L_3 = Behaviour_get_enabled_m536(__this, /*hidden argument*/&Behaviour_get_enabled_m536_MethodInfo);
		__this->___mPreviouslyEnabled_9 = L_3;
		VirtualButtonAbstractBehaviour_UpdateEnabled_m4389(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_UpdateEnabled_m4389_MethodInfo);
	}

IL_0028:
	{
		bool L_4 = (__this->___mPressed_10);
		if (!L_4)
		{
			goto IL_006e;
		}
	}
	{
		List_1_t814 * L_5 = (__this->___mHandlers_11);
		if (!L_5)
		{
			goto IL_006e;
		}
	}
	{
		List_1_t814 * L_6 = (__this->___mHandlers_11);
		NullCheck(L_6);
		Enumerator_t947  L_7 = List_1_GetEnumerator_m5561(L_6, /*hidden argument*/&List_1_GetEnumerator_m5561_MethodInfo);
		V_1 = L_7;
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0055;
		}

IL_0046:
		{
			Object_t * L_8 = Enumerator_get_Current_m5562((&V_1), /*hidden argument*/&Enumerator_get_Current_m5562_MethodInfo);
			V_0 = L_8;
			NullCheck(V_0);
			InterfaceActionInvoker1< VirtualButtonAbstractBehaviour_t30 * >::Invoke(&IVirtualButtonEventHandler_OnButtonReleased_m5472_MethodInfo, V_0, __this);
		}

IL_0055:
		{
			bool L_9 = Enumerator_MoveNext_m5563((&V_1), /*hidden argument*/&Enumerator_MoveNext_m5563_MethodInfo);
			if (L_9)
			{
				goto IL_0046;
			}
		}

IL_005e:
		{
			// IL_005e: leave.s IL_006e
			leaveInstructions[0] = 0x6E; // 1
			THROW_SENTINEL(IL_006e);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0060;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t151 *)e.ex;
		goto IL_0060;
	}

IL_0060:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t947_il2cpp_TypeInfo), &(*(&V_1))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m333_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t947_il2cpp_TypeInfo), &(*(&V_1))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x6E:
				goto IL_006e;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t151 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_006e:
	{
		__this->___mPressed_10 = 0;
	}

IL_0075:
	{
		return;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnDestroy()
extern MethodInfo VirtualButtonAbstractBehaviour_OnDestroy_m4394_MethodInfo;
 void VirtualButtonAbstractBehaviour_OnDestroy_m4394 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	ImageTargetAbstractBehaviour_t23 * V_0 = {0};
	{
		bool L_0 = Application_get_isPlaying_m2529(NULL /*static, unused*/, /*hidden argument*/&Application_get_isPlaying_m2529_MethodInfo);
		if (!L_0)
		{
			goto IL_0031;
		}
	}
	{
		bool L_1 = (__this->___mUnregisterOnDestroy_14);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		ImageTargetAbstractBehaviour_t23 * L_2 = VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4391(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4391_MethodInfo);
		V_0 = L_2;
		bool L_3 = Object_op_Inequality_m489(NULL /*static, unused*/, V_0, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		NullCheck(V_0);
		Object_t * L_4 = ImageTargetAbstractBehaviour_get_ImageTarget_m4246(V_0, /*hidden argument*/&ImageTargetAbstractBehaviour_get_ImageTarget_m4246_MethodInfo);
		VirtualButton_t639 * L_5 = (__this->___mVirtualButton_15);
		NullCheck(L_4);
		InterfaceFuncInvoker1< bool, VirtualButton_t639 * >::Invoke(&ImageTarget_DestroyVirtualButton_m4728_MethodInfo, L_4, L_5);
	}

IL_0031:
	{
		return;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Equals(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
 bool VirtualButtonAbstractBehaviour_Equals_m4395 (Object_t * __this/* static, unused */, Vector2_t99  ___vec1, Vector2_t99  ___vec2, float ___threshold, MethodInfo* method){
	Vector2_t99  V_0 = {0};
	{
		Vector2_t99  L_0 = Vector2_op_Subtraction_m688(NULL /*static, unused*/, ___vec1, ___vec2, /*hidden argument*/&Vector2_op_Subtraction_m688_MethodInfo);
		V_0 = L_0;
		NullCheck((&V_0));
		float L_1 = ((&V_0)->___x_1);
		float L_2 = fabsf(L_1);
		if ((((float)L_2) >= ((float)___threshold)))
		{
			goto IL_0027;
		}
	}
	{
		NullCheck((&V_0));
		float L_3 = ((&V_0)->___y_2);
		float L_4 = fabsf(L_3);
		return ((((float)L_4) < ((float)___threshold))? 1 : 0);
	}

IL_0027:
	{
		return 0;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_enabled()
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m509_MethodInfo;
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m509 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		bool L_0 = Behaviour_get_enabled_m536(__this, /*hidden argument*/&Behaviour_get_enabled_m536_MethodInfo);
		return L_0;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.set_enabled(System.Boolean)
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m510_MethodInfo;
 void VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m510 (VirtualButtonAbstractBehaviour_t30 * __this, bool p0, MethodInfo* method){
	{
		Behaviour_set_enabled_m547(__this, p0, /*hidden argument*/&Behaviour_set_enabled_m547_MethodInfo);
		return;
	}
}
// UnityEngine.Transform Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_transform()
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m511_MethodInfo;
 Transform_t74 * VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m511 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		Transform_t74 * L_0 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_gameObject()
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m512_MethodInfo;
 GameObject_t29 * VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m512 (VirtualButtonAbstractBehaviour_t30 * __this, MethodInfo* method){
	{
		GameObject_t29 * L_0 = Component_get_gameObject_m419(__this, /*hidden argument*/&Component_get_gameObject_m419_MethodInfo);
		return L_0;
	}
}
// Metadata Definition Vuforia.VirtualButtonAbstractBehaviour
extern Il2CppType Single_t170_0_0_32854;
FieldInfo VirtualButtonAbstractBehaviour_t30____TARGET_OFFSET_2_FieldInfo = 
{
	"TARGET_OFFSET"/* name */
	, &Single_t170_0_0_32854/* type */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mName;
FieldInfo VirtualButtonAbstractBehaviour_t30____mName_3_FieldInfo = 
{
	"mName"/* name */
	, &String_t_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t30, ___mName_3)/* data */
	, &VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mName/* custom_attributes_cache */

};
extern Il2CppType Sensitivity_t789_0_0_1;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mSensitivity;
FieldInfo VirtualButtonAbstractBehaviour_t30____mSensitivity_4_FieldInfo = 
{
	"mSensitivity"/* name */
	, &Sensitivity_t789_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t30, ___mSensitivity_4)/* data */
	, &VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mSensitivity/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mHasUpdatedPose;
FieldInfo VirtualButtonAbstractBehaviour_t30____mHasUpdatedPose_5_FieldInfo = 
{
	"mHasUpdatedPose"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t30, ___mHasUpdatedPose_5)/* data */
	, &VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mHasUpdatedPose/* custom_attributes_cache */

};
extern Il2CppType Matrix4x4_t176_0_0_1;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mPrevTransform;
FieldInfo VirtualButtonAbstractBehaviour_t30____mPrevTransform_6_FieldInfo = 
{
	"mPrevTransform"/* name */
	, &Matrix4x4_t176_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t30, ___mPrevTransform_6)/* data */
	, &VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mPrevTransform/* custom_attributes_cache */

};
extern Il2CppType GameObject_t29_0_0_1;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mPrevParent;
FieldInfo VirtualButtonAbstractBehaviour_t30____mPrevParent_7_FieldInfo = 
{
	"mPrevParent"/* name */
	, &GameObject_t29_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t30, ___mPrevParent_7)/* data */
	, &VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mPrevParent/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t30____mSensitivityDirty_8_FieldInfo = 
{
	"mSensitivityDirty"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t30, ___mSensitivityDirty_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t30____mPreviouslyEnabled_9_FieldInfo = 
{
	"mPreviouslyEnabled"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t30, ___mPreviouslyEnabled_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t30____mPressed_10_FieldInfo = 
{
	"mPressed"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t30, ___mPressed_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType List_1_t814_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t30____mHandlers_11_FieldInfo = 
{
	"mHandlers"/* name */
	, &List_1_t814_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t30, ___mHandlers_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t30____mLeftTop_12_FieldInfo = 
{
	"mLeftTop"/* name */
	, &Vector2_t99_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t30, ___mLeftTop_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t99_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t30____mRightBottom_13_FieldInfo = 
{
	"mRightBottom"/* name */
	, &Vector2_t99_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t30, ___mRightBottom_13)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t30____mUnregisterOnDestroy_14_FieldInfo = 
{
	"mUnregisterOnDestroy"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t30, ___mUnregisterOnDestroy_14)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType VirtualButton_t639_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t30____mVirtualButton_15_FieldInfo = 
{
	"mVirtualButton"/* name */
	, &VirtualButton_t639_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t30, ___mVirtualButton_15)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* VirtualButtonAbstractBehaviour_t30_FieldInfos[] =
{
	&VirtualButtonAbstractBehaviour_t30____TARGET_OFFSET_2_FieldInfo,
	&VirtualButtonAbstractBehaviour_t30____mName_3_FieldInfo,
	&VirtualButtonAbstractBehaviour_t30____mSensitivity_4_FieldInfo,
	&VirtualButtonAbstractBehaviour_t30____mHasUpdatedPose_5_FieldInfo,
	&VirtualButtonAbstractBehaviour_t30____mPrevTransform_6_FieldInfo,
	&VirtualButtonAbstractBehaviour_t30____mPrevParent_7_FieldInfo,
	&VirtualButtonAbstractBehaviour_t30____mSensitivityDirty_8_FieldInfo,
	&VirtualButtonAbstractBehaviour_t30____mPreviouslyEnabled_9_FieldInfo,
	&VirtualButtonAbstractBehaviour_t30____mPressed_10_FieldInfo,
	&VirtualButtonAbstractBehaviour_t30____mHandlers_11_FieldInfo,
	&VirtualButtonAbstractBehaviour_t30____mLeftTop_12_FieldInfo,
	&VirtualButtonAbstractBehaviour_t30____mRightBottom_13_FieldInfo,
	&VirtualButtonAbstractBehaviour_t30____mUnregisterOnDestroy_14_FieldInfo,
	&VirtualButtonAbstractBehaviour_t30____mVirtualButton_15_FieldInfo,
	NULL
};
static const float VirtualButtonAbstractBehaviour_t30____TARGET_OFFSET_2_DefaultValueData = 0.001f;
static Il2CppFieldDefaultValueEntry VirtualButtonAbstractBehaviour_t30____TARGET_OFFSET_2_DefaultValue = 
{
	&VirtualButtonAbstractBehaviour_t30____TARGET_OFFSET_2_FieldInfo/* field */
	, { (char*)&VirtualButtonAbstractBehaviour_t30____TARGET_OFFSET_2_DefaultValueData, &Single_t170_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* VirtualButtonAbstractBehaviour_t30_FieldDefaultValues[] = 
{
	&VirtualButtonAbstractBehaviour_t30____TARGET_OFFSET_2_DefaultValue,
	NULL
};
static PropertyInfo VirtualButtonAbstractBehaviour_t30____VirtualButtonName_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, "VirtualButtonName"/* name */
	, &VirtualButtonAbstractBehaviour_get_VirtualButtonName_m495_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo VirtualButtonAbstractBehaviour_t30____Pressed_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, "Pressed"/* name */
	, &VirtualButtonAbstractBehaviour_get_Pressed_m4382_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo VirtualButtonAbstractBehaviour_t30____HasUpdatedPose_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, "HasUpdatedPose"/* name */
	, &VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m507_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo VirtualButtonAbstractBehaviour_t30____UnregisterOnDestroy_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, "UnregisterOnDestroy"/* name */
	, &VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m505_MethodInfo/* get */
	, &VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m506_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo VirtualButtonAbstractBehaviour_t30____VirtualButton_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, "VirtualButton"/* name */
	, &VirtualButtonAbstractBehaviour_get_VirtualButton_m4383_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo VirtualButtonAbstractBehaviour_t30____Vuforia_IEditorVirtualButtonBehaviour_SensitivitySetting_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorVirtualButtonBehaviour.SensitivitySetting"/* name */
	, &VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m497_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo VirtualButtonAbstractBehaviour_t30____Vuforia_IEditorVirtualButtonBehaviour_PreviousTransform_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorVirtualButtonBehaviour.PreviousTransform"/* name */
	, &VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m499_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo VirtualButtonAbstractBehaviour_t30____Vuforia_IEditorVirtualButtonBehaviour_PreviousParent_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorVirtualButtonBehaviour.PreviousParent"/* name */
	, &VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m501_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* VirtualButtonAbstractBehaviour_t30_PropertyInfos[] =
{
	&VirtualButtonAbstractBehaviour_t30____VirtualButtonName_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t30____Pressed_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t30____HasUpdatedPose_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t30____UnregisterOnDestroy_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t30____VirtualButton_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t30____Vuforia_IEditorVirtualButtonBehaviour_SensitivitySetting_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t30____Vuforia_IEditorVirtualButtonBehaviour_PreviousTransform_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t30____Vuforia_IEditorVirtualButtonBehaviour_PreviousParent_PropertyInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.VirtualButtonAbstractBehaviour::get_VirtualButtonName()
MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButtonName_m495_MethodInfo = 
{
	"get_VirtualButtonName"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_VirtualButtonName_m495/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_Pressed()
MethodInfo VirtualButtonAbstractBehaviour_get_Pressed_m4382_MethodInfo = 
{
	"get_Pressed"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_Pressed_m4382/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_HasUpdatedPose()
MethodInfo VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m507_MethodInfo = 
{
	"get_HasUpdatedPose"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m507/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_UnregisterOnDestroy()
MethodInfo VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m505_MethodInfo = 
{
	"get_UnregisterOnDestroy"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m505/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2433/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m506_ParameterInfos[] = 
{
	{"value", 0, 134220034, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::set_UnregisterOnDestroy(System.Boolean)
MethodInfo VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m506_MethodInfo = 
{
	"set_UnregisterOnDestroy"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m506/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125/* invoker_method */
	, VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m506_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2434/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType VirtualButton_t639_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.VirtualButton Vuforia.VirtualButtonAbstractBehaviour::get_VirtualButton()
MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButton_m4383_MethodInfo = 
{
	"get_VirtualButton"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_VirtualButton_m4383/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButton_t639_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2435/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::.ctor()
MethodInfo VirtualButtonAbstractBehaviour__ctor_m494_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour__ctor_m494/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2436/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IVirtualButtonEventHandler_t815_0_0_0;
extern Il2CppType IVirtualButtonEventHandler_t815_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_RegisterEventHandler_m4384_ParameterInfos[] = 
{
	{"eventHandler", 0, 134220035, &EmptyCustomAttributesCache, &IVirtualButtonEventHandler_t815_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::RegisterEventHandler(Vuforia.IVirtualButtonEventHandler)
MethodInfo VirtualButtonAbstractBehaviour_RegisterEventHandler_m4384_MethodInfo = 
{
	"RegisterEventHandler"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_RegisterEventHandler_m4384/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_RegisterEventHandler_m4384_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2437/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IVirtualButtonEventHandler_t815_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4385_ParameterInfos[] = 
{
	{"eventHandler", 0, 134220036, &EmptyCustomAttributesCache, &IVirtualButtonEventHandler_t815_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UnregisterEventHandler(Vuforia.IVirtualButtonEventHandler)
MethodInfo VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4385_MethodInfo = 
{
	"UnregisterEventHandler"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4385/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4385_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2438/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t99_1_0_2;
extern Il2CppType Vector2_t99_1_0_2;
static ParameterInfo VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_CalculateButtonArea_m4386_ParameterInfos[] = 
{
	{"topLeft", 0, 134220037, &EmptyCustomAttributesCache, &Vector2_t99_1_0_2},
	{"bottomRight", 1, 134220038, &EmptyCustomAttributesCache, &Vector2_t99_1_0_2},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Vector2U26_t940_Vector2U26_t940 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::CalculateButtonArea(UnityEngine.Vector2&,UnityEngine.Vector2&)
MethodInfo VirtualButtonAbstractBehaviour_CalculateButtonArea_m4386_MethodInfo = 
{
	"CalculateButtonArea"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_CalculateButtonArea_m4386/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Vector2U26_t940_Vector2U26_t940/* invoker_method */
	, VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_CalculateButtonArea_m4386_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2439/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateAreaRectangle()
MethodInfo VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4387_MethodInfo = 
{
	"UpdateAreaRectangle"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4387/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2440/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateSensitivity()
MethodInfo VirtualButtonAbstractBehaviour_UpdateSensitivity_m4388_MethodInfo = 
{
	"UpdateSensitivity"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UpdateSensitivity_m4388/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2441/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateEnabled()
MethodInfo VirtualButtonAbstractBehaviour_UpdateEnabled_m4389_MethodInfo = 
{
	"UpdateEnabled"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UpdateEnabled_m4389/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2442/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdatePose()
MethodInfo VirtualButtonAbstractBehaviour_UpdatePose_m508_MethodInfo = 
{
	"UpdatePose"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UpdatePose_m508/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2443/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4390_ParameterInfos[] = 
{
	{"pressed", 0, 134220039, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnTrackerUpdated(System.Boolean)
MethodInfo VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4390_MethodInfo = 
{
	"OnTrackerUpdated"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4390/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125/* invoker_method */
	, VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4390_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2444/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ImageTargetAbstractBehaviour_t23_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VirtualButtonAbstractBehaviour::GetImageTargetBehaviour()
MethodInfo VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4391_MethodInfo = 
{
	"GetImageTargetBehaviour"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4391/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetAbstractBehaviour_t23_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2445/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m496_ParameterInfos[] = 
{
	{"virtualButtonName", 0, 134220040, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetVirtualButtonName(System.String)
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m496_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetVirtualButtonName"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m496/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m496_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2446/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Sensitivity_t789_0_0_0;
extern void* RuntimeInvoker_Sensitivity_t789 (MethodInfo* method, void* obj, void** args);
// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_SensitivitySetting()
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m497_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_SensitivitySetting"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m497/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Sensitivity_t789_0_0_0/* return_type */
	, RuntimeInvoker_Sensitivity_t789/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2447/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Sensitivity_t789_0_0_0;
extern Il2CppType Sensitivity_t789_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m498_ParameterInfos[] = 
{
	{"sensibility", 0, 134220041, &EmptyCustomAttributesCache, &Sensitivity_t789_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetSensitivitySetting(Vuforia.VirtualButton/Sensitivity)
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m498_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetSensitivitySetting"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m498/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m498_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2448/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Matrix4x4_t176_0_0_0;
extern void* RuntimeInvoker_Matrix4x4_t176 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Matrix4x4 Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_PreviousTransform()
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m499_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_PreviousTransform"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m499/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Matrix4x4_t176_0_0_0/* return_type */
	, RuntimeInvoker_Matrix4x4_t176/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2449/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Matrix4x4_t176_0_0_0;
extern Il2CppType Matrix4x4_t176_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m500_ParameterInfos[] = 
{
	{"transformMatrix", 0, 134220042, &EmptyCustomAttributesCache, &Matrix4x4_t176_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Matrix4x4_t176 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPreviousTransform(UnityEngine.Matrix4x4)
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m500_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetPreviousTransform"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m500/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Matrix4x4_t176/* invoker_method */
	, VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m500_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2450/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t29_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.GameObject Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_PreviousParent()
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m501_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_PreviousParent"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m501/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t29_0_0_0;
extern Il2CppType GameObject_t29_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m502_ParameterInfos[] = 
{
	{"parent", 0, 134220043, &EmptyCustomAttributesCache, &GameObject_t29_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPreviousParent(UnityEngine.GameObject)
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m502_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetPreviousParent"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m502/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m502_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType VirtualButton_t639_0_0_0;
extern Il2CppType VirtualButton_t639_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m503_ParameterInfos[] = 
{
	{"virtualButton", 0, 134220044, &EmptyCustomAttributesCache, &VirtualButton_t639_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.InitializeVirtualButton(Vuforia.VirtualButton)
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m503_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.InitializeVirtualButton"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m503/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m503_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t99_0_0_0;
extern Il2CppType Vector2_t99_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m504_ParameterInfos[] = 
{
	{"topLeft", 0, 134220045, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
	{"bottomRight", 1, 134220046, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Vector2_t99_Vector2_t99 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPosAndScaleFromButtonArea(UnityEngine.Vector2,UnityEngine.Vector2)
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m504_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetPosAndScaleFromButtonArea"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m504/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Vector2_t99_Vector2_t99/* invoker_method */
	, VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m504_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Renderer_t129_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Renderer Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.GetRenderer()
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m513_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.GetRenderer"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m513/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Renderer_t129_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::LateUpdate()
MethodInfo VirtualButtonAbstractBehaviour_LateUpdate_m4392_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_LateUpdate_m4392/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnDisable()
MethodInfo VirtualButtonAbstractBehaviour_OnDisable_m4393_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_OnDisable_m4393/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnDestroy()
MethodInfo VirtualButtonAbstractBehaviour_OnDestroy_m4394_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_OnDestroy_m4394/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t99_0_0_0;
extern Il2CppType Vector2_t99_0_0_0;
extern Il2CppType Single_t170_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Equals_m4395_ParameterInfos[] = 
{
	{"vec1", 0, 134220047, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
	{"vec2", 1, 134220048, &EmptyCustomAttributesCache, &Vector2_t99_0_0_0},
	{"threshold", 2, 134220049, &EmptyCustomAttributesCache, &Single_t170_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Vector2_t99_Vector2_t99_Single_t170 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Equals(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
MethodInfo VirtualButtonAbstractBehaviour_Equals_m4395_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Equals_m4395/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Vector2_t99_Vector2_t99_Single_t170/* invoker_method */
	, VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Equals_m4395_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_enabled()
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m509_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_enabled"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m509/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m510_ParameterInfos[] = 
{
	{"", 0, 134217728, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.set_enabled(System.Boolean)
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m510_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.set_enabled"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m510/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125/* invoker_method */
	, VirtualButtonAbstractBehaviour_t30_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m510_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Transform_t74_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_transform()
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m511_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_transform"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m511/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t74_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t29_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.GameObject Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_gameObject()
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m512_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_gameObject"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m512/* method */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* VirtualButtonAbstractBehaviour_t30_MethodInfos[] =
{
	&VirtualButtonAbstractBehaviour_get_VirtualButtonName_m495_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_Pressed_m4382_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m507_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m505_MethodInfo,
	&VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m506_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_VirtualButton_m4383_MethodInfo,
	&VirtualButtonAbstractBehaviour__ctor_m494_MethodInfo,
	&VirtualButtonAbstractBehaviour_RegisterEventHandler_m4384_MethodInfo,
	&VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4385_MethodInfo,
	&VirtualButtonAbstractBehaviour_CalculateButtonArea_m4386_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4387_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdateSensitivity_m4388_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdateEnabled_m4389_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdatePose_m508_MethodInfo,
	&VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4390_MethodInfo,
	&VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4391_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m496_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m497_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m498_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m499_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m500_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m501_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m502_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m503_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m504_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m513_MethodInfo,
	&VirtualButtonAbstractBehaviour_LateUpdate_m4392_MethodInfo,
	&VirtualButtonAbstractBehaviour_OnDisable_m4393_MethodInfo,
	&VirtualButtonAbstractBehaviour_OnDestroy_m4394_MethodInfo,
	&VirtualButtonAbstractBehaviour_Equals_m4395_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m509_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m510_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m511_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m512_MethodInfo,
	NULL
};
static MethodInfo* VirtualButtonAbstractBehaviour_t30_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_VirtualButtonName_m495_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m496_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m497_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m498_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m499_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m500_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m501_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m502_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m503_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m504_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m505_MethodInfo,
	&VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m506_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m507_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdatePose_m508_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m509_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m510_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m511_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m512_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m513_MethodInfo,
};
extern TypeInfo IEditorVirtualButtonBehaviour_t169_il2cpp_TypeInfo;
static TypeInfo* VirtualButtonAbstractBehaviour_t30_InterfacesTypeInfos[] = 
{
	&IEditorVirtualButtonBehaviour_t169_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair VirtualButtonAbstractBehaviour_t30_InterfacesOffsets[] = 
{
	{ &IEditorVirtualButtonBehaviour_t169_il2cpp_TypeInfo, 4},
};
void VirtualButtonAbstractBehaviour_t30_CustomAttributesCacheGenerator_mName(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void VirtualButtonAbstractBehaviour_t30_CustomAttributesCacheGenerator_mSensitivity(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void VirtualButtonAbstractBehaviour_t30_CustomAttributesCacheGenerator_mHasUpdatedPose(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void VirtualButtonAbstractBehaviour_t30_CustomAttributesCacheGenerator_mPrevTransform(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void VirtualButtonAbstractBehaviour_t30_CustomAttributesCacheGenerator_mPrevParent(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mName = {
2,
NULL,
&VirtualButtonAbstractBehaviour_t30_CustomAttributesCacheGenerator_mName
};
CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mSensitivity = {
2,
NULL,
&VirtualButtonAbstractBehaviour_t30_CustomAttributesCacheGenerator_mSensitivity
};
CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mHasUpdatedPose = {
2,
NULL,
&VirtualButtonAbstractBehaviour_t30_CustomAttributesCacheGenerator_mHasUpdatedPose
};
CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mPrevTransform = {
2,
NULL,
&VirtualButtonAbstractBehaviour_t30_CustomAttributesCacheGenerator_mPrevTransform
};
CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mPrevParent = {
2,
NULL,
&VirtualButtonAbstractBehaviour_t30_CustomAttributesCacheGenerator_mPrevParent
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType VirtualButtonAbstractBehaviour_t30_0_0_0;
extern Il2CppType VirtualButtonAbstractBehaviour_t30_1_0_0;
struct VirtualButtonAbstractBehaviour_t30;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mName;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mSensitivity;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mHasUpdatedPose;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mPrevTransform;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t30__CustomAttributeCache_mPrevParent;
TypeInfo VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "VirtualButtonAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VirtualButtonAbstractBehaviour_t30_MethodInfos/* methods */
	, VirtualButtonAbstractBehaviour_t30_PropertyInfos/* properties */
	, VirtualButtonAbstractBehaviour_t30_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* element_class */
	, VirtualButtonAbstractBehaviour_t30_InterfacesTypeInfos/* implemented_interfaces */
	, VirtualButtonAbstractBehaviour_t30_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &VirtualButtonAbstractBehaviour_t30_il2cpp_TypeInfo/* cast_class */
	, &VirtualButtonAbstractBehaviour_t30_0_0_0/* byval_arg */
	, &VirtualButtonAbstractBehaviour_t30_1_0_0/* this_arg */
	, VirtualButtonAbstractBehaviour_t30_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, VirtualButtonAbstractBehaviour_t30_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VirtualButtonAbstractBehaviour_t30)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 34/* method_count */
	, 8/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// Vuforia.WebCamImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamImpl.h"
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDevice.h"
extern TypeInfo WebCamImpl_t648_il2cpp_TypeInfo;
extern TypeInfo Exception_t151_il2cpp_TypeInfo;
// Vuforia.WebCamImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamImplMethodDeclarations.h"
// UnityEngine.WebCamTexture
#include "UnityEngine_UnityEngine_WebCamTextureMethodDeclarations.h"
extern MethodInfo WebCamImpl_get_IsPlaying_m4222_MethodInfo;
extern MethodInfo WebCamAbstractBehaviour_CheckNativePluginSupport_m4408_MethodInfo;
extern MethodInfo WebCamTexture_get_devices_m5456_MethodInfo;
extern MethodInfo Application_set_runInBackground_m5565_MethodInfo;
extern MethodInfo Component_GetComponentsInChildren_TisCamera_t168_m5566_MethodInfo;
extern MethodInfo WebCamImpl__ctor_m4230_MethodInfo;
extern MethodInfo WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4412_MethodInfo;
extern MethodInfo WebCamImpl_ResetPlaying_m4233_MethodInfo;
extern MethodInfo WebCamImpl_OnDestroy_m4239_MethodInfo;
extern MethodInfo Object_Destroy_m485_MethodInfo;
extern MethodInfo WebCamImpl_Update_m4240_MethodInfo;
struct Component_t128;
struct Component_t128;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
 ObjectU5BU5D_t130* Component_GetComponentsInChildren_TisObject_t_m5076_gshared (Component_t128 * __this, MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m5076(__this, method) (ObjectU5BU5D_t130*)Component_GetComponentsInChildren_TisObject_t_m5076_gshared((Component_t128 *)__this, method)
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Camera>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Camera>()
#define Component_GetComponentsInChildren_TisCamera_t168_m5566(__this, method) (CameraU5BU5D_t172*)Component_GetComponentsInChildren_TisObject_t_m5076_gshared((Component_t128 *)__this, method)


// System.Boolean Vuforia.WebCamAbstractBehaviour::get_PlayModeRenderVideo()
extern MethodInfo WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4396_MethodInfo;
 bool WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4396 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mPlayModeRenderVideo_3);
		return L_0;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::set_PlayModeRenderVideo(System.Boolean)
extern MethodInfo WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4397_MethodInfo;
 void WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4397 (WebCamAbstractBehaviour_t63 * __this, bool ___value, MethodInfo* method){
	{
		__this->___mPlayModeRenderVideo_3 = ___value;
		return;
	}
}
// System.String Vuforia.WebCamAbstractBehaviour::get_DeviceName()
extern MethodInfo WebCamAbstractBehaviour_get_DeviceName_m4398_MethodInfo;
 String_t* WebCamAbstractBehaviour_get_DeviceName_m4398 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mDeviceNameSetInEditor_4);
		return L_0;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::set_DeviceName(System.String)
extern MethodInfo WebCamAbstractBehaviour_set_DeviceName_m4399_MethodInfo;
 void WebCamAbstractBehaviour_set_DeviceName_m4399 (WebCamAbstractBehaviour_t63 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___mDeviceNameSetInEditor_4 = ___value;
		return;
	}
}
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_FlipHorizontally()
extern MethodInfo WebCamAbstractBehaviour_get_FlipHorizontally_m4400_MethodInfo;
 bool WebCamAbstractBehaviour_get_FlipHorizontally_m4400 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mFlipHorizontally_5);
		return L_0;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::set_FlipHorizontally(System.Boolean)
extern MethodInfo WebCamAbstractBehaviour_set_FlipHorizontally_m4401_MethodInfo;
 void WebCamAbstractBehaviour_set_FlipHorizontally_m4401 (WebCamAbstractBehaviour_t63 * __this, bool ___value, MethodInfo* method){
	{
		__this->___mFlipHorizontally_5 = ___value;
		return;
	}
}
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_TurnOffWebCam()
extern MethodInfo WebCamAbstractBehaviour_get_TurnOffWebCam_m4402_MethodInfo;
 bool WebCamAbstractBehaviour_get_TurnOffWebCam_m4402 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mTurnOffWebCam_6);
		return L_0;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::set_TurnOffWebCam(System.Boolean)
extern MethodInfo WebCamAbstractBehaviour_set_TurnOffWebCam_m4403_MethodInfo;
 void WebCamAbstractBehaviour_set_TurnOffWebCam_m4403 (WebCamAbstractBehaviour_t63 * __this, bool ___value, MethodInfo* method){
	{
		__this->___mTurnOffWebCam_6 = ___value;
		return;
	}
}
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_IsPlaying()
extern MethodInfo WebCamAbstractBehaviour_get_IsPlaying_m4404_MethodInfo;
 bool WebCamAbstractBehaviour_get_IsPlaying_m4404 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method){
	{
		WebCamImpl_t648 * L_0 = (__this->___mWebCamImpl_7);
		NullCheck(L_0);
		bool L_1 = WebCamImpl_get_IsPlaying_m4222(L_0, /*hidden argument*/&WebCamImpl_get_IsPlaying_m4222_MethodInfo);
		return L_1;
	}
}
// System.Boolean Vuforia.WebCamAbstractBehaviour::IsWebCamUsed()
 bool WebCamAbstractBehaviour_IsWebCamUsed_m4405 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mTurnOffWebCam_6);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		bool L_1 = WebCamAbstractBehaviour_CheckNativePluginSupport_m4408(__this, /*hidden argument*/&WebCamAbstractBehaviour_CheckNativePluginSupport_m4408_MethodInfo);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		WebCamDeviceU5BU5D_t930* L_2 = WebCamTexture_get_devices_m5456(NULL /*static, unused*/, /*hidden argument*/&WebCamTexture_get_devices_m5456_MethodInfo);
		NullCheck(L_2);
		return ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) > ((int32_t)0))? 1 : 0);
	}

IL_001b:
	{
		return 0;
	}
}
// Vuforia.WebCamImpl Vuforia.WebCamAbstractBehaviour::get_ImplementationClass()
extern MethodInfo WebCamAbstractBehaviour_get_ImplementationClass_m4406_MethodInfo;
 WebCamImpl_t648 * WebCamAbstractBehaviour_get_ImplementationClass_m4406 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method){
	{
		WebCamImpl_t648 * L_0 = (__this->___mWebCamImpl_7);
		return L_0;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::InitCamera()
extern MethodInfo WebCamAbstractBehaviour_InitCamera_m4407_MethodInfo;
 void WebCamAbstractBehaviour_InitCamera_m4407 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method){
	CameraU5BU5D_t172* V_0 = {0};
	{
		WebCamImpl_t648 * L_0 = (__this->___mWebCamImpl_7);
		if (L_0)
		{
			goto IL_0033;
		}
	}
	{
		Application_set_runInBackground_m5565(NULL /*static, unused*/, 1, /*hidden argument*/&Application_set_runInBackground_m5565_MethodInfo);
		CameraU5BU5D_t172* L_1 = Component_GetComponentsInChildren_TisCamera_t168_m5566(__this, /*hidden argument*/&Component_GetComponentsInChildren_TisCamera_t168_m5566_MethodInfo);
		V_0 = L_1;
		int32_t L_2 = (__this->___RenderTextureLayer_2);
		String_t* L_3 = (__this->___mDeviceNameSetInEditor_4);
		bool L_4 = (__this->___mFlipHorizontally_5);
		WebCamImpl_t648 * L_5 = (WebCamImpl_t648 *)il2cpp_codegen_object_new (InitializedTypeInfo(&WebCamImpl_t648_il2cpp_TypeInfo));
		WebCamImpl__ctor_m4230(L_5, V_0, L_2, L_3, L_4, /*hidden argument*/&WebCamImpl__ctor_m4230_MethodInfo);
		__this->___mWebCamImpl_7 = L_5;
	}

IL_0033:
	{
		return;
	}
}
// System.Boolean Vuforia.WebCamAbstractBehaviour::CheckNativePluginSupport()
 bool WebCamAbstractBehaviour_CheckNativePluginSupport_m4408 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	Exception_t151 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t151 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m416(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsPlayMode_m416_MethodInfo);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		V_0 = 0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		int32_t L_1 = WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4412(NULL /*static, unused*/, /*hidden argument*/&WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4412_MethodInfo);
		V_0 = L_1;
		// IL_000f: leave.s IL_0016
		goto IL_0016;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t151 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&Exception_t151_il2cpp_TypeInfo, e.ex->object.klass))
			goto IL_0011;
		throw e;
	}

IL_0011:
	{ // begin catch(System.Exception)
		V_0 = 0;
		// IL_0014: leave.s IL_0016
		goto IL_0016;
	} // end catch (depth: 1)

IL_0016:
	{
		return ((((int32_t)V_0) == ((int32_t)1))? 1 : 0);
	}

IL_001b:
	{
		return 1;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::OnLevelWasLoaded()
extern MethodInfo WebCamAbstractBehaviour_OnLevelWasLoaded_m4409_MethodInfo;
 void WebCamAbstractBehaviour_OnLevelWasLoaded_m4409 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m416(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsPlayMode_m416_MethodInfo);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		WebCamImpl_t648 * L_1 = (__this->___mWebCamImpl_7);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		WebCamImpl_t648 * L_2 = (__this->___mWebCamImpl_7);
		NullCheck(L_2);
		WebCamImpl_ResetPlaying_m4233(L_2, /*hidden argument*/&WebCamImpl_ResetPlaying_m4233_MethodInfo);
	}

IL_001a:
	{
		return;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::OnDestroy()
extern MethodInfo WebCamAbstractBehaviour_OnDestroy_m4410_MethodInfo;
 void WebCamAbstractBehaviour_OnDestroy_m4410 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m416(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsPlayMode_m416_MethodInfo);
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		WebCamImpl_t648 * L_1 = (__this->___mWebCamImpl_7);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		WebCamImpl_t648 * L_2 = (__this->___mWebCamImpl_7);
		NullCheck(L_2);
		WebCamImpl_OnDestroy_m4239(L_2, /*hidden argument*/&WebCamImpl_OnDestroy_m4239_MethodInfo);
		Camera_t168 * L_3 = (__this->___mBackgroundCameraInstance_8);
		Object_Destroy_m485(NULL /*static, unused*/, L_3, /*hidden argument*/&Object_Destroy_m485_MethodInfo);
	}

IL_0025:
	{
		return;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::Update()
extern MethodInfo WebCamAbstractBehaviour_Update_m4411_MethodInfo;
 void WebCamAbstractBehaviour_Update_m4411 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t156_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m416(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsPlayMode_m416_MethodInfo);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		WebCamImpl_t648 * L_1 = (__this->___mWebCamImpl_7);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		WebCamImpl_t648 * L_2 = (__this->___mWebCamImpl_7);
		NullCheck(L_2);
		WebCamImpl_Update_m4240(L_2, /*hidden argument*/&WebCamImpl_Update_m4240_MethodInfo);
	}

IL_001a:
	{
		return;
	}
}
// System.Int32 Vuforia.WebCamAbstractBehaviour::qcarCheckNativePluginSupport()
 int32_t WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4412 (Object_t * __this/* static, unused */, MethodInfo* method){
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = 0;
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("QCARWrapper", "qcarCheckNativePluginSupport", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'qcarCheckNativePluginSupport'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void Vuforia.WebCamAbstractBehaviour::.ctor()
extern MethodInfo WebCamAbstractBehaviour__ctor_m514_MethodInfo;
 void WebCamAbstractBehaviour__ctor_m514 (WebCamAbstractBehaviour_t63 * __this, MethodInfo* method){
	{
		__this->___mPlayModeRenderVideo_3 = 1;
		MonoBehaviour__ctor_m254(__this, /*hidden argument*/&MonoBehaviour__ctor_m254_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.WebCamAbstractBehaviour
extern Il2CppType Int32_t123_0_0_6;
FieldInfo WebCamAbstractBehaviour_t63____RenderTextureLayer_2_FieldInfo = 
{
	"RenderTextureLayer"/* name */
	, &Int32_t123_0_0_6/* type */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamAbstractBehaviour_t63, ___RenderTextureLayer_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
extern CustomAttributesCache WebCamAbstractBehaviour_t63__CustomAttributeCache_mPlayModeRenderVideo;
FieldInfo WebCamAbstractBehaviour_t63____mPlayModeRenderVideo_3_FieldInfo = 
{
	"mPlayModeRenderVideo"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamAbstractBehaviour_t63, ___mPlayModeRenderVideo_3)/* data */
	, &WebCamAbstractBehaviour_t63__CustomAttributeCache_mPlayModeRenderVideo/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache WebCamAbstractBehaviour_t63__CustomAttributeCache_mDeviceNameSetInEditor;
FieldInfo WebCamAbstractBehaviour_t63____mDeviceNameSetInEditor_4_FieldInfo = 
{
	"mDeviceNameSetInEditor"/* name */
	, &String_t_0_0_1/* type */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamAbstractBehaviour_t63, ___mDeviceNameSetInEditor_4)/* data */
	, &WebCamAbstractBehaviour_t63__CustomAttributeCache_mDeviceNameSetInEditor/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
extern CustomAttributesCache WebCamAbstractBehaviour_t63__CustomAttributeCache_mFlipHorizontally;
FieldInfo WebCamAbstractBehaviour_t63____mFlipHorizontally_5_FieldInfo = 
{
	"mFlipHorizontally"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamAbstractBehaviour_t63, ___mFlipHorizontally_5)/* data */
	, &WebCamAbstractBehaviour_t63__CustomAttributeCache_mFlipHorizontally/* custom_attributes_cache */

};
extern Il2CppType Boolean_t122_0_0_1;
extern CustomAttributesCache WebCamAbstractBehaviour_t63__CustomAttributeCache_mTurnOffWebCam;
FieldInfo WebCamAbstractBehaviour_t63____mTurnOffWebCam_6_FieldInfo = 
{
	"mTurnOffWebCam"/* name */
	, &Boolean_t122_0_0_1/* type */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamAbstractBehaviour_t63, ___mTurnOffWebCam_6)/* data */
	, &WebCamAbstractBehaviour_t63__CustomAttributeCache_mTurnOffWebCam/* custom_attributes_cache */

};
extern Il2CppType WebCamImpl_t648_0_0_1;
FieldInfo WebCamAbstractBehaviour_t63____mWebCamImpl_7_FieldInfo = 
{
	"mWebCamImpl"/* name */
	, &WebCamImpl_t648_0_0_1/* type */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamAbstractBehaviour_t63, ___mWebCamImpl_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Camera_t168_0_0_1;
FieldInfo WebCamAbstractBehaviour_t63____mBackgroundCameraInstance_8_FieldInfo = 
{
	"mBackgroundCameraInstance"/* name */
	, &Camera_t168_0_0_1/* type */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamAbstractBehaviour_t63, ___mBackgroundCameraInstance_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* WebCamAbstractBehaviour_t63_FieldInfos[] =
{
	&WebCamAbstractBehaviour_t63____RenderTextureLayer_2_FieldInfo,
	&WebCamAbstractBehaviour_t63____mPlayModeRenderVideo_3_FieldInfo,
	&WebCamAbstractBehaviour_t63____mDeviceNameSetInEditor_4_FieldInfo,
	&WebCamAbstractBehaviour_t63____mFlipHorizontally_5_FieldInfo,
	&WebCamAbstractBehaviour_t63____mTurnOffWebCam_6_FieldInfo,
	&WebCamAbstractBehaviour_t63____mWebCamImpl_7_FieldInfo,
	&WebCamAbstractBehaviour_t63____mBackgroundCameraInstance_8_FieldInfo,
	NULL
};
static PropertyInfo WebCamAbstractBehaviour_t63____PlayModeRenderVideo_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* parent */
	, "PlayModeRenderVideo"/* name */
	, &WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4396_MethodInfo/* get */
	, &WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4397_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WebCamAbstractBehaviour_t63____DeviceName_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* parent */
	, "DeviceName"/* name */
	, &WebCamAbstractBehaviour_get_DeviceName_m4398_MethodInfo/* get */
	, &WebCamAbstractBehaviour_set_DeviceName_m4399_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WebCamAbstractBehaviour_t63____FlipHorizontally_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* parent */
	, "FlipHorizontally"/* name */
	, &WebCamAbstractBehaviour_get_FlipHorizontally_m4400_MethodInfo/* get */
	, &WebCamAbstractBehaviour_set_FlipHorizontally_m4401_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WebCamAbstractBehaviour_t63____TurnOffWebCam_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* parent */
	, "TurnOffWebCam"/* name */
	, &WebCamAbstractBehaviour_get_TurnOffWebCam_m4402_MethodInfo/* get */
	, &WebCamAbstractBehaviour_set_TurnOffWebCam_m4403_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WebCamAbstractBehaviour_t63____IsPlaying_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* parent */
	, "IsPlaying"/* name */
	, &WebCamAbstractBehaviour_get_IsPlaying_m4404_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WebCamAbstractBehaviour_t63____ImplementationClass_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* parent */
	, "ImplementationClass"/* name */
	, &WebCamAbstractBehaviour_get_ImplementationClass_m4406_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* WebCamAbstractBehaviour_t63_PropertyInfos[] =
{
	&WebCamAbstractBehaviour_t63____PlayModeRenderVideo_PropertyInfo,
	&WebCamAbstractBehaviour_t63____DeviceName_PropertyInfo,
	&WebCamAbstractBehaviour_t63____FlipHorizontally_PropertyInfo,
	&WebCamAbstractBehaviour_t63____TurnOffWebCam_PropertyInfo,
	&WebCamAbstractBehaviour_t63____IsPlaying_PropertyInfo,
	&WebCamAbstractBehaviour_t63____ImplementationClass_PropertyInfo,
	NULL
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_PlayModeRenderVideo()
MethodInfo WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4396_MethodInfo = 
{
	"get_PlayModeRenderVideo"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4396/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo WebCamAbstractBehaviour_t63_WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4397_ParameterInfos[] = 
{
	{"value", 0, 134220050, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::set_PlayModeRenderVideo(System.Boolean)
MethodInfo WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4397_MethodInfo = 
{
	"set_PlayModeRenderVideo"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4397/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125/* invoker_method */
	, WebCamAbstractBehaviour_t63_WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4397_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.WebCamAbstractBehaviour::get_DeviceName()
MethodInfo WebCamAbstractBehaviour_get_DeviceName_m4398_MethodInfo = 
{
	"get_DeviceName"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_DeviceName_m4398/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo WebCamAbstractBehaviour_t63_WebCamAbstractBehaviour_set_DeviceName_m4399_ParameterInfos[] = 
{
	{"value", 0, 134220051, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::set_DeviceName(System.String)
MethodInfo WebCamAbstractBehaviour_set_DeviceName_m4399_MethodInfo = 
{
	"set_DeviceName"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_set_DeviceName_m4399/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, WebCamAbstractBehaviour_t63_WebCamAbstractBehaviour_set_DeviceName_m4399_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_FlipHorizontally()
MethodInfo WebCamAbstractBehaviour_get_FlipHorizontally_m4400_MethodInfo = 
{
	"get_FlipHorizontally"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_FlipHorizontally_m4400/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo WebCamAbstractBehaviour_t63_WebCamAbstractBehaviour_set_FlipHorizontally_m4401_ParameterInfos[] = 
{
	{"value", 0, 134220052, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::set_FlipHorizontally(System.Boolean)
MethodInfo WebCamAbstractBehaviour_set_FlipHorizontally_m4401_MethodInfo = 
{
	"set_FlipHorizontally"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_set_FlipHorizontally_m4401/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125/* invoker_method */
	, WebCamAbstractBehaviour_t63_WebCamAbstractBehaviour_set_FlipHorizontally_m4401_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_TurnOffWebCam()
MethodInfo WebCamAbstractBehaviour_get_TurnOffWebCam_m4402_MethodInfo = 
{
	"get_TurnOffWebCam"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_TurnOffWebCam_m4402/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo WebCamAbstractBehaviour_t63_WebCamAbstractBehaviour_set_TurnOffWebCam_m4403_ParameterInfos[] = 
{
	{"value", 0, 134220053, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::set_TurnOffWebCam(System.Boolean)
MethodInfo WebCamAbstractBehaviour_set_TurnOffWebCam_m4403_MethodInfo = 
{
	"set_TurnOffWebCam"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_set_TurnOffWebCam_m4403/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125/* invoker_method */
	, WebCamAbstractBehaviour_t63_WebCamAbstractBehaviour_set_TurnOffWebCam_m4403_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_IsPlaying()
MethodInfo WebCamAbstractBehaviour_get_IsPlaying_m4404_MethodInfo = 
{
	"get_IsPlaying"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_IsPlaying_m4404/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::IsWebCamUsed()
MethodInfo WebCamAbstractBehaviour_IsWebCamUsed_m4405_MethodInfo = 
{
	"IsWebCamUsed"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_IsWebCamUsed_m4405/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WebCamImpl_t648_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.WebCamImpl Vuforia.WebCamAbstractBehaviour::get_ImplementationClass()
MethodInfo WebCamAbstractBehaviour_get_ImplementationClass_m4406_MethodInfo = 
{
	"get_ImplementationClass"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_ImplementationClass_m4406/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &WebCamImpl_t648_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::InitCamera()
MethodInfo WebCamAbstractBehaviour_InitCamera_m4407_MethodInfo = 
{
	"InitCamera"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_InitCamera_m4407/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::CheckNativePluginSupport()
MethodInfo WebCamAbstractBehaviour_CheckNativePluginSupport_m4408_MethodInfo = 
{
	"CheckNativePluginSupport"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_CheckNativePluginSupport_m4408/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::OnLevelWasLoaded()
MethodInfo WebCamAbstractBehaviour_OnLevelWasLoaded_m4409_MethodInfo = 
{
	"OnLevelWasLoaded"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_OnLevelWasLoaded_m4409/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::OnDestroy()
MethodInfo WebCamAbstractBehaviour_OnDestroy_m4410_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_OnDestroy_m4410/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::Update()
MethodInfo WebCamAbstractBehaviour_Update_m4411_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_Update_m4411/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Int32 Vuforia.WebCamAbstractBehaviour::qcarCheckNativePluginSupport()
MethodInfo WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4412_MethodInfo = 
{
	"qcarCheckNativePluginSupport"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4412/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 8337/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::.ctor()
MethodInfo WebCamAbstractBehaviour__ctor_m514_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour__ctor_m514/* method */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* WebCamAbstractBehaviour_t63_MethodInfos[] =
{
	&WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4396_MethodInfo,
	&WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4397_MethodInfo,
	&WebCamAbstractBehaviour_get_DeviceName_m4398_MethodInfo,
	&WebCamAbstractBehaviour_set_DeviceName_m4399_MethodInfo,
	&WebCamAbstractBehaviour_get_FlipHorizontally_m4400_MethodInfo,
	&WebCamAbstractBehaviour_set_FlipHorizontally_m4401_MethodInfo,
	&WebCamAbstractBehaviour_get_TurnOffWebCam_m4402_MethodInfo,
	&WebCamAbstractBehaviour_set_TurnOffWebCam_m4403_MethodInfo,
	&WebCamAbstractBehaviour_get_IsPlaying_m4404_MethodInfo,
	&WebCamAbstractBehaviour_IsWebCamUsed_m4405_MethodInfo,
	&WebCamAbstractBehaviour_get_ImplementationClass_m4406_MethodInfo,
	&WebCamAbstractBehaviour_InitCamera_m4407_MethodInfo,
	&WebCamAbstractBehaviour_CheckNativePluginSupport_m4408_MethodInfo,
	&WebCamAbstractBehaviour_OnLevelWasLoaded_m4409_MethodInfo,
	&WebCamAbstractBehaviour_OnDestroy_m4410_MethodInfo,
	&WebCamAbstractBehaviour_Update_m4411_MethodInfo,
	&WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4412_MethodInfo,
	&WebCamAbstractBehaviour__ctor_m514_MethodInfo,
	NULL
};
static MethodInfo* WebCamAbstractBehaviour_t63_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
};
void WebCamAbstractBehaviour_t63_CustomAttributesCacheGenerator_mPlayModeRenderVideo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void WebCamAbstractBehaviour_t63_CustomAttributesCacheGenerator_mDeviceNameSetInEditor(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void WebCamAbstractBehaviour_t63_CustomAttributesCacheGenerator_mFlipHorizontally(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void WebCamAbstractBehaviour_t63_CustomAttributesCacheGenerator_mTurnOffWebCam(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache WebCamAbstractBehaviour_t63__CustomAttributeCache_mPlayModeRenderVideo = {
2,
NULL,
&WebCamAbstractBehaviour_t63_CustomAttributesCacheGenerator_mPlayModeRenderVideo
};
CustomAttributesCache WebCamAbstractBehaviour_t63__CustomAttributeCache_mDeviceNameSetInEditor = {
2,
NULL,
&WebCamAbstractBehaviour_t63_CustomAttributesCacheGenerator_mDeviceNameSetInEditor
};
CustomAttributesCache WebCamAbstractBehaviour_t63__CustomAttributeCache_mFlipHorizontally = {
2,
NULL,
&WebCamAbstractBehaviour_t63_CustomAttributesCacheGenerator_mFlipHorizontally
};
CustomAttributesCache WebCamAbstractBehaviour_t63__CustomAttributeCache_mTurnOffWebCam = {
2,
NULL,
&WebCamAbstractBehaviour_t63_CustomAttributesCacheGenerator_mTurnOffWebCam
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WebCamAbstractBehaviour_t63_1_0_0;
struct WebCamAbstractBehaviour_t63;
extern CustomAttributesCache WebCamAbstractBehaviour_t63__CustomAttributeCache_mPlayModeRenderVideo;
extern CustomAttributesCache WebCamAbstractBehaviour_t63__CustomAttributeCache_mDeviceNameSetInEditor;
extern CustomAttributesCache WebCamAbstractBehaviour_t63__CustomAttributeCache_mFlipHorizontally;
extern CustomAttributesCache WebCamAbstractBehaviour_t63__CustomAttributeCache_mTurnOffWebCam;
TypeInfo WebCamAbstractBehaviour_t63_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebCamAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WebCamAbstractBehaviour_t63_MethodInfos/* methods */
	, WebCamAbstractBehaviour_t63_PropertyInfos/* properties */
	, WebCamAbstractBehaviour_t63_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t10_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, WebCamAbstractBehaviour_t63_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WebCamAbstractBehaviour_t63_il2cpp_TypeInfo/* cast_class */
	, &WebCamAbstractBehaviour_t63_0_0_0/* byval_arg */
	, &WebCamAbstractBehaviour_t63_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebCamAbstractBehaviour_t63)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 18/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavio.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WordAbstractBehaviour_t34_il2cpp_TypeInfo;
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavioMethodDeclarations.h"

// Vuforia.WordTemplateMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilter.h"
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviourMethodDeclarations.h"
extern MethodInfo Word_get_Size_m5105_MethodInfo;
extern MethodInfo Vector3_get_one_m4540_MethodInfo;
extern MethodInfo Component_GetComponent_TisMeshFilter_t84_m486_MethodInfo;
extern MethodInfo MeshFilter_get_sharedMesh_m525_MethodInfo;
extern MethodInfo Mesh_get_bounds_m5567_MethodInfo;
extern MethodInfo Bounds_get_size_m2555_MethodInfo;
extern MethodInfo TrackableBehaviour__ctor_m2803_MethodInfo;
struct Component_t128;
// UnityEngine.CastHelper`1<UnityEngine.MeshFilter>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_4.h"
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t84_m486(__this, method) (MeshFilter_t84 *)Component_GetComponent_TisObject_t_m280_gshared((Component_t128 *)__this, method)


// System.Void Vuforia.WordAbstractBehaviour::InternalUnregisterTrackable()
extern MethodInfo WordAbstractBehaviour_InternalUnregisterTrackable_m553_MethodInfo;
 void WordAbstractBehaviour_InternalUnregisterTrackable_m553 (WordAbstractBehaviour_t34 * __this, MethodInfo* method){
	Object_t * V_0 = {0};
	{
		V_0 = (Object_t *)NULL;
		__this->___mWord_11 = (Object_t *)NULL;
		__this->___mTrackable_7 = V_0;
		return;
	}
}
// Vuforia.Word Vuforia.WordAbstractBehaviour::get_Word()
extern MethodInfo WordAbstractBehaviour_get_Word_m4413_MethodInfo;
 Object_t * WordAbstractBehaviour_get_Word_m4413 (WordAbstractBehaviour_t34 * __this, MethodInfo* method){
	{
		Object_t * L_0 = (__this->___mWord_11);
		return L_0;
	}
}
// System.String Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_SpecificWord()
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m554_MethodInfo;
 String_t* WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m554 (WordAbstractBehaviour_t34 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mSpecificWord_10);
		return L_0;
	}
}
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetSpecificWord(System.String)
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m555_MethodInfo;
 void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m555 (WordAbstractBehaviour_t34 * __this, String_t* ___word, MethodInfo* method){
	{
		__this->___mSpecificWord_10 = ___word;
		return;
	}
}
// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_Mode()
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m556_MethodInfo;
 int32_t WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m556 (WordAbstractBehaviour_t34 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___mMode_9);
		return L_0;
	}
}
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsTemplateMode()
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m557_MethodInfo;
 bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m557 (WordAbstractBehaviour_t34 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___mMode_9);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode()
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m558_MethodInfo;
 bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m558 (WordAbstractBehaviour_t34 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___mMode_9);
		return ((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetMode(Vuforia.WordTemplateMode)
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m559_MethodInfo;
 void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m559 (WordAbstractBehaviour_t34 * __this, int32_t ___mode, MethodInfo* method){
	{
		__this->___mMode_9 = ___mode;
		return;
	}
}
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.InitializeWord(Vuforia.Word)
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m560_MethodInfo;
 void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m560 (WordAbstractBehaviour_t34 * __this, Object_t * ___word, MethodInfo* method){
	Vector2_t99  V_0 = {0};
	Vector3_t73  V_1 = {0};
	MeshFilter_t84 * V_2 = {0};
	float V_3 = 0.0f;
	Object_t * V_4 = {0};
	Bounds_t198  V_5 = {0};
	{
		Object_t * L_0 = ___word;
		V_4 = L_0;
		__this->___mWord_11 = L_0;
		__this->___mTrackable_7 = V_4;
		NullCheck(___word);
		Vector2_t99  L_1 = (Vector2_t99 )InterfaceFuncInvoker0< Vector2_t99  >::Invoke(&Word_get_Size_m5105_MethodInfo, ___word);
		V_0 = L_1;
		Vector3_t73  L_2 = Vector3_get_one_m4540(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_one_m4540_MethodInfo);
		V_1 = L_2;
		MeshFilter_t84 * L_3 = Component_GetComponent_TisMeshFilter_t84_m486(__this, /*hidden argument*/&Component_GetComponent_TisMeshFilter_t84_m486_MethodInfo);
		V_2 = L_3;
		bool L_4 = Object_op_Inequality_m489(NULL /*static, unused*/, V_2, (Object_t120 *)NULL, /*hidden argument*/&Object_op_Inequality_m489_MethodInfo);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		NullCheck(V_2);
		Mesh_t174 * L_5 = MeshFilter_get_sharedMesh_m525(V_2, /*hidden argument*/&MeshFilter_get_sharedMesh_m525_MethodInfo);
		NullCheck(L_5);
		Bounds_t198  L_6 = Mesh_get_bounds_m5567(L_5, /*hidden argument*/&Mesh_get_bounds_m5567_MethodInfo);
		V_5 = L_6;
		Vector3_t73  L_7 = Bounds_get_size_m2555((&V_5), /*hidden argument*/&Bounds_get_size_m2555_MethodInfo);
		V_1 = L_7;
	}

IL_0044:
	{
		NullCheck((&V_0));
		float L_8 = ((&V_0)->___y_2);
		NullCheck((&V_1));
		float L_9 = ((&V_1)->___z_3);
		V_3 = ((float)((float)L_8/(float)L_9));
		Transform_t74 * L_10 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		Vector3_t73  L_11 = {0};
		Vector3__ctor_m568(&L_11, V_3, V_3, V_3, /*hidden argument*/&Vector3__ctor_m568_MethodInfo);
		NullCheck(L_10);
		Transform_set_localScale_m2507(L_10, L_11, /*hidden argument*/&Transform_set_localScale_m2507_MethodInfo);
		return;
	}
}
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
extern MethodInfo WordAbstractBehaviour__ctor_m548_MethodInfo;
 void WordAbstractBehaviour__ctor_m548 (WordAbstractBehaviour_t34 * __this, MethodInfo* method){
	{
		TrackableBehaviour__ctor_m2803(__this, /*hidden argument*/&TrackableBehaviour__ctor_m2803_MethodInfo);
		return;
	}
}
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m549_MethodInfo;
 bool WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m549 (WordAbstractBehaviour_t34 * __this, MethodInfo* method){
	{
		bool L_0 = Behaviour_get_enabled_m536(__this, /*hidden argument*/&Behaviour_get_enabled_m536_MethodInfo);
		return L_0;
	}
}
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m550_MethodInfo;
 void WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m550 (WordAbstractBehaviour_t34 * __this, bool p0, MethodInfo* method){
	{
		Behaviour_set_enabled_m547(__this, p0, /*hidden argument*/&Behaviour_set_enabled_m547_MethodInfo);
		return;
	}
}
// UnityEngine.Transform Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m551_MethodInfo;
 Transform_t74 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m551 (WordAbstractBehaviour_t34 * __this, MethodInfo* method){
	{
		Transform_t74 * L_0 = Component_get_transform_m487(__this, /*hidden argument*/&Component_get_transform_m487_MethodInfo);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m552_MethodInfo;
 GameObject_t29 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m552 (WordAbstractBehaviour_t34 * __this, MethodInfo* method){
	{
		GameObject_t29 * L_0 = Component_get_gameObject_m419(__this, /*hidden argument*/&Component_get_gameObject_m419_MethodInfo);
		return L_0;
	}
}
// Metadata Definition Vuforia.WordAbstractBehaviour
extern Il2CppType WordTemplateMode_t651_0_0_1;
extern CustomAttributesCache WordAbstractBehaviour_t34__CustomAttributeCache_mMode;
FieldInfo WordAbstractBehaviour_t34____mMode_9_FieldInfo = 
{
	"mMode"/* name */
	, &WordTemplateMode_t651_0_0_1/* type */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* parent */
	, offsetof(WordAbstractBehaviour_t34, ___mMode_9)/* data */
	, &WordAbstractBehaviour_t34__CustomAttributeCache_mMode/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache WordAbstractBehaviour_t34__CustomAttributeCache_mSpecificWord;
FieldInfo WordAbstractBehaviour_t34____mSpecificWord_10_FieldInfo = 
{
	"mSpecificWord"/* name */
	, &String_t_0_0_1/* type */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* parent */
	, offsetof(WordAbstractBehaviour_t34, ___mSpecificWord_10)/* data */
	, &WordAbstractBehaviour_t34__CustomAttributeCache_mSpecificWord/* custom_attributes_cache */

};
extern Il2CppType Word_t736_0_0_1;
FieldInfo WordAbstractBehaviour_t34____mWord_11_FieldInfo = 
{
	"mWord"/* name */
	, &Word_t736_0_0_1/* type */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* parent */
	, offsetof(WordAbstractBehaviour_t34, ___mWord_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* WordAbstractBehaviour_t34_FieldInfos[] =
{
	&WordAbstractBehaviour_t34____mMode_9_FieldInfo,
	&WordAbstractBehaviour_t34____mSpecificWord_10_FieldInfo,
	&WordAbstractBehaviour_t34____mWord_11_FieldInfo,
	NULL
};
static PropertyInfo WordAbstractBehaviour_t34____Word_PropertyInfo = 
{
	&WordAbstractBehaviour_t34_il2cpp_TypeInfo/* parent */
	, "Word"/* name */
	, &WordAbstractBehaviour_get_Word_m4413_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WordAbstractBehaviour_t34____Vuforia_IEditorWordBehaviour_SpecificWord_PropertyInfo = 
{
	&WordAbstractBehaviour_t34_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorWordBehaviour.SpecificWord"/* name */
	, &WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m554_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WordAbstractBehaviour_t34____Vuforia_IEditorWordBehaviour_Mode_PropertyInfo = 
{
	&WordAbstractBehaviour_t34_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorWordBehaviour.Mode"/* name */
	, &WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m556_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WordAbstractBehaviour_t34____Vuforia_IEditorWordBehaviour_IsTemplateMode_PropertyInfo = 
{
	&WordAbstractBehaviour_t34_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorWordBehaviour.IsTemplateMode"/* name */
	, &WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m557_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WordAbstractBehaviour_t34____Vuforia_IEditorWordBehaviour_IsSpecificWordMode_PropertyInfo = 
{
	&WordAbstractBehaviour_t34_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorWordBehaviour.IsSpecificWordMode"/* name */
	, &WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m558_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* WordAbstractBehaviour_t34_PropertyInfos[] =
{
	&WordAbstractBehaviour_t34____Word_PropertyInfo,
	&WordAbstractBehaviour_t34____Vuforia_IEditorWordBehaviour_SpecificWord_PropertyInfo,
	&WordAbstractBehaviour_t34____Vuforia_IEditorWordBehaviour_Mode_PropertyInfo,
	&WordAbstractBehaviour_t34____Vuforia_IEditorWordBehaviour_IsTemplateMode_PropertyInfo,
	&WordAbstractBehaviour_t34____Vuforia_IEditorWordBehaviour_IsSpecificWordMode_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::InternalUnregisterTrackable()
MethodInfo WordAbstractBehaviour_InternalUnregisterTrackable_m553_MethodInfo = 
{
	"InternalUnregisterTrackable"/* name */
	, (methodPointerType)&WordAbstractBehaviour_InternalUnregisterTrackable_m553/* method */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Word_t736_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.Word Vuforia.WordAbstractBehaviour::get_Word()
MethodInfo WordAbstractBehaviour_get_Word_m4413_MethodInfo = 
{
	"get_Word"/* name */
	, (methodPointerType)&WordAbstractBehaviour_get_Word_m4413/* method */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* declaring_type */
	, &Word_t736_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_SpecificWord()
MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m554_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.get_SpecificWord"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m554/* method */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo WordAbstractBehaviour_t34_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m555_ParameterInfos[] = 
{
	{"word", 0, 134220054, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetSpecificWord(System.String)
MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m555_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.SetSpecificWord"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m555/* method */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, WordAbstractBehaviour_t34_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m555_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WordTemplateMode_t651_0_0_0;
extern void* RuntimeInvoker_WordTemplateMode_t651 (MethodInfo* method, void* obj, void** args);
// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_Mode()
MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m556_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.get_Mode"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m556/* method */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* declaring_type */
	, &WordTemplateMode_t651_0_0_0/* return_type */
	, RuntimeInvoker_WordTemplateMode_t651/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsTemplateMode()
MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m557_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.get_IsTemplateMode"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m557/* method */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode()
MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m558_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m558/* method */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WordTemplateMode_t651_0_0_0;
extern Il2CppType WordTemplateMode_t651_0_0_0;
static ParameterInfo WordAbstractBehaviour_t34_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m559_ParameterInfos[] = 
{
	{"mode", 0, 134220055, &EmptyCustomAttributesCache, &WordTemplateMode_t651_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetMode(Vuforia.WordTemplateMode)
MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m559_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.SetMode"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m559/* method */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, WordAbstractBehaviour_t34_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m559_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Word_t736_0_0_0;
extern Il2CppType Word_t736_0_0_0;
static ParameterInfo WordAbstractBehaviour_t34_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m560_ParameterInfos[] = 
{
	{"word", 0, 134220056, &EmptyCustomAttributesCache, &Word_t736_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.InitializeWord(Vuforia.Word)
MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m560_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.InitializeWord"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m560/* method */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, WordAbstractBehaviour_t34_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m560_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
MethodInfo WordAbstractBehaviour__ctor_m548_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WordAbstractBehaviour__ctor_m548/* method */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m549_MethodInfo = 
{
	"Vuforia.IEditorTrackableBehaviour.get_enabled"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m549/* method */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
static ParameterInfo WordAbstractBehaviour_t34_WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m550_ParameterInfos[] = 
{
	{"", 0, 134217728, &EmptyCustomAttributesCache, &Boolean_t122_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_SByte_t125 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m550_MethodInfo = 
{
	"Vuforia.IEditorTrackableBehaviour.set_enabled"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m550/* method */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_SByte_t125/* invoker_method */
	, WordAbstractBehaviour_t34_WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m550_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Transform_t74_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m551_MethodInfo = 
{
	"Vuforia.IEditorTrackableBehaviour.get_transform"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m551/* method */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t74_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t29_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.GameObject Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m552_MethodInfo = 
{
	"Vuforia.IEditorTrackableBehaviour.get_gameObject"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m552/* method */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* WordAbstractBehaviour_t34_MethodInfos[] =
{
	&WordAbstractBehaviour_InternalUnregisterTrackable_m553_MethodInfo,
	&WordAbstractBehaviour_get_Word_m4413_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m554_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m555_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m556_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m557_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m558_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m559_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m560_MethodInfo,
	&WordAbstractBehaviour__ctor_m548_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m549_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m550_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m551_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m552_MethodInfo,
	NULL
};
extern MethodInfo TrackableBehaviour_get_TrackableName_m204_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m205_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m206_MethodInfo;
extern MethodInfo TrackableBehaviour_get_Trackable_m207_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m208_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m209_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m210_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m211_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m212_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m213_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m214_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m219_MethodInfo;
extern MethodInfo TrackableBehaviour_OnTrackerUpdate_m354_MethodInfo;
extern MethodInfo TrackableBehaviour_OnFrameIndexUpdate_m296_MethodInfo;
extern MethodInfo TrackableBehaviour_CorrectScaleImpl_m372_MethodInfo;
static MethodInfo* WordAbstractBehaviour_t34_VTable[] =
{
	&Object_Equals_m197_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m199_MethodInfo,
	&Object_ToString_m200_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m204_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m205_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m206_MethodInfo,
	&TrackableBehaviour_get_Trackable_m207_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m208_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m209_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m210_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m211_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m212_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m213_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m214_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m549_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m550_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m551_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m552_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m219_MethodInfo,
	&TrackableBehaviour_get_Trackable_m207_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m354_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m296_MethodInfo,
	&WordAbstractBehaviour_InternalUnregisterTrackable_m553_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m372_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m554_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m555_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m556_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m557_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m558_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m559_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m560_MethodInfo,
};
extern TypeInfo IEditorWordBehaviour_t178_il2cpp_TypeInfo;
extern TypeInfo IEditorTrackableBehaviour_t115_il2cpp_TypeInfo;
static TypeInfo* WordAbstractBehaviour_t34_InterfacesTypeInfos[] = 
{
	&IEditorWordBehaviour_t178_il2cpp_TypeInfo,
	&IEditorTrackableBehaviour_t115_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair WordAbstractBehaviour_t34_InterfacesOffsets[] = 
{
	{ &IEditorTrackableBehaviour_t115_il2cpp_TypeInfo, 4},
	{ &IEditorWordBehaviour_t178_il2cpp_TypeInfo, 25},
};
void WordAbstractBehaviour_t34_CustomAttributesCacheGenerator_mMode(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void WordAbstractBehaviour_t34_CustomAttributesCacheGenerator_mSpecificWord(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t469 * tmp;
		tmp = (SerializeField_t469 *)il2cpp_codegen_object_new (&SerializeField_t469_il2cpp_TypeInfo);
		SerializeField__ctor_m2084(tmp, &SerializeField__ctor_m2084_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t204 * tmp;
		tmp = (HideInInspector_t204 *)il2cpp_codegen_object_new (&HideInInspector_t204_il2cpp_TypeInfo);
		HideInInspector__ctor_m721(tmp, &HideInInspector__ctor_m721_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache WordAbstractBehaviour_t34__CustomAttributeCache_mMode = {
2,
NULL,
&WordAbstractBehaviour_t34_CustomAttributesCacheGenerator_mMode
};
CustomAttributesCache WordAbstractBehaviour_t34__CustomAttributeCache_mSpecificWord = {
2,
NULL,
&WordAbstractBehaviour_t34_CustomAttributesCacheGenerator_mSpecificWord
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WordAbstractBehaviour_t34_0_0_0;
extern Il2CppType WordAbstractBehaviour_t34_1_0_0;
struct WordAbstractBehaviour_t34;
extern CustomAttributesCache WordAbstractBehaviour_t34__CustomAttributeCache_mMode;
extern CustomAttributesCache WordAbstractBehaviour_t34__CustomAttributeCache_mSpecificWord;
TypeInfo WordAbstractBehaviour_t34_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WordAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WordAbstractBehaviour_t34_MethodInfos/* methods */
	, WordAbstractBehaviour_t34_PropertyInfos/* properties */
	, WordAbstractBehaviour_t34_FieldInfos/* fields */
	, NULL/* events */
	, &TrackableBehaviour_t17_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* element_class */
	, WordAbstractBehaviour_t34_InterfacesTypeInfos/* implemented_interfaces */
	, WordAbstractBehaviour_t34_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WordAbstractBehaviour_t34_il2cpp_TypeInfo/* cast_class */
	, &WordAbstractBehaviour_t34_0_0_0/* byval_arg */
	, &WordAbstractBehaviour_t34_1_0_0/* this_arg */
	, WordAbstractBehaviour_t34_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WordAbstractBehaviour_t34)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 14/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 32/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
// Vuforia.WordFilterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilterModeMethodDeclarations.h"



// Metadata Definition Vuforia.WordFilterMode
extern Il2CppType Int32_t123_0_0_1542;
FieldInfo WordFilterMode_t816____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t123_0_0_1542/* type */
	, &WordFilterMode_t816_il2cpp_TypeInfo/* parent */
	, offsetof(WordFilterMode_t816, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType WordFilterMode_t816_0_0_32854;
FieldInfo WordFilterMode_t816____NONE_2_FieldInfo = 
{
	"NONE"/* name */
	, &WordFilterMode_t816_0_0_32854/* type */
	, &WordFilterMode_t816_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType WordFilterMode_t816_0_0_32854;
FieldInfo WordFilterMode_t816____BLACK_LIST_3_FieldInfo = 
{
	"BLACK_LIST"/* name */
	, &WordFilterMode_t816_0_0_32854/* type */
	, &WordFilterMode_t816_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType WordFilterMode_t816_0_0_32854;
FieldInfo WordFilterMode_t816____WHITE_LIST_4_FieldInfo = 
{
	"WHITE_LIST"/* name */
	, &WordFilterMode_t816_0_0_32854/* type */
	, &WordFilterMode_t816_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* WordFilterMode_t816_FieldInfos[] =
{
	&WordFilterMode_t816____value___1_FieldInfo,
	&WordFilterMode_t816____NONE_2_FieldInfo,
	&WordFilterMode_t816____BLACK_LIST_3_FieldInfo,
	&WordFilterMode_t816____WHITE_LIST_4_FieldInfo,
	NULL
};
static const int32_t WordFilterMode_t816____NONE_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry WordFilterMode_t816____NONE_2_DefaultValue = 
{
	&WordFilterMode_t816____NONE_2_FieldInfo/* field */
	, { (char*)&WordFilterMode_t816____NONE_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t WordFilterMode_t816____BLACK_LIST_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry WordFilterMode_t816____BLACK_LIST_3_DefaultValue = 
{
	&WordFilterMode_t816____BLACK_LIST_3_FieldInfo/* field */
	, { (char*)&WordFilterMode_t816____BLACK_LIST_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t WordFilterMode_t816____WHITE_LIST_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry WordFilterMode_t816____WHITE_LIST_4_DefaultValue = 
{
	&WordFilterMode_t816____WHITE_LIST_4_FieldInfo/* field */
	, { (char*)&WordFilterMode_t816____WHITE_LIST_4_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* WordFilterMode_t816_FieldDefaultValues[] = 
{
	&WordFilterMode_t816____NONE_2_DefaultValue,
	&WordFilterMode_t816____BLACK_LIST_3_DefaultValue,
	&WordFilterMode_t816____WHITE_LIST_4_DefaultValue,
	NULL
};
static MethodInfo* WordFilterMode_t816_MethodInfos[] =
{
	NULL
};
static MethodInfo* WordFilterMode_t816_VTable[] =
{
	&Enum_Equals_m587_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Enum_GetHashCode_m588_MethodInfo,
	&Enum_ToString_m589_MethodInfo,
	&Enum_ToString_m590_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m591_MethodInfo,
	&Enum_System_IConvertible_ToByte_m592_MethodInfo,
	&Enum_System_IConvertible_ToChar_m593_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m594_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m595_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m596_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m597_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m598_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m599_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m600_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m601_MethodInfo,
	&Enum_ToString_m602_MethodInfo,
	&Enum_System_IConvertible_ToType_m603_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m604_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m605_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m606_MethodInfo,
	&Enum_CompareTo_m607_MethodInfo,
	&Enum_GetTypeCode_m608_MethodInfo,
};
static Il2CppInterfaceOffsetPair WordFilterMode_t816_InterfacesOffsets[] = 
{
	{ &IFormattable_t182_il2cpp_TypeInfo, 4},
	{ &IConvertible_t183_il2cpp_TypeInfo, 5},
	{ &IComparable_t184_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WordFilterMode_t816_1_0_0;
TypeInfo WordFilterMode_t816_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WordFilterMode"/* name */
	, "Vuforia"/* namespaze */
	, WordFilterMode_t816_MethodInfos/* methods */
	, NULL/* properties */
	, WordFilterMode_t816_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t185_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, WordFilterMode_t816_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t123_il2cpp_TypeInfo/* cast_class */
	, &WordFilterMode_t816_0_0_0/* byval_arg */
	, &WordFilterMode_t816_1_0_0/* this_arg */
	, WordFilterMode_t816_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, WordFilterMode_t816_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WordFilterMode_t816)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImplementationDet.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo __StaticArrayInitTypeSizeU3D24_t817_il2cpp_TypeInfo;
// <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImplementationDetMethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
void __StaticArrayInitTypeSizeU3D24_t817_marshal(const __StaticArrayInitTypeSizeU3D24_t817& unmarshaled, __StaticArrayInitTypeSizeU3D24_t817_marshaled& marshaled)
{
}
void __StaticArrayInitTypeSizeU3D24_t817_marshal_back(const __StaticArrayInitTypeSizeU3D24_t817_marshaled& marshaled, __StaticArrayInitTypeSizeU3D24_t817& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
void __StaticArrayInitTypeSizeU3D24_t817_marshal_cleanup(__StaticArrayInitTypeSizeU3D24_t817_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
static MethodInfo* __StaticArrayInitTypeSizeU3D24_t817_MethodInfos[] =
{
	NULL
};
static MethodInfo* __StaticArrayInitTypeSizeU3D24_t817_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType __StaticArrayInitTypeSizeU3D24_t817_0_0_0;
extern Il2CppType __StaticArrayInitTypeSizeU3D24_t817_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_il2cpp_TypeInfo;
TypeInfo __StaticArrayInitTypeSizeU3D24_t817_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "__StaticArrayInitTypeSize=24"/* name */
	, ""/* namespaze */
	, __StaticArrayInitTypeSizeU3D24_t817_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_il2cpp_TypeInfo/* nested_in */
	, &__StaticArrayInitTypeSizeU3D24_t817_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, __StaticArrayInitTypeSizeU3D24_t817_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &__StaticArrayInitTypeSizeU3D24_t817_il2cpp_TypeInfo/* cast_class */
	, &__StaticArrayInitTypeSizeU3D24_t817_0_0_0/* byval_arg */
	, &__StaticArrayInitTypeSizeU3D24_t817_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D24_t817_marshal/* marshal_to_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D24_t817_marshal_back/* marshal_from_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D24_t817_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (__StaticArrayInitTypeSizeU3D24_t817)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, sizeof(__StaticArrayInitTypeSizeU3D24_t817_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImplementationDet_0.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImplementationDet_0MethodDeclarations.h"



// Metadata Definition <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}
extern Il2CppType __StaticArrayInitTypeSizeU3D24_t817_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818____$$method0x6000856U2D1_0_FieldInfo = 
{
	"$$method0x6000856-1"/* name */
	, &__StaticArrayInitTypeSizeU3D24_t817_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_StaticFields, ___$$method0x6000856U2D1_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_FieldInfos[] =
{
	&U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818____$$method0x6000856U2D1_0_FieldInfo,
	NULL
};
static const uint8_t U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818____$$method0x6000856U2D1_0_DefaultValueData[] = { 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x3, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818____$$method0x6000856U2D1_0_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818____$$method0x6000856U2D1_0_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818____$$method0x6000856U2D1_0_DefaultValueData, &__StaticArrayInitTypeSizeU3D24_t817_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_FieldDefaultValues[] = 
{
	&U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818____$$method0x6000856U2D1_0_DefaultValue,
	NULL
};
static MethodInfo* U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_MethodInfos[] =
{
	NULL
};
extern TypeInfo __StaticArrayInitTypeSizeU3D24_t817_il2cpp_TypeInfo;
static TypeInfo* U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_il2cpp_TypeInfo__nestedTypes[2] =
{
	&__StaticArrayInitTypeSizeU3D24_t817_il2cpp_TypeInfo,
	NULL
};
static MethodInfo* U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
void U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818__CustomAttributeCache = {
1,
NULL,
&U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_CustomAttributesCacheGenerator
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_0_0_0;
extern Il2CppType U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_1_0_0;
struct U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818;
extern CustomAttributesCache U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818__CustomAttributeCache;
TypeInfo U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_MethodInfos/* methods */
	, NULL/* properties */
	, U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_il2cpp_TypeInfo__nestedTypes/* nested_types */
	, NULL/* nested_in */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_VTable/* vtable */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818__CustomAttributeCache/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_il2cpp_TypeInfo/* cast_class */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t818_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
