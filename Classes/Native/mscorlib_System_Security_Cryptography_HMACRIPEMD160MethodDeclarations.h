﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACRIPEMD160
struct HMACRIPEMD160_t2150;
// System.Byte[]
struct ByteU5BU5D_t653;

// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor()
 void HMACRIPEMD160__ctor_m12093 (HMACRIPEMD160_t2150 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor(System.Byte[])
 void HMACRIPEMD160__ctor_m12094 (HMACRIPEMD160_t2150 * __this, ByteU5BU5D_t653* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
