﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutGroup>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_103.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutGroup>
struct CachedInvokableCall_1_t3780  : public InvokableCall_1_t3781
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutGroup>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
