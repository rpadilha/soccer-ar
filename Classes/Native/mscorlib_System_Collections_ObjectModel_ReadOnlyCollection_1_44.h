﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.ImageTarget>
struct IList_1_t4431;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ImageTarget>
struct ReadOnlyCollection_1_t4427  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ImageTarget>::list
	Object_t* ___list_0;
};
