﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<Joystick_Script>
struct DefaultComparer_t3081;
// Joystick_Script
struct Joystick_Script_t102;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Joystick_Script>::.ctor()
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_0MethodDeclarations.h"
#define DefaultComparer__ctor_m15737(__this, method) (void)DefaultComparer__ctor_m14676_gshared((DefaultComparer_t2849 *)__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Joystick_Script>::Compare(T,T)
#define DefaultComparer_Compare_m15738(__this, ___x, ___y, method) (int32_t)DefaultComparer_Compare_m14677_gshared((DefaultComparer_t2849 *)__this, (Object_t *)___x, (Object_t *)___y, method)
