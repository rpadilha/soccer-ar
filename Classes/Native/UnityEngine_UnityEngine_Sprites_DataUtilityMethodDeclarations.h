﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Sprites.DataUtility
struct DataUtility_t1031;
// UnityEngine.Sprite
struct Sprite_t194;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
 Vector4_t216  DataUtility_GetInnerUV_m2387 (Object_t * __this/* static, unused */, Sprite_t194 * ___sprite, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
 Vector4_t216  DataUtility_GetOuterUV_m2386 (Object_t * __this/* static, unused */, Sprite_t194 * ___sprite, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
 Vector4_t216  DataUtility_GetPadding_m2378 (Object_t * __this/* static, unused */, Sprite_t194 * ___sprite, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Sprites.DataUtility::GetMinSize(UnityEngine.Sprite)
 Vector2_t99  DataUtility_GetMinSize_m2374 (Object_t * __this/* static, unused */, Sprite_t194 * ___sprite, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
 void DataUtility_Internal_GetMinSize_m6100 (Object_t * __this/* static, unused */, Sprite_t194 * ___sprite, Vector2_t99 * ___output, MethodInfo* method) IL2CPP_METHOD_ATTR;
