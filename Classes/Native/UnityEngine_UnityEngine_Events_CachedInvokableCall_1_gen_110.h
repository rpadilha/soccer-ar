﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.DataSetTrackableBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_112.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetTrackableBehaviour>
struct CachedInvokableCall_1_t3848  : public InvokableCall_1_t3849
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetTrackableBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
