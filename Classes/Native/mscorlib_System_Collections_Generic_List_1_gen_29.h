﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t175;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t771  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Int32>::_items
	Int32U5BU5D_t175* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.Int32>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.Int32>::_version
	int32_t ____version_3;
};
struct List_1_t771_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<System.Int32>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<System.Int32>::EmptyArray
	Int32U5BU5D_t175* ___EmptyArray_4;
};
