﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.OpenFlags>
struct InternalEnumerator_1_t5072;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Security.Cryptography.X509Certificates.OpenFlags
#include "System_System_Security_Cryptography_X509Certificates_OpenFla.h"

// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.OpenFlags>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30734 (InternalEnumerator_1_t5072 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.OpenFlags>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30735 (InternalEnumerator_1_t5072 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.OpenFlags>::Dispose()
 void InternalEnumerator_1_Dispose_m30736 (InternalEnumerator_1_t5072 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.OpenFlags>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30737 (InternalEnumerator_1_t5072 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.OpenFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30738 (InternalEnumerator_1_t5072 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
