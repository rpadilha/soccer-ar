﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.DateTimeOffset
struct DateTimeOffset_t2251 
{
	// System.DateTime System.DateTimeOffset::dt
	DateTime_t674  ___dt_2;
	// System.TimeSpan System.DateTimeOffset::utc_offset
	TimeSpan_t852  ___utc_offset_3;
};
struct DateTimeOffset_t2251_StaticFields{
	// System.DateTimeOffset System.DateTimeOffset::MaxValue
	DateTimeOffset_t2251  ___MaxValue_0;
	// System.DateTimeOffset System.DateTimeOffset::MinValue
	DateTimeOffset_t2251  ___MinValue_1;
};
