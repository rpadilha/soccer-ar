﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct KeyValuePair_2_t487;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t239;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m17421 (KeyValuePair_2_t487 * __this, int32_t ___key, PointerEventData_t239 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Key()
 int32_t KeyValuePair_2_get_Key_m2179 (KeyValuePair_2_t487 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m17422 (KeyValuePair_2_t487 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Value()
 PointerEventData_t239 * KeyValuePair_2_get_Value_m2178 (KeyValuePair_2_t487 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m17423 (KeyValuePair_2_t487 * __this, PointerEventData_t239 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::ToString()
 String_t* KeyValuePair_2_ToString_m2203 (KeyValuePair_2_t487 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
