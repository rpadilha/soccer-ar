﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator
struct X509Certificate2Enumerator_t1420;
// System.Object
struct Object_t;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t1417;
// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
struct X509Certificate2Collection_t1419;

// System.Void System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate2Collection)
 void X509Certificate2Enumerator__ctor_m7162 (X509Certificate2Enumerator_t1420 * __this, X509Certificate2Collection_t1419 * ___collection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::System.Collections.IEnumerator.get_Current()
 Object_t * X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m7163 (X509Certificate2Enumerator_t1420 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::System.Collections.IEnumerator.MoveNext()
 bool X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m7164 (X509Certificate2Enumerator_t1420 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509Certificate2 System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::get_Current()
 X509Certificate2_t1417 * X509Certificate2Enumerator_get_Current_m7165 (X509Certificate2Enumerator_t1420 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::MoveNext()
 bool X509Certificate2Enumerator_MoveNext_m7166 (X509Certificate2Enumerator_t1420 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
