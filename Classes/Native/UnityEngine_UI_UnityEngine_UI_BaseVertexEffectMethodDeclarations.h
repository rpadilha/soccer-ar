﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.BaseVertexEffect
struct BaseVertexEffect_t459;
// UnityEngine.UI.Graphic
struct Graphic_t344;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t345;

// System.Void UnityEngine.UI.BaseVertexEffect::.ctor()
 void BaseVertexEffect__ctor_m2023 (BaseVertexEffect_t459 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Graphic UnityEngine.UI.BaseVertexEffect::get_graphic()
 Graphic_t344 * BaseVertexEffect_get_graphic_m2024 (BaseVertexEffect_t459 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseVertexEffect::OnEnable()
 void BaseVertexEffect_OnEnable_m2025 (BaseVertexEffect_t459 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseVertexEffect::OnDisable()
 void BaseVertexEffect_OnDisable_m2026 (BaseVertexEffect_t459 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseVertexEffect::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
