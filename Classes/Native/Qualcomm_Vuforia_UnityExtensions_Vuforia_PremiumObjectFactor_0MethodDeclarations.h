﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PremiumObjectFactory
struct PremiumObjectFactory_t682;
// Vuforia.IPremiumObjectFactory
struct IPremiumObjectFactory_t681;

// Vuforia.IPremiumObjectFactory Vuforia.PremiumObjectFactory::get_Instance()
 Object_t * PremiumObjectFactory_get_Instance_m3141 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PremiumObjectFactory::set_Instance(Vuforia.IPremiumObjectFactory)
 void PremiumObjectFactory_set_Instance_m3142 (Object_t * __this/* static, unused */, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PremiumObjectFactory::.ctor()
 void PremiumObjectFactory__ctor_m3143 (PremiumObjectFactory_t682 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
