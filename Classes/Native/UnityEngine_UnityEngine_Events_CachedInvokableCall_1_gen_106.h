﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.PositionAsUV1>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_108.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.PositionAsUV1>
struct CachedInvokableCall_1_t3815  : public InvokableCall_1_t3816
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.PositionAsUV1>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
