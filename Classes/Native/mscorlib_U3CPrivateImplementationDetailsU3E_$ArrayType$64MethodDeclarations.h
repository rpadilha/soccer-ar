﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$64
struct $ArrayType$64_t2323;
struct $ArrayType$64_t2323_marshaled;

void $ArrayType$64_t2323_marshal(const $ArrayType$64_t2323& unmarshaled, $ArrayType$64_t2323_marshaled& marshaled);
void $ArrayType$64_t2323_marshal_back(const $ArrayType$64_t2323_marshaled& marshaled, $ArrayType$64_t2323& unmarshaled);
void $ArrayType$64_t2323_marshal_cleanup($ArrayType$64_t2323_marshaled& marshaled);
