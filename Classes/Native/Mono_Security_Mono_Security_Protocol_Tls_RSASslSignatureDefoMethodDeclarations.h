﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.RSASslSignatureDeformatter
struct RSASslSignatureDeformatter_t1671;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1403;
// System.Byte[]
struct ByteU5BU5D_t653;
// System.String
struct String_t;

// System.Void Mono.Security.Protocol.Tls.RSASslSignatureDeformatter::.ctor(System.Security.Cryptography.AsymmetricAlgorithm)
 void RSASslSignatureDeformatter__ctor_m8677 (RSASslSignatureDeformatter_t1671 * __this, AsymmetricAlgorithm_t1403 * ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RSASslSignatureDeformatter::VerifySignature(System.Byte[],System.Byte[])
 bool RSASslSignatureDeformatter_VerifySignature_m8678 (RSASslSignatureDeformatter_t1671 * __this, ByteU5BU5D_t653* ___rgbHash, ByteU5BU5D_t653* ___rgbSignature, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RSASslSignatureDeformatter::SetHashAlgorithm(System.String)
 void RSASslSignatureDeformatter_SetHashAlgorithm_m8679 (RSASslSignatureDeformatter_t1671 * __this, String_t* ___strName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RSASslSignatureDeformatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm)
 void RSASslSignatureDeformatter_SetKey_m8680 (RSASslSignatureDeformatter_t1671 * __this, AsymmetricAlgorithm_t1403 * ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
