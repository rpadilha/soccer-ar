﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>
struct InternalEnumerator_1_t3030;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// GoalKeeper_Script/GoalKeeper_State
#include "AssemblyU2DCSharp_GoalKeeper_Script_GoalKeeper_State.h"

// System.Void System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m15459 (InternalEnumerator_1_t3030 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15460 (InternalEnumerator_1_t3030 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::Dispose()
 void InternalEnumerator_1_Dispose_m15461 (InternalEnumerator_1_t3030 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m15462 (InternalEnumerator_1_t3030 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<GoalKeeper_Script/GoalKeeper_State>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m15463 (InternalEnumerator_1_t3030 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
