﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.KeepAliveAbstractBehaviour
struct KeepAliveAbstractBehaviour_t38;
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t756;

// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepARCameraAlive()
 bool KeepAliveAbstractBehaviour_get_KeepARCameraAlive_m4094 (KeepAliveAbstractBehaviour_t38 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepARCameraAlive(System.Boolean)
 void KeepAliveAbstractBehaviour_set_KeepARCameraAlive_m4095 (KeepAliveAbstractBehaviour_t38 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepTrackableBehavioursAlive()
 bool KeepAliveAbstractBehaviour_get_KeepTrackableBehavioursAlive_m4096 (KeepAliveAbstractBehaviour_t38 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepTrackableBehavioursAlive(System.Boolean)
 void KeepAliveAbstractBehaviour_set_KeepTrackableBehavioursAlive_m4097 (KeepAliveAbstractBehaviour_t38 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepTextRecoBehaviourAlive()
 bool KeepAliveAbstractBehaviour_get_KeepTextRecoBehaviourAlive_m4098 (KeepAliveAbstractBehaviour_t38 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepTextRecoBehaviourAlive(System.Boolean)
 void KeepAliveAbstractBehaviour_set_KeepTextRecoBehaviourAlive_m4099 (KeepAliveAbstractBehaviour_t38 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepUDTBuildingBehaviourAlive()
 bool KeepAliveAbstractBehaviour_get_KeepUDTBuildingBehaviourAlive_m4100 (KeepAliveAbstractBehaviour_t38 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepUDTBuildingBehaviourAlive(System.Boolean)
 void KeepAliveAbstractBehaviour_set_KeepUDTBuildingBehaviourAlive_m4101 (KeepAliveAbstractBehaviour_t38 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepSmartTerrainAlive()
 bool KeepAliveAbstractBehaviour_get_KeepSmartTerrainAlive_m4102 (KeepAliveAbstractBehaviour_t38 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepSmartTerrainAlive(System.Boolean)
 void KeepAliveAbstractBehaviour_set_KeepSmartTerrainAlive_m4103 (KeepAliveAbstractBehaviour_t38 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepCloudRecoBehaviourAlive()
 bool KeepAliveAbstractBehaviour_get_KeepCloudRecoBehaviourAlive_m4104 (KeepAliveAbstractBehaviour_t38 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepCloudRecoBehaviourAlive(System.Boolean)
 void KeepAliveAbstractBehaviour_set_KeepCloudRecoBehaviourAlive_m4105 (KeepAliveAbstractBehaviour_t38 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.KeepAliveAbstractBehaviour Vuforia.KeepAliveAbstractBehaviour::get_Instance()
 KeepAliveAbstractBehaviour_t38 * KeepAliveAbstractBehaviour_get_Instance_m4106 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::RegisterEventHandler(Vuforia.ILoadLevelEventHandler)
 void KeepAliveAbstractBehaviour_RegisterEventHandler_m4107 (KeepAliveAbstractBehaviour_t38 * __this, Object_t * ___eventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::UnregisterEventHandler(Vuforia.ILoadLevelEventHandler)
 bool KeepAliveAbstractBehaviour_UnregisterEventHandler_m4108 (KeepAliveAbstractBehaviour_t38 * __this, Object_t * ___eventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::OnLevelWasLoaded()
 void KeepAliveAbstractBehaviour_OnLevelWasLoaded_m4109 (KeepAliveAbstractBehaviour_t38 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::.ctor()
 void KeepAliveAbstractBehaviour__ctor_m347 (KeepAliveAbstractBehaviour_t38 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
