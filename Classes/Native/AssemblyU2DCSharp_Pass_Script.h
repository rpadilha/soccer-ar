﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t29;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Pass_Script
struct Pass_Script_t104  : public MonoBehaviour_t10
{
	// UnityEngine.GameObject Pass_Script::sphere
	GameObject_t29 * ___sphere_2;
};
