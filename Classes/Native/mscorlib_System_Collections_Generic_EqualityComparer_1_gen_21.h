﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.CanvasGroup>
struct EqualityComparer_1_t3667;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.CanvasGroup>
struct EqualityComparer_1_t3667  : public Object_t
{
};
struct EqualityComparer_1_t3667_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.CanvasGroup>::_default
	EqualityComparer_1_t3667 * ____default_0;
};
