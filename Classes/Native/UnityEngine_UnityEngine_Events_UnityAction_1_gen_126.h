﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// Vuforia.DataSetLoadAbstractBehaviour
struct DataSetLoadAbstractBehaviour_t8;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadAbstractBehaviour>
struct UnityAction_1_t3911  : public MulticastDelegate_t373
{
};
