﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// UnityEngine.EventSystems.PointerInputModule
struct PointerInputModule_t303;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.PointerInputModule>
struct UnityAction_1_t3323  : public MulticastDelegate_t373
{
};
