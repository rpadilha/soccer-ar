﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_t230;
// System.Diagnostics.DebuggableAttribute/DebuggingModes
#include "mscorlib_System_Diagnostics_DebuggableAttribute_DebuggingMod.h"

// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
 void DebuggableAttribute__ctor_m861 (DebuggableAttribute_t230 * __this, int32_t ___modes, MethodInfo* method) IL2CPP_METHOD_ATTR;
