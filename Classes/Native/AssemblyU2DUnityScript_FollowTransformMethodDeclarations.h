﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// FollowTransform
struct FollowTransform_t212;

// System.Void FollowTransform::.ctor()
 void FollowTransform__ctor_m745 (FollowTransform_t212 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FollowTransform::Start()
 void FollowTransform_Start_m746 (FollowTransform_t212 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FollowTransform::Update()
 void FollowTransform_Update_m747 (FollowTransform_t212 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FollowTransform::Main()
 void FollowTransform_Main_m748 (FollowTransform_t212 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
