﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.Font>
struct Action_1_t507;
// System.Object
struct Object_t;
// UnityEngine.Font
struct Font_t332;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<UnityEngine.Font>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_9MethodDeclarations.h"
#define Action_1__ctor_m2281(__this, ___object, ___method, method) (void)Action_1__ctor_m14347_gshared((Action_1_t2798 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// System.Void System.Action`1<UnityEngine.Font>::Invoke(T)
#define Action_1_Invoke_m6664(__this, ___obj, method) (void)Action_1_Invoke_m14348_gshared((Action_1_t2798 *)__this, (Object_t *)___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.Font>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m18695(__this, ___obj, ___callback, ___object, method) (Object_t *)Action_1_BeginInvoke_m14350_gshared((Action_1_t2798 *)__this, (Object_t *)___obj, (AsyncCallback_t251 *)___callback, (Object_t *)___object, method)
// System.Void System.Action`1<UnityEngine.Font>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m18696(__this, ___result, method) (void)Action_1_EndInvoke_m14352_gshared((Action_1_t2798 *)__this, (Object_t *)___result, method)
