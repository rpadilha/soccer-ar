﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Vuforia.QCARManagerImpl/WordData
#pragma pack(push, tp, 1)
struct WordData_t689 
{
	// System.IntPtr Vuforia.QCARManagerImpl/WordData::stringValue
	IntPtr_t121 ___stringValue_0;
	// System.Int32 Vuforia.QCARManagerImpl/WordData::id
	int32_t ___id_1;
	// UnityEngine.Vector2 Vuforia.QCARManagerImpl/WordData::size
	Vector2_t99  ___size_2;
	// System.Int32 Vuforia.QCARManagerImpl/WordData::unused
	int32_t ___unused_3;
};
#pragma pack(pop, tp)
