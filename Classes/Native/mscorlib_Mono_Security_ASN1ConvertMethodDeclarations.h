﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.ASN1Convert
struct ASN1Convert_t1837;
// Mono.Security.ASN1
struct ASN1_t1826;
// System.String
struct String_t;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Int32 Mono.Security.ASN1Convert::ToInt32(Mono.Security.ASN1)
 int32_t ASN1Convert_ToInt32_m10350 (Object_t * __this/* static, unused */, ASN1_t1826 * ___asn1, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.ASN1Convert::ToOid(Mono.Security.ASN1)
 String_t* ASN1Convert_ToOid_m10351 (Object_t * __this/* static, unused */, ASN1_t1826 * ___asn1, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.ASN1Convert::ToDateTime(Mono.Security.ASN1)
 DateTime_t674  ASN1Convert_ToDateTime_m10352 (Object_t * __this/* static, unused */, ASN1_t1826 * ___time, MethodInfo* method) IL2CPP_METHOD_ATTR;
