﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.MeshFilter
struct MeshFilter_t84;
// UnityEngine.Mesh
struct Mesh_t174;

// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
 Mesh_t174 * MeshFilter_get_mesh_m625 (MeshFilter_t84 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
 void MeshFilter_set_mesh_m5439 (MeshFilter_t84 * __this, Mesh_t174 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
 Mesh_t174 * MeshFilter_get_sharedMesh_m525 (MeshFilter_t84 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
 void MeshFilter_set_sharedMesh_m4665 (MeshFilter_t84 * __this, Mesh_t174 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
