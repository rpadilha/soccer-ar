﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas
struct Canvas_t340;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Canvas>
struct Comparison_1_t3530  : public MulticastDelegate_t373
{
};
