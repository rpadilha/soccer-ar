﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Dictionary_2_t743;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t742;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct KeyCollection_t881;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct ValueCollection_t880;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t2737;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1118;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
struct KeyValuePair_2U5BU5D_t4193;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>>
struct IEnumerator_1_t4195;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1352;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_15.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__15.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_27MethodDeclarations.h"
#define Dictionary_2__ctor_m5193(__this, method) (void)Dictionary_2__ctor_m18256_gshared((Dictionary_2_t3452 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m24299(__this, ___comparer, method) (void)Dictionary_2__ctor_m18258_gshared((Dictionary_2_t3452 *)__this, (Object_t*)___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Int32)
#define Dictionary_2__ctor_m24300(__this, ___capacity, method) (void)Dictionary_2__ctor_m18260_gshared((Dictionary_2_t3452 *)__this, (int32_t)___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m24301(__this, ___info, ___context, method) (void)Dictionary_2__ctor_m18262_gshared((Dictionary_2_t3452 *)__this, (SerializationInfo_t1118 *)___info, (StreamingContext_t1119 )___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m24302(__this, ___key, method) (Object_t *)Dictionary_2_System_Collections_IDictionary_get_Item_m18264_gshared((Dictionary_2_t3452 *)__this, (Object_t *)___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m24303(__this, ___key, ___value, method) (void)Dictionary_2_System_Collections_IDictionary_set_Item_m18266_gshared((Dictionary_2_t3452 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m24304(__this, ___key, ___value, method) (void)Dictionary_2_System_Collections_IDictionary_Add_m18268_gshared((Dictionary_2_t3452 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m24305(__this, ___key, method) (void)Dictionary_2_System_Collections_IDictionary_Remove_m18270_gshared((Dictionary_2_t3452 *)__this, (Object_t *)___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m24306(__this, method) (bool)Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18272_gshared((Dictionary_2_t3452 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m24307(__this, method) (Object_t *)Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18274_gshared((Dictionary_2_t3452 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m24308(__this, method) (bool)Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18276_gshared((Dictionary_2_t3452 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
 void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m24309 (Dictionary_2_t743 * __this, KeyValuePair_2_t4194  ___keyValuePair, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
 bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m24310 (Dictionary_2_t743 * __this, KeyValuePair_2_t4194  ___keyValuePair, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m24311(__this, ___array, ___index, method) (void)Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18280_gshared((Dictionary_2_t3452 *)__this, (KeyValuePair_2U5BU5D_t3453*)___array, (int32_t)___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
 bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m24312 (Dictionary_2_t743 * __this, KeyValuePair_2_t4194  ___keyValuePair, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m24313(__this, ___array, ___index, method) (void)Dictionary_2_System_Collections_ICollection_CopyTo_m18283_gshared((Dictionary_2_t3452 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m24314(__this, method) (Object_t *)Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18285_gshared((Dictionary_2_t3452 *)__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m24315(__this, method) (Object_t*)Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18287_gshared((Dictionary_2_t3452 *)__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m24316(__this, method) (Object_t *)Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18289_gshared((Dictionary_2_t3452 *)__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Count()
#define Dictionary_2_get_Count_m24317(__this, method) (int32_t)Dictionary_2_get_Count_m18291_gshared((Dictionary_2_t3452 *)__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Item(TKey)
#define Dictionary_2_get_Item_m5126(__this, ___key, method) (List_1_t742 *)Dictionary_2_get_Item_m18293_gshared((Dictionary_2_t3452 *)__this, (Object_t *)___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m5138(__this, ___key, ___value, method) (void)Dictionary_2_set_Item_m18295_gshared((Dictionary_2_t3452 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m24318(__this, ___capacity, ___hcp, method) (void)Dictionary_2_Init_m18297_gshared((Dictionary_2_t3452 *)__this, (int32_t)___capacity, (Object_t*)___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m24319(__this, ___size, method) (void)Dictionary_2_InitArrays_m18299_gshared((Dictionary_2_t3452 *)__this, (int32_t)___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m24320(__this, ___array, ___index, method) (void)Dictionary_2_CopyToCheck_m18301_gshared((Dictionary_2_t3452 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::make_pair(TKey,TValue)
 KeyValuePair_2_t4194  Dictionary_2_make_pair_m24321 (Object_t * __this/* static, unused */, String_t* ___key, List_1_t742 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m24322(__this/* static, unused */, ___key, ___value, method) (String_t*)Dictionary_2_pick_key_m18304_gshared((Object_t *)__this/* static, unused */, (Object_t *)___key, (Object_t *)___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m24323(__this/* static, unused */, ___key, ___value, method) (List_1_t742 *)Dictionary_2_pick_value_m18306_gshared((Object_t *)__this/* static, unused */, (Object_t *)___key, (Object_t *)___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m24324(__this, ___array, ___index, method) (void)Dictionary_2_CopyTo_m18308_gshared((Dictionary_2_t3452 *)__this, (KeyValuePair_2U5BU5D_t3453*)___array, (int32_t)___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Resize()
#define Dictionary_2_Resize_m24325(__this, method) (void)Dictionary_2_Resize_m18310_gshared((Dictionary_2_t3452 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Add(TKey,TValue)
#define Dictionary_2_Add_m5139(__this, ___key, ___value, method) (void)Dictionary_2_Add_m18311_gshared((Dictionary_2_t3452 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Clear()
#define Dictionary_2_Clear_m24326(__this, method) (void)Dictionary_2_Clear_m18313_gshared((Dictionary_2_t3452 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m5133(__this, ___key, method) (bool)Dictionary_2_ContainsKey_m18315_gshared((Dictionary_2_t3452 *)__this, (Object_t *)___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m24327(__this, ___value, method) (bool)Dictionary_2_ContainsValue_m18317_gshared((Dictionary_2_t3452 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m24328(__this, ___info, ___context, method) (void)Dictionary_2_GetObjectData_m18319_gshared((Dictionary_2_t3452 *)__this, (SerializationInfo_t1118 *)___info, (StreamingContext_t1119 )___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m24329(__this, ___sender, method) (void)Dictionary_2_OnDeserialization_m18321_gshared((Dictionary_2_t3452 *)__this, (Object_t *)___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Remove(TKey)
#define Dictionary_2_Remove_m5130(__this, ___key, method) (bool)Dictionary_2_Remove_m18323_gshared((Dictionary_2_t3452 *)__this, (Object_t *)___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m24330(__this, ___key, ___value, method) (bool)Dictionary_2_TryGetValue_m18324_gshared((Dictionary_2_t3452 *)__this, (Object_t *)___key, (Object_t **)___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Keys()
#define Dictionary_2_get_Keys_m5124(__this, method) (KeyCollection_t881 *)Dictionary_2_get_Keys_m18326_gshared((Dictionary_2_t3452 *)__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Values()
#define Dictionary_2_get_Values_m5119(__this, method) (ValueCollection_t880 *)Dictionary_2_get_Values_m18328_gshared((Dictionary_2_t3452 *)__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m24331(__this, ___key, method) (String_t*)Dictionary_2_ToTKey_m18330_gshared((Dictionary_2_t3452 *)__this, (Object_t *)___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m24332(__this, ___value, method) (List_1_t742 *)Dictionary_2_ToTValue_m18332_gshared((Dictionary_2_t3452 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
 bool Dictionary_2_ContainsKeyValuePair_m24333 (Dictionary_2_t743 * __this, KeyValuePair_2_t4194  ___pair, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::GetEnumerator()
 Enumerator_t4196  Dictionary_2_GetEnumerator_m24334 (Dictionary_2_t743 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m24335(__this/* static, unused */, ___key, ___value, method) (DictionaryEntry_t1355 )Dictionary_2_U3CCopyToU3Em__0_m18336_gshared((Object_t *)__this/* static, unused */, (Object_t *)___key, (Object_t *)___value, method)
