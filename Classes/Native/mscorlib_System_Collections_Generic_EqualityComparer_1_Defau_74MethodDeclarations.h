﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>
struct DefaultComparer_t5071;

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>::.ctor()
 void DefaultComparer__ctor_m30731 (DefaultComparer_t5071 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>::GetHashCode(T)
 int32_t DefaultComparer_GetHashCode_m30732 (DefaultComparer_t5071 * __this, bool ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Boolean>::Equals(T,T)
 bool DefaultComparer_Equals_m30733 (DefaultComparer_t5071 * __this, bool ___x, bool ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
