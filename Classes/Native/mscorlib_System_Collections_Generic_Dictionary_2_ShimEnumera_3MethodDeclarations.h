﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t3466;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3452;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m18416_gshared (ShimEnumerator_t3466 * __this, Dictionary_2_t3452 * ___host, MethodInfo* method);
#define ShimEnumerator__ctor_m18416(__this, ___host, method) (void)ShimEnumerator__ctor_m18416_gshared((ShimEnumerator_t3466 *)__this, (Dictionary_2_t3452 *)___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
 bool ShimEnumerator_MoveNext_m18417_gshared (ShimEnumerator_t3466 * __this, MethodInfo* method);
#define ShimEnumerator_MoveNext_m18417(__this, method) (bool)ShimEnumerator_MoveNext_m18417_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m18418_gshared (ShimEnumerator_t3466 * __this, MethodInfo* method);
#define ShimEnumerator_get_Entry_m18418(__this, method) (DictionaryEntry_t1355 )ShimEnumerator_get_Entry_m18418_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
 Object_t * ShimEnumerator_get_Key_m18419_gshared (ShimEnumerator_t3466 * __this, MethodInfo* method);
#define ShimEnumerator_get_Key_m18419(__this, method) (Object_t *)ShimEnumerator_get_Key_m18419_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
 Object_t * ShimEnumerator_get_Value_m18420_gshared (ShimEnumerator_t3466 * __this, MethodInfo* method);
#define ShimEnumerator_get_Value_m18420(__this, method) (Object_t *)ShimEnumerator_get_Value_m18420_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
 Object_t * ShimEnumerator_get_Current_m18421_gshared (ShimEnumerator_t3466 * __this, MethodInfo* method);
#define ShimEnumerator_get_Current_m18421(__this, method) (Object_t *)ShimEnumerator_get_Current_m18421_gshared((ShimEnumerator_t3466 *)__this, method)
