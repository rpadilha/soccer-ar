﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
struct ReliabilityContractAttribute_t2017;
// System.Runtime.ConstrainedExecution.Consistency
#include "mscorlib_System_Runtime_ConstrainedExecution_Consistency.h"
// System.Runtime.ConstrainedExecution.Cer
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer.h"

// System.Void System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::.ctor(System.Runtime.ConstrainedExecution.Consistency,System.Runtime.ConstrainedExecution.Cer)
 void ReliabilityContractAttribute__ctor_m11581 (ReliabilityContractAttribute_t2017 * __this, int32_t ___consistencyGuarantee, int32_t ___cer, MethodInfo* method) IL2CPP_METHOD_ATTR;
