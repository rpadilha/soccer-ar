﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.Object>
struct Comparer_1_t2848;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.Object>
struct Comparer_1_t2848  : public Object_t
{
};
struct Comparer_1_t2848_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Object>::_default
	Comparer_1_t2848 * ____default_0;
};
