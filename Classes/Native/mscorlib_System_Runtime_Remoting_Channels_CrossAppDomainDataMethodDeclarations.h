﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Channels.CrossAppDomainData
struct CrossAppDomainData_t2048;

// System.Void System.Runtime.Remoting.Channels.CrossAppDomainData::.ctor(System.Int32)
 void CrossAppDomainData__ctor_m11633 (CrossAppDomainData_t2048 * __this, int32_t ___domainId, MethodInfo* method) IL2CPP_METHOD_ATTR;
