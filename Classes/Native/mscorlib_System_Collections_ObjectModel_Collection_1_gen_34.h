﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.Word>
struct IList_1_t4165;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.Collection`1<Vuforia.Word>
struct Collection_1_t4166  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.Word>::list
	Object_t* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.Word>::syncRoot
	Object_t * ___syncRoot_1;
};
