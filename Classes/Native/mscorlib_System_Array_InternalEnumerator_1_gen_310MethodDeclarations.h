﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour/Status>
struct InternalEnumerator_1_t3847;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour/Status>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m21050 (InternalEnumerator_1_t3847 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour/Status>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21051 (InternalEnumerator_1_t3847 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour/Status>::Dispose()
 void InternalEnumerator_1_Dispose_m21052 (InternalEnumerator_1_t3847 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour/Status>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m21053 (InternalEnumerator_1_t3847 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour/Status>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m21054 (InternalEnumerator_1_t3847 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
