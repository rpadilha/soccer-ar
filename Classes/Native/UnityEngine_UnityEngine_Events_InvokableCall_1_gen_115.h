﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct UnityAction_1_t3878;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct InvokableCall_1_t3877  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaAbstractBehaviour>::Delegate
	UnityAction_1_t3878 * ___Delegate_0;
};
