﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.MultiArrayFixupRecord
struct MultiArrayFixupRecord_t2128;
// System.Runtime.Serialization.ObjectRecord
struct ObjectRecord_t2125;
// System.Int32[]
struct Int32U5BU5D_t175;
// System.Runtime.Serialization.ObjectManager
struct ObjectManager_t2118;

// System.Void System.Runtime.Serialization.MultiArrayFixupRecord::.ctor(System.Runtime.Serialization.ObjectRecord,System.Int32[],System.Runtime.Serialization.ObjectRecord)
 void MultiArrayFixupRecord__ctor_m11958 (MultiArrayFixupRecord_t2128 * __this, ObjectRecord_t2125 * ___objectToBeFixed, Int32U5BU5D_t175* ___indices, ObjectRecord_t2125 * ___objectRequired, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.MultiArrayFixupRecord::FixupImpl(System.Runtime.Serialization.ObjectManager)
 void MultiArrayFixupRecord_FixupImpl_m11959 (MultiArrayFixupRecord_t2128 * __this, ObjectManager_t2118 * ___manager, MethodInfo* method) IL2CPP_METHOD_ATTR;
