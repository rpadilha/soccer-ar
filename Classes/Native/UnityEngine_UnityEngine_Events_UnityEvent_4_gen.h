﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
struct UnityEvent_4_t5029  : public UnityEventBase_t1136
{
	// System.Object[] UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::m_InvokeArray
	ObjectU5BU5D_t130* ___m_InvokeArray_4;
};
