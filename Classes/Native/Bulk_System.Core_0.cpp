﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "System_Core_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo U3CModuleU3E_t1270_il2cpp_TypeInfo;
// <Module>
#include "System_Core_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// Metadata Definition <Module>
static MethodInfo* U3CModuleU3E_t1270_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType U3CModuleU3E_t1270_0_0_0;
extern Il2CppType U3CModuleU3E_t1270_1_0_0;
struct U3CModuleU3E_t1270;
TypeInfo U3CModuleU3E_t1270_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t1270_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &U3CModuleU3E_t1270_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &U3CModuleU3E_t1270_il2cpp_TypeInfo/* cast_class */
	, &U3CModuleU3E_t1270_0_0_0/* byval_arg */
	, &U3CModuleU3E_t1270_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t1270)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ExtensionAttribute_t822_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// System.Attribute
#include "mscorlib_System_AttributeMethodDeclarations.h"
extern MethodInfo Attribute__ctor_m4415_MethodInfo;


// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
extern MethodInfo ExtensionAttribute__ctor_m4522_MethodInfo;
 void ExtensionAttribute__ctor_m4522 (ExtensionAttribute_t822 * __this, MethodInfo* method){
	{
		Attribute__ctor_m4415(__this, /*hidden argument*/&Attribute__ctor_m4415_MethodInfo);
		return;
	}
}
// Metadata Definition System.Runtime.CompilerServices.ExtensionAttribute
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
MethodInfo ExtensionAttribute__ctor_m4522_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExtensionAttribute__ctor_m4522/* method */
	, &ExtensionAttribute_t822_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ExtensionAttribute_t822_MethodInfos[] =
{
	&ExtensionAttribute__ctor_m4522_MethodInfo,
	NULL
};
extern MethodInfo Attribute_Equals_m4416_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo Attribute_GetHashCode_m4417_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
static MethodInfo* ExtensionAttribute_t822_VTable[] =
{
	&Attribute_Equals_m4416_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Attribute_GetHashCode_m4417_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern TypeInfo _Attribute_t819_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair ExtensionAttribute_t822_InterfacesOffsets[] = 
{
	{ &_Attribute_t819_il2cpp_TypeInfo, 4},
};
extern TypeInfo AttributeUsageAttribute_t1213_il2cpp_TypeInfo;
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern MethodInfo AttributeUsageAttribute__ctor_m6676_MethodInfo;
void ExtensionAttribute_t822_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1213 * tmp;
		tmp = (AttributeUsageAttribute_t1213 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1213_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6676(tmp, 69, &AttributeUsageAttribute__ctor_m6676_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache ExtensionAttribute_t822__CustomAttributeCache = {
1,
NULL,
&ExtensionAttribute_t822_CustomAttributesCacheGenerator
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType ExtensionAttribute_t822_0_0_0;
extern Il2CppType ExtensionAttribute_t822_1_0_0;
extern TypeInfo Attribute_t145_il2cpp_TypeInfo;
struct ExtensionAttribute_t822;
extern CustomAttributesCache ExtensionAttribute_t822__CustomAttributeCache;
TypeInfo ExtensionAttribute_t822_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExtensionAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, ExtensionAttribute_t822_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Attribute_t145_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ExtensionAttribute_t822_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ExtensionAttribute_t822_VTable/* vtable */
	, &ExtensionAttribute_t822__CustomAttributeCache/* custom_attributes_cache */
	, &ExtensionAttribute_t822_il2cpp_TypeInfo/* cast_class */
	, &ExtensionAttribute_t822_0_0_0/* byval_arg */
	, &ExtensionAttribute_t822_1_0_0/* this_arg */
	, ExtensionAttribute_t822_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExtensionAttribute_t822)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo MonoTODOAttribute_t1271_il2cpp_TypeInfo;
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttributeMethodDeclarations.h"



// System.Void System.MonoTODOAttribute::.ctor()
extern MethodInfo MonoTODOAttribute__ctor_m6800_MethodInfo;
 void MonoTODOAttribute__ctor_m6800 (MonoTODOAttribute_t1271 * __this, MethodInfo* method){
	{
		Attribute__ctor_m4415(__this, /*hidden argument*/&Attribute__ctor_m4415_MethodInfo);
		return;
	}
}
// Metadata Definition System.MonoTODOAttribute
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void System.MonoTODOAttribute::.ctor()
MethodInfo MonoTODOAttribute__ctor_m6800_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoTODOAttribute__ctor_m6800/* method */
	, &MonoTODOAttribute_t1271_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MonoTODOAttribute_t1271_MethodInfos[] =
{
	&MonoTODOAttribute__ctor_m6800_MethodInfo,
	NULL
};
static MethodInfo* MonoTODOAttribute_t1271_VTable[] =
{
	&Attribute_Equals_m4416_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Attribute_GetHashCode_m4417_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
static Il2CppInterfaceOffsetPair MonoTODOAttribute_t1271_InterfacesOffsets[] = 
{
	{ &_Attribute_t819_il2cpp_TypeInfo, 4},
};
extern MethodInfo AttributeUsageAttribute_set_AllowMultiple_m6678_MethodInfo;
void MonoTODOAttribute_t1271_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1213 * tmp;
		tmp = (AttributeUsageAttribute_t1213 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1213_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6676(tmp, 32767, &AttributeUsageAttribute__ctor_m6676_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6678(tmp, true, &AttributeUsageAttribute_set_AllowMultiple_m6678_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache MonoTODOAttribute_t1271__CustomAttributeCache = {
1,
NULL,
&MonoTODOAttribute_t1271_CustomAttributesCacheGenerator
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType MonoTODOAttribute_t1271_0_0_0;
extern Il2CppType MonoTODOAttribute_t1271_1_0_0;
struct MonoTODOAttribute_t1271;
extern CustomAttributesCache MonoTODOAttribute_t1271__CustomAttributeCache;
TypeInfo MonoTODOAttribute_t1271_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTODOAttribute"/* name */
	, "System"/* namespaze */
	, MonoTODOAttribute_t1271_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Attribute_t145_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &MonoTODOAttribute_t1271_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, MonoTODOAttribute_t1271_VTable/* vtable */
	, &MonoTODOAttribute_t1271__CustomAttributeCache/* custom_attributes_cache */
	, &MonoTODOAttribute_t1271_il2cpp_TypeInfo/* cast_class */
	, &MonoTODOAttribute_t1271_0_0_0/* byval_arg */
	, &MonoTODOAttribute_t1271_1_0_0/* this_arg */
	, MonoTODOAttribute_t1271_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTODOAttribute_t1271)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Collections.Generic.HashSet`1/Link
#include "System_Core_System_Collections_Generic_HashSet_1_Link.h"
extern Il2CppGenericContainer Link_t1272_Il2CppGenericContainer;
extern TypeInfo Link_t1272_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Link_t1272_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &Link_t1272_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* Link_t1272_Il2CppGenericParametersArray[1] = 
{
	&Link_t1272_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo Link_t1272_il2cpp_TypeInfo;
Il2CppGenericContainer Link_t1272_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Link_t1272_il2cpp_TypeInfo, 1, 0, Link_t1272_Il2CppGenericParametersArray };
static MethodInfo* Link_t1272_MethodInfos[] =
{
	NULL
};
extern Il2CppType Int32_t123_0_0_6;
FieldInfo Link_t1272____HashCode_0_FieldInfo = 
{
	"HashCode"/* name */
	, &Int32_t123_0_0_6/* type */
	, &Link_t1272_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_6;
FieldInfo Link_t1272____Next_1_FieldInfo = 
{
	"Next"/* name */
	, &Int32_t123_0_0_6/* type */
	, &Link_t1272_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Link_t1272_FieldInfos[] =
{
	&Link_t1272____HashCode_0_FieldInfo,
	&Link_t1272____Next_1_FieldInfo,
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Link_t1272_0_0_0;
extern Il2CppType Link_t1272_1_0_0;
extern TypeInfo HashSet_1_t1275_il2cpp_TypeInfo;
TypeInfo Link_t1272_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Link"/* name */
	, ""/* namespaze */
	, Link_t1272_MethodInfos/* methods */
	, NULL/* properties */
	, Link_t1272_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* nested_in */
	, &Link_t1272_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &Link_t1272_0_0_0/* byval_arg */
	, &Link_t1272_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &Link_t1272_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Generic.HashSet`1/Enumerator
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator.h"
extern Il2CppGenericContainer Enumerator_t1273_Il2CppGenericContainer;
extern TypeInfo Enumerator_t1273_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerator_t1273_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerator_t1273_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerator_t1273_Il2CppGenericParametersArray[1] = 
{
	&Enumerator_t1273_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo Enumerator_t1273_il2cpp_TypeInfo;
Il2CppGenericContainer Enumerator_t1273_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerator_t1273_il2cpp_TypeInfo, 1, 0, Enumerator_t1273_Il2CppGenericParametersArray };
extern Il2CppType HashSet_1_t1284_0_0_0;
extern Il2CppType HashSet_1_t1284_0_0_0;
static ParameterInfo Enumerator_t1273_Enumerator__ctor_m6805_ParameterInfos[] = 
{
	{"hashset", 0, 134217753, &EmptyCustomAttributesCache, &HashSet_1_t1284_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
MethodInfo Enumerator__ctor_m6805_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Enumerator_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerator_t1273_Enumerator__ctor_m6805_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 26/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
// System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m6806_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &Enumerator_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 27/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
MethodInfo Enumerator_MoveNext_m6807_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &Enumerator_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 28/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Enumerator_t1273_gp_0_0_0_0;
// T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
MethodInfo Enumerator_get_Current_m6808_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &Enumerator_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Enumerator_t1273_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 29/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
MethodInfo Enumerator_Dispose_m6809_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &Enumerator_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 30/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Collections.Generic.HashSet`1/Enumerator::CheckState()
MethodInfo Enumerator_CheckState_m6810_MethodInfo = 
{
	"CheckState"/* name */
	, NULL/* method */
	, &Enumerator_t1273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 31/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Enumerator_t1273_MethodInfos[] =
{
	&Enumerator__ctor_m6805_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m6806_MethodInfo,
	&Enumerator_MoveNext_m6807_MethodInfo,
	&Enumerator_get_Current_m6808_MethodInfo,
	&Enumerator_Dispose_m6809_MethodInfo,
	&Enumerator_CheckState_m6810_MethodInfo,
	NULL
};
extern MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m6806_MethodInfo;
static PropertyInfo Enumerator_t1273____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&Enumerator_t1273_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &Enumerator_System_Collections_IEnumerator_get_Current_m6806_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Enumerator_get_Current_m6808_MethodInfo;
static PropertyInfo Enumerator_t1273____Current_PropertyInfo = 
{
	&Enumerator_t1273_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &Enumerator_get_Current_m6808_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Enumerator_t1273_PropertyInfos[] =
{
	&Enumerator_t1273____System_Collections_IEnumerator_Current_PropertyInfo,
	&Enumerator_t1273____Current_PropertyInfo,
	NULL
};
extern Il2CppType HashSet_1_t1284_0_0_1;
FieldInfo Enumerator_t1273____hashset_0_FieldInfo = 
{
	"hashset"/* name */
	, &HashSet_1_t1284_0_0_1/* type */
	, &Enumerator_t1273_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo Enumerator_t1273____next_1_FieldInfo = 
{
	"next"/* name */
	, &Int32_t123_0_0_1/* type */
	, &Enumerator_t1273_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo Enumerator_t1273____stamp_2_FieldInfo = 
{
	"stamp"/* name */
	, &Int32_t123_0_0_1/* type */
	, &Enumerator_t1273_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Enumerator_t1273_gp_0_0_0_1;
FieldInfo Enumerator_t1273____current_3_FieldInfo = 
{
	"current"/* name */
	, &Enumerator_t1273_gp_0_0_0_1/* type */
	, &Enumerator_t1273_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Enumerator_t1273_FieldInfos[] =
{
	&Enumerator_t1273____hashset_0_FieldInfo,
	&Enumerator_t1273____next_1_FieldInfo,
	&Enumerator_t1273____stamp_2_FieldInfo,
	&Enumerator_t1273____current_3_FieldInfo,
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Enumerator_t1273_0_0_0;
extern Il2CppType Enumerator_t1273_1_0_0;
TypeInfo Enumerator_t1273_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, Enumerator_t1273_MethodInfos/* methods */
	, Enumerator_t1273_PropertyInfos/* properties */
	, Enumerator_t1273_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* nested_in */
	, &Enumerator_t1273_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &Enumerator_t1273_0_0_0/* byval_arg */
	, &Enumerator_t1273_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &Enumerator_t1273_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Generic.HashSet`1/PrimeHelper
#include "System_Core_System_Collections_Generic_HashSet_1_PrimeHelper.h"
extern Il2CppGenericContainer PrimeHelper_t1274_Il2CppGenericContainer;
extern TypeInfo PrimeHelper_t1274_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull PrimeHelper_t1274_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &PrimeHelper_t1274_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* PrimeHelper_t1274_Il2CppGenericParametersArray[1] = 
{
	&PrimeHelper_t1274_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo PrimeHelper_t1274_il2cpp_TypeInfo;
Il2CppGenericContainer PrimeHelper_t1274_Il2CppGenericContainer = { { NULL, NULL }, NULL, &PrimeHelper_t1274_il2cpp_TypeInfo, 1, 0, PrimeHelper_t1274_Il2CppGenericParametersArray };
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Collections.Generic.HashSet`1/PrimeHelper::.cctor()
MethodInfo PrimeHelper__cctor_m6811_MethodInfo = 
{
	".cctor"/* name */
	, NULL/* method */
	, &PrimeHelper_t1274_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 32/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo PrimeHelper_t1274_PrimeHelper_TestPrime_m6812_ParameterInfos[] = 
{
	{"x", 0, 134217754, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper::TestPrime(System.Int32)
MethodInfo PrimeHelper_TestPrime_m6812_MethodInfo = 
{
	"TestPrime"/* name */
	, NULL/* method */
	, &PrimeHelper_t1274_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, PrimeHelper_t1274_PrimeHelper_TestPrime_m6812_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 33/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo PrimeHelper_t1274_PrimeHelper_CalcPrime_m6813_ParameterInfos[] = 
{
	{"x", 0, 134217755, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper::CalcPrime(System.Int32)
MethodInfo PrimeHelper_CalcPrime_m6813_MethodInfo = 
{
	"CalcPrime"/* name */
	, NULL/* method */
	, &PrimeHelper_t1274_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, NULL/* invoker_method */
	, PrimeHelper_t1274_PrimeHelper_CalcPrime_m6813_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 34/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo PrimeHelper_t1274_PrimeHelper_ToPrime_m6814_ParameterInfos[] = 
{
	{"x", 0, 134217756, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper::ToPrime(System.Int32)
MethodInfo PrimeHelper_ToPrime_m6814_MethodInfo = 
{
	"ToPrime"/* name */
	, NULL/* method */
	, &PrimeHelper_t1274_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, NULL/* invoker_method */
	, PrimeHelper_t1274_PrimeHelper_ToPrime_m6814_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 35/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PrimeHelper_t1274_MethodInfos[] =
{
	&PrimeHelper__cctor_m6811_MethodInfo,
	&PrimeHelper_TestPrime_m6812_MethodInfo,
	&PrimeHelper_CalcPrime_m6813_MethodInfo,
	&PrimeHelper_ToPrime_m6814_MethodInfo,
	NULL
};
extern Il2CppType Int32U5BU5D_t175_0_0_49;
FieldInfo PrimeHelper_t1274____primes_table_0_FieldInfo = 
{
	"primes_table"/* name */
	, &Int32U5BU5D_t175_0_0_49/* type */
	, &PrimeHelper_t1274_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* PrimeHelper_t1274_FieldInfos[] =
{
	&PrimeHelper_t1274____primes_table_0_FieldInfo,
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType PrimeHelper_t1274_0_0_0;
extern Il2CppType PrimeHelper_t1274_1_0_0;
struct PrimeHelper_t1274;
TypeInfo PrimeHelper_t1274_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimeHelper"/* name */
	, ""/* namespaze */
	, PrimeHelper_t1274_MethodInfos/* methods */
	, NULL/* properties */
	, PrimeHelper_t1274_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* nested_in */
	, &PrimeHelper_t1274_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &PrimeHelper_t1274_0_0_0/* byval_arg */
	, &PrimeHelper_t1274_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &PrimeHelper_t1274_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048963/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Generic.HashSet`1
#include "System_Core_System_Collections_Generic_HashSet_1.h"
extern Il2CppGenericContainer HashSet_1_t1275_Il2CppGenericContainer;
extern TypeInfo HashSet_1_t1275_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull HashSet_1_t1275_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &HashSet_1_t1275_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* HashSet_1_t1275_Il2CppGenericParametersArray[1] = 
{
	&HashSet_1_t1275_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
Il2CppGenericContainer HashSet_1_t1275_Il2CppGenericContainer = { { NULL, NULL }, NULL, &HashSet_1_t1275_il2cpp_TypeInfo, 1, 0, HashSet_1_t1275_Il2CppGenericParametersArray };
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::.ctor()
MethodInfo HashSet_1__ctor_m6815_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1118_0_0_0;
extern Il2CppType SerializationInfo_t1118_0_0_0;
extern Il2CppType StreamingContext_t1119_0_0_0;
extern Il2CppType StreamingContext_t1119_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1__ctor_m6816_ParameterInfos[] = 
{
	{"info", 0, 134217729, &EmptyCustomAttributesCache, &SerializationInfo_t1118_0_0_0},
	{"context", 1, 134217730, &EmptyCustomAttributesCache, &StreamingContext_t1119_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo HashSet_1__ctor_m6816_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1__ctor_m6816_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_1_t1287_0_0_0;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
MethodInfo HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m6817_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1287_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m6818_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TU5BU5D_t1288_0_0_0;
extern Il2CppType TU5BU5D_t1288_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m6819_ParameterInfos[] = 
{
	{"array", 0, 134217731, &EmptyCustomAttributesCache, &TU5BU5D_t1288_0_0_0},
	{"index", 1, 134217732, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m6819_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.CopyTo"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m6819_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HashSet_1_t1275_gp_0_0_0_0;
extern Il2CppType HashSet_1_t1275_gp_0_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m6820_ParameterInfos[] = 
{
	{"item", 0, 134217733, &EmptyCustomAttributesCache, &HashSet_1_t1275_gp_0_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m6820_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.Add"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m6820_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t318_0_0_0;
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
MethodInfo HashSet_1_System_Collections_IEnumerable_GetEnumerator_m6821_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t318_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
// System.Int32 System.Collections.Generic.HashSet`1::get_Count()
MethodInfo HashSet_1_get_Count_m6822_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEqualityComparer_1_t1289_0_0_0;
extern Il2CppType IEqualityComparer_1_t1289_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1_Init_m6823_ParameterInfos[] = 
{
	{"capacity", 0, 134217734, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"comparer", 1, 134217735, &EmptyCustomAttributesCache, &IEqualityComparer_1_t1289_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
MethodInfo HashSet_1_Init_m6823_MethodInfo = 
{
	"Init"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1_Init_m6823_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1_InitArrays_m6824_ParameterInfos[] = 
{
	{"size", 0, 134217736, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::InitArrays(System.Int32)
MethodInfo HashSet_1_InitArrays_m6824_MethodInfo = 
{
	"InitArrays"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1_InitArrays_m6824_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType HashSet_1_t1275_gp_0_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1_SlotsContainsAt_m6825_ParameterInfos[] = 
{
	{"index", 0, 134217737, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"hash", 1, 134217738, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 2, 134217739, &EmptyCustomAttributesCache, &HashSet_1_t1275_gp_0_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1::SlotsContainsAt(System.Int32,System.Int32,T)
MethodInfo HashSet_1_SlotsContainsAt_m6825_MethodInfo = 
{
	"SlotsContainsAt"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1_SlotsContainsAt_m6825_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TU5BU5D_t1288_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1_CopyTo_m6826_ParameterInfos[] = 
{
	{"array", 0, 134217740, &EmptyCustomAttributesCache, &TU5BU5D_t1288_0_0_0},
	{"index", 1, 134217741, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
MethodInfo HashSet_1_CopyTo_m6826_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1_CopyTo_m6826_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TU5BU5D_t1288_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1_CopyTo_m6827_ParameterInfos[] = 
{
	{"array", 0, 134217742, &EmptyCustomAttributesCache, &TU5BU5D_t1288_0_0_0},
	{"index", 1, 134217743, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"count", 2, 134217744, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
MethodInfo HashSet_1_CopyTo_m6827_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1_CopyTo_m6827_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::Resize()
MethodInfo HashSet_1_Resize_m6828_MethodInfo = 
{
	"Resize"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 16/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1_GetLinkHashCode_m6829_ParameterInfos[] = 
{
	{"index", 0, 134217745, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
// System.Int32 System.Collections.Generic.HashSet`1::GetLinkHashCode(System.Int32)
MethodInfo HashSet_1_GetLinkHashCode_m6829_MethodInfo = 
{
	"GetLinkHashCode"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1_GetLinkHashCode_m6829_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 17/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HashSet_1_t1275_gp_0_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1_GetItemHashCode_m6830_ParameterInfos[] = 
{
	{"item", 0, 134217746, &EmptyCustomAttributesCache, &HashSet_1_t1275_gp_0_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
// System.Int32 System.Collections.Generic.HashSet`1::GetItemHashCode(T)
MethodInfo HashSet_1_GetItemHashCode_m6830_MethodInfo = 
{
	"GetItemHashCode"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1_GetItemHashCode_m6830_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 18/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HashSet_1_t1275_gp_0_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1_Add_m6831_ParameterInfos[] = 
{
	{"item", 0, 134217747, &EmptyCustomAttributesCache, &HashSet_1_t1275_gp_0_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1::Add(T)
MethodInfo HashSet_1_Add_m6831_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1_Add_m6831_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 19/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Collections.Generic.HashSet`1::Clear()
MethodInfo HashSet_1_Clear_m6832_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 20/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HashSet_1_t1275_gp_0_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1_Contains_m6833_ParameterInfos[] = 
{
	{"item", 0, 134217748, &EmptyCustomAttributesCache, &HashSet_1_t1275_gp_0_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
MethodInfo HashSet_1_Contains_m6833_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1_Contains_m6833_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 21/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType HashSet_1_t1275_gp_0_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1_Remove_m6834_ParameterInfos[] = 
{
	{"item", 0, 134217749, &EmptyCustomAttributesCache, &HashSet_1_t1275_gp_0_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
MethodInfo HashSet_1_Remove_m6834_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1_Remove_m6834_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 22/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1118_0_0_0;
extern Il2CppType StreamingContext_t1119_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1_GetObjectData_m6835_ParameterInfos[] = 
{
	{"info", 0, 134217750, &EmptyCustomAttributesCache, &SerializationInfo_t1118_0_0_0},
	{"context", 1, 134217751, &EmptyCustomAttributesCache, &StreamingContext_t1119_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern CustomAttributesCache HashSet_1_t1275__CustomAttributeCache_HashSet_1_GetObjectData_m6835;
// System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo HashSet_1_GetObjectData_m6835_MethodInfo = 
{
	"GetObjectData"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1_GetObjectData_m6835_ParameterInfos/* parameters */
	, &HashSet_1_t1275__CustomAttributeCache_HashSet_1_GetObjectData_m6835/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 23/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo HashSet_1_t1275_HashSet_1_OnDeserialization_m6836_ParameterInfos[] = 
{
	{"sender", 0, 134217752, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern CustomAttributesCache HashSet_1_t1275__CustomAttributeCache_HashSet_1_OnDeserialization_m6836;
// System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
MethodInfo HashSet_1_OnDeserialization_m6836_MethodInfo = 
{
	"OnDeserialization"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1275_HashSet_1_OnDeserialization_m6836_ParameterInfos/* parameters */
	, &HashSet_1_t1275__CustomAttributeCache_HashSet_1_OnDeserialization_m6836/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 24/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Enumerator_t1290_0_0_0;
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
MethodInfo HashSet_1_GetEnumerator_m6837_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* declaring_type */
	, &Enumerator_t1290_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 25/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* HashSet_1_t1275_MethodInfos[] =
{
	&HashSet_1__ctor_m6815_MethodInfo,
	&HashSet_1__ctor_m6816_MethodInfo,
	&HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m6817_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m6818_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m6819_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m6820_MethodInfo,
	&HashSet_1_System_Collections_IEnumerable_GetEnumerator_m6821_MethodInfo,
	&HashSet_1_get_Count_m6822_MethodInfo,
	&HashSet_1_Init_m6823_MethodInfo,
	&HashSet_1_InitArrays_m6824_MethodInfo,
	&HashSet_1_SlotsContainsAt_m6825_MethodInfo,
	&HashSet_1_CopyTo_m6826_MethodInfo,
	&HashSet_1_CopyTo_m6827_MethodInfo,
	&HashSet_1_Resize_m6828_MethodInfo,
	&HashSet_1_GetLinkHashCode_m6829_MethodInfo,
	&HashSet_1_GetItemHashCode_m6830_MethodInfo,
	&HashSet_1_Add_m6831_MethodInfo,
	&HashSet_1_Clear_m6832_MethodInfo,
	&HashSet_1_Contains_m6833_MethodInfo,
	&HashSet_1_Remove_m6834_MethodInfo,
	&HashSet_1_GetObjectData_m6835_MethodInfo,
	&HashSet_1_OnDeserialization_m6836_MethodInfo,
	&HashSet_1_GetEnumerator_m6837_MethodInfo,
	NULL
};
extern MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m6818_MethodInfo;
static PropertyInfo HashSet_1_t1275____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.ICollection<T>.IsReadOnly"/* name */
	, &HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m6818_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo HashSet_1_get_Count_m6822_MethodInfo;
static PropertyInfo HashSet_1_t1275____Count_PropertyInfo = 
{
	&HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &HashSet_1_get_Count_m6822_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* HashSet_1_t1275_PropertyInfos[] =
{
	&HashSet_1_t1275____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&HashSet_1_t1275____Count_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_32849;
FieldInfo HashSet_1_t1275____INITIAL_SIZE_0_FieldInfo = 
{
	"INITIAL_SIZE"/* name */
	, &Int32_t123_0_0_32849/* type */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t170_0_0_32849;
FieldInfo HashSet_1_t1275____DEFAULT_LOAD_FACTOR_1_FieldInfo = 
{
	"DEFAULT_LOAD_FACTOR"/* name */
	, &Single_t170_0_0_32849/* type */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_32849;
FieldInfo HashSet_1_t1275____NO_SLOT_2_FieldInfo = 
{
	"NO_SLOT"/* name */
	, &Int32_t123_0_0_32849/* type */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_32849;
FieldInfo HashSet_1_t1275____HASH_FLAG_3_FieldInfo = 
{
	"HASH_FLAG"/* name */
	, &Int32_t123_0_0_32849/* type */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32U5BU5D_t175_0_0_1;
FieldInfo HashSet_1_t1275____table_4_FieldInfo = 
{
	"table"/* name */
	, &Int32U5BU5D_t175_0_0_1/* type */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType LinkU5BU5D_t1291_0_0_1;
FieldInfo HashSet_1_t1275____links_5_FieldInfo = 
{
	"links"/* name */
	, &LinkU5BU5D_t1291_0_0_1/* type */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TU5BU5D_t1288_0_0_1;
FieldInfo HashSet_1_t1275____slots_6_FieldInfo = 
{
	"slots"/* name */
	, &TU5BU5D_t1288_0_0_1/* type */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo HashSet_1_t1275____touched_7_FieldInfo = 
{
	"touched"/* name */
	, &Int32_t123_0_0_1/* type */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo HashSet_1_t1275____empty_slot_8_FieldInfo = 
{
	"empty_slot"/* name */
	, &Int32_t123_0_0_1/* type */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo HashSet_1_t1275____count_9_FieldInfo = 
{
	"count"/* name */
	, &Int32_t123_0_0_1/* type */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo HashSet_1_t1275____threshold_10_FieldInfo = 
{
	"threshold"/* name */
	, &Int32_t123_0_0_1/* type */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IEqualityComparer_1_t1289_0_0_1;
FieldInfo HashSet_1_t1275____comparer_11_FieldInfo = 
{
	"comparer"/* name */
	, &IEqualityComparer_1_t1289_0_0_1/* type */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType SerializationInfo_t1118_0_0_1;
FieldInfo HashSet_1_t1275____si_12_FieldInfo = 
{
	"si"/* name */
	, &SerializationInfo_t1118_0_0_1/* type */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo HashSet_1_t1275____generation_13_FieldInfo = 
{
	"generation"/* name */
	, &Int32_t123_0_0_1/* type */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* HashSet_1_t1275_FieldInfos[] =
{
	&HashSet_1_t1275____INITIAL_SIZE_0_FieldInfo,
	&HashSet_1_t1275____DEFAULT_LOAD_FACTOR_1_FieldInfo,
	&HashSet_1_t1275____NO_SLOT_2_FieldInfo,
	&HashSet_1_t1275____HASH_FLAG_3_FieldInfo,
	&HashSet_1_t1275____table_4_FieldInfo,
	&HashSet_1_t1275____links_5_FieldInfo,
	&HashSet_1_t1275____slots_6_FieldInfo,
	&HashSet_1_t1275____touched_7_FieldInfo,
	&HashSet_1_t1275____empty_slot_8_FieldInfo,
	&HashSet_1_t1275____count_9_FieldInfo,
	&HashSet_1_t1275____threshold_10_FieldInfo,
	&HashSet_1_t1275____comparer_11_FieldInfo,
	&HashSet_1_t1275____si_12_FieldInfo,
	&HashSet_1_t1275____generation_13_FieldInfo,
	NULL
};
static const int32_t HashSet_1_t1275____INITIAL_SIZE_0_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry HashSet_1_t1275____INITIAL_SIZE_0_DefaultValue = 
{
	&HashSet_1_t1275____INITIAL_SIZE_0_FieldInfo/* field */
	, { (char*)&HashSet_1_t1275____INITIAL_SIZE_0_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const float HashSet_1_t1275____DEFAULT_LOAD_FACTOR_1_DefaultValueData = 0.9f;
extern Il2CppType Single_t170_0_0_0;
static Il2CppFieldDefaultValueEntry HashSet_1_t1275____DEFAULT_LOAD_FACTOR_1_DefaultValue = 
{
	&HashSet_1_t1275____DEFAULT_LOAD_FACTOR_1_FieldInfo/* field */
	, { (char*)&HashSet_1_t1275____DEFAULT_LOAD_FACTOR_1_DefaultValueData, &Single_t170_0_0_0 }/* value */

};
static const int32_t HashSet_1_t1275____NO_SLOT_2_DefaultValueData = -1;
static Il2CppFieldDefaultValueEntry HashSet_1_t1275____NO_SLOT_2_DefaultValue = 
{
	&HashSet_1_t1275____NO_SLOT_2_FieldInfo/* field */
	, { (char*)&HashSet_1_t1275____NO_SLOT_2_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static const int32_t HashSet_1_t1275____HASH_FLAG_3_DefaultValueData = -2147483648;
static Il2CppFieldDefaultValueEntry HashSet_1_t1275____HASH_FLAG_3_DefaultValue = 
{
	&HashSet_1_t1275____HASH_FLAG_3_FieldInfo/* field */
	, { (char*)&HashSet_1_t1275____HASH_FLAG_3_DefaultValueData, &Int32_t123_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* HashSet_1_t1275_FieldDefaultValues[] = 
{
	&HashSet_1_t1275____INITIAL_SIZE_0_DefaultValue,
	&HashSet_1_t1275____DEFAULT_LOAD_FACTOR_1_DefaultValue,
	&HashSet_1_t1275____NO_SLOT_2_DefaultValue,
	&HashSet_1_t1275____HASH_FLAG_3_DefaultValue,
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType HashSet_1_t1275_0_0_0;
extern Il2CppType HashSet_1_t1275_1_0_0;
struct HashSet_1_t1275;
TypeInfo HashSet_1_t1275_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "HashSet`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, HashSet_1_t1275_MethodInfos/* methods */
	, HashSet_1_t1275_PropertyInfos/* properties */
	, HashSet_1_t1275_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &HashSet_1_t1275_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &HashSet_1_t1275_0_0_0/* byval_arg */
	, &HashSet_1_t1275_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &HashSet_1_t1275_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 23/* method_count */
	, 2/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
void HashSet_1_t1275_CustomAttributesCacheGenerator_HashSet_1_GetObjectData_m6835(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1271 * tmp;
		tmp = (MonoTODOAttribute_t1271 *)il2cpp_codegen_object_new (&MonoTODOAttribute_t1271_il2cpp_TypeInfo);
		MonoTODOAttribute__ctor_m6800(tmp, &MonoTODOAttribute__ctor_m6800_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void HashSet_1_t1275_CustomAttributesCacheGenerator_HashSet_1_OnDeserialization_m6836(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1271 * tmp;
		tmp = (MonoTODOAttribute_t1271 *)il2cpp_codegen_object_new (&MonoTODOAttribute_t1271_il2cpp_TypeInfo);
		MonoTODOAttribute__ctor_m6800(tmp, &MonoTODOAttribute__ctor_m6800_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache HashSet_1_t1275__CustomAttributeCache_HashSet_1_GetObjectData_m6835 = {
1,
NULL,
&HashSet_1_t1275_CustomAttributesCacheGenerator_HashSet_1_GetObjectData_m6835
};
CustomAttributesCache HashSet_1_t1275__CustomAttributeCache_HashSet_1_OnDeserialization_m6836 = {
1,
NULL,
&HashSet_1_t1275_CustomAttributesCacheGenerator_HashSet_1_OnDeserialization_m6836
};
// System.Linq.Check
#include "System_Core_System_Linq_Check.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Check_t1276_il2cpp_TypeInfo;
// System.Linq.Check
#include "System_Core_System_Linq_CheckMethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
extern TypeInfo ArgumentNullException_t1224_il2cpp_TypeInfo;
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
extern MethodInfo ArgumentNullException__ctor_m6710_MethodInfo;


// System.Void System.Linq.Check::Source(System.Object)
extern MethodInfo Check_Source_m6801_MethodInfo;
 void Check_Source_m6801 (Object_t * __this/* static, unused */, Object_t * ___source, MethodInfo* method){
	{
		if (___source)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1224 * L_0 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_0, (String_t*) &_stringLiteral479, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Linq.Check::SourceAndPredicate(System.Object,System.Object)
extern MethodInfo Check_SourceAndPredicate_m6802_MethodInfo;
 void Check_SourceAndPredicate_m6802 (Object_t * __this/* static, unused */, Object_t * ___source, Object_t * ___predicate, MethodInfo* method){
	{
		if (___source)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1224 * L_0 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_0, (String_t*) &_stringLiteral479, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0011:
	{
		if (___predicate)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t1224 * L_1 = (ArgumentNullException_t1224 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1224_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6710(L_1, (String_t*) &_stringLiteral480, /*hidden argument*/&ArgumentNullException__ctor_m6710_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0022:
	{
		return;
	}
}
// Metadata Definition System.Linq.Check
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Check_t1276_Check_Source_m6801_ParameterInfos[] = 
{
	{"source", 0, 134217757, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Linq.Check::Source(System.Object)
MethodInfo Check_Source_m6801_MethodInfo = 
{
	"Source"/* name */
	, (methodPointerType)&Check_Source_m6801/* method */
	, &Check_t1276_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Check_t1276_Check_Source_m6801_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 36/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Check_t1276_Check_SourceAndPredicate_m6802_ParameterInfos[] = 
{
	{"source", 0, 134217758, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"predicate", 1, 134217759, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Linq.Check::SourceAndPredicate(System.Object,System.Object)
MethodInfo Check_SourceAndPredicate_m6802_MethodInfo = 
{
	"SourceAndPredicate"/* name */
	, (methodPointerType)&Check_SourceAndPredicate_m6802/* method */
	, &Check_t1276_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, Check_t1276_Check_SourceAndPredicate_m6802_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 37/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Check_t1276_MethodInfos[] =
{
	&Check_Source_m6801_MethodInfo,
	&Check_SourceAndPredicate_m6802_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
static MethodInfo* Check_t1276_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Check_t1276_0_0_0;
extern Il2CppType Check_t1276_1_0_0;
extern TypeInfo Object_t_il2cpp_TypeInfo;
struct Check_t1276;
TypeInfo Check_t1276_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Check"/* name */
	, "System.Linq"/* namespaze */
	, Check_t1276_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Check_t1276_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Check_t1276_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Check_t1276_il2cpp_TypeInfo/* cast_class */
	, &Check_t1276_0_0_0/* byval_arg */
	, &Check_t1276_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Check_t1276)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1
#include "System_Core_System_Linq_Enumerable_U3CCreateCastIteratorU3Ec.h"
extern Il2CppGenericContainer U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_Il2CppGenericContainer;
extern TypeInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_gp_TResult_0_il2cpp_TypeInfo;
Il2CppGenericParamFull U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull = { { &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_Il2CppGenericContainer, 0}, {NULL, "TResult", 0, 0, NULL} };
static Il2CppGenericParamFull* U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_Il2CppGenericParametersArray[1] = 
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo;
Il2CppGenericContainer U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_Il2CppGenericContainer = { { NULL, NULL }, NULL, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo, 1, 0, U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_Il2CppGenericParametersArray };
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::.ctor()
MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m6838_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 48/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_gp_0_0_0_0;
extern CustomAttributesCache U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m6839;
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m6839_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<TResult>.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* declaring_type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m6839/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 49/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern CustomAttributesCache U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m6840;
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.IEnumerator.get_Current()
MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m6840_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m6840/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 50/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t318_0_0_0;
extern CustomAttributesCache U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m6841;
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.IEnumerable.GetEnumerator()
MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m6841_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t318_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m6841/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 51/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_1_t1294_0_0_0;
extern CustomAttributesCache U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m6842;
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m6842_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<TResult>.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1294_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m6842/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 52/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::MoveNext()
MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m6843_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 53/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern CustomAttributesCache U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m6844;
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::Dispose()
MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m6844_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m6844/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 54/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_MethodInfos[] =
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m6838_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m6839_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m6840_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m6841_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m6842_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m6843_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m6844_MethodInfo,
	NULL
};
extern MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m6839_MethodInfo;
static PropertyInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____System_Collections_Generic_IEnumeratorU3CTResultU3E_Current_PropertyInfo = 
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<TResult>.Current"/* name */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m6839_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m6840_MethodInfo;
static PropertyInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m6840_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_PropertyInfos[] =
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____System_Collections_Generic_IEnumeratorU3CTResultU3E_Current_PropertyInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern Il2CppType IEnumerable_t1179_0_0_3;
FieldInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____source_0_FieldInfo = 
{
	"source"/* name */
	, &IEnumerable_t1179_0_0_3/* type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IEnumerator_t318_0_0_3;
FieldInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____U3C$s_41U3E__0_1_FieldInfo = 
{
	"<$s_41>__0"/* name */
	, &IEnumerator_t318_0_0_3/* type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_gp_0_0_0_3;
FieldInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____U3CelementU3E__1_2_FieldInfo = 
{
	"<element>__1"/* name */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_gp_0_0_0_3/* type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_3;
FieldInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____$PC_3_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t123_0_0_3/* type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_gp_0_0_0_3;
FieldInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____$current_4_FieldInfo = 
{
	"$current"/* name */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_gp_0_0_0_3/* type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IEnumerable_t1179_0_0_3;
FieldInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____U3C$U3Esource_5_FieldInfo = 
{
	"<$>source"/* name */
	, &IEnumerable_t1179_0_0_3/* type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_FieldInfos[] =
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____source_0_FieldInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____U3C$s_41U3E__0_1_FieldInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____U3CelementU3E__1_2_FieldInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____$PC_3_FieldInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____$current_4_FieldInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277____U3C$U3Esource_5_FieldInfo,
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_0_0_0;
extern Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_1_0_0;
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t1277;
extern TypeInfo Enumerable_t148_il2cpp_TypeInfo;
TypeInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateCastIterator>c__Iterator0`1"/* name */
	, ""/* namespaze */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_MethodInfos/* methods */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_PropertyInfos/* properties */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, &Enumerable_t148_il2cpp_TypeInfo/* nested_in */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_0_0_0/* byval_arg */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
extern TypeInfo CompilerGeneratedAttribute_t202_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern MethodInfo CompilerGeneratedAttribute__ctor_m701_MethodInfo;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo DebuggerHiddenAttribute_t496_il2cpp_TypeInfo;
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern MethodInfo DebuggerHiddenAttribute__ctor_m2243_MethodInfo;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m6839(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t496 * tmp;
		tmp = (DebuggerHiddenAttribute_t496 *)il2cpp_codegen_object_new (&DebuggerHiddenAttribute_t496_il2cpp_TypeInfo);
		DebuggerHiddenAttribute__ctor_m2243(tmp, &DebuggerHiddenAttribute__ctor_m2243_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m6840(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t496 * tmp;
		tmp = (DebuggerHiddenAttribute_t496 *)il2cpp_codegen_object_new (&DebuggerHiddenAttribute_t496_il2cpp_TypeInfo);
		DebuggerHiddenAttribute__ctor_m2243(tmp, &DebuggerHiddenAttribute__ctor_m2243_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m6841(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t496 * tmp;
		tmp = (DebuggerHiddenAttribute_t496 *)il2cpp_codegen_object_new (&DebuggerHiddenAttribute_t496_il2cpp_TypeInfo);
		DebuggerHiddenAttribute__ctor_m2243(tmp, &DebuggerHiddenAttribute__ctor_m2243_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m6842(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t496 * tmp;
		tmp = (DebuggerHiddenAttribute_t496 *)il2cpp_codegen_object_new (&DebuggerHiddenAttribute_t496_il2cpp_TypeInfo);
		DebuggerHiddenAttribute__ctor_m2243(tmp, &DebuggerHiddenAttribute__ctor_m2243_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m6844(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t496 * tmp;
		tmp = (DebuggerHiddenAttribute_t496 *)il2cpp_codegen_object_new (&DebuggerHiddenAttribute_t496_il2cpp_TypeInfo);
		DebuggerHiddenAttribute__ctor_m2243(tmp, &DebuggerHiddenAttribute__ctor_m2243_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache = {
1,
NULL,
&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_CustomAttributesCacheGenerator
};
CustomAttributesCache U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m6839 = {
1,
NULL,
&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m6839
};
CustomAttributesCache U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m6840 = {
1,
NULL,
&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m6840
};
CustomAttributesCache U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m6841 = {
1,
NULL,
&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m6841
};
CustomAttributesCache U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m6842 = {
1,
NULL,
&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m6842
};
CustomAttributesCache U3CCreateCastIteratorU3Ec__Iterator0_1_t1277__CustomAttributeCache_U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m6844 = {
1,
NULL,
&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m6844
};
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereIteratorU3E.h"
extern Il2CppGenericContainer U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_Il2CppGenericContainer;
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_Il2CppGenericParametersArray[1] = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo;
Il2CppGenericContainer U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_Il2CppGenericContainer = { { NULL, NULL }, NULL, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo, 1, 0, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_Il2CppGenericParametersArray };
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::.ctor()
MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m6845_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 55/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_gp_0_0_0_0;
extern CustomAttributesCache U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6846;
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6846_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<TSource>.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* declaring_type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6846/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 56/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern CustomAttributesCache U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6847;
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.IEnumerator.get_Current()
MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6847_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6847/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 57/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_t318_0_0_0;
extern CustomAttributesCache U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m6848;
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.IEnumerable.GetEnumerator()
MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m6848_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t318_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m6848/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 58/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerator_1_t1296_0_0_0;
extern CustomAttributesCache U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6849;
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6849_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<TSource>.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1296_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6849/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 59/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t122_0_0_0;
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::MoveNext()
MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m6850_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 60/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern CustomAttributesCache U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m6851;
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::Dispose()
MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m6851_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m6851/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 61/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_MethodInfos[] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m6845_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6846_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6847_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m6848_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6849_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m6850_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m6851_MethodInfo,
	NULL
};
extern MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6846_MethodInfo;
static PropertyInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<TSource>.Current"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6846_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6847_MethodInfo;
static PropertyInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6847_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_PropertyInfos[] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern Il2CppType IEnumerable_1_t1297_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____source_0_FieldInfo = 
{
	"source"/* name */
	, &IEnumerable_1_t1297_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IEnumerator_1_t1296_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____U3C$s_97U3E__0_1_FieldInfo = 
{
	"<$s_97>__0"/* name */
	, &IEnumerator_1_t1296_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_gp_0_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____U3CelementU3E__1_2_FieldInfo = 
{
	"<element>__1"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_gp_0_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1298_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____predicate_3_FieldInfo = 
{
	"predicate"/* name */
	, &Func_2_t1298_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____$PC_4_FieldInfo = 
{
	"$PC"/* name */
	, &Int32_t123_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_gp_0_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____$current_5_FieldInfo = 
{
	"$current"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_gp_0_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IEnumerable_1_t1297_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____U3C$U3Esource_6_FieldInfo = 
{
	"<$>source"/* name */
	, &IEnumerable_1_t1297_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Func_2_t1298_0_0_3;
FieldInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____U3C$U3Epredicate_7_FieldInfo = 
{
	"<$>predicate"/* name */
	, &Func_2_t1298_0_0_3/* type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_FieldInfos[] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____source_0_FieldInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____U3C$s_97U3E__0_1_FieldInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____U3CelementU3E__1_2_FieldInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____predicate_3_FieldInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____$PC_4_FieldInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____$current_5_FieldInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____U3C$U3Esource_6_FieldInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278____U3C$U3Epredicate_7_FieldInfo,
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_0_0_0;
extern Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_1_0_0;
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278;
TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateWhereIterator>c__Iterator1D`1"/* name */
	, ""/* namespaze */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_MethodInfos/* methods */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_PropertyInfos/* properties */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, &Enumerable_t148_il2cpp_TypeInfo/* nested_in */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_0_0_0/* byval_arg */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6846(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t496 * tmp;
		tmp = (DebuggerHiddenAttribute_t496 *)il2cpp_codegen_object_new (&DebuggerHiddenAttribute_t496_il2cpp_TypeInfo);
		DebuggerHiddenAttribute__ctor_m2243(tmp, &DebuggerHiddenAttribute__ctor_m2243_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6847(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t496 * tmp;
		tmp = (DebuggerHiddenAttribute_t496 *)il2cpp_codegen_object_new (&DebuggerHiddenAttribute_t496_il2cpp_TypeInfo);
		DebuggerHiddenAttribute__ctor_m2243(tmp, &DebuggerHiddenAttribute__ctor_m2243_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m6848(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t496 * tmp;
		tmp = (DebuggerHiddenAttribute_t496 *)il2cpp_codegen_object_new (&DebuggerHiddenAttribute_t496_il2cpp_TypeInfo);
		DebuggerHiddenAttribute__ctor_m2243(tmp, &DebuggerHiddenAttribute__ctor_m2243_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6849(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t496 * tmp;
		tmp = (DebuggerHiddenAttribute_t496 *)il2cpp_codegen_object_new (&DebuggerHiddenAttribute_t496_il2cpp_TypeInfo);
		DebuggerHiddenAttribute__ctor_m2243(tmp, &DebuggerHiddenAttribute__ctor_m2243_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m6851(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t496 * tmp;
		tmp = (DebuggerHiddenAttribute_t496 *)il2cpp_codegen_object_new (&DebuggerHiddenAttribute_t496_il2cpp_TypeInfo);
		DebuggerHiddenAttribute__ctor_m2243(tmp, &DebuggerHiddenAttribute__ctor_m2243_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache = {
1,
NULL,
&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_CustomAttributesCacheGenerator
};
CustomAttributesCache U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6846 = {
1,
NULL,
&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m6846
};
CustomAttributesCache U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6847 = {
1,
NULL,
&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m6847
};
CustomAttributesCache U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m6848 = {
1,
NULL,
&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m6848
};
CustomAttributesCache U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6849 = {
1,
NULL,
&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m6849
};
CustomAttributesCache U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278__CustomAttributeCache_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m6851 = {
1,
NULL,
&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m6851
};
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
#ifndef _MSC_VER
#else
#endif
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"



// Metadata Definition System.Linq.Enumerable
extern Il2CppType IEnumerable_1_t1299_0_0_0;
extern Il2CppType IEnumerable_1_t1299_0_0_0;
static ParameterInfo Enumerable_t148_Enumerable_Any_m6852_ParameterInfos[] = 
{
	{"source", 0, 134217760, &EmptyCustomAttributesCache, &IEnumerable_1_t1299_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericContainer Enumerable_Any_m6852_Il2CppGenericContainer;
extern TypeInfo Enumerable_Any_m6852_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_Any_m6852_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_Any_m6852_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_Any_m6852_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Any_m6852_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_Any_m6852_MethodInfo;
Il2CppGenericContainer Enumerable_Any_m6852_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_Any_m6852_MethodInfo, 1, 1, Enumerable_Any_m6852_Il2CppGenericParametersArray };
extern Il2CppType ICollection_1_t1301_0_0_0;
extern Il2CppGenericMethod ICollection_1_get_Count_m6853_GenericMethod;
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m6854_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Any_m6852_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &ICollection_1_t1301_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ICollection_1_get_Count_m6853_GenericMethod }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IEnumerable_1_GetEnumerator_m6854_GenericMethod }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Any_m6852;
// System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
MethodInfo Enumerable_Any_m6852_MethodInfo = 
{
	"Any"/* name */
	, NULL/* method */
	, &Enumerable_t148_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t148_Enumerable_Any_m6852_ParameterInfos/* parameters */
	, &Enumerable_t148__CustomAttributeCache_Enumerable_Any_m6852/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 38/* token */
	, Enumerable_Any_m6852_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Any_m6852_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_t1179_0_0_0;
extern Il2CppType IEnumerable_t1179_0_0_0;
static ParameterInfo Enumerable_t148_Enumerable_Cast_m6855_ParameterInfos[] = 
{
	{"source", 0, 134217761, &EmptyCustomAttributesCache, &IEnumerable_t1179_0_0_0},
};
extern Il2CppType IEnumerable_1_t1302_0_0_0;
extern Il2CppGenericContainer Enumerable_Cast_m6855_Il2CppGenericContainer;
extern TypeInfo Enumerable_Cast_m6855_gp_TResult_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_Cast_m6855_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_Cast_m6855_Il2CppGenericContainer, 0}, {NULL, "TResult", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_Cast_m6855_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Cast_m6855_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_Cast_m6855_MethodInfo;
Il2CppGenericContainer Enumerable_Cast_m6855_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_Cast_m6855_MethodInfo, 1, 1, Enumerable_Cast_m6855_Il2CppGenericParametersArray };
extern Il2CppType IEnumerable_1_t1302_0_0_0;
extern Il2CppGenericMethod Enumerable_CreateCastIterator_TisTResult_t1303_m6856_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Cast_m6855_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &IEnumerable_1_t1302_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_CreateCastIterator_TisTResult_t1303_m6856_GenericMethod }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Cast_m6855;
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
MethodInfo Enumerable_Cast_m6855_MethodInfo = 
{
	"Cast"/* name */
	, NULL/* method */
	, &Enumerable_t148_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1302_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t148_Enumerable_Cast_m6855_ParameterInfos/* parameters */
	, &Enumerable_t148__CustomAttributeCache_Enumerable_Cast_m6855/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 39/* token */
	, Enumerable_Cast_m6855_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Cast_m6855_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_t1179_0_0_0;
static ParameterInfo Enumerable_t148_Enumerable_CreateCastIterator_m6857_ParameterInfos[] = 
{
	{"source", 0, 134217762, &EmptyCustomAttributesCache, &IEnumerable_t1179_0_0_0},
};
extern Il2CppType IEnumerable_1_t1304_0_0_0;
extern Il2CppGenericContainer Enumerable_CreateCastIterator_m6857_Il2CppGenericContainer;
extern TypeInfo Enumerable_CreateCastIterator_m6857_gp_TResult_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_CreateCastIterator_m6857_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_CreateCastIterator_m6857_Il2CppGenericContainer, 0}, {NULL, "TResult", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_CreateCastIterator_m6857_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_CreateCastIterator_m6857_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_CreateCastIterator_m6857_MethodInfo;
Il2CppGenericContainer Enumerable_CreateCastIterator_m6857_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_CreateCastIterator_m6857_MethodInfo, 1, 1, Enumerable_CreateCastIterator_m6857_Il2CppGenericParametersArray };
extern Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1306_0_0_0;
extern Il2CppGenericMethod U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m6858_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_CreateCastIterator_m6857_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1306_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m6858_GenericMethod }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_CreateCastIterator_m6857;
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CreateCastIterator(System.Collections.IEnumerable)
MethodInfo Enumerable_CreateCastIterator_m6857_MethodInfo = 
{
	"CreateCastIterator"/* name */
	, NULL/* method */
	, &Enumerable_t148_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1304_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t148_Enumerable_CreateCastIterator_m6857_ParameterInfos/* parameters */
	, &Enumerable_t148__CustomAttributeCache_Enumerable_CreateCastIterator_m6857/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 40/* token */
	, Enumerable_CreateCastIterator_m6857_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_CreateCastIterator_m6857_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1307_0_0_0;
extern Il2CppType IEnumerable_1_t1307_0_0_0;
extern Il2CppType Enumerable_Contains_m6859_gp_0_0_0_0;
extern Il2CppType Enumerable_Contains_m6859_gp_0_0_0_0;
static ParameterInfo Enumerable_t148_Enumerable_Contains_m6859_ParameterInfos[] = 
{
	{"source", 0, 134217763, &EmptyCustomAttributesCache, &IEnumerable_1_t1307_0_0_0},
	{"value", 1, 134217764, &EmptyCustomAttributesCache, &Enumerable_Contains_m6859_gp_0_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericContainer Enumerable_Contains_m6859_Il2CppGenericContainer;
extern TypeInfo Enumerable_Contains_m6859_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_Contains_m6859_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_Contains_m6859_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_Contains_m6859_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Contains_m6859_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_Contains_m6859_MethodInfo;
Il2CppGenericContainer Enumerable_Contains_m6859_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_Contains_m6859_MethodInfo, 1, 1, Enumerable_Contains_m6859_Il2CppGenericParametersArray };
extern Il2CppType ICollection_1_t1309_0_0_0;
extern Il2CppGenericMethod ICollection_1_Contains_m6860_GenericMethod;
extern Il2CppGenericMethod Enumerable_Contains_TisTSource_t1308_m6861_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Contains_m6859_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &ICollection_1_t1309_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ICollection_1_Contains_m6860_GenericMethod }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_Contains_TisTSource_t1308_m6861_GenericMethod }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Contains_m6859;
// System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
MethodInfo Enumerable_Contains_m6859_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &Enumerable_t148_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t148_Enumerable_Contains_m6859_ParameterInfos/* parameters */
	, &Enumerable_t148__CustomAttributeCache_Enumerable_Contains_m6859/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 41/* token */
	, Enumerable_Contains_m6859_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Contains_m6859_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1310_0_0_0;
extern Il2CppType IEnumerable_1_t1310_0_0_0;
extern Il2CppType Enumerable_Contains_m6862_gp_0_0_0_0;
extern Il2CppType Enumerable_Contains_m6862_gp_0_0_0_0;
extern Il2CppType IEqualityComparer_1_t1312_0_0_0;
extern Il2CppType IEqualityComparer_1_t1312_0_0_0;
static ParameterInfo Enumerable_t148_Enumerable_Contains_m6862_ParameterInfos[] = 
{
	{"source", 0, 134217765, &EmptyCustomAttributesCache, &IEnumerable_1_t1310_0_0_0},
	{"value", 1, 134217766, &EmptyCustomAttributesCache, &Enumerable_Contains_m6862_gp_0_0_0_0},
	{"comparer", 2, 134217767, &EmptyCustomAttributesCache, &IEqualityComparer_1_t1312_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern Il2CppGenericContainer Enumerable_Contains_m6862_Il2CppGenericContainer;
extern TypeInfo Enumerable_Contains_m6862_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_Contains_m6862_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_Contains_m6862_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_Contains_m6862_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Contains_m6862_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_Contains_m6862_MethodInfo;
Il2CppGenericContainer Enumerable_Contains_m6862_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_Contains_m6862_MethodInfo, 1, 1, Enumerable_Contains_m6862_Il2CppGenericParametersArray };
extern Il2CppGenericMethod EqualityComparer_1_get_Default_m6863_GenericMethod;
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m6864_GenericMethod;
extern Il2CppGenericMethod IEnumerator_1_get_Current_m6865_GenericMethod;
extern Il2CppGenericMethod IEqualityComparer_1_Equals_m6866_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Contains_m6862_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &EqualityComparer_1_get_Default_m6863_GenericMethod }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IEnumerable_1_GetEnumerator_m6864_GenericMethod }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IEnumerator_1_get_Current_m6865_GenericMethod }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IEqualityComparer_1_Equals_m6866_GenericMethod }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Contains_m6862;
// System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
MethodInfo Enumerable_Contains_m6862_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &Enumerable_t148_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t148_Enumerable_Contains_m6862_ParameterInfos/* parameters */
	, &Enumerable_t148__CustomAttributeCache_Enumerable_Contains_m6862/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 42/* token */
	, Enumerable_Contains_m6862_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Contains_m6862_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1313_0_0_0;
extern Il2CppType IEnumerable_1_t1313_0_0_0;
static ParameterInfo Enumerable_t148_Enumerable_First_m6867_ParameterInfos[] = 
{
	{"source", 0, 134217768, &EmptyCustomAttributesCache, &IEnumerable_1_t1313_0_0_0},
};
extern Il2CppType Enumerable_First_m6867_gp_0_0_0_0;
extern Il2CppGenericContainer Enumerable_First_m6867_Il2CppGenericContainer;
extern TypeInfo Enumerable_First_m6867_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_First_m6867_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_First_m6867_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_First_m6867_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_First_m6867_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_First_m6867_MethodInfo;
Il2CppGenericContainer Enumerable_First_m6867_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_First_m6867_MethodInfo, 1, 1, Enumerable_First_m6867_Il2CppGenericParametersArray };
extern Il2CppType IList_1_t1315_0_0_0;
extern Il2CppGenericMethod ICollection_1_get_Count_m6868_GenericMethod;
extern Il2CppGenericMethod IList_1_get_Item_m6869_GenericMethod;
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m6870_GenericMethod;
extern Il2CppGenericMethod IEnumerator_1_get_Current_m6871_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_First_m6867_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &IList_1_t1315_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ICollection_1_get_Count_m6868_GenericMethod }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IList_1_get_Item_m6869_GenericMethod }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IEnumerable_1_GetEnumerator_m6870_GenericMethod }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IEnumerator_1_get_Current_m6871_GenericMethod }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_First_m6867;
// TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
MethodInfo Enumerable_First_m6867_MethodInfo = 
{
	"First"/* name */
	, NULL/* method */
	, &Enumerable_t148_il2cpp_TypeInfo/* declaring_type */
	, &Enumerable_First_m6867_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t148_Enumerable_First_m6867_ParameterInfos/* parameters */
	, &Enumerable_t148__CustomAttributeCache_Enumerable_First_m6867/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 43/* token */
	, Enumerable_First_m6867_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_First_m6867_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1316_0_0_0;
extern Il2CppType IEnumerable_1_t1316_0_0_0;
static ParameterInfo Enumerable_t148_Enumerable_ToArray_m6872_ParameterInfos[] = 
{
	{"source", 0, 134217769, &EmptyCustomAttributesCache, &IEnumerable_1_t1316_0_0_0},
};
extern Il2CppType TSourceU5BU5D_t1318_0_0_0;
extern Il2CppGenericContainer Enumerable_ToArray_m6872_Il2CppGenericContainer;
extern TypeInfo Enumerable_ToArray_m6872_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_ToArray_m6872_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_ToArray_m6872_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_ToArray_m6872_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_ToArray_m6872_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_ToArray_m6872_MethodInfo;
Il2CppGenericContainer Enumerable_ToArray_m6872_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_ToArray_m6872_MethodInfo, 1, 1, Enumerable_ToArray_m6872_Il2CppGenericParametersArray };
extern Il2CppType ICollection_1_t1319_0_0_0;
extern Il2CppGenericMethod ICollection_1_get_Count_m6873_GenericMethod;
extern Il2CppType TSourceU5BU5D_t1318_0_0_0;
extern Il2CppGenericMethod ICollection_1_CopyTo_m6874_GenericMethod;
extern Il2CppType List_1_t1320_0_0_0;
extern Il2CppGenericMethod List_1__ctor_m6875_GenericMethod;
extern Il2CppGenericMethod List_1_ToArray_m6876_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_ToArray_m6872_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &ICollection_1_t1319_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ICollection_1_get_Count_m6873_GenericMethod }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &TSourceU5BU5D_t1318_0_0_0 }/* Array Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ICollection_1_CopyTo_m6874_GenericMethod }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &List_1_t1320_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1__ctor_m6875_GenericMethod }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_ToArray_m6876_GenericMethod }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_ToArray_m6872;
// TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
MethodInfo Enumerable_ToArray_m6872_MethodInfo = 
{
	"ToArray"/* name */
	, NULL/* method */
	, &Enumerable_t148_il2cpp_TypeInfo/* declaring_type */
	, &TSourceU5BU5D_t1318_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t148_Enumerable_ToArray_m6872_ParameterInfos/* parameters */
	, &Enumerable_t148__CustomAttributeCache_Enumerable_ToArray_m6872/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 44/* token */
	, Enumerable_ToArray_m6872_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_ToArray_m6872_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1321_0_0_0;
extern Il2CppType IEnumerable_1_t1321_0_0_0;
static ParameterInfo Enumerable_t148_Enumerable_ToList_m6877_ParameterInfos[] = 
{
	{"source", 0, 134217770, &EmptyCustomAttributesCache, &IEnumerable_1_t1321_0_0_0},
};
extern Il2CppType List_1_t1323_0_0_0;
extern Il2CppGenericContainer Enumerable_ToList_m6877_Il2CppGenericContainer;
extern TypeInfo Enumerable_ToList_m6877_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_ToList_m6877_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_ToList_m6877_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_ToList_m6877_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_ToList_m6877_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_ToList_m6877_MethodInfo;
Il2CppGenericContainer Enumerable_ToList_m6877_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_ToList_m6877_MethodInfo, 1, 1, Enumerable_ToList_m6877_Il2CppGenericParametersArray };
extern Il2CppType List_1_t1323_0_0_0;
extern Il2CppGenericMethod List_1__ctor_m6878_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_ToList_m6877_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &List_1_t1323_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1__ctor_m6878_GenericMethod }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_ToList_m6877;
// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
MethodInfo Enumerable_ToList_m6877_MethodInfo = 
{
	"ToList"/* name */
	, NULL/* method */
	, &Enumerable_t148_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t1323_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t148_Enumerable_ToList_m6877_ParameterInfos/* parameters */
	, &Enumerable_t148__CustomAttributeCache_Enumerable_ToList_m6877/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 45/* token */
	, Enumerable_ToList_m6877_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_ToList_m6877_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1324_0_0_0;
extern Il2CppType IEnumerable_1_t1324_0_0_0;
extern Il2CppType Func_2_t1325_0_0_0;
extern Il2CppType Func_2_t1325_0_0_0;
static ParameterInfo Enumerable_t148_Enumerable_Where_m6879_ParameterInfos[] = 
{
	{"source", 0, 134217771, &EmptyCustomAttributesCache, &IEnumerable_1_t1324_0_0_0},
	{"predicate", 1, 134217772, &EmptyCustomAttributesCache, &Func_2_t1325_0_0_0},
};
extern Il2CppType IEnumerable_1_t1324_0_0_0;
extern Il2CppGenericContainer Enumerable_Where_m6879_Il2CppGenericContainer;
extern TypeInfo Enumerable_Where_m6879_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_Where_m6879_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_Where_m6879_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_Where_m6879_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Where_m6879_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_Where_m6879_MethodInfo;
Il2CppGenericContainer Enumerable_Where_m6879_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_Where_m6879_MethodInfo, 1, 1, Enumerable_Where_m6879_Il2CppGenericParametersArray };
extern Il2CppGenericMethod Enumerable_CreateWhereIterator_TisTSource_t1326_m6880_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Where_m6879_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_CreateWhereIterator_TisTSource_t1326_m6880_GenericMethod }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Where_m6879;
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
MethodInfo Enumerable_Where_m6879_MethodInfo = 
{
	"Where"/* name */
	, NULL/* method */
	, &Enumerable_t148_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1324_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t148_Enumerable_Where_m6879_ParameterInfos/* parameters */
	, &Enumerable_t148__CustomAttributeCache_Enumerable_Where_m6879/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 46/* token */
	, Enumerable_Where_m6879_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Where_m6879_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType IEnumerable_1_t1327_0_0_0;
extern Il2CppType IEnumerable_1_t1327_0_0_0;
extern Il2CppType Func_2_t1328_0_0_0;
extern Il2CppType Func_2_t1328_0_0_0;
static ParameterInfo Enumerable_t148_Enumerable_CreateWhereIterator_m6881_ParameterInfos[] = 
{
	{"source", 0, 134217773, &EmptyCustomAttributesCache, &IEnumerable_1_t1327_0_0_0},
	{"predicate", 1, 134217774, &EmptyCustomAttributesCache, &Func_2_t1328_0_0_0},
};
extern Il2CppType IEnumerable_1_t1327_0_0_0;
extern Il2CppGenericContainer Enumerable_CreateWhereIterator_m6881_Il2CppGenericContainer;
extern TypeInfo Enumerable_CreateWhereIterator_m6881_gp_TSource_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Enumerable_CreateWhereIterator_m6881_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { { &Enumerable_CreateWhereIterator_m6881_Il2CppGenericContainer, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* Enumerable_CreateWhereIterator_m6881_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_CreateWhereIterator_m6881_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo Enumerable_CreateWhereIterator_m6881_MethodInfo;
Il2CppGenericContainer Enumerable_CreateWhereIterator_m6881_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Enumerable_CreateWhereIterator_m6881_MethodInfo, 1, 1, Enumerable_CreateWhereIterator_m6881_Il2CppGenericParametersArray };
extern Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1330_0_0_0;
extern Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m6882_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_CreateWhereIterator_m6881_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1330_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m6882_GenericMethod }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_CreateWhereIterator_m6881;
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::CreateWhereIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
MethodInfo Enumerable_CreateWhereIterator_m6881_MethodInfo = 
{
	"CreateWhereIterator"/* name */
	, NULL/* method */
	, &Enumerable_t148_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1327_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t148_Enumerable_CreateWhereIterator_m6881_ParameterInfos/* parameters */
	, &Enumerable_t148__CustomAttributeCache_Enumerable_CreateWhereIterator_m6881/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 47/* token */
	, Enumerable_CreateWhereIterator_m6881_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_CreateWhereIterator_m6881_Il2CppGenericContainer/* genericContainer */

};
static MethodInfo* Enumerable_t148_MethodInfos[] =
{
	&Enumerable_Any_m6852_MethodInfo,
	&Enumerable_Cast_m6855_MethodInfo,
	&Enumerable_CreateCastIterator_m6857_MethodInfo,
	&Enumerable_Contains_m6859_MethodInfo,
	&Enumerable_Contains_m6862_MethodInfo,
	&Enumerable_First_m6867_MethodInfo,
	&Enumerable_ToArray_m6872_MethodInfo,
	&Enumerable_ToList_m6877_MethodInfo,
	&Enumerable_Where_m6879_MethodInfo,
	&Enumerable_CreateWhereIterator_m6881_MethodInfo,
	NULL
};
extern TypeInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo;
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo;
static TypeInfo* Enumerable_t148_il2cpp_TypeInfo__nestedTypes[3] =
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1277_il2cpp_TypeInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1278_il2cpp_TypeInfo,
	NULL
};
static MethodInfo* Enumerable_t148_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
void Enumerable_t148_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t822 * tmp;
		tmp = (ExtensionAttribute_t822 *)il2cpp_codegen_object_new (&ExtensionAttribute_t822_il2cpp_TypeInfo);
		ExtensionAttribute__ctor_m4522(tmp, &ExtensionAttribute__ctor_m4522_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_Any_m6852(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t822 * tmp;
		tmp = (ExtensionAttribute_t822 *)il2cpp_codegen_object_new (&ExtensionAttribute_t822_il2cpp_TypeInfo);
		ExtensionAttribute__ctor_m4522(tmp, &ExtensionAttribute__ctor_m4522_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_Cast_m6855(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t822 * tmp;
		tmp = (ExtensionAttribute_t822 *)il2cpp_codegen_object_new (&ExtensionAttribute_t822_il2cpp_TypeInfo);
		ExtensionAttribute__ctor_m4522(tmp, &ExtensionAttribute__ctor_m4522_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_CreateCastIterator_m6857(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t496 * tmp;
		tmp = (DebuggerHiddenAttribute_t496 *)il2cpp_codegen_object_new (&DebuggerHiddenAttribute_t496_il2cpp_TypeInfo);
		DebuggerHiddenAttribute__ctor_m2243(tmp, &DebuggerHiddenAttribute__ctor_m2243_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_Contains_m6859(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t822 * tmp;
		tmp = (ExtensionAttribute_t822 *)il2cpp_codegen_object_new (&ExtensionAttribute_t822_il2cpp_TypeInfo);
		ExtensionAttribute__ctor_m4522(tmp, &ExtensionAttribute__ctor_m4522_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_Contains_m6862(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t822 * tmp;
		tmp = (ExtensionAttribute_t822 *)il2cpp_codegen_object_new (&ExtensionAttribute_t822_il2cpp_TypeInfo);
		ExtensionAttribute__ctor_m4522(tmp, &ExtensionAttribute__ctor_m4522_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_First_m6867(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t822 * tmp;
		tmp = (ExtensionAttribute_t822 *)il2cpp_codegen_object_new (&ExtensionAttribute_t822_il2cpp_TypeInfo);
		ExtensionAttribute__ctor_m4522(tmp, &ExtensionAttribute__ctor_m4522_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_ToArray_m6872(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t822 * tmp;
		tmp = (ExtensionAttribute_t822 *)il2cpp_codegen_object_new (&ExtensionAttribute_t822_il2cpp_TypeInfo);
		ExtensionAttribute__ctor_m4522(tmp, &ExtensionAttribute__ctor_m4522_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_ToList_m6877(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t822 * tmp;
		tmp = (ExtensionAttribute_t822 *)il2cpp_codegen_object_new (&ExtensionAttribute_t822_il2cpp_TypeInfo);
		ExtensionAttribute__ctor_m4522(tmp, &ExtensionAttribute__ctor_m4522_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_Where_m6879(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t822 * tmp;
		tmp = (ExtensionAttribute_t822 *)il2cpp_codegen_object_new (&ExtensionAttribute_t822_il2cpp_TypeInfo);
		ExtensionAttribute__ctor_m4522(tmp, &ExtensionAttribute__ctor_m4522_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_CreateWhereIterator_m6881(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t496 * tmp;
		tmp = (DebuggerHiddenAttribute_t496 *)il2cpp_codegen_object_new (&DebuggerHiddenAttribute_t496_il2cpp_TypeInfo);
		DebuggerHiddenAttribute__ctor_m2243(tmp, &DebuggerHiddenAttribute__ctor_m2243_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache Enumerable_t148__CustomAttributeCache = {
1,
NULL,
&Enumerable_t148_CustomAttributesCacheGenerator
};
CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Any_m6852 = {
1,
NULL,
&Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_Any_m6852
};
CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Cast_m6855 = {
1,
NULL,
&Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_Cast_m6855
};
CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_CreateCastIterator_m6857 = {
1,
NULL,
&Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_CreateCastIterator_m6857
};
CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Contains_m6859 = {
1,
NULL,
&Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_Contains_m6859
};
CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Contains_m6862 = {
1,
NULL,
&Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_Contains_m6862
};
CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_First_m6867 = {
1,
NULL,
&Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_First_m6867
};
CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_ToArray_m6872 = {
1,
NULL,
&Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_ToArray_m6872
};
CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_ToList_m6877 = {
1,
NULL,
&Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_ToList_m6877
};
CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Where_m6879 = {
1,
NULL,
&Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_Where_m6879
};
CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_CreateWhereIterator_m6881 = {
1,
NULL,
&Enumerable_t148_CustomAttributesCacheGenerator_Enumerable_CreateWhereIterator_m6881
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Enumerable_t148_0_0_0;
extern Il2CppType Enumerable_t148_1_0_0;
struct Enumerable_t148;
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache;
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Any_m6852;
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Cast_m6855;
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_CreateCastIterator_m6857;
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Contains_m6859;
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Contains_m6862;
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_First_m6867;
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_ToArray_m6872;
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_ToList_m6877;
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_Where_m6879;
extern CustomAttributesCache Enumerable_t148__CustomAttributeCache_Enumerable_CreateWhereIterator_m6881;
TypeInfo Enumerable_t148_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerable"/* name */
	, "System.Linq"/* namespaze */
	, Enumerable_t148_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, Enumerable_t148_il2cpp_TypeInfo__nestedTypes/* nested_types */
	, NULL/* nested_in */
	, &Enumerable_t148_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Enumerable_t148_VTable/* vtable */
	, &Enumerable_t148__CustomAttributeCache/* custom_attributes_cache */
	, &Enumerable_t148_il2cpp_TypeInfo/* cast_class */
	, &Enumerable_t148_0_0_0/* byval_arg */
	, &Enumerable_t148_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerable_t148)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Action
#include "System_Core_System_Action.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Action_t147_il2cpp_TypeInfo;
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern MethodInfo Action__ctor_m4584_MethodInfo;
 void Action__ctor_m4584 (Action_t147 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method){
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Action::Invoke()
extern MethodInfo Action_Invoke_m331_MethodInfo;
 void Action_Invoke_m331 (Action_t147 * __this, MethodInfo* method){
	if(__this->___prev_9 != NULL)
	{
		Action_Invoke_m331((Action_t147 *)__this->___prev_9, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
	((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
}
void pinvoke_delegate_wrapper_Action_t147(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult System.Action::BeginInvoke(System.AsyncCallback,System.Object)
extern MethodInfo Action_BeginInvoke_m6803_MethodInfo;
 Object_t * Action_BeginInvoke_m6803 (Action_t147 * __this, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method){
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Action::EndInvoke(System.IAsyncResult)
extern MethodInfo Action_EndInvoke_m6804_MethodInfo;
 void Action_EndInvoke_m6804 (Action_t147 * __this, Object_t * ___result, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Metadata Definition System.Action
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Action_t147_Action__ctor_m4584_ParameterInfos[] = 
{
	{"object", 0, 134217775, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217776, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
MethodInfo Action__ctor_m4584_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Action__ctor_m4584/* method */
	, &Action_t147_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, Action_t147_Action__ctor_m4584_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 62/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
// System.Void System.Action::Invoke()
MethodInfo Action_Invoke_m331_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Action_Invoke_m331/* method */
	, &Action_t147_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 63/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_t147_Action_BeginInvoke_m6803_ParameterInfos[] = 
{
	{"callback", 0, 134217777, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 1, 134217778, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Action::BeginInvoke(System.AsyncCallback,System.Object)
MethodInfo Action_BeginInvoke_m6803_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Action_BeginInvoke_m6803/* method */
	, &Action_t147_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, Action_t147_Action_BeginInvoke_m6803_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 64/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo Action_t147_Action_EndInvoke_m6804_ParameterInfos[] = 
{
	{"result", 0, 134217779, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void System.Action::EndInvoke(System.IAsyncResult)
MethodInfo Action_EndInvoke_m6804_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Action_EndInvoke_m6804/* method */
	, &Action_t147_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Action_t147_Action_EndInvoke_m6804_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 65/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Action_t147_MethodInfos[] =
{
	&Action__ctor_m4584_MethodInfo,
	&Action_Invoke_m331_MethodInfo,
	&Action_BeginInvoke_m6803_MethodInfo,
	&Action_EndInvoke_m6804_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
static MethodInfo* Action_t147_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&Action_Invoke_m331_MethodInfo,
	&Action_BeginInvoke_m6803_MethodInfo,
	&Action_EndInvoke_m6804_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair Action_t147_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Action_t147_0_0_0;
extern Il2CppType Action_t147_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct Action_t147;
TypeInfo Action_t147_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action"/* name */
	, "System"/* namespaze */
	, Action_t147_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Action_t147_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Action_t147_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Action_t147_il2cpp_TypeInfo/* cast_class */
	, &Action_t147_0_0_0/* byval_arg */
	, &Action_t147_1_0_0/* this_arg */
	, Action_t147_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_Action_t147/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Action_t147)/* instance_size */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Func`2
#include "System_Core_System_Func_2.h"
extern Il2CppGenericContainer Func_2_t1279_Il2CppGenericContainer;
extern TypeInfo Func_2_t1279_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull Func_2_t1279_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &Func_2_t1279_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
extern TypeInfo Func_2_t1279_gp_TResult_1_il2cpp_TypeInfo;
Il2CppGenericParamFull Func_2_t1279_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull = { { &Func_2_t1279_Il2CppGenericContainer, 1}, {NULL, "TResult", 0, 0, NULL} };
static Il2CppGenericParamFull* Func_2_t1279_Il2CppGenericParametersArray[2] = 
{
	&Func_2_t1279_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
	&Func_2_t1279_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo Func_2_t1279_il2cpp_TypeInfo;
Il2CppGenericContainer Func_2_t1279_Il2CppGenericContainer = { { NULL, NULL }, NULL, &Func_2_t1279_il2cpp_TypeInfo, 2, 0, Func_2_t1279_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Func_2_t1279_Func_2__ctor_m6883_ParameterInfos[] = 
{
	{"object", 0, 134217780, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217781, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
// System.Void System.Func`2::.ctor(System.Object,System.IntPtr)
MethodInfo Func_2__ctor_m6883_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Func_2_t1279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1279_Func_2__ctor_m6883_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 66/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Func_2_t1279_gp_0_0_0_0;
extern Il2CppType Func_2_t1279_gp_0_0_0_0;
static ParameterInfo Func_2_t1279_Func_2_Invoke_m6884_ParameterInfos[] = 
{
	{"arg1", 0, 134217782, &EmptyCustomAttributesCache, &Func_2_t1279_gp_0_0_0_0},
};
extern Il2CppType Func_2_t1279_gp_1_0_0_0;
// TResult System.Func`2::Invoke(T)
MethodInfo Func_2_Invoke_m6884_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Func_2_t1279_il2cpp_TypeInfo/* declaring_type */
	, &Func_2_t1279_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1279_Func_2_Invoke_m6884_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 67/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Func_2_t1279_gp_0_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Func_2_t1279_Func_2_BeginInvoke_m6885_ParameterInfos[] = 
{
	{"arg1", 0, 134217783, &EmptyCustomAttributesCache, &Func_2_t1279_gp_0_0_0_0},
	{"callback", 1, 134217784, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217785, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
// System.IAsyncResult System.Func`2::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Func_2_BeginInvoke_m6885_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Func_2_t1279_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1279_Func_2_BeginInvoke_m6885_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 68/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo Func_2_t1279_Func_2_EndInvoke_m6886_ParameterInfos[] = 
{
	{"result", 0, 134217786, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Func_2_t1279_gp_1_0_0_0;
// TResult System.Func`2::EndInvoke(System.IAsyncResult)
MethodInfo Func_2_EndInvoke_m6886_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Func_2_t1279_il2cpp_TypeInfo/* declaring_type */
	, &Func_2_t1279_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1279_Func_2_EndInvoke_m6886_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 69/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Func_2_t1279_MethodInfos[] =
{
	&Func_2__ctor_m6883_MethodInfo,
	&Func_2_Invoke_m6884_MethodInfo,
	&Func_2_BeginInvoke_m6885_MethodInfo,
	&Func_2_EndInvoke_m6886_MethodInfo,
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Func_2_t1279_0_0_0;
extern Il2CppType Func_2_t1279_1_0_0;
struct Func_2_t1279;
TypeInfo Func_2_t1279_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Func`2"/* name */
	, "System"/* namespaze */
	, Func_2_t1279_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Func_2_t1279_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &Func_2_t1279_0_0_0/* byval_arg */
	, &Func_2_t1279_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &Func_2_t1279_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$136
#include "System_Core_U3CPrivateImplementationDetailsU3E_$ArrayType$13.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo $ArrayType$136_t1280_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$136
#include "System_Core_U3CPrivateImplementationDetailsU3E_$ArrayType$13MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$136
void $ArrayType$136_t1280_marshal(const $ArrayType$136_t1280& unmarshaled, $ArrayType$136_t1280_marshaled& marshaled)
{
}
void $ArrayType$136_t1280_marshal_back(const $ArrayType$136_t1280_marshaled& marshaled, $ArrayType$136_t1280& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$136
void $ArrayType$136_t1280_marshal_cleanup($ArrayType$136_t1280_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$136
static MethodInfo* $ArrayType$136_t1280_MethodInfos[] =
{
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
static MethodInfo* $ArrayType$136_t1280_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType $ArrayType$136_t1280_0_0_0;
extern Il2CppType $ArrayType$136_t1280_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1281_il2cpp_TypeInfo;
TypeInfo $ArrayType$136_t1280_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$136"/* name */
	, ""/* namespaze */
	, $ArrayType$136_t1280_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &U3CPrivateImplementationDetailsU3E_t1281_il2cpp_TypeInfo/* nested_in */
	, &$ArrayType$136_t1280_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, $ArrayType$136_t1280_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &$ArrayType$136_t1280_il2cpp_TypeInfo/* cast_class */
	, &$ArrayType$136_t1280_0_0_0/* byval_arg */
	, &$ArrayType$136_t1280_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)$ArrayType$136_t1280_marshal/* marshal_to_native_func */
	, (methodPointerType)$ArrayType$136_t1280_marshal_back/* marshal_from_native_func */
	, (methodPointerType)$ArrayType$136_t1280_marshal_cleanup/* marshal_cleanup_func */
	, sizeof ($ArrayType$136_t1280)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, sizeof($ArrayType$136_t1280_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3E.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"



// Metadata Definition <PrivateImplementationDetails>
extern Il2CppType $ArrayType$136_t1280_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3E_t1281____$$fieldU2D0_0_FieldInfo = 
{
	"$$field-0"/* name */
	, &$ArrayType$136_t1280_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3E_t1281_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3E_t1281_StaticFields, ___$$fieldU2D0_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* U3CPrivateImplementationDetailsU3E_t1281_FieldInfos[] =
{
	&U3CPrivateImplementationDetailsU3E_t1281____$$fieldU2D0_0_FieldInfo,
	NULL
};
static const uint8_t U3CPrivateImplementationDetailsU3E_t1281____$$fieldU2D0_0_DefaultValueData[] = { 0xB, 0x0, 0x0, 0x0, 0x13, 0x0, 0x0, 0x0, 0x25, 0x0, 0x0, 0x0, 0x49, 0x0, 0x0, 0x0, 0x6D, 0x0, 0x0, 0x0, 0xA3, 0x0, 0x0, 0x0, 0xFB, 0x0, 0x0, 0x0, 0x6F, 0x1, 0x0, 0x0, 0x2D, 0x2, 0x0, 0x0, 0x37, 0x3, 0x0, 0x0, 0xD5, 0x4, 0x0, 0x0, 0x45, 0x7, 0x0, 0x0, 0xD9, 0xA, 0x0, 0x0, 0x51, 0x10, 0x0, 0x0, 0x67, 0x18, 0x0, 0x0, 0x9B, 0x24, 0x0, 0x0, 0xE9, 0x36, 0x0, 0x0, 0x61, 0x52, 0x0, 0x0, 0x8B, 0x7B, 0x0, 0x0, 0x47, 0xB9, 0x0, 0x0, 0xE7, 0x15, 0x1, 0x0, 0xE1, 0xA0, 0x1, 0x0, 0x49, 0x71, 0x2, 0x0, 0xE5, 0xA9, 0x3, 0x0, 0xE3, 0x7E, 0x5, 0x0, 0x39, 0x3E, 0x8, 0x0, 0x67, 0x5D, 0xC, 0x0, 0x9, 0x8C, 0x12, 0x0, 0xFF, 0xD1, 0x1B, 0x0, 0x13, 0xBB, 0x29, 0x0, 0x8B, 0x98, 0x3E, 0x0, 0xC1, 0xE4, 0x5D, 0x0, 0x21, 0xD7, 0x8C, 0x0, 0xAB, 0x42, 0xD3, 0x0 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3E_t1281____$$fieldU2D0_0_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3E_t1281____$$fieldU2D0_0_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3E_t1281____$$fieldU2D0_0_DefaultValueData, &$ArrayType$136_t1280_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* U3CPrivateImplementationDetailsU3E_t1281_FieldDefaultValues[] = 
{
	&U3CPrivateImplementationDetailsU3E_t1281____$$fieldU2D0_0_DefaultValue,
	NULL
};
static MethodInfo* U3CPrivateImplementationDetailsU3E_t1281_MethodInfos[] =
{
	NULL
};
extern TypeInfo $ArrayType$136_t1280_il2cpp_TypeInfo;
static TypeInfo* U3CPrivateImplementationDetailsU3E_t1281_il2cpp_TypeInfo__nestedTypes[2] =
{
	&$ArrayType$136_t1280_il2cpp_TypeInfo,
	NULL
};
static MethodInfo* U3CPrivateImplementationDetailsU3E_t1281_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
};
void U3CPrivateImplementationDetailsU3E_t1281_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t202 * tmp;
		tmp = (CompilerGeneratedAttribute_t202 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t202_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m701(tmp, &CompilerGeneratedAttribute__ctor_m701_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache U3CPrivateImplementationDetailsU3E_t1281__CustomAttributeCache = {
1,
NULL,
&U3CPrivateImplementationDetailsU3E_t1281_CustomAttributesCacheGenerator
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType U3CPrivateImplementationDetailsU3E_t1281_0_0_0;
extern Il2CppType U3CPrivateImplementationDetailsU3E_t1281_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t1281;
extern CustomAttributesCache U3CPrivateImplementationDetailsU3E_t1281__CustomAttributeCache;
TypeInfo U3CPrivateImplementationDetailsU3E_t1281_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t1281_MethodInfos/* methods */
	, NULL/* properties */
	, U3CPrivateImplementationDetailsU3E_t1281_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, U3CPrivateImplementationDetailsU3E_t1281_il2cpp_TypeInfo__nestedTypes/* nested_types */
	, NULL/* nested_in */
	, &U3CPrivateImplementationDetailsU3E_t1281_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, U3CPrivateImplementationDetailsU3E_t1281_VTable/* vtable */
	, &U3CPrivateImplementationDetailsU3E_t1281__CustomAttributeCache/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t1281_il2cpp_TypeInfo/* cast_class */
	, &U3CPrivateImplementationDetailsU3E_t1281_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t1281_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, U3CPrivateImplementationDetailsU3E_t1281_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1281)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t1281_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
