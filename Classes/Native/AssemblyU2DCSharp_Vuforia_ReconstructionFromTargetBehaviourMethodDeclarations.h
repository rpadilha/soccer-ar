﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionFromTargetBehaviour
struct ReconstructionFromTargetBehaviour_t47;

// System.Void Vuforia.ReconstructionFromTargetBehaviour::.ctor()
 void ReconstructionFromTargetBehaviour__ctor_m83 (ReconstructionFromTargetBehaviour_t47 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
