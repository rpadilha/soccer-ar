﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.String>
struct EqualityComparer_1_t3920;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.String>
struct EqualityComparer_1_t3920  : public Object_t
{
};
struct EqualityComparer_1_t3920_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.String>::_default
	EqualityComparer_1_t3920 * ____default_0;
};
