﻿#pragma once
#include <stdint.h>
// System.Reflection.EventInfo/AddEventAdapter
struct AddEventAdapter_t1979;
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
// System.Reflection.EventInfo
struct EventInfo_t1756  : public MemberInfo_t144
{
	// System.Reflection.EventInfo/AddEventAdapter System.Reflection.EventInfo::cached_add_event
	AddEventAdapter_t1979 * ___cached_add_event_0;
};
