﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundAbstractBehaviour>
struct UnityAction_1_t4558;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundAbstractBehaviour>
struct InvokableCall_1_t4557  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundAbstractBehaviour>::Delegate
	UnityAction_1_t4558 * ___Delegate_0;
};
