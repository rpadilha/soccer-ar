﻿#pragma once
#include <stdint.h>
// Vuforia.ObjectTracker
struct ObjectTracker_t605;
// Vuforia.MarkerTracker
struct MarkerTracker_t666;
// Vuforia.TextTracker
struct TextTracker_t722;
// Vuforia.SmartTerrainTracker
struct SmartTerrainTracker_t720;
// Vuforia.StateManager
struct StateManager_t768;
// Vuforia.TrackerManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManager.h"
// Vuforia.TrackerManagerImpl
struct TrackerManagerImpl_t788  : public TrackerManager_t787
{
	// Vuforia.ObjectTracker Vuforia.TrackerManagerImpl::mObjectTracker
	ObjectTracker_t605 * ___mObjectTracker_1;
	// Vuforia.MarkerTracker Vuforia.TrackerManagerImpl::mMarkerTracker
	MarkerTracker_t666 * ___mMarkerTracker_2;
	// Vuforia.TextTracker Vuforia.TrackerManagerImpl::mTextTracker
	TextTracker_t722 * ___mTextTracker_3;
	// Vuforia.SmartTerrainTracker Vuforia.TrackerManagerImpl::mSmartTerrainTracker
	SmartTerrainTracker_t720 * ___mSmartTerrainTracker_4;
	// Vuforia.StateManager Vuforia.TrackerManagerImpl::mStateManager
	StateManager_t768 * ___mStateManager_5;
};
