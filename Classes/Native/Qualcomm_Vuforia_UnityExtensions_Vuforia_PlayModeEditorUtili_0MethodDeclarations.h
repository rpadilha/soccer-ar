﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PlayModeEditorUtility
struct PlayModeEditorUtility_t679;
// Vuforia.IPlayModeEditorUtility
struct IPlayModeEditorUtility_t678;

// Vuforia.IPlayModeEditorUtility Vuforia.PlayModeEditorUtility::get_Instance()
 Object_t * PlayModeEditorUtility_get_Instance_m3137 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility::set_Instance(Vuforia.IPlayModeEditorUtility)
 void PlayModeEditorUtility_set_Instance_m3138 (Object_t * __this/* static, unused */, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility::.ctor()
 void PlayModeEditorUtility__ctor_m3139 (PlayModeEditorUtility_t679 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
