﻿#pragma once
#include <stdint.h>
// Vuforia.IUnityPlayer
struct IUnityPlayer_t139;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.UnityPlayer
struct UnityPlayer_t613  : public Object_t
{
};
struct UnityPlayer_t613_StaticFields{
	// Vuforia.IUnityPlayer Vuforia.UnityPlayer::sPlayer
	Object_t * ___sPlayer_0;
};
