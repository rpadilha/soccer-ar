﻿#pragma once
#include <stdint.h>
// System.Security.Policy.StrongName[]
struct StrongNameU5BU5D_t5310;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.Security.Policy.StrongName>
struct List_1_t2702  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Security.Policy.StrongName>::_items
	StrongNameU5BU5D_t5310* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::_version
	int32_t ____version_3;
};
struct List_1_t2702_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<System.Security.Policy.StrongName>::EmptyArray
	StrongNameU5BU5D_t5310* ___EmptyArray_4;
};
