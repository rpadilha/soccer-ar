﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct KeyValuePair_2_t4463;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t30;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m26824 (KeyValuePair_2_t4463 * __this, int32_t ___key, VirtualButtonAbstractBehaviour_t30 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Key()
 int32_t KeyValuePair_2_get_Key_m26825 (KeyValuePair_2_t4463 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m26826 (KeyValuePair_2_t4463 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Value()
 VirtualButtonAbstractBehaviour_t30 * KeyValuePair_2_get_Value_m26827 (KeyValuePair_2_t4463 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m26828 (KeyValuePair_2_t4463 * __this, VirtualButtonAbstractBehaviour_t30 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::ToString()
 String_t* KeyValuePair_2_ToString_m26829 (KeyValuePair_2_t4463 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
