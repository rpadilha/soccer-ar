﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Permissions.StrongNamePublicKeyBlob
struct StrongNamePublicKeyBlob_t2181;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Boolean System.Security.Permissions.StrongNamePublicKeyBlob::Equals(System.Object)
 bool StrongNamePublicKeyBlob_Equals_m12322 (StrongNamePublicKeyBlob_t2181 * __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.StrongNamePublicKeyBlob::GetHashCode()
 int32_t StrongNamePublicKeyBlob_GetHashCode_m12323 (StrongNamePublicKeyBlob_t2181 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.StrongNamePublicKeyBlob::ToString()
 String_t* StrongNamePublicKeyBlob_ToString_m12324 (StrongNamePublicKeyBlob_t2181 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
