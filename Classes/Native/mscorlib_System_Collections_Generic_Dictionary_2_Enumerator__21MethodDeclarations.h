﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>
struct Enumerator_t4412;
// System.Object
struct Object_t;
// Vuforia.ImageTarget
struct ImageTarget_t616;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.ImageTarget>
struct Dictionary_2_t782;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void Enumerator__ctor_m26427 (Enumerator_t4412 * __this, Dictionary_2_t782 * ___dictionary, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26428 (Enumerator_t4412 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::System.Collections.IDictionaryEnumerator.get_Entry()
 DictionaryEntry_t1355  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26429 (Enumerator_t4412 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::System.Collections.IDictionaryEnumerator.get_Key()
 Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26430 (Enumerator_t4412 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::System.Collections.IDictionaryEnumerator.get_Value()
 Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26431 (Enumerator_t4412 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::MoveNext()
 bool Enumerator_MoveNext_m26432 (Enumerator_t4412 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::get_Current()
 KeyValuePair_2_t4410  Enumerator_get_Current_m26433 (Enumerator_t4412 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::get_CurrentKey()
 int32_t Enumerator_get_CurrentKey_m26434 (Enumerator_t4412 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::get_CurrentValue()
 Object_t * Enumerator_get_CurrentValue_m26435 (Enumerator_t4412 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::VerifyState()
 void Enumerator_VerifyState_m26436 (Enumerator_t4412 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::VerifyCurrent()
 void Enumerator_VerifyCurrent_m26437 (Enumerator_t4412 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.ImageTarget>::Dispose()
 void Enumerator_Dispose_m26438 (Enumerator_t4412 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
