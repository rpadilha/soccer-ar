﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>
struct KeyValuePair_2_t4049;
// Vuforia.Marker
struct Marker_t667;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m22794 (KeyValuePair_2_t4049 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::get_Key()
 int32_t KeyValuePair_2_get_Key_m22795 (KeyValuePair_2_t4049 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m22796 (KeyValuePair_2_t4049 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::get_Value()
 Object_t * KeyValuePair_2_get_Value_m22797 (KeyValuePair_2_t4049 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m22798 (KeyValuePair_2_t4049 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::ToString()
 String_t* KeyValuePair_2_ToString_m22799 (KeyValuePair_2_t4049 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
