﻿#pragma once
#include <stdint.h>
// Vuforia.WebCamAbstractBehaviour
struct WebCamAbstractBehaviour_t63;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Vuforia.WebCamAbstractBehaviour>
struct CastHelper_1_t4516 
{
	// T UnityEngine.CastHelper`1<Vuforia.WebCamAbstractBehaviour>::t
	WebCamAbstractBehaviour_t63 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Vuforia.WebCamAbstractBehaviour>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
