﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Check
struct Check_t1276;
// System.Object
struct Object_t;

// System.Void System.Linq.Check::Source(System.Object)
 void Check_Source_m6801 (Object_t * __this/* static, unused */, Object_t * ___source, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Check::SourceAndPredicate(System.Object,System.Object)
 void Check_SourceAndPredicate_m6802 (Object_t * __this/* static, unused */, Object_t * ___source, Object_t * ___predicate, MethodInfo* method) IL2CPP_METHOD_ATTR;
