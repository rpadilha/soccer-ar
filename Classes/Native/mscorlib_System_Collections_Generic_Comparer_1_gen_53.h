﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>
struct Comparer_1_t4912;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>
struct Comparer_1_t4912  : public Object_t
{
};
struct Comparer_1_t4912_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::_default
	Comparer_1_t4912 * ____default_0;
};
