﻿#pragma once
#include <stdint.h>
// Vuforia.ITextRecoEventHandler[]
struct ITextRecoEventHandlerU5BU5D_t4522;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>
struct List_1_t808  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::_items
	ITextRecoEventHandlerU5BU5D_t4522* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t808_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::EmptyArray
	ITextRecoEventHandlerU5BU5D_t4522* ___EmptyArray_4;
};
