﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TextTrackerImpl
struct TextTrackerImpl_t725;
// Vuforia.WordList
struct WordList_t723;
// Vuforia.TextTrackerImpl/UpDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrackerImpl_UpD.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// Vuforia.RectangleIntData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleIntData.h"
// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"

// Vuforia.WordList Vuforia.TextTrackerImpl::get_WordList()
 WordList_t723 * TextTrackerImpl_get_WordList_m3238 (TextTrackerImpl_t725 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TextTrackerImpl::Start()
 bool TextTrackerImpl_Start_m3239 (TextTrackerImpl_t725 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TextTrackerImpl::Stop()
 void TextTrackerImpl_Stop_m3240 (TextTrackerImpl_t725 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TextTrackerImpl::SetRegionOfInterest(UnityEngine.Rect,UnityEngine.Rect)
 bool TextTrackerImpl_SetRegionOfInterest_m3241 (TextTrackerImpl_t725 * __this, Rect_t103  ___detectionRegion, Rect_t103  ___trackingRegion, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TextTrackerImpl::GetRegionOfInterest(UnityEngine.Rect&,UnityEngine.Rect&)
 bool TextTrackerImpl_GetRegionOfInterest_m3242 (TextTrackerImpl_t725 * __this, Rect_t103 * ___detectionRegion, Rect_t103 * ___trackingRegion, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Vuforia.TextTrackerImpl::ScreenSpaceRectFromCamSpaceRectData(Vuforia.RectangleIntData,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
 Rect_t103  TextTrackerImpl_ScreenSpaceRectFromCamSpaceRectData_m3243 (TextTrackerImpl_t725 * __this, RectangleIntData_t633  ___camSpaceRectData, Rect_t103  ___bgTextureViewPortRect, bool ___isTextureMirrored, VideoModeData_t602  ___videoModeData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextTrackerImpl/UpDirection Vuforia.TextTrackerImpl::get_CurrentUpDirection()
 int32_t TextTrackerImpl_get_CurrentUpDirection_m3244 (TextTrackerImpl_t725 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TextTrackerImpl::.ctor()
 void TextTrackerImpl__ctor_m3245 (TextTrackerImpl_t725 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
