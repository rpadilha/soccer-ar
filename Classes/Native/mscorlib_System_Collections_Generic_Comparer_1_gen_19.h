﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.RectTransform>
struct Comparer_1_t3762;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.RectTransform>
struct Comparer_1_t3762  : public Object_t
{
};
struct Comparer_1_t3762_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.RectTransform>::_default
	Comparer_1_t3762 * ____default_0;
};
