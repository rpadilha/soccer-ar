﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ColorBlock
struct ColorBlock_t331;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.UI.ColorBlock
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"

// UnityEngine.Color UnityEngine.UI.ColorBlock::get_normalColor()
 Color_t66  ColorBlock_get_normalColor_m1187 (ColorBlock_t331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_normalColor(UnityEngine.Color)
 void ColorBlock_set_normalColor_m1188 (ColorBlock_t331 * __this, Color_t66  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_highlightedColor()
 Color_t66  ColorBlock_get_highlightedColor_m1189 (ColorBlock_t331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_highlightedColor(UnityEngine.Color)
 void ColorBlock_set_highlightedColor_m1190 (ColorBlock_t331 * __this, Color_t66  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_pressedColor()
 Color_t66  ColorBlock_get_pressedColor_m1191 (ColorBlock_t331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_pressedColor(UnityEngine.Color)
 void ColorBlock_set_pressedColor_m1192 (ColorBlock_t331 * __this, Color_t66  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_disabledColor()
 Color_t66  ColorBlock_get_disabledColor_m1193 (ColorBlock_t331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_disabledColor(UnityEngine.Color)
 void ColorBlock_set_disabledColor_m1194 (ColorBlock_t331 * __this, Color_t66  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.ColorBlock::get_colorMultiplier()
 float ColorBlock_get_colorMultiplier_m1195 (ColorBlock_t331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_colorMultiplier(System.Single)
 void ColorBlock_set_colorMultiplier_m1196 (ColorBlock_t331 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.ColorBlock::get_fadeDuration()
 float ColorBlock_get_fadeDuration_m1197 (ColorBlock_t331 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_fadeDuration(System.Single)
 void ColorBlock_set_fadeDuration_m1198 (ColorBlock_t331 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::get_defaultColorBlock()
 ColorBlock_t331  ColorBlock_get_defaultColorBlock_m1199 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
