﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.DispIdAttribute
struct DispIdAttribute_t2025;

// System.Void System.Runtime.InteropServices.DispIdAttribute::.ctor(System.Int32)
 void DispIdAttribute__ctor_m11584 (DispIdAttribute_t2025 * __this, int32_t ___dispId, MethodInfo* method) IL2CPP_METHOD_ATTR;
