﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t17;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>
struct KeyValuePair_2_t4336 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::value
	TrackableBehaviour_t17 * ___value_1;
};
