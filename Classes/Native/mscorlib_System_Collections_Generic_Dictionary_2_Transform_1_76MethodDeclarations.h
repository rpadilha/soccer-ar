﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour,Vuforia.TrackableBehaviour>
struct Transform_1_t4342;
// System.Object
struct Object_t;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t17;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour,Vuforia.TrackableBehaviour>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m25758 (Transform_1_t4342 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour,Vuforia.TrackableBehaviour>::Invoke(TKey,TValue)
 TrackableBehaviour_t17 * Transform_1_Invoke_m25759 (Transform_1_t4342 * __this, int32_t ___key, TrackableBehaviour_t17 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour,Vuforia.TrackableBehaviour>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m25760 (Transform_1_t4342 * __this, int32_t ___key, TrackableBehaviour_t17 * ___value, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.TrackableBehaviour,Vuforia.TrackableBehaviour>::EndInvoke(System.IAsyncResult)
 TrackableBehaviour_t17 * Transform_1_EndInvoke_m25761 (Transform_1_t4342 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
