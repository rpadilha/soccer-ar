﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
struct Enumerator_t4567;
// System.Object
struct Object_t;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t4564;

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.HashSet`1<T>)
 void Enumerator__ctor_m27625_gshared (Enumerator_t4567 * __this, HashSet_1_t4564 * ___hashset, MethodInfo* method);
#define Enumerator__ctor_m27625(__this, ___hashset, method) (void)Enumerator__ctor_m27625_gshared((Enumerator_t4567 *)__this, (HashSet_1_t4564 *)___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m27626_gshared (Enumerator_t4567 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m27626(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m27626_gshared((Enumerator_t4567 *)__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
 bool Enumerator_MoveNext_m27627_gshared (Enumerator_t4567 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m27627(__this, method) (bool)Enumerator_MoveNext_m27627_gshared((Enumerator_t4567 *)__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
 Object_t * Enumerator_get_Current_m27628_gshared (Enumerator_t4567 * __this, MethodInfo* method);
#define Enumerator_get_Current_m27628(__this, method) (Object_t *)Enumerator_get_Current_m27628_gshared((Enumerator_t4567 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
 void Enumerator_Dispose_m27629_gshared (Enumerator_t4567 * __this, MethodInfo* method);
#define Enumerator_Dispose_m27629(__this, method) (void)Enumerator_Dispose_m27629_gshared((Enumerator_t4567 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::CheckState()
 void Enumerator_CheckState_m27630_gshared (Enumerator_t4567 * __this, MethodInfo* method);
#define Enumerator_CheckState_m27630(__this, method) (void)Enumerator_CheckState_m27630_gshared((Enumerator_t4567 *)__this, method)
