﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ArrayList/SimpleEnumerator
struct SimpleEnumerator_t1874;
// System.Object
struct Object_t;
// System.Collections.ArrayList
struct ArrayList_t1361;

// System.Void System.Collections.ArrayList/SimpleEnumerator::.ctor(System.Collections.ArrayList)
 void SimpleEnumerator__ctor_m10420 (SimpleEnumerator_t1874 * __this, ArrayList_t1361 * ___list, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/SimpleEnumerator::.cctor()
 void SimpleEnumerator__cctor_m10421 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/SimpleEnumerator::MoveNext()
 bool SimpleEnumerator_MoveNext_m10422 (SimpleEnumerator_t1874 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/SimpleEnumerator::get_Current()
 Object_t * SimpleEnumerator_get_Current_m10423 (SimpleEnumerator_t1874 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
