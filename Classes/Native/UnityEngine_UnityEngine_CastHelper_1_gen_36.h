﻿#pragma once
#include <stdint.h>
// UnityEngine.GUILayer
struct GUILayer_t991;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.GUILayer>
struct CastHelper_1_t4943 
{
	// T UnityEngine.CastHelper`1<UnityEngine.GUILayer>::t
	GUILayer_t991 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.GUILayer>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
