﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// Jump_Button[]
// Jump_Button[]
struct Jump_ButtonU5BU5D_t5771  : public Array_t
{
};
// CameraRelativeControl[]
// CameraRelativeControl[]
struct CameraRelativeControlU5BU5D_t5772  : public Array_t
{
};
// FirstPersonControl[]
// FirstPersonControl[]
struct FirstPersonControlU5BU5D_t5773  : public Array_t
{
};
// FollowTransform[]
// FollowTransform[]
struct FollowTransformU5BU5D_t5774  : public Array_t
{
};
// Joystick[]
// Joystick[]
struct JoystickU5BU5D_t214  : public Array_t
{
};
struct JoystickU5BU5D_t214_StaticFields{
};
// ObliqueNear[]
// ObliqueNear[]
struct ObliqueNearU5BU5D_t5775  : public Array_t
{
};
// PlayerRelativeControl[]
// PlayerRelativeControl[]
struct PlayerRelativeControlU5BU5D_t5776  : public Array_t
{
};
// RollABall[]
// RollABall[]
struct RollABallU5BU5D_t5777  : public Array_t
{
};
// ConstraintAxis[]
// ConstraintAxis[]
struct ConstraintAxisU5BU5D_t5778  : public Array_t
{
};
// RotationConstraint[]
// RotationConstraint[]
struct RotationConstraintU5BU5D_t5779  : public Array_t
{
};
// SidescrollControl[]
// SidescrollControl[]
struct SidescrollControlU5BU5D_t5780  : public Array_t
{
};
// SmoothFollow2D[]
// SmoothFollow2D[]
struct SmoothFollow2DU5BU5D_t5781  : public Array_t
{
};
// ControlState[]
// ControlState[]
struct ControlStateU5BU5D_t5782  : public Array_t
{
};
// TapControl[]
// TapControl[]
struct TapControlU5BU5D_t5783  : public Array_t
{
};
// ZoomCamera[]
// ZoomCamera[]
struct ZoomCameraU5BU5D_t5784  : public Array_t
{
};
