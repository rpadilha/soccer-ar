﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>
struct PrimeHelper_t4569;

// System.Void System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::.cctor()
 void PrimeHelper__cctor_m27631_gshared (Object_t * __this/* static, unused */, MethodInfo* method);
#define PrimeHelper__cctor_m27631(__this/* static, unused */, method) (void)PrimeHelper__cctor_m27631_gshared((Object_t *)__this/* static, unused */, method)
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::TestPrime(System.Int32)
 bool PrimeHelper_TestPrime_m27632_gshared (Object_t * __this/* static, unused */, int32_t ___x, MethodInfo* method);
#define PrimeHelper_TestPrime_m27632(__this/* static, unused */, ___x, method) (bool)PrimeHelper_TestPrime_m27632_gshared((Object_t *)__this/* static, unused */, (int32_t)___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::CalcPrime(System.Int32)
 int32_t PrimeHelper_CalcPrime_m27633_gshared (Object_t * __this/* static, unused */, int32_t ___x, MethodInfo* method);
#define PrimeHelper_CalcPrime_m27633(__this/* static, unused */, ___x, method) (int32_t)PrimeHelper_CalcPrime_m27633_gshared((Object_t *)__this/* static, unused */, (int32_t)___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::ToPrime(System.Int32)
 int32_t PrimeHelper_ToPrime_m27634_gshared (Object_t * __this/* static, unused */, int32_t ___x, MethodInfo* method);
#define PrimeHelper_ToPrime_m27634(__this/* static, unused */, ___x, method) (int32_t)PrimeHelper_ToPrime_m27634_gshared((Object_t *)__this/* static, unused */, (int32_t)___x, method)
