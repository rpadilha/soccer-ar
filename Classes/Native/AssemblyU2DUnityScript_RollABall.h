﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// RollABall
struct RollABall_t218  : public MonoBehaviour_t10
{
	// UnityEngine.Vector3 RollABall::tilt
	Vector3_t73  ___tilt_2;
	// System.Single RollABall::speed
	float ___speed_3;
	// System.Single RollABall::circ
	float ___circ_4;
	// UnityEngine.Vector3 RollABall::previousPosition
	Vector3_t73  ___previousPosition_5;
};
