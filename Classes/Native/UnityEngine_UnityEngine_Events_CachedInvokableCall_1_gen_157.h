﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_159.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>
struct CachedInvokableCall_1_t4785  : public InvokableCall_1_t4786
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
