﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.ObjectTargetAbstractBehaviour>
struct UnityAction_1_t3855;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetAbstractBehaviour>
struct InvokableCall_1_t3854  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetAbstractBehaviour>::Delegate
	UnityAction_1_t3855 * ___Delegate_0;
};
