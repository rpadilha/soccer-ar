﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct ObjectPool_1_t451;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct UnityAction_1_t452;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.CanvasListPool
struct CanvasListPool_t453  : public Object_t
{
};
struct CanvasListPool_t453_StaticFields{
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>> UnityEngine.UI.CanvasListPool::s_CanvasListPool
	ObjectPool_1_t451 * ___s_CanvasListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>> UnityEngine.UI.CanvasListPool::<>f__am$cache1
	UnityAction_1_t452 * ___U3CU3Ef__am$cache1_1;
};
