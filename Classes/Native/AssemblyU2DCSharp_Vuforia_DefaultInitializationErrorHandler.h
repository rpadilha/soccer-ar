﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Vuforia.DefaultInitializationErrorHandler
struct DefaultInitializationErrorHandler_t9  : public MonoBehaviour_t10
{
	// System.String Vuforia.DefaultInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_3;
	// System.Boolean Vuforia.DefaultInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_4;
};
