﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_2.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>
struct CachedInvokableCall_1_t2778  : public InvokableCall_1_t2779
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
