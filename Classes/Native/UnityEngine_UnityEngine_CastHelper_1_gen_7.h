﻿#pragma once
#include <stdint.h>
// Player_Script
struct Player_Script_t94;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Player_Script>
struct CastHelper_1_t3007 
{
	// T UnityEngine.CastHelper`1<Player_Script>::t
	Player_Script_t94 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Player_Script>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
