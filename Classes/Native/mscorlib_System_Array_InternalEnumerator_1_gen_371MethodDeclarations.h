﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.TargetFinder/InitState>
struct InternalEnumerator_1_t4392;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.TargetFinder/InitState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_InitSt.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/InitState>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m26185 (InternalEnumerator_1_t4392 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TargetFinder/InitState>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26186 (InternalEnumerator_1_t4392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/InitState>::Dispose()
 void InternalEnumerator_1_Dispose_m26187 (InternalEnumerator_1_t4392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TargetFinder/InitState>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m26188 (InternalEnumerator_1_t4392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.TargetFinder/InitState>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m26189 (InternalEnumerator_1_t4392 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
