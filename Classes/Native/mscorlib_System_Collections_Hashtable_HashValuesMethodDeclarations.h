﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Hashtable/HashValues
struct HashValues_t1887;
// System.Object
struct Object_t;
// System.Collections.Hashtable
struct Hashtable_t1348;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;

// System.Void System.Collections.Hashtable/HashValues::.ctor(System.Collections.Hashtable)
 void HashValues__ctor_m10552 (HashValues_t1887 * __this, Hashtable_t1348 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Hashtable/HashValues::get_Count()
 int32_t HashValues_get_Count_m10553 (HashValues_t1887 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Hashtable/HashValues::get_IsSynchronized()
 bool HashValues_get_IsSynchronized_m10554 (HashValues_t1887 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Hashtable/HashValues::get_SyncRoot()
 Object_t * HashValues_get_SyncRoot_m10555 (HashValues_t1887 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Hashtable/HashValues::CopyTo(System.Array,System.Int32)
 void HashValues_CopyTo_m10556 (HashValues_t1887 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Hashtable/HashValues::GetEnumerator()
 Object_t * HashValues_GetEnumerator_m10557 (HashValues_t1887 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
