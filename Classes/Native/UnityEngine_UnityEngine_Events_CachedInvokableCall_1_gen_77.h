﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.BaseRaycaster>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_74.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.BaseRaycaster>
struct CachedInvokableCall_1_t3375  : public InvokableCall_1_t3376
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.BaseRaycaster>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
