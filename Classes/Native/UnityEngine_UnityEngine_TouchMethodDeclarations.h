﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Touch
struct Touch_t201;
struct Touch_t201_marshaled;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"

// System.Int32 UnityEngine.Touch::get_fingerId()
 int32_t Touch_get_fingerId_m692 (Touch_t201 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
 Vector2_t99  Touch_get_position_m687 (Touch_t201 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
 Vector2_t99  Touch_get_deltaPosition_m849 (Touch_t201 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Touch::get_tapCount()
 int32_t Touch_get_tapCount_m696 (Touch_t201 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
 int32_t Touch_get_phase_m698 (Touch_t201 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void Touch_t201_marshal(const Touch_t201& unmarshaled, Touch_t201_marshaled& marshaled);
void Touch_t201_marshal_back(const Touch_t201_marshaled& marshaled, Touch_t201& unmarshaled);
void Touch_t201_marshal_cleanup(Touch_t201_marshaled& marshaled);
