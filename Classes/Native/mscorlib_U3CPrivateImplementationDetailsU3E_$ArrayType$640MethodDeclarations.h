﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$640
struct $ArrayType$640_t2332;
struct $ArrayType$640_t2332_marshaled;

void $ArrayType$640_t2332_marshal(const $ArrayType$640_t2332& unmarshaled, $ArrayType$640_t2332_marshaled& marshaled);
void $ArrayType$640_t2332_marshal_back(const $ArrayType$640_t2332_marshaled& marshaled, $ArrayType$640_t2332& unmarshaled);
void $ArrayType$640_t2332_marshal_cleanup($ArrayType$640_t2332_marshaled& marshaled);
