﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>
struct Comparer_1_t3257;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t295;

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::.ctor()
// System.Collections.Generic.Comparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_genMethodDeclarations.h"
#define Comparer_1__ctor_m16868(__this, method) (void)Comparer_1__ctor_m14672_gshared((Comparer_1_t2848 *)__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::.cctor()
#define Comparer_1__cctor_m16869(__this/* static, unused */, method) (void)Comparer_1__cctor_m14673_gshared((Object_t *)__this/* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IComparer.Compare(System.Object,System.Object)
#define Comparer_1_System_Collections_IComparer_Compare_m16870(__this, ___x, ___y, method) (int32_t)Comparer_1_System_Collections_IComparer_Compare_m14674_gshared((Comparer_1_t2848 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::get_Default()
#define Comparer_1_get_Default_m16871(__this/* static, unused */, method) (Comparer_1_t3257 *)Comparer_1_get_Default_m14675_gshared((Object_t *)__this/* static, unused */, method)
