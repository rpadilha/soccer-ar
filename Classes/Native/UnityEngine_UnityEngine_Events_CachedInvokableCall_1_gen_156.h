﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_158.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>
struct CachedInvokableCall_1_t4779  : public InvokableCall_1_t4780
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
