﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
struct List_1_t1008;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t1007;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
struct Enumerator_t1170 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::l
	List_1_t1008 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::current
	GUILayoutEntry_t1007 * ___current_3;
};
