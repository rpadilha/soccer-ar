﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.MRUList/Node
struct Node_t1470;
// System.Object
struct Object_t;

// System.Void System.Text.RegularExpressions.MRUList/Node::.ctor(System.Object)
 void Node__ctor_m7414 (Node_t1470 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
