﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_133.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundAbstractBehaviour>
struct CachedInvokableCall_1_t4556  : public InvokableCall_1_t4557
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
