﻿#pragma once
#include <stdint.h>
// System.Reflection.Assembly
struct Assembly_t1556;
// System.Object
struct Object_t;
// System.ResolveEventArgs
struct ResolveEventArgs_t2297;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.ResolveEventHandler
struct ResolveEventHandler_t2237  : public MulticastDelegate_t373
{
};
