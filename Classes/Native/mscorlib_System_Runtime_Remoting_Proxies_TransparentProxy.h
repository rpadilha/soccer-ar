﻿#pragma once
#include <stdint.h>
// System.Runtime.Remoting.Proxies.RealProxy
struct RealProxy_t2083;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Proxies.TransparentProxy
struct TransparentProxy_t2085  : public Object_t
{
	// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.Proxies.TransparentProxy::_rp
	RealProxy_t2083 * ____rp_0;
};
