﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.EncoderReplacementFallback
struct EncoderReplacementFallback_t2206;
// System.String
struct String_t;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t2194;
// System.Object
struct Object_t;

// System.Void System.Text.EncoderReplacementFallback::.ctor()
 void EncoderReplacementFallback__ctor_m12452 (EncoderReplacementFallback_t2206 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.EncoderReplacementFallback::.ctor(System.String)
 void EncoderReplacementFallback__ctor_m12453 (EncoderReplacementFallback_t2206 * __this, String_t* ___replacement, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.EncoderReplacementFallback::get_DefaultString()
 String_t* EncoderReplacementFallback_get_DefaultString_m12454 (EncoderReplacementFallback_t2206 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallbackBuffer System.Text.EncoderReplacementFallback::CreateFallbackBuffer()
 EncoderFallbackBuffer_t2194 * EncoderReplacementFallback_CreateFallbackBuffer_m12455 (EncoderReplacementFallback_t2206 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.EncoderReplacementFallback::Equals(System.Object)
 bool EncoderReplacementFallback_Equals_m12456 (EncoderReplacementFallback_t2206 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.EncoderReplacementFallback::GetHashCode()
 int32_t EncoderReplacementFallback_GetHashCode_m12457 (EncoderReplacementFallback_t2206 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
