﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.WordResult>
struct Comparer_1_t4157;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.WordResult>
struct Comparer_1_t4157  : public Object_t
{
};
struct Comparer_1_t4157_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.WordResult>::_default
	Comparer_1_t4157 * ____default_0;
};
