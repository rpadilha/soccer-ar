﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Type>
struct Enumerator_t937;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t804;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26MethodDeclarations.h"
#define Enumerator__ctor_m21342(__this, ___l, method) (void)Enumerator__ctor_m14558_gshared((Enumerator_t2837 *)__this, (List_1_t149 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21343(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14559_gshared((Enumerator_t2837 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m21344(__this, method) (void)Enumerator_Dispose_m14560_gshared((Enumerator_t2837 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Type>::VerifyState()
#define Enumerator_VerifyState_m21345(__this, method) (void)Enumerator_VerifyState_m14561_gshared((Enumerator_t2837 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m5518(__this, method) (bool)Enumerator_MoveNext_m14562_gshared((Enumerator_t2837 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m5517(__this, method) (Type_t *)Enumerator_get_Current_m14563_gshared((Enumerator_t2837 *)__this, method)
