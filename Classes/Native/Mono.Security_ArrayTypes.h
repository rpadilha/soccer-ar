﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct BigIntegerU5BU5D_t1593  : public Array_t
{
};
struct BigIntegerU5BU5D_t1593_StaticFields{
};
// Mono.Math.BigInteger/Sign[]
// Mono.Math.BigInteger/Sign[]
struct SignU5BU5D_t5914  : public Array_t
{
};
// Mono.Math.Prime.ConfidenceFactor[]
// Mono.Math.Prime.ConfidenceFactor[]
struct ConfidenceFactorU5BU5D_t5915  : public Array_t
{
};
// Mono.Security.X509.X509ChainStatusFlags[]
// Mono.Security.X509.X509ChainStatusFlags[]
struct X509ChainStatusFlagsU5BU5D_t5916  : public Array_t
{
};
// Mono.Security.X509.Extensions.KeyUsages[]
// Mono.Security.X509.Extensions.KeyUsages[]
struct KeyUsagesU5BU5D_t5917  : public Array_t
{
};
// Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes[]
// Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes[]
struct CertTypesU5BU5D_t5918  : public Array_t
{
};
// Mono.Security.Protocol.Tls.AlertLevel[]
// Mono.Security.Protocol.Tls.AlertLevel[]
struct AlertLevelU5BU5D_t5919  : public Array_t
{
};
// Mono.Security.Protocol.Tls.AlertDescription[]
// Mono.Security.Protocol.Tls.AlertDescription[]
struct AlertDescriptionU5BU5D_t5920  : public Array_t
{
};
// Mono.Security.Protocol.Tls.CipherAlgorithmType[]
// Mono.Security.Protocol.Tls.CipherAlgorithmType[]
struct CipherAlgorithmTypeU5BU5D_t5921  : public Array_t
{
};
// Mono.Security.Protocol.Tls.ContentType[]
// Mono.Security.Protocol.Tls.ContentType[]
struct ContentTypeU5BU5D_t5922  : public Array_t
{
};
// Mono.Security.Protocol.Tls.ExchangeAlgorithmType[]
// Mono.Security.Protocol.Tls.ExchangeAlgorithmType[]
struct ExchangeAlgorithmTypeU5BU5D_t5923  : public Array_t
{
};
// Mono.Security.Protocol.Tls.HandshakeState[]
// Mono.Security.Protocol.Tls.HandshakeState[]
struct HandshakeStateU5BU5D_t5924  : public Array_t
{
};
// Mono.Security.Protocol.Tls.HashAlgorithmType[]
// Mono.Security.Protocol.Tls.HashAlgorithmType[]
struct HashAlgorithmTypeU5BU5D_t5925  : public Array_t
{
};
// Mono.Security.Protocol.Tls.SecurityCompressionType[]
// Mono.Security.Protocol.Tls.SecurityCompressionType[]
struct SecurityCompressionTypeU5BU5D_t5926  : public Array_t
{
};
// Mono.Security.Protocol.Tls.SecurityProtocolType[]
// Mono.Security.Protocol.Tls.SecurityProtocolType[]
struct SecurityProtocolTypeU5BU5D_t5927  : public Array_t
{
};
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[]
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[]
struct ClientCertificateTypeU5BU5D_t1686  : public Array_t
{
};
// Mono.Security.Protocol.Tls.Handshake.HandshakeType[]
// Mono.Security.Protocol.Tls.Handshake.HandshakeType[]
struct HandshakeTypeU5BU5D_t5928  : public Array_t
{
};
