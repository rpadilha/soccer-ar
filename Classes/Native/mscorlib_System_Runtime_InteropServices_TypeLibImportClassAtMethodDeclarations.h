﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.TypeLibImportClassAttribute
struct TypeLibImportClassAttribute_t2030;
// System.Type
struct Type_t;

// System.Void System.Runtime.InteropServices.TypeLibImportClassAttribute::.ctor(System.Type)
 void TypeLibImportClassAttribute__ctor_m11613 (TypeLibImportClassAttribute_t2030 * __this, Type_t * ___importClass, MethodInfo* method) IL2CPP_METHOD_ATTR;
