﻿#pragma once
#include <stdint.h>
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t540  : public PropertyAttribute_t1114
{
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;
};
