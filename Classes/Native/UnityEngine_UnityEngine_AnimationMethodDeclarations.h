﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Animation
struct Animation_t181;
// UnityEngine.AnimationState
struct AnimationState_t186;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// UnityEngine.PlayMode
#include "UnityEngine_UnityEngine_PlayMode.h"
// UnityEngine.QueueMode
#include "UnityEngine_UnityEngine_QueueMode.h"

// System.Void UnityEngine.Animation::Stop()
 void Animation_Stop_m704 (Animation_t181 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)
 void Animation_INTERNAL_CALL_Stop_m6317 (Object_t * __this/* static, unused */, Animation_t181 * ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::IsPlaying(System.String)
 bool Animation_IsPlaying_m616 (Animation_t181 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::get_Item(System.String)
 AnimationState_t186 * Animation_get_Item_m610 (Animation_t181 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)
 bool Animation_Play_m6318 (Animation_t181 * __this, String_t* ___animation, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animation::Play(System.String)
 bool Animation_Play_m586 (Animation_t181 * __this, String_t* ___animation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::PlayQueued(System.String,UnityEngine.QueueMode,UnityEngine.PlayMode)
 AnimationState_t186 * Animation_PlayQueued_m6319 (Animation_t181 * __this, String_t* ___animation, int32_t ___queue, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::PlayQueued(System.String)
 AnimationState_t186 * Animation_PlayQueued_m626 (Animation_t181 * __this, String_t* ___animation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.Animation::GetEnumerator()
 Object_t * Animation_GetEnumerator_m6320 (Animation_t181 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::GetState(System.String)
 AnimationState_t186 * Animation_GetState_m6321 (Animation_t181 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.Animation::GetStateAtIndex(System.Int32)
 AnimationState_t186 * Animation_GetStateAtIndex_m6322 (Animation_t181 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animation::GetStateCount()
 int32_t Animation_GetStateCount_m6323 (Animation_t181 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
