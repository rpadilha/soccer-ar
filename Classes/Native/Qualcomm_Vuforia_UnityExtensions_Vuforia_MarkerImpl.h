﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableImpl.h"
// Vuforia.MarkerImpl
struct MarkerImpl_t665  : public TrackableImpl_t609
{
	// System.Single Vuforia.MarkerImpl::mSize
	float ___mSize_2;
	// System.Int32 Vuforia.MarkerImpl::<MarkerID>k__BackingField
	int32_t ___U3CMarkerIDU3Ek__BackingField_3;
};
