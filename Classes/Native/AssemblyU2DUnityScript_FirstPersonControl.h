﻿#pragma once
#include <stdint.h>
// Joystick
struct Joystick_t208;
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.CharacterController
struct CharacterController_t209;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// FirstPersonControl
struct FirstPersonControl_t211  : public MonoBehaviour_t10
{
	// Joystick FirstPersonControl::moveTouchPad
	Joystick_t208 * ___moveTouchPad_2;
	// Joystick FirstPersonControl::rotateTouchPad
	Joystick_t208 * ___rotateTouchPad_3;
	// UnityEngine.Transform FirstPersonControl::cameraPivot
	Transform_t74 * ___cameraPivot_4;
	// System.Single FirstPersonControl::forwardSpeed
	float ___forwardSpeed_5;
	// System.Single FirstPersonControl::backwardSpeed
	float ___backwardSpeed_6;
	// System.Single FirstPersonControl::sidestepSpeed
	float ___sidestepSpeed_7;
	// System.Single FirstPersonControl::jumpSpeed
	float ___jumpSpeed_8;
	// System.Single FirstPersonControl::inAirMultiplier
	float ___inAirMultiplier_9;
	// UnityEngine.Vector2 FirstPersonControl::rotationSpeed
	Vector2_t99  ___rotationSpeed_10;
	// System.Single FirstPersonControl::tiltPositiveYAxis
	float ___tiltPositiveYAxis_11;
	// System.Single FirstPersonControl::tiltNegativeYAxis
	float ___tiltNegativeYAxis_12;
	// System.Single FirstPersonControl::tiltXAxisMinimum
	float ___tiltXAxisMinimum_13;
	// UnityEngine.Transform FirstPersonControl::thisTransform
	Transform_t74 * ___thisTransform_14;
	// UnityEngine.CharacterController FirstPersonControl::character
	CharacterController_t209 * ___character_15;
	// UnityEngine.Vector3 FirstPersonControl::cameraVelocity
	Vector3_t73  ___cameraVelocity_16;
	// UnityEngine.Vector3 FirstPersonControl::velocity
	Vector3_t73  ___velocity_17;
	// System.Boolean FirstPersonControl::canJump
	bool ___canJump_18;
};
