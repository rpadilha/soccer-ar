﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.TrackableBehaviour>
struct EqualityComparer_1_t4232;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.TrackableBehaviour>
struct EqualityComparer_1_t4232  : public Object_t
{
};
struct EqualityComparer_1_t4232_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.TrackableBehaviour>::_default
	EqualityComparer_1_t4232 * ____default_0;
};
