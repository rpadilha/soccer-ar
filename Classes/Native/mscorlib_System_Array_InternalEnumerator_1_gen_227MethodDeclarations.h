﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UIVertex>
struct InternalEnumerator_1_t3516;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m18762 (InternalEnumerator_1_t3516 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18763 (InternalEnumerator_1_t3516 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::Dispose()
 void InternalEnumerator_1_Dispose_m18764 (InternalEnumerator_1_t3516 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m18765 (InternalEnumerator_1_t3516 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::get_Current()
 UIVertex_t362  InternalEnumerator_1_get_Current_m18766 (InternalEnumerator_1_t3516 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
