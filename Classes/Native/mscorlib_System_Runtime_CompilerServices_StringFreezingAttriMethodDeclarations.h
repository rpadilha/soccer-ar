﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.StringFreezingAttribute
struct StringFreezingAttribute_t2013;

// System.Void System.Runtime.CompilerServices.StringFreezingAttribute::.ctor()
 void StringFreezingAttribute__ctor_m11578 (StringFreezingAttribute_t2013 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
