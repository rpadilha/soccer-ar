﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// UnityEngine.EventSystems.IPointerUpHandler
struct IPointerUpHandler_t276;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t235;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_t255  : public MulticastDelegate_t373
{
};
