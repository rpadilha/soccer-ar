﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t951;
// UnityEngine.AssetBundle
struct AssetBundle_t953;

// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
 void AssetBundleCreateRequest__ctor_m5570 (AssetBundleCreateRequest_t951 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
 AssetBundle_t953 * AssetBundleCreateRequest_get_assetBundle_m5571 (AssetBundleCreateRequest_t951 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
 void AssetBundleCreateRequest_DisableCompatibilityChecks_m5572 (AssetBundleCreateRequest_t951 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
