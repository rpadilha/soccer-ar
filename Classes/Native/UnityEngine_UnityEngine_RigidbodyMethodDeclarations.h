﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Rigidbody
struct Rigidbody_t180;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.ForceMode
#include "UnityEngine_UnityEngine_ForceMode.h"

// System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
 void Rigidbody_INTERNAL_get_velocity_m6256 (Rigidbody_t180 * __this, Vector3_t73 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
 void Rigidbody_INTERNAL_set_velocity_m6257 (Rigidbody_t180 * __this, Vector3_t73 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
 Vector3_t73  Rigidbody_get_velocity_m580 (Rigidbody_t180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
 void Rigidbody_set_velocity_m714 (Rigidbody_t180 * __this, Vector3_t73  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_set_angularVelocity(UnityEngine.Vector3&)
 void Rigidbody_INTERNAL_set_angularVelocity_m6258 (Rigidbody_t180 * __this, Vector3_t73 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::set_angularVelocity(UnityEngine.Vector3)
 void Rigidbody_set_angularVelocity_m727 (Rigidbody_t180 * __this, Vector3_t73  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::set_drag(System.Single)
 void Rigidbody_set_drag_m660 (Rigidbody_t180 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rigidbody::get_isKinematic()
 bool Rigidbody_get_isKinematic_m584 (Rigidbody_t180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
 void Rigidbody_set_isKinematic_m618 (Rigidbody_t180 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
 void Rigidbody_AddForce_m6259 (Rigidbody_t180 * __this, Vector3_t73  ___force, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
 void Rigidbody_AddForce_m619 (Rigidbody_t180 * __this, Vector3_t73  ___force, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
 void Rigidbody_INTERNAL_CALL_AddForce_m6260 (Object_t * __this/* static, unused */, Rigidbody_t180 * ___self, Vector3_t73 * ___force, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::AddForce(System.Single,System.Single,System.Single)
 void Rigidbody_AddForce_m710 (Rigidbody_t180 * __this, float ___x, float ___y, float ___z, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::AddForce(System.Single,System.Single,System.Single,UnityEngine.ForceMode)
 void Rigidbody_AddForce_m6261 (Rigidbody_t180 * __this, float ___x, float ___y, float ___z, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_set_rotation(UnityEngine.Quaternion&)
 void Rigidbody_INTERNAL_set_rotation_m6262 (Rigidbody_t180 * __this, Quaternion_t108 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::set_rotation(UnityEngine.Quaternion)
 void Rigidbody_set_rotation_m718 (Rigidbody_t180 * __this, Quaternion_t108  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
