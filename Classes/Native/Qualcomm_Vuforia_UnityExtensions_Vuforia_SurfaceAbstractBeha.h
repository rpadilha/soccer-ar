﻿#pragma once
#include <stdint.h>
// Vuforia.Surface
struct Surface_t16;
// Vuforia.SmartTerrainTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab.h"
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t51  : public SmartTerrainTrackableBehaviour_t627
{
	// Vuforia.Surface Vuforia.SurfaceAbstractBehaviour::mSurface
	Object_t * ___mSurface_13;
};
