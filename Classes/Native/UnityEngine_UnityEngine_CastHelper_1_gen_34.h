﻿#pragma once
#include <stdint.h>
// Vuforia.QCARAbstractBehaviour
struct QCARAbstractBehaviour_t45;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Vuforia.QCARAbstractBehaviour>
struct CastHelper_1_t4574 
{
	// T UnityEngine.CastHelper`1<Vuforia.QCARAbstractBehaviour>::t
	QCARAbstractBehaviour_t45 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Vuforia.QCARAbstractBehaviour>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
