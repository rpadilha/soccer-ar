﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.Word>
struct List_1_t740;
// Vuforia.Word
struct Word_t736;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Word>
struct Enumerator_t875 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.Word>::l
	List_1_t740 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.Word>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.Word>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.Word>::current
	Object_t * ___current_3;
};
