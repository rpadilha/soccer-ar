﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ImageTargetBuilderImpl
struct ImageTargetBuilderImpl_t656;
// System.String
struct String_t;
// Vuforia.TrackableSource
struct TrackableSource_t630;
// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"

// System.Boolean Vuforia.ImageTargetBuilderImpl::Build(System.String,System.Single)
 bool ImageTargetBuilderImpl_Build_m3072 (ImageTargetBuilderImpl_t656 * __this, String_t* ___targetName, float ___sceenSizeWidth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetBuilderImpl::StartScan()
 void ImageTargetBuilderImpl_StartScan_m3073 (ImageTargetBuilderImpl_t656 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetBuilderImpl::StopScan()
 void ImageTargetBuilderImpl_StopScan_m3074 (ImageTargetBuilderImpl_t656 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.ImageTargetBuilderImpl::GetFrameQuality()
 int32_t ImageTargetBuilderImpl_GetFrameQuality_m3075 (ImageTargetBuilderImpl_t656 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TrackableSource Vuforia.ImageTargetBuilderImpl::GetTrackableSource()
 TrackableSource_t630 * ImageTargetBuilderImpl_GetTrackableSource_m3076 (ImageTargetBuilderImpl_t656 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetBuilderImpl::.ctor()
 void ImageTargetBuilderImpl__ctor_m3077 (ImageTargetBuilderImpl_t656 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
