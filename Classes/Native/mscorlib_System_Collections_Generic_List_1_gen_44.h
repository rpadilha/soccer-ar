﻿#pragma once
#include <stdint.h>
// Vuforia.IVideoBackgroundEventHandler[]
struct IVideoBackgroundEventHandlerU5BU5D_t4502;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>
struct List_1_t801  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::_items
	IVideoBackgroundEventHandlerU5BU5D_t4502* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t801_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::EmptyArray
	IVideoBackgroundEventHandlerU5BU5D_t4502* ___EmptyArray_4;
};
