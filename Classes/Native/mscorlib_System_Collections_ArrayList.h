﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ArrayList
struct ArrayList_t1361  : public Object_t
{
	// System.Int32 System.Collections.ArrayList::_size
	int32_t ____size_1;
	// System.Object[] System.Collections.ArrayList::_items
	ObjectU5BU5D_t130* ____items_2;
	// System.Int32 System.Collections.ArrayList::_version
	int32_t ____version_3;
};
struct ArrayList_t1361_StaticFields{
	// System.Int32 System.Collections.ArrayList::DefaultInitialCapacity
	int32_t ___DefaultInitialCapacity_0;
	// System.Object[] System.Collections.ArrayList::EmptyArray
	ObjectU5BU5D_t130* ___EmptyArray_4;
};
