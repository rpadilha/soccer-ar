﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.WordAbstractBehaviour>
struct EqualityComparer_1_t4181;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.WordAbstractBehaviour>
struct EqualityComparer_1_t4181  : public Object_t
{
};
struct EqualityComparer_1_t4181_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.WordAbstractBehaviour>::_default
	EqualityComparer_1_t4181 * ____default_0;
};
