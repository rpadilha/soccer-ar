﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.MethodCallDictionary
struct MethodCallDictionary_t2070;
// System.Runtime.Remoting.Messaging.IMethodMessage
struct IMethodMessage_t2071;

// System.Void System.Runtime.Remoting.Messaging.MethodCallDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodMessage)
 void MethodCallDictionary__ctor_m11732 (MethodCallDictionary_t2070 * __this, Object_t * ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodCallDictionary::.cctor()
 void MethodCallDictionary__cctor_m11733 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
