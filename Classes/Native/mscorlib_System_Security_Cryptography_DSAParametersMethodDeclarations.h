﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DSAParameters
struct DSAParameters_t1562;
struct DSAParameters_t1562_marshaled;

void DSAParameters_t1562_marshal(const DSAParameters_t1562& unmarshaled, DSAParameters_t1562_marshaled& marshaled);
void DSAParameters_t1562_marshal_back(const DSAParameters_t1562_marshaled& marshaled, DSAParameters_t1562& unmarshaled);
void DSAParameters_t1562_marshal_cleanup(DSAParameters_t1562_marshaled& marshaled);
