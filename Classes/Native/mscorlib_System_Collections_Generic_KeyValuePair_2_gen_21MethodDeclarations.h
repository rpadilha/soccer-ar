﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>
struct KeyValuePair_2_t4410;
// Vuforia.ImageTarget
struct ImageTarget_t616;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m26382 (KeyValuePair_2_t4410 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::get_Key()
 int32_t KeyValuePair_2_get_Key_m26383 (KeyValuePair_2_t4410 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m26384 (KeyValuePair_2_t4410 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::get_Value()
 Object_t * KeyValuePair_2_get_Value_m26385 (KeyValuePair_2_t4410 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m26386 (KeyValuePair_2_t4410 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::ToString()
 String_t* KeyValuePair_2_ToString_m26387 (KeyValuePair_2_t4410 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
