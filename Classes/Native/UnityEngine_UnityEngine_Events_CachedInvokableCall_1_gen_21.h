﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_17.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>
struct CachedInvokableCall_1_t2911  : public InvokableCall_1_t2912
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
