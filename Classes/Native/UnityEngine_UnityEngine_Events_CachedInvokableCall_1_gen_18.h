﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_14.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.MultiTargetBehaviour>
struct CachedInvokableCall_1_t2894  : public InvokableCall_1_t2895
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.MultiTargetBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
