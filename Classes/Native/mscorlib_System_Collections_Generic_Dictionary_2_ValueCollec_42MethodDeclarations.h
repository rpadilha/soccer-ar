﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Type,System.UInt16>
struct Enumerator_t4126;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t726;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Type,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void Enumerator__ctor_m23606 (Enumerator_t4126 * __this, Dictionary_2_t726 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Type,System.UInt16>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23607 (Enumerator_t4126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Type,System.UInt16>::Dispose()
 void Enumerator_Dispose_m23608 (Enumerator_t4126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Type,System.UInt16>::MoveNext()
 bool Enumerator_MoveNext_m23609 (Enumerator_t4126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Type,System.UInt16>::get_Current()
 uint16_t Enumerator_get_Current_m23610 (Enumerator_t4126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
