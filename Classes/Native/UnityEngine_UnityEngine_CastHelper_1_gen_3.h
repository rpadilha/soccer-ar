﻿#pragma once
#include <stdint.h>
// UnityEngine.MeshRenderer
struct MeshRenderer_t167;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.MeshRenderer>
struct CastHelper_1_t2948 
{
	// T UnityEngine.CastHelper`1<UnityEngine.MeshRenderer>::t
	MeshRenderer_t167 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.MeshRenderer>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
