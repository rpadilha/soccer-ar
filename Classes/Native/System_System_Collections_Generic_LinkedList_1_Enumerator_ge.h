﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t701;
// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t859;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>
struct Enumerator_t4073 
{
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::list
	LinkedList_1_t701 * ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::current
	LinkedListNode_1_t859 * ___current_1;
	// System.Int32 System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::index
	int32_t ___index_2;
	// System.UInt32 System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::version
	uint32_t ___version_3;
};
