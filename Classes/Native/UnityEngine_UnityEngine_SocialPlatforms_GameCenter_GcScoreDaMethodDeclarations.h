﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
struct GcScoreData_t976;
struct GcScoreData_t976_marshaled;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t1097;

// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
 Score_t1097 * GcScoreData_ToScore_m6424 (GcScoreData_t976 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void GcScoreData_t976_marshal(const GcScoreData_t976& unmarshaled, GcScoreData_t976_marshaled& marshaled);
void GcScoreData_t976_marshal_back(const GcScoreData_t976_marshaled& marshaled, GcScoreData_t976& unmarshaled);
void GcScoreData_t976_marshal_cleanup(GcScoreData_t976_marshaled& marshaled);
