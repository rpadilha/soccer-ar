﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.SimpleCollator/PreviousInfo
struct PreviousInfo_t1795;
struct PreviousInfo_t1795_marshaled;

// System.Void Mono.Globalization.Unicode.SimpleCollator/PreviousInfo::.ctor(System.Boolean)
 void PreviousInfo__ctor_m9971 (PreviousInfo_t1795 * __this, bool ___dummy, MethodInfo* method) IL2CPP_METHOD_ATTR;
void PreviousInfo_t1795_marshal(const PreviousInfo_t1795& unmarshaled, PreviousInfo_t1795_marshaled& marshaled);
void PreviousInfo_t1795_marshal_back(const PreviousInfo_t1795_marshaled& marshaled, PreviousInfo_t1795& unmarshaled);
void PreviousInfo_t1795_marshal_cleanup(PreviousInfo_t1795_marshaled& marshaled);
