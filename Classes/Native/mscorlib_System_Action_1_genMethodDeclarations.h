﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<Vuforia.QCARUnity/InitError>
struct Action_1_t117;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::.ctor(System.Object,System.IntPtr)
 void Action_1__ctor_m258 (Action_1_t117 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::Invoke(T)
 void Action_1_Invoke_m5486 (Action_1_t117 * __this, int32_t ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Action`1<Vuforia.QCARUnity/InitError>::BeginInvoke(T,System.AsyncCallback,System.Object)
 Object_t * Action_1_BeginInvoke_m14330 (Action_1_t117 * __this, int32_t ___obj, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::EndInvoke(System.IAsyncResult)
 void Action_1_EndInvoke_m14331 (Action_1_t117 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
