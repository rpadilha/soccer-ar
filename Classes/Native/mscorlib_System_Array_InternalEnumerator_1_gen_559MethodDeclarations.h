﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.UriHostNameType>
struct InternalEnumerator_1_t5098;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.UriHostNameType
#include "System_System_UriHostNameType.h"

// System.Void System.Array/InternalEnumerator`1<System.UriHostNameType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30864 (InternalEnumerator_1_t5098 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.UriHostNameType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30865 (InternalEnumerator_1_t5098 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.UriHostNameType>::Dispose()
 void InternalEnumerator_1_Dispose_m30866 (InternalEnumerator_1_t5098 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.UriHostNameType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30867 (InternalEnumerator_1_t5098 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.UriHostNameType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30868 (InternalEnumerator_1_t5098 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
