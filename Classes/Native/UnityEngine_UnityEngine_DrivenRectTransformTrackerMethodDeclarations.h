﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.DrivenRectTransformTracker
struct DrivenRectTransformTracker_t394;
// UnityEngine.Object
struct Object_t120;
struct Object_t120_marshaled;
// UnityEngine.RectTransform
struct RectTransform_t338;
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"

// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
 void DrivenRectTransformTracker_Add_m2544 (DrivenRectTransformTracker_t394 * __this, Object_t120 * ___driver, RectTransform_t338 * ___rectTransform, int32_t ___drivenProperties, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
 void DrivenRectTransformTracker_Clear_m2542 (DrivenRectTransformTracker_t394 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
