﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,Vuforia.VirtualButton>
struct Transform_1_t4013;
// System.Object
struct Object_t;
// Vuforia.VirtualButton
struct VirtualButton_t639;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,Vuforia.VirtualButton>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m22439 (Transform_1_t4013 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,Vuforia.VirtualButton>::Invoke(TKey,TValue)
 VirtualButton_t639 * Transform_1_Invoke_m22440 (Transform_1_t4013 * __this, int32_t ___key, VirtualButton_t639 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,Vuforia.VirtualButton>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m22441 (Transform_1_t4013 * __this, int32_t ___key, VirtualButton_t639 * ___value, AsyncCallback_t251 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,Vuforia.VirtualButton>::EndInvoke(System.IAsyncResult)
 VirtualButton_t639 * Transform_1_EndInvoke_m22442 (Transform_1_t4013 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
