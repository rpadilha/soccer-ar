﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct ShimEnumerator_t3958;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct Dictionary_2_t646;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m21887 (ShimEnumerator_t3958 * __this, Dictionary_2_t646 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::MoveNext()
 bool ShimEnumerator_MoveNext_m21888 (ShimEnumerator_t3958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m21889 (ShimEnumerator_t3958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Key()
 Object_t * ShimEnumerator_get_Key_m21890 (ShimEnumerator_t3958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Value()
 Object_t * ShimEnumerator_get_Value_m21891 (ShimEnumerator_t3958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Current()
 Object_t * ShimEnumerator_get_Current_m21892 (ShimEnumerator_t3958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
