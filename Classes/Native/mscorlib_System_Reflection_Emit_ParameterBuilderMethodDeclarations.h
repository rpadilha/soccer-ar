﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.ParameterBuilder
struct ParameterBuilder_t1964;
// System.String
struct String_t;

// System.Int32 System.Reflection.Emit.ParameterBuilder::get_Attributes()
 int32_t ParameterBuilder_get_Attributes_m11252 (ParameterBuilder_t1964 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.ParameterBuilder::get_Name()
 String_t* ParameterBuilder_get_Name_m11253 (ParameterBuilder_t1964 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ParameterBuilder::get_Position()
 int32_t ParameterBuilder_get_Position_m11254 (ParameterBuilder_t1964 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
