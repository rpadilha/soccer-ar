﻿#pragma once
#include <stdint.h>
// Vuforia.IUserDefinedTargetEventHandler[]
struct IUserDefinedTargetEventHandlerU5BU5D_t4542;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t811  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::_items
	IUserDefinedTargetEventHandlerU5BU5D_t4542* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t811_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::EmptyArray
	IUserDefinedTargetEventHandlerU5BU5D_t4542* ___EmptyArray_4;
};
