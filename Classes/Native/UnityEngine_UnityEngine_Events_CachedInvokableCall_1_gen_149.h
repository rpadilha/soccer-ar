﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.GUISkin>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_151.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUISkin>
struct CachedInvokableCall_1_t4715  : public InvokableCall_1_t4716
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUISkin>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
