﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Joystick
struct Joystick_t208;

// System.Void Joystick::.ctor()
 void Joystick__ctor_m750 (Joystick_t208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick::.cctor()
 void Joystick__cctor_m751 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick::Start()
 void Joystick_Start_m752 (Joystick_t208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick::Disable()
 void Joystick_Disable_m753 (Joystick_t208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick::ResetJoystick()
 void Joystick_ResetJoystick_m754 (Joystick_t208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Joystick::IsFingerDown()
 bool Joystick_IsFingerDown_m755 (Joystick_t208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick::LatchedFinger(System.Int32)
 void Joystick_LatchedFinger_m756 (Joystick_t208 * __this, int32_t ___fingerId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick::Update()
 void Joystick_Update_m757 (Joystick_t208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick::Main()
 void Joystick_Main_m758 (Joystick_t208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
