﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.ImageTarget>
struct List_1_t4425;
// Vuforia.ImageTarget
struct ImageTarget_t616;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ImageTarget>
struct Enumerator_t4430 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.ImageTarget>::l
	List_1_t4425 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ImageTarget>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ImageTarget>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.ImageTarget>::current
	Object_t * ___current_3;
};
