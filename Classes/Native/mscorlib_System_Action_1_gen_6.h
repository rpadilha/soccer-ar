﻿#pragma once
#include <stdint.h>
// UnityEngine.SocialPlatforms.IAchievement[]
struct IAchievementU5BU5D_t1159;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
struct Action_1_t964  : public MulticastDelegate_t373
{
};
