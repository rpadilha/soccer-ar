﻿#pragma once
#include <stdint.h>
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t81;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.CapsuleCollider>
struct CastHelper_1_t3094 
{
	// T UnityEngine.CastHelper`1<UnityEngine.CapsuleCollider>::t
	CapsuleCollider_t81 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.CapsuleCollider>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
