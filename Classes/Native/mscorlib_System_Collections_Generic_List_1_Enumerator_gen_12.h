﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>
struct List_1_t755;
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t756;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>
struct Enumerator_t885 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::l
	List_1_t755 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>::current
	Object_t * ___current_3;
};
