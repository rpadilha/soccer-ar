﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo UICharInfo_t533_il2cpp_TypeInfo;
// System.Comparison`1<UnityEngine.UICharInfo>
struct Comparison_1_t4896  : public MulticastDelegate_t373
{
};
