﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
struct DictionaryEnumerator_t2072;
// System.Object
struct Object_t;
// System.Runtime.Remoting.Messaging.MethodDictionary
struct MethodDictionary_t2064;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::.ctor(System.Runtime.Remoting.Messaging.MethodDictionary)
 void DictionaryEnumerator__ctor_m11734 (DictionaryEnumerator_t2072 * __this, MethodDictionary_t2064 * ___methodDictionary, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Current()
 Object_t * DictionaryEnumerator_get_Current_m11735 (DictionaryEnumerator_t2072 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::MoveNext()
 bool DictionaryEnumerator_MoveNext_m11736 (DictionaryEnumerator_t2072 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Entry()
 DictionaryEntry_t1355  DictionaryEnumerator_get_Entry_m11737 (DictionaryEnumerator_t2072 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Key()
 Object_t * DictionaryEnumerator_get_Key_m11738 (DictionaryEnumerator_t2072 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Value()
 Object_t * DictionaryEnumerator_get_Value_m11739 (DictionaryEnumerator_t2072 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
