﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityEvent
struct UnityEvent_t321;
// System.Reflection.MethodInfo
struct MethodInfo_t141;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1127;

// System.Void UnityEngine.Events.UnityEvent::.ctor()
 void UnityEvent__ctor_m2248 (UnityEvent_t321 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
 MethodInfo_t141 * UnityEvent_FindMethod_Impl_m2249 (UnityEvent_t321 * __this, String_t* ___name, Object_t * ___targetObj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
 BaseInvokableCall_t1127 * UnityEvent_GetDelegate_m2250 (UnityEvent_t321 * __this, Object_t * ___target, MethodInfo_t141 * ___theFunction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent::Invoke()
 void UnityEvent_Invoke_m2253 (UnityEvent_t321 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
