﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.SpriteState
struct SpriteState_t403;
// UnityEngine.Sprite
struct Sprite_t194;

// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_highlightedSprite()
 Sprite_t194 * SpriteState_get_highlightedSprite_m1726 (SpriteState_t403 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_highlightedSprite(UnityEngine.Sprite)
 void SpriteState_set_highlightedSprite_m1727 (SpriteState_t403 * __this, Sprite_t194 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_pressedSprite()
 Sprite_t194 * SpriteState_get_pressedSprite_m1728 (SpriteState_t403 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_pressedSprite(UnityEngine.Sprite)
 void SpriteState_set_pressedSprite_m1729 (SpriteState_t403 * __this, Sprite_t194 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_disabledSprite()
 Sprite_t194 * SpriteState_get_disabledSprite_m1730 (SpriteState_t403 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_disabledSprite(UnityEngine.Sprite)
 void SpriteState_set_disabledSprite_m1731 (SpriteState_t403 * __this, Sprite_t194 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
