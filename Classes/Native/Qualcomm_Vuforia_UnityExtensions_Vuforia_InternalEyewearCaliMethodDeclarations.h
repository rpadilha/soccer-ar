﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.InternalEyewearCalibrationProfileManager
struct InternalEyewearCalibrationProfileManager_t587;
// System.String
struct String_t;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// Vuforia.InternalEyewear/EyeID
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_Eye.h"

// System.Int32 Vuforia.InternalEyewearCalibrationProfileManager::getMaxCount()
 int32_t InternalEyewearCalibrationProfileManager_getMaxCount_m2762 (InternalEyewearCalibrationProfileManager_t587 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.InternalEyewearCalibrationProfileManager::getUsedCount()
 int32_t InternalEyewearCalibrationProfileManager_getUsedCount_m2763 (InternalEyewearCalibrationProfileManager_t587 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::isProfileUsed(System.Int32)
 bool InternalEyewearCalibrationProfileManager_isProfileUsed_m2764 (InternalEyewearCalibrationProfileManager_t587 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.InternalEyewearCalibrationProfileManager::getActiveProfile()
 int32_t InternalEyewearCalibrationProfileManager_getActiveProfile_m2765 (InternalEyewearCalibrationProfileManager_t587 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::setActiveProfile(System.Int32)
 bool InternalEyewearCalibrationProfileManager_setActiveProfile_m2766 (InternalEyewearCalibrationProfileManager_t587 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Vuforia.InternalEyewearCalibrationProfileManager::getProjectionMatrix(System.Int32,Vuforia.InternalEyewear/EyeID)
 Matrix4x4_t176  InternalEyewearCalibrationProfileManager_getProjectionMatrix_m2767 (InternalEyewearCalibrationProfileManager_t587 * __this, int32_t ___profileID, int32_t ___eyeID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::setProjectionMatrix(System.Int32,Vuforia.InternalEyewear/EyeID,UnityEngine.Matrix4x4)
 bool InternalEyewearCalibrationProfileManager_setProjectionMatrix_m2768 (InternalEyewearCalibrationProfileManager_t587 * __this, int32_t ___profileID, int32_t ___eyeID, Matrix4x4_t176  ___projectionMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.InternalEyewearCalibrationProfileManager::getProfileName(System.Int32)
 String_t* InternalEyewearCalibrationProfileManager_getProfileName_m2769 (InternalEyewearCalibrationProfileManager_t587 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::setProfileName(System.Int32,System.String)
 bool InternalEyewearCalibrationProfileManager_setProfileName_m2770 (InternalEyewearCalibrationProfileManager_t587 * __this, int32_t ___profileID, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::clearProfile(System.Int32)
 bool InternalEyewearCalibrationProfileManager_clearProfile_m2771 (InternalEyewearCalibrationProfileManager_t587 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.InternalEyewearCalibrationProfileManager::.ctor()
 void InternalEyewearCalibrationProfileManager__ctor_m2772 (InternalEyewearCalibrationProfileManager_t587 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
