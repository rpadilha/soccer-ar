﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Navigation
struct Navigation_t386;
// UnityEngine.UI.Selectable
struct Selectable_t324;
// UnityEngine.UI.Navigation/Mode
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
// UnityEngine.UI.Navigation
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"

// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::get_mode()
 int32_t Navigation_get_mode_m1498 (Navigation_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_mode(UnityEngine.UI.Navigation/Mode)
 void Navigation_set_mode_m1499 (Navigation_t386 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnUp()
 Selectable_t324 * Navigation_get_selectOnUp_m1500 (Navigation_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnUp(UnityEngine.UI.Selectable)
 void Navigation_set_selectOnUp_m1501 (Navigation_t386 * __this, Selectable_t324 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnDown()
 Selectable_t324 * Navigation_get_selectOnDown_m1502 (Navigation_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnDown(UnityEngine.UI.Selectable)
 void Navigation_set_selectOnDown_m1503 (Navigation_t386 * __this, Selectable_t324 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnLeft()
 Selectable_t324 * Navigation_get_selectOnLeft_m1504 (Navigation_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnLeft(UnityEngine.UI.Selectable)
 void Navigation_set_selectOnLeft_m1505 (Navigation_t386 * __this, Selectable_t324 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnRight()
 Selectable_t324 * Navigation_get_selectOnRight_m1506 (Navigation_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnRight(UnityEngine.UI.Selectable)
 void Navigation_set_selectOnRight_m1507 (Navigation_t386 * __this, Selectable_t324 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Navigation UnityEngine.UI.Navigation::get_defaultNavigation()
 Navigation_t386  Navigation_get_defaultNavigation_m1508 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
