﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.Capture[]
struct CaptureU5BU5D_t1454;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.CaptureCollection
struct CaptureCollection_t1455  : public Object_t
{
	// System.Text.RegularExpressions.Capture[] System.Text.RegularExpressions.CaptureCollection::list
	CaptureU5BU5D_t1454* ___list_0;
};
