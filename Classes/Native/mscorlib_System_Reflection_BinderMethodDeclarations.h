﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Binder
struct Binder_t1215;
// System.Reflection.MethodBase
struct MethodBase_t1220;
// System.Reflection.MethodBase[]
struct MethodBaseU5BU5D_t1975;
// System.Object[]
struct ObjectU5BU5D_t130;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1216;
// System.Globalization.CultureInfo
struct CultureInfo_t1218;
// System.String[]
struct StringU5BU5D_t862;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t922;
// System.Reflection.PropertyInfo
struct PropertyInfo_t1758;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1976;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1221;
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"

// System.Void System.Reflection.Binder::.ctor()
 void Binder__ctor_m11357 (Binder_t1215 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Binder::.cctor()
 void Binder__cctor_m11358 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Reflection.Binder::BindToMethod(System.Reflection.BindingFlags,System.Reflection.MethodBase[],System.Object[]&,System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[],System.Object&)
// System.Object System.Reflection.Binder::ChangeType(System.Object,System.Type,System.Globalization.CultureInfo)
// System.Void System.Reflection.Binder::ReorderArgumentArray(System.Object[]&,System.Object)
// System.Reflection.MethodBase System.Reflection.Binder::SelectMethod(System.Reflection.BindingFlags,System.Reflection.MethodBase[],System.Type[],System.Reflection.ParameterModifier[])
// System.Reflection.PropertyInfo System.Reflection.Binder::SelectProperty(System.Reflection.BindingFlags,System.Reflection.PropertyInfo[],System.Type,System.Type[],System.Reflection.ParameterModifier[])
// System.Reflection.Binder System.Reflection.Binder::get_DefaultBinder()
 Binder_t1215 * Binder_get_DefaultBinder_m11359 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Binder::ConvertArgs(System.Reflection.Binder,System.Object[],System.Reflection.ParameterInfo[],System.Globalization.CultureInfo)
 bool Binder_ConvertArgs_m11360 (Object_t * __this/* static, unused */, Binder_t1215 * ___binder, ObjectU5BU5D_t130* ___args, ParameterInfoU5BU5D_t1221* ___pinfo, CultureInfo_t1218 * ___culture, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Binder::GetDerivedLevel(System.Type)
 int32_t Binder_GetDerivedLevel_m11361 (Object_t * __this/* static, unused */, Type_t * ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Reflection.Binder::FindMostDerivedMatch(System.Reflection.MethodBase[])
 MethodBase_t1220 * Binder_FindMostDerivedMatch_m11362 (Object_t * __this/* static, unused */, MethodBaseU5BU5D_t1975* ___match, MethodInfo* method) IL2CPP_METHOD_ATTR;
