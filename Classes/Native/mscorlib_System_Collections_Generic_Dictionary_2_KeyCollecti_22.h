﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>
struct Dictionary_2_t650;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.Trackable>
struct KeyCollection_t3975  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.Trackable>::dictionary
	Dictionary_2_t650 * ___dictionary_0;
};
