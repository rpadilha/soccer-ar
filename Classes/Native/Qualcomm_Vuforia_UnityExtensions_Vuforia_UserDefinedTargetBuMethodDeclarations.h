﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
struct UserDefinedTargetBuildingAbstractBehaviour_t56;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t812;
// System.String
struct String_t;
// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"

// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::RegisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
 void UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4355 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, Object_t * ___eventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::UnregisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
 bool UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4356 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, Object_t * ___eventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StartScanning()
 void UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4357 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::BuildNewTarget(System.String,System.Single)
 void UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4358 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, String_t* ___targetName, float ___sceenSizeWidth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StopScanning()
 void UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4359 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::SetFrameQuality(Vuforia.ImageTargetBuilder/FrameQuality)
 void UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4360 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, int32_t ___frameQuality, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Start()
 void UserDefinedTargetBuildingAbstractBehaviour_Start_m4361 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Update()
 void UserDefinedTargetBuildingAbstractBehaviour_Update_m4362 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnEnable()
 void UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4363 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDisable()
 void UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4364 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDestroy()
 void UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4365 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnQCARStarted()
 void UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4366 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnPause(System.Boolean)
 void UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4367 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, bool ___pause, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::.ctor()
 void UserDefinedTargetBuildingAbstractBehaviour__ctor_m490 (UserDefinedTargetBuildingAbstractBehaviour_t56 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
