﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Collections_ObjectModel_Collection_1_gen_6.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo Collection_1_t3254_il2cpp_TypeInfo;
// System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Collections_ObjectModel_Collection_1_gen_6MethodDeclarations.h"

// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.EventSystems.BaseRaycaster
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycaster.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Collections_Generic_List_1_gen_4.h"
// System.Array
#include "mscorlib_System_Array.h"
#include "UnityEngine.UI_ArrayTypes.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.String
#include "mscorlib_System_String.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
extern TypeInfo ICollection_1_t3247_il2cpp_TypeInfo;
extern TypeInfo Boolean_t122_il2cpp_TypeInfo;
extern TypeInfo IList_1_t3253_il2cpp_TypeInfo;
extern TypeInfo BaseRaycaster_t295_il2cpp_TypeInfo;
extern TypeInfo Int32_t123_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
extern TypeInfo List_1_t293_il2cpp_TypeInfo;
extern TypeInfo ICollection_t1259_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo Array_t_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t3245_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t3246_il2cpp_TypeInfo;
extern TypeInfo BaseRaycasterU5BU5D_t3244_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
extern TypeInfo NotSupportedException_t498_il2cpp_TypeInfo;
extern TypeInfo IList_t1488_il2cpp_TypeInfo;
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Collections_Generic_List_1_gen_4MethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern Il2CppType BaseRaycaster_t295_0_0_0;
extern MethodInfo ICollection_1_get_IsReadOnly_m44759_MethodInfo;
extern MethodInfo Collection_1_IsSynchronized_m16854_MethodInfo;
extern MethodInfo Collection_1_IsFixedSize_m16855_MethodInfo;
extern MethodInfo IList_1_get_Item_m44764_MethodInfo;
extern MethodInfo Collection_1_ConvertItem_m16852_MethodInfo;
extern MethodInfo Collection_1_SetItem_m16850_MethodInfo;
extern MethodInfo ICollection_1_get_Count_m44755_MethodInfo;
extern MethodInfo Object__ctor_m312_MethodInfo;
extern MethodInfo List_1__ctor_m2140_MethodInfo;
extern MethodInfo ICollection_get_SyncRoot_m13663_MethodInfo;
extern MethodInfo ICollection_CopyTo_m7824_MethodInfo;
extern MethodInfo IEnumerable_1_GetEnumerator_m44757_MethodInfo;
extern MethodInfo Collection_1_InsertItem_m16843_MethodInfo;
extern MethodInfo Collection_1_IsValidItem_m16851_MethodInfo;
extern MethodInfo ICollection_1_Contains_m44762_MethodInfo;
extern MethodInfo IList_1_IndexOf_m44766_MethodInfo;
extern MethodInfo Collection_1_CheckWritable_m16853_MethodInfo;
extern MethodInfo Collection_1_IndexOf_m16841_MethodInfo;
extern MethodInfo Collection_1_RemoveItem_m16846_MethodInfo;
extern MethodInfo Collection_1_ClearItems_m16837_MethodInfo;
extern MethodInfo ICollection_1_Clear_m44761_MethodInfo;
extern MethodInfo ICollection_1_CopyTo_m44756_MethodInfo;
extern MethodInfo IList_1_Insert_m44767_MethodInfo;
extern MethodInfo IList_1_RemoveAt_m44768_MethodInfo;
extern MethodInfo IList_1_set_Item_m44765_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Type_get_IsValueType_m9853_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;
extern MethodInfo NotSupportedException__ctor_m2252_MethodInfo;
extern MethodInfo ICollection_get_IsSynchronized_m13662_MethodInfo;
extern MethodInfo IList_get_IsFixedSize_m13664_MethodInfo;


// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::.ctor()
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IEnumerable.GetEnumerator()
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.Add(System.Object)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.Contains(System.Object)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.IndexOf(System.Object)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.Insert(System.Int32,System.Object)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.Remove(System.Object)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.ICollection.get_IsSynchronized()
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.ICollection.get_SyncRoot()
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.get_IsFixedSize()
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.get_IsReadOnly()
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.get_Item(System.Int32)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.set_Item(System.Int32,System.Object)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::Add(T)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::Clear()
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::ClearItems()
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::Contains(T)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::CopyTo(T[],System.Int32)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::GetEnumerator()
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::IndexOf(T)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::Insert(System.Int32,T)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::InsertItem(System.Int32,T)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::Remove(T)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::RemoveAt(System.Int32)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::RemoveItem(System.Int32)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::get_Count()
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::get_Item(System.Int32)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::set_Item(System.Int32,T)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::SetItem(System.Int32,T)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::IsValidItem(System.Object)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::ConvertItem(System.Object)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::CheckWritable(System.Collections.Generic.IList`1<T>)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::IsSynchronized(System.Collections.Generic.IList`1<T>)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::IsFixedSize(System.Collections.Generic.IList`1<T>)
// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType IList_1_t3253_0_0_1;
FieldInfo Collection_1_t3254____list_0_FieldInfo = 
{
	"list"/* name */
	, &IList_1_t3253_0_0_1/* type */
	, &Collection_1_t3254_il2cpp_TypeInfo/* parent */
	, offsetof(Collection_1_t3254, ___list_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_1;
FieldInfo Collection_1_t3254____syncRoot_1_FieldInfo = 
{
	"syncRoot"/* name */
	, &Object_t_0_0_1/* type */
	, &Collection_1_t3254_il2cpp_TypeInfo/* parent */
	, offsetof(Collection_1_t3254, ___syncRoot_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Collection_1_t3254_FieldInfos[] =
{
	&Collection_1_t3254____list_0_FieldInfo,
	&Collection_1_t3254____syncRoot_1_FieldInfo,
	NULL
};
extern MethodInfo Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16821_MethodInfo;
static PropertyInfo Collection_1_t3254____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&Collection_1_t3254_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.ICollection<T>.IsReadOnly"/* name */
	, &Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16821_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Collection_1_System_Collections_ICollection_get_IsSynchronized_m16829_MethodInfo;
static PropertyInfo Collection_1_t3254____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&Collection_1_t3254_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.IsSynchronized"/* name */
	, &Collection_1_System_Collections_ICollection_get_IsSynchronized_m16829_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Collection_1_System_Collections_ICollection_get_SyncRoot_m16830_MethodInfo;
static PropertyInfo Collection_1_t3254____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&Collection_1_t3254_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.SyncRoot"/* name */
	, &Collection_1_System_Collections_ICollection_get_SyncRoot_m16830_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Collection_1_System_Collections_IList_get_IsFixedSize_m16831_MethodInfo;
static PropertyInfo Collection_1_t3254____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&Collection_1_t3254_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.IsFixedSize"/* name */
	, &Collection_1_System_Collections_IList_get_IsFixedSize_m16831_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Collection_1_System_Collections_IList_get_IsReadOnly_m16832_MethodInfo;
static PropertyInfo Collection_1_t3254____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&Collection_1_t3254_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.IsReadOnly"/* name */
	, &Collection_1_System_Collections_IList_get_IsReadOnly_m16832_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Collection_1_System_Collections_IList_get_Item_m16833_MethodInfo;
extern MethodInfo Collection_1_System_Collections_IList_set_Item_m16834_MethodInfo;
static PropertyInfo Collection_1_t3254____System_Collections_IList_Item_PropertyInfo = 
{
	&Collection_1_t3254_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.Item"/* name */
	, &Collection_1_System_Collections_IList_get_Item_m16833_MethodInfo/* get */
	, &Collection_1_System_Collections_IList_set_Item_m16834_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Collection_1_get_Count_m16847_MethodInfo;
static PropertyInfo Collection_1_t3254____Count_PropertyInfo = 
{
	&Collection_1_t3254_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &Collection_1_get_Count_m16847_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Collection_1_get_Item_m16848_MethodInfo;
extern MethodInfo Collection_1_set_Item_m16849_MethodInfo;
static PropertyInfo Collection_1_t3254____Item_PropertyInfo = 
{
	&Collection_1_t3254_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &Collection_1_get_Item_m16848_MethodInfo/* get */
	, &Collection_1_set_Item_m16849_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Collection_1_t3254_PropertyInfos[] =
{
	&Collection_1_t3254____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&Collection_1_t3254____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&Collection_1_t3254____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&Collection_1_t3254____System_Collections_IList_IsFixedSize_PropertyInfo,
	&Collection_1_t3254____System_Collections_IList_IsReadOnly_PropertyInfo,
	&Collection_1_t3254____System_Collections_IList_Item_PropertyInfo,
	&Collection_1_t3254____Count_PropertyInfo,
	&Collection_1_t3254____Item_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1__ctor_m16820_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::.ctor()
MethodInfo Collection_1__ctor_m16820_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Collection_1__ctor_m14594_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1__ctor_m16820_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16821_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
MethodInfo Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16821_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly"/* name */
	, (methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14595_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16821_GenericMethod/* genericMethod */

};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_System_Collections_ICollection_CopyTo_m16822_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_ICollection_CopyTo_m16822_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
MethodInfo Collection_1_System_Collections_ICollection_CopyTo_m16822_MethodInfo = 
{
	"System.Collections.ICollection.CopyTo"/* name */
	, (methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m14596_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Collection_1_t3254_Collection_1_System_Collections_ICollection_CopyTo_m16822_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_ICollection_CopyTo_m16822_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_t318_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IEnumerable_GetEnumerator_m16823_GenericMethod;
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IEnumerable.GetEnumerator()
MethodInfo Collection_1_System_Collections_IEnumerable_GetEnumerator_m16823_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m14597_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t318_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IEnumerable_GetEnumerator_m16823_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_System_Collections_IList_Add_m16824_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_Add_m16824_GenericMethod;
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.Add(System.Object)
MethodInfo Collection_1_System_Collections_IList_Add_m16824_MethodInfo = 
{
	"System.Collections.IList.Add"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_Add_m14598_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_System_Collections_IList_Add_m16824_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_Add_m16824_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_System_Collections_IList_Contains_m16825_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_Contains_m16825_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.Contains(System.Object)
MethodInfo Collection_1_System_Collections_IList_Contains_m16825_MethodInfo = 
{
	"System.Collections.IList.Contains"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_Contains_m14599_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_System_Collections_IList_Contains_m16825_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_Contains_m16825_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_System_Collections_IList_IndexOf_m16826_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_IndexOf_m16826_GenericMethod;
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.IndexOf(System.Object)
MethodInfo Collection_1_System_Collections_IList_IndexOf_m16826_MethodInfo = 
{
	"System.Collections.IList.IndexOf"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m14600_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_System_Collections_IList_IndexOf_m16826_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_IndexOf_m16826_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_System_Collections_IList_Insert_m16827_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_Insert_m16827_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.Insert(System.Int32,System.Object)
MethodInfo Collection_1_System_Collections_IList_Insert_m16827_MethodInfo = 
{
	"System.Collections.IList.Insert"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_Insert_m14601_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_System_Collections_IList_Insert_m16827_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_Insert_m16827_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_System_Collections_IList_Remove_m16828_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_Remove_m16828_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.Remove(System.Object)
MethodInfo Collection_1_System_Collections_IList_Remove_m16828_MethodInfo = 
{
	"System.Collections.IList.Remove"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_Remove_m14602_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_System_Collections_IList_Remove_m16828_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_Remove_m16828_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_ICollection_get_IsSynchronized_m16829_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.ICollection.get_IsSynchronized()
MethodInfo Collection_1_System_Collections_ICollection_get_IsSynchronized_m16829_MethodInfo = 
{
	"System.Collections.ICollection.get_IsSynchronized"/* name */
	, (methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m14603_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_ICollection_get_IsSynchronized_m16829_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_ICollection_get_SyncRoot_m16830_GenericMethod;
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.ICollection.get_SyncRoot()
MethodInfo Collection_1_System_Collections_ICollection_get_SyncRoot_m16830_MethodInfo = 
{
	"System.Collections.ICollection.get_SyncRoot"/* name */
	, (methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m14604_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_ICollection_get_SyncRoot_m16830_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_get_IsFixedSize_m16831_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.get_IsFixedSize()
MethodInfo Collection_1_System_Collections_IList_get_IsFixedSize_m16831_MethodInfo = 
{
	"System.Collections.IList.get_IsFixedSize"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m14605_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_get_IsFixedSize_m16831_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_get_IsReadOnly_m16832_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.get_IsReadOnly()
MethodInfo Collection_1_System_Collections_IList_get_IsReadOnly_m16832_MethodInfo = 
{
	"System.Collections.IList.get_IsReadOnly"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m14606_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_get_IsReadOnly_m16832_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_System_Collections_IList_get_Item_m16833_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_get_Item_m16833_GenericMethod;
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.get_Item(System.Int32)
MethodInfo Collection_1_System_Collections_IList_get_Item_m16833_MethodInfo = 
{
	"System.Collections.IList.get_Item"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_get_Item_m14607_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, Collection_1_t3254_Collection_1_System_Collections_IList_get_Item_m16833_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_get_Item_m16833_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_System_Collections_IList_set_Item_m16834_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_System_Collections_IList_set_Item_m16834_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IList.set_Item(System.Int32,System.Object)
MethodInfo Collection_1_System_Collections_IList_set_Item_m16834_MethodInfo = 
{
	"System.Collections.IList.set_Item"/* name */
	, (methodPointerType)&Collection_1_System_Collections_IList_set_Item_m14608_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_System_Collections_IList_set_Item_m16834_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_System_Collections_IList_set_Item_m16834_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_Add_m16835_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_Add_m16835_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::Add(T)
MethodInfo Collection_1_Add_m16835_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&Collection_1_Add_m14609_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_Add_m16835_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_Add_m16835_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_Clear_m16836_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::Clear()
MethodInfo Collection_1_Clear_m16836_MethodInfo = 
{
	"Clear"/* name */
	, (methodPointerType)&Collection_1_Clear_m14610_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_Clear_m16836_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_ClearItems_m16837_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::ClearItems()
MethodInfo Collection_1_ClearItems_m16837_MethodInfo = 
{
	"ClearItems"/* name */
	, (methodPointerType)&Collection_1_ClearItems_m14611_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_ClearItems_m16837_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_Contains_m16838_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_Contains_m16838_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::Contains(T)
MethodInfo Collection_1_Contains_m16838_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&Collection_1_Contains_m14612_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_Contains_m16838_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_Contains_m16838_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycasterU5BU5D_t3244_0_0_0;
extern Il2CppType BaseRaycasterU5BU5D_t3244_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_CopyTo_m16839_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycasterU5BU5D_t3244_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_CopyTo_m16839_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::CopyTo(T[],System.Int32)
MethodInfo Collection_1_CopyTo_m16839_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&Collection_1_CopyTo_m14613_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, Collection_1_t3254_Collection_1_CopyTo_m16839_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_CopyTo_m16839_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_1_t3246_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_GetEnumerator_m16840_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::GetEnumerator()
MethodInfo Collection_1_GetEnumerator_m16840_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&Collection_1_GetEnumerator_m14614_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t3246_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_GetEnumerator_m16840_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_IndexOf_m16841_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_IndexOf_m16841_GenericMethod;
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::IndexOf(T)
MethodInfo Collection_1_IndexOf_m16841_MethodInfo = 
{
	"IndexOf"/* name */
	, (methodPointerType)&Collection_1_IndexOf_m14615_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_IndexOf_m16841_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_IndexOf_m16841_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_Insert_m16842_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_Insert_m16842_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::Insert(System.Int32,T)
MethodInfo Collection_1_Insert_m16842_MethodInfo = 
{
	"Insert"/* name */
	, (methodPointerType)&Collection_1_Insert_m14616_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_Insert_m16842_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_Insert_m16842_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_InsertItem_m16843_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_InsertItem_m16843_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::InsertItem(System.Int32,T)
MethodInfo Collection_1_InsertItem_m16843_MethodInfo = 
{
	"InsertItem"/* name */
	, (methodPointerType)&Collection_1_InsertItem_m14617_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_InsertItem_m16843_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_InsertItem_m16843_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_Remove_m16844_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_Remove_m16844_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::Remove(T)
MethodInfo Collection_1_Remove_m16844_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&Collection_1_Remove_m14618_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_Remove_m16844_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_Remove_m16844_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_RemoveAt_m16845_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_RemoveAt_m16845_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::RemoveAt(System.Int32)
MethodInfo Collection_1_RemoveAt_m16845_MethodInfo = 
{
	"RemoveAt"/* name */
	, (methodPointerType)&Collection_1_RemoveAt_m14619_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Collection_1_t3254_Collection_1_RemoveAt_m16845_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_RemoveAt_m16845_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_RemoveItem_m16846_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_RemoveItem_m16846_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::RemoveItem(System.Int32)
MethodInfo Collection_1_RemoveItem_m16846_MethodInfo = 
{
	"RemoveItem"/* name */
	, (methodPointerType)&Collection_1_RemoveItem_m14620_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, Collection_1_t3254_Collection_1_RemoveItem_m16846_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_RemoveItem_m16846_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_get_Count_m16847_GenericMethod;
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::get_Count()
MethodInfo Collection_1_get_Count_m16847_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&Collection_1_get_Count_m14621_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_get_Count_m16847_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_get_Item_m16848_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType BaseRaycaster_t295_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_get_Item_m16848_GenericMethod;
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::get_Item(System.Int32)
MethodInfo Collection_1_get_Item_m16848_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&Collection_1_get_Item_m14622_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &BaseRaycaster_t295_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, Collection_1_t3254_Collection_1_get_Item_m16848_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_get_Item_m16848_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_set_Item_m16849_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_set_Item_m16849_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::set_Item(System.Int32,T)
MethodInfo Collection_1_set_Item_m16849_MethodInfo = 
{
	"set_Item"/* name */
	, (methodPointerType)&Collection_1_set_Item_m14623_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_set_Item_m16849_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_set_Item_m16849_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_SetItem_m16850_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_SetItem_m16850_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::SetItem(System.Int32,T)
MethodInfo Collection_1_SetItem_m16850_MethodInfo = 
{
	"SetItem"/* name */
	, (methodPointerType)&Collection_1_SetItem_m14624_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_SetItem_m16850_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_SetItem_m16850_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_IsValidItem_m16851_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_IsValidItem_m16851_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::IsValidItem(System.Object)
MethodInfo Collection_1_IsValidItem_m16851_MethodInfo = 
{
	"IsValidItem"/* name */
	, (methodPointerType)&Collection_1_IsValidItem_m14625_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_IsValidItem_m16851_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_IsValidItem_m16851_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_ConvertItem_m16852_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType BaseRaycaster_t295_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_ConvertItem_m16852_GenericMethod;
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::ConvertItem(System.Object)
MethodInfo Collection_1_ConvertItem_m16852_MethodInfo = 
{
	"ConvertItem"/* name */
	, (methodPointerType)&Collection_1_ConvertItem_m14626_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &BaseRaycaster_t295_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_ConvertItem_m16852_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_ConvertItem_m16852_GenericMethod/* genericMethod */

};
extern Il2CppType IList_1_t3253_0_0_0;
extern Il2CppType IList_1_t3253_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_CheckWritable_m16853_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &IList_1_t3253_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_CheckWritable_m16853_GenericMethod;
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::CheckWritable(System.Collections.Generic.IList`1<T>)
MethodInfo Collection_1_CheckWritable_m16853_MethodInfo = 
{
	"CheckWritable"/* name */
	, (methodPointerType)&Collection_1_CheckWritable_m14627_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_CheckWritable_m16853_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_CheckWritable_m16853_GenericMethod/* genericMethod */

};
extern Il2CppType IList_1_t3253_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_IsSynchronized_m16854_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &IList_1_t3253_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_IsSynchronized_m16854_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::IsSynchronized(System.Collections.Generic.IList`1<T>)
MethodInfo Collection_1_IsSynchronized_m16854_MethodInfo = 
{
	"IsSynchronized"/* name */
	, (methodPointerType)&Collection_1_IsSynchronized_m14628_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_IsSynchronized_m16854_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_IsSynchronized_m16854_GenericMethod/* genericMethod */

};
extern Il2CppType IList_1_t3253_0_0_0;
static ParameterInfo Collection_1_t3254_Collection_1_IsFixedSize_m16855_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &IList_1_t3253_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Collection_1_IsFixedSize_m16855_GenericMethod;
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>::IsFixedSize(System.Collections.Generic.IList`1<T>)
MethodInfo Collection_1_IsFixedSize_m16855_MethodInfo = 
{
	"IsFixedSize"/* name */
	, (methodPointerType)&Collection_1_IsFixedSize_m14629_gshared/* method */
	, &Collection_1_t3254_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Collection_1_t3254_Collection_1_IsFixedSize_m16855_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Collection_1_IsFixedSize_m16855_GenericMethod/* genericMethod */

};
static MethodInfo* Collection_1_t3254_MethodInfos[] =
{
	&Collection_1__ctor_m16820_MethodInfo,
	&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16821_MethodInfo,
	&Collection_1_System_Collections_ICollection_CopyTo_m16822_MethodInfo,
	&Collection_1_System_Collections_IEnumerable_GetEnumerator_m16823_MethodInfo,
	&Collection_1_System_Collections_IList_Add_m16824_MethodInfo,
	&Collection_1_System_Collections_IList_Contains_m16825_MethodInfo,
	&Collection_1_System_Collections_IList_IndexOf_m16826_MethodInfo,
	&Collection_1_System_Collections_IList_Insert_m16827_MethodInfo,
	&Collection_1_System_Collections_IList_Remove_m16828_MethodInfo,
	&Collection_1_System_Collections_ICollection_get_IsSynchronized_m16829_MethodInfo,
	&Collection_1_System_Collections_ICollection_get_SyncRoot_m16830_MethodInfo,
	&Collection_1_System_Collections_IList_get_IsFixedSize_m16831_MethodInfo,
	&Collection_1_System_Collections_IList_get_IsReadOnly_m16832_MethodInfo,
	&Collection_1_System_Collections_IList_get_Item_m16833_MethodInfo,
	&Collection_1_System_Collections_IList_set_Item_m16834_MethodInfo,
	&Collection_1_Add_m16835_MethodInfo,
	&Collection_1_Clear_m16836_MethodInfo,
	&Collection_1_ClearItems_m16837_MethodInfo,
	&Collection_1_Contains_m16838_MethodInfo,
	&Collection_1_CopyTo_m16839_MethodInfo,
	&Collection_1_GetEnumerator_m16840_MethodInfo,
	&Collection_1_IndexOf_m16841_MethodInfo,
	&Collection_1_Insert_m16842_MethodInfo,
	&Collection_1_InsertItem_m16843_MethodInfo,
	&Collection_1_Remove_m16844_MethodInfo,
	&Collection_1_RemoveAt_m16845_MethodInfo,
	&Collection_1_RemoveItem_m16846_MethodInfo,
	&Collection_1_get_Count_m16847_MethodInfo,
	&Collection_1_get_Item_m16848_MethodInfo,
	&Collection_1_set_Item_m16849_MethodInfo,
	&Collection_1_SetItem_m16850_MethodInfo,
	&Collection_1_IsValidItem_m16851_MethodInfo,
	&Collection_1_ConvertItem_m16852_MethodInfo,
	&Collection_1_CheckWritable_m16853_MethodInfo,
	&Collection_1_IsSynchronized_m16854_MethodInfo,
	&Collection_1_IsFixedSize_m16855_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo Collection_1_System_Collections_IEnumerable_GetEnumerator_m16823_MethodInfo;
extern MethodInfo Collection_1_System_Collections_ICollection_CopyTo_m16822_MethodInfo;
extern MethodInfo Collection_1_System_Collections_IList_Add_m16824_MethodInfo;
extern MethodInfo Collection_1_Clear_m16836_MethodInfo;
extern MethodInfo Collection_1_System_Collections_IList_Contains_m16825_MethodInfo;
extern MethodInfo Collection_1_System_Collections_IList_IndexOf_m16826_MethodInfo;
extern MethodInfo Collection_1_System_Collections_IList_Insert_m16827_MethodInfo;
extern MethodInfo Collection_1_System_Collections_IList_Remove_m16828_MethodInfo;
extern MethodInfo Collection_1_RemoveAt_m16845_MethodInfo;
extern MethodInfo Collection_1_Add_m16835_MethodInfo;
extern MethodInfo Collection_1_Contains_m16838_MethodInfo;
extern MethodInfo Collection_1_CopyTo_m16839_MethodInfo;
extern MethodInfo Collection_1_Remove_m16844_MethodInfo;
extern MethodInfo Collection_1_Insert_m16842_MethodInfo;
extern MethodInfo Collection_1_GetEnumerator_m16840_MethodInfo;
static MethodInfo* Collection_1_t3254_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&Collection_1_System_Collections_IEnumerable_GetEnumerator_m16823_MethodInfo,
	&Collection_1_get_Count_m16847_MethodInfo,
	&Collection_1_System_Collections_ICollection_get_IsSynchronized_m16829_MethodInfo,
	&Collection_1_System_Collections_ICollection_get_SyncRoot_m16830_MethodInfo,
	&Collection_1_System_Collections_ICollection_CopyTo_m16822_MethodInfo,
	&Collection_1_System_Collections_IList_get_IsFixedSize_m16831_MethodInfo,
	&Collection_1_System_Collections_IList_get_IsReadOnly_m16832_MethodInfo,
	&Collection_1_System_Collections_IList_get_Item_m16833_MethodInfo,
	&Collection_1_System_Collections_IList_set_Item_m16834_MethodInfo,
	&Collection_1_System_Collections_IList_Add_m16824_MethodInfo,
	&Collection_1_Clear_m16836_MethodInfo,
	&Collection_1_System_Collections_IList_Contains_m16825_MethodInfo,
	&Collection_1_System_Collections_IList_IndexOf_m16826_MethodInfo,
	&Collection_1_System_Collections_IList_Insert_m16827_MethodInfo,
	&Collection_1_System_Collections_IList_Remove_m16828_MethodInfo,
	&Collection_1_RemoveAt_m16845_MethodInfo,
	&Collection_1_get_Count_m16847_MethodInfo,
	&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16821_MethodInfo,
	&Collection_1_Add_m16835_MethodInfo,
	&Collection_1_Clear_m16836_MethodInfo,
	&Collection_1_Contains_m16838_MethodInfo,
	&Collection_1_CopyTo_m16839_MethodInfo,
	&Collection_1_Remove_m16844_MethodInfo,
	&Collection_1_IndexOf_m16841_MethodInfo,
	&Collection_1_Insert_m16842_MethodInfo,
	&Collection_1_RemoveAt_m16845_MethodInfo,
	&Collection_1_get_Item_m16848_MethodInfo,
	&Collection_1_set_Item_m16849_MethodInfo,
	&Collection_1_GetEnumerator_m16840_MethodInfo,
	&Collection_1_ClearItems_m16837_MethodInfo,
	&Collection_1_InsertItem_m16843_MethodInfo,
	&Collection_1_RemoveItem_m16846_MethodInfo,
	&Collection_1_SetItem_m16850_MethodInfo,
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
static TypeInfo* Collection_1_t3254_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_t1259_il2cpp_TypeInfo,
	&IList_t1488_il2cpp_TypeInfo,
	&ICollection_1_t3247_il2cpp_TypeInfo,
	&IList_1_t3253_il2cpp_TypeInfo,
	&IEnumerable_1_t3245_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Collection_1_t3254_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1179_il2cpp_TypeInfo, 4},
	{ &ICollection_t1259_il2cpp_TypeInfo, 5},
	{ &IList_t1488_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t3247_il2cpp_TypeInfo, 20},
	{ &IList_1_t3253_il2cpp_TypeInfo, 27},
	{ &IEnumerable_1_t3245_il2cpp_TypeInfo, 32},
};
extern TypeInfo List_1_t293_il2cpp_TypeInfo;
extern TypeInfo BaseRaycaster_t295_il2cpp_TypeInfo;
static Il2CppRGCTXData Collection_1_t3254_RGCTXData[25] = 
{
	&List_1_t293_il2cpp_TypeInfo/* Class Usage */,
	&List_1__ctor_m2140_MethodInfo/* Method Usage */,
	&ICollection_1_get_IsReadOnly_m44759_MethodInfo/* Method Usage */,
	&IEnumerable_1_GetEnumerator_m44757_MethodInfo/* Method Usage */,
	&ICollection_1_get_Count_m44755_MethodInfo/* Method Usage */,
	&Collection_1_ConvertItem_m16852_MethodInfo/* Method Usage */,
	&Collection_1_InsertItem_m16843_MethodInfo/* Method Usage */,
	&Collection_1_IsValidItem_m16851_MethodInfo/* Method Usage */,
	&BaseRaycaster_t295_il2cpp_TypeInfo/* Class Usage */,
	&ICollection_1_Contains_m44762_MethodInfo/* Method Usage */,
	&IList_1_IndexOf_m44766_MethodInfo/* Method Usage */,
	&Collection_1_CheckWritable_m16853_MethodInfo/* Method Usage */,
	&Collection_1_IndexOf_m16841_MethodInfo/* Method Usage */,
	&Collection_1_RemoveItem_m16846_MethodInfo/* Method Usage */,
	&Collection_1_IsSynchronized_m16854_MethodInfo/* Method Usage */,
	&Collection_1_IsFixedSize_m16855_MethodInfo/* Method Usage */,
	&IList_1_get_Item_m44764_MethodInfo/* Method Usage */,
	&Collection_1_SetItem_m16850_MethodInfo/* Method Usage */,
	&Collection_1_ClearItems_m16837_MethodInfo/* Method Usage */,
	&ICollection_1_Clear_m44761_MethodInfo/* Method Usage */,
	&ICollection_1_CopyTo_m44756_MethodInfo/* Method Usage */,
	&IList_1_Insert_m44767_MethodInfo/* Method Usage */,
	&IList_1_RemoveAt_m44768_MethodInfo/* Method Usage */,
	&IList_1_set_Item_m44765_MethodInfo/* Method Usage */,
	&BaseRaycaster_t295_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Collection_1_t3254_0_0_0;
extern Il2CppType Collection_1_t3254_1_0_0;
struct Collection_1_t3254;
extern Il2CppGenericClass Collection_1_t3254_GenericClass;
extern CustomAttributesCache Collection_1_t1872__CustomAttributeCache;
TypeInfo Collection_1_t3254_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Collection`1"/* name */
	, "System.Collections.ObjectModel"/* namespaze */
	, Collection_1_t3254_MethodInfos/* methods */
	, Collection_1_t3254_PropertyInfos/* properties */
	, Collection_1_t3254_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Collection_1_t3254_il2cpp_TypeInfo/* element_class */
	, Collection_1_t3254_InterfacesTypeInfos/* implemented_interfaces */
	, Collection_1_t3254_VTable/* vtable */
	, &Collection_1_t1872__CustomAttributeCache/* custom_attributes_cache */
	, &Collection_1_t3254_il2cpp_TypeInfo/* cast_class */
	, &Collection_1_t3254_0_0_0/* byval_arg */
	, &Collection_1_t3254_1_0_0/* this_arg */
	, Collection_1_t3254_InterfacesOffsets/* interface_offsets */
	, &Collection_1_t3254_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, Collection_1_t3254_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Collection_1_t3254)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 36/* method_count */
	, 8/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 37/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_6.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo EqualityComparer_1_t3255_il2cpp_TypeInfo;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_6MethodDeclarations.h"

// System.Collections.Generic.GenericEqualityComparer`1
#include "mscorlib_System_Collections_Generic_GenericEqualityComparer_.h"
#include "mscorlib_ArrayTypes.h"
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_7.h"
extern TypeInfo IEquatable_1_t9721_il2cpp_TypeInfo;
extern TypeInfo GenericEqualityComparer_1_t1867_il2cpp_TypeInfo;
extern TypeInfo TypeU5BU5D_t922_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t3256_il2cpp_TypeInfo;
// System.Activator
#include "mscorlib_System_ActivatorMethodDeclarations.h"
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_7MethodDeclarations.h"
extern Il2CppType IEquatable_1_t9721_0_0_0;
extern Il2CppType GenericEqualityComparer_1_t1867_0_0_0;
extern MethodInfo Type_IsAssignableFrom_m6740_MethodInfo;
extern MethodInfo Type_MakeGenericType_m6738_MethodInfo;
extern MethodInfo Activator_CreateInstance_m12682_MethodInfo;
extern MethodInfo DefaultComparer__ctor_m16861_MethodInfo;
extern MethodInfo EqualityComparer_1_GetHashCode_m44769_MethodInfo;
extern MethodInfo EqualityComparer_1_Equals_m34303_MethodInfo;


// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::.ctor()
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::.cctor()
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::get_Default()
// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType EqualityComparer_1_t3255_0_0_49;
FieldInfo EqualityComparer_1_t3255_____default_0_FieldInfo = 
{
	"_default"/* name */
	, &EqualityComparer_1_t3255_0_0_49/* type */
	, &EqualityComparer_1_t3255_il2cpp_TypeInfo/* parent */
	, offsetof(EqualityComparer_1_t3255_StaticFields, ____default_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* EqualityComparer_1_t3255_FieldInfos[] =
{
	&EqualityComparer_1_t3255_____default_0_FieldInfo,
	NULL
};
extern MethodInfo EqualityComparer_1_get_Default_m16860_MethodInfo;
static PropertyInfo EqualityComparer_1_t3255____Default_PropertyInfo = 
{
	&EqualityComparer_1_t3255_il2cpp_TypeInfo/* parent */
	, "Default"/* name */
	, &EqualityComparer_1_get_Default_m16860_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* EqualityComparer_1_t3255_PropertyInfos[] =
{
	&EqualityComparer_1_t3255____Default_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1__ctor_m16856_GenericMethod;
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::.ctor()
MethodInfo EqualityComparer_1__ctor_m16856_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EqualityComparer_1__ctor_m14630_gshared/* method */
	, &EqualityComparer_1_t3255_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1__ctor_m16856_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1__cctor_m16857_GenericMethod;
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::.cctor()
MethodInfo EqualityComparer_1__cctor_m16857_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&EqualityComparer_1__cctor_m14631_gshared/* method */
	, &EqualityComparer_1_t3255_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1__cctor_m16857_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo EqualityComparer_1_t3255_EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16858_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16858_GenericMethod;
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16858_MethodInfo = 
{
	"System.Collections.IEqualityComparer.GetHashCode"/* name */
	, (methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14632_gshared/* method */
	, &EqualityComparer_1_t3255_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, EqualityComparer_1_t3255_EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16858_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16858_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo EqualityComparer_1_t3255_EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16859_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16859_GenericMethod;
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16859_MethodInfo = 
{
	"System.Collections.IEqualityComparer.Equals"/* name */
	, (methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14633_gshared/* method */
	, &EqualityComparer_1_t3255_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, EqualityComparer_1_t3255_EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16859_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16859_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo EqualityComparer_1_t3255_EqualityComparer_1_GetHashCode_m44769_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_GetHashCode_m44769_GenericMethod;
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::GetHashCode(T)
MethodInfo EqualityComparer_1_GetHashCode_m44769_MethodInfo = 
{
	"GetHashCode"/* name */
	, NULL/* method */
	, &EqualityComparer_1_t3255_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, EqualityComparer_1_t3255_EqualityComparer_1_GetHashCode_m44769_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_GetHashCode_m44769_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo EqualityComparer_1_t3255_EqualityComparer_1_Equals_m34303_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_Equals_m34303_GenericMethod;
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::Equals(T,T)
MethodInfo EqualityComparer_1_Equals_m34303_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &EqualityComparer_1_t3255_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, EqualityComparer_1_t3255_EqualityComparer_1_Equals_m34303_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_Equals_m34303_GenericMethod/* genericMethod */

};
extern Il2CppType EqualityComparer_1_t3255_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod EqualityComparer_1_get_Default_m16860_GenericMethod;
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::get_Default()
MethodInfo EqualityComparer_1_get_Default_m16860_MethodInfo = 
{
	"get_Default"/* name */
	, (methodPointerType)&EqualityComparer_1_get_Default_m14634_gshared/* method */
	, &EqualityComparer_1_t3255_il2cpp_TypeInfo/* declaring_type */
	, &EqualityComparer_1_t3255_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &EqualityComparer_1_get_Default_m16860_GenericMethod/* genericMethod */

};
static MethodInfo* EqualityComparer_1_t3255_MethodInfos[] =
{
	&EqualityComparer_1__ctor_m16856_MethodInfo,
	&EqualityComparer_1__cctor_m16857_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16858_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16859_MethodInfo,
	&EqualityComparer_1_GetHashCode_m44769_MethodInfo,
	&EqualityComparer_1_Equals_m34303_MethodInfo,
	&EqualityComparer_1_get_Default_m16860_MethodInfo,
	NULL
};
extern MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16859_MethodInfo;
extern MethodInfo EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16858_MethodInfo;
static MethodInfo* EqualityComparer_1_t3255_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&EqualityComparer_1_Equals_m34303_MethodInfo,
	&EqualityComparer_1_GetHashCode_m44769_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16859_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16858_MethodInfo,
	NULL,
	NULL,
};
extern TypeInfo IEqualityComparer_1_t9722_il2cpp_TypeInfo;
extern TypeInfo IEqualityComparer_t1363_il2cpp_TypeInfo;
static TypeInfo* EqualityComparer_1_t3255_InterfacesTypeInfos[] = 
{
	&IEqualityComparer_1_t9722_il2cpp_TypeInfo,
	&IEqualityComparer_t1363_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair EqualityComparer_1_t3255_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t9722_il2cpp_TypeInfo, 4},
	{ &IEqualityComparer_t1363_il2cpp_TypeInfo, 6},
};
extern TypeInfo EqualityComparer_1_t3255_il2cpp_TypeInfo;
extern TypeInfo EqualityComparer_1_t3255_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t3256_il2cpp_TypeInfo;
extern TypeInfo BaseRaycaster_t295_il2cpp_TypeInfo;
static Il2CppRGCTXData EqualityComparer_1_t3255_RGCTXData[9] = 
{
	&IEquatable_1_t9721_0_0_0/* Type Usage */,
	&BaseRaycaster_t295_0_0_0/* Type Usage */,
	&EqualityComparer_1_t3255_il2cpp_TypeInfo/* Class Usage */,
	&EqualityComparer_1_t3255_il2cpp_TypeInfo/* Static Usage */,
	&DefaultComparer_t3256_il2cpp_TypeInfo/* Class Usage */,
	&DefaultComparer__ctor_m16861_MethodInfo/* Method Usage */,
	&BaseRaycaster_t295_il2cpp_TypeInfo/* Class Usage */,
	&EqualityComparer_1_GetHashCode_m44769_MethodInfo/* Method Usage */,
	&EqualityComparer_1_Equals_m34303_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType EqualityComparer_1_t3255_0_0_0;
extern Il2CppType EqualityComparer_1_t3255_1_0_0;
struct EqualityComparer_1_t3255;
extern Il2CppGenericClass EqualityComparer_1_t3255_GenericClass;
TypeInfo EqualityComparer_1_t3255_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, EqualityComparer_1_t3255_MethodInfos/* methods */
	, EqualityComparer_1_t3255_PropertyInfos/* properties */
	, EqualityComparer_1_t3255_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &EqualityComparer_1_t3255_il2cpp_TypeInfo/* element_class */
	, EqualityComparer_1_t3255_InterfacesTypeInfos/* implemented_interfaces */
	, EqualityComparer_1_t3255_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &EqualityComparer_1_t3255_il2cpp_TypeInfo/* cast_class */
	, &EqualityComparer_1_t3255_0_0_0/* byval_arg */
	, &EqualityComparer_1_t3255_1_0_0/* this_arg */
	, EqualityComparer_1_t3255_InterfacesOffsets/* interface_offsets */
	, &EqualityComparer_1_t3255_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, EqualityComparer_1_t3255_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EqualityComparer_1_t3255)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EqualityComparer_1_t3255_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Boolean System.Collections.Generic.IEqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::Equals(T,T)
// System.Int32 System.Collections.Generic.IEqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::GetHashCode(T)
// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType BaseRaycaster_t295_0_0_0;
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo IEqualityComparer_1_t9722_IEqualityComparer_1_Equals_m44770_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEqualityComparer_1_Equals_m44770_GenericMethod;
// System.Boolean System.Collections.Generic.IEqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::Equals(T,T)
MethodInfo IEqualityComparer_1_Equals_m44770_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEqualityComparer_1_t9722_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, IEqualityComparer_1_t9722_IEqualityComparer_1_Equals_m44770_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEqualityComparer_1_Equals_m44770_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo IEqualityComparer_1_t9722_IEqualityComparer_1_GetHashCode_m44771_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEqualityComparer_1_GetHashCode_m44771_GenericMethod;
// System.Int32 System.Collections.Generic.IEqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>::GetHashCode(T)
MethodInfo IEqualityComparer_1_GetHashCode_m44771_MethodInfo = 
{
	"GetHashCode"/* name */
	, NULL/* method */
	, &IEqualityComparer_1_t9722_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IEqualityComparer_1_t9722_IEqualityComparer_1_GetHashCode_m44771_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEqualityComparer_1_GetHashCode_m44771_GenericMethod/* genericMethod */

};
static MethodInfo* IEqualityComparer_1_t9722_MethodInfos[] =
{
	&IEqualityComparer_1_Equals_m44770_MethodInfo,
	&IEqualityComparer_1_GetHashCode_m44771_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEqualityComparer_1_t9722_0_0_0;
extern Il2CppType IEqualityComparer_1_t9722_1_0_0;
struct IEqualityComparer_1_t9722;
extern Il2CppGenericClass IEqualityComparer_1_t9722_GenericClass;
TypeInfo IEqualityComparer_1_t9722_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEqualityComparer_1_t9722_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEqualityComparer_1_t9722_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEqualityComparer_1_t9722_il2cpp_TypeInfo/* cast_class */
	, &IEqualityComparer_1_t9722_0_0_0/* byval_arg */
	, &IEqualityComparer_1_t9722_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEqualityComparer_1_t9722_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Boolean System.IEquatable`1<UnityEngine.EventSystems.BaseRaycaster>::Equals(T)
// Metadata Definition System.IEquatable`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo IEquatable_1_t9721_IEquatable_1_Equals_m44772_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEquatable_1_Equals_m44772_GenericMethod;
// System.Boolean System.IEquatable`1<UnityEngine.EventSystems.BaseRaycaster>::Equals(T)
MethodInfo IEquatable_1_Equals_m44772_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEquatable_1_t9721_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, IEquatable_1_t9721_IEquatable_1_Equals_m44772_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEquatable_1_Equals_m44772_GenericMethod/* genericMethod */

};
static MethodInfo* IEquatable_1_t9721_MethodInfos[] =
{
	&IEquatable_1_Equals_m44772_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEquatable_1_t9721_1_0_0;
struct IEquatable_1_t9721;
extern Il2CppGenericClass IEquatable_1_t9721_GenericClass;
TypeInfo IEquatable_1_t9721_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, IEquatable_1_t9721_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEquatable_1_t9721_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEquatable_1_t9721_il2cpp_TypeInfo/* cast_class */
	, &IEquatable_1_t9721_0_0_0/* byval_arg */
	, &IEquatable_1_t9721_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEquatable_1_t9721_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo EqualityComparer_1__ctor_m16856_MethodInfo;


// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>::.ctor()
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>::Equals(T,T)
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer__ctor_m16861_GenericMethod;
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>::.ctor()
MethodInfo DefaultComparer__ctor_m16861_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultComparer__ctor_m14665_gshared/* method */
	, &DefaultComparer_t3256_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer__ctor_m16861_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo DefaultComparer_t3256_DefaultComparer_GetHashCode_m16862_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer_GetHashCode_m16862_GenericMethod;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>::GetHashCode(T)
MethodInfo DefaultComparer_GetHashCode_m16862_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&DefaultComparer_GetHashCode_m14666_gshared/* method */
	, &DefaultComparer_t3256_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, DefaultComparer_t3256_DefaultComparer_GetHashCode_m16862_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer_GetHashCode_m16862_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo DefaultComparer_t3256_DefaultComparer_Equals_m16863_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer_Equals_m16863_GenericMethod;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>::Equals(T,T)
MethodInfo DefaultComparer_Equals_m16863_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&DefaultComparer_Equals_m14667_gshared/* method */
	, &DefaultComparer_t3256_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, DefaultComparer_t3256_DefaultComparer_Equals_m16863_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer_Equals_m16863_GenericMethod/* genericMethod */

};
static MethodInfo* DefaultComparer_t3256_MethodInfos[] =
{
	&DefaultComparer__ctor_m16861_MethodInfo,
	&DefaultComparer_GetHashCode_m16862_MethodInfo,
	&DefaultComparer_Equals_m16863_MethodInfo,
	NULL
};
extern MethodInfo DefaultComparer_Equals_m16863_MethodInfo;
extern MethodInfo DefaultComparer_GetHashCode_m16862_MethodInfo;
static MethodInfo* DefaultComparer_t3256_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&DefaultComparer_Equals_m16863_MethodInfo,
	&DefaultComparer_GetHashCode_m16862_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16859_MethodInfo,
	&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16858_MethodInfo,
	&DefaultComparer_GetHashCode_m16862_MethodInfo,
	&DefaultComparer_Equals_m16863_MethodInfo,
};
static Il2CppInterfaceOffsetPair DefaultComparer_t3256_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t9722_il2cpp_TypeInfo, 4},
	{ &IEqualityComparer_t1363_il2cpp_TypeInfo, 6},
};
extern TypeInfo EqualityComparer_1_t3255_il2cpp_TypeInfo;
extern TypeInfo EqualityComparer_1_t3255_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t3256_il2cpp_TypeInfo;
extern TypeInfo BaseRaycaster_t295_il2cpp_TypeInfo;
extern TypeInfo BaseRaycaster_t295_il2cpp_TypeInfo;
static Il2CppRGCTXData DefaultComparer_t3256_RGCTXData[11] = 
{
	&IEquatable_1_t9721_0_0_0/* Type Usage */,
	&BaseRaycaster_t295_0_0_0/* Type Usage */,
	&EqualityComparer_1_t3255_il2cpp_TypeInfo/* Class Usage */,
	&EqualityComparer_1_t3255_il2cpp_TypeInfo/* Static Usage */,
	&DefaultComparer_t3256_il2cpp_TypeInfo/* Class Usage */,
	&DefaultComparer__ctor_m16861_MethodInfo/* Method Usage */,
	&BaseRaycaster_t295_il2cpp_TypeInfo/* Class Usage */,
	&EqualityComparer_1_GetHashCode_m44769_MethodInfo/* Method Usage */,
	&EqualityComparer_1_Equals_m34303_MethodInfo/* Method Usage */,
	&EqualityComparer_1__ctor_m16856_MethodInfo/* Method Usage */,
	&BaseRaycaster_t295_il2cpp_TypeInfo/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType DefaultComparer_t3256_0_0_0;
extern Il2CppType DefaultComparer_t3256_1_0_0;
struct DefaultComparer_t3256;
extern Il2CppGenericClass DefaultComparer_t3256_GenericClass;
extern TypeInfo EqualityComparer_1_t1866_il2cpp_TypeInfo;
TypeInfo DefaultComparer_t3256_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultComparer"/* name */
	, ""/* namespaze */
	, DefaultComparer_t3256_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &EqualityComparer_1_t3255_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &EqualityComparer_1_t1866_il2cpp_TypeInfo/* nested_in */
	, &DefaultComparer_t3256_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DefaultComparer_t3256_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DefaultComparer_t3256_il2cpp_TypeInfo/* cast_class */
	, &DefaultComparer_t3256_0_0_0/* byval_arg */
	, &DefaultComparer_t3256_1_0_0/* this_arg */
	, DefaultComparer_t3256_InterfacesOffsets/* interface_offsets */
	, &DefaultComparer_t3256_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, DefaultComparer_t3256_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultComparer_t3256)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057027/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Predicate_1_gen_10.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Predicate_1_t3249_il2cpp_TypeInfo;
// System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Predicate_1_gen_10MethodDeclarations.h"

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>::.ctor(System.Object,System.IntPtr)
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>::Invoke(T)
// System.IAsyncResult System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>::BeginInvoke(T,System.AsyncCallback,System.Object)
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>::EndInvoke(System.IAsyncResult)
// Metadata Definition System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Predicate_1_t3249_Predicate_1__ctor_m16864_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Predicate_1__ctor_m16864_GenericMethod;
// System.Void System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>::.ctor(System.Object,System.IntPtr)
MethodInfo Predicate_1__ctor_m16864_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Predicate_1__ctor_m14668_gshared/* method */
	, &Predicate_1_t3249_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, Predicate_1_t3249_Predicate_1__ctor_m16864_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Predicate_1__ctor_m16864_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo Predicate_1_t3249_Predicate_1_Invoke_m16865_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Predicate_1_Invoke_m16865_GenericMethod;
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>::Invoke(T)
MethodInfo Predicate_1_Invoke_m16865_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Predicate_1_Invoke_m14669_gshared/* method */
	, &Predicate_1_t3249_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Predicate_1_t3249_Predicate_1_Invoke_m16865_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Predicate_1_Invoke_m16865_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Predicate_1_t3249_Predicate_1_BeginInvoke_m16866_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Predicate_1_BeginInvoke_m16866_GenericMethod;
// System.IAsyncResult System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Predicate_1_BeginInvoke_m16866_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Predicate_1_BeginInvoke_m14670_gshared/* method */
	, &Predicate_1_t3249_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Predicate_1_t3249_Predicate_1_BeginInvoke_m16866_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Predicate_1_BeginInvoke_m16866_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo Predicate_1_t3249_Predicate_1_EndInvoke_m16867_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Predicate_1_EndInvoke_m16867_GenericMethod;
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>::EndInvoke(System.IAsyncResult)
MethodInfo Predicate_1_EndInvoke_m16867_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Predicate_1_EndInvoke_m14671_gshared/* method */
	, &Predicate_1_t3249_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, Predicate_1_t3249_Predicate_1_EndInvoke_m16867_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Predicate_1_EndInvoke_m16867_GenericMethod/* genericMethod */

};
static MethodInfo* Predicate_1_t3249_MethodInfos[] =
{
	&Predicate_1__ctor_m16864_MethodInfo,
	&Predicate_1_Invoke_m16865_MethodInfo,
	&Predicate_1_BeginInvoke_m16866_MethodInfo,
	&Predicate_1_EndInvoke_m16867_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
extern MethodInfo Predicate_1_Invoke_m16865_MethodInfo;
extern MethodInfo Predicate_1_BeginInvoke_m16866_MethodInfo;
extern MethodInfo Predicate_1_EndInvoke_m16867_MethodInfo;
static MethodInfo* Predicate_1_t3249_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&Predicate_1_Invoke_m16865_MethodInfo,
	&Predicate_1_BeginInvoke_m16866_MethodInfo,
	&Predicate_1_EndInvoke_m16867_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair Predicate_1_t3249_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Predicate_1_t3249_0_0_0;
extern Il2CppType Predicate_1_t3249_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct Predicate_1_t3249;
extern Il2CppGenericClass Predicate_1_t3249_GenericClass;
TypeInfo Predicate_1_t3249_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Predicate`1"/* name */
	, "System"/* namespaze */
	, Predicate_1_t3249_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Predicate_1_t3249_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Predicate_1_t3249_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Predicate_1_t3249_il2cpp_TypeInfo/* cast_class */
	, &Predicate_1_t3249_0_0_0/* byval_arg */
	, &Predicate_1_t3249_1_0_0/* this_arg */
	, Predicate_1_t3249_InterfacesOffsets/* interface_offsets */
	, &Predicate_1_t3249_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Predicate_1_t3249)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_6.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Comparer_1_t3257_il2cpp_TypeInfo;
// System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_6MethodDeclarations.h"

// System.Collections.Generic.GenericComparer`1
#include "mscorlib_System_Collections_Generic_GenericComparer_1.h"
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_7.h"
extern TypeInfo IComparable_1_t6262_il2cpp_TypeInfo;
extern TypeInfo GenericComparer_1_t1855_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t3258_il2cpp_TypeInfo;
// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_7MethodDeclarations.h"
extern Il2CppType IComparable_1_t6262_0_0_0;
extern Il2CppType GenericComparer_1_t1855_0_0_0;
extern MethodInfo DefaultComparer__ctor_m16872_MethodInfo;
extern MethodInfo Comparer_1_Compare_m44773_MethodInfo;
extern MethodInfo ArgumentException__ctor_m12706_MethodInfo;


// System.Void System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::.ctor()
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::.cctor()
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IComparer.Compare(System.Object,System.Object)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::get_Default()
// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType Comparer_1_t3257_0_0_49;
FieldInfo Comparer_1_t3257_____default_0_FieldInfo = 
{
	"_default"/* name */
	, &Comparer_1_t3257_0_0_49/* type */
	, &Comparer_1_t3257_il2cpp_TypeInfo/* parent */
	, offsetof(Comparer_1_t3257_StaticFields, ____default_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Comparer_1_t3257_FieldInfos[] =
{
	&Comparer_1_t3257_____default_0_FieldInfo,
	NULL
};
extern MethodInfo Comparer_1_get_Default_m16871_MethodInfo;
static PropertyInfo Comparer_1_t3257____Default_PropertyInfo = 
{
	&Comparer_1_t3257_il2cpp_TypeInfo/* parent */
	, "Default"/* name */
	, &Comparer_1_get_Default_m16871_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Comparer_1_t3257_PropertyInfos[] =
{
	&Comparer_1_t3257____Default_PropertyInfo,
	NULL
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1__ctor_m16868_GenericMethod;
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::.ctor()
MethodInfo Comparer_1__ctor_m16868_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Comparer_1__ctor_m14672_gshared/* method */
	, &Comparer_1_t3257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1__ctor_m16868_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1__cctor_m16869_GenericMethod;
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::.cctor()
MethodInfo Comparer_1__cctor_m16869_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Comparer_1__cctor_m14673_gshared/* method */
	, &Comparer_1_t3257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1__cctor_m16869_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Comparer_1_t3257_Comparer_1_System_Collections_IComparer_Compare_m16870_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1_System_Collections_IComparer_Compare_m16870_GenericMethod;
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::System.Collections.IComparer.Compare(System.Object,System.Object)
MethodInfo Comparer_1_System_Collections_IComparer_Compare_m16870_MethodInfo = 
{
	"System.Collections.IComparer.Compare"/* name */
	, (methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m14674_gshared/* method */
	, &Comparer_1_t3257_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, Comparer_1_t3257_Comparer_1_System_Collections_IComparer_Compare_m16870_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1_System_Collections_IComparer_Compare_m16870_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo Comparer_1_t3257_Comparer_1_Compare_m44773_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1_Compare_m44773_GenericMethod;
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::Compare(T,T)
MethodInfo Comparer_1_Compare_m44773_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &Comparer_1_t3257_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, Comparer_1_t3257_Comparer_1_Compare_m44773_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1_Compare_m44773_GenericMethod/* genericMethod */

};
extern Il2CppType Comparer_1_t3257_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparer_1_get_Default_m16871_GenericMethod;
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::get_Default()
MethodInfo Comparer_1_get_Default_m16871_MethodInfo = 
{
	"get_Default"/* name */
	, (methodPointerType)&Comparer_1_get_Default_m14675_gshared/* method */
	, &Comparer_1_t3257_il2cpp_TypeInfo/* declaring_type */
	, &Comparer_1_t3257_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparer_1_get_Default_m16871_GenericMethod/* genericMethod */

};
static MethodInfo* Comparer_1_t3257_MethodInfos[] =
{
	&Comparer_1__ctor_m16868_MethodInfo,
	&Comparer_1__cctor_m16869_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m16870_MethodInfo,
	&Comparer_1_Compare_m44773_MethodInfo,
	&Comparer_1_get_Default_m16871_MethodInfo,
	NULL
};
extern MethodInfo Comparer_1_System_Collections_IComparer_Compare_m16870_MethodInfo;
static MethodInfo* Comparer_1_t3257_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&Comparer_1_Compare_m44773_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m16870_MethodInfo,
	NULL,
};
extern TypeInfo IComparer_1_t6261_il2cpp_TypeInfo;
extern TypeInfo IComparer_t1356_il2cpp_TypeInfo;
static TypeInfo* Comparer_1_t3257_InterfacesTypeInfos[] = 
{
	&IComparer_1_t6261_il2cpp_TypeInfo,
	&IComparer_t1356_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Comparer_1_t3257_InterfacesOffsets[] = 
{
	{ &IComparer_1_t6261_il2cpp_TypeInfo, 4},
	{ &IComparer_t1356_il2cpp_TypeInfo, 5},
};
extern TypeInfo Comparer_1_t3257_il2cpp_TypeInfo;
extern TypeInfo Comparer_1_t3257_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t3258_il2cpp_TypeInfo;
extern TypeInfo BaseRaycaster_t295_il2cpp_TypeInfo;
static Il2CppRGCTXData Comparer_1_t3257_RGCTXData[8] = 
{
	&IComparable_1_t6262_0_0_0/* Type Usage */,
	&BaseRaycaster_t295_0_0_0/* Type Usage */,
	&Comparer_1_t3257_il2cpp_TypeInfo/* Class Usage */,
	&Comparer_1_t3257_il2cpp_TypeInfo/* Static Usage */,
	&DefaultComparer_t3258_il2cpp_TypeInfo/* Class Usage */,
	&DefaultComparer__ctor_m16872_MethodInfo/* Method Usage */,
	&BaseRaycaster_t295_il2cpp_TypeInfo/* Class Usage */,
	&Comparer_1_Compare_m44773_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Comparer_1_t3257_0_0_0;
extern Il2CppType Comparer_1_t3257_1_0_0;
struct Comparer_1_t3257;
extern Il2CppGenericClass Comparer_1_t3257_GenericClass;
TypeInfo Comparer_1_t3257_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, Comparer_1_t3257_MethodInfos/* methods */
	, Comparer_1_t3257_PropertyInfos/* properties */
	, Comparer_1_t3257_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Comparer_1_t3257_il2cpp_TypeInfo/* element_class */
	, Comparer_1_t3257_InterfacesTypeInfos/* implemented_interfaces */
	, Comparer_1_t3257_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Comparer_1_t3257_il2cpp_TypeInfo/* cast_class */
	, &Comparer_1_t3257_0_0_0/* byval_arg */
	, &Comparer_1_t3257_1_0_0/* this_arg */
	, Comparer_1_t3257_InterfacesOffsets/* interface_offsets */
	, &Comparer_1_t3257_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, Comparer_1_t3257_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Comparer_1_t3257)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Comparer_1_t3257_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.IComparer`1<UnityEngine.EventSystems.BaseRaycaster>::Compare(T,T)
// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType BaseRaycaster_t295_0_0_0;
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo IComparer_1_t6261_IComparer_1_Compare_m34311_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparer_1_Compare_m34311_GenericMethod;
// System.Int32 System.Collections.Generic.IComparer`1<UnityEngine.EventSystems.BaseRaycaster>::Compare(T,T)
MethodInfo IComparer_1_Compare_m34311_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &IComparer_1_t6261_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, IComparer_1_t6261_IComparer_1_Compare_m34311_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparer_1_Compare_m34311_GenericMethod/* genericMethod */

};
static MethodInfo* IComparer_1_t6261_MethodInfos[] =
{
	&IComparer_1_Compare_m34311_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparer_1_t6261_0_0_0;
extern Il2CppType IComparer_1_t6261_1_0_0;
struct IComparer_1_t6261;
extern Il2CppGenericClass IComparer_1_t6261_GenericClass;
TypeInfo IComparer_1_t6261_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IComparer_1_t6261_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparer_1_t6261_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparer_1_t6261_il2cpp_TypeInfo/* cast_class */
	, &IComparer_1_t6261_0_0_0/* byval_arg */
	, &IComparer_1_t6261_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparer_1_t6261_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.IComparable`1<UnityEngine.EventSystems.BaseRaycaster>::CompareTo(T)
// Metadata Definition System.IComparable`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo IComparable_1_t6262_IComparable_1_CompareTo_m34312_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparable_1_CompareTo_m34312_GenericMethod;
// System.Int32 System.IComparable`1<UnityEngine.EventSystems.BaseRaycaster>::CompareTo(T)
MethodInfo IComparable_1_CompareTo_m34312_MethodInfo = 
{
	"CompareTo"/* name */
	, NULL/* method */
	, &IComparable_1_t6262_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IComparable_1_t6262_IComparable_1_CompareTo_m34312_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparable_1_CompareTo_m34312_GenericMethod/* genericMethod */

};
static MethodInfo* IComparable_1_t6262_MethodInfos[] =
{
	&IComparable_1_CompareTo_m34312_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1_t6262_1_0_0;
struct IComparable_1_t6262;
extern Il2CppGenericClass IComparable_1_t6262_GenericClass;
TypeInfo IComparable_1_t6262_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, IComparable_1_t6262_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t6262_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1_t6262_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1_t6262_0_0_0/* byval_arg */
	, &IComparable_1_t6262_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparable_1_t6262_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo IComparable_t184_il2cpp_TypeInfo;
extern MethodInfo Comparer_1__ctor_m16868_MethodInfo;
extern MethodInfo IComparable_1_CompareTo_m34312_MethodInfo;
extern MethodInfo IComparable_CompareTo_m13522_MethodInfo;


// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>::.ctor()
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>::Compare(T,T)
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer__ctor_m16872_GenericMethod;
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>::.ctor()
MethodInfo DefaultComparer__ctor_m16872_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultComparer__ctor_m14676_gshared/* method */
	, &DefaultComparer_t3258_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer__ctor_m16872_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo DefaultComparer_t3258_DefaultComparer_Compare_m16873_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod DefaultComparer_Compare_m16873_GenericMethod;
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>::Compare(T,T)
MethodInfo DefaultComparer_Compare_m16873_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&DefaultComparer_Compare_m14677_gshared/* method */
	, &DefaultComparer_t3258_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, DefaultComparer_t3258_DefaultComparer_Compare_m16873_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DefaultComparer_Compare_m16873_GenericMethod/* genericMethod */

};
static MethodInfo* DefaultComparer_t3258_MethodInfos[] =
{
	&DefaultComparer__ctor_m16872_MethodInfo,
	&DefaultComparer_Compare_m16873_MethodInfo,
	NULL
};
extern MethodInfo DefaultComparer_Compare_m16873_MethodInfo;
static MethodInfo* DefaultComparer_t3258_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&DefaultComparer_Compare_m16873_MethodInfo,
	&Comparer_1_System_Collections_IComparer_Compare_m16870_MethodInfo,
	&DefaultComparer_Compare_m16873_MethodInfo,
};
static Il2CppInterfaceOffsetPair DefaultComparer_t3258_InterfacesOffsets[] = 
{
	{ &IComparer_1_t6261_il2cpp_TypeInfo, 4},
	{ &IComparer_t1356_il2cpp_TypeInfo, 5},
};
extern TypeInfo Comparer_1_t3257_il2cpp_TypeInfo;
extern TypeInfo Comparer_1_t3257_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t3258_il2cpp_TypeInfo;
extern TypeInfo BaseRaycaster_t295_il2cpp_TypeInfo;
extern TypeInfo BaseRaycaster_t295_il2cpp_TypeInfo;
extern TypeInfo IComparable_1_t6262_il2cpp_TypeInfo;
static Il2CppRGCTXData DefaultComparer_t3258_RGCTXData[12] = 
{
	&IComparable_1_t6262_0_0_0/* Type Usage */,
	&BaseRaycaster_t295_0_0_0/* Type Usage */,
	&Comparer_1_t3257_il2cpp_TypeInfo/* Class Usage */,
	&Comparer_1_t3257_il2cpp_TypeInfo/* Static Usage */,
	&DefaultComparer_t3258_il2cpp_TypeInfo/* Class Usage */,
	&DefaultComparer__ctor_m16872_MethodInfo/* Method Usage */,
	&BaseRaycaster_t295_il2cpp_TypeInfo/* Class Usage */,
	&Comparer_1_Compare_m44773_MethodInfo/* Method Usage */,
	&Comparer_1__ctor_m16868_MethodInfo/* Method Usage */,
	&BaseRaycaster_t295_il2cpp_TypeInfo/* Class Usage */,
	&IComparable_1_t6262_il2cpp_TypeInfo/* Class Usage */,
	&IComparable_1_CompareTo_m34312_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType DefaultComparer_t3258_0_0_0;
extern Il2CppType DefaultComparer_t3258_1_0_0;
struct DefaultComparer_t3258;
extern Il2CppGenericClass DefaultComparer_t3258_GenericClass;
extern TypeInfo Comparer_1_t1854_il2cpp_TypeInfo;
TypeInfo DefaultComparer_t3258_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultComparer"/* name */
	, ""/* namespaze */
	, DefaultComparer_t3258_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Comparer_1_t3257_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Comparer_1_t1854_il2cpp_TypeInfo/* nested_in */
	, &DefaultComparer_t3258_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DefaultComparer_t3258_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DefaultComparer_t3258_il2cpp_TypeInfo/* cast_class */
	, &DefaultComparer_t3258_0_0_0/* byval_arg */
	, &DefaultComparer_t3258_1_0_0/* this_arg */
	, DefaultComparer_t3258_InterfacesOffsets/* interface_offsets */
	, &DefaultComparer_t3258_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, DefaultComparer_t3258_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultComparer_t3258)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Comparison`1<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Comparison_1_gen_9.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Comparison_1_t3250_il2cpp_TypeInfo;
// System.Comparison`1<UnityEngine.EventSystems.BaseRaycaster>
#include "mscorlib_System_Comparison_1_gen_9MethodDeclarations.h"



// System.Void System.Comparison`1<UnityEngine.EventSystems.BaseRaycaster>::.ctor(System.Object,System.IntPtr)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.BaseRaycaster>::Invoke(T,T)
// System.IAsyncResult System.Comparison`1<UnityEngine.EventSystems.BaseRaycaster>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.BaseRaycaster>::EndInvoke(System.IAsyncResult)
// Metadata Definition System.Comparison`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Comparison_1_t3250_Comparison_1__ctor_m16874_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1__ctor_m16874_GenericMethod;
// System.Void System.Comparison`1<UnityEngine.EventSystems.BaseRaycaster>::.ctor(System.Object,System.IntPtr)
MethodInfo Comparison_1__ctor_m16874_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Comparison_1__ctor_m14743_gshared/* method */
	, &Comparison_1_t3250_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, Comparison_1_t3250_Comparison_1__ctor_m16874_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1__ctor_m16874_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
extern Il2CppType BaseRaycaster_t295_0_0_0;
static ParameterInfo Comparison_1_t3250_Comparison_1_Invoke_m16875_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1_Invoke_m16875_GenericMethod;
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.BaseRaycaster>::Invoke(T,T)
MethodInfo Comparison_1_Invoke_m16875_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Comparison_1_Invoke_m14744_gshared/* method */
	, &Comparison_1_t3250_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t_Object_t/* invoker_method */
	, Comparison_1_t3250_Comparison_1_Invoke_m16875_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1_Invoke_m16875_GenericMethod/* genericMethod */

};
extern Il2CppType BaseRaycaster_t295_0_0_0;
extern Il2CppType BaseRaycaster_t295_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Comparison_1_t3250_Comparison_1_BeginInvoke_m16876_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &BaseRaycaster_t295_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1_BeginInvoke_m16876_GenericMethod;
// System.IAsyncResult System.Comparison`1<UnityEngine.EventSystems.BaseRaycaster>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
MethodInfo Comparison_1_BeginInvoke_m16876_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Comparison_1_BeginInvoke_m14745_gshared/* method */
	, &Comparison_1_t3250_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Comparison_1_t3250_Comparison_1_BeginInvoke_m16876_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1_BeginInvoke_m16876_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo Comparison_1_t3250_Comparison_1_EndInvoke_m16877_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Comparison_1_EndInvoke_m16877_GenericMethod;
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.BaseRaycaster>::EndInvoke(System.IAsyncResult)
MethodInfo Comparison_1_EndInvoke_m16877_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Comparison_1_EndInvoke_m14746_gshared/* method */
	, &Comparison_1_t3250_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, Comparison_1_t3250_Comparison_1_EndInvoke_m16877_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Comparison_1_EndInvoke_m16877_GenericMethod/* genericMethod */

};
static MethodInfo* Comparison_1_t3250_MethodInfos[] =
{
	&Comparison_1__ctor_m16874_MethodInfo,
	&Comparison_1_Invoke_m16875_MethodInfo,
	&Comparison_1_BeginInvoke_m16876_MethodInfo,
	&Comparison_1_EndInvoke_m16877_MethodInfo,
	NULL
};
extern MethodInfo Comparison_1_Invoke_m16875_MethodInfo;
extern MethodInfo Comparison_1_BeginInvoke_m16876_MethodInfo;
extern MethodInfo Comparison_1_EndInvoke_m16877_MethodInfo;
static MethodInfo* Comparison_1_t3250_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&Comparison_1_Invoke_m16875_MethodInfo,
	&Comparison_1_BeginInvoke_m16876_MethodInfo,
	&Comparison_1_EndInvoke_m16877_MethodInfo,
};
static Il2CppInterfaceOffsetPair Comparison_1_t3250_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Comparison_1_t3250_0_0_0;
extern Il2CppType Comparison_1_t3250_1_0_0;
struct Comparison_1_t3250;
extern Il2CppGenericClass Comparison_1_t3250_GenericClass;
TypeInfo Comparison_1_t3250_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparison`1"/* name */
	, "System"/* namespaze */
	, Comparison_1_t3250_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Comparison_1_t3250_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Comparison_1_t3250_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Comparison_1_t3250_il2cpp_TypeInfo/* cast_class */
	, &Comparison_1_t3250_0_0_0/* byval_arg */
	, &Comparison_1_t3250_1_0_0/* this_arg */
	, Comparison_1_t3250_InterfacesOffsets/* interface_offsets */
	, &Comparison_1_t3250_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Comparison_1_t3250)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6264_il2cpp_TypeInfo;

// UnityEngine.EventSystems.EventTrigger
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventTrigger>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventTrigger>
extern MethodInfo IEnumerator_1_get_Current_m44774_MethodInfo;
static PropertyInfo IEnumerator_1_t6264____Current_PropertyInfo = 
{
	&IEnumerator_1_t6264_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44774_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6264_PropertyInfos[] =
{
	&IEnumerator_1_t6264____Current_PropertyInfo,
	NULL
};
extern Il2CppType EventTrigger_t246_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44774_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventTrigger>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44774_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6264_il2cpp_TypeInfo/* declaring_type */
	, &EventTrigger_t246_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44774_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6264_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44774_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t6264_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6264_0_0_0;
extern Il2CppType IEnumerator_1_t6264_1_0_0;
struct IEnumerator_1_t6264;
extern Il2CppGenericClass IEnumerator_1_t6264_GenericClass;
TypeInfo IEnumerator_1_t6264_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6264_MethodInfos/* methods */
	, IEnumerator_1_t6264_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6264_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6264_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6264_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6264_0_0_0/* byval_arg */
	, &IEnumerator_1_t6264_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6264_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_170.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3259_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_170MethodDeclarations.h"

// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
extern TypeInfo EventTrigger_t246_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m16882_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEventTrigger_t246_m34317_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.EventTrigger>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.EventTrigger>(System.Int32)
#define Array_InternalArray__get_Item_TisEventTrigger_t246_m34317(__this, p0, method) (EventTrigger_t246 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3259____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3259_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3259, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3259____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3259_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3259, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3259_FieldInfos[] =
{
	&InternalEnumerator_1_t3259____array_0_FieldInfo,
	&InternalEnumerator_1_t3259____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16879_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3259____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3259_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16879_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3259____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3259_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16882_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3259_PropertyInfos[] =
{
	&InternalEnumerator_1_t3259____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3259____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3259_InternalEnumerator_1__ctor_m16878_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16878_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16878_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3259_InternalEnumerator_1__ctor_m16878_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16878_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16879_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16879_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3259_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16879_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16880_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16880_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16880_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16881_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16881_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3259_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16881_GenericMethod/* genericMethod */

};
extern Il2CppType EventTrigger_t246_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16882_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16882_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3259_il2cpp_TypeInfo/* declaring_type */
	, &EventTrigger_t246_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16882_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3259_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16878_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16879_MethodInfo,
	&InternalEnumerator_1_Dispose_m16880_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16881_MethodInfo,
	&InternalEnumerator_1_get_Current_m16882_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m16881_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16880_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3259_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16879_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16881_MethodInfo,
	&InternalEnumerator_1_Dispose_m16880_MethodInfo,
	&InternalEnumerator_1_get_Current_m16882_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3259_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6264_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3259_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6264_il2cpp_TypeInfo, 7},
};
extern TypeInfo EventTrigger_t246_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3259_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16882_MethodInfo/* Method Usage */,
	&EventTrigger_t246_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisEventTrigger_t246_m34317_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3259_0_0_0;
extern Il2CppType InternalEnumerator_1_t3259_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t3259_GenericClass;
TypeInfo InternalEnumerator_1_t3259_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3259_MethodInfos/* methods */
	, InternalEnumerator_1_t3259_PropertyInfos/* properties */
	, InternalEnumerator_1_t3259_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3259_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3259_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3259_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3259_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3259_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3259_1_0_0/* this_arg */
	, InternalEnumerator_1_t3259_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3259_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3259_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3259)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7994_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>
extern MethodInfo ICollection_1_get_Count_m44775_MethodInfo;
static PropertyInfo ICollection_1_t7994____Count_PropertyInfo = 
{
	&ICollection_1_t7994_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44775_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44776_MethodInfo;
static PropertyInfo ICollection_1_t7994____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7994_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44776_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7994_PropertyInfos[] =
{
	&ICollection_1_t7994____Count_PropertyInfo,
	&ICollection_1_t7994____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44775_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>::get_Count()
MethodInfo ICollection_1_get_Count_m44775_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7994_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44775_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44776_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44776_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7994_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44776_GenericMethod/* genericMethod */

};
extern Il2CppType EventTrigger_t246_0_0_0;
extern Il2CppType EventTrigger_t246_0_0_0;
static ParameterInfo ICollection_1_t7994_ICollection_1_Add_m44777_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventTrigger_t246_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44777_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>::Add(T)
MethodInfo ICollection_1_Add_m44777_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7994_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7994_ICollection_1_Add_m44777_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44777_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44778_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>::Clear()
MethodInfo ICollection_1_Clear_m44778_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7994_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44778_GenericMethod/* genericMethod */

};
extern Il2CppType EventTrigger_t246_0_0_0;
static ParameterInfo ICollection_1_t7994_ICollection_1_Contains_m44779_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventTrigger_t246_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44779_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>::Contains(T)
MethodInfo ICollection_1_Contains_m44779_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7994_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7994_ICollection_1_Contains_m44779_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44779_GenericMethod/* genericMethod */

};
extern Il2CppType EventTriggerU5BU5D_t5788_0_0_0;
extern Il2CppType EventTriggerU5BU5D_t5788_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7994_ICollection_1_CopyTo_m44780_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &EventTriggerU5BU5D_t5788_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44780_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44780_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7994_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7994_ICollection_1_CopyTo_m44780_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44780_GenericMethod/* genericMethod */

};
extern Il2CppType EventTrigger_t246_0_0_0;
static ParameterInfo ICollection_1_t7994_ICollection_1_Remove_m44781_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventTrigger_t246_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44781_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>::Remove(T)
MethodInfo ICollection_1_Remove_m44781_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7994_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7994_ICollection_1_Remove_m44781_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44781_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7994_MethodInfos[] =
{
	&ICollection_1_get_Count_m44775_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44776_MethodInfo,
	&ICollection_1_Add_m44777_MethodInfo,
	&ICollection_1_Clear_m44778_MethodInfo,
	&ICollection_1_Contains_m44779_MethodInfo,
	&ICollection_1_CopyTo_m44780_MethodInfo,
	&ICollection_1_Remove_m44781_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7996_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7994_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7996_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7994_0_0_0;
extern Il2CppType ICollection_1_t7994_1_0_0;
struct ICollection_1_t7994;
extern Il2CppGenericClass ICollection_1_t7994_GenericClass;
TypeInfo ICollection_1_t7994_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7994_MethodInfos/* methods */
	, ICollection_1_t7994_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7994_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7994_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7994_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7994_0_0_0/* byval_arg */
	, &ICollection_1_t7994_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7994_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventTrigger>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventTrigger>
extern Il2CppType IEnumerator_1_t6264_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44782_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventTrigger>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44782_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7996_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6264_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44782_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7996_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44782_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7996_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7996_0_0_0;
extern Il2CppType IEnumerable_1_t7996_1_0_0;
struct IEnumerable_1_t7996;
extern Il2CppGenericClass IEnumerable_1_t7996_GenericClass;
TypeInfo IEnumerable_1_t7996_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7996_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7996_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7996_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7996_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7996_0_0_0/* byval_arg */
	, &IEnumerable_1_t7996_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7996_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7995_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventTrigger>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventTrigger>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventTrigger>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventTrigger>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventTrigger>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventTrigger>
extern MethodInfo IList_1_get_Item_m44783_MethodInfo;
extern MethodInfo IList_1_set_Item_m44784_MethodInfo;
static PropertyInfo IList_1_t7995____Item_PropertyInfo = 
{
	&IList_1_t7995_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44783_MethodInfo/* get */
	, &IList_1_set_Item_m44784_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7995_PropertyInfos[] =
{
	&IList_1_t7995____Item_PropertyInfo,
	NULL
};
extern Il2CppType EventTrigger_t246_0_0_0;
static ParameterInfo IList_1_t7995_IList_1_IndexOf_m44785_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EventTrigger_t246_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44785_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventTrigger>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44785_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7995_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7995_IList_1_IndexOf_m44785_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44785_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EventTrigger_t246_0_0_0;
static ParameterInfo IList_1_t7995_IList_1_Insert_m44786_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &EventTrigger_t246_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44786_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventTrigger>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44786_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7995_IList_1_Insert_m44786_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44786_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7995_IList_1_RemoveAt_m44787_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44787_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventTrigger>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44787_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7995_IList_1_RemoveAt_m44787_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44787_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7995_IList_1_get_Item_m44783_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType EventTrigger_t246_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44783_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventTrigger>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44783_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7995_il2cpp_TypeInfo/* declaring_type */
	, &EventTrigger_t246_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7995_IList_1_get_Item_m44783_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44783_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType EventTrigger_t246_0_0_0;
static ParameterInfo IList_1_t7995_IList_1_set_Item_m44784_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &EventTrigger_t246_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44784_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventTrigger>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44784_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7995_IList_1_set_Item_m44784_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44784_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7995_MethodInfos[] =
{
	&IList_1_IndexOf_m44785_MethodInfo,
	&IList_1_Insert_m44786_MethodInfo,
	&IList_1_RemoveAt_m44787_MethodInfo,
	&IList_1_get_Item_m44783_MethodInfo,
	&IList_1_set_Item_m44784_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7995_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7994_il2cpp_TypeInfo,
	&IEnumerable_1_t7996_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7995_0_0_0;
extern Il2CppType IList_1_t7995_1_0_0;
struct IList_1_t7995;
extern Il2CppGenericClass IList_1_t7995_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7995_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7995_MethodInfos/* methods */
	, IList_1_t7995_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7995_il2cpp_TypeInfo/* element_class */
	, IList_1_t7995_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7995_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7995_0_0_0/* byval_arg */
	, &IList_1_t7995_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7995_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7997_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>
extern MethodInfo ICollection_1_get_Count_m44788_MethodInfo;
static PropertyInfo ICollection_1_t7997____Count_PropertyInfo = 
{
	&ICollection_1_t7997_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44788_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44789_MethodInfo;
static PropertyInfo ICollection_1_t7997____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7997_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44789_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7997_PropertyInfos[] =
{
	&ICollection_1_t7997____Count_PropertyInfo,
	&ICollection_1_t7997____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44788_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44788_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7997_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44788_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44789_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44789_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7997_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44789_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerEnterHandler_t273_0_0_0;
extern Il2CppType IPointerEnterHandler_t273_0_0_0;
static ParameterInfo ICollection_1_t7997_ICollection_1_Add_m44790_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerEnterHandler_t273_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44790_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>::Add(T)
MethodInfo ICollection_1_Add_m44790_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7997_ICollection_1_Add_m44790_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44790_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44791_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>::Clear()
MethodInfo ICollection_1_Clear_m44791_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44791_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerEnterHandler_t273_0_0_0;
static ParameterInfo ICollection_1_t7997_ICollection_1_Contains_m44792_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerEnterHandler_t273_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44792_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44792_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7997_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7997_ICollection_1_Contains_m44792_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44792_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerEnterHandlerU5BU5D_t5789_0_0_0;
extern Il2CppType IPointerEnterHandlerU5BU5D_t5789_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7997_ICollection_1_CopyTo_m44793_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IPointerEnterHandlerU5BU5D_t5789_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44793_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44793_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7997_ICollection_1_CopyTo_m44793_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44793_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerEnterHandler_t273_0_0_0;
static ParameterInfo ICollection_1_t7997_ICollection_1_Remove_m44794_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerEnterHandler_t273_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44794_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerEnterHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44794_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7997_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7997_ICollection_1_Remove_m44794_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44794_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7997_MethodInfos[] =
{
	&ICollection_1_get_Count_m44788_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44789_MethodInfo,
	&ICollection_1_Add_m44790_MethodInfo,
	&ICollection_1_Clear_m44791_MethodInfo,
	&ICollection_1_Contains_m44792_MethodInfo,
	&ICollection_1_CopyTo_m44793_MethodInfo,
	&ICollection_1_Remove_m44794_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7999_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7997_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7999_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7997_0_0_0;
extern Il2CppType ICollection_1_t7997_1_0_0;
struct ICollection_1_t7997;
extern Il2CppGenericClass ICollection_1_t7997_GenericClass;
TypeInfo ICollection_1_t7997_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7997_MethodInfos/* methods */
	, ICollection_1_t7997_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7997_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7997_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7997_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7997_0_0_0/* byval_arg */
	, &ICollection_1_t7997_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7997_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerEnterHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerEnterHandler>
extern Il2CppType IEnumerator_1_t6266_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44795_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerEnterHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44795_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7999_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44795_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7999_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44795_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7999_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7999_0_0_0;
extern Il2CppType IEnumerable_1_t7999_1_0_0;
struct IEnumerable_1_t7999;
extern Il2CppGenericClass IEnumerable_1_t7999_GenericClass;
TypeInfo IEnumerable_1_t7999_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7999_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7999_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7999_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7999_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7999_0_0_0/* byval_arg */
	, &IEnumerable_1_t7999_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7999_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6266_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>
extern MethodInfo IEnumerator_1_get_Current_m44796_MethodInfo;
static PropertyInfo IEnumerator_1_t6266____Current_PropertyInfo = 
{
	&IEnumerator_1_t6266_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44796_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6266_PropertyInfos[] =
{
	&IEnumerator_1_t6266____Current_PropertyInfo,
	NULL
};
extern Il2CppType IPointerEnterHandler_t273_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44796_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44796_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6266_il2cpp_TypeInfo/* declaring_type */
	, &IPointerEnterHandler_t273_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44796_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6266_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44796_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6266_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6266_0_0_0;
extern Il2CppType IEnumerator_1_t6266_1_0_0;
struct IEnumerator_1_t6266;
extern Il2CppGenericClass IEnumerator_1_t6266_GenericClass;
TypeInfo IEnumerator_1_t6266_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6266_MethodInfos/* methods */
	, IEnumerator_1_t6266_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6266_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6266_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6266_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6266_0_0_0/* byval_arg */
	, &IEnumerator_1_t6266_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6266_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_171.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3260_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_171MethodDeclarations.h"

extern TypeInfo IPointerEnterHandler_t273_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16887_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIPointerEnterHandler_t273_m34328_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IPointerEnterHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IPointerEnterHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIPointerEnterHandler_t273_m34328(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3260____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3260_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3260, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3260____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3260_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3260, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3260_FieldInfos[] =
{
	&InternalEnumerator_1_t3260____array_0_FieldInfo,
	&InternalEnumerator_1_t3260____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16884_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3260____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3260_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16884_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3260____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3260_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16887_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3260_PropertyInfos[] =
{
	&InternalEnumerator_1_t3260____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3260____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3260_InternalEnumerator_1__ctor_m16883_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16883_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16883_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3260_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3260_InternalEnumerator_1__ctor_m16883_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16883_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16884_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16884_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3260_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16884_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16885_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16885_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3260_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16885_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16886_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16886_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3260_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16886_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerEnterHandler_t273_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16887_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerEnterHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16887_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3260_il2cpp_TypeInfo/* declaring_type */
	, &IPointerEnterHandler_t273_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16887_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3260_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16883_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16884_MethodInfo,
	&InternalEnumerator_1_Dispose_m16885_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16886_MethodInfo,
	&InternalEnumerator_1_get_Current_m16887_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16886_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16885_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3260_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16884_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16886_MethodInfo,
	&InternalEnumerator_1_Dispose_m16885_MethodInfo,
	&InternalEnumerator_1_get_Current_m16887_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3260_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6266_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3260_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6266_il2cpp_TypeInfo, 7},
};
extern TypeInfo IPointerEnterHandler_t273_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3260_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16887_MethodInfo/* Method Usage */,
	&IPointerEnterHandler_t273_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIPointerEnterHandler_t273_m34328_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3260_0_0_0;
extern Il2CppType InternalEnumerator_1_t3260_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3260_GenericClass;
TypeInfo InternalEnumerator_1_t3260_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3260_MethodInfos/* methods */
	, InternalEnumerator_1_t3260_PropertyInfos/* properties */
	, InternalEnumerator_1_t3260_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3260_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3260_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3260_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3260_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3260_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3260_1_0_0/* this_arg */
	, InternalEnumerator_1_t3260_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3260_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3260_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3260)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7998_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerEnterHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerEnterHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerEnterHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerEnterHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerEnterHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerEnterHandler>
extern MethodInfo IList_1_get_Item_m44797_MethodInfo;
extern MethodInfo IList_1_set_Item_m44798_MethodInfo;
static PropertyInfo IList_1_t7998____Item_PropertyInfo = 
{
	&IList_1_t7998_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44797_MethodInfo/* get */
	, &IList_1_set_Item_m44798_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7998_PropertyInfos[] =
{
	&IList_1_t7998____Item_PropertyInfo,
	NULL
};
extern Il2CppType IPointerEnterHandler_t273_0_0_0;
static ParameterInfo IList_1_t7998_IList_1_IndexOf_m44799_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerEnterHandler_t273_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44799_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerEnterHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44799_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7998_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7998_IList_1_IndexOf_m44799_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44799_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IPointerEnterHandler_t273_0_0_0;
static ParameterInfo IList_1_t7998_IList_1_Insert_m44800_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IPointerEnterHandler_t273_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44800_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerEnterHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44800_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7998_IList_1_Insert_m44800_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44800_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7998_IList_1_RemoveAt_m44801_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44801_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerEnterHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44801_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7998_IList_1_RemoveAt_m44801_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44801_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7998_IList_1_get_Item_m44797_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IPointerEnterHandler_t273_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44797_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerEnterHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44797_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7998_il2cpp_TypeInfo/* declaring_type */
	, &IPointerEnterHandler_t273_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7998_IList_1_get_Item_m44797_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44797_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IPointerEnterHandler_t273_0_0_0;
static ParameterInfo IList_1_t7998_IList_1_set_Item_m44798_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IPointerEnterHandler_t273_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44798_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerEnterHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44798_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7998_IList_1_set_Item_m44798_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44798_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7998_MethodInfos[] =
{
	&IList_1_IndexOf_m44799_MethodInfo,
	&IList_1_Insert_m44800_MethodInfo,
	&IList_1_RemoveAt_m44801_MethodInfo,
	&IList_1_get_Item_m44797_MethodInfo,
	&IList_1_set_Item_m44798_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7998_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7997_il2cpp_TypeInfo,
	&IEnumerable_1_t7999_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7998_0_0_0;
extern Il2CppType IList_1_t7998_1_0_0;
struct IList_1_t7998;
extern Il2CppGenericClass IList_1_t7998_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7998_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7998_MethodInfos/* methods */
	, IList_1_t7998_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7998_il2cpp_TypeInfo/* element_class */
	, IList_1_t7998_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7998_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7998_0_0_0/* byval_arg */
	, &IList_1_t7998_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7998_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8000_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>
extern MethodInfo ICollection_1_get_Count_m44802_MethodInfo;
static PropertyInfo ICollection_1_t8000____Count_PropertyInfo = 
{
	&ICollection_1_t8000_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44802_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44803_MethodInfo;
static PropertyInfo ICollection_1_t8000____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8000_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44803_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8000_PropertyInfos[] =
{
	&ICollection_1_t8000____Count_PropertyInfo,
	&ICollection_1_t8000____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44802_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44802_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8000_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44802_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44803_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44803_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8000_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44803_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerExitHandler_t274_0_0_0;
extern Il2CppType IPointerExitHandler_t274_0_0_0;
static ParameterInfo ICollection_1_t8000_ICollection_1_Add_m44804_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerExitHandler_t274_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44804_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>::Add(T)
MethodInfo ICollection_1_Add_m44804_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8000_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8000_ICollection_1_Add_m44804_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44804_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44805_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>::Clear()
MethodInfo ICollection_1_Clear_m44805_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8000_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44805_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerExitHandler_t274_0_0_0;
static ParameterInfo ICollection_1_t8000_ICollection_1_Contains_m44806_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerExitHandler_t274_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44806_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44806_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8000_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8000_ICollection_1_Contains_m44806_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44806_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerExitHandlerU5BU5D_t5790_0_0_0;
extern Il2CppType IPointerExitHandlerU5BU5D_t5790_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8000_ICollection_1_CopyTo_m44807_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IPointerExitHandlerU5BU5D_t5790_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44807_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44807_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8000_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8000_ICollection_1_CopyTo_m44807_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44807_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerExitHandler_t274_0_0_0;
static ParameterInfo ICollection_1_t8000_ICollection_1_Remove_m44808_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerExitHandler_t274_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44808_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerExitHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44808_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8000_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8000_ICollection_1_Remove_m44808_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44808_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8000_MethodInfos[] =
{
	&ICollection_1_get_Count_m44802_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44803_MethodInfo,
	&ICollection_1_Add_m44804_MethodInfo,
	&ICollection_1_Clear_m44805_MethodInfo,
	&ICollection_1_Contains_m44806_MethodInfo,
	&ICollection_1_CopyTo_m44807_MethodInfo,
	&ICollection_1_Remove_m44808_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8002_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8000_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8002_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8000_0_0_0;
extern Il2CppType ICollection_1_t8000_1_0_0;
struct ICollection_1_t8000;
extern Il2CppGenericClass ICollection_1_t8000_GenericClass;
TypeInfo ICollection_1_t8000_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8000_MethodInfos/* methods */
	, ICollection_1_t8000_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8000_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8000_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8000_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8000_0_0_0/* byval_arg */
	, &ICollection_1_t8000_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8000_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerExitHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerExitHandler>
extern Il2CppType IEnumerator_1_t6268_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44809_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerExitHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44809_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8002_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6268_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44809_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8002_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44809_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8002_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8002_0_0_0;
extern Il2CppType IEnumerable_1_t8002_1_0_0;
struct IEnumerable_1_t8002;
extern Il2CppGenericClass IEnumerable_1_t8002_GenericClass;
TypeInfo IEnumerable_1_t8002_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8002_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8002_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8002_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8002_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8002_0_0_0/* byval_arg */
	, &IEnumerable_1_t8002_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8002_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6268_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>
extern MethodInfo IEnumerator_1_get_Current_m44810_MethodInfo;
static PropertyInfo IEnumerator_1_t6268____Current_PropertyInfo = 
{
	&IEnumerator_1_t6268_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44810_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6268_PropertyInfos[] =
{
	&IEnumerator_1_t6268____Current_PropertyInfo,
	NULL
};
extern Il2CppType IPointerExitHandler_t274_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44810_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44810_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6268_il2cpp_TypeInfo/* declaring_type */
	, &IPointerExitHandler_t274_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44810_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6268_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44810_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6268_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6268_0_0_0;
extern Il2CppType IEnumerator_1_t6268_1_0_0;
struct IEnumerator_1_t6268;
extern Il2CppGenericClass IEnumerator_1_t6268_GenericClass;
TypeInfo IEnumerator_1_t6268_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6268_MethodInfos/* methods */
	, IEnumerator_1_t6268_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6268_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6268_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6268_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6268_0_0_0/* byval_arg */
	, &IEnumerator_1_t6268_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6268_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_172.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3261_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_172MethodDeclarations.h"

extern TypeInfo IPointerExitHandler_t274_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16892_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIPointerExitHandler_t274_m34339_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IPointerExitHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IPointerExitHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIPointerExitHandler_t274_m34339(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3261____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3261_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3261, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3261____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3261_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3261, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3261_FieldInfos[] =
{
	&InternalEnumerator_1_t3261____array_0_FieldInfo,
	&InternalEnumerator_1_t3261____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16889_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3261____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3261_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16889_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3261____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3261_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16892_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3261_PropertyInfos[] =
{
	&InternalEnumerator_1_t3261____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3261____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3261_InternalEnumerator_1__ctor_m16888_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16888_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16888_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3261_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3261_InternalEnumerator_1__ctor_m16888_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16888_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16889_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16889_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3261_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16889_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16890_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16890_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3261_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16890_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16891_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16891_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3261_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16891_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerExitHandler_t274_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16892_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerExitHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16892_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3261_il2cpp_TypeInfo/* declaring_type */
	, &IPointerExitHandler_t274_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16892_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3261_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16888_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16889_MethodInfo,
	&InternalEnumerator_1_Dispose_m16890_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16891_MethodInfo,
	&InternalEnumerator_1_get_Current_m16892_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16891_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16890_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3261_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16889_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16891_MethodInfo,
	&InternalEnumerator_1_Dispose_m16890_MethodInfo,
	&InternalEnumerator_1_get_Current_m16892_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3261_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6268_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3261_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6268_il2cpp_TypeInfo, 7},
};
extern TypeInfo IPointerExitHandler_t274_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3261_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16892_MethodInfo/* Method Usage */,
	&IPointerExitHandler_t274_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIPointerExitHandler_t274_m34339_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3261_0_0_0;
extern Il2CppType InternalEnumerator_1_t3261_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3261_GenericClass;
TypeInfo InternalEnumerator_1_t3261_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3261_MethodInfos/* methods */
	, InternalEnumerator_1_t3261_PropertyInfos/* properties */
	, InternalEnumerator_1_t3261_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3261_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3261_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3261_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3261_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3261_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3261_1_0_0/* this_arg */
	, InternalEnumerator_1_t3261_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3261_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3261_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3261)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8001_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerExitHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerExitHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerExitHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerExitHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerExitHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerExitHandler>
extern MethodInfo IList_1_get_Item_m44811_MethodInfo;
extern MethodInfo IList_1_set_Item_m44812_MethodInfo;
static PropertyInfo IList_1_t8001____Item_PropertyInfo = 
{
	&IList_1_t8001_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44811_MethodInfo/* get */
	, &IList_1_set_Item_m44812_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8001_PropertyInfos[] =
{
	&IList_1_t8001____Item_PropertyInfo,
	NULL
};
extern Il2CppType IPointerExitHandler_t274_0_0_0;
static ParameterInfo IList_1_t8001_IList_1_IndexOf_m44813_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerExitHandler_t274_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44813_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerExitHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44813_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8001_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8001_IList_1_IndexOf_m44813_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44813_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IPointerExitHandler_t274_0_0_0;
static ParameterInfo IList_1_t8001_IList_1_Insert_m44814_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IPointerExitHandler_t274_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44814_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerExitHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44814_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8001_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8001_IList_1_Insert_m44814_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44814_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8001_IList_1_RemoveAt_m44815_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44815_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerExitHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44815_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8001_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8001_IList_1_RemoveAt_m44815_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44815_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8001_IList_1_get_Item_m44811_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IPointerExitHandler_t274_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44811_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerExitHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44811_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8001_il2cpp_TypeInfo/* declaring_type */
	, &IPointerExitHandler_t274_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8001_IList_1_get_Item_m44811_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44811_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IPointerExitHandler_t274_0_0_0;
static ParameterInfo IList_1_t8001_IList_1_set_Item_m44812_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IPointerExitHandler_t274_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44812_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerExitHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44812_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8001_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8001_IList_1_set_Item_m44812_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44812_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8001_MethodInfos[] =
{
	&IList_1_IndexOf_m44813_MethodInfo,
	&IList_1_Insert_m44814_MethodInfo,
	&IList_1_RemoveAt_m44815_MethodInfo,
	&IList_1_get_Item_m44811_MethodInfo,
	&IList_1_set_Item_m44812_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8001_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8000_il2cpp_TypeInfo,
	&IEnumerable_1_t8002_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8001_0_0_0;
extern Il2CppType IList_1_t8001_1_0_0;
struct IList_1_t8001;
extern Il2CppGenericClass IList_1_t8001_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8001_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8001_MethodInfos/* methods */
	, IList_1_t8001_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8001_il2cpp_TypeInfo/* element_class */
	, IList_1_t8001_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8001_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8001_0_0_0/* byval_arg */
	, &IList_1_t8001_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8001_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8003_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>
extern MethodInfo ICollection_1_get_Count_m44816_MethodInfo;
static PropertyInfo ICollection_1_t8003____Count_PropertyInfo = 
{
	&ICollection_1_t8003_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44816_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44817_MethodInfo;
static PropertyInfo ICollection_1_t8003____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8003_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44817_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8003_PropertyInfos[] =
{
	&ICollection_1_t8003____Count_PropertyInfo,
	&ICollection_1_t8003____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44816_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44816_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8003_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44816_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44817_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44817_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8003_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44817_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerDownHandler_t275_0_0_0;
extern Il2CppType IPointerDownHandler_t275_0_0_0;
static ParameterInfo ICollection_1_t8003_ICollection_1_Add_m44818_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerDownHandler_t275_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44818_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>::Add(T)
MethodInfo ICollection_1_Add_m44818_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8003_ICollection_1_Add_m44818_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44818_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44819_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>::Clear()
MethodInfo ICollection_1_Clear_m44819_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44819_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerDownHandler_t275_0_0_0;
static ParameterInfo ICollection_1_t8003_ICollection_1_Contains_m44820_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerDownHandler_t275_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44820_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44820_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8003_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8003_ICollection_1_Contains_m44820_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44820_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerDownHandlerU5BU5D_t5791_0_0_0;
extern Il2CppType IPointerDownHandlerU5BU5D_t5791_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8003_ICollection_1_CopyTo_m44821_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IPointerDownHandlerU5BU5D_t5791_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44821_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44821_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8003_ICollection_1_CopyTo_m44821_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44821_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerDownHandler_t275_0_0_0;
static ParameterInfo ICollection_1_t8003_ICollection_1_Remove_m44822_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerDownHandler_t275_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44822_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44822_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8003_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8003_ICollection_1_Remove_m44822_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44822_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8003_MethodInfos[] =
{
	&ICollection_1_get_Count_m44816_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44817_MethodInfo,
	&ICollection_1_Add_m44818_MethodInfo,
	&ICollection_1_Clear_m44819_MethodInfo,
	&ICollection_1_Contains_m44820_MethodInfo,
	&ICollection_1_CopyTo_m44821_MethodInfo,
	&ICollection_1_Remove_m44822_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8005_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8003_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8005_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8003_0_0_0;
extern Il2CppType ICollection_1_t8003_1_0_0;
struct ICollection_1_t8003;
extern Il2CppGenericClass ICollection_1_t8003_GenericClass;
TypeInfo ICollection_1_t8003_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8003_MethodInfos/* methods */
	, ICollection_1_t8003_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8003_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8003_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8003_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8003_0_0_0/* byval_arg */
	, &ICollection_1_t8003_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8003_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerDownHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerDownHandler>
extern Il2CppType IEnumerator_1_t6270_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44823_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerDownHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44823_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8005_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6270_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44823_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8005_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44823_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8005_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8005_0_0_0;
extern Il2CppType IEnumerable_1_t8005_1_0_0;
struct IEnumerable_1_t8005;
extern Il2CppGenericClass IEnumerable_1_t8005_GenericClass;
TypeInfo IEnumerable_1_t8005_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8005_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8005_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8005_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8005_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8005_0_0_0/* byval_arg */
	, &IEnumerable_1_t8005_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8005_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6270_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>
extern MethodInfo IEnumerator_1_get_Current_m44824_MethodInfo;
static PropertyInfo IEnumerator_1_t6270____Current_PropertyInfo = 
{
	&IEnumerator_1_t6270_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44824_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6270_PropertyInfos[] =
{
	&IEnumerator_1_t6270____Current_PropertyInfo,
	NULL
};
extern Il2CppType IPointerDownHandler_t275_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44824_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44824_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6270_il2cpp_TypeInfo/* declaring_type */
	, &IPointerDownHandler_t275_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44824_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6270_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44824_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6270_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6270_0_0_0;
extern Il2CppType IEnumerator_1_t6270_1_0_0;
struct IEnumerator_1_t6270;
extern Il2CppGenericClass IEnumerator_1_t6270_GenericClass;
TypeInfo IEnumerator_1_t6270_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6270_MethodInfos/* methods */
	, IEnumerator_1_t6270_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6270_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6270_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6270_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6270_0_0_0/* byval_arg */
	, &IEnumerator_1_t6270_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6270_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_173.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3262_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_173MethodDeclarations.h"

extern TypeInfo IPointerDownHandler_t275_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16897_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIPointerDownHandler_t275_m34350_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IPointerDownHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IPointerDownHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIPointerDownHandler_t275_m34350(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3262____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3262_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3262, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3262____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3262_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3262, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3262_FieldInfos[] =
{
	&InternalEnumerator_1_t3262____array_0_FieldInfo,
	&InternalEnumerator_1_t3262____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16894_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3262____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3262_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16894_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3262____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3262_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16897_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3262_PropertyInfos[] =
{
	&InternalEnumerator_1_t3262____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3262____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3262_InternalEnumerator_1__ctor_m16893_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16893_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16893_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3262_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3262_InternalEnumerator_1__ctor_m16893_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16893_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16894_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16894_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3262_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16894_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16895_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16895_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3262_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16895_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16896_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16896_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3262_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16896_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerDownHandler_t275_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16897_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16897_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3262_il2cpp_TypeInfo/* declaring_type */
	, &IPointerDownHandler_t275_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16897_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3262_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16893_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16894_MethodInfo,
	&InternalEnumerator_1_Dispose_m16895_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16896_MethodInfo,
	&InternalEnumerator_1_get_Current_m16897_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16896_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16895_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3262_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16894_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16896_MethodInfo,
	&InternalEnumerator_1_Dispose_m16895_MethodInfo,
	&InternalEnumerator_1_get_Current_m16897_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3262_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6270_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3262_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6270_il2cpp_TypeInfo, 7},
};
extern TypeInfo IPointerDownHandler_t275_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3262_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16897_MethodInfo/* Method Usage */,
	&IPointerDownHandler_t275_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIPointerDownHandler_t275_m34350_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3262_0_0_0;
extern Il2CppType InternalEnumerator_1_t3262_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3262_GenericClass;
TypeInfo InternalEnumerator_1_t3262_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3262_MethodInfos/* methods */
	, InternalEnumerator_1_t3262_PropertyInfos/* properties */
	, InternalEnumerator_1_t3262_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3262_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3262_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3262_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3262_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3262_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3262_1_0_0/* this_arg */
	, InternalEnumerator_1_t3262_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3262_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3262_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3262)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8004_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerDownHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerDownHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerDownHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerDownHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerDownHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerDownHandler>
extern MethodInfo IList_1_get_Item_m44825_MethodInfo;
extern MethodInfo IList_1_set_Item_m44826_MethodInfo;
static PropertyInfo IList_1_t8004____Item_PropertyInfo = 
{
	&IList_1_t8004_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44825_MethodInfo/* get */
	, &IList_1_set_Item_m44826_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8004_PropertyInfos[] =
{
	&IList_1_t8004____Item_PropertyInfo,
	NULL
};
extern Il2CppType IPointerDownHandler_t275_0_0_0;
static ParameterInfo IList_1_t8004_IList_1_IndexOf_m44827_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerDownHandler_t275_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44827_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerDownHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44827_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8004_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8004_IList_1_IndexOf_m44827_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44827_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IPointerDownHandler_t275_0_0_0;
static ParameterInfo IList_1_t8004_IList_1_Insert_m44828_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IPointerDownHandler_t275_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44828_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerDownHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44828_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8004_IList_1_Insert_m44828_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44828_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8004_IList_1_RemoveAt_m44829_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44829_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerDownHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44829_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8004_IList_1_RemoveAt_m44829_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44829_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8004_IList_1_get_Item_m44825_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IPointerDownHandler_t275_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44825_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerDownHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44825_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8004_il2cpp_TypeInfo/* declaring_type */
	, &IPointerDownHandler_t275_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8004_IList_1_get_Item_m44825_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44825_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IPointerDownHandler_t275_0_0_0;
static ParameterInfo IList_1_t8004_IList_1_set_Item_m44826_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IPointerDownHandler_t275_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44826_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerDownHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44826_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8004_IList_1_set_Item_m44826_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44826_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8004_MethodInfos[] =
{
	&IList_1_IndexOf_m44827_MethodInfo,
	&IList_1_Insert_m44828_MethodInfo,
	&IList_1_RemoveAt_m44829_MethodInfo,
	&IList_1_get_Item_m44825_MethodInfo,
	&IList_1_set_Item_m44826_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8004_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8003_il2cpp_TypeInfo,
	&IEnumerable_1_t8005_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8004_0_0_0;
extern Il2CppType IList_1_t8004_1_0_0;
struct IList_1_t8004;
extern Il2CppGenericClass IList_1_t8004_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8004_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8004_MethodInfos/* methods */
	, IList_1_t8004_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8004_il2cpp_TypeInfo/* element_class */
	, IList_1_t8004_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8004_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8004_0_0_0/* byval_arg */
	, &IList_1_t8004_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8004_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8006_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>
extern MethodInfo ICollection_1_get_Count_m44830_MethodInfo;
static PropertyInfo ICollection_1_t8006____Count_PropertyInfo = 
{
	&ICollection_1_t8006_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44830_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44831_MethodInfo;
static PropertyInfo ICollection_1_t8006____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8006_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44831_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8006_PropertyInfos[] =
{
	&ICollection_1_t8006____Count_PropertyInfo,
	&ICollection_1_t8006____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44830_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44830_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8006_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44830_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44831_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44831_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8006_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44831_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerUpHandler_t276_0_0_0;
extern Il2CppType IPointerUpHandler_t276_0_0_0;
static ParameterInfo ICollection_1_t8006_ICollection_1_Add_m44832_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerUpHandler_t276_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44832_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>::Add(T)
MethodInfo ICollection_1_Add_m44832_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8006_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8006_ICollection_1_Add_m44832_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44832_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44833_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>::Clear()
MethodInfo ICollection_1_Clear_m44833_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8006_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44833_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerUpHandler_t276_0_0_0;
static ParameterInfo ICollection_1_t8006_ICollection_1_Contains_m44834_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerUpHandler_t276_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44834_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44834_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8006_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8006_ICollection_1_Contains_m44834_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44834_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerUpHandlerU5BU5D_t5792_0_0_0;
extern Il2CppType IPointerUpHandlerU5BU5D_t5792_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8006_ICollection_1_CopyTo_m44835_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IPointerUpHandlerU5BU5D_t5792_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44835_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44835_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8006_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8006_ICollection_1_CopyTo_m44835_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44835_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerUpHandler_t276_0_0_0;
static ParameterInfo ICollection_1_t8006_ICollection_1_Remove_m44836_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerUpHandler_t276_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44836_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44836_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8006_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8006_ICollection_1_Remove_m44836_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44836_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8006_MethodInfos[] =
{
	&ICollection_1_get_Count_m44830_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44831_MethodInfo,
	&ICollection_1_Add_m44832_MethodInfo,
	&ICollection_1_Clear_m44833_MethodInfo,
	&ICollection_1_Contains_m44834_MethodInfo,
	&ICollection_1_CopyTo_m44835_MethodInfo,
	&ICollection_1_Remove_m44836_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8008_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8006_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8008_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8006_0_0_0;
extern Il2CppType ICollection_1_t8006_1_0_0;
struct ICollection_1_t8006;
extern Il2CppGenericClass ICollection_1_t8006_GenericClass;
TypeInfo ICollection_1_t8006_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8006_MethodInfos/* methods */
	, ICollection_1_t8006_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8006_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8006_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8006_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8006_0_0_0/* byval_arg */
	, &ICollection_1_t8006_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8006_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerUpHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerUpHandler>
extern Il2CppType IEnumerator_1_t6272_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44837_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerUpHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44837_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8008_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6272_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44837_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8008_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44837_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8008_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8008_0_0_0;
extern Il2CppType IEnumerable_1_t8008_1_0_0;
struct IEnumerable_1_t8008;
extern Il2CppGenericClass IEnumerable_1_t8008_GenericClass;
TypeInfo IEnumerable_1_t8008_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8008_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8008_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8008_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8008_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8008_0_0_0/* byval_arg */
	, &IEnumerable_1_t8008_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8008_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6272_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>
extern MethodInfo IEnumerator_1_get_Current_m44838_MethodInfo;
static PropertyInfo IEnumerator_1_t6272____Current_PropertyInfo = 
{
	&IEnumerator_1_t6272_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44838_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6272_PropertyInfos[] =
{
	&IEnumerator_1_t6272____Current_PropertyInfo,
	NULL
};
extern Il2CppType IPointerUpHandler_t276_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44838_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44838_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6272_il2cpp_TypeInfo/* declaring_type */
	, &IPointerUpHandler_t276_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44838_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6272_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44838_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6272_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6272_0_0_0;
extern Il2CppType IEnumerator_1_t6272_1_0_0;
struct IEnumerator_1_t6272;
extern Il2CppGenericClass IEnumerator_1_t6272_GenericClass;
TypeInfo IEnumerator_1_t6272_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6272_MethodInfos/* methods */
	, IEnumerator_1_t6272_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6272_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6272_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6272_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6272_0_0_0/* byval_arg */
	, &IEnumerator_1_t6272_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6272_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_174.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3263_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_174MethodDeclarations.h"

extern TypeInfo IPointerUpHandler_t276_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16902_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIPointerUpHandler_t276_m34361_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IPointerUpHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IPointerUpHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIPointerUpHandler_t276_m34361(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3263____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3263_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3263, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3263____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3263_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3263, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3263_FieldInfos[] =
{
	&InternalEnumerator_1_t3263____array_0_FieldInfo,
	&InternalEnumerator_1_t3263____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16899_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3263____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3263_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16899_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3263____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3263_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16902_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3263_PropertyInfos[] =
{
	&InternalEnumerator_1_t3263____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3263____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3263_InternalEnumerator_1__ctor_m16898_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16898_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16898_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3263_InternalEnumerator_1__ctor_m16898_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16898_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16899_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16899_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3263_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16899_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16900_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16900_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16900_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16901_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16901_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3263_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16901_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerUpHandler_t276_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16902_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16902_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3263_il2cpp_TypeInfo/* declaring_type */
	, &IPointerUpHandler_t276_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16902_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3263_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16898_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16899_MethodInfo,
	&InternalEnumerator_1_Dispose_m16900_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16901_MethodInfo,
	&InternalEnumerator_1_get_Current_m16902_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16901_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16900_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3263_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16899_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16901_MethodInfo,
	&InternalEnumerator_1_Dispose_m16900_MethodInfo,
	&InternalEnumerator_1_get_Current_m16902_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3263_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6272_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3263_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6272_il2cpp_TypeInfo, 7},
};
extern TypeInfo IPointerUpHandler_t276_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3263_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16902_MethodInfo/* Method Usage */,
	&IPointerUpHandler_t276_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIPointerUpHandler_t276_m34361_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3263_0_0_0;
extern Il2CppType InternalEnumerator_1_t3263_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3263_GenericClass;
TypeInfo InternalEnumerator_1_t3263_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3263_MethodInfos/* methods */
	, InternalEnumerator_1_t3263_PropertyInfos/* properties */
	, InternalEnumerator_1_t3263_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3263_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3263_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3263_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3263_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3263_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3263_1_0_0/* this_arg */
	, InternalEnumerator_1_t3263_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3263_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3263_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3263)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8007_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerUpHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerUpHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerUpHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerUpHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerUpHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerUpHandler>
extern MethodInfo IList_1_get_Item_m44839_MethodInfo;
extern MethodInfo IList_1_set_Item_m44840_MethodInfo;
static PropertyInfo IList_1_t8007____Item_PropertyInfo = 
{
	&IList_1_t8007_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44839_MethodInfo/* get */
	, &IList_1_set_Item_m44840_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8007_PropertyInfos[] =
{
	&IList_1_t8007____Item_PropertyInfo,
	NULL
};
extern Il2CppType IPointerUpHandler_t276_0_0_0;
static ParameterInfo IList_1_t8007_IList_1_IndexOf_m44841_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerUpHandler_t276_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44841_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerUpHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44841_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8007_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8007_IList_1_IndexOf_m44841_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44841_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IPointerUpHandler_t276_0_0_0;
static ParameterInfo IList_1_t8007_IList_1_Insert_m44842_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IPointerUpHandler_t276_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44842_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerUpHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44842_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8007_IList_1_Insert_m44842_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44842_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8007_IList_1_RemoveAt_m44843_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44843_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerUpHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44843_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8007_IList_1_RemoveAt_m44843_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44843_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8007_IList_1_get_Item_m44839_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IPointerUpHandler_t276_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44839_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerUpHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44839_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8007_il2cpp_TypeInfo/* declaring_type */
	, &IPointerUpHandler_t276_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8007_IList_1_get_Item_m44839_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44839_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IPointerUpHandler_t276_0_0_0;
static ParameterInfo IList_1_t8007_IList_1_set_Item_m44840_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IPointerUpHandler_t276_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44840_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerUpHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44840_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8007_IList_1_set_Item_m44840_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44840_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8007_MethodInfos[] =
{
	&IList_1_IndexOf_m44841_MethodInfo,
	&IList_1_Insert_m44842_MethodInfo,
	&IList_1_RemoveAt_m44843_MethodInfo,
	&IList_1_get_Item_m44839_MethodInfo,
	&IList_1_set_Item_m44840_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8007_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8006_il2cpp_TypeInfo,
	&IEnumerable_1_t8008_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8007_0_0_0;
extern Il2CppType IList_1_t8007_1_0_0;
struct IList_1_t8007;
extern Il2CppGenericClass IList_1_t8007_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8007_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8007_MethodInfos/* methods */
	, IList_1_t8007_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8007_il2cpp_TypeInfo/* element_class */
	, IList_1_t8007_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8007_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8007_0_0_0/* byval_arg */
	, &IList_1_t8007_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8007_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8009_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>
extern MethodInfo ICollection_1_get_Count_m44844_MethodInfo;
static PropertyInfo ICollection_1_t8009____Count_PropertyInfo = 
{
	&ICollection_1_t8009_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44844_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44845_MethodInfo;
static PropertyInfo ICollection_1_t8009____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8009_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44845_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8009_PropertyInfos[] =
{
	&ICollection_1_t8009____Count_PropertyInfo,
	&ICollection_1_t8009____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44844_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44844_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8009_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44844_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44845_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44845_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8009_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44845_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerClickHandler_t277_0_0_0;
extern Il2CppType IPointerClickHandler_t277_0_0_0;
static ParameterInfo ICollection_1_t8009_ICollection_1_Add_m44846_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerClickHandler_t277_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44846_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>::Add(T)
MethodInfo ICollection_1_Add_m44846_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8009_ICollection_1_Add_m44846_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44846_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44847_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>::Clear()
MethodInfo ICollection_1_Clear_m44847_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44847_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerClickHandler_t277_0_0_0;
static ParameterInfo ICollection_1_t8009_ICollection_1_Contains_m44848_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerClickHandler_t277_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44848_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44848_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8009_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8009_ICollection_1_Contains_m44848_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44848_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerClickHandlerU5BU5D_t5793_0_0_0;
extern Il2CppType IPointerClickHandlerU5BU5D_t5793_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8009_ICollection_1_CopyTo_m44849_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IPointerClickHandlerU5BU5D_t5793_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44849_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44849_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8009_ICollection_1_CopyTo_m44849_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44849_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerClickHandler_t277_0_0_0;
static ParameterInfo ICollection_1_t8009_ICollection_1_Remove_m44850_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerClickHandler_t277_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44850_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44850_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8009_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8009_ICollection_1_Remove_m44850_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44850_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8009_MethodInfos[] =
{
	&ICollection_1_get_Count_m44844_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44845_MethodInfo,
	&ICollection_1_Add_m44846_MethodInfo,
	&ICollection_1_Clear_m44847_MethodInfo,
	&ICollection_1_Contains_m44848_MethodInfo,
	&ICollection_1_CopyTo_m44849_MethodInfo,
	&ICollection_1_Remove_m44850_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8011_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8009_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8011_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8009_0_0_0;
extern Il2CppType ICollection_1_t8009_1_0_0;
struct ICollection_1_t8009;
extern Il2CppGenericClass ICollection_1_t8009_GenericClass;
TypeInfo ICollection_1_t8009_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8009_MethodInfos/* methods */
	, ICollection_1_t8009_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8009_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8009_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8009_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8009_0_0_0/* byval_arg */
	, &ICollection_1_t8009_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8009_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerClickHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerClickHandler>
extern Il2CppType IEnumerator_1_t6274_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44851_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerClickHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44851_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8011_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6274_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44851_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8011_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44851_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8011_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8011_0_0_0;
extern Il2CppType IEnumerable_1_t8011_1_0_0;
struct IEnumerable_1_t8011;
extern Il2CppGenericClass IEnumerable_1_t8011_GenericClass;
TypeInfo IEnumerable_1_t8011_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8011_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8011_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8011_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8011_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8011_0_0_0/* byval_arg */
	, &IEnumerable_1_t8011_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8011_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6274_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>
extern MethodInfo IEnumerator_1_get_Current_m44852_MethodInfo;
static PropertyInfo IEnumerator_1_t6274____Current_PropertyInfo = 
{
	&IEnumerator_1_t6274_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44852_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6274_PropertyInfos[] =
{
	&IEnumerator_1_t6274____Current_PropertyInfo,
	NULL
};
extern Il2CppType IPointerClickHandler_t277_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44852_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44852_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6274_il2cpp_TypeInfo/* declaring_type */
	, &IPointerClickHandler_t277_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44852_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6274_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44852_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6274_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6274_0_0_0;
extern Il2CppType IEnumerator_1_t6274_1_0_0;
struct IEnumerator_1_t6274;
extern Il2CppGenericClass IEnumerator_1_t6274_GenericClass;
TypeInfo IEnumerator_1_t6274_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6274_MethodInfos/* methods */
	, IEnumerator_1_t6274_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6274_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6274_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6274_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6274_0_0_0/* byval_arg */
	, &IEnumerator_1_t6274_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6274_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_175.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3264_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_175MethodDeclarations.h"

extern TypeInfo IPointerClickHandler_t277_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16907_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIPointerClickHandler_t277_m34372_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IPointerClickHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IPointerClickHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIPointerClickHandler_t277_m34372(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3264____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3264_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3264, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3264____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3264_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3264, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3264_FieldInfos[] =
{
	&InternalEnumerator_1_t3264____array_0_FieldInfo,
	&InternalEnumerator_1_t3264____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16904_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3264____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3264_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16904_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3264____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3264_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16907_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3264_PropertyInfos[] =
{
	&InternalEnumerator_1_t3264____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3264____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3264_InternalEnumerator_1__ctor_m16903_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16903_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16903_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3264_InternalEnumerator_1__ctor_m16903_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16903_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16904_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16904_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3264_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16904_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16905_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16905_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16905_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16906_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16906_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3264_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16906_GenericMethod/* genericMethod */

};
extern Il2CppType IPointerClickHandler_t277_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16907_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16907_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3264_il2cpp_TypeInfo/* declaring_type */
	, &IPointerClickHandler_t277_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16907_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3264_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16903_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16904_MethodInfo,
	&InternalEnumerator_1_Dispose_m16905_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16906_MethodInfo,
	&InternalEnumerator_1_get_Current_m16907_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16906_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16905_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3264_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16904_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16906_MethodInfo,
	&InternalEnumerator_1_Dispose_m16905_MethodInfo,
	&InternalEnumerator_1_get_Current_m16907_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3264_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6274_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3264_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6274_il2cpp_TypeInfo, 7},
};
extern TypeInfo IPointerClickHandler_t277_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3264_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16907_MethodInfo/* Method Usage */,
	&IPointerClickHandler_t277_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIPointerClickHandler_t277_m34372_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3264_0_0_0;
extern Il2CppType InternalEnumerator_1_t3264_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3264_GenericClass;
TypeInfo InternalEnumerator_1_t3264_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3264_MethodInfos/* methods */
	, InternalEnumerator_1_t3264_PropertyInfos/* properties */
	, InternalEnumerator_1_t3264_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3264_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3264_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3264_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3264_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3264_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3264_1_0_0/* this_arg */
	, InternalEnumerator_1_t3264_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3264_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3264_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3264)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8010_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerClickHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerClickHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerClickHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerClickHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerClickHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerClickHandler>
extern MethodInfo IList_1_get_Item_m44853_MethodInfo;
extern MethodInfo IList_1_set_Item_m44854_MethodInfo;
static PropertyInfo IList_1_t8010____Item_PropertyInfo = 
{
	&IList_1_t8010_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44853_MethodInfo/* get */
	, &IList_1_set_Item_m44854_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8010_PropertyInfos[] =
{
	&IList_1_t8010____Item_PropertyInfo,
	NULL
};
extern Il2CppType IPointerClickHandler_t277_0_0_0;
static ParameterInfo IList_1_t8010_IList_1_IndexOf_m44855_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IPointerClickHandler_t277_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44855_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerClickHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44855_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8010_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8010_IList_1_IndexOf_m44855_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44855_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IPointerClickHandler_t277_0_0_0;
static ParameterInfo IList_1_t8010_IList_1_Insert_m44856_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IPointerClickHandler_t277_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44856_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerClickHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44856_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8010_IList_1_Insert_m44856_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44856_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8010_IList_1_RemoveAt_m44857_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44857_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerClickHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44857_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8010_IList_1_RemoveAt_m44857_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44857_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8010_IList_1_get_Item_m44853_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IPointerClickHandler_t277_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44853_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerClickHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44853_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8010_il2cpp_TypeInfo/* declaring_type */
	, &IPointerClickHandler_t277_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8010_IList_1_get_Item_m44853_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44853_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IPointerClickHandler_t277_0_0_0;
static ParameterInfo IList_1_t8010_IList_1_set_Item_m44854_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IPointerClickHandler_t277_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44854_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerClickHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44854_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8010_IList_1_set_Item_m44854_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44854_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8010_MethodInfos[] =
{
	&IList_1_IndexOf_m44855_MethodInfo,
	&IList_1_Insert_m44856_MethodInfo,
	&IList_1_RemoveAt_m44857_MethodInfo,
	&IList_1_get_Item_m44853_MethodInfo,
	&IList_1_set_Item_m44854_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8010_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8009_il2cpp_TypeInfo,
	&IEnumerable_1_t8011_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8010_0_0_0;
extern Il2CppType IList_1_t8010_1_0_0;
struct IList_1_t8010;
extern Il2CppGenericClass IList_1_t8010_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8010_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8010_MethodInfos/* methods */
	, IList_1_t8010_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8010_il2cpp_TypeInfo/* element_class */
	, IList_1_t8010_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8010_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8010_0_0_0/* byval_arg */
	, &IList_1_t8010_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8010_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8012_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>
extern MethodInfo ICollection_1_get_Count_m44858_MethodInfo;
static PropertyInfo ICollection_1_t8012____Count_PropertyInfo = 
{
	&ICollection_1_t8012_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44858_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44859_MethodInfo;
static PropertyInfo ICollection_1_t8012____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8012_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44859_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8012_PropertyInfos[] =
{
	&ICollection_1_t8012____Count_PropertyInfo,
	&ICollection_1_t8012____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44858_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44858_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8012_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44858_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44859_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44859_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8012_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44859_GenericMethod/* genericMethod */

};
extern Il2CppType IBeginDragHandler_t279_0_0_0;
extern Il2CppType IBeginDragHandler_t279_0_0_0;
static ParameterInfo ICollection_1_t8012_ICollection_1_Add_m44860_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IBeginDragHandler_t279_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44860_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>::Add(T)
MethodInfo ICollection_1_Add_m44860_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8012_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8012_ICollection_1_Add_m44860_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44860_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44861_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>::Clear()
MethodInfo ICollection_1_Clear_m44861_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8012_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44861_GenericMethod/* genericMethod */

};
extern Il2CppType IBeginDragHandler_t279_0_0_0;
static ParameterInfo ICollection_1_t8012_ICollection_1_Contains_m44862_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IBeginDragHandler_t279_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44862_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44862_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8012_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8012_ICollection_1_Contains_m44862_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44862_GenericMethod/* genericMethod */

};
extern Il2CppType IBeginDragHandlerU5BU5D_t5794_0_0_0;
extern Il2CppType IBeginDragHandlerU5BU5D_t5794_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8012_ICollection_1_CopyTo_m44863_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IBeginDragHandlerU5BU5D_t5794_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44863_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44863_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8012_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8012_ICollection_1_CopyTo_m44863_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44863_GenericMethod/* genericMethod */

};
extern Il2CppType IBeginDragHandler_t279_0_0_0;
static ParameterInfo ICollection_1_t8012_ICollection_1_Remove_m44864_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IBeginDragHandler_t279_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44864_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44864_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8012_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8012_ICollection_1_Remove_m44864_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44864_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8012_MethodInfos[] =
{
	&ICollection_1_get_Count_m44858_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44859_MethodInfo,
	&ICollection_1_Add_m44860_MethodInfo,
	&ICollection_1_Clear_m44861_MethodInfo,
	&ICollection_1_Contains_m44862_MethodInfo,
	&ICollection_1_CopyTo_m44863_MethodInfo,
	&ICollection_1_Remove_m44864_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8014_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8012_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8014_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8012_0_0_0;
extern Il2CppType ICollection_1_t8012_1_0_0;
struct ICollection_1_t8012;
extern Il2CppGenericClass ICollection_1_t8012_GenericClass;
TypeInfo ICollection_1_t8012_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8012_MethodInfos/* methods */
	, ICollection_1_t8012_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8012_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8012_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8012_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8012_0_0_0/* byval_arg */
	, &ICollection_1_t8012_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8012_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IBeginDragHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IBeginDragHandler>
extern Il2CppType IEnumerator_1_t6276_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44865_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IBeginDragHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44865_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8014_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6276_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44865_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8014_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44865_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8014_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8014_0_0_0;
extern Il2CppType IEnumerable_1_t8014_1_0_0;
struct IEnumerable_1_t8014;
extern Il2CppGenericClass IEnumerable_1_t8014_GenericClass;
TypeInfo IEnumerable_1_t8014_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8014_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8014_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8014_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8014_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8014_0_0_0/* byval_arg */
	, &IEnumerable_1_t8014_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8014_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6276_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>
extern MethodInfo IEnumerator_1_get_Current_m44866_MethodInfo;
static PropertyInfo IEnumerator_1_t6276____Current_PropertyInfo = 
{
	&IEnumerator_1_t6276_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44866_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6276_PropertyInfos[] =
{
	&IEnumerator_1_t6276____Current_PropertyInfo,
	NULL
};
extern Il2CppType IBeginDragHandler_t279_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44866_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44866_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6276_il2cpp_TypeInfo/* declaring_type */
	, &IBeginDragHandler_t279_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44866_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6276_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44866_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6276_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6276_0_0_0;
extern Il2CppType IEnumerator_1_t6276_1_0_0;
struct IEnumerator_1_t6276;
extern Il2CppGenericClass IEnumerator_1_t6276_GenericClass;
TypeInfo IEnumerator_1_t6276_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6276_MethodInfos/* methods */
	, IEnumerator_1_t6276_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6276_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6276_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6276_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6276_0_0_0/* byval_arg */
	, &IEnumerator_1_t6276_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6276_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_176.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3265_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_176MethodDeclarations.h"

extern TypeInfo IBeginDragHandler_t279_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16912_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIBeginDragHandler_t279_m34383_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IBeginDragHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IBeginDragHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIBeginDragHandler_t279_m34383(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3265____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3265_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3265, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3265____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3265_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3265, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3265_FieldInfos[] =
{
	&InternalEnumerator_1_t3265____array_0_FieldInfo,
	&InternalEnumerator_1_t3265____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16909_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3265____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3265_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16909_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3265____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3265_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16912_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3265_PropertyInfos[] =
{
	&InternalEnumerator_1_t3265____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3265____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3265_InternalEnumerator_1__ctor_m16908_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16908_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16908_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3265_InternalEnumerator_1__ctor_m16908_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16908_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16909_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16909_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3265_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16909_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16910_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16910_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16910_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16911_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16911_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3265_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16911_GenericMethod/* genericMethod */

};
extern Il2CppType IBeginDragHandler_t279_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16912_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16912_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3265_il2cpp_TypeInfo/* declaring_type */
	, &IBeginDragHandler_t279_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16912_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3265_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16908_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16909_MethodInfo,
	&InternalEnumerator_1_Dispose_m16910_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16911_MethodInfo,
	&InternalEnumerator_1_get_Current_m16912_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16911_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16910_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3265_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16909_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16911_MethodInfo,
	&InternalEnumerator_1_Dispose_m16910_MethodInfo,
	&InternalEnumerator_1_get_Current_m16912_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3265_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6276_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3265_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6276_il2cpp_TypeInfo, 7},
};
extern TypeInfo IBeginDragHandler_t279_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3265_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16912_MethodInfo/* Method Usage */,
	&IBeginDragHandler_t279_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIBeginDragHandler_t279_m34383_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3265_0_0_0;
extern Il2CppType InternalEnumerator_1_t3265_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3265_GenericClass;
TypeInfo InternalEnumerator_1_t3265_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3265_MethodInfos/* methods */
	, InternalEnumerator_1_t3265_PropertyInfos/* properties */
	, InternalEnumerator_1_t3265_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3265_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3265_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3265_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3265_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3265_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3265_1_0_0/* this_arg */
	, InternalEnumerator_1_t3265_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3265_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3265_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3265)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8013_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IBeginDragHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IBeginDragHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IBeginDragHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IBeginDragHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IBeginDragHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IBeginDragHandler>
extern MethodInfo IList_1_get_Item_m44867_MethodInfo;
extern MethodInfo IList_1_set_Item_m44868_MethodInfo;
static PropertyInfo IList_1_t8013____Item_PropertyInfo = 
{
	&IList_1_t8013_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44867_MethodInfo/* get */
	, &IList_1_set_Item_m44868_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8013_PropertyInfos[] =
{
	&IList_1_t8013____Item_PropertyInfo,
	NULL
};
extern Il2CppType IBeginDragHandler_t279_0_0_0;
static ParameterInfo IList_1_t8013_IList_1_IndexOf_m44869_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IBeginDragHandler_t279_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44869_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IBeginDragHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44869_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8013_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8013_IList_1_IndexOf_m44869_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44869_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IBeginDragHandler_t279_0_0_0;
static ParameterInfo IList_1_t8013_IList_1_Insert_m44870_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IBeginDragHandler_t279_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44870_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IBeginDragHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44870_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8013_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8013_IList_1_Insert_m44870_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44870_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8013_IList_1_RemoveAt_m44871_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44871_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IBeginDragHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44871_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8013_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8013_IList_1_RemoveAt_m44871_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44871_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8013_IList_1_get_Item_m44867_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IBeginDragHandler_t279_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44867_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IBeginDragHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44867_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8013_il2cpp_TypeInfo/* declaring_type */
	, &IBeginDragHandler_t279_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8013_IList_1_get_Item_m44867_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44867_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IBeginDragHandler_t279_0_0_0;
static ParameterInfo IList_1_t8013_IList_1_set_Item_m44868_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IBeginDragHandler_t279_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44868_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IBeginDragHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44868_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8013_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8013_IList_1_set_Item_m44868_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44868_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8013_MethodInfos[] =
{
	&IList_1_IndexOf_m44869_MethodInfo,
	&IList_1_Insert_m44870_MethodInfo,
	&IList_1_RemoveAt_m44871_MethodInfo,
	&IList_1_get_Item_m44867_MethodInfo,
	&IList_1_set_Item_m44868_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8013_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8012_il2cpp_TypeInfo,
	&IEnumerable_1_t8014_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8013_0_0_0;
extern Il2CppType IList_1_t8013_1_0_0;
struct IList_1_t8013;
extern Il2CppGenericClass IList_1_t8013_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8013_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8013_MethodInfos/* methods */
	, IList_1_t8013_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8013_il2cpp_TypeInfo/* element_class */
	, IList_1_t8013_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8013_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8013_0_0_0/* byval_arg */
	, &IList_1_t8013_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8013_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8015_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
extern MethodInfo ICollection_1_get_Count_m44872_MethodInfo;
static PropertyInfo ICollection_1_t8015____Count_PropertyInfo = 
{
	&ICollection_1_t8015_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44872_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44873_MethodInfo;
static PropertyInfo ICollection_1_t8015____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8015_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44873_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8015_PropertyInfos[] =
{
	&ICollection_1_t8015____Count_PropertyInfo,
	&ICollection_1_t8015____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44872_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44872_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8015_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44872_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44873_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44873_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8015_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44873_GenericMethod/* genericMethod */

};
extern Il2CppType IInitializePotentialDragHandler_t278_0_0_0;
extern Il2CppType IInitializePotentialDragHandler_t278_0_0_0;
static ParameterInfo ICollection_1_t8015_ICollection_1_Add_m44874_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IInitializePotentialDragHandler_t278_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44874_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::Add(T)
MethodInfo ICollection_1_Add_m44874_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8015_ICollection_1_Add_m44874_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44874_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44875_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::Clear()
MethodInfo ICollection_1_Clear_m44875_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44875_GenericMethod/* genericMethod */

};
extern Il2CppType IInitializePotentialDragHandler_t278_0_0_0;
static ParameterInfo ICollection_1_t8015_ICollection_1_Contains_m44876_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IInitializePotentialDragHandler_t278_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44876_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44876_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8015_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8015_ICollection_1_Contains_m44876_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44876_GenericMethod/* genericMethod */

};
extern Il2CppType IInitializePotentialDragHandlerU5BU5D_t5795_0_0_0;
extern Il2CppType IInitializePotentialDragHandlerU5BU5D_t5795_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8015_ICollection_1_CopyTo_m44877_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IInitializePotentialDragHandlerU5BU5D_t5795_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44877_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44877_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8015_ICollection_1_CopyTo_m44877_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44877_GenericMethod/* genericMethod */

};
extern Il2CppType IInitializePotentialDragHandler_t278_0_0_0;
static ParameterInfo ICollection_1_t8015_ICollection_1_Remove_m44878_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IInitializePotentialDragHandler_t278_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44878_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44878_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8015_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8015_ICollection_1_Remove_m44878_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44878_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8015_MethodInfos[] =
{
	&ICollection_1_get_Count_m44872_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44873_MethodInfo,
	&ICollection_1_Add_m44874_MethodInfo,
	&ICollection_1_Clear_m44875_MethodInfo,
	&ICollection_1_Contains_m44876_MethodInfo,
	&ICollection_1_CopyTo_m44877_MethodInfo,
	&ICollection_1_Remove_m44878_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8017_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8015_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8017_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8015_0_0_0;
extern Il2CppType ICollection_1_t8015_1_0_0;
struct ICollection_1_t8015;
extern Il2CppGenericClass ICollection_1_t8015_GenericClass;
TypeInfo ICollection_1_t8015_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8015_MethodInfos/* methods */
	, ICollection_1_t8015_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8015_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8015_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8015_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8015_0_0_0/* byval_arg */
	, &ICollection_1_t8015_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8015_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
extern Il2CppType IEnumerator_1_t6278_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44879_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44879_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8017_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6278_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44879_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8017_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44879_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8017_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8017_0_0_0;
extern Il2CppType IEnumerable_1_t8017_1_0_0;
struct IEnumerable_1_t8017;
extern Il2CppGenericClass IEnumerable_1_t8017_GenericClass;
TypeInfo IEnumerable_1_t8017_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8017_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8017_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8017_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8017_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8017_0_0_0/* byval_arg */
	, &IEnumerable_1_t8017_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8017_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6278_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
extern MethodInfo IEnumerator_1_get_Current_m44880_MethodInfo;
static PropertyInfo IEnumerator_1_t6278____Current_PropertyInfo = 
{
	&IEnumerator_1_t6278_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44880_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6278_PropertyInfos[] =
{
	&IEnumerator_1_t6278____Current_PropertyInfo,
	NULL
};
extern Il2CppType IInitializePotentialDragHandler_t278_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44880_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44880_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6278_il2cpp_TypeInfo/* declaring_type */
	, &IInitializePotentialDragHandler_t278_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44880_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6278_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44880_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6278_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6278_0_0_0;
extern Il2CppType IEnumerator_1_t6278_1_0_0;
struct IEnumerator_1_t6278;
extern Il2CppGenericClass IEnumerator_1_t6278_GenericClass;
TypeInfo IEnumerator_1_t6278_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6278_MethodInfos/* methods */
	, IEnumerator_1_t6278_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6278_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6278_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6278_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6278_0_0_0/* byval_arg */
	, &IEnumerator_1_t6278_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6278_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_177.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3266_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_177MethodDeclarations.h"

extern TypeInfo IInitializePotentialDragHandler_t278_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16917_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIInitializePotentialDragHandler_t278_m34394_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IInitializePotentialDragHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IInitializePotentialDragHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIInitializePotentialDragHandler_t278_m34394(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3266____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3266_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3266, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3266____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3266_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3266, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3266_FieldInfos[] =
{
	&InternalEnumerator_1_t3266____array_0_FieldInfo,
	&InternalEnumerator_1_t3266____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16914_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3266____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3266_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16914_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3266____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3266_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16917_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3266_PropertyInfos[] =
{
	&InternalEnumerator_1_t3266____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3266____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3266_InternalEnumerator_1__ctor_m16913_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16913_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16913_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3266_InternalEnumerator_1__ctor_m16913_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16913_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16914_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16914_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3266_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16914_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16915_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16915_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16915_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16916_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16916_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3266_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16916_GenericMethod/* genericMethod */

};
extern Il2CppType IInitializePotentialDragHandler_t278_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16917_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16917_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3266_il2cpp_TypeInfo/* declaring_type */
	, &IInitializePotentialDragHandler_t278_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16917_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3266_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16913_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16914_MethodInfo,
	&InternalEnumerator_1_Dispose_m16915_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16916_MethodInfo,
	&InternalEnumerator_1_get_Current_m16917_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16916_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16915_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3266_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16914_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16916_MethodInfo,
	&InternalEnumerator_1_Dispose_m16915_MethodInfo,
	&InternalEnumerator_1_get_Current_m16917_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3266_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6278_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3266_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6278_il2cpp_TypeInfo, 7},
};
extern TypeInfo IInitializePotentialDragHandler_t278_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3266_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16917_MethodInfo/* Method Usage */,
	&IInitializePotentialDragHandler_t278_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIInitializePotentialDragHandler_t278_m34394_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3266_0_0_0;
extern Il2CppType InternalEnumerator_1_t3266_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3266_GenericClass;
TypeInfo InternalEnumerator_1_t3266_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3266_MethodInfos/* methods */
	, InternalEnumerator_1_t3266_PropertyInfos/* properties */
	, InternalEnumerator_1_t3266_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3266_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3266_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3266_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3266_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3266_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3266_1_0_0/* this_arg */
	, InternalEnumerator_1_t3266_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3266_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3266_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3266)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8016_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
extern MethodInfo IList_1_get_Item_m44881_MethodInfo;
extern MethodInfo IList_1_set_Item_m44882_MethodInfo;
static PropertyInfo IList_1_t8016____Item_PropertyInfo = 
{
	&IList_1_t8016_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44881_MethodInfo/* get */
	, &IList_1_set_Item_m44882_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8016_PropertyInfos[] =
{
	&IList_1_t8016____Item_PropertyInfo,
	NULL
};
extern Il2CppType IInitializePotentialDragHandler_t278_0_0_0;
static ParameterInfo IList_1_t8016_IList_1_IndexOf_m44883_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IInitializePotentialDragHandler_t278_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44883_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44883_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8016_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8016_IList_1_IndexOf_m44883_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44883_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IInitializePotentialDragHandler_t278_0_0_0;
static ParameterInfo IList_1_t8016_IList_1_Insert_m44884_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IInitializePotentialDragHandler_t278_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44884_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44884_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8016_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8016_IList_1_Insert_m44884_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44884_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8016_IList_1_RemoveAt_m44885_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44885_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44885_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8016_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8016_IList_1_RemoveAt_m44885_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44885_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8016_IList_1_get_Item_m44881_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IInitializePotentialDragHandler_t278_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44881_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44881_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8016_il2cpp_TypeInfo/* declaring_type */
	, &IInitializePotentialDragHandler_t278_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8016_IList_1_get_Item_m44881_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44881_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IInitializePotentialDragHandler_t278_0_0_0;
static ParameterInfo IList_1_t8016_IList_1_set_Item_m44882_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IInitializePotentialDragHandler_t278_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44882_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44882_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8016_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8016_IList_1_set_Item_m44882_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44882_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8016_MethodInfos[] =
{
	&IList_1_IndexOf_m44883_MethodInfo,
	&IList_1_Insert_m44884_MethodInfo,
	&IList_1_RemoveAt_m44885_MethodInfo,
	&IList_1_get_Item_m44881_MethodInfo,
	&IList_1_set_Item_m44882_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8016_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8015_il2cpp_TypeInfo,
	&IEnumerable_1_t8017_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8016_0_0_0;
extern Il2CppType IList_1_t8016_1_0_0;
struct IList_1_t8016;
extern Il2CppGenericClass IList_1_t8016_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8016_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8016_MethodInfos/* methods */
	, IList_1_t8016_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8016_il2cpp_TypeInfo/* element_class */
	, IList_1_t8016_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8016_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8016_0_0_0/* byval_arg */
	, &IList_1_t8016_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8016_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8018_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>
extern MethodInfo ICollection_1_get_Count_m44886_MethodInfo;
static PropertyInfo ICollection_1_t8018____Count_PropertyInfo = 
{
	&ICollection_1_t8018_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44886_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44887_MethodInfo;
static PropertyInfo ICollection_1_t8018____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8018_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44887_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8018_PropertyInfos[] =
{
	&ICollection_1_t8018____Count_PropertyInfo,
	&ICollection_1_t8018____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44886_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44886_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8018_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44886_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44887_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44887_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8018_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44887_GenericMethod/* genericMethod */

};
extern Il2CppType IDragHandler_t280_0_0_0;
extern Il2CppType IDragHandler_t280_0_0_0;
static ParameterInfo ICollection_1_t8018_ICollection_1_Add_m44888_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IDragHandler_t280_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44888_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>::Add(T)
MethodInfo ICollection_1_Add_m44888_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8018_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8018_ICollection_1_Add_m44888_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44888_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44889_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>::Clear()
MethodInfo ICollection_1_Clear_m44889_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8018_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44889_GenericMethod/* genericMethod */

};
extern Il2CppType IDragHandler_t280_0_0_0;
static ParameterInfo ICollection_1_t8018_ICollection_1_Contains_m44890_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IDragHandler_t280_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44890_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44890_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8018_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8018_ICollection_1_Contains_m44890_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44890_GenericMethod/* genericMethod */

};
extern Il2CppType IDragHandlerU5BU5D_t5796_0_0_0;
extern Il2CppType IDragHandlerU5BU5D_t5796_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8018_ICollection_1_CopyTo_m44891_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IDragHandlerU5BU5D_t5796_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44891_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44891_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8018_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8018_ICollection_1_CopyTo_m44891_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44891_GenericMethod/* genericMethod */

};
extern Il2CppType IDragHandler_t280_0_0_0;
static ParameterInfo ICollection_1_t8018_ICollection_1_Remove_m44892_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IDragHandler_t280_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44892_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDragHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44892_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8018_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8018_ICollection_1_Remove_m44892_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44892_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8018_MethodInfos[] =
{
	&ICollection_1_get_Count_m44886_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44887_MethodInfo,
	&ICollection_1_Add_m44888_MethodInfo,
	&ICollection_1_Clear_m44889_MethodInfo,
	&ICollection_1_Contains_m44890_MethodInfo,
	&ICollection_1_CopyTo_m44891_MethodInfo,
	&ICollection_1_Remove_m44892_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8020_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8018_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8020_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8018_0_0_0;
extern Il2CppType ICollection_1_t8018_1_0_0;
struct ICollection_1_t8018;
extern Il2CppGenericClass ICollection_1_t8018_GenericClass;
TypeInfo ICollection_1_t8018_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8018_MethodInfos/* methods */
	, ICollection_1_t8018_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8018_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8018_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8018_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8018_0_0_0/* byval_arg */
	, &ICollection_1_t8018_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8018_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IDragHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IDragHandler>
extern Il2CppType IEnumerator_1_t6280_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44893_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IDragHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44893_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8020_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6280_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44893_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8020_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44893_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8020_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8020_0_0_0;
extern Il2CppType IEnumerable_1_t8020_1_0_0;
struct IEnumerable_1_t8020;
extern Il2CppGenericClass IEnumerable_1_t8020_GenericClass;
TypeInfo IEnumerable_1_t8020_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8020_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8020_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8020_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8020_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8020_0_0_0/* byval_arg */
	, &IEnumerable_1_t8020_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8020_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6280_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IDragHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IDragHandler>
extern MethodInfo IEnumerator_1_get_Current_m44894_MethodInfo;
static PropertyInfo IEnumerator_1_t6280____Current_PropertyInfo = 
{
	&IEnumerator_1_t6280_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44894_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6280_PropertyInfos[] =
{
	&IEnumerator_1_t6280____Current_PropertyInfo,
	NULL
};
extern Il2CppType IDragHandler_t280_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44894_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IDragHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44894_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6280_il2cpp_TypeInfo/* declaring_type */
	, &IDragHandler_t280_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44894_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6280_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44894_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6280_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6280_0_0_0;
extern Il2CppType IEnumerator_1_t6280_1_0_0;
struct IEnumerator_1_t6280;
extern Il2CppGenericClass IEnumerator_1_t6280_GenericClass;
TypeInfo IEnumerator_1_t6280_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6280_MethodInfos/* methods */
	, IEnumerator_1_t6280_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6280_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6280_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6280_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6280_0_0_0/* byval_arg */
	, &IEnumerator_1_t6280_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6280_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDragHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_178.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3267_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDragHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_178MethodDeclarations.h"

extern TypeInfo IDragHandler_t280_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16922_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIDragHandler_t280_m34405_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IDragHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IDragHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIDragHandler_t280_m34405(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDragHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDragHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDragHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDragHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDragHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDragHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3267____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3267_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3267, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3267____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3267_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3267, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3267_FieldInfos[] =
{
	&InternalEnumerator_1_t3267____array_0_FieldInfo,
	&InternalEnumerator_1_t3267____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16919_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3267____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3267_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16919_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3267____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3267_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16922_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3267_PropertyInfos[] =
{
	&InternalEnumerator_1_t3267____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3267____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3267_InternalEnumerator_1__ctor_m16918_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16918_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDragHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16918_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3267_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3267_InternalEnumerator_1__ctor_m16918_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16918_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16919_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDragHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16919_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3267_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16919_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16920_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDragHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16920_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3267_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16920_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16921_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDragHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16921_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3267_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16921_GenericMethod/* genericMethod */

};
extern Il2CppType IDragHandler_t280_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16922_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDragHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16922_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3267_il2cpp_TypeInfo/* declaring_type */
	, &IDragHandler_t280_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16922_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3267_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16918_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16919_MethodInfo,
	&InternalEnumerator_1_Dispose_m16920_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16921_MethodInfo,
	&InternalEnumerator_1_get_Current_m16922_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16921_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16920_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3267_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16919_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16921_MethodInfo,
	&InternalEnumerator_1_Dispose_m16920_MethodInfo,
	&InternalEnumerator_1_get_Current_m16922_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3267_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6280_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3267_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6280_il2cpp_TypeInfo, 7},
};
extern TypeInfo IDragHandler_t280_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3267_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16922_MethodInfo/* Method Usage */,
	&IDragHandler_t280_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIDragHandler_t280_m34405_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3267_0_0_0;
extern Il2CppType InternalEnumerator_1_t3267_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3267_GenericClass;
TypeInfo InternalEnumerator_1_t3267_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3267_MethodInfos/* methods */
	, InternalEnumerator_1_t3267_PropertyInfos/* properties */
	, InternalEnumerator_1_t3267_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3267_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3267_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3267_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3267_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3267_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3267_1_0_0/* this_arg */
	, InternalEnumerator_1_t3267_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3267_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3267_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3267)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8019_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDragHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDragHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDragHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDragHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDragHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDragHandler>
extern MethodInfo IList_1_get_Item_m44895_MethodInfo;
extern MethodInfo IList_1_set_Item_m44896_MethodInfo;
static PropertyInfo IList_1_t8019____Item_PropertyInfo = 
{
	&IList_1_t8019_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44895_MethodInfo/* get */
	, &IList_1_set_Item_m44896_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8019_PropertyInfos[] =
{
	&IList_1_t8019____Item_PropertyInfo,
	NULL
};
extern Il2CppType IDragHandler_t280_0_0_0;
static ParameterInfo IList_1_t8019_IList_1_IndexOf_m44897_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IDragHandler_t280_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44897_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDragHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44897_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8019_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8019_IList_1_IndexOf_m44897_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44897_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IDragHandler_t280_0_0_0;
static ParameterInfo IList_1_t8019_IList_1_Insert_m44898_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IDragHandler_t280_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44898_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDragHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44898_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8019_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8019_IList_1_Insert_m44898_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44898_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8019_IList_1_RemoveAt_m44899_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44899_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDragHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44899_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8019_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8019_IList_1_RemoveAt_m44899_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44899_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8019_IList_1_get_Item_m44895_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IDragHandler_t280_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44895_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDragHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44895_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8019_il2cpp_TypeInfo/* declaring_type */
	, &IDragHandler_t280_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8019_IList_1_get_Item_m44895_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44895_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IDragHandler_t280_0_0_0;
static ParameterInfo IList_1_t8019_IList_1_set_Item_m44896_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IDragHandler_t280_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44896_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDragHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44896_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8019_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8019_IList_1_set_Item_m44896_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44896_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8019_MethodInfos[] =
{
	&IList_1_IndexOf_m44897_MethodInfo,
	&IList_1_Insert_m44898_MethodInfo,
	&IList_1_RemoveAt_m44899_MethodInfo,
	&IList_1_get_Item_m44895_MethodInfo,
	&IList_1_set_Item_m44896_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8019_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8018_il2cpp_TypeInfo,
	&IEnumerable_1_t8020_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8019_0_0_0;
extern Il2CppType IList_1_t8019_1_0_0;
struct IList_1_t8019;
extern Il2CppGenericClass IList_1_t8019_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8019_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8019_MethodInfos/* methods */
	, IList_1_t8019_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8019_il2cpp_TypeInfo/* element_class */
	, IList_1_t8019_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8019_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8019_0_0_0/* byval_arg */
	, &IList_1_t8019_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8019_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8021_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>
extern MethodInfo ICollection_1_get_Count_m44900_MethodInfo;
static PropertyInfo ICollection_1_t8021____Count_PropertyInfo = 
{
	&ICollection_1_t8021_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44900_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44901_MethodInfo;
static PropertyInfo ICollection_1_t8021____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8021_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44901_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8021_PropertyInfos[] =
{
	&ICollection_1_t8021____Count_PropertyInfo,
	&ICollection_1_t8021____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44900_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44900_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8021_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44900_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44901_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44901_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8021_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44901_GenericMethod/* genericMethod */

};
extern Il2CppType IEndDragHandler_t281_0_0_0;
extern Il2CppType IEndDragHandler_t281_0_0_0;
static ParameterInfo ICollection_1_t8021_ICollection_1_Add_m44902_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEndDragHandler_t281_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44902_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>::Add(T)
MethodInfo ICollection_1_Add_m44902_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8021_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8021_ICollection_1_Add_m44902_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44902_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44903_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>::Clear()
MethodInfo ICollection_1_Clear_m44903_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8021_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44903_GenericMethod/* genericMethod */

};
extern Il2CppType IEndDragHandler_t281_0_0_0;
static ParameterInfo ICollection_1_t8021_ICollection_1_Contains_m44904_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEndDragHandler_t281_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44904_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44904_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8021_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8021_ICollection_1_Contains_m44904_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44904_GenericMethod/* genericMethod */

};
extern Il2CppType IEndDragHandlerU5BU5D_t5797_0_0_0;
extern Il2CppType IEndDragHandlerU5BU5D_t5797_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8021_ICollection_1_CopyTo_m44905_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEndDragHandlerU5BU5D_t5797_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44905_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44905_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8021_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8021_ICollection_1_CopyTo_m44905_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44905_GenericMethod/* genericMethod */

};
extern Il2CppType IEndDragHandler_t281_0_0_0;
static ParameterInfo ICollection_1_t8021_ICollection_1_Remove_m44906_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEndDragHandler_t281_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44906_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IEndDragHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44906_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8021_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8021_ICollection_1_Remove_m44906_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44906_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8021_MethodInfos[] =
{
	&ICollection_1_get_Count_m44900_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44901_MethodInfo,
	&ICollection_1_Add_m44902_MethodInfo,
	&ICollection_1_Clear_m44903_MethodInfo,
	&ICollection_1_Contains_m44904_MethodInfo,
	&ICollection_1_CopyTo_m44905_MethodInfo,
	&ICollection_1_Remove_m44906_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8023_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8021_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8023_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8021_0_0_0;
extern Il2CppType ICollection_1_t8021_1_0_0;
struct ICollection_1_t8021;
extern Il2CppGenericClass ICollection_1_t8021_GenericClass;
TypeInfo ICollection_1_t8021_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8021_MethodInfos/* methods */
	, ICollection_1_t8021_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8021_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8021_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8021_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8021_0_0_0/* byval_arg */
	, &ICollection_1_t8021_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8021_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IEndDragHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IEndDragHandler>
extern Il2CppType IEnumerator_1_t6282_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44907_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IEndDragHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44907_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8023_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6282_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44907_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8023_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44907_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8023_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8023_0_0_0;
extern Il2CppType IEnumerable_1_t8023_1_0_0;
struct IEnumerable_1_t8023;
extern Il2CppGenericClass IEnumerable_1_t8023_GenericClass;
TypeInfo IEnumerable_1_t8023_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8023_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8023_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8023_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8023_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8023_0_0_0/* byval_arg */
	, &IEnumerable_1_t8023_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8023_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6282_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>
extern MethodInfo IEnumerator_1_get_Current_m44908_MethodInfo;
static PropertyInfo IEnumerator_1_t6282____Current_PropertyInfo = 
{
	&IEnumerator_1_t6282_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44908_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6282_PropertyInfos[] =
{
	&IEnumerator_1_t6282____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEndDragHandler_t281_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44908_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44908_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6282_il2cpp_TypeInfo/* declaring_type */
	, &IEndDragHandler_t281_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44908_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6282_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44908_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6282_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6282_0_0_0;
extern Il2CppType IEnumerator_1_t6282_1_0_0;
struct IEnumerator_1_t6282;
extern Il2CppGenericClass IEnumerator_1_t6282_GenericClass;
TypeInfo IEnumerator_1_t6282_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6282_MethodInfos/* methods */
	, IEnumerator_1_t6282_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6282_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6282_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6282_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6282_0_0_0/* byval_arg */
	, &IEnumerator_1_t6282_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6282_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_179.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3268_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_179MethodDeclarations.h"

extern TypeInfo IEndDragHandler_t281_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16927_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEndDragHandler_t281_m34416_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IEndDragHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IEndDragHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIEndDragHandler_t281_m34416(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3268____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3268_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3268, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3268____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3268_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3268, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3268_FieldInfos[] =
{
	&InternalEnumerator_1_t3268____array_0_FieldInfo,
	&InternalEnumerator_1_t3268____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16924_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3268____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3268_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16924_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3268____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3268_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16927_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3268_PropertyInfos[] =
{
	&InternalEnumerator_1_t3268____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3268____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3268_InternalEnumerator_1__ctor_m16923_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16923_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16923_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3268_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3268_InternalEnumerator_1__ctor_m16923_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16923_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16924_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16924_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3268_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16924_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16925_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16925_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3268_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16925_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16926_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16926_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3268_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16926_GenericMethod/* genericMethod */

};
extern Il2CppType IEndDragHandler_t281_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16927_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IEndDragHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16927_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3268_il2cpp_TypeInfo/* declaring_type */
	, &IEndDragHandler_t281_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16927_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3268_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16923_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16924_MethodInfo,
	&InternalEnumerator_1_Dispose_m16925_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16926_MethodInfo,
	&InternalEnumerator_1_get_Current_m16927_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16926_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16925_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3268_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16924_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16926_MethodInfo,
	&InternalEnumerator_1_Dispose_m16925_MethodInfo,
	&InternalEnumerator_1_get_Current_m16927_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3268_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6282_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3268_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6282_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEndDragHandler_t281_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3268_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16927_MethodInfo/* Method Usage */,
	&IEndDragHandler_t281_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEndDragHandler_t281_m34416_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3268_0_0_0;
extern Il2CppType InternalEnumerator_1_t3268_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3268_GenericClass;
TypeInfo InternalEnumerator_1_t3268_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3268_MethodInfos/* methods */
	, InternalEnumerator_1_t3268_PropertyInfos/* properties */
	, InternalEnumerator_1_t3268_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3268_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3268_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3268_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3268_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3268_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3268_1_0_0/* this_arg */
	, InternalEnumerator_1_t3268_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3268_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3268_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3268)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8022_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEndDragHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEndDragHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEndDragHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEndDragHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEndDragHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEndDragHandler>
extern MethodInfo IList_1_get_Item_m44909_MethodInfo;
extern MethodInfo IList_1_set_Item_m44910_MethodInfo;
static PropertyInfo IList_1_t8022____Item_PropertyInfo = 
{
	&IList_1_t8022_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44909_MethodInfo/* get */
	, &IList_1_set_Item_m44910_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8022_PropertyInfos[] =
{
	&IList_1_t8022____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEndDragHandler_t281_0_0_0;
static ParameterInfo IList_1_t8022_IList_1_IndexOf_m44911_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEndDragHandler_t281_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44911_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEndDragHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44911_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8022_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8022_IList_1_IndexOf_m44911_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44911_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEndDragHandler_t281_0_0_0;
static ParameterInfo IList_1_t8022_IList_1_Insert_m44912_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEndDragHandler_t281_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44912_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEndDragHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44912_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8022_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8022_IList_1_Insert_m44912_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44912_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8022_IList_1_RemoveAt_m44913_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44913_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEndDragHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44913_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8022_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8022_IList_1_RemoveAt_m44913_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44913_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8022_IList_1_get_Item_m44909_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEndDragHandler_t281_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44909_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEndDragHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44909_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8022_il2cpp_TypeInfo/* declaring_type */
	, &IEndDragHandler_t281_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8022_IList_1_get_Item_m44909_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44909_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEndDragHandler_t281_0_0_0;
static ParameterInfo IList_1_t8022_IList_1_set_Item_m44910_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEndDragHandler_t281_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44910_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IEndDragHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44910_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8022_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8022_IList_1_set_Item_m44910_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44910_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8022_MethodInfos[] =
{
	&IList_1_IndexOf_m44911_MethodInfo,
	&IList_1_Insert_m44912_MethodInfo,
	&IList_1_RemoveAt_m44913_MethodInfo,
	&IList_1_get_Item_m44909_MethodInfo,
	&IList_1_set_Item_m44910_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8022_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8021_il2cpp_TypeInfo,
	&IEnumerable_1_t8023_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8022_0_0_0;
extern Il2CppType IList_1_t8022_1_0_0;
struct IList_1_t8022;
extern Il2CppGenericClass IList_1_t8022_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8022_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8022_MethodInfos/* methods */
	, IList_1_t8022_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8022_il2cpp_TypeInfo/* element_class */
	, IList_1_t8022_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8022_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8022_0_0_0/* byval_arg */
	, &IList_1_t8022_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8022_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8024_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>
extern MethodInfo ICollection_1_get_Count_m44914_MethodInfo;
static PropertyInfo ICollection_1_t8024____Count_PropertyInfo = 
{
	&ICollection_1_t8024_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44914_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44915_MethodInfo;
static PropertyInfo ICollection_1_t8024____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8024_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44915_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8024_PropertyInfos[] =
{
	&ICollection_1_t8024____Count_PropertyInfo,
	&ICollection_1_t8024____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44914_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44914_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8024_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44914_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44915_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44915_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8024_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44915_GenericMethod/* genericMethod */

};
extern Il2CppType IDropHandler_t282_0_0_0;
extern Il2CppType IDropHandler_t282_0_0_0;
static ParameterInfo ICollection_1_t8024_ICollection_1_Add_m44916_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IDropHandler_t282_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44916_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>::Add(T)
MethodInfo ICollection_1_Add_m44916_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8024_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8024_ICollection_1_Add_m44916_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44916_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44917_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>::Clear()
MethodInfo ICollection_1_Clear_m44917_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8024_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44917_GenericMethod/* genericMethod */

};
extern Il2CppType IDropHandler_t282_0_0_0;
static ParameterInfo ICollection_1_t8024_ICollection_1_Contains_m44918_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IDropHandler_t282_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44918_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44918_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8024_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8024_ICollection_1_Contains_m44918_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44918_GenericMethod/* genericMethod */

};
extern Il2CppType IDropHandlerU5BU5D_t5798_0_0_0;
extern Il2CppType IDropHandlerU5BU5D_t5798_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8024_ICollection_1_CopyTo_m44919_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IDropHandlerU5BU5D_t5798_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44919_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44919_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8024_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8024_ICollection_1_CopyTo_m44919_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44919_GenericMethod/* genericMethod */

};
extern Il2CppType IDropHandler_t282_0_0_0;
static ParameterInfo ICollection_1_t8024_ICollection_1_Remove_m44920_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IDropHandler_t282_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44920_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDropHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44920_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8024_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8024_ICollection_1_Remove_m44920_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44920_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8024_MethodInfos[] =
{
	&ICollection_1_get_Count_m44914_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44915_MethodInfo,
	&ICollection_1_Add_m44916_MethodInfo,
	&ICollection_1_Clear_m44917_MethodInfo,
	&ICollection_1_Contains_m44918_MethodInfo,
	&ICollection_1_CopyTo_m44919_MethodInfo,
	&ICollection_1_Remove_m44920_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8026_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8024_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8026_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8024_0_0_0;
extern Il2CppType ICollection_1_t8024_1_0_0;
struct ICollection_1_t8024;
extern Il2CppGenericClass ICollection_1_t8024_GenericClass;
TypeInfo ICollection_1_t8024_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8024_MethodInfos/* methods */
	, ICollection_1_t8024_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8024_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8024_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8024_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8024_0_0_0/* byval_arg */
	, &ICollection_1_t8024_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8024_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IDropHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IDropHandler>
extern Il2CppType IEnumerator_1_t6284_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44921_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IDropHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44921_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8026_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44921_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8026_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44921_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8026_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8026_0_0_0;
extern Il2CppType IEnumerable_1_t8026_1_0_0;
struct IEnumerable_1_t8026;
extern Il2CppGenericClass IEnumerable_1_t8026_GenericClass;
TypeInfo IEnumerable_1_t8026_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8026_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8026_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8026_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8026_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8026_0_0_0/* byval_arg */
	, &IEnumerable_1_t8026_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8026_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6284_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IDropHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IDropHandler>
extern MethodInfo IEnumerator_1_get_Current_m44922_MethodInfo;
static PropertyInfo IEnumerator_1_t6284____Current_PropertyInfo = 
{
	&IEnumerator_1_t6284_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44922_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6284_PropertyInfos[] =
{
	&IEnumerator_1_t6284____Current_PropertyInfo,
	NULL
};
extern Il2CppType IDropHandler_t282_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44922_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IDropHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44922_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6284_il2cpp_TypeInfo/* declaring_type */
	, &IDropHandler_t282_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44922_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6284_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44922_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6284_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6284_0_0_0;
extern Il2CppType IEnumerator_1_t6284_1_0_0;
struct IEnumerator_1_t6284;
extern Il2CppGenericClass IEnumerator_1_t6284_GenericClass;
TypeInfo IEnumerator_1_t6284_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6284_MethodInfos/* methods */
	, IEnumerator_1_t6284_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6284_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6284_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6284_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6284_0_0_0/* byval_arg */
	, &IEnumerator_1_t6284_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6284_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDropHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_180.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3269_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDropHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_180MethodDeclarations.h"

extern TypeInfo IDropHandler_t282_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16932_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIDropHandler_t282_m34427_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IDropHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IDropHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIDropHandler_t282_m34427(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDropHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDropHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDropHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDropHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDropHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDropHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3269____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3269_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3269, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3269____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3269_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3269, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3269_FieldInfos[] =
{
	&InternalEnumerator_1_t3269____array_0_FieldInfo,
	&InternalEnumerator_1_t3269____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16929_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3269____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3269_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16929_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3269____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3269_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16932_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3269_PropertyInfos[] =
{
	&InternalEnumerator_1_t3269____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3269____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3269_InternalEnumerator_1__ctor_m16928_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16928_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDropHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16928_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3269_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3269_InternalEnumerator_1__ctor_m16928_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16928_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16929_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDropHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16929_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3269_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16929_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16930_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDropHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16930_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3269_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16930_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16931_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDropHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16931_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3269_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16931_GenericMethod/* genericMethod */

};
extern Il2CppType IDropHandler_t282_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16932_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDropHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16932_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3269_il2cpp_TypeInfo/* declaring_type */
	, &IDropHandler_t282_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16932_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3269_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16928_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16929_MethodInfo,
	&InternalEnumerator_1_Dispose_m16930_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16931_MethodInfo,
	&InternalEnumerator_1_get_Current_m16932_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16931_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16930_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3269_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16929_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16931_MethodInfo,
	&InternalEnumerator_1_Dispose_m16930_MethodInfo,
	&InternalEnumerator_1_get_Current_m16932_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3269_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6284_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3269_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6284_il2cpp_TypeInfo, 7},
};
extern TypeInfo IDropHandler_t282_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3269_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16932_MethodInfo/* Method Usage */,
	&IDropHandler_t282_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIDropHandler_t282_m34427_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3269_0_0_0;
extern Il2CppType InternalEnumerator_1_t3269_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3269_GenericClass;
TypeInfo InternalEnumerator_1_t3269_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3269_MethodInfos/* methods */
	, InternalEnumerator_1_t3269_PropertyInfos/* properties */
	, InternalEnumerator_1_t3269_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3269_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3269_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3269_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3269_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3269_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3269_1_0_0/* this_arg */
	, InternalEnumerator_1_t3269_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3269_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3269_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3269)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8025_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDropHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDropHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDropHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDropHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDropHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDropHandler>
extern MethodInfo IList_1_get_Item_m44923_MethodInfo;
extern MethodInfo IList_1_set_Item_m44924_MethodInfo;
static PropertyInfo IList_1_t8025____Item_PropertyInfo = 
{
	&IList_1_t8025_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44923_MethodInfo/* get */
	, &IList_1_set_Item_m44924_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8025_PropertyInfos[] =
{
	&IList_1_t8025____Item_PropertyInfo,
	NULL
};
extern Il2CppType IDropHandler_t282_0_0_0;
static ParameterInfo IList_1_t8025_IList_1_IndexOf_m44925_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IDropHandler_t282_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44925_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDropHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44925_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8025_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8025_IList_1_IndexOf_m44925_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44925_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IDropHandler_t282_0_0_0;
static ParameterInfo IList_1_t8025_IList_1_Insert_m44926_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IDropHandler_t282_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44926_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDropHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44926_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8025_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8025_IList_1_Insert_m44926_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44926_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8025_IList_1_RemoveAt_m44927_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44927_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDropHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44927_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8025_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8025_IList_1_RemoveAt_m44927_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44927_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8025_IList_1_get_Item_m44923_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IDropHandler_t282_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44923_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDropHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44923_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8025_il2cpp_TypeInfo/* declaring_type */
	, &IDropHandler_t282_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8025_IList_1_get_Item_m44923_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44923_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IDropHandler_t282_0_0_0;
static ParameterInfo IList_1_t8025_IList_1_set_Item_m44924_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IDropHandler_t282_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44924_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDropHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44924_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8025_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8025_IList_1_set_Item_m44924_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44924_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8025_MethodInfos[] =
{
	&IList_1_IndexOf_m44925_MethodInfo,
	&IList_1_Insert_m44926_MethodInfo,
	&IList_1_RemoveAt_m44927_MethodInfo,
	&IList_1_get_Item_m44923_MethodInfo,
	&IList_1_set_Item_m44924_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8025_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8024_il2cpp_TypeInfo,
	&IEnumerable_1_t8026_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8025_0_0_0;
extern Il2CppType IList_1_t8025_1_0_0;
struct IList_1_t8025;
extern Il2CppGenericClass IList_1_t8025_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8025_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8025_MethodInfos/* methods */
	, IList_1_t8025_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8025_il2cpp_TypeInfo/* element_class */
	, IList_1_t8025_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8025_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8025_0_0_0/* byval_arg */
	, &IList_1_t8025_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8025_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8027_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>
extern MethodInfo ICollection_1_get_Count_m44928_MethodInfo;
static PropertyInfo ICollection_1_t8027____Count_PropertyInfo = 
{
	&ICollection_1_t8027_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44928_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44929_MethodInfo;
static PropertyInfo ICollection_1_t8027____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8027_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44929_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8027_PropertyInfos[] =
{
	&ICollection_1_t8027____Count_PropertyInfo,
	&ICollection_1_t8027____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44928_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44928_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8027_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44928_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44929_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44929_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8027_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44929_GenericMethod/* genericMethod */

};
extern Il2CppType IScrollHandler_t283_0_0_0;
extern Il2CppType IScrollHandler_t283_0_0_0;
static ParameterInfo ICollection_1_t8027_ICollection_1_Add_m44930_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IScrollHandler_t283_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44930_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>::Add(T)
MethodInfo ICollection_1_Add_m44930_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8027_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8027_ICollection_1_Add_m44930_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44930_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44931_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>::Clear()
MethodInfo ICollection_1_Clear_m44931_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8027_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44931_GenericMethod/* genericMethod */

};
extern Il2CppType IScrollHandler_t283_0_0_0;
static ParameterInfo ICollection_1_t8027_ICollection_1_Contains_m44932_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IScrollHandler_t283_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44932_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44932_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8027_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8027_ICollection_1_Contains_m44932_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44932_GenericMethod/* genericMethod */

};
extern Il2CppType IScrollHandlerU5BU5D_t5799_0_0_0;
extern Il2CppType IScrollHandlerU5BU5D_t5799_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8027_ICollection_1_CopyTo_m44933_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IScrollHandlerU5BU5D_t5799_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44933_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44933_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8027_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8027_ICollection_1_CopyTo_m44933_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44933_GenericMethod/* genericMethod */

};
extern Il2CppType IScrollHandler_t283_0_0_0;
static ParameterInfo ICollection_1_t8027_ICollection_1_Remove_m44934_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IScrollHandler_t283_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44934_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IScrollHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44934_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8027_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8027_ICollection_1_Remove_m44934_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44934_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8027_MethodInfos[] =
{
	&ICollection_1_get_Count_m44928_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44929_MethodInfo,
	&ICollection_1_Add_m44930_MethodInfo,
	&ICollection_1_Clear_m44931_MethodInfo,
	&ICollection_1_Contains_m44932_MethodInfo,
	&ICollection_1_CopyTo_m44933_MethodInfo,
	&ICollection_1_Remove_m44934_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8029_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8027_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8029_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8027_0_0_0;
extern Il2CppType ICollection_1_t8027_1_0_0;
struct ICollection_1_t8027;
extern Il2CppGenericClass ICollection_1_t8027_GenericClass;
TypeInfo ICollection_1_t8027_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8027_MethodInfos/* methods */
	, ICollection_1_t8027_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8027_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8027_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8027_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8027_0_0_0/* byval_arg */
	, &ICollection_1_t8027_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8027_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IScrollHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IScrollHandler>
extern Il2CppType IEnumerator_1_t6286_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44935_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IScrollHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44935_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8029_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6286_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44935_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8029_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44935_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8029_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8029_0_0_0;
extern Il2CppType IEnumerable_1_t8029_1_0_0;
struct IEnumerable_1_t8029;
extern Il2CppGenericClass IEnumerable_1_t8029_GenericClass;
TypeInfo IEnumerable_1_t8029_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8029_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8029_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8029_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8029_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8029_0_0_0/* byval_arg */
	, &IEnumerable_1_t8029_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8029_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6286_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IScrollHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IScrollHandler>
extern MethodInfo IEnumerator_1_get_Current_m44936_MethodInfo;
static PropertyInfo IEnumerator_1_t6286____Current_PropertyInfo = 
{
	&IEnumerator_1_t6286_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44936_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6286_PropertyInfos[] =
{
	&IEnumerator_1_t6286____Current_PropertyInfo,
	NULL
};
extern Il2CppType IScrollHandler_t283_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44936_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IScrollHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44936_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6286_il2cpp_TypeInfo/* declaring_type */
	, &IScrollHandler_t283_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44936_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6286_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44936_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6286_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6286_0_0_0;
extern Il2CppType IEnumerator_1_t6286_1_0_0;
struct IEnumerator_1_t6286;
extern Il2CppGenericClass IEnumerator_1_t6286_GenericClass;
TypeInfo IEnumerator_1_t6286_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6286_MethodInfos/* methods */
	, IEnumerator_1_t6286_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6286_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6286_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6286_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6286_0_0_0/* byval_arg */
	, &IEnumerator_1_t6286_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6286_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IScrollHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_181.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3270_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IScrollHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_181MethodDeclarations.h"

extern TypeInfo IScrollHandler_t283_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16937_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIScrollHandler_t283_m34438_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IScrollHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IScrollHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIScrollHandler_t283_m34438(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IScrollHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IScrollHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IScrollHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IScrollHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IScrollHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IScrollHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3270____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3270_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3270, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3270____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3270_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3270, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3270_FieldInfos[] =
{
	&InternalEnumerator_1_t3270____array_0_FieldInfo,
	&InternalEnumerator_1_t3270____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16934_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3270____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3270_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16934_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3270____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3270_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16937_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3270_PropertyInfos[] =
{
	&InternalEnumerator_1_t3270____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3270____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3270_InternalEnumerator_1__ctor_m16933_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16933_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IScrollHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16933_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3270_InternalEnumerator_1__ctor_m16933_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16933_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16934_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IScrollHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16934_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3270_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16934_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16935_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IScrollHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16935_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16935_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16936_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IScrollHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16936_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3270_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16936_GenericMethod/* genericMethod */

};
extern Il2CppType IScrollHandler_t283_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16937_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IScrollHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16937_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3270_il2cpp_TypeInfo/* declaring_type */
	, &IScrollHandler_t283_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16937_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3270_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16933_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16934_MethodInfo,
	&InternalEnumerator_1_Dispose_m16935_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16936_MethodInfo,
	&InternalEnumerator_1_get_Current_m16937_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16936_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16935_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3270_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16934_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16936_MethodInfo,
	&InternalEnumerator_1_Dispose_m16935_MethodInfo,
	&InternalEnumerator_1_get_Current_m16937_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3270_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6286_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3270_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6286_il2cpp_TypeInfo, 7},
};
extern TypeInfo IScrollHandler_t283_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3270_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16937_MethodInfo/* Method Usage */,
	&IScrollHandler_t283_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIScrollHandler_t283_m34438_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3270_0_0_0;
extern Il2CppType InternalEnumerator_1_t3270_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3270_GenericClass;
TypeInfo InternalEnumerator_1_t3270_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3270_MethodInfos/* methods */
	, InternalEnumerator_1_t3270_PropertyInfos/* properties */
	, InternalEnumerator_1_t3270_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3270_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3270_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3270_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3270_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3270_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3270_1_0_0/* this_arg */
	, InternalEnumerator_1_t3270_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3270_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3270_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3270)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8028_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IScrollHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IScrollHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IScrollHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IScrollHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IScrollHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IScrollHandler>
extern MethodInfo IList_1_get_Item_m44937_MethodInfo;
extern MethodInfo IList_1_set_Item_m44938_MethodInfo;
static PropertyInfo IList_1_t8028____Item_PropertyInfo = 
{
	&IList_1_t8028_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44937_MethodInfo/* get */
	, &IList_1_set_Item_m44938_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8028_PropertyInfos[] =
{
	&IList_1_t8028____Item_PropertyInfo,
	NULL
};
extern Il2CppType IScrollHandler_t283_0_0_0;
static ParameterInfo IList_1_t8028_IList_1_IndexOf_m44939_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IScrollHandler_t283_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44939_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IScrollHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44939_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8028_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8028_IList_1_IndexOf_m44939_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44939_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IScrollHandler_t283_0_0_0;
static ParameterInfo IList_1_t8028_IList_1_Insert_m44940_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IScrollHandler_t283_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44940_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IScrollHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44940_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8028_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8028_IList_1_Insert_m44940_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44940_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8028_IList_1_RemoveAt_m44941_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44941_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IScrollHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44941_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8028_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8028_IList_1_RemoveAt_m44941_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44941_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8028_IList_1_get_Item_m44937_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IScrollHandler_t283_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44937_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IScrollHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44937_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8028_il2cpp_TypeInfo/* declaring_type */
	, &IScrollHandler_t283_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8028_IList_1_get_Item_m44937_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44937_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IScrollHandler_t283_0_0_0;
static ParameterInfo IList_1_t8028_IList_1_set_Item_m44938_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IScrollHandler_t283_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44938_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IScrollHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44938_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8028_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8028_IList_1_set_Item_m44938_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44938_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8028_MethodInfos[] =
{
	&IList_1_IndexOf_m44939_MethodInfo,
	&IList_1_Insert_m44940_MethodInfo,
	&IList_1_RemoveAt_m44941_MethodInfo,
	&IList_1_get_Item_m44937_MethodInfo,
	&IList_1_set_Item_m44938_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8028_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8027_il2cpp_TypeInfo,
	&IEnumerable_1_t8029_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8028_0_0_0;
extern Il2CppType IList_1_t8028_1_0_0;
struct IList_1_t8028;
extern Il2CppGenericClass IList_1_t8028_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8028_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8028_MethodInfos/* methods */
	, IList_1_t8028_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8028_il2cpp_TypeInfo/* element_class */
	, IList_1_t8028_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8028_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8028_0_0_0/* byval_arg */
	, &IList_1_t8028_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8028_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8030_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
extern MethodInfo ICollection_1_get_Count_m44942_MethodInfo;
static PropertyInfo ICollection_1_t8030____Count_PropertyInfo = 
{
	&ICollection_1_t8030_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44942_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44943_MethodInfo;
static PropertyInfo ICollection_1_t8030____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8030_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44943_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8030_PropertyInfos[] =
{
	&ICollection_1_t8030____Count_PropertyInfo,
	&ICollection_1_t8030____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44942_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44942_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8030_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44942_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44943_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44943_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8030_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44943_GenericMethod/* genericMethod */

};
extern Il2CppType IUpdateSelectedHandler_t284_0_0_0;
extern Il2CppType IUpdateSelectedHandler_t284_0_0_0;
static ParameterInfo ICollection_1_t8030_ICollection_1_Add_m44944_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IUpdateSelectedHandler_t284_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44944_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::Add(T)
MethodInfo ICollection_1_Add_m44944_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8030_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8030_ICollection_1_Add_m44944_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44944_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44945_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::Clear()
MethodInfo ICollection_1_Clear_m44945_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8030_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44945_GenericMethod/* genericMethod */

};
extern Il2CppType IUpdateSelectedHandler_t284_0_0_0;
static ParameterInfo ICollection_1_t8030_ICollection_1_Contains_m44946_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IUpdateSelectedHandler_t284_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44946_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44946_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8030_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8030_ICollection_1_Contains_m44946_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44946_GenericMethod/* genericMethod */

};
extern Il2CppType IUpdateSelectedHandlerU5BU5D_t5800_0_0_0;
extern Il2CppType IUpdateSelectedHandlerU5BU5D_t5800_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8030_ICollection_1_CopyTo_m44947_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IUpdateSelectedHandlerU5BU5D_t5800_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44947_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44947_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8030_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8030_ICollection_1_CopyTo_m44947_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44947_GenericMethod/* genericMethod */

};
extern Il2CppType IUpdateSelectedHandler_t284_0_0_0;
static ParameterInfo ICollection_1_t8030_ICollection_1_Remove_m44948_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IUpdateSelectedHandler_t284_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44948_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44948_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8030_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8030_ICollection_1_Remove_m44948_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44948_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8030_MethodInfos[] =
{
	&ICollection_1_get_Count_m44942_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44943_MethodInfo,
	&ICollection_1_Add_m44944_MethodInfo,
	&ICollection_1_Clear_m44945_MethodInfo,
	&ICollection_1_Contains_m44946_MethodInfo,
	&ICollection_1_CopyTo_m44947_MethodInfo,
	&ICollection_1_Remove_m44948_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8032_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8030_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8032_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8030_0_0_0;
extern Il2CppType ICollection_1_t8030_1_0_0;
struct ICollection_1_t8030;
extern Il2CppGenericClass ICollection_1_t8030_GenericClass;
TypeInfo ICollection_1_t8030_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8030_MethodInfos/* methods */
	, ICollection_1_t8030_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8030_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8030_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8030_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8030_0_0_0/* byval_arg */
	, &ICollection_1_t8030_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8030_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
extern Il2CppType IEnumerator_1_t6288_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44949_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44949_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8032_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6288_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44949_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8032_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44949_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8032_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8032_0_0_0;
extern Il2CppType IEnumerable_1_t8032_1_0_0;
struct IEnumerable_1_t8032;
extern Il2CppGenericClass IEnumerable_1_t8032_GenericClass;
TypeInfo IEnumerable_1_t8032_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8032_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8032_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8032_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8032_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8032_0_0_0/* byval_arg */
	, &IEnumerable_1_t8032_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8032_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6288_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
extern MethodInfo IEnumerator_1_get_Current_m44950_MethodInfo;
static PropertyInfo IEnumerator_1_t6288____Current_PropertyInfo = 
{
	&IEnumerator_1_t6288_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44950_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6288_PropertyInfos[] =
{
	&IEnumerator_1_t6288____Current_PropertyInfo,
	NULL
};
extern Il2CppType IUpdateSelectedHandler_t284_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44950_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44950_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6288_il2cpp_TypeInfo/* declaring_type */
	, &IUpdateSelectedHandler_t284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44950_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6288_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44950_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6288_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6288_0_0_0;
extern Il2CppType IEnumerator_1_t6288_1_0_0;
struct IEnumerator_1_t6288;
extern Il2CppGenericClass IEnumerator_1_t6288_GenericClass;
TypeInfo IEnumerator_1_t6288_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6288_MethodInfos/* methods */
	, IEnumerator_1_t6288_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6288_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6288_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6288_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6288_0_0_0/* byval_arg */
	, &IEnumerator_1_t6288_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6288_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_182.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3271_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_182MethodDeclarations.h"

extern TypeInfo IUpdateSelectedHandler_t284_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16942_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIUpdateSelectedHandler_t284_m34449_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IUpdateSelectedHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IUpdateSelectedHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIUpdateSelectedHandler_t284_m34449(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3271____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3271_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3271, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3271____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3271_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3271, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3271_FieldInfos[] =
{
	&InternalEnumerator_1_t3271____array_0_FieldInfo,
	&InternalEnumerator_1_t3271____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16939_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3271____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3271_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16939_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3271____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3271_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16942_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3271_PropertyInfos[] =
{
	&InternalEnumerator_1_t3271____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3271____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3271_InternalEnumerator_1__ctor_m16938_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16938_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16938_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3271_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3271_InternalEnumerator_1__ctor_m16938_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16938_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16939_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16939_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3271_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16939_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16940_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16940_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3271_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16940_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16941_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16941_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3271_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16941_GenericMethod/* genericMethod */

};
extern Il2CppType IUpdateSelectedHandler_t284_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16942_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16942_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3271_il2cpp_TypeInfo/* declaring_type */
	, &IUpdateSelectedHandler_t284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16942_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3271_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16938_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16939_MethodInfo,
	&InternalEnumerator_1_Dispose_m16940_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16941_MethodInfo,
	&InternalEnumerator_1_get_Current_m16942_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16941_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16940_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3271_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16939_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16941_MethodInfo,
	&InternalEnumerator_1_Dispose_m16940_MethodInfo,
	&InternalEnumerator_1_get_Current_m16942_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3271_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6288_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3271_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6288_il2cpp_TypeInfo, 7},
};
extern TypeInfo IUpdateSelectedHandler_t284_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3271_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16942_MethodInfo/* Method Usage */,
	&IUpdateSelectedHandler_t284_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIUpdateSelectedHandler_t284_m34449_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3271_0_0_0;
extern Il2CppType InternalEnumerator_1_t3271_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3271_GenericClass;
TypeInfo InternalEnumerator_1_t3271_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3271_MethodInfos/* methods */
	, InternalEnumerator_1_t3271_PropertyInfos/* properties */
	, InternalEnumerator_1_t3271_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3271_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3271_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3271_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3271_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3271_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3271_1_0_0/* this_arg */
	, InternalEnumerator_1_t3271_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3271_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3271_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3271)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8031_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
extern MethodInfo IList_1_get_Item_m44951_MethodInfo;
extern MethodInfo IList_1_set_Item_m44952_MethodInfo;
static PropertyInfo IList_1_t8031____Item_PropertyInfo = 
{
	&IList_1_t8031_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44951_MethodInfo/* get */
	, &IList_1_set_Item_m44952_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8031_PropertyInfos[] =
{
	&IList_1_t8031____Item_PropertyInfo,
	NULL
};
extern Il2CppType IUpdateSelectedHandler_t284_0_0_0;
static ParameterInfo IList_1_t8031_IList_1_IndexOf_m44953_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IUpdateSelectedHandler_t284_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44953_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44953_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8031_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8031_IList_1_IndexOf_m44953_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44953_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IUpdateSelectedHandler_t284_0_0_0;
static ParameterInfo IList_1_t8031_IList_1_Insert_m44954_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IUpdateSelectedHandler_t284_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44954_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44954_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8031_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8031_IList_1_Insert_m44954_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44954_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8031_IList_1_RemoveAt_m44955_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44955_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44955_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8031_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8031_IList_1_RemoveAt_m44955_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44955_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8031_IList_1_get_Item_m44951_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IUpdateSelectedHandler_t284_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44951_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44951_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8031_il2cpp_TypeInfo/* declaring_type */
	, &IUpdateSelectedHandler_t284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8031_IList_1_get_Item_m44951_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44951_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IUpdateSelectedHandler_t284_0_0_0;
static ParameterInfo IList_1_t8031_IList_1_set_Item_m44952_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IUpdateSelectedHandler_t284_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44952_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IUpdateSelectedHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44952_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8031_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8031_IList_1_set_Item_m44952_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44952_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8031_MethodInfos[] =
{
	&IList_1_IndexOf_m44953_MethodInfo,
	&IList_1_Insert_m44954_MethodInfo,
	&IList_1_RemoveAt_m44955_MethodInfo,
	&IList_1_get_Item_m44951_MethodInfo,
	&IList_1_set_Item_m44952_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8031_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8030_il2cpp_TypeInfo,
	&IEnumerable_1_t8032_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8031_0_0_0;
extern Il2CppType IList_1_t8031_1_0_0;
struct IList_1_t8031;
extern Il2CppGenericClass IList_1_t8031_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8031_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8031_MethodInfos/* methods */
	, IList_1_t8031_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8031_il2cpp_TypeInfo/* element_class */
	, IList_1_t8031_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8031_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8031_0_0_0/* byval_arg */
	, &IList_1_t8031_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8031_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8033_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>
extern MethodInfo ICollection_1_get_Count_m44956_MethodInfo;
static PropertyInfo ICollection_1_t8033____Count_PropertyInfo = 
{
	&ICollection_1_t8033_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44956_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44957_MethodInfo;
static PropertyInfo ICollection_1_t8033____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8033_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44957_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8033_PropertyInfos[] =
{
	&ICollection_1_t8033____Count_PropertyInfo,
	&ICollection_1_t8033____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44956_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44956_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8033_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44956_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44957_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44957_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8033_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44957_GenericMethod/* genericMethod */

};
extern Il2CppType ISelectHandler_t285_0_0_0;
extern Il2CppType ISelectHandler_t285_0_0_0;
static ParameterInfo ICollection_1_t8033_ICollection_1_Add_m44958_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISelectHandler_t285_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44958_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>::Add(T)
MethodInfo ICollection_1_Add_m44958_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8033_ICollection_1_Add_m44958_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44958_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44959_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>::Clear()
MethodInfo ICollection_1_Clear_m44959_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44959_GenericMethod/* genericMethod */

};
extern Il2CppType ISelectHandler_t285_0_0_0;
static ParameterInfo ICollection_1_t8033_ICollection_1_Contains_m44960_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISelectHandler_t285_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44960_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44960_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8033_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8033_ICollection_1_Contains_m44960_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44960_GenericMethod/* genericMethod */

};
extern Il2CppType ISelectHandlerU5BU5D_t5801_0_0_0;
extern Il2CppType ISelectHandlerU5BU5D_t5801_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8033_ICollection_1_CopyTo_m44961_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ISelectHandlerU5BU5D_t5801_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44961_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44961_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8033_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8033_ICollection_1_CopyTo_m44961_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44961_GenericMethod/* genericMethod */

};
extern Il2CppType ISelectHandler_t285_0_0_0;
static ParameterInfo ICollection_1_t8033_ICollection_1_Remove_m44962_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISelectHandler_t285_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44962_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISelectHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44962_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8033_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8033_ICollection_1_Remove_m44962_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44962_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8033_MethodInfos[] =
{
	&ICollection_1_get_Count_m44956_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44957_MethodInfo,
	&ICollection_1_Add_m44958_MethodInfo,
	&ICollection_1_Clear_m44959_MethodInfo,
	&ICollection_1_Contains_m44960_MethodInfo,
	&ICollection_1_CopyTo_m44961_MethodInfo,
	&ICollection_1_Remove_m44962_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8035_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8033_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8035_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8033_0_0_0;
extern Il2CppType ICollection_1_t8033_1_0_0;
struct ICollection_1_t8033;
extern Il2CppGenericClass ICollection_1_t8033_GenericClass;
TypeInfo ICollection_1_t8033_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8033_MethodInfos/* methods */
	, ICollection_1_t8033_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8033_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8033_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8033_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8033_0_0_0/* byval_arg */
	, &ICollection_1_t8033_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8033_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.ISelectHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.ISelectHandler>
extern Il2CppType IEnumerator_1_t6290_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44963_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.ISelectHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44963_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8035_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6290_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44963_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8035_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44963_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8035_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8035_0_0_0;
extern Il2CppType IEnumerable_1_t8035_1_0_0;
struct IEnumerable_1_t8035;
extern Il2CppGenericClass IEnumerable_1_t8035_GenericClass;
TypeInfo IEnumerable_1_t8035_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8035_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8035_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8035_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8035_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8035_0_0_0/* byval_arg */
	, &IEnumerable_1_t8035_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8035_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6290_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.ISelectHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.ISelectHandler>
extern MethodInfo IEnumerator_1_get_Current_m44964_MethodInfo;
static PropertyInfo IEnumerator_1_t6290____Current_PropertyInfo = 
{
	&IEnumerator_1_t6290_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44964_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6290_PropertyInfos[] =
{
	&IEnumerator_1_t6290____Current_PropertyInfo,
	NULL
};
extern Il2CppType ISelectHandler_t285_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44964_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.ISelectHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44964_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6290_il2cpp_TypeInfo/* declaring_type */
	, &ISelectHandler_t285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44964_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6290_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44964_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6290_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6290_0_0_0;
extern Il2CppType IEnumerator_1_t6290_1_0_0;
struct IEnumerator_1_t6290;
extern Il2CppGenericClass IEnumerator_1_t6290_GenericClass;
TypeInfo IEnumerator_1_t6290_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6290_MethodInfos/* methods */
	, IEnumerator_1_t6290_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6290_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6290_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6290_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6290_0_0_0/* byval_arg */
	, &IEnumerator_1_t6290_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6290_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISelectHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_183.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3272_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISelectHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_183MethodDeclarations.h"

extern TypeInfo ISelectHandler_t285_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16947_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisISelectHandler_t285_m34460_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.ISelectHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.ISelectHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisISelectHandler_t285_m34460(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISelectHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISelectHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISelectHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISelectHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISelectHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISelectHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3272____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3272_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3272, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3272____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3272_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3272, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3272_FieldInfos[] =
{
	&InternalEnumerator_1_t3272____array_0_FieldInfo,
	&InternalEnumerator_1_t3272____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16944_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3272____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3272_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16944_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3272____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3272_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16947_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3272_PropertyInfos[] =
{
	&InternalEnumerator_1_t3272____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3272____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3272_InternalEnumerator_1__ctor_m16943_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16943_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISelectHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16943_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3272_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3272_InternalEnumerator_1__ctor_m16943_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16943_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16944_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISelectHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16944_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3272_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16944_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16945_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISelectHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16945_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3272_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16945_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16946_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISelectHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16946_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3272_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16946_GenericMethod/* genericMethod */

};
extern Il2CppType ISelectHandler_t285_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16947_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISelectHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16947_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3272_il2cpp_TypeInfo/* declaring_type */
	, &ISelectHandler_t285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16947_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3272_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16943_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16944_MethodInfo,
	&InternalEnumerator_1_Dispose_m16945_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16946_MethodInfo,
	&InternalEnumerator_1_get_Current_m16947_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16946_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16945_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3272_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16944_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16946_MethodInfo,
	&InternalEnumerator_1_Dispose_m16945_MethodInfo,
	&InternalEnumerator_1_get_Current_m16947_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3272_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6290_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3272_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6290_il2cpp_TypeInfo, 7},
};
extern TypeInfo ISelectHandler_t285_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3272_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16947_MethodInfo/* Method Usage */,
	&ISelectHandler_t285_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisISelectHandler_t285_m34460_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3272_0_0_0;
extern Il2CppType InternalEnumerator_1_t3272_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3272_GenericClass;
TypeInfo InternalEnumerator_1_t3272_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3272_MethodInfos/* methods */
	, InternalEnumerator_1_t3272_PropertyInfos/* properties */
	, InternalEnumerator_1_t3272_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3272_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3272_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3272_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3272_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3272_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3272_1_0_0/* this_arg */
	, InternalEnumerator_1_t3272_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3272_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3272_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3272)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8034_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISelectHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISelectHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISelectHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISelectHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISelectHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISelectHandler>
extern MethodInfo IList_1_get_Item_m44965_MethodInfo;
extern MethodInfo IList_1_set_Item_m44966_MethodInfo;
static PropertyInfo IList_1_t8034____Item_PropertyInfo = 
{
	&IList_1_t8034_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44965_MethodInfo/* get */
	, &IList_1_set_Item_m44966_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8034_PropertyInfos[] =
{
	&IList_1_t8034____Item_PropertyInfo,
	NULL
};
extern Il2CppType ISelectHandler_t285_0_0_0;
static ParameterInfo IList_1_t8034_IList_1_IndexOf_m44967_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISelectHandler_t285_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44967_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISelectHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44967_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8034_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8034_IList_1_IndexOf_m44967_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44967_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ISelectHandler_t285_0_0_0;
static ParameterInfo IList_1_t8034_IList_1_Insert_m44968_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ISelectHandler_t285_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44968_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISelectHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44968_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8034_IList_1_Insert_m44968_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44968_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8034_IList_1_RemoveAt_m44969_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44969_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISelectHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44969_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8034_IList_1_RemoveAt_m44969_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44969_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8034_IList_1_get_Item_m44965_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ISelectHandler_t285_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44965_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISelectHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44965_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8034_il2cpp_TypeInfo/* declaring_type */
	, &ISelectHandler_t285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8034_IList_1_get_Item_m44965_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44965_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ISelectHandler_t285_0_0_0;
static ParameterInfo IList_1_t8034_IList_1_set_Item_m44966_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ISelectHandler_t285_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44966_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISelectHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44966_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8034_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8034_IList_1_set_Item_m44966_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44966_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8034_MethodInfos[] =
{
	&IList_1_IndexOf_m44967_MethodInfo,
	&IList_1_Insert_m44968_MethodInfo,
	&IList_1_RemoveAt_m44969_MethodInfo,
	&IList_1_get_Item_m44965_MethodInfo,
	&IList_1_set_Item_m44966_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8034_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8033_il2cpp_TypeInfo,
	&IEnumerable_1_t8035_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8034_0_0_0;
extern Il2CppType IList_1_t8034_1_0_0;
struct IList_1_t8034;
extern Il2CppGenericClass IList_1_t8034_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8034_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8034_MethodInfos/* methods */
	, IList_1_t8034_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8034_il2cpp_TypeInfo/* element_class */
	, IList_1_t8034_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8034_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8034_0_0_0/* byval_arg */
	, &IList_1_t8034_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8034_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8036_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>
extern MethodInfo ICollection_1_get_Count_m44970_MethodInfo;
static PropertyInfo ICollection_1_t8036____Count_PropertyInfo = 
{
	&ICollection_1_t8036_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44970_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44971_MethodInfo;
static PropertyInfo ICollection_1_t8036____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8036_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44971_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8036_PropertyInfos[] =
{
	&ICollection_1_t8036____Count_PropertyInfo,
	&ICollection_1_t8036____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44970_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44970_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8036_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44970_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44971_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44971_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8036_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44971_GenericMethod/* genericMethod */

};
extern Il2CppType IDeselectHandler_t286_0_0_0;
extern Il2CppType IDeselectHandler_t286_0_0_0;
static ParameterInfo ICollection_1_t8036_ICollection_1_Add_m44972_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IDeselectHandler_t286_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44972_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>::Add(T)
MethodInfo ICollection_1_Add_m44972_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8036_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8036_ICollection_1_Add_m44972_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44972_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44973_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>::Clear()
MethodInfo ICollection_1_Clear_m44973_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8036_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44973_GenericMethod/* genericMethod */

};
extern Il2CppType IDeselectHandler_t286_0_0_0;
static ParameterInfo ICollection_1_t8036_ICollection_1_Contains_m44974_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IDeselectHandler_t286_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44974_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44974_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8036_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8036_ICollection_1_Contains_m44974_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44974_GenericMethod/* genericMethod */

};
extern Il2CppType IDeselectHandlerU5BU5D_t5802_0_0_0;
extern Il2CppType IDeselectHandlerU5BU5D_t5802_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8036_ICollection_1_CopyTo_m44975_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IDeselectHandlerU5BU5D_t5802_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44975_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44975_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8036_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8036_ICollection_1_CopyTo_m44975_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44975_GenericMethod/* genericMethod */

};
extern Il2CppType IDeselectHandler_t286_0_0_0;
static ParameterInfo ICollection_1_t8036_ICollection_1_Remove_m44976_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IDeselectHandler_t286_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44976_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IDeselectHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44976_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8036_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8036_ICollection_1_Remove_m44976_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44976_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8036_MethodInfos[] =
{
	&ICollection_1_get_Count_m44970_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44971_MethodInfo,
	&ICollection_1_Add_m44972_MethodInfo,
	&ICollection_1_Clear_m44973_MethodInfo,
	&ICollection_1_Contains_m44974_MethodInfo,
	&ICollection_1_CopyTo_m44975_MethodInfo,
	&ICollection_1_Remove_m44976_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8038_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8036_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8038_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8036_0_0_0;
extern Il2CppType ICollection_1_t8036_1_0_0;
struct ICollection_1_t8036;
extern Il2CppGenericClass ICollection_1_t8036_GenericClass;
TypeInfo ICollection_1_t8036_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8036_MethodInfos/* methods */
	, ICollection_1_t8036_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8036_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8036_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8036_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8036_0_0_0/* byval_arg */
	, &ICollection_1_t8036_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8036_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IDeselectHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IDeselectHandler>
extern Il2CppType IEnumerator_1_t6292_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44977_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IDeselectHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44977_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8038_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6292_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44977_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8038_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44977_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8038_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8038_0_0_0;
extern Il2CppType IEnumerable_1_t8038_1_0_0;
struct IEnumerable_1_t8038;
extern Il2CppGenericClass IEnumerable_1_t8038_GenericClass;
TypeInfo IEnumerable_1_t8038_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8038_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8038_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8038_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8038_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8038_0_0_0/* byval_arg */
	, &IEnumerable_1_t8038_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8038_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6292_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>
extern MethodInfo IEnumerator_1_get_Current_m44978_MethodInfo;
static PropertyInfo IEnumerator_1_t6292____Current_PropertyInfo = 
{
	&IEnumerator_1_t6292_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44978_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6292_PropertyInfos[] =
{
	&IEnumerator_1_t6292____Current_PropertyInfo,
	NULL
};
extern Il2CppType IDeselectHandler_t286_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44978_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44978_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6292_il2cpp_TypeInfo/* declaring_type */
	, &IDeselectHandler_t286_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44978_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6292_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44978_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6292_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6292_0_0_0;
extern Il2CppType IEnumerator_1_t6292_1_0_0;
struct IEnumerator_1_t6292;
extern Il2CppGenericClass IEnumerator_1_t6292_GenericClass;
TypeInfo IEnumerator_1_t6292_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6292_MethodInfos/* methods */
	, IEnumerator_1_t6292_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6292_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6292_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6292_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6292_0_0_0/* byval_arg */
	, &IEnumerator_1_t6292_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6292_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_184.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3273_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_184MethodDeclarations.h"

extern TypeInfo IDeselectHandler_t286_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16952_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIDeselectHandler_t286_m34471_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IDeselectHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IDeselectHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIDeselectHandler_t286_m34471(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3273____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3273_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3273, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3273____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3273_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3273, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3273_FieldInfos[] =
{
	&InternalEnumerator_1_t3273____array_0_FieldInfo,
	&InternalEnumerator_1_t3273____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16949_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3273____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3273_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16949_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3273____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3273_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16952_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3273_PropertyInfos[] =
{
	&InternalEnumerator_1_t3273____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3273____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3273_InternalEnumerator_1__ctor_m16948_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16948_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16948_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3273_InternalEnumerator_1__ctor_m16948_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16948_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16949_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16949_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3273_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16949_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16950_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16950_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16950_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16951_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16951_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3273_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16951_GenericMethod/* genericMethod */

};
extern Il2CppType IDeselectHandler_t286_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16952_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IDeselectHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16952_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3273_il2cpp_TypeInfo/* declaring_type */
	, &IDeselectHandler_t286_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16952_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3273_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16948_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16949_MethodInfo,
	&InternalEnumerator_1_Dispose_m16950_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16951_MethodInfo,
	&InternalEnumerator_1_get_Current_m16952_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16951_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16950_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3273_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16949_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16951_MethodInfo,
	&InternalEnumerator_1_Dispose_m16950_MethodInfo,
	&InternalEnumerator_1_get_Current_m16952_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3273_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6292_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3273_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6292_il2cpp_TypeInfo, 7},
};
extern TypeInfo IDeselectHandler_t286_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3273_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16952_MethodInfo/* Method Usage */,
	&IDeselectHandler_t286_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIDeselectHandler_t286_m34471_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3273_0_0_0;
extern Il2CppType InternalEnumerator_1_t3273_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3273_GenericClass;
TypeInfo InternalEnumerator_1_t3273_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3273_MethodInfos/* methods */
	, InternalEnumerator_1_t3273_PropertyInfos/* properties */
	, InternalEnumerator_1_t3273_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3273_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3273_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3273_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3273_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3273_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3273_1_0_0/* this_arg */
	, InternalEnumerator_1_t3273_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3273_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3273_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3273)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8037_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDeselectHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDeselectHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDeselectHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDeselectHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDeselectHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDeselectHandler>
extern MethodInfo IList_1_get_Item_m44979_MethodInfo;
extern MethodInfo IList_1_set_Item_m44980_MethodInfo;
static PropertyInfo IList_1_t8037____Item_PropertyInfo = 
{
	&IList_1_t8037_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44979_MethodInfo/* get */
	, &IList_1_set_Item_m44980_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8037_PropertyInfos[] =
{
	&IList_1_t8037____Item_PropertyInfo,
	NULL
};
extern Il2CppType IDeselectHandler_t286_0_0_0;
static ParameterInfo IList_1_t8037_IList_1_IndexOf_m44981_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IDeselectHandler_t286_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44981_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDeselectHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44981_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8037_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8037_IList_1_IndexOf_m44981_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44981_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IDeselectHandler_t286_0_0_0;
static ParameterInfo IList_1_t8037_IList_1_Insert_m44982_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IDeselectHandler_t286_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44982_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDeselectHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44982_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8037_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8037_IList_1_Insert_m44982_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44982_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8037_IList_1_RemoveAt_m44983_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44983_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDeselectHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44983_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8037_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8037_IList_1_RemoveAt_m44983_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44983_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8037_IList_1_get_Item_m44979_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IDeselectHandler_t286_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44979_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDeselectHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44979_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8037_il2cpp_TypeInfo/* declaring_type */
	, &IDeselectHandler_t286_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8037_IList_1_get_Item_m44979_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44979_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IDeselectHandler_t286_0_0_0;
static ParameterInfo IList_1_t8037_IList_1_set_Item_m44980_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IDeselectHandler_t286_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44980_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IDeselectHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44980_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8037_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8037_IList_1_set_Item_m44980_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44980_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8037_MethodInfos[] =
{
	&IList_1_IndexOf_m44981_MethodInfo,
	&IList_1_Insert_m44982_MethodInfo,
	&IList_1_RemoveAt_m44983_MethodInfo,
	&IList_1_get_Item_m44979_MethodInfo,
	&IList_1_set_Item_m44980_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8037_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8036_il2cpp_TypeInfo,
	&IEnumerable_1_t8038_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8037_0_0_0;
extern Il2CppType IList_1_t8037_1_0_0;
struct IList_1_t8037;
extern Il2CppGenericClass IList_1_t8037_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8037_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8037_MethodInfos/* methods */
	, IList_1_t8037_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8037_il2cpp_TypeInfo/* element_class */
	, IList_1_t8037_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8037_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8037_0_0_0/* byval_arg */
	, &IList_1_t8037_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8037_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8039_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>
extern MethodInfo ICollection_1_get_Count_m44984_MethodInfo;
static PropertyInfo ICollection_1_t8039____Count_PropertyInfo = 
{
	&ICollection_1_t8039_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44984_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44985_MethodInfo;
static PropertyInfo ICollection_1_t8039____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8039_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44985_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8039_PropertyInfos[] =
{
	&ICollection_1_t8039____Count_PropertyInfo,
	&ICollection_1_t8039____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44984_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44984_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8039_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44984_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44985_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44985_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8039_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44985_GenericMethod/* genericMethod */

};
extern Il2CppType IMoveHandler_t287_0_0_0;
extern Il2CppType IMoveHandler_t287_0_0_0;
static ParameterInfo ICollection_1_t8039_ICollection_1_Add_m44986_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMoveHandler_t287_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44986_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>::Add(T)
MethodInfo ICollection_1_Add_m44986_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8039_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8039_ICollection_1_Add_m44986_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44986_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44987_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>::Clear()
MethodInfo ICollection_1_Clear_m44987_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8039_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44987_GenericMethod/* genericMethod */

};
extern Il2CppType IMoveHandler_t287_0_0_0;
static ParameterInfo ICollection_1_t8039_ICollection_1_Contains_m44988_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMoveHandler_t287_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44988_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m44988_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8039_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8039_ICollection_1_Contains_m44988_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44988_GenericMethod/* genericMethod */

};
extern Il2CppType IMoveHandlerU5BU5D_t5803_0_0_0;
extern Il2CppType IMoveHandlerU5BU5D_t5803_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8039_ICollection_1_CopyTo_m44989_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IMoveHandlerU5BU5D_t5803_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44989_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44989_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8039_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8039_ICollection_1_CopyTo_m44989_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44989_GenericMethod/* genericMethod */

};
extern Il2CppType IMoveHandler_t287_0_0_0;
static ParameterInfo ICollection_1_t8039_ICollection_1_Remove_m44990_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMoveHandler_t287_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44990_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IMoveHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m44990_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8039_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8039_ICollection_1_Remove_m44990_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44990_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8039_MethodInfos[] =
{
	&ICollection_1_get_Count_m44984_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44985_MethodInfo,
	&ICollection_1_Add_m44986_MethodInfo,
	&ICollection_1_Clear_m44987_MethodInfo,
	&ICollection_1_Contains_m44988_MethodInfo,
	&ICollection_1_CopyTo_m44989_MethodInfo,
	&ICollection_1_Remove_m44990_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8041_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8039_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8041_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8039_0_0_0;
extern Il2CppType ICollection_1_t8039_1_0_0;
struct ICollection_1_t8039;
extern Il2CppGenericClass ICollection_1_t8039_GenericClass;
TypeInfo ICollection_1_t8039_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8039_MethodInfos/* methods */
	, ICollection_1_t8039_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8039_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8039_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8039_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8039_0_0_0/* byval_arg */
	, &ICollection_1_t8039_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8039_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IMoveHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IMoveHandler>
extern Il2CppType IEnumerator_1_t6294_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44991_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IMoveHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44991_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8041_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6294_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44991_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8041_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44991_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8041_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8041_0_0_0;
extern Il2CppType IEnumerable_1_t8041_1_0_0;
struct IEnumerable_1_t8041;
extern Il2CppGenericClass IEnumerable_1_t8041_GenericClass;
TypeInfo IEnumerable_1_t8041_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8041_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8041_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8041_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8041_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8041_0_0_0/* byval_arg */
	, &IEnumerable_1_t8041_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8041_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6294_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IMoveHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IMoveHandler>
extern MethodInfo IEnumerator_1_get_Current_m44992_MethodInfo;
static PropertyInfo IEnumerator_1_t6294____Current_PropertyInfo = 
{
	&IEnumerator_1_t6294_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44992_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6294_PropertyInfos[] =
{
	&IEnumerator_1_t6294____Current_PropertyInfo,
	NULL
};
extern Il2CppType IMoveHandler_t287_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44992_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IMoveHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44992_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6294_il2cpp_TypeInfo/* declaring_type */
	, &IMoveHandler_t287_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44992_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6294_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44992_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6294_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6294_0_0_0;
extern Il2CppType IEnumerator_1_t6294_1_0_0;
struct IEnumerator_1_t6294;
extern Il2CppGenericClass IEnumerator_1_t6294_GenericClass;
TypeInfo IEnumerator_1_t6294_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6294_MethodInfos/* methods */
	, IEnumerator_1_t6294_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6294_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6294_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6294_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6294_0_0_0/* byval_arg */
	, &IEnumerator_1_t6294_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6294_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IMoveHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_185.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3274_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IMoveHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_185MethodDeclarations.h"

extern TypeInfo IMoveHandler_t287_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16957_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIMoveHandler_t287_m34482_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IMoveHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.IMoveHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIMoveHandler_t287_m34482(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IMoveHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IMoveHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IMoveHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IMoveHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IMoveHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IMoveHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3274____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3274_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3274, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3274____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3274_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3274, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3274_FieldInfos[] =
{
	&InternalEnumerator_1_t3274____array_0_FieldInfo,
	&InternalEnumerator_1_t3274____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16954_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3274____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3274_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16954_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3274____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3274_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16957_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3274_PropertyInfos[] =
{
	&InternalEnumerator_1_t3274____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3274____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3274_InternalEnumerator_1__ctor_m16953_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16953_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IMoveHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16953_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3274_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3274_InternalEnumerator_1__ctor_m16953_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16953_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16954_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IMoveHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16954_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3274_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16954_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16955_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IMoveHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16955_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3274_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16955_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16956_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IMoveHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16956_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3274_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16956_GenericMethod/* genericMethod */

};
extern Il2CppType IMoveHandler_t287_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16957_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IMoveHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16957_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3274_il2cpp_TypeInfo/* declaring_type */
	, &IMoveHandler_t287_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16957_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3274_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16953_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16954_MethodInfo,
	&InternalEnumerator_1_Dispose_m16955_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16956_MethodInfo,
	&InternalEnumerator_1_get_Current_m16957_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16956_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16955_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3274_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16954_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16956_MethodInfo,
	&InternalEnumerator_1_Dispose_m16955_MethodInfo,
	&InternalEnumerator_1_get_Current_m16957_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3274_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6294_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3274_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6294_il2cpp_TypeInfo, 7},
};
extern TypeInfo IMoveHandler_t287_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3274_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16957_MethodInfo/* Method Usage */,
	&IMoveHandler_t287_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIMoveHandler_t287_m34482_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3274_0_0_0;
extern Il2CppType InternalEnumerator_1_t3274_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3274_GenericClass;
TypeInfo InternalEnumerator_1_t3274_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3274_MethodInfos/* methods */
	, InternalEnumerator_1_t3274_PropertyInfos/* properties */
	, InternalEnumerator_1_t3274_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3274_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3274_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3274_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3274_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3274_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3274_1_0_0/* this_arg */
	, InternalEnumerator_1_t3274_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3274_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3274_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3274)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8040_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IMoveHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IMoveHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IMoveHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IMoveHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IMoveHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IMoveHandler>
extern MethodInfo IList_1_get_Item_m44993_MethodInfo;
extern MethodInfo IList_1_set_Item_m44994_MethodInfo;
static PropertyInfo IList_1_t8040____Item_PropertyInfo = 
{
	&IList_1_t8040_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44993_MethodInfo/* get */
	, &IList_1_set_Item_m44994_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8040_PropertyInfos[] =
{
	&IList_1_t8040____Item_PropertyInfo,
	NULL
};
extern Il2CppType IMoveHandler_t287_0_0_0;
static ParameterInfo IList_1_t8040_IList_1_IndexOf_m44995_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMoveHandler_t287_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44995_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.IMoveHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44995_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8040_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8040_IList_1_IndexOf_m44995_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44995_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IMoveHandler_t287_0_0_0;
static ParameterInfo IList_1_t8040_IList_1_Insert_m44996_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IMoveHandler_t287_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44996_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IMoveHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44996_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8040_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8040_IList_1_Insert_m44996_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44996_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8040_IList_1_RemoveAt_m44997_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44997_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IMoveHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44997_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8040_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8040_IList_1_RemoveAt_m44997_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44997_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8040_IList_1_get_Item_m44993_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IMoveHandler_t287_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44993_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.IMoveHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44993_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8040_il2cpp_TypeInfo/* declaring_type */
	, &IMoveHandler_t287_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8040_IList_1_get_Item_m44993_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44993_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IMoveHandler_t287_0_0_0;
static ParameterInfo IList_1_t8040_IList_1_set_Item_m44994_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IMoveHandler_t287_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44994_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.IMoveHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44994_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8040_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8040_IList_1_set_Item_m44994_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44994_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8040_MethodInfos[] =
{
	&IList_1_IndexOf_m44995_MethodInfo,
	&IList_1_Insert_m44996_MethodInfo,
	&IList_1_RemoveAt_m44997_MethodInfo,
	&IList_1_get_Item_m44993_MethodInfo,
	&IList_1_set_Item_m44994_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8040_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8039_il2cpp_TypeInfo,
	&IEnumerable_1_t8041_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8040_0_0_0;
extern Il2CppType IList_1_t8040_1_0_0;
struct IList_1_t8040;
extern Il2CppGenericClass IList_1_t8040_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8040_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8040_MethodInfos/* methods */
	, IList_1_t8040_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8040_il2cpp_TypeInfo/* element_class */
	, IList_1_t8040_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8040_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8040_0_0_0/* byval_arg */
	, &IList_1_t8040_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8040_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8042_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>
extern MethodInfo ICollection_1_get_Count_m44998_MethodInfo;
static PropertyInfo ICollection_1_t8042____Count_PropertyInfo = 
{
	&ICollection_1_t8042_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44998_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44999_MethodInfo;
static PropertyInfo ICollection_1_t8042____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8042_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44999_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8042_PropertyInfos[] =
{
	&ICollection_1_t8042____Count_PropertyInfo,
	&ICollection_1_t8042____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44998_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m44998_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8042_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44998_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44999_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44999_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8042_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44999_GenericMethod/* genericMethod */

};
extern Il2CppType ISubmitHandler_t288_0_0_0;
extern Il2CppType ISubmitHandler_t288_0_0_0;
static ParameterInfo ICollection_1_t8042_ICollection_1_Add_m45000_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISubmitHandler_t288_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m45000_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>::Add(T)
MethodInfo ICollection_1_Add_m45000_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8042_ICollection_1_Add_m45000_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m45000_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m45001_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>::Clear()
MethodInfo ICollection_1_Clear_m45001_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m45001_GenericMethod/* genericMethod */

};
extern Il2CppType ISubmitHandler_t288_0_0_0;
static ParameterInfo ICollection_1_t8042_ICollection_1_Contains_m45002_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISubmitHandler_t288_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m45002_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m45002_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8042_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8042_ICollection_1_Contains_m45002_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m45002_GenericMethod/* genericMethod */

};
extern Il2CppType ISubmitHandlerU5BU5D_t5804_0_0_0;
extern Il2CppType ISubmitHandlerU5BU5D_t5804_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8042_ICollection_1_CopyTo_m45003_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ISubmitHandlerU5BU5D_t5804_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m45003_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m45003_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8042_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8042_ICollection_1_CopyTo_m45003_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m45003_GenericMethod/* genericMethod */

};
extern Il2CppType ISubmitHandler_t288_0_0_0;
static ParameterInfo ICollection_1_t8042_ICollection_1_Remove_m45004_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISubmitHandler_t288_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m45004_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ISubmitHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m45004_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8042_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8042_ICollection_1_Remove_m45004_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m45004_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8042_MethodInfos[] =
{
	&ICollection_1_get_Count_m44998_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44999_MethodInfo,
	&ICollection_1_Add_m45000_MethodInfo,
	&ICollection_1_Clear_m45001_MethodInfo,
	&ICollection_1_Contains_m45002_MethodInfo,
	&ICollection_1_CopyTo_m45003_MethodInfo,
	&ICollection_1_Remove_m45004_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8044_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8042_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8044_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8042_0_0_0;
extern Il2CppType ICollection_1_t8042_1_0_0;
struct ICollection_1_t8042;
extern Il2CppGenericClass ICollection_1_t8042_GenericClass;
TypeInfo ICollection_1_t8042_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8042_MethodInfos/* methods */
	, ICollection_1_t8042_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8042_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8042_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8042_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8042_0_0_0/* byval_arg */
	, &ICollection_1_t8042_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8042_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.ISubmitHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.ISubmitHandler>
extern Il2CppType IEnumerator_1_t6296_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m45005_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.ISubmitHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m45005_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8044_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6296_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m45005_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8044_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m45005_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8044_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8044_0_0_0;
extern Il2CppType IEnumerable_1_t8044_1_0_0;
struct IEnumerable_1_t8044;
extern Il2CppGenericClass IEnumerable_1_t8044_GenericClass;
TypeInfo IEnumerable_1_t8044_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8044_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8044_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8044_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8044_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8044_0_0_0/* byval_arg */
	, &IEnumerable_1_t8044_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8044_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6296_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>
extern MethodInfo IEnumerator_1_get_Current_m45006_MethodInfo;
static PropertyInfo IEnumerator_1_t6296____Current_PropertyInfo = 
{
	&IEnumerator_1_t6296_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m45006_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6296_PropertyInfos[] =
{
	&IEnumerator_1_t6296____Current_PropertyInfo,
	NULL
};
extern Il2CppType ISubmitHandler_t288_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m45006_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m45006_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6296_il2cpp_TypeInfo/* declaring_type */
	, &ISubmitHandler_t288_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m45006_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6296_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m45006_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6296_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6296_0_0_0;
extern Il2CppType IEnumerator_1_t6296_1_0_0;
struct IEnumerator_1_t6296;
extern Il2CppGenericClass IEnumerator_1_t6296_GenericClass;
TypeInfo IEnumerator_1_t6296_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6296_MethodInfos/* methods */
	, IEnumerator_1_t6296_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6296_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6296_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6296_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6296_0_0_0/* byval_arg */
	, &IEnumerator_1_t6296_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6296_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_186.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3275_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_186MethodDeclarations.h"

extern TypeInfo ISubmitHandler_t288_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16962_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisISubmitHandler_t288_m34493_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.ISubmitHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.ISubmitHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisISubmitHandler_t288_m34493(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3275____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3275_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3275, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3275____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3275_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3275, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3275_FieldInfos[] =
{
	&InternalEnumerator_1_t3275____array_0_FieldInfo,
	&InternalEnumerator_1_t3275____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16959_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3275____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3275_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16959_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3275____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3275_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16962_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3275_PropertyInfos[] =
{
	&InternalEnumerator_1_t3275____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3275____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3275_InternalEnumerator_1__ctor_m16958_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16958_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16958_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3275_InternalEnumerator_1__ctor_m16958_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16958_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16959_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16959_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3275_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16959_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16960_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16960_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16960_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16961_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16961_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3275_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16961_GenericMethod/* genericMethod */

};
extern Il2CppType ISubmitHandler_t288_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16962_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ISubmitHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16962_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3275_il2cpp_TypeInfo/* declaring_type */
	, &ISubmitHandler_t288_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16962_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3275_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16958_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16959_MethodInfo,
	&InternalEnumerator_1_Dispose_m16960_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16961_MethodInfo,
	&InternalEnumerator_1_get_Current_m16962_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16961_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16960_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3275_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16959_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16961_MethodInfo,
	&InternalEnumerator_1_Dispose_m16960_MethodInfo,
	&InternalEnumerator_1_get_Current_m16962_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3275_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6296_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3275_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6296_il2cpp_TypeInfo, 7},
};
extern TypeInfo ISubmitHandler_t288_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3275_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16962_MethodInfo/* Method Usage */,
	&ISubmitHandler_t288_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisISubmitHandler_t288_m34493_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3275_0_0_0;
extern Il2CppType InternalEnumerator_1_t3275_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3275_GenericClass;
TypeInfo InternalEnumerator_1_t3275_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3275_MethodInfos/* methods */
	, InternalEnumerator_1_t3275_PropertyInfos/* properties */
	, InternalEnumerator_1_t3275_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3275_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3275_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3275_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3275_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3275_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3275_1_0_0/* this_arg */
	, InternalEnumerator_1_t3275_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3275_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3275_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3275)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8043_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISubmitHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISubmitHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISubmitHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISubmitHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISubmitHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISubmitHandler>
extern MethodInfo IList_1_get_Item_m45007_MethodInfo;
extern MethodInfo IList_1_set_Item_m45008_MethodInfo;
static PropertyInfo IList_1_t8043____Item_PropertyInfo = 
{
	&IList_1_t8043_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m45007_MethodInfo/* get */
	, &IList_1_set_Item_m45008_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8043_PropertyInfos[] =
{
	&IList_1_t8043____Item_PropertyInfo,
	NULL
};
extern Il2CppType ISubmitHandler_t288_0_0_0;
static ParameterInfo IList_1_t8043_IList_1_IndexOf_m45009_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISubmitHandler_t288_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m45009_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISubmitHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m45009_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8043_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8043_IList_1_IndexOf_m45009_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m45009_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ISubmitHandler_t288_0_0_0;
static ParameterInfo IList_1_t8043_IList_1_Insert_m45010_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ISubmitHandler_t288_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m45010_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISubmitHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m45010_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8043_IList_1_Insert_m45010_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m45010_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8043_IList_1_RemoveAt_m45011_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m45011_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISubmitHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m45011_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t8043_IList_1_RemoveAt_m45011_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m45011_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t8043_IList_1_get_Item_m45007_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType ISubmitHandler_t288_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m45007_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISubmitHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m45007_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8043_il2cpp_TypeInfo/* declaring_type */
	, &ISubmitHandler_t288_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t8043_IList_1_get_Item_m45007_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m45007_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType ISubmitHandler_t288_0_0_0;
static ParameterInfo IList_1_t8043_IList_1_set_Item_m45008_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ISubmitHandler_t288_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m45008_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.EventSystems.ISubmitHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m45008_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8043_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t8043_IList_1_set_Item_m45008_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m45008_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8043_MethodInfos[] =
{
	&IList_1_IndexOf_m45009_MethodInfo,
	&IList_1_Insert_m45010_MethodInfo,
	&IList_1_RemoveAt_m45011_MethodInfo,
	&IList_1_get_Item_m45007_MethodInfo,
	&IList_1_set_Item_m45008_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8043_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t8042_il2cpp_TypeInfo,
	&IEnumerable_1_t8044_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8043_0_0_0;
extern Il2CppType IList_1_t8043_1_0_0;
struct IList_1_t8043;
extern Il2CppGenericClass IList_1_t8043_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t8043_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8043_MethodInfos/* methods */
	, IList_1_t8043_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8043_il2cpp_TypeInfo/* element_class */
	, IList_1_t8043_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8043_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8043_0_0_0/* byval_arg */
	, &IList_1_t8043_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8043_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8045_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>
extern MethodInfo ICollection_1_get_Count_m45012_MethodInfo;
static PropertyInfo ICollection_1_t8045____Count_PropertyInfo = 
{
	&ICollection_1_t8045_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m45012_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m45013_MethodInfo;
static PropertyInfo ICollection_1_t8045____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8045_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m45013_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8045_PropertyInfos[] =
{
	&ICollection_1_t8045____Count_PropertyInfo,
	&ICollection_1_t8045____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m45012_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m45012_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8045_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m45012_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m45013_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m45013_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8045_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m45013_GenericMethod/* genericMethod */

};
extern Il2CppType ICancelHandler_t289_0_0_0;
extern Il2CppType ICancelHandler_t289_0_0_0;
static ParameterInfo ICollection_1_t8045_ICollection_1_Add_m45014_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ICancelHandler_t289_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m45014_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>::Add(T)
MethodInfo ICollection_1_Add_m45014_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8045_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t8045_ICollection_1_Add_m45014_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m45014_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m45015_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>::Clear()
MethodInfo ICollection_1_Clear_m45015_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8045_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m45015_GenericMethod/* genericMethod */

};
extern Il2CppType ICancelHandler_t289_0_0_0;
static ParameterInfo ICollection_1_t8045_ICollection_1_Contains_m45016_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ICancelHandler_t289_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m45016_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m45016_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8045_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8045_ICollection_1_Contains_m45016_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m45016_GenericMethod/* genericMethod */

};
extern Il2CppType ICancelHandlerU5BU5D_t5805_0_0_0;
extern Il2CppType ICancelHandlerU5BU5D_t5805_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t8045_ICollection_1_CopyTo_m45017_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ICancelHandlerU5BU5D_t5805_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m45017_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m45017_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8045_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t8045_ICollection_1_CopyTo_m45017_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m45017_GenericMethod/* genericMethod */

};
extern Il2CppType ICancelHandler_t289_0_0_0;
static ParameterInfo ICollection_1_t8045_ICollection_1_Remove_m45018_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ICancelHandler_t289_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m45018_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.ICancelHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m45018_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8045_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t8045_ICollection_1_Remove_m45018_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m45018_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8045_MethodInfos[] =
{
	&ICollection_1_get_Count_m45012_MethodInfo,
	&ICollection_1_get_IsReadOnly_m45013_MethodInfo,
	&ICollection_1_Add_m45014_MethodInfo,
	&ICollection_1_Clear_m45015_MethodInfo,
	&ICollection_1_Contains_m45016_MethodInfo,
	&ICollection_1_CopyTo_m45017_MethodInfo,
	&ICollection_1_Remove_m45018_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8047_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8045_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t8047_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8045_0_0_0;
extern Il2CppType ICollection_1_t8045_1_0_0;
struct ICollection_1_t8045;
extern Il2CppGenericClass ICollection_1_t8045_GenericClass;
TypeInfo ICollection_1_t8045_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8045_MethodInfos/* methods */
	, ICollection_1_t8045_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8045_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8045_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8045_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8045_0_0_0/* byval_arg */
	, &ICollection_1_t8045_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8045_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.ICancelHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.ICancelHandler>
extern Il2CppType IEnumerator_1_t6298_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m45019_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.ICancelHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m45019_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8047_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6298_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m45019_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8047_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m45019_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8047_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8047_0_0_0;
extern Il2CppType IEnumerable_1_t8047_1_0_0;
struct IEnumerable_1_t8047;
extern Il2CppGenericClass IEnumerable_1_t8047_GenericClass;
TypeInfo IEnumerable_1_t8047_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8047_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8047_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8047_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8047_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8047_0_0_0/* byval_arg */
	, &IEnumerable_1_t8047_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8047_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6298_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.ICancelHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.ICancelHandler>
extern MethodInfo IEnumerator_1_get_Current_m45020_MethodInfo;
static PropertyInfo IEnumerator_1_t6298____Current_PropertyInfo = 
{
	&IEnumerator_1_t6298_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m45020_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6298_PropertyInfos[] =
{
	&IEnumerator_1_t6298____Current_PropertyInfo,
	NULL
};
extern Il2CppType ICancelHandler_t289_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m45020_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.ICancelHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m45020_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6298_il2cpp_TypeInfo/* declaring_type */
	, &ICancelHandler_t289_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m45020_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6298_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m45020_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6298_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6298_0_0_0;
extern Il2CppType IEnumerator_1_t6298_1_0_0;
struct IEnumerator_1_t6298;
extern Il2CppGenericClass IEnumerator_1_t6298_GenericClass;
TypeInfo IEnumerator_1_t6298_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6298_MethodInfos/* methods */
	, IEnumerator_1_t6298_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6298_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6298_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6298_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6298_0_0_0/* byval_arg */
	, &IEnumerator_1_t6298_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6298_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ICancelHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_187.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3276_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ICancelHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_187MethodDeclarations.h"

extern TypeInfo ICancelHandler_t289_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m16967_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisICancelHandler_t289_m34504_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.ICancelHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.ICancelHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisICancelHandler_t289_m34504(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ICancelHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ICancelHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ICancelHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ICancelHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ICancelHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ICancelHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3276____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3276_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3276, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3276____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3276_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3276, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3276_FieldInfos[] =
{
	&InternalEnumerator_1_t3276____array_0_FieldInfo,
	&InternalEnumerator_1_t3276____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16964_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3276____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3276_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16964_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3276____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3276_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m16967_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3276_PropertyInfos[] =
{
	&InternalEnumerator_1_t3276____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3276____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3276_InternalEnumerator_1__ctor_m16963_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m16963_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ICancelHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m16963_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3276_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3276_InternalEnumerator_1__ctor_m16963_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m16963_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16964_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ICancelHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16964_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3276_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16964_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m16965_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ICancelHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m16965_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3276_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m16965_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m16966_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ICancelHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m16966_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3276_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m16966_GenericMethod/* genericMethod */

};
extern Il2CppType ICancelHandler_t289_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m16967_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.ICancelHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m16967_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3276_il2cpp_TypeInfo/* declaring_type */
	, &ICancelHandler_t289_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m16967_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3276_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m16963_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16964_MethodInfo,
	&InternalEnumerator_1_Dispose_m16965_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16966_MethodInfo,
	&InternalEnumerator_1_get_Current_m16967_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m16966_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m16965_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3276_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16964_MethodInfo,
	&InternalEnumerator_1_MoveNext_m16966_MethodInfo,
	&InternalEnumerator_1_Dispose_m16965_MethodInfo,
	&InternalEnumerator_1_get_Current_m16967_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3276_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6298_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3276_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6298_il2cpp_TypeInfo, 7},
};
extern TypeInfo ICancelHandler_t289_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3276_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m16967_MethodInfo/* Method Usage */,
	&ICancelHandler_t289_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisICancelHandler_t289_m34504_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3276_0_0_0;
extern Il2CppType InternalEnumerator_1_t3276_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3276_GenericClass;
TypeInfo InternalEnumerator_1_t3276_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3276_MethodInfos/* methods */
	, InternalEnumerator_1_t3276_PropertyInfos/* properties */
	, InternalEnumerator_1_t3276_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3276_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3276_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3276_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3276_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3276_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3276_1_0_0/* this_arg */
	, InternalEnumerator_1_t3276_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3276_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3276_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3276)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
