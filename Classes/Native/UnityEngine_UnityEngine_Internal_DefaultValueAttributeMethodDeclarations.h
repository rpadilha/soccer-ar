﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Internal.DefaultValueAttribute
struct DefaultValueAttribute_t1146;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
 void DefaultValueAttribute__ctor_m6568 (DefaultValueAttribute_t1146 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
 Object_t * DefaultValueAttribute_get_Value_m6569 (DefaultValueAttribute_t1146 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
 bool DefaultValueAttribute_Equals_m6570 (DefaultValueAttribute_t1146 * __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
 int32_t DefaultValueAttribute_GetHashCode_m6571 (DefaultValueAttribute_t1146 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
