﻿#pragma once
#include <stdint.h>
// Sphere
struct Sphere_t71;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Banda_Script
struct Banda_Script_t72  : public MonoBehaviour_t10
{
	// Sphere Banda_Script::sphere
	Sphere_t71 * ___sphere_2;
	// UnityEngine.Vector3 Banda_Script::direction_throwin
	Vector3_t73  ___direction_throwin_3;
};
