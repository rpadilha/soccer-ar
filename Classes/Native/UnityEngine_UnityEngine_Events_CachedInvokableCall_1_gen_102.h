﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_104.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>
struct CachedInvokableCall_1_t3784  : public InvokableCall_1_t3785
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
