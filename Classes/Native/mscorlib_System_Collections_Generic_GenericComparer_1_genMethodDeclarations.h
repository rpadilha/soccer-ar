﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct GenericComparer_1_t2711;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTime>::.ctor()
 void GenericComparer_1__ctor_m14123 (GenericComparer_1_t2711 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTime>::Compare(T,T)
 int32_t GenericComparer_1_Compare_m32100 (GenericComparer_1_t2711 * __this, DateTime_t674  ___x, DateTime_t674  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
