﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.FileWebRequestCreator
struct FileWebRequestCreator_t1379;
// System.Net.WebRequest
struct WebRequest_t1374;
// System.Uri
struct Uri_t1375;

// System.Void System.Net.FileWebRequestCreator::.ctor()
 void FileWebRequestCreator__ctor_m6989 (FileWebRequestCreator_t1379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.FileWebRequestCreator::Create(System.Uri)
 WebRequest_t1374 * FileWebRequestCreator_Create_m6990 (FileWebRequestCreator_t1379 * __this, Uri_t1375 * ___uri, MethodInfo* method) IL2CPP_METHOD_ATTR;
