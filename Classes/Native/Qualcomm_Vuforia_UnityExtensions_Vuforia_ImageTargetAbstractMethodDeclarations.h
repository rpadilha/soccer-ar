﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t23;
// Vuforia.ImageTarget
struct ImageTarget_t616;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t597;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t30;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t29;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonAbstractBehaviour>
struct IEnumerable_1_t795;
// Vuforia.VirtualButton
struct VirtualButton_t639;
// UnityEngine.Transform
struct Transform_t74;
// Vuforia.ImageTargetType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetType.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// Vuforia.ImageTarget Vuforia.ImageTargetAbstractBehaviour::get_ImageTarget()
 Object_t * ImageTargetAbstractBehaviour_get_ImageTarget_m4246 (ImageTargetAbstractBehaviour_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::.ctor()
 void ImageTargetAbstractBehaviour__ctor_m291 (ImageTargetAbstractBehaviour_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetAbstractBehaviour::CorrectScaleImpl()
 bool ImageTargetAbstractBehaviour_CorrectScaleImpl_m298 (ImageTargetAbstractBehaviour_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::InternalUnregisterTrackable()
 void ImageTargetAbstractBehaviour_InternalUnregisterTrackable_m297 (ImageTargetAbstractBehaviour_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
 void ImageTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m299 (ImageTargetAbstractBehaviour_t23 * __this, Vector3_t73 * ___boundsMin, Vector3_t73 * ___boundsMax, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget)
 void ImageTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m300 (ImageTargetAbstractBehaviour_t23 * __this, Object_t * ___reconstructionFromTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.ImageTargetAbstractBehaviour::CreateVirtualButton(System.String,UnityEngine.Vector2,UnityEngine.Vector2)
 VirtualButtonAbstractBehaviour_t30 * ImageTargetAbstractBehaviour_CreateVirtualButton_m4247 (ImageTargetAbstractBehaviour_t23 * __this, String_t* ___vbName, Vector2_t99  ___position, Vector2_t99  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.ImageTargetAbstractBehaviour::CreateVirtualButton(System.String,UnityEngine.Vector2,UnityEngine.GameObject)
 VirtualButtonAbstractBehaviour_t30 * ImageTargetAbstractBehaviour_CreateVirtualButton_m4248 (Object_t * __this/* static, unused */, String_t* ___vbName, Vector2_t99  ___localScale, GameObject_t29 * ___immediateParent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonAbstractBehaviour> Vuforia.ImageTargetAbstractBehaviour::GetVirtualButtonBehaviours()
 Object_t* ImageTargetAbstractBehaviour_GetVirtualButtonBehaviours_m4249 (ImageTargetAbstractBehaviour_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::DestroyVirtualButton(System.String)
 void ImageTargetAbstractBehaviour_DestroyVirtualButton_m4250 (ImageTargetAbstractBehaviour_t23 * __this, String_t* ___vbName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vuforia.ImageTargetAbstractBehaviour::GetSize()
 Vector2_t99  ImageTargetAbstractBehaviour_GetSize_m305 (ImageTargetAbstractBehaviour_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::SetWidth(System.Single)
 void ImageTargetAbstractBehaviour_SetWidth_m306 (ImageTargetAbstractBehaviour_t23 * __this, float ___width, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::SetHeight(System.Single)
 void ImageTargetAbstractBehaviour_SetHeight_m307 (ImageTargetAbstractBehaviour_t23 * __this, float ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.get_AspectRatio()
 float ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_AspectRatio_m301 (ImageTargetAbstractBehaviour_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetType Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.get_ImageTargetType()
 int32_t ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_ImageTargetType_m302 (ImageTargetAbstractBehaviour_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.SetAspectRatio(System.Single)
 bool ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetAspectRatio_m303 (ImageTargetAbstractBehaviour_t23 * __this, float ___aspectRatio, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.SetImageTargetType(Vuforia.ImageTargetType)
 bool ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetImageTargetType_m304 (ImageTargetAbstractBehaviour_t23 * __this, int32_t ___imageTargetType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.InitializeImageTarget(Vuforia.ImageTarget)
 void ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_InitializeImageTarget_m308 (ImageTargetAbstractBehaviour_t23 * __this, Object_t * ___imageTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.AssociateExistingVirtualButtonBehaviour(Vuforia.VirtualButtonAbstractBehaviour)
 void ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_AssociateExistingVirtualButtonBehaviour_m311 (ImageTargetAbstractBehaviour_t23 * __this, VirtualButtonAbstractBehaviour_t30 * ___virtualButtonBehaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.CreateMissingVirtualButtonBehaviours()
 void ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_CreateMissingVirtualButtonBehaviours_m309 (ImageTargetAbstractBehaviour_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.TryGetVirtualButtonBehaviourByID(System.Int32,Vuforia.VirtualButtonAbstractBehaviour&)
 bool ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_TryGetVirtualButtonBehaviourByID_m310 (ImageTargetAbstractBehaviour_t23 * __this, int32_t ___id, VirtualButtonAbstractBehaviour_t30 ** ___virtualButtonBehaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::CreateVirtualButtonFromNative(Vuforia.VirtualButton)
 void ImageTargetAbstractBehaviour_CreateVirtualButtonFromNative_m4251 (ImageTargetAbstractBehaviour_t23 * __this, VirtualButton_t639 * ___virtualButton, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetAbstractBehaviour::CreateNewVirtualButtonFromBehaviour(Vuforia.VirtualButtonAbstractBehaviour)
 bool ImageTargetAbstractBehaviour_CreateNewVirtualButtonFromBehaviour_m4252 (ImageTargetAbstractBehaviour_t23 * __this, VirtualButtonAbstractBehaviour_t30 * ___newVBB, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m292 (ImageTargetAbstractBehaviour_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m293 (ImageTargetAbstractBehaviour_t23 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t74 * ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m294 (ImageTargetAbstractBehaviour_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t29 * ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m295 (ImageTargetAbstractBehaviour_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
