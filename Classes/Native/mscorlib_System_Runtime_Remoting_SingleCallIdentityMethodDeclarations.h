﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.SingleCallIdentity
struct SingleCallIdentity_t2104;
// System.String
struct String_t;
// System.Runtime.Remoting.Contexts.Context
struct Context_t2043;
// System.Type
struct Type_t;

// System.Void System.Runtime.Remoting.SingleCallIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
 void SingleCallIdentity__ctor_m11879 (SingleCallIdentity_t2104 * __this, String_t* ___objectUri, Context_t2043 * ___context, Type_t * ___objectType, MethodInfo* method) IL2CPP_METHOD_ATTR;
