﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t74;
// Sphere
struct Sphere_t71;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t81;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// GoalKeeper_Script/GoalKeeper_State
#include "AssemblyU2DCSharp_GoalKeeper_Script_GoalKeeper_State.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// GoalKeeper_Script
struct GoalKeeper_Script_t77  : public MonoBehaviour_t10
{
	// System.String GoalKeeper_Script::Name
	String_t* ___Name_2;
	// GoalKeeper_Script/GoalKeeper_State GoalKeeper_Script::state
	int32_t ___state_3;
	// UnityEngine.Transform GoalKeeper_Script::centro_campo
	Transform_t74 * ___centro_campo_4;
	// Sphere GoalKeeper_Script::sphere
	Sphere_t71 * ___sphere_5;
	// UnityEngine.Vector3 GoalKeeper_Script::initial_Position
	Vector3_t73  ___initial_Position_6;
	// UnityEngine.Transform GoalKeeper_Script::hand_bone
	Transform_t74 * ___hand_bone_7;
	// System.Single GoalKeeper_Script::timeToSacar
	float ___timeToSacar_8;
	// UnityEngine.CapsuleCollider GoalKeeper_Script::capsuleCollider
	CapsuleCollider_t81 * ___capsuleCollider_9;
};
