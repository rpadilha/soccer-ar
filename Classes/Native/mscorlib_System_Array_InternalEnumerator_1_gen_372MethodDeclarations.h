﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.TargetFinder/UpdateState>
struct InternalEnumerator_1_t4393;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.TargetFinder/UpdateState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Update.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/UpdateState>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m26190 (InternalEnumerator_1_t4393 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TargetFinder/UpdateState>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26191 (InternalEnumerator_1_t4393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/UpdateState>::Dispose()
 void InternalEnumerator_1_Dispose_m26192 (InternalEnumerator_1_t4393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TargetFinder/UpdateState>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m26193 (InternalEnumerator_1_t4393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.TargetFinder/UpdateState>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m26194 (InternalEnumerator_1_t4393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
