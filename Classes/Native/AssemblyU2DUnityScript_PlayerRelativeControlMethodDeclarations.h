﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PlayerRelativeControl
struct PlayerRelativeControl_t217;

// System.Void PlayerRelativeControl::.ctor()
 void PlayerRelativeControl__ctor_m763 (PlayerRelativeControl_t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerRelativeControl::Start()
 void PlayerRelativeControl_Start_m764 (PlayerRelativeControl_t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerRelativeControl::OnEndGame()
 void PlayerRelativeControl_OnEndGame_m765 (PlayerRelativeControl_t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerRelativeControl::Update()
 void PlayerRelativeControl_Update_m766 (PlayerRelativeControl_t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerRelativeControl::Main()
 void PlayerRelativeControl_Main_m767 (PlayerRelativeControl_t217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
