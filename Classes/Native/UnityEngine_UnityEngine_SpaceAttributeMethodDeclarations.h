﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SpaceAttribute
struct SpaceAttribute_t540;

// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
 void SpaceAttribute__ctor_m2548 (SpaceAttribute_t540 * __this, float ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
