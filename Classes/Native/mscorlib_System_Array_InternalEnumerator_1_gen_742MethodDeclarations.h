﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Decimal>
struct InternalEnumerator_1_t5288;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Decimal
#include "mscorlib_System_Decimal.h"

// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31812 (InternalEnumerator_1_t5288 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31813 (InternalEnumerator_1_t5288 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
 void InternalEnumerator_1_Dispose_m31814 (InternalEnumerator_1_t5288 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31815 (InternalEnumerator_1_t5288 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
 Decimal_t1740  InternalEnumerator_1_get_Current_m31816 (InternalEnumerator_1_t5288 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
