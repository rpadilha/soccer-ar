﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnSerializingAttribute>
struct InternalEnumerator_1_t5301;
// System.Object
struct Object_t;
// System.Runtime.Serialization.OnSerializingAttribute
struct OnSerializingAttribute_t2135;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnSerializingAttribute>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m31877(__this, ___array, method) (void)InternalEnumerator_1__ctor_m14156_gshared((InternalEnumerator_1_t2752 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnSerializingAttribute>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31878(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnSerializingAttribute>::Dispose()
#define InternalEnumerator_1_Dispose_m31879(__this, method) (void)InternalEnumerator_1_Dispose_m14160_gshared((InternalEnumerator_1_t2752 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnSerializingAttribute>::MoveNext()
#define InternalEnumerator_1_MoveNext_m31880(__this, method) (bool)InternalEnumerator_1_MoveNext_m14162_gshared((InternalEnumerator_1_t2752 *)__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnSerializingAttribute>::get_Current()
#define InternalEnumerator_1_get_Current_m31881(__this, method) (OnSerializingAttribute_t2135 *)InternalEnumerator_1_get_Current_m14164_gshared((InternalEnumerator_1_t2752 *)__this, method)
