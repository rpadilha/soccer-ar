﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Banda_Script
struct Banda_Script_t72;
// UnityEngine.Collider
struct Collider_t70;

// System.Void Banda_Script::.ctor()
 void Banda_Script__ctor_m110 (Banda_Script_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Banda_Script::Start()
 void Banda_Script_Start_m111 (Banda_Script_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Banda_Script::Update()
 void Banda_Script_Update_m112 (Banda_Script_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Banda_Script::OnTriggerEnter(UnityEngine.Collider)
 void Banda_Script_OnTriggerEnter_m113 (Banda_Script_t72 * __this, Collider_t70 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
