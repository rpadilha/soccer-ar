﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.DataSetImpl>
struct EqualityComparer_1_t4027;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.DataSetImpl>
struct EqualityComparer_1_t4027  : public Object_t
{
};
struct EqualityComparer_1_t4027_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.DataSetImpl>::_default
	EqualityComparer_1_t4027 * ____default_0;
};
