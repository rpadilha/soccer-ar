﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.ComponentFactoryStarterBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_10.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ComponentFactoryStarterBehaviour>
struct CachedInvokableCall_1_t2823  : public InvokableCall_1_t2824
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.ComponentFactoryStarterBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
