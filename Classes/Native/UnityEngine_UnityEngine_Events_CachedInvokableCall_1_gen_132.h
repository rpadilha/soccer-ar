﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRendererAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_134.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRendererAbstractBehaviour>
struct CachedInvokableCall_1_t4575  : public InvokableCall_1_t4576
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRendererAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
