﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ContentSizeFitter
struct ContentSizeFitter_t430;
// UnityEngine.RectTransform
struct RectTransform_t338;
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"

// System.Void UnityEngine.UI.ContentSizeFitter::.ctor()
 void ContentSizeFitter__ctor_m1860 (ContentSizeFitter_t430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_horizontalFit()
 int32_t ContentSizeFitter_get_horizontalFit_m1861 (ContentSizeFitter_t430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::set_horizontalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
 void ContentSizeFitter_set_horizontalFit_m1862 (ContentSizeFitter_t430 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_verticalFit()
 int32_t ContentSizeFitter_get_verticalFit_m1863 (ContentSizeFitter_t430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::set_verticalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
 void ContentSizeFitter_set_verticalFit_m1864 (ContentSizeFitter_t430 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::get_rectTransform()
 RectTransform_t338 * ContentSizeFitter_get_rectTransform_m1865 (ContentSizeFitter_t430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::OnEnable()
 void ContentSizeFitter_OnEnable_m1866 (ContentSizeFitter_t430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::OnDisable()
 void ContentSizeFitter_OnDisable_m1867 (ContentSizeFitter_t430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::OnRectTransformDimensionsChange()
 void ContentSizeFitter_OnRectTransformDimensionsChange_m1868 (ContentSizeFitter_t430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::HandleSelfFittingAlongAxis(System.Int32)
 void ContentSizeFitter_HandleSelfFittingAlongAxis_m1869 (ContentSizeFitter_t430 * __this, int32_t ___axis, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutHorizontal()
 void ContentSizeFitter_SetLayoutHorizontal_m1870 (ContentSizeFitter_t430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutVertical()
 void ContentSizeFitter_SetLayoutVertical_m1871 (ContentSizeFitter_t430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ContentSizeFitter::SetDirty()
 void ContentSizeFitter_SetDirty_m1872 (ContentSizeFitter_t430 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
