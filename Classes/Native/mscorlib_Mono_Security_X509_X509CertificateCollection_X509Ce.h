﻿#pragma once
#include <stdint.h>
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
struct X509CertificateEnumerator_t1835  : public Object_t
{
	// System.Collections.IEnumerator Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator::enumerator
	Object_t * ___enumerator_0;
};
