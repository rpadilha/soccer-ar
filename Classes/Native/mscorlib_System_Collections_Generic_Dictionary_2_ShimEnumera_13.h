﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__0.h"
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordAbstractBehaviour>
struct ShimEnumerator_t4180  : public Object_t
{
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordAbstractBehaviour>::host_enumerator
	Enumerator_t877  ___host_enumerator_0;
};
