﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>
struct ReadOnlyCollection_1_t4184;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t34;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<Vuforia.WordAbstractBehaviour>
struct IList_1_t4187;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// Vuforia.WordAbstractBehaviour[]
struct WordAbstractBehaviourU5BU5D_t871;
// System.Collections.Generic.IEnumerator`1<Vuforia.WordAbstractBehaviour>
struct IEnumerator_1_t884;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_0MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m24219(__this, ___list, method) (void)ReadOnlyCollection_1__ctor_m14564_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t*)___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m24220(__this, ___item, method) (void)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14565_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m24221(__this, method) (void)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14566_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m24222(__this, ___index, ___item, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14567_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m24223(__this, ___item, method) (bool)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14568_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m24224(__this, ___index, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14569_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m24225(__this, ___index, method) (WordAbstractBehaviour_t34 *)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14570_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m24226(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14571_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24227(__this, method) (bool)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14572_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m24228(__this, ___array, ___index, method) (void)ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14573_gshared((ReadOnlyCollection_1_t2836 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m24229(__this, method) (Object_t *)ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14574_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m24230(__this, ___value, method) (int32_t)ReadOnlyCollection_1_System_Collections_IList_Add_m14575_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m24231(__this, method) (void)ReadOnlyCollection_1_System_Collections_IList_Clear_m14576_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m24232(__this, ___value, method) (bool)ReadOnlyCollection_1_System_Collections_IList_Contains_m14577_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m24233(__this, ___value, method) (int32_t)ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14578_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m24234(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_Insert_m14579_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m24235(__this, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_Remove_m14580_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m24236(__this, ___index, method) (void)ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14581_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m24237(__this, method) (bool)ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14582_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m24238(__this, method) (Object_t *)ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14583_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m24239(__this, method) (bool)ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14584_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m24240(__this, method) (bool)ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14585_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m24241(__this, ___index, method) (Object_t *)ReadOnlyCollection_1_System_Collections_IList_get_Item_m14586_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m24242(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_set_Item_m14587_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::Contains(T)
#define ReadOnlyCollection_1_Contains_m24243(__this, ___value, method) (bool)ReadOnlyCollection_1_Contains_m14588_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m24244(__this, ___array, ___index, method) (void)ReadOnlyCollection_1_CopyTo_m14589_gshared((ReadOnlyCollection_1_t2836 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m24245(__this, method) (Object_t*)ReadOnlyCollection_1_GetEnumerator_m14590_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m24246(__this, ___value, method) (int32_t)ReadOnlyCollection_1_IndexOf_m14591_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::get_Count()
#define ReadOnlyCollection_1_get_Count_m24247(__this, method) (int32_t)ReadOnlyCollection_1_get_Count_m14592_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m24248(__this, ___index, method) (WordAbstractBehaviour_t34 *)ReadOnlyCollection_1_get_Item_m14593_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
