﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_122.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.KeepAliveAbstractBehaviour>
struct CachedInvokableCall_1_t4208  : public InvokableCall_1_t4209
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.KeepAliveAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
