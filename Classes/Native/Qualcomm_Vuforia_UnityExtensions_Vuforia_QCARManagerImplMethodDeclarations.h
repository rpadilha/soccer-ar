﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARManagerImpl
struct QCARManagerImpl_t702;
// Vuforia.WorldCenterTrackableBehaviour
struct WorldCenterTrackableBehaviour_t116;
// UnityEngine.Transform
struct Transform_t74;
// Vuforia.StateManagerImpl
struct StateManagerImpl_t703;
// Vuforia.QCARAbstractBehaviour/WorldCenterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio_0.h"
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// Vuforia.QCARManagerImpl/FrameState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Fra.h"

// System.Void Vuforia.QCARManagerImpl::set_WorldCenterMode(Vuforia.QCARAbstractBehaviour/WorldCenterMode)
 void QCARManagerImpl_set_WorldCenterMode_m3148 (QCARManagerImpl_t702 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARAbstractBehaviour/WorldCenterMode Vuforia.QCARManagerImpl::get_WorldCenterMode()
 int32_t QCARManagerImpl_get_WorldCenterMode_m3149 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::set_WorldCenter(Vuforia.WorldCenterTrackableBehaviour)
 void QCARManagerImpl_set_WorldCenter_m3150 (QCARManagerImpl_t702 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WorldCenterTrackableBehaviour Vuforia.QCARManagerImpl::get_WorldCenter()
 Object_t * QCARManagerImpl_get_WorldCenter_m3151 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::set_ARCameraTransform(UnityEngine.Transform)
 void QCARManagerImpl_set_ARCameraTransform_m3152 (QCARManagerImpl_t702 * __this, Transform_t74 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.QCARManagerImpl::get_ARCameraTransform()
 Transform_t74 * QCARManagerImpl_get_ARCameraTransform_m3153 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARManagerImpl::get_Initialized()
 bool QCARManagerImpl_get_Initialized_m3154 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARManagerImpl::get_QCARFrameIndex()
 int32_t QCARManagerImpl_get_QCARFrameIndex_m3155 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::set_VideoBackgroundTextureSet(System.Boolean)
 void QCARManagerImpl_set_VideoBackgroundTextureSet_m3156 (QCARManagerImpl_t702 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARManagerImpl::get_VideoBackgroundTextureSet()
 bool QCARManagerImpl_get_VideoBackgroundTextureSet_m3157 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARManagerImpl::Init()
 bool QCARManagerImpl_Init_m3158 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::Deinit()
 void QCARManagerImpl_Deinit_m3159 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARManagerImpl::Update(UnityEngine.ScreenOrientation)
 bool QCARManagerImpl_Update_m3160 (QCARManagerImpl_t702 * __this, int32_t ___counterRotation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::StartRendering()
 void QCARManagerImpl_StartRendering_m3161 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::FinishRendering()
 void QCARManagerImpl_FinishRendering_m3162 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::Pause(System.Boolean)
 void QCARManagerImpl_Pause_m3163 (QCARManagerImpl_t702 * __this, bool ___pause, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::SetStatesToDiscard()
 void QCARManagerImpl_SetStatesToDiscard_m3164 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::InitializeTrackableContainer(System.Int32)
 void QCARManagerImpl_InitializeTrackableContainer_m3165 (QCARManagerImpl_t702 * __this, int32_t ___numTrackableResults, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::UpdateTrackers(Vuforia.QCARManagerImpl/FrameState)
 void QCARManagerImpl_UpdateTrackers_m3166 (QCARManagerImpl_t702 * __this, FrameState_t695  ___frameState, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::UpdateSmartTerrain(Vuforia.QCARManagerImpl/FrameState,Vuforia.StateManagerImpl)
 void QCARManagerImpl_UpdateSmartTerrain_m3167 (QCARManagerImpl_t702 * __this, FrameState_t695  ___frameState, StateManagerImpl_t703 * ___stateManager, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::UpdateTrackablesEditor()
 void QCARManagerImpl_UpdateTrackablesEditor_m3168 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::UpdateWordTrackables(Vuforia.QCARManagerImpl/FrameState)
 void QCARManagerImpl_UpdateWordTrackables_m3169 (QCARManagerImpl_t702 * __this, FrameState_t695  ___frameState, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::UpdateImageContainer()
 void QCARManagerImpl_UpdateImageContainer_m3170 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::UpdateCameraFrame()
 void QCARManagerImpl_UpdateCameraFrame_m3171 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::InjectCameraFrame()
 void QCARManagerImpl_InjectCameraFrame_m3172 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARManagerImpl::.ctor()
 void QCARManagerImpl__ctor_m3173 (QCARManagerImpl_t702 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
