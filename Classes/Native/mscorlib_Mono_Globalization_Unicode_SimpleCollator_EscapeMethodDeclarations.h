﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.SimpleCollator/Escape
struct Escape_t1796;
struct Escape_t1796_marshaled;

void Escape_t1796_marshal(const Escape_t1796& unmarshaled, Escape_t1796_marshaled& marshaled);
void Escape_t1796_marshal_back(const Escape_t1796_marshaled& marshaled, Escape_t1796& unmarshaled);
void Escape_t1796_marshal_cleanup(Escape_t1796_marshaled& marshaled);
