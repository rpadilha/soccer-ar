﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t344;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Graphic>
struct Predicate_1_t3548  : public MulticastDelegate_t373
{
};
