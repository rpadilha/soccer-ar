﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$48
struct $ArrayType$48_t2322;
struct $ArrayType$48_t2322_marshaled;

void $ArrayType$48_t2322_marshal(const $ArrayType$48_t2322& unmarshaled, $ArrayType$48_t2322_marshaled& marshaled);
void $ArrayType$48_t2322_marshal_back(const $ArrayType$48_t2322_marshaled& marshaled, $ArrayType$48_t2322& unmarshaled);
void $ArrayType$48_t2322_marshal_cleanup($ArrayType$48_t2322_marshaled& marshaled);
