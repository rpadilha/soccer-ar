﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
struct NonBacktrackingGroup_t1504;
// System.Text.RegularExpressions.ICompiler
struct ICompiler_t1499;

// System.Void System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::.ctor()
 void NonBacktrackingGroup__ctor_m7621 (NonBacktrackingGroup_t1504 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
 void NonBacktrackingGroup_Compile_m7622 (NonBacktrackingGroup_t1504 * __this, Object_t * ___cmp, bool ___reverse, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::IsComplex()
 bool NonBacktrackingGroup_IsComplex_m7623 (NonBacktrackingGroup_t1504 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
