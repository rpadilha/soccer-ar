﻿#pragma once
#include <stdint.h>
// System.Array
struct Array_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>
struct InternalEnumerator_1_t4447 
{
	// System.Array System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::array
	Array_t * ___array_0;
	// System.Int32 System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::idx
	int32_t ___idx_1;
};
