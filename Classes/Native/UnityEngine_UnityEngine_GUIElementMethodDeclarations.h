﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIElement
struct GUIElement_t990;
// UnityEngine.Camera
struct Camera_t168;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Boolean UnityEngine.GUIElement::HitTest(UnityEngine.Vector3)
 bool GUIElement_HitTest_m691 (GUIElement_t990 * __this, Vector3_t73  ___screenPosition, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUIElement::INTERNAL_CALL_HitTest(UnityEngine.GUIElement,UnityEngine.Vector3&,UnityEngine.Camera)
 bool GUIElement_INTERNAL_CALL_HitTest_m5718 (Object_t * __this/* static, unused */, GUIElement_t990 * ___self, Vector3_t73 * ___screenPosition, Camera_t168 * ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
