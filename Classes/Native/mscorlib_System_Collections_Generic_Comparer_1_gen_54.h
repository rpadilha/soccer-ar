﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.Events.PersistentCall>
struct Comparer_1_t5008;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Events.PersistentCall>
struct Comparer_1_t5008  : public Object_t
{
};
struct Comparer_1_t5008_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Events.PersistentCall>::_default
	Comparer_1_t5008 * ____default_0;
};
