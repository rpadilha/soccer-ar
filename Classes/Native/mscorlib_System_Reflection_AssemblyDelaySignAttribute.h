﻿#pragma once
#include <stdint.h>
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyDelaySignAttribute
struct AssemblyDelaySignAttribute_t1337  : public Attribute_t145
{
	// System.Boolean System.Reflection.AssemblyDelaySignAttribute::delay
	bool ___delay_0;
};
