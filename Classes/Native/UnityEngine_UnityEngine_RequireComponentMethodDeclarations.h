﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RequireComponent
struct RequireComponent_t154;
// System.Type
struct Type_t;

// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
 void RequireComponent__ctor_m348 (RequireComponent_t154 * __this, Type_t * ___requiredComponent, MethodInfo* method) IL2CPP_METHOD_ATTR;
