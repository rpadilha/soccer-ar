﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.OutAttribute
struct OutAttribute_t1761;

// System.Void System.Runtime.InteropServices.OutAttribute::.ctor()
 void OutAttribute__ctor_m9910 (OutAttribute_t1761 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
