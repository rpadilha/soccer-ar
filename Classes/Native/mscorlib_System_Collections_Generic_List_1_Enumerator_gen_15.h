﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.Surface>
struct List_1_t767;
// Vuforia.Surface
struct Surface_t16;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>
struct Enumerator_t897 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::l
	List_1_t767 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::current
	Object_t * ___current_3;
};
