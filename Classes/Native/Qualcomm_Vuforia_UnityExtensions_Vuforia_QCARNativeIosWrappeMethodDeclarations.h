﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARNativeIosWrapper
struct QCARNativeIosWrapper_t750;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t466;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceInitCamera(System.Int32)
 int32_t QCARNativeIosWrapper_CameraDeviceInitCamera_m3317 (QCARNativeIosWrapper_t750 * __this, int32_t ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceDeinitCamera()
 int32_t QCARNativeIosWrapper_CameraDeviceDeinitCamera_m3318 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceStartCamera()
 int32_t QCARNativeIosWrapper_CameraDeviceStartCamera_m3319 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceStopCamera()
 int32_t QCARNativeIosWrapper_CameraDeviceStopCamera_m3320 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceGetNumVideoModes()
 int32_t QCARNativeIosWrapper_CameraDeviceGetNumVideoModes_m3321 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::CameraDeviceGetVideoMode(System.Int32,System.IntPtr)
 void QCARNativeIosWrapper_CameraDeviceGetVideoMode_m3322 (QCARNativeIosWrapper_t750 * __this, int32_t ___idx, IntPtr_t121 ___videoMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceSelectVideoMode(System.Int32)
 int32_t QCARNativeIosWrapper_CameraDeviceSelectVideoMode_m3323 (QCARNativeIosWrapper_t750 * __this, int32_t ___idx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceSetFlashTorchMode(System.Int32)
 int32_t QCARNativeIosWrapper_CameraDeviceSetFlashTorchMode_m3324 (QCARNativeIosWrapper_t750 * __this, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceSetFocusMode(System.Int32)
 int32_t QCARNativeIosWrapper_CameraDeviceSetFocusMode_m3325 (QCARNativeIosWrapper_t750 * __this, int32_t ___focusMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceSetCameraConfiguration(System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_CameraDeviceSetCameraConfiguration_m3326 (QCARNativeIosWrapper_t750 * __this, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarSetFrameFormat(System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_QcarSetFrameFormat_m3327 (QCARNativeIosWrapper_t750 * __this, int32_t ___format, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetExists(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_DataSetExists_m3328 (QCARNativeIosWrapper_t750 * __this, String_t* ___relativePath, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetLoad(System.String,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_DataSetLoad_m3329 (QCARNativeIosWrapper_t750 * __this, String_t* ___relativePath, int32_t ___storageType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_DataSetGetNumTrackableType_m3330 (QCARNativeIosWrapper_t750 * __this, int32_t ___trackableType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_DataSetGetTrackablesOfType_m3331 (QCARNativeIosWrapper_t750 * __this, int32_t ___trackableType, IntPtr_t121 ___trackableDataArray, int32_t ___trackableDataArrayLength, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNativeIosWrapper_DataSetGetTrackableName_m3332 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, StringBuilder_t466 * ___trackableName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetCreateTrackable(System.IntPtr,System.IntPtr,System.Text.StringBuilder,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_DataSetCreateTrackable_m3333 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, IntPtr_t121 ___trackableSourcePtr, StringBuilder_t466 * ___trackableName, int32_t ___nameMaxLength, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetDestroyTrackable(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_DataSetDestroyTrackable_m3334 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetHasReachedTrackableLimit(System.IntPtr)
 int32_t QCARNativeIosWrapper_DataSetHasReachedTrackableLimit_m3335 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::GetCameraThreadID()
 int32_t QCARNativeIosWrapper_GetCameraThreadID_m3336 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetBuilderBuild(System.String,System.Single)
 int32_t QCARNativeIosWrapper_ImageTargetBuilderBuild_m3337 (QCARNativeIosWrapper_t750 * __this, String_t* ___targetName, float ___sceenSizeWidth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::FrameCounterGetBenchmarkingData(System.IntPtr,System.Boolean)
 void QCARNativeIosWrapper_FrameCounterGetBenchmarkingData_m3338 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___benchmarkingData, bool ___isStereoRendering, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::ImageTargetBuilderStartScan()
 void QCARNativeIosWrapper_ImageTargetBuilderStartScan_m3339 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::ImageTargetBuilderStopScan()
 void QCARNativeIosWrapper_ImageTargetBuilderStopScan_m3340 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetBuilderGetFrameQuality()
 int32_t QCARNativeIosWrapper_ImageTargetBuilderGetFrameQuality_m3341 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::ImageTargetBuilderGetTrackableSource()
 IntPtr_t121 QCARNativeIosWrapper_ImageTargetBuilderGetTrackableSource_m3342 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_ImageTargetCreateVirtualButton_m3343 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String)
 int32_t QCARNativeIosWrapper_ImageTargetDestroyVirtualButton_m3344 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::VirtualButtonGetId(System.IntPtr,System.String,System.String)
 int32_t QCARNativeIosWrapper_VirtualButtonGetId_m3345 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetGetNumVirtualButtons(System.IntPtr,System.String)
 int32_t QCARNativeIosWrapper_ImageTargetGetNumVirtualButtons_m3346 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String)
 int32_t QCARNativeIosWrapper_ImageTargetGetVirtualButtons_m3347 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___virtualButtonDataArray, IntPtr_t121 ___rectangleDataArray, int32_t ___virtualButtonDataArrayLength, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNativeIosWrapper_ImageTargetGetVirtualButtonName_m3348 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, int32_t ___idx, StringBuilder_t466 * ___vbName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_CylinderTargetGetDimensions_m3349 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___dimensions, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CylinderTargetSetSideLength(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeIosWrapper_CylinderTargetSetSideLength_m3350 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___sideLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CylinderTargetSetTopDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeIosWrapper_CylinderTargetSetTopDiameter_m3351 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___topDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CylinderTargetSetBottomDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeIosWrapper_CylinderTargetSetBottomDiameter_m3352 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___bottomDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTargetSetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_ObjectTargetSetSize_m3353 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTargetGetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_ObjectTargetGetSize_m3354 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerStart()
 int32_t QCARNativeIosWrapper_ObjectTrackerStart_m3355 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::ObjectTrackerStop()
 void QCARNativeIosWrapper_ObjectTrackerStop_m3356 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::ObjectTrackerCreateDataSet()
 IntPtr_t121 QCARNativeIosWrapper_ObjectTrackerCreateDataSet_m3357 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerDestroyDataSet(System.IntPtr)
 int32_t QCARNativeIosWrapper_ObjectTrackerDestroyDataSet_m3358 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerActivateDataSet(System.IntPtr)
 int32_t QCARNativeIosWrapper_ObjectTrackerActivateDataSet_m3359 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerDeactivateDataSet(System.IntPtr)
 int32_t QCARNativeIosWrapper_ObjectTrackerDeactivateDataSet_m3360 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerPersistExtendedTracking(System.Int32)
 int32_t QCARNativeIosWrapper_ObjectTrackerPersistExtendedTracking_m3361 (QCARNativeIosWrapper_t750 * __this, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerResetExtendedTracking()
 int32_t QCARNativeIosWrapper_ObjectTrackerResetExtendedTracking_m3362 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::MarkerSetSize(System.Int32,System.Single)
 int32_t QCARNativeIosWrapper_MarkerSetSize_m3363 (QCARNativeIosWrapper_t750 * __this, int32_t ___trackableIndex, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::MarkerTrackerStart()
 int32_t QCARNativeIosWrapper_MarkerTrackerStart_m3364 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::MarkerTrackerStop()
 void QCARNativeIosWrapper_MarkerTrackerStop_m3365 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::MarkerTrackerCreateMarker(System.Int32,System.String,System.Single)
 int32_t QCARNativeIosWrapper_MarkerTrackerCreateMarker_m3366 (QCARNativeIosWrapper_t750 * __this, int32_t ___id, String_t* ___trackableName, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::MarkerTrackerDestroyMarker(System.Int32)
 int32_t QCARNativeIosWrapper_MarkerTrackerDestroyMarker_m3367 (QCARNativeIosWrapper_t750 * __this, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::InitPlatformNative()
 void QCARNativeIosWrapper_InitPlatformNative_m3368 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::InitFrameState(System.IntPtr)
 void QCARNativeIosWrapper_InitFrameState_m3369 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::DeinitFrameState(System.IntPtr)
 void QCARNativeIosWrapper_DeinitFrameState_m3370 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::OnSurfaceChanged(System.Int32,System.Int32)
 void QCARNativeIosWrapper_OnSurfaceChanged_m3371 (QCARNativeIosWrapper_t750 * __this, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::OnPause()
 void QCARNativeIosWrapper_OnPause_m3372 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::OnResume()
 void QCARNativeIosWrapper_OnResume_m3373 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::HasSurfaceBeenRecreated()
 bool QCARNativeIosWrapper_HasSurfaceBeenRecreated_m3374 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::UpdateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_UpdateQCAR_m3375 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___imageHeaderDataArray, int32_t ___imageHeaderArrayLength, IntPtr_t121 ___frameIndex, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::RendererEnd()
 void QCARNativeIosWrapper_RendererEnd_m3376 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarGetBufferSize(System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_QcarGetBufferSize_m3377 (QCARNativeIosWrapper_t750 * __this, int32_t ___width, int32_t ___height, int32_t ___format, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::QcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 void QCARNativeIosWrapper_QcarAddCameraFrame_m3378 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___pixels, int32_t ___width, int32_t ___height, int32_t ___format, int32_t ___stride, int32_t ___frameIdx, int32_t ___flipHorizontally, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::RendererSetVideoBackgroundCfg(System.IntPtr)
 void QCARNativeIosWrapper_RendererSetVideoBackgroundCfg_m3379 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::RendererGetVideoBackgroundCfg(System.IntPtr)
 void QCARNativeIosWrapper_RendererGetVideoBackgroundCfg_m3380 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::RendererGetVideoBackgroundTextureInfo(System.IntPtr)
 void QCARNativeIosWrapper_RendererGetVideoBackgroundTextureInfo_m3381 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___texInfo, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::RendererSetVideoBackgroundTextureID(System.Int32)
 int32_t QCARNativeIosWrapper_RendererSetVideoBackgroundTextureID_m3382 (QCARNativeIosWrapper_t750 * __this, int32_t ___textureID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::RendererIsVideoBackgroundTextureInfoAvailable()
 int32_t QCARNativeIosWrapper_RendererIsVideoBackgroundTextureInfoAvailable_m3383 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarSetHint(System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_QcarSetHint_m3384 (QCARNativeIosWrapper_t750 * __this, int32_t ___hint, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::GetProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_GetProjectionGL_m3385 (QCARNativeIosWrapper_t750 * __this, float ___nearClip, float ___farClip, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::SetApplicationEnvironment(System.Int32,System.Int32,System.Int32)
 void QCARNativeIosWrapper_SetApplicationEnvironment_m3386 (QCARNativeIosWrapper_t750 * __this, int32_t ___unityVersionMajor, int32_t ___unityVersionMinor, int32_t ___unityVersionChange, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::SetStateBufferSize(System.Int32)
 void QCARNativeIosWrapper_SetStateBufferSize_m3387 (QCARNativeIosWrapper_t750 * __this, int32_t ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerStart()
 int32_t QCARNativeIosWrapper_SmartTerrainTrackerStart_m3388 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerStop()
 void QCARNativeIosWrapper_SmartTerrainTrackerStop_m3389 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerSetScaleToMillimeter(System.Single)
 bool QCARNativeIosWrapper_SmartTerrainTrackerSetScaleToMillimeter_m3390 (QCARNativeIosWrapper_t750 * __this, float ___scaleFactor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerInitBuilder()
 bool QCARNativeIosWrapper_SmartTerrainTrackerInitBuilder_m3391 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerDeinitBuilder()
 bool QCARNativeIosWrapper_SmartTerrainTrackerDeinitBuilder_m3392 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderCreateReconstructionFromTarget()
 IntPtr_t121 QCARNativeIosWrapper_SmartTerrainBuilderCreateReconstructionFromTarget_m3393 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderCreateReconstructionFromEnvironment()
 IntPtr_t121 QCARNativeIosWrapper_SmartTerrainBuilderCreateReconstructionFromEnvironment_m3394 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderAddReconstruction(System.IntPtr)
 bool QCARNativeIosWrapper_SmartTerrainBuilderAddReconstruction_m3395 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderRemoveReconstruction(System.IntPtr)
 bool QCARNativeIosWrapper_SmartTerrainBuilderRemoveReconstruction_m3396 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderDestroyReconstruction(System.IntPtr)
 bool QCARNativeIosWrapper_SmartTerrainBuilderDestroyReconstruction_m3397 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionStart(System.IntPtr)
 bool QCARNativeIosWrapper_ReconstructionStart_m3398 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionStop(System.IntPtr)
 bool QCARNativeIosWrapper_ReconstructionStop_m3399 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionIsReconstructing(System.IntPtr)
 bool QCARNativeIosWrapper_ReconstructionIsReconstructing_m3400 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionReset(System.IntPtr)
 bool QCARNativeIosWrapper_ReconstructionReset_m3401 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::ReconstructionSetNavMeshPadding(System.IntPtr,System.Single)
 void QCARNativeIosWrapper_ReconstructionSetNavMeshPadding_m3402 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___reconstruction, float ___padding, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single)
 bool QCARNativeIosWrapper_ReconstructionFromTargetSetInitializationTarget_m3403 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___reconstruction, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, IntPtr_t121 ___occluderMin, IntPtr_t121 ___occluderMax, IntPtr_t121 ___offsetToOccluder, IntPtr_t121 ___rotationAxisToOccluder, float ___rotationAngleToOccluder, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionSetMaximumArea(System.IntPtr,System.IntPtr)
 bool QCARNativeIosWrapper_ReconstructionSetMaximumArea_m3404 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___reconstruction, IntPtr_t121 ___maximumArea, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ReconstructioFromEnvironmentGetReconstructionState(System.IntPtr)
 int32_t QCARNativeIosWrapper_ReconstructioFromEnvironmentGetReconstructionState_m3405 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderStartInit(System.String,System.String)
 int32_t QCARNativeIosWrapper_TargetFinderStartInit_m3406 (QCARNativeIosWrapper_t750 * __this, String_t* ___userKey, String_t* ___secretKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderGetInitState()
 int32_t QCARNativeIosWrapper_TargetFinderGetInitState_m3407 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderDeinit()
 int32_t QCARNativeIosWrapper_TargetFinderDeinit_m3408 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderStartRecognition()
 int32_t QCARNativeIosWrapper_TargetFinderStartRecognition_m3409 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderStop()
 int32_t QCARNativeIosWrapper_TargetFinderStop_m3410 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderSetUIScanlineColor(System.Single,System.Single,System.Single)
 void QCARNativeIosWrapper_TargetFinderSetUIScanlineColor_m3411 (QCARNativeIosWrapper_t750 * __this, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderSetUIPointColor(System.Single,System.Single,System.Single)
 void QCARNativeIosWrapper_TargetFinderSetUIPointColor_m3412 (QCARNativeIosWrapper_t750 * __this, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderUpdate(System.IntPtr)
 void QCARNativeIosWrapper_TargetFinderUpdate_m3413 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___targetFinderState, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderGetResults(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_TargetFinderGetResults_m3414 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___searchResultArray, int32_t ___searchResultArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderEnableTracking(System.IntPtr,System.IntPtr)
 int32_t QCARNativeIosWrapper_TargetFinderEnableTracking_m3415 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___searchResult, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderGetImageTargets(System.IntPtr,System.Int32)
 void QCARNativeIosWrapper_TargetFinderGetImageTargets_m3416 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___trackableIdArray, int32_t ___trackableIdArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderClearTrackables()
 void QCARNativeIosWrapper_TargetFinderClearTrackables_m3417 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TextTrackerStart()
 int32_t QCARNativeIosWrapper_TextTrackerStart_m3418 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TextTrackerStop()
 void QCARNativeIosWrapper_TextTrackerStop_m3419 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TextTrackerSetRegionOfInterest(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_TextTrackerSetRegionOfInterest_m3420 (QCARNativeIosWrapper_t750 * __this, int32_t ___detectionLeftTopX, int32_t ___detectionLeftTopY, int32_t ___detectionRightBottomX, int32_t ___detectionRightBottomY, int32_t ___trackingLeftTopX, int32_t ___trackingLeftTopY, int32_t ___trackingRightBottomX, int32_t ___trackingRightBottomY, int32_t ___upDirection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TextTrackerGetRegionOfInterest(System.IntPtr,System.IntPtr)
 void QCARNativeIosWrapper_TextTrackerGetRegionOfInterest_m3421 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___detectionROI, IntPtr_t121 ___trackingROI, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListLoadWordList(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_WordListLoadWordList_m3422 (QCARNativeIosWrapper_t750 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListAddWordsFromFile(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_WordListAddWordsFromFile_m3423 (QCARNativeIosWrapper_t750 * __this, String_t* ___path, int32_t ___storagetType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListAddWordU(System.IntPtr)
 int32_t QCARNativeIosWrapper_WordListAddWordU_m3424 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListRemoveWordU(System.IntPtr)
 int32_t QCARNativeIosWrapper_WordListRemoveWordU_m3425 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListContainsWordU(System.IntPtr)
 int32_t QCARNativeIosWrapper_WordListContainsWordU_m3426 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListUnloadAllLists()
 int32_t QCARNativeIosWrapper_WordListUnloadAllLists_m3427 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListSetFilterMode(System.Int32)
 int32_t QCARNativeIosWrapper_WordListSetFilterMode_m3428 (QCARNativeIosWrapper_t750 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListGetFilterMode()
 int32_t QCARNativeIosWrapper_WordListGetFilterMode_m3429 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListLoadFilterList(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_WordListLoadFilterList_m3430 (QCARNativeIosWrapper_t750 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListAddWordToFilterListU(System.IntPtr)
 int32_t QCARNativeIosWrapper_WordListAddWordToFilterListU_m3431 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListRemoveWordFromFilterListU(System.IntPtr)
 int32_t QCARNativeIosWrapper_WordListRemoveWordFromFilterListU_m3432 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListClearFilterList()
 int32_t QCARNativeIosWrapper_WordListClearFilterList_m3433 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListGetFilterListWordCount()
 int32_t QCARNativeIosWrapper_WordListGetFilterListWordCount_m3434 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::WordListGetFilterListWordU(System.Int32)
 IntPtr_t121 QCARNativeIosWrapper_WordListGetFilterListWordU_m3435 (QCARNativeIosWrapper_t750 * __this, int32_t ___i, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordGetLetterMask(System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_WordGetLetterMask_m3436 (QCARNativeIosWrapper_t750 * __this, int32_t ___wordID, IntPtr_t121 ___letterMaskImage, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordGetLetterBoundingBoxes(System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_WordGetLetterBoundingBoxes_m3437 (QCARNativeIosWrapper_t750 * __this, int32_t ___wordID, IntPtr_t121 ___letterBoundingBoxes, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TrackerManagerInitTracker(System.Int32)
 int32_t QCARNativeIosWrapper_TrackerManagerInitTracker_m3438 (QCARNativeIosWrapper_t750 * __this, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TrackerManagerDeinitTracker(System.Int32)
 int32_t QCARNativeIosWrapper_TrackerManagerDeinitTracker_m3439 (QCARNativeIosWrapper_t750 * __this, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::VirtualButtonSetEnabled(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNativeIosWrapper_VirtualButtonSetEnabled_m3440 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::VirtualButtonSetSensitivity(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNativeIosWrapper_VirtualButtonSetSensitivity_m3441 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___sensitivity, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::VirtualButtonSetAreaRectangle(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_VirtualButtonSetAreaRectangle_m3442 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarInit(System.String)
 int32_t QCARNativeIosWrapper_QcarInit_m3443 (QCARNativeIosWrapper_t750 * __this, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarDeinit()
 int32_t QCARNativeIosWrapper_QcarDeinit_m3444 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::StartExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_StartExtendedTracking_m3445 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::StopExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_StopExtendedTracking_m3446 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsSupportedDeviceDetected()
 bool QCARNativeIosWrapper_EyewearIsSupportedDeviceDetected_m3447 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsSeeThru()
 bool QCARNativeIosWrapper_EyewearIsSeeThru_m3448 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearGetScreenOrientation()
 int32_t QCARNativeIosWrapper_EyewearGetScreenOrientation_m3449 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsStereoCapable()
 bool QCARNativeIosWrapper_EyewearIsStereoCapable_m3450 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsStereoEnabled()
 bool QCARNativeIosWrapper_EyewearIsStereoEnabled_m3451 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsStereoGLOnly()
 bool QCARNativeIosWrapper_EyewearIsStereoGLOnly_m3452 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearSetStereo(System.Boolean)
 bool QCARNativeIosWrapper_EyewearSetStereo_m3453 (QCARNativeIosWrapper_t750 * __this, bool ___enable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearGetDefaultSceneScale(System.IntPtr)
 int32_t QCARNativeIosWrapper_EyewearGetDefaultSceneScale_m3454 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_EyewearGetProjectionMatrix_m3455 (QCARNativeIosWrapper_t750 * __this, int32_t ___eyeID, int32_t ___profileID, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearCPMGetMaxCount()
 int32_t QCARNativeIosWrapper_EyewearCPMGetMaxCount_m3456 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearCPMGetUsedCount()
 int32_t QCARNativeIosWrapper_EyewearCPMGetUsedCount_m3457 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMIsProfileUsed(System.Int32)
 bool QCARNativeIosWrapper_EyewearCPMIsProfileUsed_m3458 (QCARNativeIosWrapper_t750 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearCPMGetActiveProfile()
 int32_t QCARNativeIosWrapper_EyewearCPMGetActiveProfile_m3459 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMSetActiveProfile(System.Int32)
 bool QCARNativeIosWrapper_EyewearCPMSetActiveProfile_m3460 (QCARNativeIosWrapper_t750 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearCPMGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_EyewearCPMGetProjectionMatrix_m3461 (QCARNativeIosWrapper_t750 * __this, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMSetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 bool QCARNativeIosWrapper_EyewearCPMSetProjectionMatrix_m3462 (QCARNativeIosWrapper_t750 * __this, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::EyewearCPMGetProfileName(System.Int32)
 IntPtr_t121 QCARNativeIosWrapper_EyewearCPMGetProfileName_m3463 (QCARNativeIosWrapper_t750 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMSetProfileName(System.Int32,System.IntPtr)
 bool QCARNativeIosWrapper_EyewearCPMSetProfileName_m3464 (QCARNativeIosWrapper_t750 * __this, int32_t ___profileID, IntPtr_t121 ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMClearProfile(System.Int32)
 bool QCARNativeIosWrapper_EyewearCPMClearProfile_m3465 (QCARNativeIosWrapper_t750 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorInit(System.Int32,System.Int32,System.Int32,System.Int32)
 bool QCARNativeIosWrapper_EyewearUserCalibratorInit_m3466 (QCARNativeIosWrapper_t750 * __this, int32_t ___surfaceWidth, int32_t ___surfaceHeight, int32_t ___targetWidth, int32_t ___targetHeight, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorGetMinScaleHint()
 float QCARNativeIosWrapper_EyewearUserCalibratorGetMinScaleHint_m3467 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorGetMaxScaleHint()
 float QCARNativeIosWrapper_EyewearUserCalibratorGetMaxScaleHint_m3468 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorIsStereoStretched()
 bool QCARNativeIosWrapper_EyewearUserCalibratorIsStereoStretched_m3469 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorGetProjectionMatrix(System.IntPtr,System.Int32,System.IntPtr)
 bool QCARNativeIosWrapper_EyewearUserCalibratorGetProjectionMatrix_m3470 (QCARNativeIosWrapper_t750 * __this, IntPtr_t121 ___readingsArray, int32_t ___numReadings, IntPtr_t121 ___calibrationResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::smartTerrainTrackerStart()
 int32_t QCARNativeIosWrapper_smartTerrainTrackerStart_m3471 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::smartTerrainTrackerStop()
 void QCARNativeIosWrapper_smartTerrainTrackerStop_m3472 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainTrackerSetScaleToMillimeter(System.Single)
 bool QCARNativeIosWrapper_smartTerrainTrackerSetScaleToMillimeter_m3473 (Object_t * __this/* static, unused */, float ___scaleFactor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainTrackerInitBuilder()
 bool QCARNativeIosWrapper_smartTerrainTrackerInitBuilder_m3474 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainTrackerDeinitBuilder()
 bool QCARNativeIosWrapper_smartTerrainTrackerDeinitBuilder_m3475 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::smartTerrainBuilderCreateReconstructionFromTarget()
 IntPtr_t121 QCARNativeIosWrapper_smartTerrainBuilderCreateReconstructionFromTarget_m3476 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::smartTerrainBuilderCreateReconstructionFromEnvironment()
 IntPtr_t121 QCARNativeIosWrapper_smartTerrainBuilderCreateReconstructionFromEnvironment_m3477 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainBuilderAddReconstruction(System.IntPtr)
 bool QCARNativeIosWrapper_smartTerrainBuilderAddReconstruction_m3478 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainBuilderRemoveReconstruction(System.IntPtr)
 bool QCARNativeIosWrapper_smartTerrainBuilderRemoveReconstruction_m3479 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainBuilderDestroyReconstruction(System.IntPtr)
 bool QCARNativeIosWrapper_smartTerrainBuilderDestroyReconstruction_m3480 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionStart(System.IntPtr)
 bool QCARNativeIosWrapper_reconstructionStart_m3481 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionStop(System.IntPtr)
 bool QCARNativeIosWrapper_reconstructionStop_m3482 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionIsReconstructing(System.IntPtr)
 bool QCARNativeIosWrapper_reconstructionIsReconstructing_m3483 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionReset(System.IntPtr)
 bool QCARNativeIosWrapper_reconstructionReset_m3484 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::reconstructionSetNavMeshPadding(System.IntPtr,System.Single)
 void QCARNativeIosWrapper_reconstructionSetNavMeshPadding_m3485 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, float ___padding, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single)
 bool QCARNativeIosWrapper_reconstructionFromTargetSetInitializationTarget_m3486 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, IntPtr_t121 ___occluderMin, IntPtr_t121 ___occluderMax, IntPtr_t121 ___offsetToOccluder, IntPtr_t121 ___rotationAxisToOccluder, float ___rotationAngleToOccluder, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionSetMaximumArea(System.IntPtr,System.IntPtr)
 bool QCARNativeIosWrapper_reconstructionSetMaximumArea_m3487 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, IntPtr_t121 ___maximumArea, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::reconstructioFromEnvironmentGetReconstructionState(System.IntPtr)
 int32_t QCARNativeIosWrapper_reconstructioFromEnvironmentGetReconstructionState_m3488 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceInitCamera(System.Int32)
 int32_t QCARNativeIosWrapper_cameraDeviceInitCamera_m3489 (Object_t * __this/* static, unused */, int32_t ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceDeinitCamera()
 int32_t QCARNativeIosWrapper_cameraDeviceDeinitCamera_m3490 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceStartCamera()
 int32_t QCARNativeIosWrapper_cameraDeviceStartCamera_m3491 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceStopCamera()
 int32_t QCARNativeIosWrapper_cameraDeviceStopCamera_m3492 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceGetNumVideoModes()
 int32_t QCARNativeIosWrapper_cameraDeviceGetNumVideoModes_m3493 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::cameraDeviceGetVideoMode(System.Int32,System.IntPtr)
 void QCARNativeIosWrapper_cameraDeviceGetVideoMode_m3494 (Object_t * __this/* static, unused */, int32_t ___idx, IntPtr_t121 ___videoMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceSelectVideoMode(System.Int32)
 int32_t QCARNativeIosWrapper_cameraDeviceSelectVideoMode_m3495 (Object_t * __this/* static, unused */, int32_t ___idx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceSetFlashTorchMode(System.Int32)
 int32_t QCARNativeIosWrapper_cameraDeviceSetFlashTorchMode_m3496 (Object_t * __this/* static, unused */, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceSetFocusMode(System.Int32)
 int32_t QCARNativeIosWrapper_cameraDeviceSetFocusMode_m3497 (Object_t * __this/* static, unused */, int32_t ___focusMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceSetCameraConfiguration(System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_cameraDeviceSetCameraConfiguration_m3498 (Object_t * __this/* static, unused */, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarSetFrameFormat(System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_qcarSetFrameFormat_m3499 (Object_t * __this/* static, unused */, int32_t ___format, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetExists(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_dataSetExists_m3500 (Object_t * __this/* static, unused */, String_t* ___relativePath, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetLoad(System.String,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_dataSetLoad_m3501 (Object_t * __this/* static, unused */, String_t* ___relativePath, int32_t ___storageType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetGetNumTrackableType(System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_dataSetGetNumTrackableType_m3502 (Object_t * __this/* static, unused */, int32_t ___trackableType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_dataSetGetTrackablesOfType_m3503 (Object_t * __this/* static, unused */, int32_t ___trackableType, IntPtr_t121 ___trackableDataArray, int32_t ___trackableDataArrayLength, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNativeIosWrapper_dataSetGetTrackableName_m3504 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, StringBuilder_t466 * ___trackableName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetCreateTrackable(System.IntPtr,System.IntPtr,System.Text.StringBuilder,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_dataSetCreateTrackable_m3505 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, IntPtr_t121 ___trackableSourcePtr, StringBuilder_t466 * ___trackableName, int32_t ___nameMaxLength, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetDestroyTrackable(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_dataSetDestroyTrackable_m3506 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetHasReachedTrackableLimit(System.IntPtr)
 int32_t QCARNativeIosWrapper_dataSetHasReachedTrackableLimit_m3507 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::getCameraThreadID()
 int32_t QCARNativeIosWrapper_getCameraThreadID_m3508 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetBuilderBuild(System.String,System.Single)
 int32_t QCARNativeIosWrapper_imageTargetBuilderBuild_m3509 (Object_t * __this/* static, unused */, String_t* ___targetName, float ___sceenSizeWidth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::frameCounterGetBenchmarkingData(System.IntPtr,System.Boolean)
 void QCARNativeIosWrapper_frameCounterGetBenchmarkingData_m3510 (Object_t * __this/* static, unused */, IntPtr_t121 ___benchmarkingData, bool ___isStereoRendering, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::imageTargetBuilderStartScan()
 void QCARNativeIosWrapper_imageTargetBuilderStartScan_m3511 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::imageTargetBuilderStopScan()
 void QCARNativeIosWrapper_imageTargetBuilderStopScan_m3512 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetBuilderGetFrameQuality()
 int32_t QCARNativeIosWrapper_imageTargetBuilderGetFrameQuality_m3513 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::imageTargetBuilderGetTrackableSource()
 IntPtr_t121 QCARNativeIosWrapper_imageTargetBuilderGetTrackableSource_m3514 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_imageTargetCreateVirtualButton_m3515 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String)
 int32_t QCARNativeIosWrapper_imageTargetDestroyVirtualButton_m3516 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::virtualButtonGetId(System.IntPtr,System.String,System.String)
 int32_t QCARNativeIosWrapper_virtualButtonGetId_m3517 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetGetNumVirtualButtons(System.IntPtr,System.String)
 int32_t QCARNativeIosWrapper_imageTargetGetNumVirtualButtons_m3518 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String)
 int32_t QCARNativeIosWrapper_imageTargetGetVirtualButtons_m3519 (Object_t * __this/* static, unused */, IntPtr_t121 ___virtualButtonDataArray, IntPtr_t121 ___rectangleDataArray, int32_t ___virtualButtonDataArrayLength, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNativeIosWrapper_imageTargetGetVirtualButtonName_m3520 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, int32_t ___idx, StringBuilder_t466 * ___vbName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_cylinderTargetGetDimensions_m3521 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___dimensions, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cylinderTargetSetSideLength(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeIosWrapper_cylinderTargetSetSideLength_m3522 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___sideLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cylinderTargetSetTopDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeIosWrapper_cylinderTargetSetTopDiameter_m3523 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___topDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cylinderTargetSetBottomDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeIosWrapper_cylinderTargetSetBottomDiameter_m3524 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___bottomDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTargetSetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_objectTargetSetSize_m3525 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTargetGetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_objectTargetGetSize_m3526 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerStart()
 int32_t QCARNativeIosWrapper_objectTrackerStart_m3527 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::objectTrackerStop()
 void QCARNativeIosWrapper_objectTrackerStop_m3528 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::objectTrackerCreateDataSet()
 IntPtr_t121 QCARNativeIosWrapper_objectTrackerCreateDataSet_m3529 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerDestroyDataSet(System.IntPtr)
 int32_t QCARNativeIosWrapper_objectTrackerDestroyDataSet_m3530 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerActivateDataSet(System.IntPtr)
 int32_t QCARNativeIosWrapper_objectTrackerActivateDataSet_m3531 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerDeactivateDataSet(System.IntPtr)
 int32_t QCARNativeIosWrapper_objectTrackerDeactivateDataSet_m3532 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerPersistExtendedTracking(System.Int32)
 int32_t QCARNativeIosWrapper_objectTrackerPersistExtendedTracking_m3533 (Object_t * __this/* static, unused */, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerResetExtendedTracking()
 int32_t QCARNativeIosWrapper_objectTrackerResetExtendedTracking_m3534 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::markerSetSize(System.Int32,System.Single)
 int32_t QCARNativeIosWrapper_markerSetSize_m3535 (Object_t * __this/* static, unused */, int32_t ___trackableIndex, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::markerTrackerStart()
 int32_t QCARNativeIosWrapper_markerTrackerStart_m3536 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::markerTrackerStop()
 void QCARNativeIosWrapper_markerTrackerStop_m3537 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::markerTrackerCreateMarker(System.Int32,System.String,System.Single)
 int32_t QCARNativeIosWrapper_markerTrackerCreateMarker_m3538 (Object_t * __this/* static, unused */, int32_t ___id, String_t* ___trackableName, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::markerTrackerDestroyMarker(System.Int32)
 int32_t QCARNativeIosWrapper_markerTrackerDestroyMarker_m3539 (Object_t * __this/* static, unused */, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::initPlatformNative()
 void QCARNativeIosWrapper_initPlatformNative_m3540 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::initFrameState(System.IntPtr)
 void QCARNativeIosWrapper_initFrameState_m3541 (Object_t * __this/* static, unused */, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::deinitFrameState(System.IntPtr)
 void QCARNativeIosWrapper_deinitFrameState_m3542 (Object_t * __this/* static, unused */, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::onSurfaceChanged(System.Int32,System.Int32)
 void QCARNativeIosWrapper_onSurfaceChanged_m3543 (Object_t * __this/* static, unused */, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::onPause()
 void QCARNativeIosWrapper_onPause_m3544 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::onResume()
 void QCARNativeIosWrapper_onResume_m3545 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::hasSurfaceBeenRecreated()
 bool QCARNativeIosWrapper_hasSurfaceBeenRecreated_m3546 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::updateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_updateQCAR_m3547 (Object_t * __this/* static, unused */, IntPtr_t121 ___imageHeaderDataArray, int32_t ___imageHeaderArrayLength, IntPtr_t121 ___frameIndex, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::rendererEnd()
 void QCARNativeIosWrapper_rendererEnd_m3548 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarGetBufferSize(System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_qcarGetBufferSize_m3549 (Object_t * __this/* static, unused */, int32_t ___width, int32_t ___height, int32_t ___format, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::qcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 void QCARNativeIosWrapper_qcarAddCameraFrame_m3550 (Object_t * __this/* static, unused */, IntPtr_t121 ___pixels, int32_t ___width, int32_t ___height, int32_t ___format, int32_t ___stride, int32_t ___frameIdx, int32_t ___flipHorizontally, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::rendererSetVideoBackgroundCfg(System.IntPtr)
 void QCARNativeIosWrapper_rendererSetVideoBackgroundCfg_m3551 (Object_t * __this/* static, unused */, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::rendererGetVideoBackgroundCfg(System.IntPtr)
 void QCARNativeIosWrapper_rendererGetVideoBackgroundCfg_m3552 (Object_t * __this/* static, unused */, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::rendererGetVideoBackgroundTextureInfo(System.IntPtr)
 void QCARNativeIosWrapper_rendererGetVideoBackgroundTextureInfo_m3553 (Object_t * __this/* static, unused */, IntPtr_t121 ___texInfo, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::rendererSetVideoBackgroundTextureID(System.Int32)
 int32_t QCARNativeIosWrapper_rendererSetVideoBackgroundTextureID_m3554 (Object_t * __this/* static, unused */, int32_t ___textureID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::rendererIsVideoBackgroundTextureInfoAvailable()
 int32_t QCARNativeIosWrapper_rendererIsVideoBackgroundTextureInfoAvailable_m3555 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarInit(System.String)
 int32_t QCARNativeIosWrapper_qcarInit_m3556 (Object_t * __this/* static, unused */, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarSetHint(System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_qcarSetHint_m3557 (Object_t * __this/* static, unused */, int32_t ___hint, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::getProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_getProjectionGL_m3558 (Object_t * __this/* static, unused */, float ___nearClip, float ___farClip, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::setApplicationEnvironment(System.Int32,System.Int32,System.Int32)
 void QCARNativeIosWrapper_setApplicationEnvironment_m3559 (Object_t * __this/* static, unused */, int32_t ___unityVersionMajor, int32_t ___unityVersionMinor, int32_t ___unityVersionChange, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::setStateBufferSize(System.Int32)
 void QCARNativeIosWrapper_setStateBufferSize_m3560 (Object_t * __this/* static, unused */, int32_t ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderStartInit(System.String,System.String)
 int32_t QCARNativeIosWrapper_targetFinderStartInit_m3561 (Object_t * __this/* static, unused */, String_t* ___userKey, String_t* ___secretKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderGetInitState()
 int32_t QCARNativeIosWrapper_targetFinderGetInitState_m3562 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderDeinit()
 int32_t QCARNativeIosWrapper_targetFinderDeinit_m3563 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderStartRecognition()
 int32_t QCARNativeIosWrapper_targetFinderStartRecognition_m3564 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderStop()
 int32_t QCARNativeIosWrapper_targetFinderStop_m3565 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderSetUIScanlineColor(System.Single,System.Single,System.Single)
 void QCARNativeIosWrapper_targetFinderSetUIScanlineColor_m3566 (Object_t * __this/* static, unused */, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderSetUIPointColor(System.Single,System.Single,System.Single)
 void QCARNativeIosWrapper_targetFinderSetUIPointColor_m3567 (Object_t * __this/* static, unused */, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderUpdate(System.IntPtr)
 void QCARNativeIosWrapper_targetFinderUpdate_m3568 (Object_t * __this/* static, unused */, IntPtr_t121 ___targetFinderState, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderGetResults(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_targetFinderGetResults_m3569 (Object_t * __this/* static, unused */, IntPtr_t121 ___searchResultArray, int32_t ___searchResultArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderEnableTracking(System.IntPtr,System.IntPtr)
 int32_t QCARNativeIosWrapper_targetFinderEnableTracking_m3570 (Object_t * __this/* static, unused */, IntPtr_t121 ___searchResult, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderGetImageTargets(System.IntPtr,System.Int32)
 void QCARNativeIosWrapper_targetFinderGetImageTargets_m3571 (Object_t * __this/* static, unused */, IntPtr_t121 ___trackableIdArray, int32_t ___trackableIdArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderClearTrackables()
 void QCARNativeIosWrapper_targetFinderClearTrackables_m3572 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::textTrackerStart()
 int32_t QCARNativeIosWrapper_textTrackerStart_m3573 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::textTrackerStop()
 void QCARNativeIosWrapper_textTrackerStop_m3574 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::textTrackerSetRegionOfInterest(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_textTrackerSetRegionOfInterest_m3575 (Object_t * __this/* static, unused */, int32_t ___detectionLeftTopX, int32_t ___detectionLeftTopY, int32_t ___detectionRightBottomX, int32_t ___detectionRightBottomY, int32_t ___trackingLeftTopX, int32_t ___trackingLeftTopY, int32_t ___trackingRightBottomX, int32_t ___trackingRightBottomY, int32_t ___upDirection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::textTrackerGetRegionOfInterest(System.IntPtr,System.IntPtr)
 int32_t QCARNativeIosWrapper_textTrackerGetRegionOfInterest_m3576 (Object_t * __this/* static, unused */, IntPtr_t121 ___detectionROI, IntPtr_t121 ___trackingROI, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListLoadWordList(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_wordListLoadWordList_m3577 (Object_t * __this/* static, unused */, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListAddWordsFromFile(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_wordListAddWordsFromFile_m3578 (Object_t * __this/* static, unused */, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListAddWordU(System.IntPtr)
 int32_t QCARNativeIosWrapper_wordListAddWordU_m3579 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListRemoveWordU(System.IntPtr)
 int32_t QCARNativeIosWrapper_wordListRemoveWordU_m3580 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListContainsWordU(System.IntPtr)
 int32_t QCARNativeIosWrapper_wordListContainsWordU_m3581 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListUnloadAllLists()
 int32_t QCARNativeIosWrapper_wordListUnloadAllLists_m3582 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListSetFilterMode(System.Int32)
 int32_t QCARNativeIosWrapper_wordListSetFilterMode_m3583 (Object_t * __this/* static, unused */, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListGetFilterMode()
 int32_t QCARNativeIosWrapper_wordListGetFilterMode_m3584 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListAddWordToFilterListU(System.IntPtr)
 int32_t QCARNativeIosWrapper_wordListAddWordToFilterListU_m3585 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListRemoveWordFromFilterListU(System.IntPtr)
 int32_t QCARNativeIosWrapper_wordListRemoveWordFromFilterListU_m3586 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListClearFilterList()
 int32_t QCARNativeIosWrapper_wordListClearFilterList_m3587 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListLoadFilterList(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_wordListLoadFilterList_m3588 (Object_t * __this/* static, unused */, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListGetFilterListWordCount()
 int32_t QCARNativeIosWrapper_wordListGetFilterListWordCount_m3589 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::wordListGetFilterListWordU(System.Int32)
 IntPtr_t121 QCARNativeIosWrapper_wordListGetFilterListWordU_m3590 (Object_t * __this/* static, unused */, int32_t ___i, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordGetLetterMask(System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_wordGetLetterMask_m3591 (Object_t * __this/* static, unused */, int32_t ___wordID, IntPtr_t121 ___letterMaskImage, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordGetLetterBoundingBoxes(System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_wordGetLetterBoundingBoxes_m3592 (Object_t * __this/* static, unused */, int32_t ___wordID, IntPtr_t121 ___letterBoundingBoxes, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::trackerManagerInitTracker(System.Int32)
 int32_t QCARNativeIosWrapper_trackerManagerInitTracker_m3593 (Object_t * __this/* static, unused */, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::trackerManagerDeinitTracker(System.Int32)
 int32_t QCARNativeIosWrapper_trackerManagerDeinitTracker_m3594 (Object_t * __this/* static, unused */, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::virtualButtonSetEnabled(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNativeIosWrapper_virtualButtonSetEnabled_m3595 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::virtualButtonSetSensitivity(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNativeIosWrapper_virtualButtonSetSensitivity_m3596 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___sensitivity, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::virtualButtonSetAreaRectangle(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_virtualButtonSetAreaRectangle_m3597 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarDeinit()
 int32_t QCARNativeIosWrapper_qcarDeinit_m3598 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::startExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_startExtendedTracking_m3599 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::stopExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_stopExtendedTracking_m3600 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsSupportedDeviceDetected()
 int32_t QCARNativeIosWrapper_eyewearIsSupportedDeviceDetected_m3601 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsSeeThru()
 int32_t QCARNativeIosWrapper_eyewearIsSeeThru_m3602 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearGetScreenOrientation()
 int32_t QCARNativeIosWrapper_eyewearGetScreenOrientation_m3603 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsStereoCapable()
 int32_t QCARNativeIosWrapper_eyewearIsStereoCapable_m3604 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsStereoEnabled()
 int32_t QCARNativeIosWrapper_eyewearIsStereoEnabled_m3605 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsStereoGLOnly()
 int32_t QCARNativeIosWrapper_eyewearIsStereoGLOnly_m3606 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearSetStereo(System.Boolean)
 int32_t QCARNativeIosWrapper_eyewearSetStereo_m3607 (Object_t * __this/* static, unused */, bool ___enable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearGetDefaultSceneScale(System.IntPtr)
 int32_t QCARNativeIosWrapper_eyewearGetDefaultSceneScale_m3608 (Object_t * __this/* static, unused */, IntPtr_t121 ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_eyewearGetProjectionMatrix_m3609 (Object_t * __this/* static, unused */, int32_t ___eyeID, int32_t ___profileID, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMGetMaxCount()
 int32_t QCARNativeIosWrapper_eyewearCPMGetMaxCount_m3610 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMGetUsedCount()
 int32_t QCARNativeIosWrapper_eyewearCPMGetUsedCount_m3611 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMIsProfileUsed(System.Int32)
 int32_t QCARNativeIosWrapper_eyewearCPMIsProfileUsed_m3612 (Object_t * __this/* static, unused */, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMGetActiveProfile()
 int32_t QCARNativeIosWrapper_eyewearCPMGetActiveProfile_m3613 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMSetActiveProfile(System.Int32)
 int32_t QCARNativeIosWrapper_eyewearCPMSetActiveProfile_m3614 (Object_t * __this/* static, unused */, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_eyewearCPMGetProjectionMatrix_m3615 (Object_t * __this/* static, unused */, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMSetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_eyewearCPMSetProjectionMatrix_m3616 (Object_t * __this/* static, unused */, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::eyewearCPMGetProfileName(System.Int32)
 IntPtr_t121 QCARNativeIosWrapper_eyewearCPMGetProfileName_m3617 (Object_t * __this/* static, unused */, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMSetProfileName(System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_eyewearCPMSetProfileName_m3618 (Object_t * __this/* static, unused */, int32_t ___profileID, IntPtr_t121 ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMClearProfile(System.Int32)
 int32_t QCARNativeIosWrapper_eyewearCPMClearProfile_m3619 (Object_t * __this/* static, unused */, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorInit(System.Int32,System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_eyewearUserCalibratorInit_m3620 (Object_t * __this/* static, unused */, int32_t ___surfaceWidth, int32_t ___surfaceHeight, int32_t ___targetWidth, int32_t ___targetHeight, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorGetMinScaleHint()
 float QCARNativeIosWrapper_eyewearUserCalibratorGetMinScaleHint_m3621 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorGetMaxScaleHint()
 float QCARNativeIosWrapper_eyewearUserCalibratorGetMaxScaleHint_m3622 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorIsStereoStretched()
 int32_t QCARNativeIosWrapper_eyewearUserCalibratorIsStereoStretched_m3623 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorGetProjectionMatrix(System.IntPtr,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_eyewearUserCalibratorGetProjectionMatrix_m3624 (Object_t * __this/* static, unused */, IntPtr_t121 ___readingsArray, int32_t ___numReadings, IntPtr_t121 ___calibrationResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::.ctor()
 void QCARNativeIosWrapper__ctor_m3625 (QCARNativeIosWrapper_t750 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
