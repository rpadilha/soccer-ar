﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Transform/Enumerator
struct Enumerator_t1046;
// System.Object
struct Object_t;
// UnityEngine.Transform
struct Transform_t74;

// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
 void Enumerator__ctor_m6200 (Enumerator_t1046 * __this, Transform_t74 * ___outer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Transform/Enumerator::get_Current()
 Object_t * Enumerator_get_Current_m6201 (Enumerator_t1046 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
 bool Enumerator_MoveNext_m6202 (Enumerator_t1046 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
