﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Toggle>
struct EqualityComparer_1_t3718;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Toggle>
struct EqualityComparer_1_t3718  : public Object_t
{
};
struct EqualityComparer_1_t3718_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Toggle>::_default
	EqualityComparer_1_t3718 * ____default_0;
};
