﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t28;

// System.Void Vuforia.MaskOutAbstractBehaviour::.ctor()
 void MaskOutAbstractBehaviour__ctor_m360 (MaskOutAbstractBehaviour_t28 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
