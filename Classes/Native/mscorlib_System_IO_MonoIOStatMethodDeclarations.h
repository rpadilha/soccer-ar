﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.MonoIOStat
struct MonoIOStat_t1932;
struct MonoIOStat_t1932_marshaled;

void MonoIOStat_t1932_marshal(const MonoIOStat_t1932& unmarshaled, MonoIOStat_t1932_marshaled& marshaled);
void MonoIOStat_t1932_marshal_back(const MonoIOStat_t1932_marshaled& marshaled, MonoIOStat_t1932& unmarshaled);
void MonoIOStat_t1932_marshal_cleanup(MonoIOStat_t1932_marshaled& marshaled);
