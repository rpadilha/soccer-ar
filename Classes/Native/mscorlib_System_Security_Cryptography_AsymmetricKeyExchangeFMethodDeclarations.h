﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.AsymmetricKeyExchangeFormatter
struct AsymmetricKeyExchangeFormatter_t2142;
// System.Byte[]
struct ByteU5BU5D_t653;

// System.Void System.Security.Cryptography.AsymmetricKeyExchangeFormatter::.ctor()
 void AsymmetricKeyExchangeFormatter__ctor_m12022 (AsymmetricKeyExchangeFormatter_t2142 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.AsymmetricKeyExchangeFormatter::CreateKeyExchange(System.Byte[])
