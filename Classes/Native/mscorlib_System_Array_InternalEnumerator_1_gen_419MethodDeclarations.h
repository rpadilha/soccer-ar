﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.ScaleMode>
struct InternalEnumerator_1_t4678;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.ScaleMode
#include "UnityEngine_UnityEngine_ScaleMode.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.ScaleMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m28240 (InternalEnumerator_1_t4678 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ScaleMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28241 (InternalEnumerator_1_t4678 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ScaleMode>::Dispose()
 void InternalEnumerator_1_Dispose_m28242 (InternalEnumerator_1_t4678 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ScaleMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m28243 (InternalEnumerator_1_t4678 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.ScaleMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28244 (InternalEnumerator_1_t4678 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
