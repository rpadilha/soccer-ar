﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackerAbstractBehaviour
struct SmartTerrainTrackerAbstractBehaviour_t50;
// System.Action
struct Action_t147;

// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Awake()
 void SmartTerrainTrackerAbstractBehaviour_Awake_m2926 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnEnable()
 void SmartTerrainTrackerAbstractBehaviour_OnEnable_m2927 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDisable()
 void SmartTerrainTrackerAbstractBehaviour_OnDisable_m2928 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDestroy()
 void SmartTerrainTrackerAbstractBehaviour_OnDestroy_m2929 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::RegisterTrackerStartedCallback(System.Action)
 void SmartTerrainTrackerAbstractBehaviour_RegisterTrackerStartedCallback_m2930 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, Action_t147 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::UnregisterTrackerStartedCallback(System.Action)
 void SmartTerrainTrackerAbstractBehaviour_UnregisterTrackerStartedCallback_m2931 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, Action_t147 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StartSmartTerrainTracker()
 void SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m2932 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StopSmartTerrainTracker()
 void SmartTerrainTrackerAbstractBehaviour_StopSmartTerrainTracker_m2933 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::InitSmartTerrainTracker()
 void SmartTerrainTrackerAbstractBehaviour_InitSmartTerrainTracker_m2934 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnQCARInitialized()
 void SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2935 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnQCARStarted()
 void SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2936 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnPause(System.Boolean)
 void SmartTerrainTrackerAbstractBehaviour_OnPause_m2937 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, bool ___pause, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetAutomaticStart(System.Boolean)
 void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetAutomaticStart_m449 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, bool ___autoStart, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_AutomaticStart()
 bool SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_AutomaticStart_m450 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetSmartTerrainScaleToMM(System.Single)
 void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetSmartTerrainScaleToMM_m451 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, float ___scaleToMM, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_ScaleToMM()
 float SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_ScaleToMM_m452 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::.ctor()
 void SmartTerrainTrackerAbstractBehaviour__ctor_m448 (SmartTerrainTrackerAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
