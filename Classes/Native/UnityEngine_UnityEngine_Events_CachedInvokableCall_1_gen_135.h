﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.WordAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_137.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.WordAbstractBehaviour>
struct CachedInvokableCall_1_t4599  : public InvokableCall_1_t4600
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.WordAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
