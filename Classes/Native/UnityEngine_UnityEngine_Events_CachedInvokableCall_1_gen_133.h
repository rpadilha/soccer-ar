﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_135.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonAbstractBehaviour>
struct CachedInvokableCall_1_t4578  : public InvokableCall_1_t4579
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
