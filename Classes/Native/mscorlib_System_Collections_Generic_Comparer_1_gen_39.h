﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.Prop>
struct Comparer_1_t4318;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.Prop>
struct Comparer_1_t4318  : public Object_t
{
};
struct Comparer_1_t4318_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.Prop>::_default
	Comparer_1_t4318 * ____default_0;
};
