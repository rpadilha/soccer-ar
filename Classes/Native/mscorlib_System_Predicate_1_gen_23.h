﻿#pragma once
#include <stdint.h>
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t607;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.ICloudRecoEventHandler>
struct Predicate_1_t3867  : public MulticastDelegate_t373
{
};
