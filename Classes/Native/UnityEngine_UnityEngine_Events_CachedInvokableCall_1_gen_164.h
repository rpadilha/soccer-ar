﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_166.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider>
struct CachedInvokableCall_1_t4811  : public InvokableCall_1_t4812
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
