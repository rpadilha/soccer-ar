﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<UnityEngine.UI.Toggle>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3723;
// UnityEngine.UI.Toggle
struct Toggle_t418;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Toggle>
struct IEnumerator_1_t3711;

// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<UnityEngine.UI.Toggle>::.ctor()
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereIteratorU3E_0MethodDeclarations.h"
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m20356(__this, method) (void)U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m20349_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 *)__this, method)
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<UnityEngine.UI.Toggle>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m20357(__this, method) (Toggle_t418 *)U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m20350_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 *)__this, method)
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<UnityEngine.UI.Toggle>::System.Collections.IEnumerator.get_Current()
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m20358(__this, method) (Object_t *)U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m20351_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 *)__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<UnityEngine.UI.Toggle>::System.Collections.IEnumerable.GetEnumerator()
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m20359(__this, method) (Object_t *)U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m20352_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 *)__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<UnityEngine.UI.Toggle>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m20360(__this, method) (Object_t*)U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m20353_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 *)__this, method)
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<UnityEngine.UI.Toggle>::MoveNext()
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m20361(__this, method) (bool)U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m20354_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 *)__this, method)
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<UnityEngine.UI.Toggle>::Dispose()
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m20362(__this, method) (void)U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m20355_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3722 *)__this, method)
