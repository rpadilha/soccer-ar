﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$20
struct $ArrayType$20_t1702;
struct $ArrayType$20_t1702_marshaled;

void $ArrayType$20_t1702_marshal(const $ArrayType$20_t1702& unmarshaled, $ArrayType$20_t1702_marshaled& marshaled);
void $ArrayType$20_t1702_marshal_back(const $ArrayType$20_t1702_marshaled& marshaled, $ArrayType$20_t1702& unmarshaled);
void $ArrayType$20_t1702_marshal_cleanup($ArrayType$20_t1702_marshaled& marshaled);
