﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$136
struct $ArrayType$136_t1280;
struct $ArrayType$136_t1280_marshaled;

void $ArrayType$136_t1280_marshal(const $ArrayType$136_t1280& unmarshaled, $ArrayType$136_t1280_marshaled& marshaled);
void $ArrayType$136_t1280_marshal_back(const $ArrayType$136_t1280_marshaled& marshaled, $ArrayType$136_t1280& unmarshaled);
void $ArrayType$136_t1280_marshal_cleanup($ArrayType$136_t1280_marshaled& marshaled);
