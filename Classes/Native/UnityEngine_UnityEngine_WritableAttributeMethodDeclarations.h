﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WritableAttribute
struct WritableAttribute_t1092;

// System.Void UnityEngine.WritableAttribute::.ctor()
 void WritableAttribute__ctor_m6418 (WritableAttribute_t1092 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
