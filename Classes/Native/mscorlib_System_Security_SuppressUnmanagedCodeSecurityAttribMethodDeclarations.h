﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.SuppressUnmanagedCodeSecurityAttribute
struct SuppressUnmanagedCodeSecurityAttribute_t2191;

// System.Void System.Security.SuppressUnmanagedCodeSecurityAttribute::.ctor()
 void SuppressUnmanagedCodeSecurityAttribute__ctor_m12383 (SuppressUnmanagedCodeSecurityAttribute_t2191 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
