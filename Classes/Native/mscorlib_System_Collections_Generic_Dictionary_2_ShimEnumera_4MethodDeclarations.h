﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct ShimEnumerator_t3496;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct Dictionary_2_t334;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_3MethodDeclarations.h"
#define ShimEnumerator__ctor_m18673(__this, ___host, method) (void)ShimEnumerator__ctor_m18416_gshared((ShimEnumerator_t3466 *)__this, (Dictionary_2_t3452 *)___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::MoveNext()
#define ShimEnumerator_MoveNext_m18674(__this, method) (bool)ShimEnumerator_MoveNext_m18417_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Entry()
#define ShimEnumerator_get_Entry_m18675(__this, method) (DictionaryEntry_t1355 )ShimEnumerator_get_Entry_m18418_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Key()
#define ShimEnumerator_get_Key_m18676(__this, method) (Object_t *)ShimEnumerator_get_Key_m18419_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Value()
#define ShimEnumerator_get_Value_m18677(__this, method) (Object_t *)ShimEnumerator_get_Value_m18420_gshared((ShimEnumerator_t3466 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Current()
#define ShimEnumerator_get_Current_m18678(__this, method) (Object_t *)ShimEnumerator_get_Current_m18421_gshared((ShimEnumerator_t3466 *)__this, method)
