﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.VirtualButtonAbstractBehaviour>
struct Comparer_1_t4390;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.VirtualButtonAbstractBehaviour>
struct Comparer_1_t4390  : public Object_t
{
};
struct Comparer_1_t4390_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.VirtualButtonAbstractBehaviour>::_default
	Comparer_1_t4390 * ____default_0;
};
