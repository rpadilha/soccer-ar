﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackable>
struct IList_1_t4096;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.Collection`1<Vuforia.SmartTerrainTrackable>
struct Collection_1_t4097  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.SmartTerrainTrackable>::list
	Object_t* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.SmartTerrainTrackable>::syncRoot
	Object_t * ___syncRoot_1;
};
