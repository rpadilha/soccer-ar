﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t954;
// UnityEngine.Object
struct Object_t120;
struct Object_t120_marshaled;
// UnityEngine.Object[]
struct ObjectU5BU5D_t227;

// System.Void UnityEngine.AssetBundleRequest::.ctor()
 void AssetBundleRequest__ctor_m5573 (AssetBundleRequest_t954 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
 Object_t120 * AssetBundleRequest_get_asset_m5574 (AssetBundleRequest_t954 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
 ObjectU5BU5D_t227* AssetBundleRequest_get_allAssets_m5575 (AssetBundleRequest_t954 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
