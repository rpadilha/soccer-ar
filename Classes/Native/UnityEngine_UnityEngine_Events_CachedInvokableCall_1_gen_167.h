﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.CharacterController>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_169.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.CharacterController>
struct CachedInvokableCall_1_t4824  : public InvokableCall_1_t4825
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.CharacterController>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
