﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.QCARAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_129.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARAbstractBehaviour>
struct CachedInvokableCall_1_t4485  : public InvokableCall_1_t4486
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
