﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIContent
struct GUIContent_t529;
// System.String
struct String_t;

// System.Void UnityEngine.GUIContent::.ctor()
 void GUIContent__ctor_m5866 (GUIContent_t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor(System.String)
 void GUIContent__ctor_m2447 (GUIContent_t529 * __this, String_t* ___text, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.cctor()
 void GUIContent__cctor_m5867 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUIContent::get_text()
 String_t* GUIContent_get_text_m2446 (GUIContent_t529 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::set_text(System.String)
 void GUIContent_set_text_m5868 (GUIContent_t529 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(System.String)
 GUIContent_t529 * GUIContent_Temp_m5869 (Object_t * __this/* static, unused */, String_t* ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::ClearStaticCache()
 void GUIContent_ClearStaticCache_m5870 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
