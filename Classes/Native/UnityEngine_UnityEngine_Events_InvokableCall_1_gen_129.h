﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.QCARAbstractBehaviour>
struct UnityAction_1_t4487;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.QCARAbstractBehaviour>
struct InvokableCall_1_t4486  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.QCARAbstractBehaviour>::Delegate
	UnityAction_1_t4487 * ___Delegate_0;
};
