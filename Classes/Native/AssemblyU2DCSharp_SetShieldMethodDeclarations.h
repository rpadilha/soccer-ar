﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SetShield
struct SetShield_t91;

// System.Void SetShield::.ctor()
 void SetShield__ctor_m147 (SetShield_t91 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetShield::Start()
 void SetShield_Start_m148 (SetShield_t91 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetShield::Update()
 void SetShield_Update_m149 (SetShield_t91 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
