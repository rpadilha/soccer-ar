﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableBehaviour[]
struct TrackableBehaviourU5BU5D_t857;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>
struct List_1_t772  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::_items
	TrackableBehaviourU5BU5D_t857* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::_version
	int32_t ____version_3;
};
struct List_1_t772_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::EmptyArray
	TrackableBehaviourU5BU5D_t857* ___EmptyArray_4;
};
