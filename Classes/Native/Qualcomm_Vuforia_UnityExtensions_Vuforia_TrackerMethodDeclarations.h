﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.Tracker
struct Tracker_t659;

// System.Boolean Vuforia.Tracker::Start()
// System.Void Vuforia.Tracker::Stop()
// System.Boolean Vuforia.Tracker::get_IsActive()
 bool Tracker_get_IsActive_m3087 (Tracker_t659 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Tracker::set_IsActive(System.Boolean)
 void Tracker_set_IsActive_m3088 (Tracker_t659 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.Tracker::.ctor()
 void Tracker__ctor_m3089 (Tracker_t659 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
