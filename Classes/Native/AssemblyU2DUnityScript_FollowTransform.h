﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// FollowTransform
struct FollowTransform_t212  : public MonoBehaviour_t10
{
	// UnityEngine.Transform FollowTransform::targetTransform
	Transform_t74 * ___targetTransform_2;
	// System.Boolean FollowTransform::faceForward
	bool ___faceForward_3;
	// UnityEngine.Transform FollowTransform::thisTransform
	Transform_t74 * ___thisTransform_4;
};
