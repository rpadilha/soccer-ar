﻿#pragma once
#include <stdint.h>
// UnityEngine.GUITexture
struct GUITexture_t100;
// Boundary
struct Boundary_t98;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Joystick_Script>
struct List_1_t101;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// Joystick_Script
struct Joystick_Script_t102  : public MonoBehaviour_t10
{
	// System.Boolean Joystick_Script::touchPad
	bool ___touchPad_2;
	// System.Boolean Joystick_Script::fadeGUI
	bool ___fadeGUI_3;
	// UnityEngine.Vector2 Joystick_Script::deadZone
	Vector2_t99  ___deadZone_4;
	// System.Boolean Joystick_Script::normalize
	bool ___normalize_5;
	// System.Int32 Joystick_Script::tapCount
	int32_t ___tapCount_6;
	// UnityEngine.Rect Joystick_Script::touchZone
	Rect_t103  ___touchZone_7;
	// System.Int32 Joystick_Script::lastFingerId
	int32_t ___lastFingerId_8;
	// System.Single Joystick_Script::tapTimeWindow
	float ___tapTimeWindow_9;
	// UnityEngine.Vector2 Joystick_Script::fingerDownPos
	Vector2_t99  ___fingerDownPos_10;
	// System.Single Joystick_Script::firstDeltaTime
	float ___firstDeltaTime_11;
	// UnityEngine.GUITexture Joystick_Script::gui
	GUITexture_t100 * ___gui_12;
	// UnityEngine.Rect Joystick_Script::defaultRect
	Rect_t103  ___defaultRect_13;
	// Boundary Joystick_Script::guiBoundary
	Boundary_t98 * ___guiBoundary_14;
	// UnityEngine.Vector2 Joystick_Script::guiTouchOffset
	Vector2_t99  ___guiTouchOffset_15;
	// UnityEngine.Vector2 Joystick_Script::guiCenter
	Vector2_t99  ___guiCenter_16;
	// UnityEngine.Vector2 Joystick_Script::<position>k__BackingField
	Vector2_t99  ___U3CpositionU3Ek__BackingField_21;
};
struct Joystick_Script_t102_StaticFields{
	// System.String Joystick_Script::joysticksTag
	String_t* ___joysticksTag_17;
	// System.Collections.Generic.List`1<Joystick_Script> Joystick_Script::joysticks
	List_1_t101 * ___joysticks_18;
	// System.Boolean Joystick_Script::enumeratedJoysticks
	bool ___enumeratedJoysticks_19;
	// System.Single Joystick_Script::tapTimeDelta
	float ___tapTimeDelta_20;
};
