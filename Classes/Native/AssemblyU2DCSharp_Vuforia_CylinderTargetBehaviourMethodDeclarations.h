﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CylinderTargetBehaviour
struct CylinderTargetBehaviour_t5;

// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
 void CylinderTargetBehaviour__ctor_m2 (CylinderTargetBehaviour_t5 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
