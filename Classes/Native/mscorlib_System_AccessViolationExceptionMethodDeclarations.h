﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AccessViolationException
struct AccessViolationException_t2230;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1118;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.AccessViolationException::.ctor()
 void AccessViolationException__ctor_m12676 (AccessViolationException_t2230 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AccessViolationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void AccessViolationException__ctor_m12677 (AccessViolationException_t2230 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
