﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// ZoomCamera
struct ZoomCamera_t224  : public MonoBehaviour_t10
{
	// UnityEngine.Transform ZoomCamera::origin
	Transform_t74 * ___origin_2;
	// System.Single ZoomCamera::zoom
	float ___zoom_3;
	// System.Single ZoomCamera::zoomMin
	float ___zoomMin_4;
	// System.Single ZoomCamera::zoomMax
	float ___zoomMax_5;
	// System.Single ZoomCamera::seekTime
	float ___seekTime_6;
	// System.Boolean ZoomCamera::smoothZoomIn
	bool ___smoothZoomIn_7;
	// UnityEngine.Vector3 ZoomCamera::defaultLocalPosition
	Vector3_t73  ___defaultLocalPosition_8;
	// UnityEngine.Transform ZoomCamera::thisTransform
	Transform_t74 * ___thisTransform_9;
	// System.Single ZoomCamera::currentZoom
	float ___currentZoom_10;
	// System.Single ZoomCamera::targetZoom
	float ___targetZoom_11;
	// System.Single ZoomCamera::zoomVelocity
	float ___zoomVelocity_12;
};
