﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.EyewearComponentFactory
struct EyewearComponentFactory_t586;
// Vuforia.IEyewearComponentFactory
struct IEyewearComponentFactory_t585;

// Vuforia.IEyewearComponentFactory Vuforia.EyewearComponentFactory::get_Instance()
 Object_t * EyewearComponentFactory_get_Instance_m2759 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.EyewearComponentFactory::set_Instance(Vuforia.IEyewearComponentFactory)
 void EyewearComponentFactory_set_Instance_m2760 (Object_t * __this/* static, unused */, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.EyewearComponentFactory::.ctor()
 void EyewearComponentFactory__ctor_m2761 (EyewearComponentFactory_t586 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
