﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo RaycastResult_t241_il2cpp_TypeInfo;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t3235  : public MulticastDelegate_t373
{
};
