﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackableSourceImpl
struct TrackableSourceImpl_t784;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.IntPtr Vuforia.TrackableSourceImpl::get_TrackableSourcePtr()
 IntPtr_t121 TrackableSourceImpl_get_TrackableSourcePtr_m4199 (TrackableSourceImpl_t784 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableSourceImpl::set_TrackableSourcePtr(System.IntPtr)
 void TrackableSourceImpl_set_TrackableSourcePtr_m4200 (TrackableSourceImpl_t784 * __this, IntPtr_t121 ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableSourceImpl::.ctor(System.IntPtr)
 void TrackableSourceImpl__ctor_m4201 (TrackableSourceImpl_t784 * __this, IntPtr_t121 ___trackableSourcePtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
