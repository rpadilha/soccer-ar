﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_5.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
struct CachedInvokableCall_1_t2793  : public InvokableCall_1_t2794
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
