﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
struct GcAchievementData_t974;
struct GcAchievementData_t974_marshaled;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t1096;

// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
 Achievement_t1096 * GcAchievementData_ToAchievement_m6423 (GcAchievementData_t974 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void GcAchievementData_t974_marshal(const GcAchievementData_t974& unmarshaled, GcAchievementData_t974_marshaled& marshaled);
void GcAchievementData_t974_marshal_back(const GcAchievementData_t974_marshaled& marshaled, GcAchievementData_t974& unmarshaled);
void GcAchievementData_t974_marshal_cleanup(GcAchievementData_t974_marshaled& marshaled);
