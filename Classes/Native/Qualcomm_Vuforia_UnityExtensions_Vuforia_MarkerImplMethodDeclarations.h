﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerImpl
struct MarkerImpl_t665;
// System.String
struct String_t;

// System.Int32 Vuforia.MarkerImpl::get_MarkerID()
 int32_t MarkerImpl_get_MarkerID_m3105 (MarkerImpl_t665 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerImpl::set_MarkerID(System.Int32)
 void MarkerImpl_set_MarkerID_m3106 (MarkerImpl_t665 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerImpl::.ctor(System.String,System.Int32,System.Single,System.Int32)
 void MarkerImpl__ctor_m3107 (MarkerImpl_t665 * __this, String_t* ___name, int32_t ___id, float ___size, int32_t ___markerID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.MarkerImpl::GetSize()
 float MarkerImpl_GetSize_m3108 (MarkerImpl_t665 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerImpl::SetSize(System.Single)
 void MarkerImpl_SetSize_m3109 (MarkerImpl_t665 * __this, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerImpl::StartExtendedTracking()
 bool MarkerImpl_StartExtendedTracking_m3110 (MarkerImpl_t665 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerImpl::StopExtendedTracking()
 bool MarkerImpl_StopExtendedTracking_m3111 (MarkerImpl_t665 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
