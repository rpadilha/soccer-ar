﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.InternalEyewear/EyeID
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_Eye.h"
// Vuforia.InternalEyewear/EyeID
struct EyeID_t621 
{
	// System.Int32 Vuforia.InternalEyewear/EyeID::value__
	int32_t ___value___1;
};
