﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct Stack_1_t3207;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct IEnumerator_1_t3212;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
struct List_1_t291;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m16420(__this, method) (void)Stack_1__ctor_m16404_gshared((Stack_1_t3209 *)__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m16421(__this, method) (bool)Stack_1_System_Collections_ICollection_get_IsSynchronized_m16405_gshared((Stack_1_t3209 *)__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m16422(__this, method) (Object_t *)Stack_1_System_Collections_ICollection_get_SyncRoot_m16406_gshared((Stack_1_t3209 *)__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m16423(__this, ___dest, ___idx, method) (void)Stack_1_System_Collections_ICollection_CopyTo_m16407_gshared((Stack_1_t3209 *)__this, (Array_t *)___dest, (int32_t)___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16424(__this, method) (Object_t*)Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16408_gshared((Stack_1_t3209 *)__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m16425(__this, method) (Object_t *)Stack_1_System_Collections_IEnumerable_GetEnumerator_m16409_gshared((Stack_1_t3209 *)__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Peek()
#define Stack_1_Peek_m16426(__this, method) (List_1_t291 *)Stack_1_Peek_m16410_gshared((Stack_1_t3209 *)__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Pop()
#define Stack_1_Pop_m16427(__this, method) (List_1_t291 *)Stack_1_Pop_m16411_gshared((Stack_1_t3209 *)__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Push(T)
#define Stack_1_Push_m16428(__this, ___t, method) (void)Stack_1_Push_m16412_gshared((Stack_1_t3209 *)__this, (Object_t *)___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_Count()
#define Stack_1_get_Count_m16429(__this, method) (int32_t)Stack_1_get_Count_m16413_gshared((Stack_1_t3209 *)__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::GetEnumerator()
 Enumerator_t3213  Stack_1_GetEnumerator_m16430 (Stack_1_t3207 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
