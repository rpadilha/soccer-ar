﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ImageImpl
struct ImageImpl_t655;
// System.Byte[]
struct ByteU5BU5D_t653;
// UnityEngine.Texture2D
struct Texture2D_t196;
// UnityEngine.Color32[]
struct Color32U5BU5D_t654;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"

// System.Int32 Vuforia.ImageImpl::get_Width()
 int32_t ImageImpl_get_Width_m3049 (ImageImpl_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageImpl::set_Width(System.Int32)
 void ImageImpl_set_Width_m3050 (ImageImpl_t655 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.ImageImpl::get_Height()
 int32_t ImageImpl_get_Height_m3051 (ImageImpl_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageImpl::set_Height(System.Int32)
 void ImageImpl_set_Height_m3052 (ImageImpl_t655 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.ImageImpl::get_Stride()
 int32_t ImageImpl_get_Stride_m3053 (ImageImpl_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageImpl::set_Stride(System.Int32)
 void ImageImpl_set_Stride_m3054 (ImageImpl_t655 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.ImageImpl::get_BufferWidth()
 int32_t ImageImpl_get_BufferWidth_m3055 (ImageImpl_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageImpl::set_BufferWidth(System.Int32)
 void ImageImpl_set_BufferWidth_m3056 (ImageImpl_t655 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.ImageImpl::get_BufferHeight()
 int32_t ImageImpl_get_BufferHeight_m3057 (ImageImpl_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageImpl::set_BufferHeight(System.Int32)
 void ImageImpl_set_BufferHeight_m3058 (ImageImpl_t655 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Image/PIXEL_FORMAT Vuforia.ImageImpl::get_PixelFormat()
 int32_t ImageImpl_get_PixelFormat_m3059 (ImageImpl_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageImpl::set_PixelFormat(Vuforia.Image/PIXEL_FORMAT)
 void ImageImpl_set_PixelFormat_m3060 (ImageImpl_t655 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Vuforia.ImageImpl::get_Pixels()
 ByteU5BU5D_t653* ImageImpl_get_Pixels_m3061 (ImageImpl_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageImpl::set_Pixels(System.Byte[])
 void ImageImpl_set_Pixels_m3062 (ImageImpl_t655 * __this, ByteU5BU5D_t653* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.ImageImpl::get_UnmanagedData()
 IntPtr_t121 ImageImpl_get_UnmanagedData_m3063 (ImageImpl_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageImpl::set_UnmanagedData(System.IntPtr)
 void ImageImpl_set_UnmanagedData_m3064 (ImageImpl_t655 * __this, IntPtr_t121 ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageImpl::.ctor()
 void ImageImpl__ctor_m3065 (ImageImpl_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageImpl::Finalize()
 void ImageImpl_Finalize_m3066 (ImageImpl_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageImpl::IsValid()
 bool ImageImpl_IsValid_m3067 (ImageImpl_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageImpl::CopyToTexture(UnityEngine.Texture2D)
 void ImageImpl_CopyToTexture_m3068 (ImageImpl_t655 * __this, Texture2D_t196 * ___texture2D, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageImpl::CopyPixelsFromUnmanagedBuffer()
 void ImageImpl_CopyPixelsFromUnmanagedBuffer_m3069 (ImageImpl_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] Vuforia.ImageImpl::GetPixels32()
 Color32U5BU5D_t654* ImageImpl_GetPixels32_m3070 (ImageImpl_t655 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextureFormat Vuforia.ImageImpl::ConvertPixelFormat(Vuforia.Image/PIXEL_FORMAT)
 int32_t ImageImpl_ConvertPixelFormat_m3071 (ImageImpl_t655 * __this, int32_t ___input, MethodInfo* method) IL2CPP_METHOD_ATTR;
