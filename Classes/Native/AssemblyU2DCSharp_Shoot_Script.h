﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t29;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Shoot_Script
struct Shoot_Script_t109  : public MonoBehaviour_t10
{
	// UnityEngine.GameObject Shoot_Script::sphere
	GameObject_t29 * ___sphere_2;
};
