﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MulticastDelegate
struct MulticastDelegate_t373;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1118;
// System.Object
struct Object_t;
// System.Delegate[]
struct DelegateU5BU5D_t1744;
// System.Delegate
struct Delegate_t152;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.MulticastDelegate::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void MulticastDelegate_GetObjectData_m2419 (MulticastDelegate_t373 * __this, SerializationInfo_t1118 * ___info, StreamingContext_t1119  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::Equals(System.Object)
 bool MulticastDelegate_Equals_m2417 (MulticastDelegate_t373 * __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MulticastDelegate::GetHashCode()
 int32_t MulticastDelegate_GetHashCode_m2418 (MulticastDelegate_t373 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate[] System.MulticastDelegate::GetInvocationList()
 DelegateU5BU5D_t1744* MulticastDelegate_GetInvocationList_m2421 (MulticastDelegate_t373 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::CombineImpl(System.Delegate)
 Delegate_t152 * MulticastDelegate_CombineImpl_m2422 (MulticastDelegate_t373 * __this, Delegate_t152 * ___follow, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::BaseEquals(System.MulticastDelegate)
 bool MulticastDelegate_BaseEquals_m9690 (MulticastDelegate_t373 * __this, MulticastDelegate_t373 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.MulticastDelegate System.MulticastDelegate::KPM(System.MulticastDelegate,System.MulticastDelegate,System.MulticastDelegate&)
 MulticastDelegate_t373 * MulticastDelegate_KPM_m9691 (Object_t * __this/* static, unused */, MulticastDelegate_t373 * ___needle, MulticastDelegate_t373 * ___haystack, MulticastDelegate_t373 ** ___tail, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::RemoveImpl(System.Delegate)
 Delegate_t152 * MulticastDelegate_RemoveImpl_m2423 (MulticastDelegate_t373 * __this, Delegate_t152 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
