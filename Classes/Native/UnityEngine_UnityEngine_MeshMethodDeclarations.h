﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Mesh
struct Mesh_t174;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t85;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t225;
// System.Int32[]
struct Int32U5BU5D_t175;
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"

// System.Void UnityEngine.Mesh::.ctor()
 void Mesh__ctor_m5337 (Mesh_t174 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
 void Mesh_Internal_Create_m5665 (Object_t * __this/* static, unused */, Mesh_t174 * ___mono, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::Clear(System.Boolean)
 void Mesh_Clear_m5666 (Mesh_t174 * __this, bool ___keepVertexLayout, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::Clear()
 void Mesh_Clear_m4450 (Mesh_t174 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
 Vector3U5BU5D_t85* Mesh_get_vertices_m526 (Mesh_t174 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
 void Mesh_set_vertices_m631 (Mesh_t174 * __this, Vector3U5BU5D_t85* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
 Vector3U5BU5D_t85* Mesh_get_normals_m629 (Mesh_t174 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])
 void Mesh_set_normals_m4454 (Mesh_t174 * __this, Vector3U5BU5D_t85* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
 Vector2U5BU5D_t225* Mesh_get_uv_m4453 (Mesh_t174 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
 void Mesh_set_uv_m4452 (Mesh_t174 * __this, Vector2U5BU5D_t225* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::INTERNAL_get_bounds(UnityEngine.Bounds&)
 void Mesh_INTERNAL_get_bounds_m5667 (Mesh_t174 * __this, Bounds_t198 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UnityEngine.Mesh::get_bounds()
 Bounds_t198  Mesh_get_bounds_m5567 (Mesh_t174 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::RecalculateNormals()
 void Mesh_RecalculateNormals_m4455 (Mesh_t174 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine.Mesh::get_triangles()
 Int32U5BU5D_t175* Mesh_get_triangles_m527 (Mesh_t174 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
 void Mesh_set_triangles_m4451 (Mesh_t174 * __this, Int32U5BU5D_t175* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mesh::get_vertexCount()
 int32_t Mesh_get_vertexCount_m627 (Mesh_t174 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
