﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.PhysicsRaycaster>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_76.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.PhysicsRaycaster>
struct CachedInvokableCall_1_t3385  : public InvokableCall_1_t3386
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.PhysicsRaycaster>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
