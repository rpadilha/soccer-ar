﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.DataSet>
struct IList_1_t4037;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>
struct ReadOnlyCollection_1_t4033  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::list
	Object_t* ___list_0;
};
