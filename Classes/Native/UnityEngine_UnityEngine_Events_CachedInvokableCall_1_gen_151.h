﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Shader>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_153.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Shader>
struct CachedInvokableCall_1_t4762  : public InvokableCall_1_t4763
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Shader>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
