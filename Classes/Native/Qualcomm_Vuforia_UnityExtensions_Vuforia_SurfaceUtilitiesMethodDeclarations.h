﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SurfaceUtilities
struct SurfaceUtilities_t136;
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"

// System.Void Vuforia.SurfaceUtilities::OnSurfaceCreated()
 void SurfaceUtilities_OnSurfaceCreated_m318 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::OnSurfaceDeinit()
 void SurfaceUtilities_OnSurfaceDeinit_m4335 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SurfaceUtilities::HasSurfaceBeenRecreated()
 bool SurfaceUtilities_HasSurfaceBeenRecreated_m313 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::OnSurfaceChanged(System.Int32,System.Int32)
 void SurfaceUtilities_OnSurfaceChanged_m4336 (Object_t * __this/* static, unused */, int32_t ___screenWidth, int32_t ___screenHeight, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::SetSurfaceOrientation(UnityEngine.ScreenOrientation)
 void SurfaceUtilities_SetSurfaceOrientation_m319 (Object_t * __this/* static, unused */, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation Vuforia.SurfaceUtilities::GetSurfaceOrientation()
 int32_t SurfaceUtilities_GetSurfaceOrientation_m4337 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::.cctor()
 void SurfaceUtilities__cctor_m4338 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
