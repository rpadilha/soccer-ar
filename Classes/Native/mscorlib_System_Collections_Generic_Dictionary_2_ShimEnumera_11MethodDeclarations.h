﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Type,System.UInt16>
struct ShimEnumerator_t4129;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t726;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Type,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m23623 (ShimEnumerator_t4129 * __this, Dictionary_2_t726 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Type,System.UInt16>::MoveNext()
 bool ShimEnumerator_MoveNext_m23624 (ShimEnumerator_t4129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Type,System.UInt16>::get_Entry()
 DictionaryEntry_t1355  ShimEnumerator_get_Entry_m23625 (ShimEnumerator_t4129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Type,System.UInt16>::get_Key()
 Object_t * ShimEnumerator_get_Key_m23626 (ShimEnumerator_t4129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Type,System.UInt16>::get_Value()
 Object_t * ShimEnumerator_get_Value_m23627 (ShimEnumerator_t4129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Type,System.UInt16>::get_Current()
 Object_t * ShimEnumerator_get_Current_m23628 (ShimEnumerator_t4129 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
