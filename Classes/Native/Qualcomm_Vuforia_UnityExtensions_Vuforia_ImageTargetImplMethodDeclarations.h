﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ImageTargetImpl
struct ImageTargetImpl_t658;
// System.String
struct String_t;
// Vuforia.DataSet
struct DataSet_t612;
// Vuforia.VirtualButton
struct VirtualButton_t639;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButton>
struct IEnumerable_1_t640;
// Vuforia.ImageTargetType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetType.h"
// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"

// System.Void Vuforia.ImageTargetImpl::.ctor(System.String,System.Int32,Vuforia.ImageTargetType,Vuforia.DataSet)
 void ImageTargetImpl__ctor_m3078 (ImageTargetImpl_t658 * __this, String_t* ___name, int32_t ___id, int32_t ___imageTargetType, DataSet_t612 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetType Vuforia.ImageTargetImpl::get_ImageTargetType()
 int32_t ImageTargetImpl_get_ImageTargetType_m3079 (ImageTargetImpl_t658 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButton Vuforia.ImageTargetImpl::CreateVirtualButton(System.String,Vuforia.RectangleData)
 VirtualButton_t639 * ImageTargetImpl_CreateVirtualButton_m3080 (ImageTargetImpl_t658 * __this, String_t* ___name, RectangleData_t632  ___area, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButton Vuforia.ImageTargetImpl::GetVirtualButtonByName(System.String)
 VirtualButton_t639 * ImageTargetImpl_GetVirtualButtonByName_m3081 (ImageTargetImpl_t658 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButton> Vuforia.ImageTargetImpl::GetVirtualButtons()
 Object_t* ImageTargetImpl_GetVirtualButtons_m3082 (ImageTargetImpl_t658 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetImpl::DestroyVirtualButton(Vuforia.VirtualButton)
 bool ImageTargetImpl_DestroyVirtualButton_m3083 (ImageTargetImpl_t658 * __this, VirtualButton_t639 * ___vb, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButton Vuforia.ImageTargetImpl::CreateNewVirtualButtonInNative(System.String,Vuforia.RectangleData)
 VirtualButton_t639 * ImageTargetImpl_CreateNewVirtualButtonInNative_m3084 (ImageTargetImpl_t658 * __this, String_t* ___name, RectangleData_t632  ___rectangleData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetImpl::UnregisterVirtualButtonInNative(Vuforia.VirtualButton)
 bool ImageTargetImpl_UnregisterVirtualButtonInNative_m3085 (ImageTargetImpl_t658 * __this, VirtualButton_t639 * ___vb, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetImpl::CreateVirtualButtonsFromNative()
 void ImageTargetImpl_CreateVirtualButtonsFromNative_m3086 (ImageTargetImpl_t658 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
