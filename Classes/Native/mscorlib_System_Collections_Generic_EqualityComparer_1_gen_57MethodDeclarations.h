﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<Vuforia.IVideoBackgroundEventHandler>
struct EqualityComparer_1_t4511;
// System.Object
struct Object_t;
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t112;

// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.IVideoBackgroundEventHandler>::.ctor()
// System.Collections.Generic.EqualityComparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_genMethodDeclarations.h"
#define EqualityComparer_1__ctor_m27221(__this, method) (void)EqualityComparer_1__ctor_m14630_gshared((EqualityComparer_1_t2840 *)__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.IVideoBackgroundEventHandler>::.cctor()
#define EqualityComparer_1__cctor_m27222(__this/* static, unused */, method) (void)EqualityComparer_1__cctor_m14631_gshared((Object_t *)__this/* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m27223(__this, ___obj, method) (int32_t)EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14632_gshared((EqualityComparer_1_t2840 *)__this, (Object_t *)___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m27224(__this, ___x, ___y, method) (bool)EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14633_gshared((EqualityComparer_1_t2840 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.IVideoBackgroundEventHandler>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.IVideoBackgroundEventHandler>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.IVideoBackgroundEventHandler>::get_Default()
#define EqualityComparer_1_get_Default_m27225(__this/* static, unused */, method) (EqualityComparer_1_t4511 *)EqualityComparer_1_get_Default_m14634_gshared((Object_t *)__this/* static, unused */, method)
