﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.CapsuleCollider>
struct UnityAction_1_t4821;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.CapsuleCollider>
struct InvokableCall_1_t4820  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.CapsuleCollider>::Delegate
	UnityAction_1_t4821 * ___Delegate_0;
};
