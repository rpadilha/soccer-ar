﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_120.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetAbstractBehaviour>
struct CachedInvokableCall_1_t3905  : public InvokableCall_1_t3906
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
