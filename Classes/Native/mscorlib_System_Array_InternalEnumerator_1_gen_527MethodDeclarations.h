﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>
struct InternalEnumerator_1_t5049;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Net.Security.AuthenticationLevel
#include "System_System_Net_Security_AuthenticationLevel.h"

// System.Void System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30572 (InternalEnumerator_1_t5049 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573 (InternalEnumerator_1_t5049 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::Dispose()
 void InternalEnumerator_1_Dispose_m30574 (InternalEnumerator_1_t5049 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30575 (InternalEnumerator_1_t5049 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30576 (InternalEnumerator_1_t5049 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
