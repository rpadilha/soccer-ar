﻿#pragma once
#include <stdint.h>
// Vuforia.Prop
struct Prop_t15;
// UnityEngine.BoxCollider
struct BoxCollider_t197;
// Vuforia.SmartTerrainTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab.h"
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t43  : public SmartTerrainTrackableBehaviour_t627
{
	// Vuforia.Prop Vuforia.PropAbstractBehaviour::mProp
	Object_t * ___mProp_13;
	// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::mBoxColliderToUpdate
	BoxCollider_t197 * ___mBoxColliderToUpdate_14;
};
