﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>
struct InternalEnumerator_1_t5171;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Mono.Math.Prime.ConfidenceFactor
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31239 (InternalEnumerator_1_t5171 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31240 (InternalEnumerator_1_t5171 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::Dispose()
 void InternalEnumerator_1_Dispose_m31241 (InternalEnumerator_1_t5171 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31242 (InternalEnumerator_1_t5171 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31243 (InternalEnumerator_1_t5171 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
