﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_8.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>
struct CachedInvokableCall_1_t2813  : public InvokableCall_1_t2814
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
