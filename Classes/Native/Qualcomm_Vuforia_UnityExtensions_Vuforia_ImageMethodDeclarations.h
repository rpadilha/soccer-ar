﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.Image
struct Image_t604;
// System.Byte[]
struct ByteU5BU5D_t653;
// UnityEngine.Texture2D
struct Texture2D_t196;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Int32 Vuforia.Image::get_Width()
// System.Void Vuforia.Image::set_Width(System.Int32)
// System.Int32 Vuforia.Image::get_Height()
// System.Void Vuforia.Image::set_Height(System.Int32)
// System.Int32 Vuforia.Image::get_Stride()
// System.Void Vuforia.Image::set_Stride(System.Int32)
// System.Int32 Vuforia.Image::get_BufferWidth()
// System.Void Vuforia.Image::set_BufferWidth(System.Int32)
// System.Int32 Vuforia.Image::get_BufferHeight()
// System.Void Vuforia.Image::set_BufferHeight(System.Int32)
// Vuforia.Image/PIXEL_FORMAT Vuforia.Image::get_PixelFormat()
// System.Void Vuforia.Image::set_PixelFormat(Vuforia.Image/PIXEL_FORMAT)
// System.Byte[] Vuforia.Image::get_Pixels()
// System.Void Vuforia.Image::set_Pixels(System.Byte[])
// System.Boolean Vuforia.Image::IsValid()
// System.Void Vuforia.Image::CopyToTexture(UnityEngine.Texture2D)
// System.Void Vuforia.Image::.ctor()
 void Image__ctor_m3048 (Image_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
