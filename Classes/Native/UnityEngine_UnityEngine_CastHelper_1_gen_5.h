﻿#pragma once
#include <stdint.h>
// GoalKeeper_Script
struct GoalKeeper_Script_t77;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<GoalKeeper_Script>
struct CastHelper_1_t3000 
{
	// T UnityEngine.CastHelper`1<GoalKeeper_Script>::t
	GoalKeeper_Script_t77 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<GoalKeeper_Script>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
