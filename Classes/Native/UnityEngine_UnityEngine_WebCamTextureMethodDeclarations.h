﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WebCamTexture
struct WebCamTexture_t728;
// System.String
struct String_t;
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t930;

// System.Void UnityEngine.WebCamTexture::.ctor()
 void WebCamTexture__ctor_m5097 (WebCamTexture_t728 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)
 void WebCamTexture_Internal_CreateWebCamTexture_m6284 (Object_t * __this/* static, unused */, WebCamTexture_t728 * ___self, String_t* ___scriptingDevice, int32_t ___requestedWidth, int32_t ___requestedHeight, int32_t ___maxFramerate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::Play()
 void WebCamTexture_Play_m5102 (WebCamTexture_t728 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)
 void WebCamTexture_INTERNAL_CALL_Play_m6285 (Object_t * __this/* static, unused */, WebCamTexture_t728 * ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::Stop()
 void WebCamTexture_Stop_m5103 (WebCamTexture_t728 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Stop(UnityEngine.WebCamTexture)
 void WebCamTexture_INTERNAL_CALL_Stop_m6286 (Object_t * __this/* static, unused */, WebCamTexture_t728 * ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WebCamTexture::get_isPlaying()
 bool WebCamTexture_get_isPlaying_m5096 (WebCamTexture_t728 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::set_deviceName(System.String)
 void WebCamTexture_set_deviceName_m5098 (WebCamTexture_t728 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::set_requestedFPS(System.Single)
 void WebCamTexture_set_requestedFPS_m5099 (WebCamTexture_t728 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::set_requestedWidth(System.Int32)
 void WebCamTexture_set_requestedWidth_m5100 (WebCamTexture_t728 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::set_requestedHeight(System.Int32)
 void WebCamTexture_set_requestedHeight_m5101 (WebCamTexture_t728 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WebCamDevice[] UnityEngine.WebCamTexture::get_devices()
 WebCamDeviceU5BU5D_t930* WebCamTexture_get_devices_m5456 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WebCamTexture::get_didUpdateThisFrame()
 bool WebCamTexture_get_didUpdateThisFrame_m5095 (WebCamTexture_t728 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
