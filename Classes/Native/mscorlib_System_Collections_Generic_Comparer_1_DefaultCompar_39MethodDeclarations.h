﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.ISmartTerrainEventHandler>
struct DefaultComparer_t4252;
// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t763;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.ISmartTerrainEventHandler>::.ctor()
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_0MethodDeclarations.h"
#define DefaultComparer__ctor_m24868(__this, method) (void)DefaultComparer__ctor_m14676_gshared((DefaultComparer_t2849 *)__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.ISmartTerrainEventHandler>::Compare(T,T)
#define DefaultComparer_Compare_m24869(__this, ___x, ___y, method) (int32_t)DefaultComparer_Compare_m14677_gshared((DefaultComparer_t2849 *)__this, (Object_t *)___x, (Object_t *)___y, method)
