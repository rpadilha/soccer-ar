﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory
struct NullBehaviourComponentFactory_t636;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t28;
// UnityEngine.GameObject
struct GameObject_t29;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t30;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t31;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t23;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t32;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t33;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t6;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t34;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t35;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t36;

// Vuforia.MaskOutAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
 MaskOutAbstractBehaviour_t28 * NullBehaviourComponentFactory_AddMaskOutBehaviour_m2971 (NullBehaviourComponentFactory_t636 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
 VirtualButtonAbstractBehaviour_t30 * NullBehaviourComponentFactory_AddVirtualButtonBehaviour_m2972 (NullBehaviourComponentFactory_t636 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TurnOffAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
 TurnOffAbstractBehaviour_t31 * NullBehaviourComponentFactory_AddTurnOffBehaviour_m2973 (NullBehaviourComponentFactory_t636 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
 ImageTargetAbstractBehaviour_t23 * NullBehaviourComponentFactory_AddImageTargetBehaviour_m2974 (NullBehaviourComponentFactory_t636 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
 MarkerAbstractBehaviour_t32 * NullBehaviourComponentFactory_AddMarkerBehaviour_m2975 (NullBehaviourComponentFactory_t636 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
 MultiTargetAbstractBehaviour_t33 * NullBehaviourComponentFactory_AddMultiTargetBehaviour_m2976 (NullBehaviourComponentFactory_t636 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
 CylinderTargetAbstractBehaviour_t6 * NullBehaviourComponentFactory_AddCylinderTargetBehaviour_m2977 (NullBehaviourComponentFactory_t636 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
 WordAbstractBehaviour_t34 * NullBehaviourComponentFactory_AddWordBehaviour_m2978 (NullBehaviourComponentFactory_t636 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextRecoAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
 TextRecoAbstractBehaviour_t35 * NullBehaviourComponentFactory_AddTextRecoBehaviour_m2979 (NullBehaviourComponentFactory_t636 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
 ObjectTargetAbstractBehaviour_t36 * NullBehaviourComponentFactory_AddObjectTargetBehaviour_m2980 (NullBehaviourComponentFactory_t636 * __this, GameObject_t29 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::.ctor()
 void NullBehaviourComponentFactory__ctor_m2981 (NullBehaviourComponentFactory_t636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
