﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
struct List_1_t1140;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1127;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Events.BaseInvokableCall>
struct IEnumerable_1_t5011;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Events.BaseInvokableCall>
struct IEnumerator_1_t5012;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.ICollection`1<UnityEngine.Events.BaseInvokableCall>
struct ICollection_1_t5013;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Events.BaseInvokableCall>
struct ReadOnlyCollection_1_t5014;
// UnityEngine.Events.BaseInvokableCall[]
struct BaseInvokableCallU5BU5D_t5010;
// System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
struct Predicate_1_t1247;
// System.Comparison`1<UnityEngine.Events.BaseInvokableCall>
struct Comparison_1_t5015;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_55.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
#define List_1__ctor_m6746(__this, method) (void)List_1__ctor_m14461_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m30301(__this, ___collection, method) (void)List_1__ctor_m14463_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Int32)
#define List_1__ctor_m30302(__this, ___capacity, method) (void)List_1__ctor_m14465_gshared((List_1_t149 *)__this, (int32_t)___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.cctor()
#define List_1__cctor_m30303(__this/* static, unused */, method) (void)List_1__cctor_m14467_gshared((Object_t *)__this/* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m30304(__this, method) (Object_t*)List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14469_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m30305(__this, ___array, ___arrayIndex, method) (void)List_1_System_Collections_ICollection_CopyTo_m14471_gshared((List_1_t149 *)__this, (Array_t *)___array, (int32_t)___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m30306(__this, method) (Object_t *)List_1_System_Collections_IEnumerable_GetEnumerator_m14473_gshared((List_1_t149 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m30307(__this, ___item, method) (int32_t)List_1_System_Collections_IList_Add_m14475_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m30308(__this, ___item, method) (bool)List_1_System_Collections_IList_Contains_m14477_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m30309(__this, ___item, method) (int32_t)List_1_System_Collections_IList_IndexOf_m14479_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m30310(__this, ___index, ___item, method) (void)List_1_System_Collections_IList_Insert_m14481_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m30311(__this, ___item, method) (void)List_1_System_Collections_IList_Remove_m14483_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m30312(__this, method) (bool)List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14485_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m30313(__this, method) (bool)List_1_System_Collections_ICollection_get_IsSynchronized_m14487_gshared((List_1_t149 *)__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m30314(__this, method) (Object_t *)List_1_System_Collections_ICollection_get_SyncRoot_m14489_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m30315(__this, method) (bool)List_1_System_Collections_IList_get_IsFixedSize_m14491_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m30316(__this, method) (bool)List_1_System_Collections_IList_get_IsReadOnly_m14493_gshared((List_1_t149 *)__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m30317(__this, ___index, method) (Object_t *)List_1_System_Collections_IList_get_Item_m14495_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m30318(__this, ___index, ___value, method) (void)List_1_System_Collections_IList_set_Item_m14497_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(T)
#define List_1_Add_m6747(__this, ___item, method) (void)List_1_Add_m14499_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m30319(__this, ___newCount, method) (void)List_1_GrowIfNeeded_m14501_gshared((List_1_t149 *)__this, (int32_t)___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m30320(__this, ___collection, method) (void)List_1_AddCollection_m14503_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m30321(__this, ___enumerable, method) (void)List_1_AddEnumerable_m14505_gshared((List_1_t149 *)__this, (Object_t*)___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m6754(__this, ___collection, method) (void)List_1_AddRange_m14506_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AsReadOnly()
#define List_1_AsReadOnly_m30322(__this, method) (ReadOnlyCollection_1_t5014 *)List_1_AsReadOnly_m14508_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear()
#define List_1_Clear_m6753(__this, method) (void)List_1_Clear_m14510_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Contains(T)
#define List_1_Contains_m6750(__this, ___item, method) (bool)List_1_Contains_m14512_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m30323(__this, ___array, ___arrayIndex, method) (void)List_1_CopyTo_m14514_gshared((List_1_t149 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Find(System.Predicate`1<T>)
#define List_1_Find_m30324(__this, ___match, method) (BaseInvokableCall_t1127 *)List_1_Find_m14516_gshared((List_1_t149 *)__this, (Predicate_1_t2832 *)___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m30325(__this/* static, unused */, ___match, method) (void)List_1_CheckMatch_m14518_gshared((Object_t *)__this/* static, unused */, (Predicate_1_t2832 *)___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m30326(__this, ___startIndex, ___count, ___match, method) (int32_t)List_1_GetIndex_m14520_gshared((List_1_t149 *)__this, (int32_t)___startIndex, (int32_t)___count, (Predicate_1_t2832 *)___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::GetEnumerator()
 Enumerator_t5016  List_1_GetEnumerator_m30327 (List_1_t1140 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::IndexOf(T)
#define List_1_IndexOf_m30328(__this, ___item, method) (int32_t)List_1_IndexOf_m14522_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m30329(__this, ___start, ___delta, method) (void)List_1_Shift_m14524_gshared((List_1_t149 *)__this, (int32_t)___start, (int32_t)___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m30330(__this, ___index, method) (void)List_1_CheckIndex_m14526_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Insert(System.Int32,T)
#define List_1_Insert_m30331(__this, ___index, ___item, method) (void)List_1_Insert_m14528_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m30332(__this, ___collection, method) (void)List_1_CheckCollection_m14530_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Remove(T)
#define List_1_Remove_m30333(__this, ___item, method) (bool)List_1_Remove_m14532_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m6752(__this, ___match, method) (int32_t)List_1_RemoveAll_m14534_gshared((List_1_t149 *)__this, (Predicate_1_t2832 *)___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m30334(__this, ___index, method) (void)List_1_RemoveAt_m14536_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Reverse()
#define List_1_Reverse_m30335(__this, method) (void)List_1_Reverse_m14538_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Sort()
#define List_1_Sort_m30336(__this, method) (void)List_1_Sort_m14540_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m30337(__this, ___comparison, method) (void)List_1_Sort_m14542_gshared((List_1_t149 *)__this, (Comparison_1_t2833 *)___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::ToArray()
#define List_1_ToArray_m30338(__this, method) (BaseInvokableCallU5BU5D_t5010*)List_1_ToArray_m14544_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::TrimExcess()
#define List_1_TrimExcess_m30339(__this, method) (void)List_1_TrimExcess_m14546_gshared((List_1_t149 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Capacity()
#define List_1_get_Capacity_m30340(__this, method) (int32_t)List_1_get_Capacity_m14548_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m30341(__this, ___value, method) (void)List_1_set_Capacity_m14550_gshared((List_1_t149 *)__this, (int32_t)___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Count()
#define List_1_get_Count_m6749(__this, method) (int32_t)List_1_get_Count_m14552_gshared((List_1_t149 *)__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32)
#define List_1_get_Item_m6748(__this, ___index, method) (BaseInvokableCall_t1127 *)List_1_get_Item_m14554_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::set_Item(System.Int32,T)
#define List_1_set_Item_m30342(__this, ___index, ___value, method) (void)List_1_set_Item_m14556_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___value, method)
