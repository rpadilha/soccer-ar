﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__10.h"
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Trackable>
struct ShimEnumerator_t3987  : public Object_t
{
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Trackable>::host_enumerator
	Enumerator_t3979  ___host_enumerator_0;
};
