﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Animator
struct Animator_t404;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t543;
// System.String
struct String_t;

// System.Void UnityEngine.Animator::SetTrigger(System.String)
 void Animator_SetTrigger_m2598 (Animator_t404 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
 void Animator_ResetTrigger_m2597 (Animator_t404 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
 RuntimeAnimatorController_t543 * Animator_get_runtimeAnimatorController_m2596 (Animator_t404 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
 int32_t Animator_StringToHash_m6342 (Object_t * __this/* static, unused */, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
 void Animator_SetTriggerString_m6343 (Animator_t404 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
 void Animator_ResetTriggerString_m6344 (Animator_t404 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
