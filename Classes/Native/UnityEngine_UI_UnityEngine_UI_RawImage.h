﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture
struct Texture_t107;
// UnityEngine.UI.MaskableGraphic
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.UI.RawImage
struct RawImage_t387  : public MaskableGraphic_t361
{
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t107 * ___m_Texture_23;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t103  ___m_UVRect_24;
};
