﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DSASignatureDescription
struct DSASignatureDescription_t2176;

// System.Void System.Security.Cryptography.DSASignatureDescription::.ctor()
 void DSASignatureDescription__ctor_m12296 (DSASignatureDescription_t2176 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
