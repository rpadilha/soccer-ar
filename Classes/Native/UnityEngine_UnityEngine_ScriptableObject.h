﻿#pragma once
#include <stdint.h>
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.ScriptableObject
struct ScriptableObject_t962  : public Object_t120
{
};
// Native definition for marshalling of: UnityEngine.ScriptableObject
struct ScriptableObject_t962_marshaled
{
};
