﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// UnityEngine.Events.UnityAction`1<Player_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_56.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo UnityAction_1_t3093_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Player_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_56MethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Player_Script
#include "AssemblyU2DCSharp_Player_Script.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Void UnityEngine.Events.UnityAction`1<Player_Script>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Player_Script>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Player_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Player_Script>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Player_Script>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3093_UnityAction_1__ctor_m15774_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15774_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Player_Script>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15774_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3093_UnityAction_1__ctor_m15774_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15774_GenericMethod/* genericMethod */

};
extern Il2CppType Player_Script_t94_0_0_0;
extern Il2CppType Player_Script_t94_0_0_0;
static ParameterInfo UnityAction_1_t3093_UnityAction_1_Invoke_m15775_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Player_Script_t94_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15775_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Player_Script>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15775_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3093_UnityAction_1_Invoke_m15775_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15775_GenericMethod/* genericMethod */

};
extern Il2CppType Player_Script_t94_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3093_UnityAction_1_BeginInvoke_m15776_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Player_Script_t94_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15776_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Player_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15776_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3093_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3093_UnityAction_1_BeginInvoke_m15776_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15776_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3093_UnityAction_1_EndInvoke_m15777_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15777_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Player_Script>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15777_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3093_UnityAction_1_EndInvoke_m15777_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15777_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3093_MethodInfos[] =
{
	&UnityAction_1__ctor_m15774_MethodInfo,
	&UnityAction_1_Invoke_m15775_MethodInfo,
	&UnityAction_1_BeginInvoke_m15776_MethodInfo,
	&UnityAction_1_EndInvoke_m15777_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2417_MethodInfo;
extern MethodInfo Object_Finalize_m198_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2418_MethodInfo;
extern MethodInfo Object_ToString_m322_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2419_MethodInfo;
extern MethodInfo Delegate_Clone_m2420_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2421_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2422_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2423_MethodInfo;
extern MethodInfo UnityAction_1_Invoke_m15775_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m15776_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15777_MethodInfo;
static MethodInfo* UnityAction_1_t3093_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15775_MethodInfo,
	&UnityAction_1_BeginInvoke_m15776_MethodInfo,
	&UnityAction_1_EndInvoke_m15777_MethodInfo,
};
extern TypeInfo ICloneable_t525_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t526_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t3093_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3093_0_0_0;
extern Il2CppType UnityAction_1_t3093_1_0_0;
extern TypeInfo MulticastDelegate_t373_il2cpp_TypeInfo;
struct UnityAction_1_t3093;
extern Il2CppGenericClass UnityAction_1_t3093_GenericClass;
TypeInfo UnityAction_1_t3093_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3093_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3093_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3093_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3093_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3093_0_0_0/* byval_arg */
	, &UnityAction_1_t3093_1_0_0/* this_arg */
	, UnityAction_1_t3093_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3093_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3093)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.CapsuleCollider>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_15.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t3094_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.CapsuleCollider>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_15MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.CapsuleCollider>
extern Il2CppType CapsuleCollider_t81_0_0_6;
FieldInfo CastHelper_1_t3094____t_0_FieldInfo = 
{
	"t"/* name */
	, &CapsuleCollider_t81_0_0_6/* type */
	, &CastHelper_1_t3094_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3094, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t3094____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t3094_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3094, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t3094_FieldInfos[] =
{
	&CastHelper_1_t3094____t_0_FieldInfo,
	&CastHelper_1_t3094____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t3094_MethodInfos[] =
{
	NULL
};
extern MethodInfo ValueType_Equals_m2145_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m2146_MethodInfo;
extern MethodInfo ValueType_ToString_m2236_MethodInfo;
static MethodInfo* CastHelper_1_t3094_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t3094_0_0_0;
extern Il2CppType CastHelper_1_t3094_1_0_0;
extern TypeInfo ValueType_t484_il2cpp_TypeInfo;
extern Il2CppGenericClass CastHelper_1_t3094_GenericClass;
TypeInfo CastHelper_1_t3094_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t3094_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t3094_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t3094_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t3094_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t3094_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t3094_0_0_0/* byval_arg */
	, &CastHelper_1_t3094_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t3094_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t3094)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.Transform>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_16.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t3095_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.Transform>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_16MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.Transform>
extern Il2CppType Transform_t74_0_0_6;
FieldInfo CastHelper_1_t3095____t_0_FieldInfo = 
{
	"t"/* name */
	, &Transform_t74_0_0_6/* type */
	, &CastHelper_1_t3095_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3095, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t3095____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t3095_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t3095, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t3095_FieldInfos[] =
{
	&CastHelper_1_t3095____t_0_FieldInfo,
	&CastHelper_1_t3095____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t3095_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t3095_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t3095_0_0_0;
extern Il2CppType CastHelper_1_t3095_1_0_0;
extern Il2CppGenericClass CastHelper_1_t3095_GenericClass;
TypeInfo CastHelper_1_t3095_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t3095_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t3095_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t3095_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t3095_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t3095_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t3095_0_0_0/* byval_arg */
	, &CastHelper_1_t3095_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t3095_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t3095)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6185_il2cpp_TypeInfo;

// Player_Script/TypePlayer
#include "AssemblyU2DCSharp_Player_Script_TypePlayer.h"


// T System.Collections.Generic.IEnumerator`1<Player_Script/TypePlayer>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Player_Script/TypePlayer>
extern MethodInfo IEnumerator_1_get_Current_m44262_MethodInfo;
static PropertyInfo IEnumerator_1_t6185____Current_PropertyInfo = 
{
	&IEnumerator_1_t6185_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44262_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6185_PropertyInfos[] =
{
	&IEnumerator_1_t6185____Current_PropertyInfo,
	NULL
};
extern Il2CppType TypePlayer_t105_0_0_0;
extern void* RuntimeInvoker_TypePlayer_t105 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44262_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Player_Script/TypePlayer>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44262_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6185_il2cpp_TypeInfo/* declaring_type */
	, &TypePlayer_t105_0_0_0/* return_type */
	, RuntimeInvoker_TypePlayer_t105/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44262_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6185_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44262_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t318_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t138_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t6185_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6185_0_0_0;
extern Il2CppType IEnumerator_1_t6185_1_0_0;
struct IEnumerator_1_t6185;
extern Il2CppGenericClass IEnumerator_1_t6185_GenericClass;
TypeInfo IEnumerator_1_t6185_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6185_MethodInfos/* methods */
	, IEnumerator_1_t6185_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6185_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6185_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6185_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6185_0_0_0/* byval_arg */
	, &IEnumerator_1_t6185_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6185_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Player_Script/TypePlayer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_135.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3096_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Player_Script/TypePlayer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_135MethodDeclarations.h"

// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo TypePlayer_t105_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1546_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m15782_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7828_MethodInfo;
extern MethodInfo Array_get_Length_m814_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTypePlayer_t105_m33829_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// Declaration !!0 System.Array::InternalArray__get_Item<Player_Script/TypePlayer>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Player_Script/TypePlayer>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisTypePlayer_t105_m33829 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m15778_MethodInfo;
 void InternalEnumerator_1__ctor_m15778 (InternalEnumerator_1_t3096 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15779_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15779 (InternalEnumerator_1_t3096 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m15782(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m15782_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&TypePlayer_t105_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m15780_MethodInfo;
 void InternalEnumerator_1_Dispose_m15780 (InternalEnumerator_1_t3096 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m15781_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m15781 (InternalEnumerator_1_t3096 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m15782 (InternalEnumerator_1_t3096 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisTypePlayer_t105_m33829(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisTypePlayer_t105_m33829_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Player_Script/TypePlayer>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3096____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3096_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3096, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3096____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3096_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3096, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3096_FieldInfos[] =
{
	&InternalEnumerator_1_t3096____array_0_FieldInfo,
	&InternalEnumerator_1_t3096____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3096____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3096_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15779_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3096____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3096_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15782_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3096_PropertyInfos[] =
{
	&InternalEnumerator_1_t3096____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3096____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3096_InternalEnumerator_1__ctor_m15778_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15778_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15778_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m15778/* method */
	, &InternalEnumerator_1_t3096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3096_InternalEnumerator_1__ctor_m15778_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15778_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15779_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15779_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15779/* method */
	, &InternalEnumerator_1_t3096_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15779_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15780_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15780_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m15780/* method */
	, &InternalEnumerator_1_t3096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15780_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15781_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15781_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m15781/* method */
	, &InternalEnumerator_1_t3096_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15781_GenericMethod/* genericMethod */

};
extern Il2CppType TypePlayer_t105_0_0_0;
extern void* RuntimeInvoker_TypePlayer_t105 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15782_GenericMethod;
// T System.Array/InternalEnumerator`1<Player_Script/TypePlayer>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15782_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m15782/* method */
	, &InternalEnumerator_1_t3096_il2cpp_TypeInfo/* declaring_type */
	, &TypePlayer_t105_0_0_0/* return_type */
	, RuntimeInvoker_TypePlayer_t105/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15782_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3096_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15778_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15779_MethodInfo,
	&InternalEnumerator_1_Dispose_m15780_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15781_MethodInfo,
	&InternalEnumerator_1_get_Current_m15782_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3096_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15779_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15781_MethodInfo,
	&InternalEnumerator_1_Dispose_m15780_MethodInfo,
	&InternalEnumerator_1_get_Current_m15782_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3096_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6185_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3096_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6185_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3096_0_0_0;
extern Il2CppType InternalEnumerator_1_t3096_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3096_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t3096_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3096_MethodInfos/* methods */
	, InternalEnumerator_1_t3096_PropertyInfos/* properties */
	, InternalEnumerator_1_t3096_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3096_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3096_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3096_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3096_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3096_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3096_1_0_0/* this_arg */
	, InternalEnumerator_1_t3096_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3096_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3096)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7904_il2cpp_TypeInfo;

#include "Assembly-CSharp_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>
extern MethodInfo ICollection_1_get_Count_m44263_MethodInfo;
static PropertyInfo ICollection_1_t7904____Count_PropertyInfo = 
{
	&ICollection_1_t7904_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44263_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44264_MethodInfo;
static PropertyInfo ICollection_1_t7904____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7904_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44264_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7904_PropertyInfos[] =
{
	&ICollection_1_t7904____Count_PropertyInfo,
	&ICollection_1_t7904____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44263_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>::get_Count()
MethodInfo ICollection_1_get_Count_m44263_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7904_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44263_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44264_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44264_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7904_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44264_GenericMethod/* genericMethod */

};
extern Il2CppType TypePlayer_t105_0_0_0;
extern Il2CppType TypePlayer_t105_0_0_0;
static ParameterInfo ICollection_1_t7904_ICollection_1_Add_m44265_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypePlayer_t105_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44265_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>::Add(T)
MethodInfo ICollection_1_Add_m44265_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t7904_ICollection_1_Add_m44265_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44265_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44266_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>::Clear()
MethodInfo ICollection_1_Clear_m44266_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44266_GenericMethod/* genericMethod */

};
extern Il2CppType TypePlayer_t105_0_0_0;
static ParameterInfo ICollection_1_t7904_ICollection_1_Contains_m44267_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypePlayer_t105_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44267_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>::Contains(T)
MethodInfo ICollection_1_Contains_m44267_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7904_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t7904_ICollection_1_Contains_m44267_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44267_GenericMethod/* genericMethod */

};
extern Il2CppType TypePlayerU5BU5D_t5408_0_0_0;
extern Il2CppType TypePlayerU5BU5D_t5408_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7904_ICollection_1_CopyTo_m44268_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TypePlayerU5BU5D_t5408_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44268_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44268_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7904_ICollection_1_CopyTo_m44268_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44268_GenericMethod/* genericMethod */

};
extern Il2CppType TypePlayer_t105_0_0_0;
static ParameterInfo ICollection_1_t7904_ICollection_1_Remove_m44269_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypePlayer_t105_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44269_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Player_Script/TypePlayer>::Remove(T)
MethodInfo ICollection_1_Remove_m44269_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7904_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t7904_ICollection_1_Remove_m44269_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44269_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7904_MethodInfos[] =
{
	&ICollection_1_get_Count_m44263_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44264_MethodInfo,
	&ICollection_1_Add_m44265_MethodInfo,
	&ICollection_1_Clear_m44266_MethodInfo,
	&ICollection_1_Contains_m44267_MethodInfo,
	&ICollection_1_CopyTo_m44268_MethodInfo,
	&ICollection_1_Remove_m44269_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7906_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7904_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7906_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7904_0_0_0;
extern Il2CppType ICollection_1_t7904_1_0_0;
struct ICollection_1_t7904;
extern Il2CppGenericClass ICollection_1_t7904_GenericClass;
TypeInfo ICollection_1_t7904_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7904_MethodInfos/* methods */
	, ICollection_1_t7904_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7904_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7904_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7904_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7904_0_0_0/* byval_arg */
	, &ICollection_1_t7904_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7904_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Player_Script/TypePlayer>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Player_Script/TypePlayer>
extern Il2CppType IEnumerator_1_t6185_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44270_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Player_Script/TypePlayer>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44270_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7906_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6185_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44270_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7906_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44270_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7906_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7906_0_0_0;
extern Il2CppType IEnumerable_1_t7906_1_0_0;
struct IEnumerable_1_t7906;
extern Il2CppGenericClass IEnumerable_1_t7906_GenericClass;
TypeInfo IEnumerable_1_t7906_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7906_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7906_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7906_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7906_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7906_0_0_0/* byval_arg */
	, &IEnumerable_1_t7906_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7906_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7905_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Player_Script/TypePlayer>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Player_Script/TypePlayer>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Player_Script/TypePlayer>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Player_Script/TypePlayer>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Player_Script/TypePlayer>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Player_Script/TypePlayer>
extern MethodInfo IList_1_get_Item_m44271_MethodInfo;
extern MethodInfo IList_1_set_Item_m44272_MethodInfo;
static PropertyInfo IList_1_t7905____Item_PropertyInfo = 
{
	&IList_1_t7905_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44271_MethodInfo/* get */
	, &IList_1_set_Item_m44272_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7905_PropertyInfos[] =
{
	&IList_1_t7905____Item_PropertyInfo,
	NULL
};
extern Il2CppType TypePlayer_t105_0_0_0;
static ParameterInfo IList_1_t7905_IList_1_IndexOf_m44273_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypePlayer_t105_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44273_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Player_Script/TypePlayer>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44273_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7905_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7905_IList_1_IndexOf_m44273_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44273_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypePlayer_t105_0_0_0;
static ParameterInfo IList_1_t7905_IList_1_Insert_m44274_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TypePlayer_t105_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44274_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Player_Script/TypePlayer>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44274_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7905_IList_1_Insert_m44274_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44274_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7905_IList_1_RemoveAt_m44275_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44275_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Player_Script/TypePlayer>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44275_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7905_IList_1_RemoveAt_m44275_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44275_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7905_IList_1_get_Item_m44271_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType TypePlayer_t105_0_0_0;
extern void* RuntimeInvoker_TypePlayer_t105_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44271_GenericMethod;
// T System.Collections.Generic.IList`1<Player_Script/TypePlayer>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44271_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7905_il2cpp_TypeInfo/* declaring_type */
	, &TypePlayer_t105_0_0_0/* return_type */
	, RuntimeInvoker_TypePlayer_t105_Int32_t123/* invoker_method */
	, IList_1_t7905_IList_1_get_Item_m44271_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44271_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType TypePlayer_t105_0_0_0;
static ParameterInfo IList_1_t7905_IList_1_set_Item_m44272_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TypePlayer_t105_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44272_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Player_Script/TypePlayer>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44272_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7905_IList_1_set_Item_m44272_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44272_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7905_MethodInfos[] =
{
	&IList_1_IndexOf_m44273_MethodInfo,
	&IList_1_Insert_m44274_MethodInfo,
	&IList_1_RemoveAt_m44275_MethodInfo,
	&IList_1_get_Item_m44271_MethodInfo,
	&IList_1_set_Item_m44272_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7905_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7904_il2cpp_TypeInfo,
	&IEnumerable_1_t7906_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7905_0_0_0;
extern Il2CppType IList_1_t7905_1_0_0;
struct IList_1_t7905;
extern Il2CppGenericClass IList_1_t7905_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7905_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7905_MethodInfos/* methods */
	, IList_1_t7905_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7905_il2cpp_TypeInfo/* element_class */
	, IList_1_t7905_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7905_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7905_0_0_0/* byval_arg */
	, &IList_1_t7905_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7905_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6187_il2cpp_TypeInfo;

// Player_Script/Player_State
#include "AssemblyU2DCSharp_Player_Script_Player_State.h"


// T System.Collections.Generic.IEnumerator`1<Player_Script/Player_State>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Player_Script/Player_State>
extern MethodInfo IEnumerator_1_get_Current_m44276_MethodInfo;
static PropertyInfo IEnumerator_1_t6187____Current_PropertyInfo = 
{
	&IEnumerator_1_t6187_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44276_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6187_PropertyInfos[] =
{
	&IEnumerator_1_t6187____Current_PropertyInfo,
	NULL
};
extern Il2CppType Player_State_t106_0_0_0;
extern void* RuntimeInvoker_Player_State_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44276_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Player_Script/Player_State>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44276_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6187_il2cpp_TypeInfo/* declaring_type */
	, &Player_State_t106_0_0_0/* return_type */
	, RuntimeInvoker_Player_State_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44276_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6187_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44276_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6187_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6187_0_0_0;
extern Il2CppType IEnumerator_1_t6187_1_0_0;
struct IEnumerator_1_t6187;
extern Il2CppGenericClass IEnumerator_1_t6187_GenericClass;
TypeInfo IEnumerator_1_t6187_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6187_MethodInfos/* methods */
	, IEnumerator_1_t6187_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6187_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6187_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6187_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6187_0_0_0/* byval_arg */
	, &IEnumerator_1_t6187_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6187_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Player_Script/Player_State>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_136.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3097_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Player_Script/Player_State>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_136MethodDeclarations.h"

extern TypeInfo Player_State_t106_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15787_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisPlayer_State_t106_m33840_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Player_Script/Player_State>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Player_Script/Player_State>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisPlayer_State_t106_m33840 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Player_Script/Player_State>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m15783_MethodInfo;
 void InternalEnumerator_1__ctor_m15783 (InternalEnumerator_1_t3097 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Player_Script/Player_State>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15784_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15784 (InternalEnumerator_1_t3097 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m15787(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m15787_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Player_State_t106_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Player_Script/Player_State>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m15785_MethodInfo;
 void InternalEnumerator_1_Dispose_m15785 (InternalEnumerator_1_t3097 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Player_Script/Player_State>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m15786_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m15786 (InternalEnumerator_1_t3097 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m814(L_1, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Player_Script/Player_State>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m15787 (InternalEnumerator_1_t3097 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1546 * L_1 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_1, (String_t*) &_stringLiteral1322, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1546 * L_3 = (InvalidOperationException_t1546 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1546_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7828(L_3, (String_t*) &_stringLiteral1323, /*hidden argument*/&InvalidOperationException__ctor_m7828_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m814(L_5, /*hidden argument*/&Array_get_Length_m814_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisPlayer_State_t106_m33840(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisPlayer_State_t106_m33840_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Player_Script/Player_State>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3097____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3097_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3097, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3097____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3097_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3097, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3097_FieldInfos[] =
{
	&InternalEnumerator_1_t3097____array_0_FieldInfo,
	&InternalEnumerator_1_t3097____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3097____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3097_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15784_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3097____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3097_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15787_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3097_PropertyInfos[] =
{
	&InternalEnumerator_1_t3097____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3097____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3097_InternalEnumerator_1__ctor_m15783_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15783_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Player_Script/Player_State>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15783_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m15783/* method */
	, &InternalEnumerator_1_t3097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3097_InternalEnumerator_1__ctor_m15783_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15783_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15784_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Player_Script/Player_State>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15784_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15784/* method */
	, &InternalEnumerator_1_t3097_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15784_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15785_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Player_Script/Player_State>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15785_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m15785/* method */
	, &InternalEnumerator_1_t3097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15785_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15786_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Player_Script/Player_State>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15786_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m15786/* method */
	, &InternalEnumerator_1_t3097_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15786_GenericMethod/* genericMethod */

};
extern Il2CppType Player_State_t106_0_0_0;
extern void* RuntimeInvoker_Player_State_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15787_GenericMethod;
// T System.Array/InternalEnumerator`1<Player_Script/Player_State>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15787_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m15787/* method */
	, &InternalEnumerator_1_t3097_il2cpp_TypeInfo/* declaring_type */
	, &Player_State_t106_0_0_0/* return_type */
	, RuntimeInvoker_Player_State_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15787_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3097_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15783_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15784_MethodInfo,
	&InternalEnumerator_1_Dispose_m15785_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15786_MethodInfo,
	&InternalEnumerator_1_get_Current_m15787_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3097_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15784_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15786_MethodInfo,
	&InternalEnumerator_1_Dispose_m15785_MethodInfo,
	&InternalEnumerator_1_get_Current_m15787_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3097_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6187_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3097_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6187_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3097_0_0_0;
extern Il2CppType InternalEnumerator_1_t3097_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3097_GenericClass;
TypeInfo InternalEnumerator_1_t3097_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3097_MethodInfos/* methods */
	, InternalEnumerator_1_t3097_PropertyInfos/* properties */
	, InternalEnumerator_1_t3097_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3097_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3097_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3097_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3097_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3097_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3097_1_0_0/* this_arg */
	, InternalEnumerator_1_t3097_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3097_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3097)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7907_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Player_Script/Player_State>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Player_Script/Player_State>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Player_Script/Player_State>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Player_Script/Player_State>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Player_Script/Player_State>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Player_Script/Player_State>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Player_Script/Player_State>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Player_Script/Player_State>
extern MethodInfo ICollection_1_get_Count_m44277_MethodInfo;
static PropertyInfo ICollection_1_t7907____Count_PropertyInfo = 
{
	&ICollection_1_t7907_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44277_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44278_MethodInfo;
static PropertyInfo ICollection_1_t7907____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7907_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44278_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7907_PropertyInfos[] =
{
	&ICollection_1_t7907____Count_PropertyInfo,
	&ICollection_1_t7907____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44277_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Player_Script/Player_State>::get_Count()
MethodInfo ICollection_1_get_Count_m44277_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7907_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44277_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44278_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Player_Script/Player_State>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44278_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7907_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44278_GenericMethod/* genericMethod */

};
extern Il2CppType Player_State_t106_0_0_0;
extern Il2CppType Player_State_t106_0_0_0;
static ParameterInfo ICollection_1_t7907_ICollection_1_Add_m44279_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Player_State_t106_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44279_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Player_Script/Player_State>::Add(T)
MethodInfo ICollection_1_Add_m44279_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, ICollection_1_t7907_ICollection_1_Add_m44279_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44279_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44280_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Player_Script/Player_State>::Clear()
MethodInfo ICollection_1_Clear_m44280_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44280_GenericMethod/* genericMethod */

};
extern Il2CppType Player_State_t106_0_0_0;
static ParameterInfo ICollection_1_t7907_ICollection_1_Contains_m44281_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Player_State_t106_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44281_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Player_Script/Player_State>::Contains(T)
MethodInfo ICollection_1_Contains_m44281_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7907_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t7907_ICollection_1_Contains_m44281_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44281_GenericMethod/* genericMethod */

};
extern Il2CppType Player_StateU5BU5D_t5409_0_0_0;
extern Il2CppType Player_StateU5BU5D_t5409_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7907_ICollection_1_CopyTo_m44282_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Player_StateU5BU5D_t5409_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44282_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Player_Script/Player_State>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44282_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7907_ICollection_1_CopyTo_m44282_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44282_GenericMethod/* genericMethod */

};
extern Il2CppType Player_State_t106_0_0_0;
static ParameterInfo ICollection_1_t7907_ICollection_1_Remove_m44283_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Player_State_t106_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44283_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Player_Script/Player_State>::Remove(T)
MethodInfo ICollection_1_Remove_m44283_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7907_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Int32_t123/* invoker_method */
	, ICollection_1_t7907_ICollection_1_Remove_m44283_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44283_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7907_MethodInfos[] =
{
	&ICollection_1_get_Count_m44277_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44278_MethodInfo,
	&ICollection_1_Add_m44279_MethodInfo,
	&ICollection_1_Clear_m44280_MethodInfo,
	&ICollection_1_Contains_m44281_MethodInfo,
	&ICollection_1_CopyTo_m44282_MethodInfo,
	&ICollection_1_Remove_m44283_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7909_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7907_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7909_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7907_0_0_0;
extern Il2CppType ICollection_1_t7907_1_0_0;
struct ICollection_1_t7907;
extern Il2CppGenericClass ICollection_1_t7907_GenericClass;
TypeInfo ICollection_1_t7907_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7907_MethodInfos/* methods */
	, ICollection_1_t7907_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7907_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7907_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7907_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7907_0_0_0/* byval_arg */
	, &ICollection_1_t7907_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7907_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Player_Script/Player_State>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Player_Script/Player_State>
extern Il2CppType IEnumerator_1_t6187_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44284_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Player_Script/Player_State>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44284_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7909_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6187_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44284_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7909_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44284_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7909_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7909_0_0_0;
extern Il2CppType IEnumerable_1_t7909_1_0_0;
struct IEnumerable_1_t7909;
extern Il2CppGenericClass IEnumerable_1_t7909_GenericClass;
TypeInfo IEnumerable_1_t7909_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7909_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7909_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7909_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7909_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7909_0_0_0/* byval_arg */
	, &IEnumerable_1_t7909_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7909_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7908_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Player_Script/Player_State>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Player_Script/Player_State>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Player_Script/Player_State>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Player_Script/Player_State>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Player_Script/Player_State>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Player_Script/Player_State>
extern MethodInfo IList_1_get_Item_m44285_MethodInfo;
extern MethodInfo IList_1_set_Item_m44286_MethodInfo;
static PropertyInfo IList_1_t7908____Item_PropertyInfo = 
{
	&IList_1_t7908_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44285_MethodInfo/* get */
	, &IList_1_set_Item_m44286_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7908_PropertyInfos[] =
{
	&IList_1_t7908____Item_PropertyInfo,
	NULL
};
extern Il2CppType Player_State_t106_0_0_0;
static ParameterInfo IList_1_t7908_IList_1_IndexOf_m44287_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Player_State_t106_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44287_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Player_Script/Player_State>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44287_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7908_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7908_IList_1_IndexOf_m44287_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44287_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Player_State_t106_0_0_0;
static ParameterInfo IList_1_t7908_IList_1_Insert_m44288_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Player_State_t106_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44288_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Player_Script/Player_State>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44288_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7908_IList_1_Insert_m44288_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44288_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7908_IList_1_RemoveAt_m44289_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44289_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Player_Script/Player_State>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44289_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7908_IList_1_RemoveAt_m44289_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44289_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7908_IList_1_get_Item_m44285_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Player_State_t106_0_0_0;
extern void* RuntimeInvoker_Player_State_t106_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44285_GenericMethod;
// T System.Collections.Generic.IList`1<Player_Script/Player_State>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44285_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7908_il2cpp_TypeInfo/* declaring_type */
	, &Player_State_t106_0_0_0/* return_type */
	, RuntimeInvoker_Player_State_t106_Int32_t123/* invoker_method */
	, IList_1_t7908_IList_1_get_Item_m44285_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44285_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Player_State_t106_0_0_0;
static ParameterInfo IList_1_t7908_IList_1_set_Item_m44286_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Player_State_t106_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44286_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Player_Script/Player_State>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44286_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Int32_t123/* invoker_method */
	, IList_1_t7908_IList_1_set_Item_m44286_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44286_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7908_MethodInfos[] =
{
	&IList_1_IndexOf_m44287_MethodInfo,
	&IList_1_Insert_m44288_MethodInfo,
	&IList_1_RemoveAt_m44289_MethodInfo,
	&IList_1_get_Item_m44285_MethodInfo,
	&IList_1_set_Item_m44286_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7908_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7907_il2cpp_TypeInfo,
	&IEnumerable_1_t7909_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7908_0_0_0;
extern Il2CppType IList_1_t7908_1_0_0;
struct IList_1_t7908;
extern Il2CppGenericClass IList_1_t7908_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7908_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7908_MethodInfos/* methods */
	, IList_1_t7908_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7908_il2cpp_TypeInfo/* element_class */
	, IList_1_t7908_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7908_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7908_0_0_0/* byval_arg */
	, &IList_1_t7908_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7908_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6189_il2cpp_TypeInfo;

// Shoot_Script
#include "AssemblyU2DCSharp_Shoot_Script.h"


// T System.Collections.Generic.IEnumerator`1<Shoot_Script>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Shoot_Script>
extern MethodInfo IEnumerator_1_get_Current_m44290_MethodInfo;
static PropertyInfo IEnumerator_1_t6189____Current_PropertyInfo = 
{
	&IEnumerator_1_t6189_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44290_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6189_PropertyInfos[] =
{
	&IEnumerator_1_t6189____Current_PropertyInfo,
	NULL
};
extern Il2CppType Shoot_Script_t109_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44290_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Shoot_Script>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44290_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6189_il2cpp_TypeInfo/* declaring_type */
	, &Shoot_Script_t109_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44290_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6189_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44290_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6189_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6189_0_0_0;
extern Il2CppType IEnumerator_1_t6189_1_0_0;
struct IEnumerator_1_t6189;
extern Il2CppGenericClass IEnumerator_1_t6189_GenericClass;
TypeInfo IEnumerator_1_t6189_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6189_MethodInfos/* methods */
	, IEnumerator_1_t6189_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6189_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6189_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6189_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6189_0_0_0/* byval_arg */
	, &IEnumerator_1_t6189_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6189_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Shoot_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_137.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3098_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Shoot_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_137MethodDeclarations.h"

extern TypeInfo Shoot_Script_t109_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15792_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisShoot_Script_t109_m33851_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m32233_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m32233(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<Shoot_Script>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Shoot_Script>(System.Int32)
#define Array_InternalArray__get_Item_TisShoot_Script_t109_m33851(__this, p0, method) (Shoot_Script_t109 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Shoot_Script>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Shoot_Script>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Shoot_Script>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Shoot_Script>::MoveNext()
// T System.Array/InternalEnumerator`1<Shoot_Script>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Shoot_Script>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3098____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3098_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3098, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3098____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3098_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3098, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3098_FieldInfos[] =
{
	&InternalEnumerator_1_t3098____array_0_FieldInfo,
	&InternalEnumerator_1_t3098____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15789_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3098____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3098_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15789_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3098____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3098_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15792_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3098_PropertyInfos[] =
{
	&InternalEnumerator_1_t3098____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3098____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3098_InternalEnumerator_1__ctor_m15788_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15788_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Shoot_Script>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15788_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3098_InternalEnumerator_1__ctor_m15788_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15788_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15789_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Shoot_Script>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15789_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3098_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15789_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15790_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Shoot_Script>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15790_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15790_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15791_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Shoot_Script>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15791_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3098_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15791_GenericMethod/* genericMethod */

};
extern Il2CppType Shoot_Script_t109_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15792_GenericMethod;
// T System.Array/InternalEnumerator`1<Shoot_Script>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15792_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3098_il2cpp_TypeInfo/* declaring_type */
	, &Shoot_Script_t109_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15792_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3098_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15788_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15789_MethodInfo,
	&InternalEnumerator_1_Dispose_m15790_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15791_MethodInfo,
	&InternalEnumerator_1_get_Current_m15792_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15791_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15790_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3098_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15789_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15791_MethodInfo,
	&InternalEnumerator_1_Dispose_m15790_MethodInfo,
	&InternalEnumerator_1_get_Current_m15792_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3098_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6189_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3098_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6189_il2cpp_TypeInfo, 7},
};
extern TypeInfo Shoot_Script_t109_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3098_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15792_MethodInfo/* Method Usage */,
	&Shoot_Script_t109_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisShoot_Script_t109_m33851_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3098_0_0_0;
extern Il2CppType InternalEnumerator_1_t3098_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3098_GenericClass;
TypeInfo InternalEnumerator_1_t3098_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3098_MethodInfos/* methods */
	, InternalEnumerator_1_t3098_PropertyInfos/* properties */
	, InternalEnumerator_1_t3098_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3098_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3098_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3098_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3098_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3098_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3098_1_0_0/* this_arg */
	, InternalEnumerator_1_t3098_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3098_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3098_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3098)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7910_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Shoot_Script>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Shoot_Script>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Shoot_Script>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Shoot_Script>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Shoot_Script>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Shoot_Script>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Shoot_Script>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Shoot_Script>
extern MethodInfo ICollection_1_get_Count_m44291_MethodInfo;
static PropertyInfo ICollection_1_t7910____Count_PropertyInfo = 
{
	&ICollection_1_t7910_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44291_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44292_MethodInfo;
static PropertyInfo ICollection_1_t7910____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7910_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44292_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7910_PropertyInfos[] =
{
	&ICollection_1_t7910____Count_PropertyInfo,
	&ICollection_1_t7910____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44291_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Shoot_Script>::get_Count()
MethodInfo ICollection_1_get_Count_m44291_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7910_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44291_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44292_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Shoot_Script>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44292_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7910_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44292_GenericMethod/* genericMethod */

};
extern Il2CppType Shoot_Script_t109_0_0_0;
extern Il2CppType Shoot_Script_t109_0_0_0;
static ParameterInfo ICollection_1_t7910_ICollection_1_Add_m44293_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Shoot_Script_t109_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44293_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Shoot_Script>::Add(T)
MethodInfo ICollection_1_Add_m44293_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7910_ICollection_1_Add_m44293_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44293_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44294_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Shoot_Script>::Clear()
MethodInfo ICollection_1_Clear_m44294_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44294_GenericMethod/* genericMethod */

};
extern Il2CppType Shoot_Script_t109_0_0_0;
static ParameterInfo ICollection_1_t7910_ICollection_1_Contains_m44295_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Shoot_Script_t109_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44295_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Shoot_Script>::Contains(T)
MethodInfo ICollection_1_Contains_m44295_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7910_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7910_ICollection_1_Contains_m44295_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44295_GenericMethod/* genericMethod */

};
extern Il2CppType Shoot_ScriptU5BU5D_t5410_0_0_0;
extern Il2CppType Shoot_ScriptU5BU5D_t5410_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7910_ICollection_1_CopyTo_m44296_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Shoot_ScriptU5BU5D_t5410_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44296_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Shoot_Script>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44296_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7910_ICollection_1_CopyTo_m44296_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44296_GenericMethod/* genericMethod */

};
extern Il2CppType Shoot_Script_t109_0_0_0;
static ParameterInfo ICollection_1_t7910_ICollection_1_Remove_m44297_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Shoot_Script_t109_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44297_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Shoot_Script>::Remove(T)
MethodInfo ICollection_1_Remove_m44297_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7910_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7910_ICollection_1_Remove_m44297_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44297_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7910_MethodInfos[] =
{
	&ICollection_1_get_Count_m44291_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44292_MethodInfo,
	&ICollection_1_Add_m44293_MethodInfo,
	&ICollection_1_Clear_m44294_MethodInfo,
	&ICollection_1_Contains_m44295_MethodInfo,
	&ICollection_1_CopyTo_m44296_MethodInfo,
	&ICollection_1_Remove_m44297_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7912_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7910_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7912_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7910_0_0_0;
extern Il2CppType ICollection_1_t7910_1_0_0;
struct ICollection_1_t7910;
extern Il2CppGenericClass ICollection_1_t7910_GenericClass;
TypeInfo ICollection_1_t7910_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7910_MethodInfos/* methods */
	, ICollection_1_t7910_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7910_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7910_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7910_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7910_0_0_0/* byval_arg */
	, &ICollection_1_t7910_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7910_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Shoot_Script>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Shoot_Script>
extern Il2CppType IEnumerator_1_t6189_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44298_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Shoot_Script>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44298_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7912_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6189_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44298_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7912_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44298_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7912_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7912_0_0_0;
extern Il2CppType IEnumerable_1_t7912_1_0_0;
struct IEnumerable_1_t7912;
extern Il2CppGenericClass IEnumerable_1_t7912_GenericClass;
TypeInfo IEnumerable_1_t7912_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7912_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7912_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7912_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7912_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7912_0_0_0/* byval_arg */
	, &IEnumerable_1_t7912_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7912_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7911_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Shoot_Script>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Shoot_Script>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Shoot_Script>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Shoot_Script>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Shoot_Script>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Shoot_Script>
extern MethodInfo IList_1_get_Item_m44299_MethodInfo;
extern MethodInfo IList_1_set_Item_m44300_MethodInfo;
static PropertyInfo IList_1_t7911____Item_PropertyInfo = 
{
	&IList_1_t7911_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44299_MethodInfo/* get */
	, &IList_1_set_Item_m44300_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7911_PropertyInfos[] =
{
	&IList_1_t7911____Item_PropertyInfo,
	NULL
};
extern Il2CppType Shoot_Script_t109_0_0_0;
static ParameterInfo IList_1_t7911_IList_1_IndexOf_m44301_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Shoot_Script_t109_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44301_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Shoot_Script>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44301_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7911_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7911_IList_1_IndexOf_m44301_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44301_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Shoot_Script_t109_0_0_0;
static ParameterInfo IList_1_t7911_IList_1_Insert_m44302_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Shoot_Script_t109_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44302_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Shoot_Script>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44302_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7911_IList_1_Insert_m44302_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44302_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7911_IList_1_RemoveAt_m44303_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44303_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Shoot_Script>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44303_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7911_IList_1_RemoveAt_m44303_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44303_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7911_IList_1_get_Item_m44299_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Shoot_Script_t109_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44299_GenericMethod;
// T System.Collections.Generic.IList`1<Shoot_Script>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44299_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7911_il2cpp_TypeInfo/* declaring_type */
	, &Shoot_Script_t109_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7911_IList_1_get_Item_m44299_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44299_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Shoot_Script_t109_0_0_0;
static ParameterInfo IList_1_t7911_IList_1_set_Item_m44300_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Shoot_Script_t109_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44300_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Shoot_Script>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44300_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7911_IList_1_set_Item_m44300_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44300_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7911_MethodInfos[] =
{
	&IList_1_IndexOf_m44301_MethodInfo,
	&IList_1_Insert_m44302_MethodInfo,
	&IList_1_RemoveAt_m44303_MethodInfo,
	&IList_1_get_Item_m44299_MethodInfo,
	&IList_1_set_Item_m44300_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7911_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7910_il2cpp_TypeInfo,
	&IEnumerable_1_t7912_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7911_0_0_0;
extern Il2CppType IList_1_t7911_1_0_0;
struct IList_1_t7911;
extern Il2CppGenericClass IList_1_t7911_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7911_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7911_MethodInfos/* methods */
	, IList_1_t7911_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7911_il2cpp_TypeInfo/* element_class */
	, IList_1_t7911_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7911_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7911_0_0_0/* byval_arg */
	, &IList_1_t7911_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7911_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Shoot_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_54.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3099_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Shoot_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_54MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.Events.InvokableCall`1<Shoot_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_50.h"
extern TypeInfo ObjectU5BU5D_t130_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t3100_il2cpp_TypeInfo;
extern TypeInfo Void_t111_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Shoot_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_50MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15795_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15797_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Shoot_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Shoot_Script>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Shoot_Script>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3099____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3099_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3099, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3099_FieldInfos[] =
{
	&CachedInvokableCall_1_t3099____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Shoot_Script_t109_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3099_CachedInvokableCall_1__ctor_m15793_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Shoot_Script_t109_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15793_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Shoot_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15793_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3099_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3099_CachedInvokableCall_1__ctor_m15793_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15793_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3099_CachedInvokableCall_1_Invoke_m15794_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15794_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Shoot_Script>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15794_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3099_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3099_CachedInvokableCall_1_Invoke_m15794_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15794_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3099_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15793_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15794_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m320_MethodInfo;
extern MethodInfo Object_GetHashCode_m321_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m15794_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15798_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3099_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15794_MethodInfo,
	&InvokableCall_1_Find_m15798_MethodInfo,
};
extern Il2CppType UnityAction_1_t3101_0_0_0;
extern TypeInfo UnityAction_1_t3101_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisShoot_Script_t109_m33861_MethodInfo;
extern TypeInfo Shoot_Script_t109_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15800_MethodInfo;
extern TypeInfo Shoot_Script_t109_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3099_RGCTXData[8] = 
{
	&UnityAction_1_t3101_0_0_0/* Type Usage */,
	&UnityAction_1_t3101_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisShoot_Script_t109_m33861_MethodInfo/* Method Usage */,
	&Shoot_Script_t109_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15800_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15795_MethodInfo/* Method Usage */,
	&Shoot_Script_t109_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15797_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3099_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3099_1_0_0;
struct CachedInvokableCall_1_t3099;
extern Il2CppGenericClass CachedInvokableCall_1_t3099_GenericClass;
TypeInfo CachedInvokableCall_1_t3099_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3099_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3099_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3100_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3099_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3099_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3099_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3099_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3099_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3099_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3099_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3099)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Shoot_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_57.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t3101_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t551_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<Shoot_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_57MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6534_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m255_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m330_MethodInfo;
extern MethodInfo Delegate_Combine_m2327_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6533_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2636_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6535_MethodInfo;
extern MethodInfo Delegate_get_Target_m6713_MethodInfo;
extern MethodInfo Delegate_get_Method_m6711_MethodInfo;
struct BaseInvokableCall_t1127;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Shoot_Script>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Shoot_Script>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisShoot_Script_t109_m33861(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Shoot_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Shoot_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Shoot_Script>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Shoot_Script>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Shoot_Script>
extern Il2CppType UnityAction_1_t3101_0_0_1;
FieldInfo InvokableCall_1_t3100____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3101_0_0_1/* type */
	, &InvokableCall_1_t3100_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3100, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3100_FieldInfos[] =
{
	&InvokableCall_1_t3100____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3100_InvokableCall_1__ctor_m15795_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15795_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Shoot_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15795_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3100_InvokableCall_1__ctor_m15795_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15795_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3101_0_0_0;
static ParameterInfo InvokableCall_1_t3100_InvokableCall_1__ctor_m15796_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3101_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15796_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Shoot_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15796_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3100_InvokableCall_1__ctor_m15796_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15796_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3100_InvokableCall_1_Invoke_m15797_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15797_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Shoot_Script>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15797_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3100_InvokableCall_1_Invoke_m15797_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15797_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3100_InvokableCall_1_Find_m15798_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15798_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Shoot_Script>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15798_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3100_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3100_InvokableCall_1_Find_m15798_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15798_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3100_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15795_MethodInfo,
	&InvokableCall_1__ctor_m15796_MethodInfo,
	&InvokableCall_1_Invoke_m15797_MethodInfo,
	&InvokableCall_1_Find_m15798_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3100_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15797_MethodInfo,
	&InvokableCall_1_Find_m15798_MethodInfo,
};
extern TypeInfo UnityAction_1_t3101_il2cpp_TypeInfo;
extern TypeInfo Shoot_Script_t109_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3100_RGCTXData[5] = 
{
	&UnityAction_1_t3101_0_0_0/* Type Usage */,
	&UnityAction_1_t3101_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisShoot_Script_t109_m33861_MethodInfo/* Method Usage */,
	&Shoot_Script_t109_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15800_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3100_0_0_0;
extern Il2CppType InvokableCall_1_t3100_1_0_0;
extern TypeInfo BaseInvokableCall_t1127_il2cpp_TypeInfo;
struct InvokableCall_1_t3100;
extern Il2CppGenericClass InvokableCall_1_t3100_GenericClass;
TypeInfo InvokableCall_1_t3100_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3100_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3100_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3100_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3100_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3100_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3100_0_0_0/* byval_arg */
	, &InvokableCall_1_t3100_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3100_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3100_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3100)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Shoot_Script>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Shoot_Script>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Shoot_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Shoot_Script>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Shoot_Script>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3101_UnityAction_1__ctor_m15799_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15799_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Shoot_Script>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15799_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3101_UnityAction_1__ctor_m15799_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15799_GenericMethod/* genericMethod */

};
extern Il2CppType Shoot_Script_t109_0_0_0;
static ParameterInfo UnityAction_1_t3101_UnityAction_1_Invoke_m15800_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Shoot_Script_t109_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15800_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Shoot_Script>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15800_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3101_UnityAction_1_Invoke_m15800_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15800_GenericMethod/* genericMethod */

};
extern Il2CppType Shoot_Script_t109_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3101_UnityAction_1_BeginInvoke_m15801_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Shoot_Script_t109_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15801_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Shoot_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15801_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3101_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3101_UnityAction_1_BeginInvoke_m15801_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15801_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3101_UnityAction_1_EndInvoke_m15802_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15802_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Shoot_Script>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15802_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3101_UnityAction_1_EndInvoke_m15802_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15802_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3101_MethodInfos[] =
{
	&UnityAction_1__ctor_m15799_MethodInfo,
	&UnityAction_1_Invoke_m15800_MethodInfo,
	&UnityAction_1_BeginInvoke_m15801_MethodInfo,
	&UnityAction_1_EndInvoke_m15802_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15801_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15802_MethodInfo;
static MethodInfo* UnityAction_1_t3101_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15800_MethodInfo,
	&UnityAction_1_BeginInvoke_m15801_MethodInfo,
	&UnityAction_1_EndInvoke_m15802_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3101_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3101_1_0_0;
struct UnityAction_1_t3101;
extern Il2CppGenericClass UnityAction_1_t3101_GenericClass;
TypeInfo UnityAction_1_t3101_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3101_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3101_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3101_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3101_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3101_0_0_0/* byval_arg */
	, &UnityAction_1_t3101_1_0_0/* this_arg */
	, UnityAction_1_t3101_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3101_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3101)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6191_il2cpp_TypeInfo;

// Sombra_Script
#include "AssemblyU2DCSharp_Sombra_Script.h"


// T System.Collections.Generic.IEnumerator`1<Sombra_Script>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Sombra_Script>
extern MethodInfo IEnumerator_1_get_Current_m44304_MethodInfo;
static PropertyInfo IEnumerator_1_t6191____Current_PropertyInfo = 
{
	&IEnumerator_1_t6191_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44304_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6191_PropertyInfos[] =
{
	&IEnumerator_1_t6191____Current_PropertyInfo,
	NULL
};
extern Il2CppType Sombra_Script_t110_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44304_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Sombra_Script>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44304_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6191_il2cpp_TypeInfo/* declaring_type */
	, &Sombra_Script_t110_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44304_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6191_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44304_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6191_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6191_0_0_0;
extern Il2CppType IEnumerator_1_t6191_1_0_0;
struct IEnumerator_1_t6191;
extern Il2CppGenericClass IEnumerator_1_t6191_GenericClass;
TypeInfo IEnumerator_1_t6191_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6191_MethodInfos/* methods */
	, IEnumerator_1_t6191_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6191_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6191_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6191_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6191_0_0_0/* byval_arg */
	, &IEnumerator_1_t6191_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6191_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Sombra_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_138.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3102_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Sombra_Script>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_138MethodDeclarations.h"

extern TypeInfo Sombra_Script_t110_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15807_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSombra_Script_t110_m33863_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Sombra_Script>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Sombra_Script>(System.Int32)
#define Array_InternalArray__get_Item_TisSombra_Script_t110_m33863(__this, p0, method) (Sombra_Script_t110 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Sombra_Script>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Sombra_Script>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Sombra_Script>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Sombra_Script>::MoveNext()
// T System.Array/InternalEnumerator`1<Sombra_Script>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Sombra_Script>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3102____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3102_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3102, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3102____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3102_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3102, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3102_FieldInfos[] =
{
	&InternalEnumerator_1_t3102____array_0_FieldInfo,
	&InternalEnumerator_1_t3102____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15804_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3102____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3102_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15804_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3102____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3102_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15807_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3102_PropertyInfos[] =
{
	&InternalEnumerator_1_t3102____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3102____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3102_InternalEnumerator_1__ctor_m15803_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15803_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Sombra_Script>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15803_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3102_InternalEnumerator_1__ctor_m15803_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15803_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15804_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Sombra_Script>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15804_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3102_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15804_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15805_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Sombra_Script>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15805_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15805_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15806_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Sombra_Script>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15806_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3102_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15806_GenericMethod/* genericMethod */

};
extern Il2CppType Sombra_Script_t110_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15807_GenericMethod;
// T System.Array/InternalEnumerator`1<Sombra_Script>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15807_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3102_il2cpp_TypeInfo/* declaring_type */
	, &Sombra_Script_t110_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15807_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3102_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15803_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15804_MethodInfo,
	&InternalEnumerator_1_Dispose_m15805_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15806_MethodInfo,
	&InternalEnumerator_1_get_Current_m15807_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15806_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15805_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3102_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15804_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15806_MethodInfo,
	&InternalEnumerator_1_Dispose_m15805_MethodInfo,
	&InternalEnumerator_1_get_Current_m15807_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3102_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6191_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3102_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6191_il2cpp_TypeInfo, 7},
};
extern TypeInfo Sombra_Script_t110_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3102_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15807_MethodInfo/* Method Usage */,
	&Sombra_Script_t110_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSombra_Script_t110_m33863_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3102_0_0_0;
extern Il2CppType InternalEnumerator_1_t3102_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3102_GenericClass;
TypeInfo InternalEnumerator_1_t3102_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3102_MethodInfos/* methods */
	, InternalEnumerator_1_t3102_PropertyInfos/* properties */
	, InternalEnumerator_1_t3102_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3102_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3102_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3102_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3102_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3102_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3102_1_0_0/* this_arg */
	, InternalEnumerator_1_t3102_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3102_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3102_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3102)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7913_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Sombra_Script>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Sombra_Script>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Sombra_Script>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Sombra_Script>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Sombra_Script>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Sombra_Script>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Sombra_Script>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Sombra_Script>
extern MethodInfo ICollection_1_get_Count_m44305_MethodInfo;
static PropertyInfo ICollection_1_t7913____Count_PropertyInfo = 
{
	&ICollection_1_t7913_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44305_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44306_MethodInfo;
static PropertyInfo ICollection_1_t7913____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7913_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44306_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7913_PropertyInfos[] =
{
	&ICollection_1_t7913____Count_PropertyInfo,
	&ICollection_1_t7913____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44305_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Sombra_Script>::get_Count()
MethodInfo ICollection_1_get_Count_m44305_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7913_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44305_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44306_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Sombra_Script>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44306_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7913_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44306_GenericMethod/* genericMethod */

};
extern Il2CppType Sombra_Script_t110_0_0_0;
extern Il2CppType Sombra_Script_t110_0_0_0;
static ParameterInfo ICollection_1_t7913_ICollection_1_Add_m44307_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sombra_Script_t110_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44307_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Sombra_Script>::Add(T)
MethodInfo ICollection_1_Add_m44307_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7913_ICollection_1_Add_m44307_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44307_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44308_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Sombra_Script>::Clear()
MethodInfo ICollection_1_Clear_m44308_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44308_GenericMethod/* genericMethod */

};
extern Il2CppType Sombra_Script_t110_0_0_0;
static ParameterInfo ICollection_1_t7913_ICollection_1_Contains_m44309_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sombra_Script_t110_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44309_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Sombra_Script>::Contains(T)
MethodInfo ICollection_1_Contains_m44309_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7913_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7913_ICollection_1_Contains_m44309_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44309_GenericMethod/* genericMethod */

};
extern Il2CppType Sombra_ScriptU5BU5D_t5411_0_0_0;
extern Il2CppType Sombra_ScriptU5BU5D_t5411_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7913_ICollection_1_CopyTo_m44310_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Sombra_ScriptU5BU5D_t5411_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44310_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Sombra_Script>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44310_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7913_ICollection_1_CopyTo_m44310_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44310_GenericMethod/* genericMethod */

};
extern Il2CppType Sombra_Script_t110_0_0_0;
static ParameterInfo ICollection_1_t7913_ICollection_1_Remove_m44311_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sombra_Script_t110_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44311_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Sombra_Script>::Remove(T)
MethodInfo ICollection_1_Remove_m44311_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7913_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7913_ICollection_1_Remove_m44311_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44311_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7913_MethodInfos[] =
{
	&ICollection_1_get_Count_m44305_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44306_MethodInfo,
	&ICollection_1_Add_m44307_MethodInfo,
	&ICollection_1_Clear_m44308_MethodInfo,
	&ICollection_1_Contains_m44309_MethodInfo,
	&ICollection_1_CopyTo_m44310_MethodInfo,
	&ICollection_1_Remove_m44311_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7915_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7913_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7915_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7913_0_0_0;
extern Il2CppType ICollection_1_t7913_1_0_0;
struct ICollection_1_t7913;
extern Il2CppGenericClass ICollection_1_t7913_GenericClass;
TypeInfo ICollection_1_t7913_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7913_MethodInfos/* methods */
	, ICollection_1_t7913_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7913_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7913_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7913_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7913_0_0_0/* byval_arg */
	, &ICollection_1_t7913_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7913_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Sombra_Script>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Sombra_Script>
extern Il2CppType IEnumerator_1_t6191_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44312_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Sombra_Script>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44312_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7915_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6191_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44312_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7915_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44312_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7915_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7915_0_0_0;
extern Il2CppType IEnumerable_1_t7915_1_0_0;
struct IEnumerable_1_t7915;
extern Il2CppGenericClass IEnumerable_1_t7915_GenericClass;
TypeInfo IEnumerable_1_t7915_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7915_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7915_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7915_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7915_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7915_0_0_0/* byval_arg */
	, &IEnumerable_1_t7915_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7915_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7914_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Sombra_Script>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Sombra_Script>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Sombra_Script>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Sombra_Script>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Sombra_Script>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Sombra_Script>
extern MethodInfo IList_1_get_Item_m44313_MethodInfo;
extern MethodInfo IList_1_set_Item_m44314_MethodInfo;
static PropertyInfo IList_1_t7914____Item_PropertyInfo = 
{
	&IList_1_t7914_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44313_MethodInfo/* get */
	, &IList_1_set_Item_m44314_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7914_PropertyInfos[] =
{
	&IList_1_t7914____Item_PropertyInfo,
	NULL
};
extern Il2CppType Sombra_Script_t110_0_0_0;
static ParameterInfo IList_1_t7914_IList_1_IndexOf_m44315_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sombra_Script_t110_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44315_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Sombra_Script>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44315_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7914_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7914_IList_1_IndexOf_m44315_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44315_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Sombra_Script_t110_0_0_0;
static ParameterInfo IList_1_t7914_IList_1_Insert_m44316_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Sombra_Script_t110_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44316_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Sombra_Script>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44316_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7914_IList_1_Insert_m44316_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44316_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7914_IList_1_RemoveAt_m44317_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44317_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Sombra_Script>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44317_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7914_IList_1_RemoveAt_m44317_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44317_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7914_IList_1_get_Item_m44313_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Sombra_Script_t110_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44313_GenericMethod;
// T System.Collections.Generic.IList`1<Sombra_Script>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44313_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7914_il2cpp_TypeInfo/* declaring_type */
	, &Sombra_Script_t110_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7914_IList_1_get_Item_m44313_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44313_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Sombra_Script_t110_0_0_0;
static ParameterInfo IList_1_t7914_IList_1_set_Item_m44314_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Sombra_Script_t110_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44314_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Sombra_Script>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44314_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7914_IList_1_set_Item_m44314_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44314_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7914_MethodInfos[] =
{
	&IList_1_IndexOf_m44315_MethodInfo,
	&IList_1_Insert_m44316_MethodInfo,
	&IList_1_RemoveAt_m44317_MethodInfo,
	&IList_1_get_Item_m44313_MethodInfo,
	&IList_1_set_Item_m44314_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7914_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7913_il2cpp_TypeInfo,
	&IEnumerable_1_t7915_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7914_0_0_0;
extern Il2CppType IList_1_t7914_1_0_0;
struct IList_1_t7914;
extern Il2CppGenericClass IList_1_t7914_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7914_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7914_MethodInfos/* methods */
	, IList_1_t7914_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7914_il2cpp_TypeInfo/* element_class */
	, IList_1_t7914_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7914_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7914_0_0_0/* byval_arg */
	, &IList_1_t7914_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7914_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Sombra_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_55.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3103_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Sombra_Script>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_55MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Sombra_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_51.h"
extern TypeInfo InvokableCall_1_t3104_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Sombra_Script>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_51MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15810_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15812_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Sombra_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Sombra_Script>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Sombra_Script>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3103____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3103_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3103, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3103_FieldInfos[] =
{
	&CachedInvokableCall_1_t3103____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Sombra_Script_t110_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3103_CachedInvokableCall_1__ctor_m15808_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Sombra_Script_t110_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15808_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Sombra_Script>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15808_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3103_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3103_CachedInvokableCall_1__ctor_m15808_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15808_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3103_CachedInvokableCall_1_Invoke_m15809_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15809_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Sombra_Script>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15809_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3103_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3103_CachedInvokableCall_1_Invoke_m15809_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15809_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3103_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15808_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15809_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15809_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15813_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3103_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15809_MethodInfo,
	&InvokableCall_1_Find_m15813_MethodInfo,
};
extern Il2CppType UnityAction_1_t3105_0_0_0;
extern TypeInfo UnityAction_1_t3105_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisSombra_Script_t110_m33873_MethodInfo;
extern TypeInfo Sombra_Script_t110_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15815_MethodInfo;
extern TypeInfo Sombra_Script_t110_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3103_RGCTXData[8] = 
{
	&UnityAction_1_t3105_0_0_0/* Type Usage */,
	&UnityAction_1_t3105_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSombra_Script_t110_m33873_MethodInfo/* Method Usage */,
	&Sombra_Script_t110_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15815_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15810_MethodInfo/* Method Usage */,
	&Sombra_Script_t110_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15812_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3103_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3103_1_0_0;
struct CachedInvokableCall_1_t3103;
extern Il2CppGenericClass CachedInvokableCall_1_t3103_GenericClass;
TypeInfo CachedInvokableCall_1_t3103_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3103_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3103_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3104_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3103_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3103_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3103_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3103_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3103_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3103_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3103_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3103)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Sombra_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_58.h"
extern TypeInfo UnityAction_1_t3105_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Sombra_Script>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_58MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Sombra_Script>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Sombra_Script>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisSombra_Script_t110_m33873(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Sombra_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Sombra_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Sombra_Script>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Sombra_Script>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Sombra_Script>
extern Il2CppType UnityAction_1_t3105_0_0_1;
FieldInfo InvokableCall_1_t3104____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3105_0_0_1/* type */
	, &InvokableCall_1_t3104_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3104, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3104_FieldInfos[] =
{
	&InvokableCall_1_t3104____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3104_InvokableCall_1__ctor_m15810_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15810_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Sombra_Script>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15810_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3104_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3104_InvokableCall_1__ctor_m15810_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15810_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3105_0_0_0;
static ParameterInfo InvokableCall_1_t3104_InvokableCall_1__ctor_m15811_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3105_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15811_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Sombra_Script>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15811_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3104_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3104_InvokableCall_1__ctor_m15811_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15811_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3104_InvokableCall_1_Invoke_m15812_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15812_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Sombra_Script>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15812_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3104_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3104_InvokableCall_1_Invoke_m15812_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15812_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3104_InvokableCall_1_Find_m15813_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15813_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Sombra_Script>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15813_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3104_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3104_InvokableCall_1_Find_m15813_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15813_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3104_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15810_MethodInfo,
	&InvokableCall_1__ctor_m15811_MethodInfo,
	&InvokableCall_1_Invoke_m15812_MethodInfo,
	&InvokableCall_1_Find_m15813_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3104_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15812_MethodInfo,
	&InvokableCall_1_Find_m15813_MethodInfo,
};
extern TypeInfo UnityAction_1_t3105_il2cpp_TypeInfo;
extern TypeInfo Sombra_Script_t110_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3104_RGCTXData[5] = 
{
	&UnityAction_1_t3105_0_0_0/* Type Usage */,
	&UnityAction_1_t3105_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSombra_Script_t110_m33873_MethodInfo/* Method Usage */,
	&Sombra_Script_t110_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15815_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3104_0_0_0;
extern Il2CppType InvokableCall_1_t3104_1_0_0;
struct InvokableCall_1_t3104;
extern Il2CppGenericClass InvokableCall_1_t3104_GenericClass;
TypeInfo InvokableCall_1_t3104_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3104_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3104_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3104_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3104_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3104_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3104_0_0_0/* byval_arg */
	, &InvokableCall_1_t3104_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3104_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3104_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3104)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Sombra_Script>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Sombra_Script>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Sombra_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Sombra_Script>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Sombra_Script>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3105_UnityAction_1__ctor_m15814_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15814_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Sombra_Script>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15814_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3105_UnityAction_1__ctor_m15814_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15814_GenericMethod/* genericMethod */

};
extern Il2CppType Sombra_Script_t110_0_0_0;
static ParameterInfo UnityAction_1_t3105_UnityAction_1_Invoke_m15815_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Sombra_Script_t110_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15815_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Sombra_Script>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15815_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3105_UnityAction_1_Invoke_m15815_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15815_GenericMethod/* genericMethod */

};
extern Il2CppType Sombra_Script_t110_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3105_UnityAction_1_BeginInvoke_m15816_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Sombra_Script_t110_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15816_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Sombra_Script>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15816_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3105_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3105_UnityAction_1_BeginInvoke_m15816_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15816_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3105_UnityAction_1_EndInvoke_m15817_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15817_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Sombra_Script>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15817_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3105_UnityAction_1_EndInvoke_m15817_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15817_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3105_MethodInfos[] =
{
	&UnityAction_1__ctor_m15814_MethodInfo,
	&UnityAction_1_Invoke_m15815_MethodInfo,
	&UnityAction_1_BeginInvoke_m15816_MethodInfo,
	&UnityAction_1_EndInvoke_m15817_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15816_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15817_MethodInfo;
static MethodInfo* UnityAction_1_t3105_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15815_MethodInfo,
	&UnityAction_1_BeginInvoke_m15816_MethodInfo,
	&UnityAction_1_EndInvoke_m15817_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3105_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3105_1_0_0;
struct UnityAction_1_t3105;
extern Il2CppGenericClass UnityAction_1_t3105_GenericClass;
TypeInfo UnityAction_1_t3105_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3105_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3105_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3105_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3105_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3105_0_0_0/* byval_arg */
	, &UnityAction_1_t3105_1_0_0/* this_arg */
	, UnityAction_1_t3105_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3105_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3105)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Sphere>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_56.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3106_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Sphere>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_56MethodDeclarations.h"

// Sphere
#include "AssemblyU2DCSharp_Sphere.h"
// UnityEngine.Events.InvokableCall`1<Sphere>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_52.h"
extern TypeInfo Sphere_t71_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t3107_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Sphere>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_52MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15820_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15822_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Sphere>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Sphere>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Sphere>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3106____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3106_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3106, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3106_FieldInfos[] =
{
	&CachedInvokableCall_1_t3106____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Sphere_t71_0_0_0;
extern Il2CppType Sphere_t71_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3106_CachedInvokableCall_1__ctor_m15818_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Sphere_t71_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15818_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Sphere>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15818_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3106_CachedInvokableCall_1__ctor_m15818_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15818_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3106_CachedInvokableCall_1_Invoke_m15819_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15819_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Sphere>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15819_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3106_CachedInvokableCall_1_Invoke_m15819_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15819_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3106_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15818_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15819_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15819_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15823_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3106_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15819_MethodInfo,
	&InvokableCall_1_Find_m15823_MethodInfo,
};
extern Il2CppType UnityAction_1_t3108_0_0_0;
extern TypeInfo UnityAction_1_t3108_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisSphere_t71_m33874_MethodInfo;
extern TypeInfo Sphere_t71_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15825_MethodInfo;
extern TypeInfo Sphere_t71_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3106_RGCTXData[8] = 
{
	&UnityAction_1_t3108_0_0_0/* Type Usage */,
	&UnityAction_1_t3108_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSphere_t71_m33874_MethodInfo/* Method Usage */,
	&Sphere_t71_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15825_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15820_MethodInfo/* Method Usage */,
	&Sphere_t71_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15822_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3106_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3106_1_0_0;
struct CachedInvokableCall_1_t3106;
extern Il2CppGenericClass CachedInvokableCall_1_t3106_GenericClass;
TypeInfo CachedInvokableCall_1_t3106_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3106_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3106_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3107_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3106_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3106_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3106_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3106_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3106_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3106_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3106_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3106)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Sphere>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_59.h"
extern TypeInfo UnityAction_1_t3108_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Sphere>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_59MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Sphere>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Sphere>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisSphere_t71_m33874(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Sphere>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Sphere>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Sphere>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Sphere>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Sphere>
extern Il2CppType UnityAction_1_t3108_0_0_1;
FieldInfo InvokableCall_1_t3107____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3108_0_0_1/* type */
	, &InvokableCall_1_t3107_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3107, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3107_FieldInfos[] =
{
	&InvokableCall_1_t3107____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3107_InvokableCall_1__ctor_m15820_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15820_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Sphere>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15820_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3107_InvokableCall_1__ctor_m15820_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15820_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3108_0_0_0;
static ParameterInfo InvokableCall_1_t3107_InvokableCall_1__ctor_m15821_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3108_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15821_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Sphere>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15821_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3107_InvokableCall_1__ctor_m15821_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15821_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3107_InvokableCall_1_Invoke_m15822_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15822_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Sphere>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15822_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3107_InvokableCall_1_Invoke_m15822_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15822_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3107_InvokableCall_1_Find_m15823_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15823_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Sphere>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15823_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3107_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3107_InvokableCall_1_Find_m15823_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15823_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3107_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15820_MethodInfo,
	&InvokableCall_1__ctor_m15821_MethodInfo,
	&InvokableCall_1_Invoke_m15822_MethodInfo,
	&InvokableCall_1_Find_m15823_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3107_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15822_MethodInfo,
	&InvokableCall_1_Find_m15823_MethodInfo,
};
extern TypeInfo UnityAction_1_t3108_il2cpp_TypeInfo;
extern TypeInfo Sphere_t71_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3107_RGCTXData[5] = 
{
	&UnityAction_1_t3108_0_0_0/* Type Usage */,
	&UnityAction_1_t3108_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSphere_t71_m33874_MethodInfo/* Method Usage */,
	&Sphere_t71_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15825_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3107_0_0_0;
extern Il2CppType InvokableCall_1_t3107_1_0_0;
struct InvokableCall_1_t3107;
extern Il2CppGenericClass InvokableCall_1_t3107_GenericClass;
TypeInfo InvokableCall_1_t3107_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3107_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3107_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3107_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3107_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3107_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3107_0_0_0/* byval_arg */
	, &InvokableCall_1_t3107_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3107_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3107_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3107)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Sphere>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Sphere>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Sphere>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Sphere>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Sphere>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3108_UnityAction_1__ctor_m15824_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15824_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Sphere>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15824_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3108_UnityAction_1__ctor_m15824_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15824_GenericMethod/* genericMethod */

};
extern Il2CppType Sphere_t71_0_0_0;
static ParameterInfo UnityAction_1_t3108_UnityAction_1_Invoke_m15825_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Sphere_t71_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15825_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Sphere>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15825_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3108_UnityAction_1_Invoke_m15825_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15825_GenericMethod/* genericMethod */

};
extern Il2CppType Sphere_t71_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3108_UnityAction_1_BeginInvoke_m15826_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Sphere_t71_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15826_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Sphere>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15826_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3108_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3108_UnityAction_1_BeginInvoke_m15826_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15826_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3108_UnityAction_1_EndInvoke_m15827_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15827_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Sphere>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15827_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3108_UnityAction_1_EndInvoke_m15827_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15827_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3108_MethodInfos[] =
{
	&UnityAction_1__ctor_m15824_MethodInfo,
	&UnityAction_1_Invoke_m15825_MethodInfo,
	&UnityAction_1_BeginInvoke_m15826_MethodInfo,
	&UnityAction_1_EndInvoke_m15827_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15826_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15827_MethodInfo;
static MethodInfo* UnityAction_1_t3108_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15825_MethodInfo,
	&UnityAction_1_BeginInvoke_m15826_MethodInfo,
	&UnityAction_1_EndInvoke_m15827_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3108_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3108_1_0_0;
struct UnityAction_1_t3108;
extern Il2CppGenericClass UnityAction_1_t3108_GenericClass;
TypeInfo UnityAction_1_t3108_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3108_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3108_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3108_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3108_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3108_0_0_0/* byval_arg */
	, &UnityAction_1_t3108_1_0_0/* this_arg */
	, UnityAction_1_t3108_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3108_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3108)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6193_il2cpp_TypeInfo;

// Jump_Button
#include "AssemblyU2DUnityScript_Jump_Button.h"


// T System.Collections.Generic.IEnumerator`1<Jump_Button>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Jump_Button>
extern MethodInfo IEnumerator_1_get_Current_m44318_MethodInfo;
static PropertyInfo IEnumerator_1_t6193____Current_PropertyInfo = 
{
	&IEnumerator_1_t6193_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44318_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6193_PropertyInfos[] =
{
	&IEnumerator_1_t6193____Current_PropertyInfo,
	NULL
};
extern Il2CppType Jump_Button_t207_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44318_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Jump_Button>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44318_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6193_il2cpp_TypeInfo/* declaring_type */
	, &Jump_Button_t207_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44318_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6193_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44318_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6193_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6193_0_0_0;
extern Il2CppType IEnumerator_1_t6193_1_0_0;
struct IEnumerator_1_t6193;
extern Il2CppGenericClass IEnumerator_1_t6193_GenericClass;
TypeInfo IEnumerator_1_t6193_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6193_MethodInfos/* methods */
	, IEnumerator_1_t6193_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6193_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6193_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6193_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6193_0_0_0/* byval_arg */
	, &IEnumerator_1_t6193_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6193_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Jump_Button>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_139.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3109_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Jump_Button>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_139MethodDeclarations.h"

extern TypeInfo Jump_Button_t207_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15832_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisJump_Button_t207_m33876_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Jump_Button>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Jump_Button>(System.Int32)
#define Array_InternalArray__get_Item_TisJump_Button_t207_m33876(__this, p0, method) (Jump_Button_t207 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Jump_Button>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Jump_Button>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Jump_Button>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Jump_Button>::MoveNext()
// T System.Array/InternalEnumerator`1<Jump_Button>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Jump_Button>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3109____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3109_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3109, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3109____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3109_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3109, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3109_FieldInfos[] =
{
	&InternalEnumerator_1_t3109____array_0_FieldInfo,
	&InternalEnumerator_1_t3109____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15829_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3109____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3109_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15829_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3109____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3109_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15832_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3109_PropertyInfos[] =
{
	&InternalEnumerator_1_t3109____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3109____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3109_InternalEnumerator_1__ctor_m15828_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15828_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Jump_Button>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15828_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3109_InternalEnumerator_1__ctor_m15828_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15828_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15829_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Jump_Button>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15829_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3109_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15829_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15830_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Jump_Button>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15830_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15830_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15831_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Jump_Button>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15831_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3109_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15831_GenericMethod/* genericMethod */

};
extern Il2CppType Jump_Button_t207_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15832_GenericMethod;
// T System.Array/InternalEnumerator`1<Jump_Button>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15832_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3109_il2cpp_TypeInfo/* declaring_type */
	, &Jump_Button_t207_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15832_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3109_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15828_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15829_MethodInfo,
	&InternalEnumerator_1_Dispose_m15830_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15831_MethodInfo,
	&InternalEnumerator_1_get_Current_m15832_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15831_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15830_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3109_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15829_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15831_MethodInfo,
	&InternalEnumerator_1_Dispose_m15830_MethodInfo,
	&InternalEnumerator_1_get_Current_m15832_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3109_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6193_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3109_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6193_il2cpp_TypeInfo, 7},
};
extern TypeInfo Jump_Button_t207_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3109_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15832_MethodInfo/* Method Usage */,
	&Jump_Button_t207_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisJump_Button_t207_m33876_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3109_0_0_0;
extern Il2CppType InternalEnumerator_1_t3109_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3109_GenericClass;
TypeInfo InternalEnumerator_1_t3109_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3109_MethodInfos/* methods */
	, InternalEnumerator_1_t3109_PropertyInfos/* properties */
	, InternalEnumerator_1_t3109_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3109_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3109_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3109_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3109_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3109_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3109_1_0_0/* this_arg */
	, InternalEnumerator_1_t3109_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3109_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3109_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3109)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7916_il2cpp_TypeInfo;

#include "Assembly-UnityScript_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Jump_Button>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Jump_Button>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Jump_Button>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Jump_Button>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Jump_Button>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Jump_Button>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Jump_Button>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Jump_Button>
extern MethodInfo ICollection_1_get_Count_m44319_MethodInfo;
static PropertyInfo ICollection_1_t7916____Count_PropertyInfo = 
{
	&ICollection_1_t7916_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44319_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44320_MethodInfo;
static PropertyInfo ICollection_1_t7916____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7916_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44320_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7916_PropertyInfos[] =
{
	&ICollection_1_t7916____Count_PropertyInfo,
	&ICollection_1_t7916____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44319_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Jump_Button>::get_Count()
MethodInfo ICollection_1_get_Count_m44319_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7916_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44319_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44320_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Jump_Button>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44320_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7916_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44320_GenericMethod/* genericMethod */

};
extern Il2CppType Jump_Button_t207_0_0_0;
extern Il2CppType Jump_Button_t207_0_0_0;
static ParameterInfo ICollection_1_t7916_ICollection_1_Add_m44321_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Jump_Button_t207_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44321_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Jump_Button>::Add(T)
MethodInfo ICollection_1_Add_m44321_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7916_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7916_ICollection_1_Add_m44321_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44321_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44322_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Jump_Button>::Clear()
MethodInfo ICollection_1_Clear_m44322_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7916_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44322_GenericMethod/* genericMethod */

};
extern Il2CppType Jump_Button_t207_0_0_0;
static ParameterInfo ICollection_1_t7916_ICollection_1_Contains_m44323_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Jump_Button_t207_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44323_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Jump_Button>::Contains(T)
MethodInfo ICollection_1_Contains_m44323_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7916_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7916_ICollection_1_Contains_m44323_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44323_GenericMethod/* genericMethod */

};
extern Il2CppType Jump_ButtonU5BU5D_t5771_0_0_0;
extern Il2CppType Jump_ButtonU5BU5D_t5771_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7916_ICollection_1_CopyTo_m44324_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Jump_ButtonU5BU5D_t5771_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44324_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Jump_Button>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44324_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7916_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7916_ICollection_1_CopyTo_m44324_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44324_GenericMethod/* genericMethod */

};
extern Il2CppType Jump_Button_t207_0_0_0;
static ParameterInfo ICollection_1_t7916_ICollection_1_Remove_m44325_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Jump_Button_t207_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44325_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Jump_Button>::Remove(T)
MethodInfo ICollection_1_Remove_m44325_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7916_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7916_ICollection_1_Remove_m44325_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44325_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7916_MethodInfos[] =
{
	&ICollection_1_get_Count_m44319_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44320_MethodInfo,
	&ICollection_1_Add_m44321_MethodInfo,
	&ICollection_1_Clear_m44322_MethodInfo,
	&ICollection_1_Contains_m44323_MethodInfo,
	&ICollection_1_CopyTo_m44324_MethodInfo,
	&ICollection_1_Remove_m44325_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7918_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7916_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7918_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7916_0_0_0;
extern Il2CppType ICollection_1_t7916_1_0_0;
struct ICollection_1_t7916;
extern Il2CppGenericClass ICollection_1_t7916_GenericClass;
TypeInfo ICollection_1_t7916_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7916_MethodInfos/* methods */
	, ICollection_1_t7916_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7916_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7916_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7916_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7916_0_0_0/* byval_arg */
	, &ICollection_1_t7916_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7916_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Jump_Button>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Jump_Button>
extern Il2CppType IEnumerator_1_t6193_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44326_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Jump_Button>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44326_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7918_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6193_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44326_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7918_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44326_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7918_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7918_0_0_0;
extern Il2CppType IEnumerable_1_t7918_1_0_0;
struct IEnumerable_1_t7918;
extern Il2CppGenericClass IEnumerable_1_t7918_GenericClass;
TypeInfo IEnumerable_1_t7918_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7918_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7918_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7918_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7918_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7918_0_0_0/* byval_arg */
	, &IEnumerable_1_t7918_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7918_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7917_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Jump_Button>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Jump_Button>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Jump_Button>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Jump_Button>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Jump_Button>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Jump_Button>
extern MethodInfo IList_1_get_Item_m44327_MethodInfo;
extern MethodInfo IList_1_set_Item_m44328_MethodInfo;
static PropertyInfo IList_1_t7917____Item_PropertyInfo = 
{
	&IList_1_t7917_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44327_MethodInfo/* get */
	, &IList_1_set_Item_m44328_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7917_PropertyInfos[] =
{
	&IList_1_t7917____Item_PropertyInfo,
	NULL
};
extern Il2CppType Jump_Button_t207_0_0_0;
static ParameterInfo IList_1_t7917_IList_1_IndexOf_m44329_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Jump_Button_t207_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44329_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Jump_Button>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44329_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7917_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7917_IList_1_IndexOf_m44329_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44329_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Jump_Button_t207_0_0_0;
static ParameterInfo IList_1_t7917_IList_1_Insert_m44330_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Jump_Button_t207_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44330_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Jump_Button>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44330_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7917_IList_1_Insert_m44330_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44330_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7917_IList_1_RemoveAt_m44331_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44331_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Jump_Button>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44331_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7917_IList_1_RemoveAt_m44331_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44331_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7917_IList_1_get_Item_m44327_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Jump_Button_t207_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44327_GenericMethod;
// T System.Collections.Generic.IList`1<Jump_Button>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44327_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7917_il2cpp_TypeInfo/* declaring_type */
	, &Jump_Button_t207_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7917_IList_1_get_Item_m44327_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44327_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Jump_Button_t207_0_0_0;
static ParameterInfo IList_1_t7917_IList_1_set_Item_m44328_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Jump_Button_t207_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44328_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Jump_Button>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44328_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7917_IList_1_set_Item_m44328_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44328_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7917_MethodInfos[] =
{
	&IList_1_IndexOf_m44329_MethodInfo,
	&IList_1_Insert_m44330_MethodInfo,
	&IList_1_RemoveAt_m44331_MethodInfo,
	&IList_1_get_Item_m44327_MethodInfo,
	&IList_1_set_Item_m44328_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7917_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7916_il2cpp_TypeInfo,
	&IEnumerable_1_t7918_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7917_0_0_0;
extern Il2CppType IList_1_t7917_1_0_0;
struct IList_1_t7917;
extern Il2CppGenericClass IList_1_t7917_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7917_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7917_MethodInfos/* methods */
	, IList_1_t7917_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7917_il2cpp_TypeInfo/* element_class */
	, IList_1_t7917_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7917_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7917_0_0_0/* byval_arg */
	, &IList_1_t7917_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7917_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Jump_Button>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_57.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3110_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Jump_Button>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_57MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Jump_Button>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_53.h"
extern TypeInfo InvokableCall_1_t3111_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Jump_Button>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_53MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15835_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15837_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Jump_Button>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Jump_Button>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Jump_Button>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3110____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3110_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3110, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3110_FieldInfos[] =
{
	&CachedInvokableCall_1_t3110____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Jump_Button_t207_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3110_CachedInvokableCall_1__ctor_m15833_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Jump_Button_t207_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15833_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Jump_Button>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15833_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3110_CachedInvokableCall_1__ctor_m15833_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15833_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3110_CachedInvokableCall_1_Invoke_m15834_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15834_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Jump_Button>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15834_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3110_CachedInvokableCall_1_Invoke_m15834_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15834_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3110_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15833_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15834_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15834_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15838_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3110_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15834_MethodInfo,
	&InvokableCall_1_Find_m15838_MethodInfo,
};
extern Il2CppType UnityAction_1_t3112_0_0_0;
extern TypeInfo UnityAction_1_t3112_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisJump_Button_t207_m33886_MethodInfo;
extern TypeInfo Jump_Button_t207_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15840_MethodInfo;
extern TypeInfo Jump_Button_t207_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3110_RGCTXData[8] = 
{
	&UnityAction_1_t3112_0_0_0/* Type Usage */,
	&UnityAction_1_t3112_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisJump_Button_t207_m33886_MethodInfo/* Method Usage */,
	&Jump_Button_t207_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15840_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15835_MethodInfo/* Method Usage */,
	&Jump_Button_t207_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15837_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3110_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3110_1_0_0;
struct CachedInvokableCall_1_t3110;
extern Il2CppGenericClass CachedInvokableCall_1_t3110_GenericClass;
TypeInfo CachedInvokableCall_1_t3110_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3110_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3110_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3111_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3110_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3110_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3110_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3110_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3110_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3110_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3110_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3110)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Jump_Button>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_60.h"
extern TypeInfo UnityAction_1_t3112_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Jump_Button>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_60MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Jump_Button>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Jump_Button>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisJump_Button_t207_m33886(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Jump_Button>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Jump_Button>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Jump_Button>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Jump_Button>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Jump_Button>
extern Il2CppType UnityAction_1_t3112_0_0_1;
FieldInfo InvokableCall_1_t3111____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3112_0_0_1/* type */
	, &InvokableCall_1_t3111_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3111, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3111_FieldInfos[] =
{
	&InvokableCall_1_t3111____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3111_InvokableCall_1__ctor_m15835_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15835_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Jump_Button>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15835_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3111_InvokableCall_1__ctor_m15835_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15835_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3112_0_0_0;
static ParameterInfo InvokableCall_1_t3111_InvokableCall_1__ctor_m15836_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3112_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15836_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Jump_Button>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15836_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3111_InvokableCall_1__ctor_m15836_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15836_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3111_InvokableCall_1_Invoke_m15837_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15837_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Jump_Button>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15837_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3111_InvokableCall_1_Invoke_m15837_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15837_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3111_InvokableCall_1_Find_m15838_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15838_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Jump_Button>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15838_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3111_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3111_InvokableCall_1_Find_m15838_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15838_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3111_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15835_MethodInfo,
	&InvokableCall_1__ctor_m15836_MethodInfo,
	&InvokableCall_1_Invoke_m15837_MethodInfo,
	&InvokableCall_1_Find_m15838_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3111_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15837_MethodInfo,
	&InvokableCall_1_Find_m15838_MethodInfo,
};
extern TypeInfo UnityAction_1_t3112_il2cpp_TypeInfo;
extern TypeInfo Jump_Button_t207_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3111_RGCTXData[5] = 
{
	&UnityAction_1_t3112_0_0_0/* Type Usage */,
	&UnityAction_1_t3112_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisJump_Button_t207_m33886_MethodInfo/* Method Usage */,
	&Jump_Button_t207_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15840_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3111_0_0_0;
extern Il2CppType InvokableCall_1_t3111_1_0_0;
struct InvokableCall_1_t3111;
extern Il2CppGenericClass InvokableCall_1_t3111_GenericClass;
TypeInfo InvokableCall_1_t3111_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3111_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3111_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3111_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3111_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3111_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3111_0_0_0/* byval_arg */
	, &InvokableCall_1_t3111_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3111_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3111_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3111)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Jump_Button>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Jump_Button>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Jump_Button>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Jump_Button>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Jump_Button>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3112_UnityAction_1__ctor_m15839_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15839_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Jump_Button>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15839_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3112_UnityAction_1__ctor_m15839_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15839_GenericMethod/* genericMethod */

};
extern Il2CppType Jump_Button_t207_0_0_0;
static ParameterInfo UnityAction_1_t3112_UnityAction_1_Invoke_m15840_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Jump_Button_t207_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15840_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Jump_Button>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15840_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3112_UnityAction_1_Invoke_m15840_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15840_GenericMethod/* genericMethod */

};
extern Il2CppType Jump_Button_t207_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3112_UnityAction_1_BeginInvoke_m15841_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Jump_Button_t207_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15841_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Jump_Button>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15841_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3112_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3112_UnityAction_1_BeginInvoke_m15841_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15841_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3112_UnityAction_1_EndInvoke_m15842_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15842_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Jump_Button>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15842_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3112_UnityAction_1_EndInvoke_m15842_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15842_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3112_MethodInfos[] =
{
	&UnityAction_1__ctor_m15839_MethodInfo,
	&UnityAction_1_Invoke_m15840_MethodInfo,
	&UnityAction_1_BeginInvoke_m15841_MethodInfo,
	&UnityAction_1_EndInvoke_m15842_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15841_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15842_MethodInfo;
static MethodInfo* UnityAction_1_t3112_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15840_MethodInfo,
	&UnityAction_1_BeginInvoke_m15841_MethodInfo,
	&UnityAction_1_EndInvoke_m15842_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3112_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3112_1_0_0;
struct UnityAction_1_t3112;
extern Il2CppGenericClass UnityAction_1_t3112_GenericClass;
TypeInfo UnityAction_1_t3112_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3112_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3112_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3112_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3112_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3112_0_0_0/* byval_arg */
	, &UnityAction_1_t3112_1_0_0/* this_arg */
	, UnityAction_1_t3112_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3112_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3112)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6195_il2cpp_TypeInfo;

// CameraRelativeControl
#include "AssemblyU2DUnityScript_CameraRelativeControl.h"


// T System.Collections.Generic.IEnumerator`1<CameraRelativeControl>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<CameraRelativeControl>
extern MethodInfo IEnumerator_1_get_Current_m44332_MethodInfo;
static PropertyInfo IEnumerator_1_t6195____Current_PropertyInfo = 
{
	&IEnumerator_1_t6195_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44332_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6195_PropertyInfos[] =
{
	&IEnumerator_1_t6195____Current_PropertyInfo,
	NULL
};
extern Il2CppType CameraRelativeControl_t210_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44332_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<CameraRelativeControl>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44332_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6195_il2cpp_TypeInfo/* declaring_type */
	, &CameraRelativeControl_t210_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44332_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6195_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44332_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6195_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6195_0_0_0;
extern Il2CppType IEnumerator_1_t6195_1_0_0;
struct IEnumerator_1_t6195;
extern Il2CppGenericClass IEnumerator_1_t6195_GenericClass;
TypeInfo IEnumerator_1_t6195_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6195_MethodInfos/* methods */
	, IEnumerator_1_t6195_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6195_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6195_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6195_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6195_0_0_0/* byval_arg */
	, &IEnumerator_1_t6195_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6195_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<CameraRelativeControl>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_140.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3113_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<CameraRelativeControl>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_140MethodDeclarations.h"

extern TypeInfo CameraRelativeControl_t210_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15847_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCameraRelativeControl_t210_m33888_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<CameraRelativeControl>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<CameraRelativeControl>(System.Int32)
#define Array_InternalArray__get_Item_TisCameraRelativeControl_t210_m33888(__this, p0, method) (CameraRelativeControl_t210 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<CameraRelativeControl>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<CameraRelativeControl>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<CameraRelativeControl>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<CameraRelativeControl>::MoveNext()
// T System.Array/InternalEnumerator`1<CameraRelativeControl>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<CameraRelativeControl>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3113____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3113_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3113, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3113____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3113_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3113, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3113_FieldInfos[] =
{
	&InternalEnumerator_1_t3113____array_0_FieldInfo,
	&InternalEnumerator_1_t3113____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15844_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3113____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3113_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15844_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3113____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3113_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15847_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3113_PropertyInfos[] =
{
	&InternalEnumerator_1_t3113____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3113____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3113_InternalEnumerator_1__ctor_m15843_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15843_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<CameraRelativeControl>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15843_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3113_InternalEnumerator_1__ctor_m15843_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15843_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15844_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<CameraRelativeControl>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15844_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3113_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15844_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15845_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<CameraRelativeControl>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15845_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15845_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15846_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<CameraRelativeControl>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15846_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3113_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15846_GenericMethod/* genericMethod */

};
extern Il2CppType CameraRelativeControl_t210_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15847_GenericMethod;
// T System.Array/InternalEnumerator`1<CameraRelativeControl>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15847_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3113_il2cpp_TypeInfo/* declaring_type */
	, &CameraRelativeControl_t210_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15847_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3113_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15843_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15844_MethodInfo,
	&InternalEnumerator_1_Dispose_m15845_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15846_MethodInfo,
	&InternalEnumerator_1_get_Current_m15847_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15846_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15845_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3113_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15844_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15846_MethodInfo,
	&InternalEnumerator_1_Dispose_m15845_MethodInfo,
	&InternalEnumerator_1_get_Current_m15847_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3113_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6195_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3113_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6195_il2cpp_TypeInfo, 7},
};
extern TypeInfo CameraRelativeControl_t210_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3113_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15847_MethodInfo/* Method Usage */,
	&CameraRelativeControl_t210_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCameraRelativeControl_t210_m33888_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3113_0_0_0;
extern Il2CppType InternalEnumerator_1_t3113_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3113_GenericClass;
TypeInfo InternalEnumerator_1_t3113_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3113_MethodInfos/* methods */
	, InternalEnumerator_1_t3113_PropertyInfos/* properties */
	, InternalEnumerator_1_t3113_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3113_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3113_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3113_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3113_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3113_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3113_1_0_0/* this_arg */
	, InternalEnumerator_1_t3113_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3113_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3113_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3113)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7919_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<CameraRelativeControl>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<CameraRelativeControl>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<CameraRelativeControl>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<CameraRelativeControl>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<CameraRelativeControl>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<CameraRelativeControl>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<CameraRelativeControl>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<CameraRelativeControl>
extern MethodInfo ICollection_1_get_Count_m44333_MethodInfo;
static PropertyInfo ICollection_1_t7919____Count_PropertyInfo = 
{
	&ICollection_1_t7919_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44333_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44334_MethodInfo;
static PropertyInfo ICollection_1_t7919____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7919_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44334_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7919_PropertyInfos[] =
{
	&ICollection_1_t7919____Count_PropertyInfo,
	&ICollection_1_t7919____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44333_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<CameraRelativeControl>::get_Count()
MethodInfo ICollection_1_get_Count_m44333_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7919_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44333_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44334_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<CameraRelativeControl>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44334_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7919_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44334_GenericMethod/* genericMethod */

};
extern Il2CppType CameraRelativeControl_t210_0_0_0;
extern Il2CppType CameraRelativeControl_t210_0_0_0;
static ParameterInfo ICollection_1_t7919_ICollection_1_Add_m44335_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CameraRelativeControl_t210_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44335_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<CameraRelativeControl>::Add(T)
MethodInfo ICollection_1_Add_m44335_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7919_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7919_ICollection_1_Add_m44335_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44335_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44336_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<CameraRelativeControl>::Clear()
MethodInfo ICollection_1_Clear_m44336_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7919_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44336_GenericMethod/* genericMethod */

};
extern Il2CppType CameraRelativeControl_t210_0_0_0;
static ParameterInfo ICollection_1_t7919_ICollection_1_Contains_m44337_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CameraRelativeControl_t210_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44337_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<CameraRelativeControl>::Contains(T)
MethodInfo ICollection_1_Contains_m44337_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7919_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7919_ICollection_1_Contains_m44337_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44337_GenericMethod/* genericMethod */

};
extern Il2CppType CameraRelativeControlU5BU5D_t5772_0_0_0;
extern Il2CppType CameraRelativeControlU5BU5D_t5772_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7919_ICollection_1_CopyTo_m44338_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CameraRelativeControlU5BU5D_t5772_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44338_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<CameraRelativeControl>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44338_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7919_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7919_ICollection_1_CopyTo_m44338_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44338_GenericMethod/* genericMethod */

};
extern Il2CppType CameraRelativeControl_t210_0_0_0;
static ParameterInfo ICollection_1_t7919_ICollection_1_Remove_m44339_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CameraRelativeControl_t210_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44339_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<CameraRelativeControl>::Remove(T)
MethodInfo ICollection_1_Remove_m44339_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7919_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7919_ICollection_1_Remove_m44339_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44339_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7919_MethodInfos[] =
{
	&ICollection_1_get_Count_m44333_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44334_MethodInfo,
	&ICollection_1_Add_m44335_MethodInfo,
	&ICollection_1_Clear_m44336_MethodInfo,
	&ICollection_1_Contains_m44337_MethodInfo,
	&ICollection_1_CopyTo_m44338_MethodInfo,
	&ICollection_1_Remove_m44339_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7921_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7919_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7921_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7919_0_0_0;
extern Il2CppType ICollection_1_t7919_1_0_0;
struct ICollection_1_t7919;
extern Il2CppGenericClass ICollection_1_t7919_GenericClass;
TypeInfo ICollection_1_t7919_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7919_MethodInfos/* methods */
	, ICollection_1_t7919_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7919_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7919_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7919_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7919_0_0_0/* byval_arg */
	, &ICollection_1_t7919_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7919_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<CameraRelativeControl>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<CameraRelativeControl>
extern Il2CppType IEnumerator_1_t6195_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44340_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<CameraRelativeControl>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44340_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7921_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6195_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44340_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7921_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44340_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7921_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7921_0_0_0;
extern Il2CppType IEnumerable_1_t7921_1_0_0;
struct IEnumerable_1_t7921;
extern Il2CppGenericClass IEnumerable_1_t7921_GenericClass;
TypeInfo IEnumerable_1_t7921_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7921_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7921_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7921_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7921_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7921_0_0_0/* byval_arg */
	, &IEnumerable_1_t7921_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7921_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7920_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<CameraRelativeControl>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<CameraRelativeControl>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<CameraRelativeControl>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<CameraRelativeControl>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<CameraRelativeControl>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<CameraRelativeControl>
extern MethodInfo IList_1_get_Item_m44341_MethodInfo;
extern MethodInfo IList_1_set_Item_m44342_MethodInfo;
static PropertyInfo IList_1_t7920____Item_PropertyInfo = 
{
	&IList_1_t7920_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44341_MethodInfo/* get */
	, &IList_1_set_Item_m44342_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7920_PropertyInfos[] =
{
	&IList_1_t7920____Item_PropertyInfo,
	NULL
};
extern Il2CppType CameraRelativeControl_t210_0_0_0;
static ParameterInfo IList_1_t7920_IList_1_IndexOf_m44343_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CameraRelativeControl_t210_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44343_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<CameraRelativeControl>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44343_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7920_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7920_IList_1_IndexOf_m44343_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44343_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CameraRelativeControl_t210_0_0_0;
static ParameterInfo IList_1_t7920_IList_1_Insert_m44344_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CameraRelativeControl_t210_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44344_GenericMethod;
// System.Void System.Collections.Generic.IList`1<CameraRelativeControl>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44344_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7920_IList_1_Insert_m44344_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44344_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7920_IList_1_RemoveAt_m44345_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44345_GenericMethod;
// System.Void System.Collections.Generic.IList`1<CameraRelativeControl>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44345_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7920_IList_1_RemoveAt_m44345_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44345_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7920_IList_1_get_Item_m44341_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType CameraRelativeControl_t210_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44341_GenericMethod;
// T System.Collections.Generic.IList`1<CameraRelativeControl>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44341_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7920_il2cpp_TypeInfo/* declaring_type */
	, &CameraRelativeControl_t210_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7920_IList_1_get_Item_m44341_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44341_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CameraRelativeControl_t210_0_0_0;
static ParameterInfo IList_1_t7920_IList_1_set_Item_m44342_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CameraRelativeControl_t210_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44342_GenericMethod;
// System.Void System.Collections.Generic.IList`1<CameraRelativeControl>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44342_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7920_IList_1_set_Item_m44342_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44342_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7920_MethodInfos[] =
{
	&IList_1_IndexOf_m44343_MethodInfo,
	&IList_1_Insert_m44344_MethodInfo,
	&IList_1_RemoveAt_m44345_MethodInfo,
	&IList_1_get_Item_m44341_MethodInfo,
	&IList_1_set_Item_m44342_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7920_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7919_il2cpp_TypeInfo,
	&IEnumerable_1_t7921_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7920_0_0_0;
extern Il2CppType IList_1_t7920_1_0_0;
struct IList_1_t7920;
extern Il2CppGenericClass IList_1_t7920_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7920_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7920_MethodInfos/* methods */
	, IList_1_t7920_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7920_il2cpp_TypeInfo/* element_class */
	, IList_1_t7920_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7920_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7920_0_0_0/* byval_arg */
	, &IList_1_t7920_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7920_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<CameraRelativeControl>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_58.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3114_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<CameraRelativeControl>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_58MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<CameraRelativeControl>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_54.h"
extern TypeInfo InvokableCall_1_t3115_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<CameraRelativeControl>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_54MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15850_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15852_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<CameraRelativeControl>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<CameraRelativeControl>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<CameraRelativeControl>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3114____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3114_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3114, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3114_FieldInfos[] =
{
	&CachedInvokableCall_1_t3114____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType CameraRelativeControl_t210_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3114_CachedInvokableCall_1__ctor_m15848_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &CameraRelativeControl_t210_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15848_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<CameraRelativeControl>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15848_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3114_CachedInvokableCall_1__ctor_m15848_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15848_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3114_CachedInvokableCall_1_Invoke_m15849_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15849_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<CameraRelativeControl>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15849_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3114_CachedInvokableCall_1_Invoke_m15849_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15849_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3114_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15848_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15849_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15849_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15853_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3114_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15849_MethodInfo,
	&InvokableCall_1_Find_m15853_MethodInfo,
};
extern Il2CppType UnityAction_1_t3116_0_0_0;
extern TypeInfo UnityAction_1_t3116_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisCameraRelativeControl_t210_m33898_MethodInfo;
extern TypeInfo CameraRelativeControl_t210_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15855_MethodInfo;
extern TypeInfo CameraRelativeControl_t210_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3114_RGCTXData[8] = 
{
	&UnityAction_1_t3116_0_0_0/* Type Usage */,
	&UnityAction_1_t3116_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCameraRelativeControl_t210_m33898_MethodInfo/* Method Usage */,
	&CameraRelativeControl_t210_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15855_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15850_MethodInfo/* Method Usage */,
	&CameraRelativeControl_t210_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15852_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3114_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3114_1_0_0;
struct CachedInvokableCall_1_t3114;
extern Il2CppGenericClass CachedInvokableCall_1_t3114_GenericClass;
TypeInfo CachedInvokableCall_1_t3114_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3114_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3114_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3115_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3114_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3114_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3114_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3114_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3114_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3114_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3114_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3114)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<CameraRelativeControl>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_61.h"
extern TypeInfo UnityAction_1_t3116_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<CameraRelativeControl>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_61MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<CameraRelativeControl>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<CameraRelativeControl>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisCameraRelativeControl_t210_m33898(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<CameraRelativeControl>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<CameraRelativeControl>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<CameraRelativeControl>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<CameraRelativeControl>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<CameraRelativeControl>
extern Il2CppType UnityAction_1_t3116_0_0_1;
FieldInfo InvokableCall_1_t3115____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3116_0_0_1/* type */
	, &InvokableCall_1_t3115_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3115, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3115_FieldInfos[] =
{
	&InvokableCall_1_t3115____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3115_InvokableCall_1__ctor_m15850_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15850_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<CameraRelativeControl>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15850_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3115_InvokableCall_1__ctor_m15850_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15850_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3116_0_0_0;
static ParameterInfo InvokableCall_1_t3115_InvokableCall_1__ctor_m15851_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3116_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15851_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<CameraRelativeControl>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15851_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3115_InvokableCall_1__ctor_m15851_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15851_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3115_InvokableCall_1_Invoke_m15852_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15852_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<CameraRelativeControl>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15852_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3115_InvokableCall_1_Invoke_m15852_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15852_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3115_InvokableCall_1_Find_m15853_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15853_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<CameraRelativeControl>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15853_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3115_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3115_InvokableCall_1_Find_m15853_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15853_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3115_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15850_MethodInfo,
	&InvokableCall_1__ctor_m15851_MethodInfo,
	&InvokableCall_1_Invoke_m15852_MethodInfo,
	&InvokableCall_1_Find_m15853_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3115_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15852_MethodInfo,
	&InvokableCall_1_Find_m15853_MethodInfo,
};
extern TypeInfo UnityAction_1_t3116_il2cpp_TypeInfo;
extern TypeInfo CameraRelativeControl_t210_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3115_RGCTXData[5] = 
{
	&UnityAction_1_t3116_0_0_0/* Type Usage */,
	&UnityAction_1_t3116_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCameraRelativeControl_t210_m33898_MethodInfo/* Method Usage */,
	&CameraRelativeControl_t210_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15855_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3115_0_0_0;
extern Il2CppType InvokableCall_1_t3115_1_0_0;
struct InvokableCall_1_t3115;
extern Il2CppGenericClass InvokableCall_1_t3115_GenericClass;
TypeInfo InvokableCall_1_t3115_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3115_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3115_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3115_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3115_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3115_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3115_0_0_0/* byval_arg */
	, &InvokableCall_1_t3115_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3115_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3115_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3115)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<CameraRelativeControl>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<CameraRelativeControl>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<CameraRelativeControl>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<CameraRelativeControl>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<CameraRelativeControl>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3116_UnityAction_1__ctor_m15854_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15854_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<CameraRelativeControl>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15854_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3116_UnityAction_1__ctor_m15854_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15854_GenericMethod/* genericMethod */

};
extern Il2CppType CameraRelativeControl_t210_0_0_0;
static ParameterInfo UnityAction_1_t3116_UnityAction_1_Invoke_m15855_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &CameraRelativeControl_t210_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15855_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<CameraRelativeControl>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15855_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3116_UnityAction_1_Invoke_m15855_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15855_GenericMethod/* genericMethod */

};
extern Il2CppType CameraRelativeControl_t210_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3116_UnityAction_1_BeginInvoke_m15856_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &CameraRelativeControl_t210_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15856_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<CameraRelativeControl>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15856_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3116_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3116_UnityAction_1_BeginInvoke_m15856_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15856_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3116_UnityAction_1_EndInvoke_m15857_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15857_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<CameraRelativeControl>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15857_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3116_UnityAction_1_EndInvoke_m15857_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15857_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3116_MethodInfos[] =
{
	&UnityAction_1__ctor_m15854_MethodInfo,
	&UnityAction_1_Invoke_m15855_MethodInfo,
	&UnityAction_1_BeginInvoke_m15856_MethodInfo,
	&UnityAction_1_EndInvoke_m15857_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15856_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15857_MethodInfo;
static MethodInfo* UnityAction_1_t3116_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15855_MethodInfo,
	&UnityAction_1_BeginInvoke_m15856_MethodInfo,
	&UnityAction_1_EndInvoke_m15857_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3116_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3116_1_0_0;
struct UnityAction_1_t3116;
extern Il2CppGenericClass UnityAction_1_t3116_GenericClass;
TypeInfo UnityAction_1_t3116_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3116_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3116_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3116_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3116_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3116_0_0_0/* byval_arg */
	, &UnityAction_1_t3116_1_0_0/* this_arg */
	, UnityAction_1_t3116_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3116_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3116)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t3300_il2cpp_TypeInfo;

// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Transform>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Transform>
extern MethodInfo IEnumerator_1_get_Current_m44346_MethodInfo;
static PropertyInfo IEnumerator_1_t3300____Current_PropertyInfo = 
{
	&IEnumerator_1_t3300_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44346_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t3300_PropertyInfos[] =
{
	&IEnumerator_1_t3300____Current_PropertyInfo,
	NULL
};
extern Il2CppType Transform_t74_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44346_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Transform>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44346_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t3300_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t74_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44346_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t3300_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44346_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t3300_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t3300_0_0_0;
extern Il2CppType IEnumerator_1_t3300_1_0_0;
struct IEnumerator_1_t3300;
extern Il2CppGenericClass IEnumerator_1_t3300_GenericClass;
TypeInfo IEnumerator_1_t3300_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t3300_MethodInfos/* methods */
	, IEnumerator_1_t3300_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t3300_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t3300_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t3300_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t3300_0_0_0/* byval_arg */
	, &IEnumerator_1_t3300_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t3300_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Transform>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_141.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3117_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Transform>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_141MethodDeclarations.h"

extern TypeInfo Transform_t74_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15862_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTransform_t74_m33900_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Transform>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Transform>(System.Int32)
#define Array_InternalArray__get_Item_TisTransform_t74_m33900(__this, p0, method) (Transform_t74 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Transform>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Transform>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Transform>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Transform>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Transform>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Transform>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3117____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3117_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3117, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3117____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3117_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3117, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3117_FieldInfos[] =
{
	&InternalEnumerator_1_t3117____array_0_FieldInfo,
	&InternalEnumerator_1_t3117____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15859_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3117____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3117_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15859_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3117____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3117_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15862_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3117_PropertyInfos[] =
{
	&InternalEnumerator_1_t3117____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3117____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3117_InternalEnumerator_1__ctor_m15858_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15858_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Transform>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15858_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3117_InternalEnumerator_1__ctor_m15858_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15858_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15859_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Transform>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15859_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3117_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15859_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15860_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Transform>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15860_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15860_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15861_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Transform>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15861_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3117_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15861_GenericMethod/* genericMethod */

};
extern Il2CppType Transform_t74_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15862_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Transform>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15862_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3117_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t74_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15862_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3117_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15858_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15859_MethodInfo,
	&InternalEnumerator_1_Dispose_m15860_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15861_MethodInfo,
	&InternalEnumerator_1_get_Current_m15862_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15861_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15860_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3117_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15859_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15861_MethodInfo,
	&InternalEnumerator_1_Dispose_m15860_MethodInfo,
	&InternalEnumerator_1_get_Current_m15862_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3117_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t3300_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3117_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t3300_il2cpp_TypeInfo, 7},
};
extern TypeInfo Transform_t74_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3117_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15862_MethodInfo/* Method Usage */,
	&Transform_t74_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTransform_t74_m33900_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3117_0_0_0;
extern Il2CppType InternalEnumerator_1_t3117_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3117_GenericClass;
TypeInfo InternalEnumerator_1_t3117_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3117_MethodInfos/* methods */
	, InternalEnumerator_1_t3117_PropertyInfos/* properties */
	, InternalEnumerator_1_t3117_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3117_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3117_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3117_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3117_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3117_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3117_1_0_0/* this_arg */
	, InternalEnumerator_1_t3117_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3117_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3117_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3117)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t473_il2cpp_TypeInfo;

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Transform>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Transform>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Transform>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Transform>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Transform>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Transform>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Transform>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Transform>
extern MethodInfo ICollection_1_get_Count_m44347_MethodInfo;
static PropertyInfo ICollection_1_t473____Count_PropertyInfo = 
{
	&ICollection_1_t473_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44347_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44348_MethodInfo;
static PropertyInfo ICollection_1_t473____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t473_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44348_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t473_PropertyInfos[] =
{
	&ICollection_1_t473____Count_PropertyInfo,
	&ICollection_1_t473____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44347_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Transform>::get_Count()
MethodInfo ICollection_1_get_Count_m44347_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t473_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44347_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44348_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Transform>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44348_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t473_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44348_GenericMethod/* genericMethod */

};
extern Il2CppType Transform_t74_0_0_0;
extern Il2CppType Transform_t74_0_0_0;
static ParameterInfo ICollection_1_t473_ICollection_1_Add_m2124_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Transform_t74_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m2124_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Transform>::Add(T)
MethodInfo ICollection_1_Add_m2124_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t473_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t473_ICollection_1_Add_m2124_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m2124_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m2123_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Transform>::Clear()
MethodInfo ICollection_1_Clear_m2123_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t473_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m2123_GenericMethod/* genericMethod */

};
extern Il2CppType Transform_t74_0_0_0;
static ParameterInfo ICollection_1_t473_ICollection_1_Contains_m44349_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Transform_t74_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44349_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Transform>::Contains(T)
MethodInfo ICollection_1_Contains_m44349_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t473_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t473_ICollection_1_Contains_m44349_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44349_GenericMethod/* genericMethod */

};
extern Il2CppType TransformU5BU5D_t3298_0_0_0;
extern Il2CppType TransformU5BU5D_t3298_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t473_ICollection_1_CopyTo_m44350_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TransformU5BU5D_t3298_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44350_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Transform>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44350_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t473_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t473_ICollection_1_CopyTo_m44350_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44350_GenericMethod/* genericMethod */

};
extern Il2CppType Transform_t74_0_0_0;
static ParameterInfo ICollection_1_t473_ICollection_1_Remove_m44351_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Transform_t74_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44351_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Transform>::Remove(T)
MethodInfo ICollection_1_Remove_m44351_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t473_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t473_ICollection_1_Remove_m44351_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44351_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t473_MethodInfos[] =
{
	&ICollection_1_get_Count_m44347_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44348_MethodInfo,
	&ICollection_1_Add_m2124_MethodInfo,
	&ICollection_1_Clear_m2123_MethodInfo,
	&ICollection_1_Contains_m44349_MethodInfo,
	&ICollection_1_CopyTo_m44350_MethodInfo,
	&ICollection_1_Remove_m44351_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t3299_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t473_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t3299_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t473_0_0_0;
extern Il2CppType ICollection_1_t473_1_0_0;
struct ICollection_1_t473;
extern Il2CppGenericClass ICollection_1_t473_GenericClass;
TypeInfo ICollection_1_t473_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t473_MethodInfos/* methods */
	, ICollection_1_t473_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t473_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t473_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t473_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t473_0_0_0/* byval_arg */
	, &ICollection_1_t473_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t473_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Transform>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Transform>
extern Il2CppType IEnumerator_1_t3300_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44352_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Transform>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44352_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t3299_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t3300_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44352_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t3299_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44352_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t3299_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t3299_0_0_0;
extern Il2CppType IEnumerable_1_t3299_1_0_0;
struct IEnumerable_1_t3299;
extern Il2CppGenericClass IEnumerable_1_t3299_GenericClass;
TypeInfo IEnumerable_1_t3299_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t3299_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t3299_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t3299_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t3299_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t3299_0_0_0/* byval_arg */
	, &IEnumerable_1_t3299_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t3299_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t290_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Transform>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Transform>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Transform>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Transform>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Transform>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Transform>
extern MethodInfo IList_1_get_Item_m44353_MethodInfo;
extern MethodInfo IList_1_set_Item_m44354_MethodInfo;
static PropertyInfo IList_1_t290____Item_PropertyInfo = 
{
	&IList_1_t290_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44353_MethodInfo/* get */
	, &IList_1_set_Item_m44354_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t290_PropertyInfos[] =
{
	&IList_1_t290____Item_PropertyInfo,
	NULL
};
extern Il2CppType Transform_t74_0_0_0;
static ParameterInfo IList_1_t290_IList_1_IndexOf_m44355_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Transform_t74_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44355_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Transform>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44355_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t290_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t290_IList_1_IndexOf_m44355_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44355_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Transform_t74_0_0_0;
static ParameterInfo IList_1_t290_IList_1_Insert_m44356_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Transform_t74_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44356_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Transform>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44356_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t290_IList_1_Insert_m44356_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44356_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t290_IList_1_RemoveAt_m44357_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44357_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Transform>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44357_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t290_IList_1_RemoveAt_m44357_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44357_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t290_IList_1_get_Item_m44353_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Transform_t74_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44353_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Transform>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44353_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t290_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t74_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t290_IList_1_get_Item_m44353_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44353_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Transform_t74_0_0_0;
static ParameterInfo IList_1_t290_IList_1_set_Item_m44354_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Transform_t74_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44354_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Transform>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44354_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t290_IList_1_set_Item_m44354_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44354_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t290_MethodInfos[] =
{
	&IList_1_IndexOf_m44355_MethodInfo,
	&IList_1_Insert_m44356_MethodInfo,
	&IList_1_RemoveAt_m44357_MethodInfo,
	&IList_1_get_Item_m44353_MethodInfo,
	&IList_1_set_Item_m44354_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t290_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t473_il2cpp_TypeInfo,
	&IEnumerable_1_t3299_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t290_0_0_0;
extern Il2CppType IList_1_t290_1_0_0;
struct IList_1_t290;
extern Il2CppGenericClass IList_1_t290_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t290_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t290_MethodInfos/* methods */
	, IList_1_t290_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t290_il2cpp_TypeInfo/* element_class */
	, IList_1_t290_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t290_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t290_0_0_0/* byval_arg */
	, &IList_1_t290_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t290_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7922_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>
extern MethodInfo ICollection_1_get_Count_m44358_MethodInfo;
static PropertyInfo ICollection_1_t7922____Count_PropertyInfo = 
{
	&ICollection_1_t7922_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44358_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44359_MethodInfo;
static PropertyInfo ICollection_1_t7922____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7922_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44359_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7922_PropertyInfos[] =
{
	&ICollection_1_t7922____Count_PropertyInfo,
	&ICollection_1_t7922____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44358_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::get_Count()
MethodInfo ICollection_1_get_Count_m44358_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7922_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44358_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44359_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44359_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7922_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44359_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_t1179_0_0_0;
extern Il2CppType IEnumerable_t1179_0_0_0;
static ParameterInfo ICollection_1_t7922_ICollection_1_Add_m44360_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_t1179_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44360_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Add(T)
MethodInfo ICollection_1_Add_m44360_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7922_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7922_ICollection_1_Add_m44360_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44360_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44361_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Clear()
MethodInfo ICollection_1_Clear_m44361_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7922_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44361_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_t1179_0_0_0;
static ParameterInfo ICollection_1_t7922_ICollection_1_Contains_m44362_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_t1179_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44362_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Contains(T)
MethodInfo ICollection_1_Contains_m44362_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7922_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7922_ICollection_1_Contains_m44362_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44362_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerableU5BU5D_t5429_0_0_0;
extern Il2CppType IEnumerableU5BU5D_t5429_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7922_ICollection_1_CopyTo_m44363_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerableU5BU5D_t5429_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44363_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44363_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7922_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7922_ICollection_1_CopyTo_m44363_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44363_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_t1179_0_0_0;
static ParameterInfo ICollection_1_t7922_ICollection_1_Remove_m44364_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_t1179_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44364_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Remove(T)
MethodInfo ICollection_1_Remove_m44364_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7922_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7922_ICollection_1_Remove_m44364_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44364_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7922_MethodInfos[] =
{
	&ICollection_1_get_Count_m44358_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44359_MethodInfo,
	&ICollection_1_Add_m44360_MethodInfo,
	&ICollection_1_Clear_m44361_MethodInfo,
	&ICollection_1_Contains_m44362_MethodInfo,
	&ICollection_1_CopyTo_m44363_MethodInfo,
	&ICollection_1_Remove_m44364_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7924_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7922_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7924_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7922_0_0_0;
extern Il2CppType ICollection_1_t7922_1_0_0;
struct ICollection_1_t7922;
extern Il2CppGenericClass ICollection_1_t7922_GenericClass;
TypeInfo ICollection_1_t7922_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7922_MethodInfos/* methods */
	, ICollection_1_t7922_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7922_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7922_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7922_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7922_0_0_0/* byval_arg */
	, &ICollection_1_t7922_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7922_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>
extern Il2CppType IEnumerator_1_t6198_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44365_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44365_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7924_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6198_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44365_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7924_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44365_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7924_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7924_0_0_0;
extern Il2CppType IEnumerable_1_t7924_1_0_0;
struct IEnumerable_1_t7924;
extern Il2CppGenericClass IEnumerable_1_t7924_GenericClass;
TypeInfo IEnumerable_1_t7924_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7924_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7924_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7924_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7924_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7924_0_0_0/* byval_arg */
	, &IEnumerable_1_t7924_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7924_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6198_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Collections.IEnumerable>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.IEnumerable>
extern MethodInfo IEnumerator_1_get_Current_m44366_MethodInfo;
static PropertyInfo IEnumerator_1_t6198____Current_PropertyInfo = 
{
	&IEnumerator_1_t6198_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44366_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6198_PropertyInfos[] =
{
	&IEnumerator_1_t6198____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEnumerable_t1179_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44366_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Collections.IEnumerable>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44366_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6198_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_t1179_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44366_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6198_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44366_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6198_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6198_0_0_0;
extern Il2CppType IEnumerator_1_t6198_1_0_0;
struct IEnumerator_1_t6198;
extern Il2CppGenericClass IEnumerator_1_t6198_GenericClass;
TypeInfo IEnumerator_1_t6198_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6198_MethodInfos/* methods */
	, IEnumerator_1_t6198_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6198_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6198_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6198_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6198_0_0_0/* byval_arg */
	, &IEnumerator_1_t6198_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6198_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Collections.IEnumerable>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_142.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3118_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Collections.IEnumerable>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_142MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m15867_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEnumerable_t1179_m33911_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Collections.IEnumerable>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Collections.IEnumerable>(System.Int32)
#define Array_InternalArray__get_Item_TisIEnumerable_t1179_m33911(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.IEnumerable>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3118____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3118_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3118, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3118____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3118_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3118, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3118_FieldInfos[] =
{
	&InternalEnumerator_1_t3118____array_0_FieldInfo,
	&InternalEnumerator_1_t3118____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15864_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3118____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3118_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15864_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3118____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3118_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15867_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3118_PropertyInfos[] =
{
	&InternalEnumerator_1_t3118____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3118____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3118_InternalEnumerator_1__ctor_m15863_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15863_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15863_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3118_InternalEnumerator_1__ctor_m15863_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15863_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15864_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15864_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3118_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15864_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15865_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15865_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15865_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15866_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15866_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3118_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15866_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_t1179_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15867_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15867_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3118_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_t1179_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15867_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3118_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15863_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15864_MethodInfo,
	&InternalEnumerator_1_Dispose_m15865_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15866_MethodInfo,
	&InternalEnumerator_1_get_Current_m15867_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15866_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15865_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3118_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15864_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15866_MethodInfo,
	&InternalEnumerator_1_Dispose_m15865_MethodInfo,
	&InternalEnumerator_1_get_Current_m15867_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3118_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6198_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3118_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6198_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEnumerable_t1179_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3118_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15867_MethodInfo/* Method Usage */,
	&IEnumerable_t1179_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEnumerable_t1179_m33911_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3118_0_0_0;
extern Il2CppType InternalEnumerator_1_t3118_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3118_GenericClass;
TypeInfo InternalEnumerator_1_t3118_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3118_MethodInfos/* methods */
	, InternalEnumerator_1_t3118_PropertyInfos/* properties */
	, InternalEnumerator_1_t3118_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3118_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3118_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3118_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3118_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3118_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3118_1_0_0/* this_arg */
	, InternalEnumerator_1_t3118_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3118_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3118_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3118)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7923_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Collections.IEnumerable>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Collections.IEnumerable>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Collections.IEnumerable>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Collections.IEnumerable>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Collections.IEnumerable>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Collections.IEnumerable>
extern MethodInfo IList_1_get_Item_m44367_MethodInfo;
extern MethodInfo IList_1_set_Item_m44368_MethodInfo;
static PropertyInfo IList_1_t7923____Item_PropertyInfo = 
{
	&IList_1_t7923_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44367_MethodInfo/* get */
	, &IList_1_set_Item_m44368_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7923_PropertyInfos[] =
{
	&IList_1_t7923____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEnumerable_t1179_0_0_0;
static ParameterInfo IList_1_t7923_IList_1_IndexOf_m44369_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_t1179_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44369_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Collections.IEnumerable>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44369_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7923_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7923_IList_1_IndexOf_m44369_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44369_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEnumerable_t1179_0_0_0;
static ParameterInfo IList_1_t7923_IList_1_Insert_m44370_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEnumerable_t1179_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44370_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Collections.IEnumerable>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44370_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7923_IList_1_Insert_m44370_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44370_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7923_IList_1_RemoveAt_m44371_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44371_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Collections.IEnumerable>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44371_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7923_IList_1_RemoveAt_m44371_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44371_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7923_IList_1_get_Item_m44367_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType IEnumerable_t1179_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44367_GenericMethod;
// T System.Collections.Generic.IList`1<System.Collections.IEnumerable>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44367_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7923_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_t1179_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7923_IList_1_get_Item_m44367_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44367_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType IEnumerable_t1179_0_0_0;
static ParameterInfo IList_1_t7923_IList_1_set_Item_m44368_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEnumerable_t1179_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44368_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Collections.IEnumerable>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44368_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7923_IList_1_set_Item_m44368_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44368_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7923_MethodInfos[] =
{
	&IList_1_IndexOf_m44369_MethodInfo,
	&IList_1_Insert_m44370_MethodInfo,
	&IList_1_RemoveAt_m44371_MethodInfo,
	&IList_1_get_Item_m44367_MethodInfo,
	&IList_1_set_Item_m44368_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7923_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7922_il2cpp_TypeInfo,
	&IEnumerable_1_t7924_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7923_0_0_0;
extern Il2CppType IList_1_t7923_1_0_0;
struct IList_1_t7923;
extern Il2CppGenericClass IList_1_t7923_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7923_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7923_MethodInfos/* methods */
	, IList_1_t7923_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7923_il2cpp_TypeInfo/* element_class */
	, IList_1_t7923_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7923_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7923_0_0_0/* byval_arg */
	, &IList_1_t7923_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7923_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6200_il2cpp_TypeInfo;

// UnityEngine.CharacterController
#include "UnityEngine_UnityEngine_CharacterController.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.CharacterController>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.CharacterController>
extern MethodInfo IEnumerator_1_get_Current_m44372_MethodInfo;
static PropertyInfo IEnumerator_1_t6200____Current_PropertyInfo = 
{
	&IEnumerator_1_t6200_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44372_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6200_PropertyInfos[] =
{
	&IEnumerator_1_t6200____Current_PropertyInfo,
	NULL
};
extern Il2CppType CharacterController_t209_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44372_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.CharacterController>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44372_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6200_il2cpp_TypeInfo/* declaring_type */
	, &CharacterController_t209_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44372_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6200_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44372_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6200_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6200_0_0_0;
extern Il2CppType IEnumerator_1_t6200_1_0_0;
struct IEnumerator_1_t6200;
extern Il2CppGenericClass IEnumerator_1_t6200_GenericClass;
TypeInfo IEnumerator_1_t6200_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6200_MethodInfos/* methods */
	, IEnumerator_1_t6200_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6200_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6200_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6200_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6200_0_0_0/* byval_arg */
	, &IEnumerator_1_t6200_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6200_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.CharacterController>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_143.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3119_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.CharacterController>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_143MethodDeclarations.h"

extern TypeInfo CharacterController_t209_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15872_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCharacterController_t209_m33922_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.CharacterController>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.CharacterController>(System.Int32)
#define Array_InternalArray__get_Item_TisCharacterController_t209_m33922(__this, p0, method) (CharacterController_t209 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterController>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.CharacterController>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterController>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.CharacterController>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.CharacterController>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.CharacterController>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3119____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3119_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3119, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3119____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3119_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3119, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3119_FieldInfos[] =
{
	&InternalEnumerator_1_t3119____array_0_FieldInfo,
	&InternalEnumerator_1_t3119____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15869_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3119____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3119_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15869_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3119____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3119_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15872_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3119_PropertyInfos[] =
{
	&InternalEnumerator_1_t3119____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3119____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3119_InternalEnumerator_1__ctor_m15868_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15868_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterController>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15868_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3119_InternalEnumerator_1__ctor_m15868_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15868_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15869_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.CharacterController>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15869_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3119_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15869_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15870_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterController>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15870_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15870_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15871_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.CharacterController>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15871_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3119_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15871_GenericMethod/* genericMethod */

};
extern Il2CppType CharacterController_t209_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15872_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.CharacterController>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15872_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3119_il2cpp_TypeInfo/* declaring_type */
	, &CharacterController_t209_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15872_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3119_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15868_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15869_MethodInfo,
	&InternalEnumerator_1_Dispose_m15870_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15871_MethodInfo,
	&InternalEnumerator_1_get_Current_m15872_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15871_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15870_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3119_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15869_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15871_MethodInfo,
	&InternalEnumerator_1_Dispose_m15870_MethodInfo,
	&InternalEnumerator_1_get_Current_m15872_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3119_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6200_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3119_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6200_il2cpp_TypeInfo, 7},
};
extern TypeInfo CharacterController_t209_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3119_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15872_MethodInfo/* Method Usage */,
	&CharacterController_t209_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCharacterController_t209_m33922_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3119_0_0_0;
extern Il2CppType InternalEnumerator_1_t3119_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3119_GenericClass;
TypeInfo InternalEnumerator_1_t3119_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3119_MethodInfos/* methods */
	, InternalEnumerator_1_t3119_PropertyInfos/* properties */
	, InternalEnumerator_1_t3119_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3119_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3119_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3119_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3119_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3119_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3119_1_0_0/* this_arg */
	, InternalEnumerator_1_t3119_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3119_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3119_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3119)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7925_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>
extern MethodInfo ICollection_1_get_Count_m44373_MethodInfo;
static PropertyInfo ICollection_1_t7925____Count_PropertyInfo = 
{
	&ICollection_1_t7925_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44373_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44374_MethodInfo;
static PropertyInfo ICollection_1_t7925____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7925_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44374_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7925_PropertyInfos[] =
{
	&ICollection_1_t7925____Count_PropertyInfo,
	&ICollection_1_t7925____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44373_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>::get_Count()
MethodInfo ICollection_1_get_Count_m44373_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7925_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44373_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44374_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44374_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7925_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44374_GenericMethod/* genericMethod */

};
extern Il2CppType CharacterController_t209_0_0_0;
extern Il2CppType CharacterController_t209_0_0_0;
static ParameterInfo ICollection_1_t7925_ICollection_1_Add_m44375_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CharacterController_t209_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44375_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>::Add(T)
MethodInfo ICollection_1_Add_m44375_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7925_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7925_ICollection_1_Add_m44375_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44375_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44376_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>::Clear()
MethodInfo ICollection_1_Clear_m44376_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7925_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44376_GenericMethod/* genericMethod */

};
extern Il2CppType CharacterController_t209_0_0_0;
static ParameterInfo ICollection_1_t7925_ICollection_1_Contains_m44377_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CharacterController_t209_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44377_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>::Contains(T)
MethodInfo ICollection_1_Contains_m44377_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7925_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7925_ICollection_1_Contains_m44377_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44377_GenericMethod/* genericMethod */

};
extern Il2CppType CharacterControllerU5BU5D_t5680_0_0_0;
extern Il2CppType CharacterControllerU5BU5D_t5680_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7925_ICollection_1_CopyTo_m44378_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CharacterControllerU5BU5D_t5680_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44378_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44378_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7925_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7925_ICollection_1_CopyTo_m44378_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44378_GenericMethod/* genericMethod */

};
extern Il2CppType CharacterController_t209_0_0_0;
static ParameterInfo ICollection_1_t7925_ICollection_1_Remove_m44379_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CharacterController_t209_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44379_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.CharacterController>::Remove(T)
MethodInfo ICollection_1_Remove_m44379_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7925_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7925_ICollection_1_Remove_m44379_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44379_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7925_MethodInfos[] =
{
	&ICollection_1_get_Count_m44373_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44374_MethodInfo,
	&ICollection_1_Add_m44375_MethodInfo,
	&ICollection_1_Clear_m44376_MethodInfo,
	&ICollection_1_Contains_m44377_MethodInfo,
	&ICollection_1_CopyTo_m44378_MethodInfo,
	&ICollection_1_Remove_m44379_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7927_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7925_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7927_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7925_0_0_0;
extern Il2CppType ICollection_1_t7925_1_0_0;
struct ICollection_1_t7925;
extern Il2CppGenericClass ICollection_1_t7925_GenericClass;
TypeInfo ICollection_1_t7925_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7925_MethodInfos/* methods */
	, ICollection_1_t7925_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7925_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7925_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7925_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7925_0_0_0/* byval_arg */
	, &ICollection_1_t7925_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7925_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.CharacterController>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.CharacterController>
extern Il2CppType IEnumerator_1_t6200_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44380_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.CharacterController>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44380_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7927_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6200_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44380_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7927_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44380_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7927_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7927_0_0_0;
extern Il2CppType IEnumerable_1_t7927_1_0_0;
struct IEnumerable_1_t7927;
extern Il2CppGenericClass IEnumerable_1_t7927_GenericClass;
TypeInfo IEnumerable_1_t7927_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7927_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7927_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7927_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7927_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7927_0_0_0/* byval_arg */
	, &IEnumerable_1_t7927_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7927_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7926_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.CharacterController>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.CharacterController>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.CharacterController>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.CharacterController>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.CharacterController>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.CharacterController>
extern MethodInfo IList_1_get_Item_m44381_MethodInfo;
extern MethodInfo IList_1_set_Item_m44382_MethodInfo;
static PropertyInfo IList_1_t7926____Item_PropertyInfo = 
{
	&IList_1_t7926_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44381_MethodInfo/* get */
	, &IList_1_set_Item_m44382_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7926_PropertyInfos[] =
{
	&IList_1_t7926____Item_PropertyInfo,
	NULL
};
extern Il2CppType CharacterController_t209_0_0_0;
static ParameterInfo IList_1_t7926_IList_1_IndexOf_m44383_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CharacterController_t209_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44383_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.CharacterController>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44383_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7926_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7926_IList_1_IndexOf_m44383_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44383_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CharacterController_t209_0_0_0;
static ParameterInfo IList_1_t7926_IList_1_Insert_m44384_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CharacterController_t209_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44384_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.CharacterController>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44384_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7926_IList_1_Insert_m44384_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44384_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7926_IList_1_RemoveAt_m44385_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44385_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.CharacterController>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44385_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7926_IList_1_RemoveAt_m44385_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44385_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7926_IList_1_get_Item_m44381_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType CharacterController_t209_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44381_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.CharacterController>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44381_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7926_il2cpp_TypeInfo/* declaring_type */
	, &CharacterController_t209_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7926_IList_1_get_Item_m44381_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44381_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType CharacterController_t209_0_0_0;
static ParameterInfo IList_1_t7926_IList_1_set_Item_m44382_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CharacterController_t209_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44382_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.CharacterController>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44382_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7926_IList_1_set_Item_m44382_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44382_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7926_MethodInfos[] =
{
	&IList_1_IndexOf_m44383_MethodInfo,
	&IList_1_Insert_m44384_MethodInfo,
	&IList_1_RemoveAt_m44385_MethodInfo,
	&IList_1_get_Item_m44381_MethodInfo,
	&IList_1_set_Item_m44382_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7926_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7925_il2cpp_TypeInfo,
	&IEnumerable_1_t7927_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7926_0_0_0;
extern Il2CppType IList_1_t7926_1_0_0;
struct IList_1_t7926;
extern Il2CppGenericClass IList_1_t7926_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7926_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7926_MethodInfos/* methods */
	, IList_1_t7926_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7926_il2cpp_TypeInfo/* element_class */
	, IList_1_t7926_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7926_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7926_0_0_0/* byval_arg */
	, &IList_1_t7926_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7926_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6202_il2cpp_TypeInfo;

// FirstPersonControl
#include "AssemblyU2DUnityScript_FirstPersonControl.h"


// T System.Collections.Generic.IEnumerator`1<FirstPersonControl>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<FirstPersonControl>
extern MethodInfo IEnumerator_1_get_Current_m44386_MethodInfo;
static PropertyInfo IEnumerator_1_t6202____Current_PropertyInfo = 
{
	&IEnumerator_1_t6202_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44386_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6202_PropertyInfos[] =
{
	&IEnumerator_1_t6202____Current_PropertyInfo,
	NULL
};
extern Il2CppType FirstPersonControl_t211_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44386_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<FirstPersonControl>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44386_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6202_il2cpp_TypeInfo/* declaring_type */
	, &FirstPersonControl_t211_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44386_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6202_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44386_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6202_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6202_0_0_0;
extern Il2CppType IEnumerator_1_t6202_1_0_0;
struct IEnumerator_1_t6202;
extern Il2CppGenericClass IEnumerator_1_t6202_GenericClass;
TypeInfo IEnumerator_1_t6202_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6202_MethodInfos/* methods */
	, IEnumerator_1_t6202_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6202_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6202_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6202_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6202_0_0_0/* byval_arg */
	, &IEnumerator_1_t6202_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6202_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<FirstPersonControl>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_144.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3120_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<FirstPersonControl>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_144MethodDeclarations.h"

extern TypeInfo FirstPersonControl_t211_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15877_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFirstPersonControl_t211_m33933_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<FirstPersonControl>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<FirstPersonControl>(System.Int32)
#define Array_InternalArray__get_Item_TisFirstPersonControl_t211_m33933(__this, p0, method) (FirstPersonControl_t211 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<FirstPersonControl>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<FirstPersonControl>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<FirstPersonControl>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<FirstPersonControl>::MoveNext()
// T System.Array/InternalEnumerator`1<FirstPersonControl>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<FirstPersonControl>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3120____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3120_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3120, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3120____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3120_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3120, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3120_FieldInfos[] =
{
	&InternalEnumerator_1_t3120____array_0_FieldInfo,
	&InternalEnumerator_1_t3120____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15874_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3120____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3120_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15874_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3120____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3120_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15877_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3120_PropertyInfos[] =
{
	&InternalEnumerator_1_t3120____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3120____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3120_InternalEnumerator_1__ctor_m15873_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15873_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<FirstPersonControl>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15873_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3120_InternalEnumerator_1__ctor_m15873_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15873_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15874_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<FirstPersonControl>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15874_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3120_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15874_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15875_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<FirstPersonControl>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15875_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15875_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15876_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<FirstPersonControl>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15876_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3120_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15876_GenericMethod/* genericMethod */

};
extern Il2CppType FirstPersonControl_t211_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15877_GenericMethod;
// T System.Array/InternalEnumerator`1<FirstPersonControl>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15877_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3120_il2cpp_TypeInfo/* declaring_type */
	, &FirstPersonControl_t211_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15877_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3120_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15873_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15874_MethodInfo,
	&InternalEnumerator_1_Dispose_m15875_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15876_MethodInfo,
	&InternalEnumerator_1_get_Current_m15877_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15876_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15875_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3120_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15874_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15876_MethodInfo,
	&InternalEnumerator_1_Dispose_m15875_MethodInfo,
	&InternalEnumerator_1_get_Current_m15877_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3120_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6202_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3120_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6202_il2cpp_TypeInfo, 7},
};
extern TypeInfo FirstPersonControl_t211_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3120_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15877_MethodInfo/* Method Usage */,
	&FirstPersonControl_t211_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisFirstPersonControl_t211_m33933_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3120_0_0_0;
extern Il2CppType InternalEnumerator_1_t3120_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3120_GenericClass;
TypeInfo InternalEnumerator_1_t3120_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3120_MethodInfos/* methods */
	, InternalEnumerator_1_t3120_PropertyInfos/* properties */
	, InternalEnumerator_1_t3120_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3120_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3120_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3120_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3120_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3120_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3120_1_0_0/* this_arg */
	, InternalEnumerator_1_t3120_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3120_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3120_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3120)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7928_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<FirstPersonControl>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<FirstPersonControl>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<FirstPersonControl>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<FirstPersonControl>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<FirstPersonControl>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<FirstPersonControl>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<FirstPersonControl>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<FirstPersonControl>
extern MethodInfo ICollection_1_get_Count_m44387_MethodInfo;
static PropertyInfo ICollection_1_t7928____Count_PropertyInfo = 
{
	&ICollection_1_t7928_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44387_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44388_MethodInfo;
static PropertyInfo ICollection_1_t7928____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7928_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44388_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7928_PropertyInfos[] =
{
	&ICollection_1_t7928____Count_PropertyInfo,
	&ICollection_1_t7928____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44387_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<FirstPersonControl>::get_Count()
MethodInfo ICollection_1_get_Count_m44387_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7928_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44387_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44388_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<FirstPersonControl>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44388_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7928_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44388_GenericMethod/* genericMethod */

};
extern Il2CppType FirstPersonControl_t211_0_0_0;
extern Il2CppType FirstPersonControl_t211_0_0_0;
static ParameterInfo ICollection_1_t7928_ICollection_1_Add_m44389_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FirstPersonControl_t211_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44389_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<FirstPersonControl>::Add(T)
MethodInfo ICollection_1_Add_m44389_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7928_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7928_ICollection_1_Add_m44389_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44389_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44390_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<FirstPersonControl>::Clear()
MethodInfo ICollection_1_Clear_m44390_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7928_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44390_GenericMethod/* genericMethod */

};
extern Il2CppType FirstPersonControl_t211_0_0_0;
static ParameterInfo ICollection_1_t7928_ICollection_1_Contains_m44391_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FirstPersonControl_t211_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44391_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<FirstPersonControl>::Contains(T)
MethodInfo ICollection_1_Contains_m44391_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7928_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7928_ICollection_1_Contains_m44391_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44391_GenericMethod/* genericMethod */

};
extern Il2CppType FirstPersonControlU5BU5D_t5773_0_0_0;
extern Il2CppType FirstPersonControlU5BU5D_t5773_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7928_ICollection_1_CopyTo_m44392_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FirstPersonControlU5BU5D_t5773_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44392_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<FirstPersonControl>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44392_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7928_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7928_ICollection_1_CopyTo_m44392_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44392_GenericMethod/* genericMethod */

};
extern Il2CppType FirstPersonControl_t211_0_0_0;
static ParameterInfo ICollection_1_t7928_ICollection_1_Remove_m44393_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FirstPersonControl_t211_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44393_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<FirstPersonControl>::Remove(T)
MethodInfo ICollection_1_Remove_m44393_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7928_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7928_ICollection_1_Remove_m44393_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44393_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7928_MethodInfos[] =
{
	&ICollection_1_get_Count_m44387_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44388_MethodInfo,
	&ICollection_1_Add_m44389_MethodInfo,
	&ICollection_1_Clear_m44390_MethodInfo,
	&ICollection_1_Contains_m44391_MethodInfo,
	&ICollection_1_CopyTo_m44392_MethodInfo,
	&ICollection_1_Remove_m44393_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7930_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7928_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7930_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7928_0_0_0;
extern Il2CppType ICollection_1_t7928_1_0_0;
struct ICollection_1_t7928;
extern Il2CppGenericClass ICollection_1_t7928_GenericClass;
TypeInfo ICollection_1_t7928_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7928_MethodInfos/* methods */
	, ICollection_1_t7928_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7928_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7928_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7928_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7928_0_0_0/* byval_arg */
	, &ICollection_1_t7928_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7928_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<FirstPersonControl>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<FirstPersonControl>
extern Il2CppType IEnumerator_1_t6202_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44394_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<FirstPersonControl>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44394_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7930_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6202_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44394_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7930_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44394_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7930_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7930_0_0_0;
extern Il2CppType IEnumerable_1_t7930_1_0_0;
struct IEnumerable_1_t7930;
extern Il2CppGenericClass IEnumerable_1_t7930_GenericClass;
TypeInfo IEnumerable_1_t7930_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7930_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7930_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7930_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7930_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7930_0_0_0/* byval_arg */
	, &IEnumerable_1_t7930_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7930_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7929_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<FirstPersonControl>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<FirstPersonControl>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<FirstPersonControl>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<FirstPersonControl>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<FirstPersonControl>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<FirstPersonControl>
extern MethodInfo IList_1_get_Item_m44395_MethodInfo;
extern MethodInfo IList_1_set_Item_m44396_MethodInfo;
static PropertyInfo IList_1_t7929____Item_PropertyInfo = 
{
	&IList_1_t7929_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44395_MethodInfo/* get */
	, &IList_1_set_Item_m44396_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7929_PropertyInfos[] =
{
	&IList_1_t7929____Item_PropertyInfo,
	NULL
};
extern Il2CppType FirstPersonControl_t211_0_0_0;
static ParameterInfo IList_1_t7929_IList_1_IndexOf_m44397_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FirstPersonControl_t211_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44397_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<FirstPersonControl>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44397_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7929_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7929_IList_1_IndexOf_m44397_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44397_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FirstPersonControl_t211_0_0_0;
static ParameterInfo IList_1_t7929_IList_1_Insert_m44398_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FirstPersonControl_t211_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44398_GenericMethod;
// System.Void System.Collections.Generic.IList`1<FirstPersonControl>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44398_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7929_IList_1_Insert_m44398_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44398_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7929_IList_1_RemoveAt_m44399_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44399_GenericMethod;
// System.Void System.Collections.Generic.IList`1<FirstPersonControl>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44399_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7929_IList_1_RemoveAt_m44399_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44399_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7929_IList_1_get_Item_m44395_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType FirstPersonControl_t211_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44395_GenericMethod;
// T System.Collections.Generic.IList`1<FirstPersonControl>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44395_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7929_il2cpp_TypeInfo/* declaring_type */
	, &FirstPersonControl_t211_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7929_IList_1_get_Item_m44395_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44395_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FirstPersonControl_t211_0_0_0;
static ParameterInfo IList_1_t7929_IList_1_set_Item_m44396_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FirstPersonControl_t211_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44396_GenericMethod;
// System.Void System.Collections.Generic.IList`1<FirstPersonControl>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44396_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7929_IList_1_set_Item_m44396_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44396_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7929_MethodInfos[] =
{
	&IList_1_IndexOf_m44397_MethodInfo,
	&IList_1_Insert_m44398_MethodInfo,
	&IList_1_RemoveAt_m44399_MethodInfo,
	&IList_1_get_Item_m44395_MethodInfo,
	&IList_1_set_Item_m44396_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7929_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7928_il2cpp_TypeInfo,
	&IEnumerable_1_t7930_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7929_0_0_0;
extern Il2CppType IList_1_t7929_1_0_0;
struct IList_1_t7929;
extern Il2CppGenericClass IList_1_t7929_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7929_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7929_MethodInfos/* methods */
	, IList_1_t7929_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7929_il2cpp_TypeInfo/* element_class */
	, IList_1_t7929_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7929_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7929_0_0_0/* byval_arg */
	, &IList_1_t7929_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7929_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<FirstPersonControl>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_59.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3121_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<FirstPersonControl>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_59MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<FirstPersonControl>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_55.h"
extern TypeInfo InvokableCall_1_t3122_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<FirstPersonControl>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_55MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15880_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15882_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<FirstPersonControl>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<FirstPersonControl>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<FirstPersonControl>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3121____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3121_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3121, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3121_FieldInfos[] =
{
	&CachedInvokableCall_1_t3121____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType FirstPersonControl_t211_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3121_CachedInvokableCall_1__ctor_m15878_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &FirstPersonControl_t211_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15878_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<FirstPersonControl>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15878_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3121_CachedInvokableCall_1__ctor_m15878_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15878_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3121_CachedInvokableCall_1_Invoke_m15879_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15879_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<FirstPersonControl>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15879_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3121_CachedInvokableCall_1_Invoke_m15879_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15879_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3121_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15878_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15879_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15879_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15883_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3121_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15879_MethodInfo,
	&InvokableCall_1_Find_m15883_MethodInfo,
};
extern Il2CppType UnityAction_1_t3123_0_0_0;
extern TypeInfo UnityAction_1_t3123_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisFirstPersonControl_t211_m33943_MethodInfo;
extern TypeInfo FirstPersonControl_t211_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15885_MethodInfo;
extern TypeInfo FirstPersonControl_t211_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3121_RGCTXData[8] = 
{
	&UnityAction_1_t3123_0_0_0/* Type Usage */,
	&UnityAction_1_t3123_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisFirstPersonControl_t211_m33943_MethodInfo/* Method Usage */,
	&FirstPersonControl_t211_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15885_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15880_MethodInfo/* Method Usage */,
	&FirstPersonControl_t211_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15882_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3121_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3121_1_0_0;
struct CachedInvokableCall_1_t3121;
extern Il2CppGenericClass CachedInvokableCall_1_t3121_GenericClass;
TypeInfo CachedInvokableCall_1_t3121_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3121_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3121_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3122_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3121_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3121_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3121_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3121_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3121_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3121_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3121_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3121)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<FirstPersonControl>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_62.h"
extern TypeInfo UnityAction_1_t3123_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<FirstPersonControl>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_62MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<FirstPersonControl>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<FirstPersonControl>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisFirstPersonControl_t211_m33943(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<FirstPersonControl>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<FirstPersonControl>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<FirstPersonControl>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<FirstPersonControl>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<FirstPersonControl>
extern Il2CppType UnityAction_1_t3123_0_0_1;
FieldInfo InvokableCall_1_t3122____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3123_0_0_1/* type */
	, &InvokableCall_1_t3122_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3122, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3122_FieldInfos[] =
{
	&InvokableCall_1_t3122____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3122_InvokableCall_1__ctor_m15880_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15880_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<FirstPersonControl>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15880_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3122_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3122_InvokableCall_1__ctor_m15880_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15880_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3123_0_0_0;
static ParameterInfo InvokableCall_1_t3122_InvokableCall_1__ctor_m15881_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15881_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<FirstPersonControl>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15881_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3122_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3122_InvokableCall_1__ctor_m15881_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15881_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3122_InvokableCall_1_Invoke_m15882_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15882_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<FirstPersonControl>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15882_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3122_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3122_InvokableCall_1_Invoke_m15882_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15882_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3122_InvokableCall_1_Find_m15883_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15883_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<FirstPersonControl>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15883_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3122_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3122_InvokableCall_1_Find_m15883_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15883_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3122_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15880_MethodInfo,
	&InvokableCall_1__ctor_m15881_MethodInfo,
	&InvokableCall_1_Invoke_m15882_MethodInfo,
	&InvokableCall_1_Find_m15883_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3122_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15882_MethodInfo,
	&InvokableCall_1_Find_m15883_MethodInfo,
};
extern TypeInfo UnityAction_1_t3123_il2cpp_TypeInfo;
extern TypeInfo FirstPersonControl_t211_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3122_RGCTXData[5] = 
{
	&UnityAction_1_t3123_0_0_0/* Type Usage */,
	&UnityAction_1_t3123_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisFirstPersonControl_t211_m33943_MethodInfo/* Method Usage */,
	&FirstPersonControl_t211_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15885_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3122_0_0_0;
extern Il2CppType InvokableCall_1_t3122_1_0_0;
struct InvokableCall_1_t3122;
extern Il2CppGenericClass InvokableCall_1_t3122_GenericClass;
TypeInfo InvokableCall_1_t3122_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3122_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3122_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3122_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3122_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3122_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3122_0_0_0/* byval_arg */
	, &InvokableCall_1_t3122_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3122_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3122_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3122)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<FirstPersonControl>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<FirstPersonControl>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<FirstPersonControl>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<FirstPersonControl>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<FirstPersonControl>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3123_UnityAction_1__ctor_m15884_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15884_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<FirstPersonControl>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15884_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3123_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3123_UnityAction_1__ctor_m15884_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15884_GenericMethod/* genericMethod */

};
extern Il2CppType FirstPersonControl_t211_0_0_0;
static ParameterInfo UnityAction_1_t3123_UnityAction_1_Invoke_m15885_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &FirstPersonControl_t211_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15885_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<FirstPersonControl>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15885_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3123_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3123_UnityAction_1_Invoke_m15885_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15885_GenericMethod/* genericMethod */

};
extern Il2CppType FirstPersonControl_t211_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3123_UnityAction_1_BeginInvoke_m15886_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &FirstPersonControl_t211_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15886_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<FirstPersonControl>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15886_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3123_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3123_UnityAction_1_BeginInvoke_m15886_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15886_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3123_UnityAction_1_EndInvoke_m15887_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15887_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<FirstPersonControl>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15887_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3123_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3123_UnityAction_1_EndInvoke_m15887_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15887_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3123_MethodInfos[] =
{
	&UnityAction_1__ctor_m15884_MethodInfo,
	&UnityAction_1_Invoke_m15885_MethodInfo,
	&UnityAction_1_BeginInvoke_m15886_MethodInfo,
	&UnityAction_1_EndInvoke_m15887_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15886_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15887_MethodInfo;
static MethodInfo* UnityAction_1_t3123_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15885_MethodInfo,
	&UnityAction_1_BeginInvoke_m15886_MethodInfo,
	&UnityAction_1_EndInvoke_m15887_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3123_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3123_1_0_0;
struct UnityAction_1_t3123;
extern Il2CppGenericClass UnityAction_1_t3123_GenericClass;
TypeInfo UnityAction_1_t3123_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3123_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3123_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3123_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3123_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3123_0_0_0/* byval_arg */
	, &UnityAction_1_t3123_1_0_0/* this_arg */
	, UnityAction_1_t3123_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3123_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3123)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6204_il2cpp_TypeInfo;

// FollowTransform
#include "AssemblyU2DUnityScript_FollowTransform.h"


// T System.Collections.Generic.IEnumerator`1<FollowTransform>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<FollowTransform>
extern MethodInfo IEnumerator_1_get_Current_m44400_MethodInfo;
static PropertyInfo IEnumerator_1_t6204____Current_PropertyInfo = 
{
	&IEnumerator_1_t6204_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44400_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6204_PropertyInfos[] =
{
	&IEnumerator_1_t6204____Current_PropertyInfo,
	NULL
};
extern Il2CppType FollowTransform_t212_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44400_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<FollowTransform>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44400_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6204_il2cpp_TypeInfo/* declaring_type */
	, &FollowTransform_t212_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44400_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6204_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44400_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6204_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6204_0_0_0;
extern Il2CppType IEnumerator_1_t6204_1_0_0;
struct IEnumerator_1_t6204;
extern Il2CppGenericClass IEnumerator_1_t6204_GenericClass;
TypeInfo IEnumerator_1_t6204_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6204_MethodInfos/* methods */
	, IEnumerator_1_t6204_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6204_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6204_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6204_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6204_0_0_0/* byval_arg */
	, &IEnumerator_1_t6204_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6204_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<FollowTransform>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_145.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3124_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<FollowTransform>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_145MethodDeclarations.h"

extern TypeInfo FollowTransform_t212_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15892_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFollowTransform_t212_m33945_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<FollowTransform>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<FollowTransform>(System.Int32)
#define Array_InternalArray__get_Item_TisFollowTransform_t212_m33945(__this, p0, method) (FollowTransform_t212 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<FollowTransform>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<FollowTransform>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<FollowTransform>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<FollowTransform>::MoveNext()
// T System.Array/InternalEnumerator`1<FollowTransform>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<FollowTransform>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3124____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3124_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3124, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3124____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3124_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3124, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3124_FieldInfos[] =
{
	&InternalEnumerator_1_t3124____array_0_FieldInfo,
	&InternalEnumerator_1_t3124____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15889_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3124____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3124_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15889_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3124____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3124_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15892_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3124_PropertyInfos[] =
{
	&InternalEnumerator_1_t3124____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3124____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3124_InternalEnumerator_1__ctor_m15888_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15888_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<FollowTransform>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15888_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3124_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3124_InternalEnumerator_1__ctor_m15888_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15888_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15889_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<FollowTransform>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15889_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3124_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15889_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15890_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<FollowTransform>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15890_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3124_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15890_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15891_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<FollowTransform>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15891_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3124_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15891_GenericMethod/* genericMethod */

};
extern Il2CppType FollowTransform_t212_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15892_GenericMethod;
// T System.Array/InternalEnumerator`1<FollowTransform>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15892_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3124_il2cpp_TypeInfo/* declaring_type */
	, &FollowTransform_t212_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15892_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3124_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15888_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15889_MethodInfo,
	&InternalEnumerator_1_Dispose_m15890_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15891_MethodInfo,
	&InternalEnumerator_1_get_Current_m15892_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15891_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15890_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3124_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15889_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15891_MethodInfo,
	&InternalEnumerator_1_Dispose_m15890_MethodInfo,
	&InternalEnumerator_1_get_Current_m15892_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3124_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6204_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3124_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6204_il2cpp_TypeInfo, 7},
};
extern TypeInfo FollowTransform_t212_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3124_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15892_MethodInfo/* Method Usage */,
	&FollowTransform_t212_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisFollowTransform_t212_m33945_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3124_0_0_0;
extern Il2CppType InternalEnumerator_1_t3124_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3124_GenericClass;
TypeInfo InternalEnumerator_1_t3124_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3124_MethodInfos/* methods */
	, InternalEnumerator_1_t3124_PropertyInfos/* properties */
	, InternalEnumerator_1_t3124_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3124_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3124_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3124_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3124_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3124_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3124_1_0_0/* this_arg */
	, InternalEnumerator_1_t3124_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3124_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3124_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3124)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7931_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<FollowTransform>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<FollowTransform>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<FollowTransform>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<FollowTransform>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<FollowTransform>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<FollowTransform>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<FollowTransform>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<FollowTransform>
extern MethodInfo ICollection_1_get_Count_m44401_MethodInfo;
static PropertyInfo ICollection_1_t7931____Count_PropertyInfo = 
{
	&ICollection_1_t7931_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44401_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44402_MethodInfo;
static PropertyInfo ICollection_1_t7931____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7931_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44402_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7931_PropertyInfos[] =
{
	&ICollection_1_t7931____Count_PropertyInfo,
	&ICollection_1_t7931____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44401_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<FollowTransform>::get_Count()
MethodInfo ICollection_1_get_Count_m44401_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7931_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44401_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44402_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<FollowTransform>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44402_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7931_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44402_GenericMethod/* genericMethod */

};
extern Il2CppType FollowTransform_t212_0_0_0;
extern Il2CppType FollowTransform_t212_0_0_0;
static ParameterInfo ICollection_1_t7931_ICollection_1_Add_m44403_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FollowTransform_t212_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44403_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<FollowTransform>::Add(T)
MethodInfo ICollection_1_Add_m44403_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7931_ICollection_1_Add_m44403_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44403_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44404_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<FollowTransform>::Clear()
MethodInfo ICollection_1_Clear_m44404_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44404_GenericMethod/* genericMethod */

};
extern Il2CppType FollowTransform_t212_0_0_0;
static ParameterInfo ICollection_1_t7931_ICollection_1_Contains_m44405_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FollowTransform_t212_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44405_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<FollowTransform>::Contains(T)
MethodInfo ICollection_1_Contains_m44405_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7931_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7931_ICollection_1_Contains_m44405_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44405_GenericMethod/* genericMethod */

};
extern Il2CppType FollowTransformU5BU5D_t5774_0_0_0;
extern Il2CppType FollowTransformU5BU5D_t5774_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7931_ICollection_1_CopyTo_m44406_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FollowTransformU5BU5D_t5774_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44406_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<FollowTransform>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44406_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7931_ICollection_1_CopyTo_m44406_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44406_GenericMethod/* genericMethod */

};
extern Il2CppType FollowTransform_t212_0_0_0;
static ParameterInfo ICollection_1_t7931_ICollection_1_Remove_m44407_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FollowTransform_t212_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44407_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<FollowTransform>::Remove(T)
MethodInfo ICollection_1_Remove_m44407_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7931_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7931_ICollection_1_Remove_m44407_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44407_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7931_MethodInfos[] =
{
	&ICollection_1_get_Count_m44401_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44402_MethodInfo,
	&ICollection_1_Add_m44403_MethodInfo,
	&ICollection_1_Clear_m44404_MethodInfo,
	&ICollection_1_Contains_m44405_MethodInfo,
	&ICollection_1_CopyTo_m44406_MethodInfo,
	&ICollection_1_Remove_m44407_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7933_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7931_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7933_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7931_0_0_0;
extern Il2CppType ICollection_1_t7931_1_0_0;
struct ICollection_1_t7931;
extern Il2CppGenericClass ICollection_1_t7931_GenericClass;
TypeInfo ICollection_1_t7931_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7931_MethodInfos/* methods */
	, ICollection_1_t7931_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7931_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7931_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7931_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7931_0_0_0/* byval_arg */
	, &ICollection_1_t7931_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7931_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<FollowTransform>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<FollowTransform>
extern Il2CppType IEnumerator_1_t6204_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44408_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<FollowTransform>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44408_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7933_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6204_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44408_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7933_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44408_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7933_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7933_0_0_0;
extern Il2CppType IEnumerable_1_t7933_1_0_0;
struct IEnumerable_1_t7933;
extern Il2CppGenericClass IEnumerable_1_t7933_GenericClass;
TypeInfo IEnumerable_1_t7933_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7933_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7933_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7933_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7933_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7933_0_0_0/* byval_arg */
	, &IEnumerable_1_t7933_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7933_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7932_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<FollowTransform>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<FollowTransform>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<FollowTransform>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<FollowTransform>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<FollowTransform>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<FollowTransform>
extern MethodInfo IList_1_get_Item_m44409_MethodInfo;
extern MethodInfo IList_1_set_Item_m44410_MethodInfo;
static PropertyInfo IList_1_t7932____Item_PropertyInfo = 
{
	&IList_1_t7932_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44409_MethodInfo/* get */
	, &IList_1_set_Item_m44410_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7932_PropertyInfos[] =
{
	&IList_1_t7932____Item_PropertyInfo,
	NULL
};
extern Il2CppType FollowTransform_t212_0_0_0;
static ParameterInfo IList_1_t7932_IList_1_IndexOf_m44411_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FollowTransform_t212_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44411_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<FollowTransform>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44411_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7932_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7932_IList_1_IndexOf_m44411_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44411_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FollowTransform_t212_0_0_0;
static ParameterInfo IList_1_t7932_IList_1_Insert_m44412_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FollowTransform_t212_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44412_GenericMethod;
// System.Void System.Collections.Generic.IList`1<FollowTransform>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44412_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7932_IList_1_Insert_m44412_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44412_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7932_IList_1_RemoveAt_m44413_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44413_GenericMethod;
// System.Void System.Collections.Generic.IList`1<FollowTransform>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44413_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7932_IList_1_RemoveAt_m44413_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44413_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7932_IList_1_get_Item_m44409_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType FollowTransform_t212_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44409_GenericMethod;
// T System.Collections.Generic.IList`1<FollowTransform>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44409_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7932_il2cpp_TypeInfo/* declaring_type */
	, &FollowTransform_t212_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7932_IList_1_get_Item_m44409_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44409_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType FollowTransform_t212_0_0_0;
static ParameterInfo IList_1_t7932_IList_1_set_Item_m44410_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FollowTransform_t212_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44410_GenericMethod;
// System.Void System.Collections.Generic.IList`1<FollowTransform>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44410_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7932_IList_1_set_Item_m44410_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44410_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7932_MethodInfos[] =
{
	&IList_1_IndexOf_m44411_MethodInfo,
	&IList_1_Insert_m44412_MethodInfo,
	&IList_1_RemoveAt_m44413_MethodInfo,
	&IList_1_get_Item_m44409_MethodInfo,
	&IList_1_set_Item_m44410_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7932_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7931_il2cpp_TypeInfo,
	&IEnumerable_1_t7933_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7932_0_0_0;
extern Il2CppType IList_1_t7932_1_0_0;
struct IList_1_t7932;
extern Il2CppGenericClass IList_1_t7932_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7932_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7932_MethodInfos/* methods */
	, IList_1_t7932_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7932_il2cpp_TypeInfo/* element_class */
	, IList_1_t7932_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7932_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7932_0_0_0/* byval_arg */
	, &IList_1_t7932_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7932_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<FollowTransform>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_60.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3125_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<FollowTransform>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_60MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<FollowTransform>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_56.h"
extern TypeInfo InvokableCall_1_t3126_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<FollowTransform>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_56MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15895_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15897_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<FollowTransform>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<FollowTransform>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<FollowTransform>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3125____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3125_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3125, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3125_FieldInfos[] =
{
	&CachedInvokableCall_1_t3125____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType FollowTransform_t212_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3125_CachedInvokableCall_1__ctor_m15893_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &FollowTransform_t212_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15893_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<FollowTransform>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15893_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3125_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3125_CachedInvokableCall_1__ctor_m15893_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15893_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3125_CachedInvokableCall_1_Invoke_m15894_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15894_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<FollowTransform>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15894_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3125_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3125_CachedInvokableCall_1_Invoke_m15894_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15894_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3125_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15893_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15894_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15894_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15898_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3125_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15894_MethodInfo,
	&InvokableCall_1_Find_m15898_MethodInfo,
};
extern Il2CppType UnityAction_1_t3127_0_0_0;
extern TypeInfo UnityAction_1_t3127_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisFollowTransform_t212_m33955_MethodInfo;
extern TypeInfo FollowTransform_t212_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15900_MethodInfo;
extern TypeInfo FollowTransform_t212_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3125_RGCTXData[8] = 
{
	&UnityAction_1_t3127_0_0_0/* Type Usage */,
	&UnityAction_1_t3127_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisFollowTransform_t212_m33955_MethodInfo/* Method Usage */,
	&FollowTransform_t212_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15900_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15895_MethodInfo/* Method Usage */,
	&FollowTransform_t212_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15897_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3125_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3125_1_0_0;
struct CachedInvokableCall_1_t3125;
extern Il2CppGenericClass CachedInvokableCall_1_t3125_GenericClass;
TypeInfo CachedInvokableCall_1_t3125_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3125_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3125_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3126_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3125_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3125_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3125_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3125_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3125_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3125_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3125_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3125)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<FollowTransform>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_63.h"
extern TypeInfo UnityAction_1_t3127_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<FollowTransform>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_63MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<FollowTransform>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<FollowTransform>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisFollowTransform_t212_m33955(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<FollowTransform>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<FollowTransform>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<FollowTransform>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<FollowTransform>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<FollowTransform>
extern Il2CppType UnityAction_1_t3127_0_0_1;
FieldInfo InvokableCall_1_t3126____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3127_0_0_1/* type */
	, &InvokableCall_1_t3126_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3126, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3126_FieldInfos[] =
{
	&InvokableCall_1_t3126____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3126_InvokableCall_1__ctor_m15895_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15895_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<FollowTransform>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15895_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3126_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3126_InvokableCall_1__ctor_m15895_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15895_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3127_0_0_0;
static ParameterInfo InvokableCall_1_t3126_InvokableCall_1__ctor_m15896_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3127_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15896_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<FollowTransform>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15896_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3126_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3126_InvokableCall_1__ctor_m15896_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15896_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3126_InvokableCall_1_Invoke_m15897_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15897_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<FollowTransform>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15897_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3126_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3126_InvokableCall_1_Invoke_m15897_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15897_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3126_InvokableCall_1_Find_m15898_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15898_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<FollowTransform>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15898_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3126_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3126_InvokableCall_1_Find_m15898_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15898_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3126_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15895_MethodInfo,
	&InvokableCall_1__ctor_m15896_MethodInfo,
	&InvokableCall_1_Invoke_m15897_MethodInfo,
	&InvokableCall_1_Find_m15898_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3126_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15897_MethodInfo,
	&InvokableCall_1_Find_m15898_MethodInfo,
};
extern TypeInfo UnityAction_1_t3127_il2cpp_TypeInfo;
extern TypeInfo FollowTransform_t212_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3126_RGCTXData[5] = 
{
	&UnityAction_1_t3127_0_0_0/* Type Usage */,
	&UnityAction_1_t3127_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisFollowTransform_t212_m33955_MethodInfo/* Method Usage */,
	&FollowTransform_t212_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15900_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3126_0_0_0;
extern Il2CppType InvokableCall_1_t3126_1_0_0;
struct InvokableCall_1_t3126;
extern Il2CppGenericClass InvokableCall_1_t3126_GenericClass;
TypeInfo InvokableCall_1_t3126_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3126_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3126_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3126_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3126_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3126_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3126_0_0_0/* byval_arg */
	, &InvokableCall_1_t3126_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3126_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3126_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3126)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<FollowTransform>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<FollowTransform>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<FollowTransform>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<FollowTransform>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<FollowTransform>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3127_UnityAction_1__ctor_m15899_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15899_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<FollowTransform>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15899_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3127_UnityAction_1__ctor_m15899_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15899_GenericMethod/* genericMethod */

};
extern Il2CppType FollowTransform_t212_0_0_0;
static ParameterInfo UnityAction_1_t3127_UnityAction_1_Invoke_m15900_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &FollowTransform_t212_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15900_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<FollowTransform>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15900_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3127_UnityAction_1_Invoke_m15900_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15900_GenericMethod/* genericMethod */

};
extern Il2CppType FollowTransform_t212_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3127_UnityAction_1_BeginInvoke_m15901_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &FollowTransform_t212_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15901_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<FollowTransform>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15901_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3127_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3127_UnityAction_1_BeginInvoke_m15901_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15901_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3127_UnityAction_1_EndInvoke_m15902_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15902_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<FollowTransform>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15902_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3127_UnityAction_1_EndInvoke_m15902_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15902_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3127_MethodInfos[] =
{
	&UnityAction_1__ctor_m15899_MethodInfo,
	&UnityAction_1_Invoke_m15900_MethodInfo,
	&UnityAction_1_BeginInvoke_m15901_MethodInfo,
	&UnityAction_1_EndInvoke_m15902_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15901_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15902_MethodInfo;
static MethodInfo* UnityAction_1_t3127_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15900_MethodInfo,
	&UnityAction_1_BeginInvoke_m15901_MethodInfo,
	&UnityAction_1_EndInvoke_m15902_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3127_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3127_1_0_0;
struct UnityAction_1_t3127;
extern Il2CppGenericClass UnityAction_1_t3127_GenericClass;
TypeInfo UnityAction_1_t3127_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3127_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3127_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3127_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3127_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3127_0_0_0/* byval_arg */
	, &UnityAction_1_t3127_1_0_0/* this_arg */
	, UnityAction_1_t3127_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3127_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3127)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6206_il2cpp_TypeInfo;

// Joystick
#include "AssemblyU2DUnityScript_Joystick.h"


// T System.Collections.Generic.IEnumerator`1<Joystick>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Joystick>
extern MethodInfo IEnumerator_1_get_Current_m44414_MethodInfo;
static PropertyInfo IEnumerator_1_t6206____Current_PropertyInfo = 
{
	&IEnumerator_1_t6206_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44414_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6206_PropertyInfos[] =
{
	&IEnumerator_1_t6206____Current_PropertyInfo,
	NULL
};
extern Il2CppType Joystick_t208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44414_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Joystick>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44414_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6206_il2cpp_TypeInfo/* declaring_type */
	, &Joystick_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44414_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6206_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44414_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6206_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6206_0_0_0;
extern Il2CppType IEnumerator_1_t6206_1_0_0;
struct IEnumerator_1_t6206;
extern Il2CppGenericClass IEnumerator_1_t6206_GenericClass;
TypeInfo IEnumerator_1_t6206_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6206_MethodInfos/* methods */
	, IEnumerator_1_t6206_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6206_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6206_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6206_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6206_0_0_0/* byval_arg */
	, &IEnumerator_1_t6206_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6206_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Joystick>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_146.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3128_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Joystick>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_146MethodDeclarations.h"

extern TypeInfo Joystick_t208_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15907_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisJoystick_t208_m33957_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Joystick>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Joystick>(System.Int32)
#define Array_InternalArray__get_Item_TisJoystick_t208_m33957(__this, p0, method) (Joystick_t208 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Joystick>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Joystick>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Joystick>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Joystick>::MoveNext()
// T System.Array/InternalEnumerator`1<Joystick>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Joystick>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3128____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3128_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3128, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3128____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3128_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3128, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3128_FieldInfos[] =
{
	&InternalEnumerator_1_t3128____array_0_FieldInfo,
	&InternalEnumerator_1_t3128____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15904_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3128____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3128_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15904_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3128____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3128_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15907_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3128_PropertyInfos[] =
{
	&InternalEnumerator_1_t3128____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3128____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3128_InternalEnumerator_1__ctor_m15903_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15903_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Joystick>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15903_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3128_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3128_InternalEnumerator_1__ctor_m15903_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15903_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15904_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Joystick>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15904_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3128_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15904_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15905_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Joystick>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15905_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3128_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15905_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15906_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Joystick>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15906_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3128_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15906_GenericMethod/* genericMethod */

};
extern Il2CppType Joystick_t208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15907_GenericMethod;
// T System.Array/InternalEnumerator`1<Joystick>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15907_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3128_il2cpp_TypeInfo/* declaring_type */
	, &Joystick_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15907_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3128_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15903_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15904_MethodInfo,
	&InternalEnumerator_1_Dispose_m15905_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15906_MethodInfo,
	&InternalEnumerator_1_get_Current_m15907_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15906_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15905_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3128_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15904_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15906_MethodInfo,
	&InternalEnumerator_1_Dispose_m15905_MethodInfo,
	&InternalEnumerator_1_get_Current_m15907_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3128_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6206_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3128_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6206_il2cpp_TypeInfo, 7},
};
extern TypeInfo Joystick_t208_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3128_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15907_MethodInfo/* Method Usage */,
	&Joystick_t208_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisJoystick_t208_m33957_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3128_0_0_0;
extern Il2CppType InternalEnumerator_1_t3128_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3128_GenericClass;
TypeInfo InternalEnumerator_1_t3128_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3128_MethodInfos/* methods */
	, InternalEnumerator_1_t3128_PropertyInfos/* properties */
	, InternalEnumerator_1_t3128_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3128_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3128_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3128_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3128_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3128_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3128_1_0_0/* this_arg */
	, InternalEnumerator_1_t3128_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3128_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3128_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3128)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7934_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Joystick>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Joystick>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Joystick>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Joystick>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Joystick>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Joystick>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Joystick>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Joystick>
extern MethodInfo ICollection_1_get_Count_m44415_MethodInfo;
static PropertyInfo ICollection_1_t7934____Count_PropertyInfo = 
{
	&ICollection_1_t7934_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44415_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44416_MethodInfo;
static PropertyInfo ICollection_1_t7934____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7934_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44416_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7934_PropertyInfos[] =
{
	&ICollection_1_t7934____Count_PropertyInfo,
	&ICollection_1_t7934____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44415_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Joystick>::get_Count()
MethodInfo ICollection_1_get_Count_m44415_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7934_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44415_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44416_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Joystick>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44416_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7934_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44416_GenericMethod/* genericMethod */

};
extern Il2CppType Joystick_t208_0_0_0;
extern Il2CppType Joystick_t208_0_0_0;
static ParameterInfo ICollection_1_t7934_ICollection_1_Add_m44417_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Joystick_t208_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44417_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Joystick>::Add(T)
MethodInfo ICollection_1_Add_m44417_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7934_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7934_ICollection_1_Add_m44417_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44417_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44418_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Joystick>::Clear()
MethodInfo ICollection_1_Clear_m44418_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7934_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44418_GenericMethod/* genericMethod */

};
extern Il2CppType Joystick_t208_0_0_0;
static ParameterInfo ICollection_1_t7934_ICollection_1_Contains_m44419_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Joystick_t208_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44419_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Joystick>::Contains(T)
MethodInfo ICollection_1_Contains_m44419_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7934_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7934_ICollection_1_Contains_m44419_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44419_GenericMethod/* genericMethod */

};
extern Il2CppType JoystickU5BU5D_t214_0_0_0;
extern Il2CppType JoystickU5BU5D_t214_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7934_ICollection_1_CopyTo_m44420_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &JoystickU5BU5D_t214_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44420_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Joystick>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44420_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7934_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7934_ICollection_1_CopyTo_m44420_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44420_GenericMethod/* genericMethod */

};
extern Il2CppType Joystick_t208_0_0_0;
static ParameterInfo ICollection_1_t7934_ICollection_1_Remove_m44421_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Joystick_t208_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44421_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Joystick>::Remove(T)
MethodInfo ICollection_1_Remove_m44421_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7934_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7934_ICollection_1_Remove_m44421_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44421_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7934_MethodInfos[] =
{
	&ICollection_1_get_Count_m44415_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44416_MethodInfo,
	&ICollection_1_Add_m44417_MethodInfo,
	&ICollection_1_Clear_m44418_MethodInfo,
	&ICollection_1_Contains_m44419_MethodInfo,
	&ICollection_1_CopyTo_m44420_MethodInfo,
	&ICollection_1_Remove_m44421_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7936_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7934_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7936_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7934_0_0_0;
extern Il2CppType ICollection_1_t7934_1_0_0;
struct ICollection_1_t7934;
extern Il2CppGenericClass ICollection_1_t7934_GenericClass;
TypeInfo ICollection_1_t7934_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7934_MethodInfos/* methods */
	, ICollection_1_t7934_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7934_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7934_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7934_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7934_0_0_0/* byval_arg */
	, &ICollection_1_t7934_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7934_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Joystick>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Joystick>
extern Il2CppType IEnumerator_1_t6206_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44422_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Joystick>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44422_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7936_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6206_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44422_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7936_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44422_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7936_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7936_0_0_0;
extern Il2CppType IEnumerable_1_t7936_1_0_0;
struct IEnumerable_1_t7936;
extern Il2CppGenericClass IEnumerable_1_t7936_GenericClass;
TypeInfo IEnumerable_1_t7936_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7936_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7936_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7936_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7936_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7936_0_0_0/* byval_arg */
	, &IEnumerable_1_t7936_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7936_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7935_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Joystick>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Joystick>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Joystick>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Joystick>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Joystick>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Joystick>
extern MethodInfo IList_1_get_Item_m44423_MethodInfo;
extern MethodInfo IList_1_set_Item_m44424_MethodInfo;
static PropertyInfo IList_1_t7935____Item_PropertyInfo = 
{
	&IList_1_t7935_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44423_MethodInfo/* get */
	, &IList_1_set_Item_m44424_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7935_PropertyInfos[] =
{
	&IList_1_t7935____Item_PropertyInfo,
	NULL
};
extern Il2CppType Joystick_t208_0_0_0;
static ParameterInfo IList_1_t7935_IList_1_IndexOf_m44425_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Joystick_t208_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44425_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Joystick>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44425_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7935_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7935_IList_1_IndexOf_m44425_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44425_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Joystick_t208_0_0_0;
static ParameterInfo IList_1_t7935_IList_1_Insert_m44426_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Joystick_t208_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44426_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Joystick>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44426_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7935_IList_1_Insert_m44426_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44426_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7935_IList_1_RemoveAt_m44427_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44427_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Joystick>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44427_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7935_IList_1_RemoveAt_m44427_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44427_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7935_IList_1_get_Item_m44423_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Joystick_t208_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44423_GenericMethod;
// T System.Collections.Generic.IList`1<Joystick>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44423_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7935_il2cpp_TypeInfo/* declaring_type */
	, &Joystick_t208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7935_IList_1_get_Item_m44423_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44423_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType Joystick_t208_0_0_0;
static ParameterInfo IList_1_t7935_IList_1_set_Item_m44424_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Joystick_t208_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44424_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Joystick>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44424_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7935_IList_1_set_Item_m44424_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44424_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7935_MethodInfos[] =
{
	&IList_1_IndexOf_m44425_MethodInfo,
	&IList_1_Insert_m44426_MethodInfo,
	&IList_1_RemoveAt_m44427_MethodInfo,
	&IList_1_get_Item_m44423_MethodInfo,
	&IList_1_set_Item_m44424_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7935_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7934_il2cpp_TypeInfo,
	&IEnumerable_1_t7936_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7935_0_0_0;
extern Il2CppType IList_1_t7935_1_0_0;
struct IList_1_t7935;
extern Il2CppGenericClass IList_1_t7935_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7935_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7935_MethodInfos/* methods */
	, IList_1_t7935_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7935_il2cpp_TypeInfo/* element_class */
	, IList_1_t7935_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7935_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7935_0_0_0/* byval_arg */
	, &IList_1_t7935_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7935_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Joystick>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_61.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3129_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Joystick>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_61MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Joystick>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_57.h"
extern TypeInfo InvokableCall_1_t3130_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Joystick>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_57MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15910_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15912_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Joystick>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Joystick>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Joystick>
extern Il2CppType ObjectU5BU5D_t130_0_0_33;
FieldInfo CachedInvokableCall_1_t3129____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t130_0_0_33/* type */
	, &CachedInvokableCall_1_t3129_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3129, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3129_FieldInfos[] =
{
	&CachedInvokableCall_1_t3129____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t120_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
extern Il2CppType Joystick_t208_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3129_CachedInvokableCall_1__ctor_m15908_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t120_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Joystick_t208_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15908_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Joystick>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15908_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14196_gshared/* method */
	, &CachedInvokableCall_1_t3129_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3129_CachedInvokableCall_1__ctor_m15908_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15908_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3129_CachedInvokableCall_1_Invoke_m15909_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15909_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Joystick>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15909_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14198_gshared/* method */
	, &CachedInvokableCall_1_t3129_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3129_CachedInvokableCall_1_Invoke_m15909_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15909_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3129_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15908_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15909_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15909_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15913_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3129_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15909_MethodInfo,
	&InvokableCall_1_Find_m15913_MethodInfo,
};
extern Il2CppType UnityAction_1_t3131_0_0_0;
extern TypeInfo UnityAction_1_t3131_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisJoystick_t208_m33967_MethodInfo;
extern TypeInfo Joystick_t208_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15915_MethodInfo;
extern TypeInfo Joystick_t208_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3129_RGCTXData[8] = 
{
	&UnityAction_1_t3131_0_0_0/* Type Usage */,
	&UnityAction_1_t3131_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisJoystick_t208_m33967_MethodInfo/* Method Usage */,
	&Joystick_t208_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15915_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15910_MethodInfo/* Method Usage */,
	&Joystick_t208_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15912_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3129_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3129_1_0_0;
struct CachedInvokableCall_1_t3129;
extern Il2CppGenericClass CachedInvokableCall_1_t3129_GenericClass;
TypeInfo CachedInvokableCall_1_t3129_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3129_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3129_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3130_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3129_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3129_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3129_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3129_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3129_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3129_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3129_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3129)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Joystick>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_64.h"
extern TypeInfo UnityAction_1_t3131_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Joystick>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_64MethodDeclarations.h"
struct BaseInvokableCall_t1127;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Joystick>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Joystick>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisJoystick_t208_m33967(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m32320_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Joystick>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Joystick>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Joystick>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Joystick>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Joystick>
extern Il2CppType UnityAction_1_t3131_0_0_1;
FieldInfo InvokableCall_1_t3130____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3131_0_0_1/* type */
	, &InvokableCall_1_t3130_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3130, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3130_FieldInfos[] =
{
	&InvokableCall_1_t3130____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3130_InvokableCall_1__ctor_m15910_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15910_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Joystick>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15910_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14199_gshared/* method */
	, &InvokableCall_1_t3130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3130_InvokableCall_1__ctor_m15910_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15910_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3131_0_0_0;
static ParameterInfo InvokableCall_1_t3130_InvokableCall_1__ctor_m15911_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3131_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15911_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Joystick>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15911_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14200_gshared/* method */
	, &InvokableCall_1_t3130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3130_InvokableCall_1__ctor_m15911_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15911_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t130_0_0_0;
static ParameterInfo InvokableCall_1_t3130_InvokableCall_1_Invoke_m15912_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t130_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15912_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Joystick>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15912_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14201_gshared/* method */
	, &InvokableCall_1_t3130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InvokableCall_1_t3130_InvokableCall_1_Invoke_m15912_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15912_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t141_0_0_0;
static ParameterInfo InvokableCall_1_t3130_InvokableCall_1_Find_m15913_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t141_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15913_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Joystick>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15913_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14202_gshared/* method */
	, &InvokableCall_1_t3130_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3130_InvokableCall_1_Find_m15913_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15913_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3130_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15910_MethodInfo,
	&InvokableCall_1__ctor_m15911_MethodInfo,
	&InvokableCall_1_Invoke_m15912_MethodInfo,
	&InvokableCall_1_Find_m15913_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3130_VTable[] =
{
	&Object_Equals_m320_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&Object_GetHashCode_m321_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&InvokableCall_1_Invoke_m15912_MethodInfo,
	&InvokableCall_1_Find_m15913_MethodInfo,
};
extern TypeInfo UnityAction_1_t3131_il2cpp_TypeInfo;
extern TypeInfo Joystick_t208_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3130_RGCTXData[5] = 
{
	&UnityAction_1_t3131_0_0_0/* Type Usage */,
	&UnityAction_1_t3131_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisJoystick_t208_m33967_MethodInfo/* Method Usage */,
	&Joystick_t208_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15915_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3130_0_0_0;
extern Il2CppType InvokableCall_1_t3130_1_0_0;
struct InvokableCall_1_t3130;
extern Il2CppGenericClass InvokableCall_1_t3130_GenericClass;
TypeInfo InvokableCall_1_t3130_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3130_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3130_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1127_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3130_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3130_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3130_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3130_0_0_0/* byval_arg */
	, &InvokableCall_1_t3130_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3130_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3130_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3130)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Joystick>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Joystick>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Joystick>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Joystick>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Joystick>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3131_UnityAction_1__ctor_m15914_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15914_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Joystick>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15914_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14203_gshared/* method */
	, &UnityAction_1_t3131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3131_UnityAction_1__ctor_m15914_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15914_GenericMethod/* genericMethod */

};
extern Il2CppType Joystick_t208_0_0_0;
static ParameterInfo UnityAction_1_t3131_UnityAction_1_Invoke_m15915_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Joystick_t208_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15915_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Joystick>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15915_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14204_gshared/* method */
	, &UnityAction_1_t3131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3131_UnityAction_1_Invoke_m15915_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15915_GenericMethod/* genericMethod */

};
extern Il2CppType Joystick_t208_0_0_0;
extern Il2CppType AsyncCallback_t251_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3131_UnityAction_1_BeginInvoke_m15916_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Joystick_t208_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t251_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t250_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15916_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Joystick>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15916_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14205_gshared/* method */
	, &UnityAction_1_t3131_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3131_UnityAction_1_BeginInvoke_m15916_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15916_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t250_0_0_0;
static ParameterInfo UnityAction_1_t3131_UnityAction_1_EndInvoke_m15917_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t250_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15917_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Joystick>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15917_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14206_gshared/* method */
	, &UnityAction_1_t3131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, UnityAction_1_t3131_UnityAction_1_EndInvoke_m15917_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15917_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3131_MethodInfos[] =
{
	&UnityAction_1__ctor_m15914_MethodInfo,
	&UnityAction_1_Invoke_m15915_MethodInfo,
	&UnityAction_1_BeginInvoke_m15916_MethodInfo,
	&UnityAction_1_EndInvoke_m15917_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15916_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15917_MethodInfo;
static MethodInfo* UnityAction_1_t3131_VTable[] =
{
	&MulticastDelegate_Equals_m2417_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&MulticastDelegate_GetHashCode_m2418_MethodInfo,
	&Object_ToString_m322_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&Delegate_Clone_m2420_MethodInfo,
	&MulticastDelegate_GetObjectData_m2419_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2421_MethodInfo,
	&MulticastDelegate_CombineImpl_m2422_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2423_MethodInfo,
	&UnityAction_1_Invoke_m15915_MethodInfo,
	&UnityAction_1_BeginInvoke_m15916_MethodInfo,
	&UnityAction_1_EndInvoke_m15917_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3131_InterfacesOffsets[] = 
{
	{ &ICloneable_t525_il2cpp_TypeInfo, 4},
	{ &ISerializable_t526_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3131_1_0_0;
struct UnityAction_1_t3131;
extern Il2CppGenericClass UnityAction_1_t3131_GenericClass;
TypeInfo UnityAction_1_t3131_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3131_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t373_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3131_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3131_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3131_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3131_0_0_0/* byval_arg */
	, &UnityAction_1_t3131_1_0_0/* this_arg */
	, UnityAction_1_t3131_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3131_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3131)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6208_il2cpp_TypeInfo;

// UnityEngine.GUITexture
#include "UnityEngine_UnityEngine_GUITexture.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.GUITexture>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.GUITexture>
extern MethodInfo IEnumerator_1_get_Current_m44428_MethodInfo;
static PropertyInfo IEnumerator_1_t6208____Current_PropertyInfo = 
{
	&IEnumerator_1_t6208_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44428_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6208_PropertyInfos[] =
{
	&IEnumerator_1_t6208____Current_PropertyInfo,
	NULL
};
extern Il2CppType GUITexture_t100_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44428_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.GUITexture>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44428_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6208_il2cpp_TypeInfo/* declaring_type */
	, &GUITexture_t100_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44428_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6208_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44428_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6208_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6208_0_0_0;
extern Il2CppType IEnumerator_1_t6208_1_0_0;
struct IEnumerator_1_t6208;
extern Il2CppGenericClass IEnumerator_1_t6208_GenericClass;
TypeInfo IEnumerator_1_t6208_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6208_MethodInfos/* methods */
	, IEnumerator_1_t6208_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6208_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6208_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6208_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6208_0_0_0/* byval_arg */
	, &IEnumerator_1_t6208_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6208_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.GUITexture>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_147.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3132_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.GUITexture>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_147MethodDeclarations.h"

extern TypeInfo GUITexture_t100_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15922_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGUITexture_t100_m33969_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.GUITexture>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.GUITexture>(System.Int32)
#define Array_InternalArray__get_Item_TisGUITexture_t100_m33969(__this, p0, method) (GUITexture_t100 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUITexture>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.GUITexture>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUITexture>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.GUITexture>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.GUITexture>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.GUITexture>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3132____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3132_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3132, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3132____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3132_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3132, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3132_FieldInfos[] =
{
	&InternalEnumerator_1_t3132____array_0_FieldInfo,
	&InternalEnumerator_1_t3132____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15919_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3132____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3132_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15919_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3132____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3132_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15922_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3132_PropertyInfos[] =
{
	&InternalEnumerator_1_t3132____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3132____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3132_InternalEnumerator_1__ctor_m15918_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15918_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUITexture>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15918_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3132_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3132_InternalEnumerator_1__ctor_m15918_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15918_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15919_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.GUITexture>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15919_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3132_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15919_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15920_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUITexture>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15920_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3132_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15920_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15921_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.GUITexture>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15921_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3132_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15921_GenericMethod/* genericMethod */

};
extern Il2CppType GUITexture_t100_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15922_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.GUITexture>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15922_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3132_il2cpp_TypeInfo/* declaring_type */
	, &GUITexture_t100_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15922_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3132_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15918_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15919_MethodInfo,
	&InternalEnumerator_1_Dispose_m15920_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15921_MethodInfo,
	&InternalEnumerator_1_get_Current_m15922_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15921_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15920_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3132_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15919_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15921_MethodInfo,
	&InternalEnumerator_1_Dispose_m15920_MethodInfo,
	&InternalEnumerator_1_get_Current_m15922_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3132_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6208_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3132_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6208_il2cpp_TypeInfo, 7},
};
extern TypeInfo GUITexture_t100_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3132_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15922_MethodInfo/* Method Usage */,
	&GUITexture_t100_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisGUITexture_t100_m33969_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3132_0_0_0;
extern Il2CppType InternalEnumerator_1_t3132_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3132_GenericClass;
TypeInfo InternalEnumerator_1_t3132_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3132_MethodInfos/* methods */
	, InternalEnumerator_1_t3132_PropertyInfos/* properties */
	, InternalEnumerator_1_t3132_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3132_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3132_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3132_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3132_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3132_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3132_1_0_0/* this_arg */
	, InternalEnumerator_1_t3132_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3132_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3132_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3132)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7937_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>
extern MethodInfo ICollection_1_get_Count_m44429_MethodInfo;
static PropertyInfo ICollection_1_t7937____Count_PropertyInfo = 
{
	&ICollection_1_t7937_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44429_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44430_MethodInfo;
static PropertyInfo ICollection_1_t7937____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7937_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44430_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7937_PropertyInfos[] =
{
	&ICollection_1_t7937____Count_PropertyInfo,
	&ICollection_1_t7937____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44429_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>::get_Count()
MethodInfo ICollection_1_get_Count_m44429_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7937_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44429_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44430_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44430_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7937_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44430_GenericMethod/* genericMethod */

};
extern Il2CppType GUITexture_t100_0_0_0;
extern Il2CppType GUITexture_t100_0_0_0;
static ParameterInfo ICollection_1_t7937_ICollection_1_Add_m44431_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GUITexture_t100_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44431_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>::Add(T)
MethodInfo ICollection_1_Add_m44431_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7937_ICollection_1_Add_m44431_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44431_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44432_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>::Clear()
MethodInfo ICollection_1_Clear_m44432_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44432_GenericMethod/* genericMethod */

};
extern Il2CppType GUITexture_t100_0_0_0;
static ParameterInfo ICollection_1_t7937_ICollection_1_Contains_m44433_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GUITexture_t100_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44433_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>::Contains(T)
MethodInfo ICollection_1_Contains_m44433_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7937_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7937_ICollection_1_Contains_m44433_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44433_GenericMethod/* genericMethod */

};
extern Il2CppType GUITextureU5BU5D_t5681_0_0_0;
extern Il2CppType GUITextureU5BU5D_t5681_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7937_ICollection_1_CopyTo_m44434_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GUITextureU5BU5D_t5681_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44434_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44434_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7937_ICollection_1_CopyTo_m44434_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44434_GenericMethod/* genericMethod */

};
extern Il2CppType GUITexture_t100_0_0_0;
static ParameterInfo ICollection_1_t7937_ICollection_1_Remove_m44435_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GUITexture_t100_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44435_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUITexture>::Remove(T)
MethodInfo ICollection_1_Remove_m44435_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7937_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7937_ICollection_1_Remove_m44435_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44435_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7937_MethodInfos[] =
{
	&ICollection_1_get_Count_m44429_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44430_MethodInfo,
	&ICollection_1_Add_m44431_MethodInfo,
	&ICollection_1_Clear_m44432_MethodInfo,
	&ICollection_1_Contains_m44433_MethodInfo,
	&ICollection_1_CopyTo_m44434_MethodInfo,
	&ICollection_1_Remove_m44435_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7939_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7937_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7939_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7937_0_0_0;
extern Il2CppType ICollection_1_t7937_1_0_0;
struct ICollection_1_t7937;
extern Il2CppGenericClass ICollection_1_t7937_GenericClass;
TypeInfo ICollection_1_t7937_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7937_MethodInfos/* methods */
	, ICollection_1_t7937_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7937_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7937_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7937_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7937_0_0_0/* byval_arg */
	, &ICollection_1_t7937_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7937_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.GUITexture>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.GUITexture>
extern Il2CppType IEnumerator_1_t6208_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44436_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.GUITexture>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44436_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7939_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6208_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44436_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7939_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44436_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7939_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7939_0_0_0;
extern Il2CppType IEnumerable_1_t7939_1_0_0;
struct IEnumerable_1_t7939;
extern Il2CppGenericClass IEnumerable_1_t7939_GenericClass;
TypeInfo IEnumerable_1_t7939_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7939_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7939_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7939_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7939_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7939_0_0_0/* byval_arg */
	, &IEnumerable_1_t7939_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7939_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7938_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.GUITexture>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUITexture>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUITexture>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.GUITexture>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUITexture>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.GUITexture>
extern MethodInfo IList_1_get_Item_m44437_MethodInfo;
extern MethodInfo IList_1_set_Item_m44438_MethodInfo;
static PropertyInfo IList_1_t7938____Item_PropertyInfo = 
{
	&IList_1_t7938_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44437_MethodInfo/* get */
	, &IList_1_set_Item_m44438_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7938_PropertyInfos[] =
{
	&IList_1_t7938____Item_PropertyInfo,
	NULL
};
extern Il2CppType GUITexture_t100_0_0_0;
static ParameterInfo IList_1_t7938_IList_1_IndexOf_m44439_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GUITexture_t100_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44439_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.GUITexture>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44439_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7938_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7938_IList_1_IndexOf_m44439_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44439_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GUITexture_t100_0_0_0;
static ParameterInfo IList_1_t7938_IList_1_Insert_m44440_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GUITexture_t100_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44440_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUITexture>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44440_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7938_IList_1_Insert_m44440_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44440_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7938_IList_1_RemoveAt_m44441_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44441_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUITexture>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44441_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7938_IList_1_RemoveAt_m44441_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44441_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7938_IList_1_get_Item_m44437_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType GUITexture_t100_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44437_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.GUITexture>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44437_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7938_il2cpp_TypeInfo/* declaring_type */
	, &GUITexture_t100_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7938_IList_1_get_Item_m44437_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44437_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GUITexture_t100_0_0_0;
static ParameterInfo IList_1_t7938_IList_1_set_Item_m44438_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GUITexture_t100_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44438_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUITexture>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44438_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7938_IList_1_set_Item_m44438_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44438_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7938_MethodInfos[] =
{
	&IList_1_IndexOf_m44439_MethodInfo,
	&IList_1_Insert_m44440_MethodInfo,
	&IList_1_RemoveAt_m44441_MethodInfo,
	&IList_1_get_Item_m44437_MethodInfo,
	&IList_1_set_Item_m44438_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7938_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7937_il2cpp_TypeInfo,
	&IEnumerable_1_t7939_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7938_0_0_0;
extern Il2CppType IList_1_t7938_1_0_0;
struct IList_1_t7938;
extern Il2CppGenericClass IList_1_t7938_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7938_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7938_MethodInfos/* methods */
	, IList_1_t7938_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7938_il2cpp_TypeInfo/* element_class */
	, IList_1_t7938_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7938_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7938_0_0_0/* byval_arg */
	, &IList_1_t7938_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7938_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7940_il2cpp_TypeInfo;

// UnityEngine.GUIElement
#include "UnityEngine_UnityEngine_GUIElement.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>
extern MethodInfo ICollection_1_get_Count_m44442_MethodInfo;
static PropertyInfo ICollection_1_t7940____Count_PropertyInfo = 
{
	&ICollection_1_t7940_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44442_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44443_MethodInfo;
static PropertyInfo ICollection_1_t7940____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7940_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44443_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7940_PropertyInfos[] =
{
	&ICollection_1_t7940____Count_PropertyInfo,
	&ICollection_1_t7940____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44442_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>::get_Count()
MethodInfo ICollection_1_get_Count_m44442_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7940_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44442_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44443_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44443_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7940_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44443_GenericMethod/* genericMethod */

};
extern Il2CppType GUIElement_t990_0_0_0;
extern Il2CppType GUIElement_t990_0_0_0;
static ParameterInfo ICollection_1_t7940_ICollection_1_Add_m44444_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GUIElement_t990_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44444_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>::Add(T)
MethodInfo ICollection_1_Add_m44444_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7940_ICollection_1_Add_m44444_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44444_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44445_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>::Clear()
MethodInfo ICollection_1_Clear_m44445_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44445_GenericMethod/* genericMethod */

};
extern Il2CppType GUIElement_t990_0_0_0;
static ParameterInfo ICollection_1_t7940_ICollection_1_Contains_m44446_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GUIElement_t990_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44446_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>::Contains(T)
MethodInfo ICollection_1_Contains_m44446_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7940_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7940_ICollection_1_Contains_m44446_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44446_GenericMethod/* genericMethod */

};
extern Il2CppType GUIElementU5BU5D_t5682_0_0_0;
extern Il2CppType GUIElementU5BU5D_t5682_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7940_ICollection_1_CopyTo_m44447_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GUIElementU5BU5D_t5682_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44447_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44447_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7940_ICollection_1_CopyTo_m44447_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44447_GenericMethod/* genericMethod */

};
extern Il2CppType GUIElement_t990_0_0_0;
static ParameterInfo ICollection_1_t7940_ICollection_1_Remove_m44448_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GUIElement_t990_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44448_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GUIElement>::Remove(T)
MethodInfo ICollection_1_Remove_m44448_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7940_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7940_ICollection_1_Remove_m44448_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44448_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7940_MethodInfos[] =
{
	&ICollection_1_get_Count_m44442_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44443_MethodInfo,
	&ICollection_1_Add_m44444_MethodInfo,
	&ICollection_1_Clear_m44445_MethodInfo,
	&ICollection_1_Contains_m44446_MethodInfo,
	&ICollection_1_CopyTo_m44447_MethodInfo,
	&ICollection_1_Remove_m44448_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7942_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7940_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7942_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7940_0_0_0;
extern Il2CppType ICollection_1_t7940_1_0_0;
struct ICollection_1_t7940;
extern Il2CppGenericClass ICollection_1_t7940_GenericClass;
TypeInfo ICollection_1_t7940_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7940_MethodInfos/* methods */
	, ICollection_1_t7940_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7940_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7940_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7940_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7940_0_0_0/* byval_arg */
	, &ICollection_1_t7940_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7940_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.GUIElement>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.GUIElement>
extern Il2CppType IEnumerator_1_t6210_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44449_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.GUIElement>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44449_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7942_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6210_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44449_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7942_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44449_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7942_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7942_0_0_0;
extern Il2CppType IEnumerable_1_t7942_1_0_0;
struct IEnumerable_1_t7942;
extern Il2CppGenericClass IEnumerable_1_t7942_GenericClass;
TypeInfo IEnumerable_1_t7942_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7942_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7942_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7942_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7942_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7942_0_0_0/* byval_arg */
	, &IEnumerable_1_t7942_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7942_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6210_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.GUIElement>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.GUIElement>
extern MethodInfo IEnumerator_1_get_Current_m44450_MethodInfo;
static PropertyInfo IEnumerator_1_t6210____Current_PropertyInfo = 
{
	&IEnumerator_1_t6210_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44450_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6210_PropertyInfos[] =
{
	&IEnumerator_1_t6210____Current_PropertyInfo,
	NULL
};
extern Il2CppType GUIElement_t990_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44450_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.GUIElement>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44450_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6210_il2cpp_TypeInfo/* declaring_type */
	, &GUIElement_t990_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44450_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6210_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44450_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6210_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6210_0_0_0;
extern Il2CppType IEnumerator_1_t6210_1_0_0;
struct IEnumerator_1_t6210;
extern Il2CppGenericClass IEnumerator_1_t6210_GenericClass;
TypeInfo IEnumerator_1_t6210_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6210_MethodInfos/* methods */
	, IEnumerator_1_t6210_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6210_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6210_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6210_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6210_0_0_0/* byval_arg */
	, &IEnumerator_1_t6210_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6210_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.GUIElement>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_148.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3133_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.GUIElement>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_148MethodDeclarations.h"

extern TypeInfo GUIElement_t990_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15927_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGUIElement_t990_m33980_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.GUIElement>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.GUIElement>(System.Int32)
#define Array_InternalArray__get_Item_TisGUIElement_t990_m33980(__this, p0, method) (GUIElement_t990 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUIElement>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.GUIElement>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUIElement>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.GUIElement>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.GUIElement>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.GUIElement>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3133____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3133_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3133, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3133____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3133_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3133, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3133_FieldInfos[] =
{
	&InternalEnumerator_1_t3133____array_0_FieldInfo,
	&InternalEnumerator_1_t3133____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15924_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3133____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3133_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15924_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3133____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3133_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15927_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3133_PropertyInfos[] =
{
	&InternalEnumerator_1_t3133____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3133____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3133_InternalEnumerator_1__ctor_m15923_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15923_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUIElement>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15923_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3133_InternalEnumerator_1__ctor_m15923_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15923_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15924_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.GUIElement>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15924_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3133_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15924_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15925_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUIElement>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15925_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15925_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15926_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.GUIElement>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15926_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3133_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15926_GenericMethod/* genericMethod */

};
extern Il2CppType GUIElement_t990_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15927_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.GUIElement>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15927_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3133_il2cpp_TypeInfo/* declaring_type */
	, &GUIElement_t990_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15927_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3133_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15923_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15924_MethodInfo,
	&InternalEnumerator_1_Dispose_m15925_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15926_MethodInfo,
	&InternalEnumerator_1_get_Current_m15927_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15926_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15925_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3133_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15924_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15926_MethodInfo,
	&InternalEnumerator_1_Dispose_m15925_MethodInfo,
	&InternalEnumerator_1_get_Current_m15927_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3133_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6210_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3133_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6210_il2cpp_TypeInfo, 7},
};
extern TypeInfo GUIElement_t990_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3133_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15927_MethodInfo/* Method Usage */,
	&GUIElement_t990_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisGUIElement_t990_m33980_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3133_0_0_0;
extern Il2CppType InternalEnumerator_1_t3133_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3133_GenericClass;
TypeInfo InternalEnumerator_1_t3133_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3133_MethodInfos/* methods */
	, InternalEnumerator_1_t3133_PropertyInfos/* properties */
	, InternalEnumerator_1_t3133_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3133_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3133_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3133_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3133_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3133_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3133_1_0_0/* this_arg */
	, InternalEnumerator_1_t3133_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3133_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3133_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3133)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7941_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.GUIElement>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUIElement>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUIElement>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.GUIElement>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUIElement>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.GUIElement>
extern MethodInfo IList_1_get_Item_m44451_MethodInfo;
extern MethodInfo IList_1_set_Item_m44452_MethodInfo;
static PropertyInfo IList_1_t7941____Item_PropertyInfo = 
{
	&IList_1_t7941_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44451_MethodInfo/* get */
	, &IList_1_set_Item_m44452_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7941_PropertyInfos[] =
{
	&IList_1_t7941____Item_PropertyInfo,
	NULL
};
extern Il2CppType GUIElement_t990_0_0_0;
static ParameterInfo IList_1_t7941_IList_1_IndexOf_m44453_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GUIElement_t990_0_0_0},
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44453_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.GUIElement>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44453_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7941_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7941_IList_1_IndexOf_m44453_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44453_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GUIElement_t990_0_0_0;
static ParameterInfo IList_1_t7941_IList_1_Insert_m44454_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GUIElement_t990_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44454_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUIElement>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44454_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7941_IList_1_Insert_m44454_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44454_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7941_IList_1_RemoveAt_m44455_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44455_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUIElement>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44455_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123/* invoker_method */
	, IList_1_t7941_IList_1_RemoveAt_m44455_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44455_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo IList_1_t7941_IList_1_get_Item_m44451_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType GUIElement_t990_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44451_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.GUIElement>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44451_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7941_il2cpp_TypeInfo/* declaring_type */
	, &GUIElement_t990_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t123/* invoker_method */
	, IList_1_t7941_IList_1_get_Item_m44451_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44451_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t123_0_0_0;
extern Il2CppType GUIElement_t990_0_0_0;
static ParameterInfo IList_1_t7941_IList_1_set_Item_m44452_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GUIElement_t990_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Int32_t123_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44452_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.GUIElement>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44452_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Int32_t123_Object_t/* invoker_method */
	, IList_1_t7941_IList_1_set_Item_m44452_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44452_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7941_MethodInfos[] =
{
	&IList_1_IndexOf_m44453_MethodInfo,
	&IList_1_Insert_m44454_MethodInfo,
	&IList_1_RemoveAt_m44455_MethodInfo,
	&IList_1_get_Item_m44451_MethodInfo,
	&IList_1_set_Item_m44452_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7941_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&ICollection_1_t7940_il2cpp_TypeInfo,
	&IEnumerable_1_t7942_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7941_0_0_0;
extern Il2CppType IList_1_t7941_1_0_0;
struct IList_1_t7941;
extern Il2CppGenericClass IList_1_t7941_GenericClass;
extern CustomAttributesCache IList_1_t2539__CustomAttributeCache;
TypeInfo IList_1_t7941_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7941_MethodInfos/* methods */
	, IList_1_t7941_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7941_il2cpp_TypeInfo/* element_class */
	, IList_1_t7941_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2539__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7941_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7941_0_0_0/* byval_arg */
	, &IList_1_t7941_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7941_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6212_il2cpp_TypeInfo;

// ObliqueNear
#include "AssemblyU2DUnityScript_ObliqueNear.h"


// T System.Collections.Generic.IEnumerator`1<ObliqueNear>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<ObliqueNear>
extern MethodInfo IEnumerator_1_get_Current_m44456_MethodInfo;
static PropertyInfo IEnumerator_1_t6212____Current_PropertyInfo = 
{
	&IEnumerator_1_t6212_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44456_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6212_PropertyInfos[] =
{
	&IEnumerator_1_t6212____Current_PropertyInfo,
	NULL
};
extern Il2CppType ObliqueNear_t215_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44456_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<ObliqueNear>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44456_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6212_il2cpp_TypeInfo/* declaring_type */
	, &ObliqueNear_t215_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44456_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6212_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44456_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6212_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6212_0_0_0;
extern Il2CppType IEnumerator_1_t6212_1_0_0;
struct IEnumerator_1_t6212;
extern Il2CppGenericClass IEnumerator_1_t6212_GenericClass;
TypeInfo IEnumerator_1_t6212_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6212_MethodInfos/* methods */
	, IEnumerator_1_t6212_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6212_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6212_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6212_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6212_0_0_0/* byval_arg */
	, &IEnumerator_1_t6212_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6212_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<ObliqueNear>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_149.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3134_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<ObliqueNear>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_149MethodDeclarations.h"

extern TypeInfo ObliqueNear_t215_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15932_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisObliqueNear_t215_m33991_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<ObliqueNear>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<ObliqueNear>(System.Int32)
#define Array_InternalArray__get_Item_TisObliqueNear_t215_m33991(__this, p0, method) (ObliqueNear_t215 *)Array_InternalArray__get_Item_TisObject_t_m32233_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<ObliqueNear>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<ObliqueNear>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<ObliqueNear>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<ObliqueNear>::MoveNext()
// T System.Array/InternalEnumerator`1<ObliqueNear>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<ObliqueNear>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3134____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3134_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3134, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t123_0_0_1;
FieldInfo InternalEnumerator_1_t3134____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t123_0_0_1/* type */
	, &InternalEnumerator_1_t3134_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3134, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3134_FieldInfos[] =
{
	&InternalEnumerator_1_t3134____array_0_FieldInfo,
	&InternalEnumerator_1_t3134____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15929_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3134____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3134_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15929_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3134____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3134_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15932_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3134_PropertyInfos[] =
{
	&InternalEnumerator_1_t3134____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3134____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3134_InternalEnumerator_1__ctor_m15928_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15928_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ObliqueNear>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15928_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14156_gshared/* method */
	, &InternalEnumerator_1_t3134_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, InternalEnumerator_1_t3134_InternalEnumerator_1__ctor_m15928_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15928_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15929_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<ObliqueNear>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15929_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_gshared/* method */
	, &InternalEnumerator_1_t3134_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15929_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15930_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ObliqueNear>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15930_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14160_gshared/* method */
	, &InternalEnumerator_1_t3134_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15930_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15931_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<ObliqueNear>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15931_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14162_gshared/* method */
	, &InternalEnumerator_1_t3134_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15931_GenericMethod/* genericMethod */

};
extern Il2CppType ObliqueNear_t215_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15932_GenericMethod;
// T System.Array/InternalEnumerator`1<ObliqueNear>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15932_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14164_gshared/* method */
	, &InternalEnumerator_1_t3134_il2cpp_TypeInfo/* declaring_type */
	, &ObliqueNear_t215_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15932_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3134_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15928_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15929_MethodInfo,
	&InternalEnumerator_1_Dispose_m15930_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15931_MethodInfo,
	&InternalEnumerator_1_get_Current_m15932_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15931_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15930_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3134_VTable[] =
{
	&ValueType_Equals_m2145_MethodInfo,
	&Object_Finalize_m198_MethodInfo,
	&ValueType_GetHashCode_m2146_MethodInfo,
	&ValueType_ToString_m2236_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15929_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15931_MethodInfo,
	&InternalEnumerator_1_Dispose_m15930_MethodInfo,
	&InternalEnumerator_1_get_Current_m15932_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3134_InterfacesTypeInfos[] = 
{
	&IEnumerator_t318_il2cpp_TypeInfo,
	&IDisposable_t138_il2cpp_TypeInfo,
	&IEnumerator_1_t6212_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3134_InterfacesOffsets[] = 
{
	{ &IEnumerator_t318_il2cpp_TypeInfo, 4},
	{ &IDisposable_t138_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6212_il2cpp_TypeInfo, 7},
};
extern TypeInfo ObliqueNear_t215_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3134_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15932_MethodInfo/* Method Usage */,
	&ObliqueNear_t215_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisObliqueNear_t215_m33991_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3134_0_0_0;
extern Il2CppType InternalEnumerator_1_t3134_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3134_GenericClass;
TypeInfo InternalEnumerator_1_t3134_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3134_MethodInfos/* methods */
	, InternalEnumerator_1_t3134_PropertyInfos/* properties */
	, InternalEnumerator_1_t3134_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t484_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3134_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3134_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3134_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3134_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3134_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3134_1_0_0/* this_arg */
	, InternalEnumerator_1_t3134_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3134_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3134_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3134)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7943_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<ObliqueNear>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<ObliqueNear>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<ObliqueNear>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<ObliqueNear>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<ObliqueNear>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<ObliqueNear>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<ObliqueNear>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<ObliqueNear>
extern MethodInfo ICollection_1_get_Count_m44457_MethodInfo;
static PropertyInfo ICollection_1_t7943____Count_PropertyInfo = 
{
	&ICollection_1_t7943_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44457_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44458_MethodInfo;
static PropertyInfo ICollection_1_t7943____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7943_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44458_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7943_PropertyInfos[] =
{
	&ICollection_1_t7943____Count_PropertyInfo,
	&ICollection_1_t7943____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t123_0_0_0;
extern void* RuntimeInvoker_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44457_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<ObliqueNear>::get_Count()
MethodInfo ICollection_1_get_Count_m44457_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7943_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t123_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t123/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44457_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44458_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ObliqueNear>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44458_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7943_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44458_GenericMethod/* genericMethod */

};
extern Il2CppType ObliqueNear_t215_0_0_0;
extern Il2CppType ObliqueNear_t215_0_0_0;
static ParameterInfo ICollection_1_t7943_ICollection_1_Add_m44459_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ObliqueNear_t215_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44459_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ObliqueNear>::Add(T)
MethodInfo ICollection_1_Add_m44459_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t/* invoker_method */
	, ICollection_1_t7943_ICollection_1_Add_m44459_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44459_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44460_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ObliqueNear>::Clear()
MethodInfo ICollection_1_Clear_m44460_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44460_GenericMethod/* genericMethod */

};
extern Il2CppType ObliqueNear_t215_0_0_0;
static ParameterInfo ICollection_1_t7943_ICollection_1_Contains_m44461_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ObliqueNear_t215_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44461_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ObliqueNear>::Contains(T)
MethodInfo ICollection_1_Contains_m44461_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7943_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7943_ICollection_1_Contains_m44461_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44461_GenericMethod/* genericMethod */

};
extern Il2CppType ObliqueNearU5BU5D_t5775_0_0_0;
extern Il2CppType ObliqueNearU5BU5D_t5775_0_0_0;
extern Il2CppType Int32_t123_0_0_0;
static ParameterInfo ICollection_1_t7943_ICollection_1_CopyTo_m44462_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ObliqueNearU5BU5D_t5775_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t123_0_0_0},
};
extern Il2CppType Void_t111_0_0_0;
extern void* RuntimeInvoker_Void_t111_Object_t_Int32_t123 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44462_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ObliqueNear>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44462_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t111_0_0_0/* return_type */
	, RuntimeInvoker_Void_t111_Object_t_Int32_t123/* invoker_method */
	, ICollection_1_t7943_ICollection_1_CopyTo_m44462_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44462_GenericMethod/* genericMethod */

};
extern Il2CppType ObliqueNear_t215_0_0_0;
static ParameterInfo ICollection_1_t7943_ICollection_1_Remove_m44463_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ObliqueNear_t215_0_0_0},
};
extern Il2CppType Boolean_t122_0_0_0;
extern void* RuntimeInvoker_Boolean_t122_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44463_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ObliqueNear>::Remove(T)
MethodInfo ICollection_1_Remove_m44463_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7943_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t122_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t122_Object_t/* invoker_method */
	, ICollection_1_t7943_ICollection_1_Remove_m44463_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44463_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7943_MethodInfos[] =
{
	&ICollection_1_get_Count_m44457_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44458_MethodInfo,
	&ICollection_1_Add_m44459_MethodInfo,
	&ICollection_1_Clear_m44460_MethodInfo,
	&ICollection_1_Contains_m44461_MethodInfo,
	&ICollection_1_CopyTo_m44462_MethodInfo,
	&ICollection_1_Remove_m44463_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7945_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7943_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1179_il2cpp_TypeInfo,
	&IEnumerable_1_t7945_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7943_0_0_0;
extern Il2CppType ICollection_1_t7943_1_0_0;
struct ICollection_1_t7943;
extern Il2CppGenericClass ICollection_1_t7943_GenericClass;
TypeInfo ICollection_1_t7943_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7943_MethodInfos/* methods */
	, ICollection_1_t7943_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7943_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7943_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7943_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7943_0_0_0/* byval_arg */
	, &ICollection_1_t7943_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7943_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
