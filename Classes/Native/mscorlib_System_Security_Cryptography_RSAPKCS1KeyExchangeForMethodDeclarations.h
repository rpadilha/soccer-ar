﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter
struct RSAPKCS1KeyExchangeFormatter_t1737;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1403;
// System.Byte[]
struct ByteU5BU5D_t653;

// System.Void System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::.ctor(System.Security.Cryptography.AsymmetricAlgorithm)
 void RSAPKCS1KeyExchangeFormatter__ctor_m9131 (RSAPKCS1KeyExchangeFormatter_t1737 * __this, AsymmetricAlgorithm_t1403 * ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::CreateKeyExchange(System.Byte[])
 ByteU5BU5D_t653* RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m9132 (RSAPKCS1KeyExchangeFormatter_t1737 * __this, ByteU5BU5D_t653* ___rgbData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter::SetRSAKey(System.Security.Cryptography.AsymmetricAlgorithm)
 void RSAPKCS1KeyExchangeFormatter_SetRSAKey_m12197 (RSAPKCS1KeyExchangeFormatter_t1737 * __this, AsymmetricAlgorithm_t1403 * ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
