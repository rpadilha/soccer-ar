﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
struct Enumerator_t3462;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3452;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void Enumerator__ctor_m18368_gshared (Enumerator_t3462 * __this, Dictionary_2_t3452 * ___host, MethodInfo* method);
#define Enumerator__ctor_m18368(__this, ___host, method) (void)Enumerator__ctor_m18368_gshared((Enumerator_t3462 *)__this, (Dictionary_2_t3452 *)___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18369_gshared (Enumerator_t3462 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18369(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m18369_gshared((Enumerator_t3462 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::Dispose()
 void Enumerator_Dispose_m18370_gshared (Enumerator_t3462 * __this, MethodInfo* method);
#define Enumerator_Dispose_m18370(__this, method) (void)Enumerator_Dispose_m18370_gshared((Enumerator_t3462 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::MoveNext()
 bool Enumerator_MoveNext_m18371_gshared (Enumerator_t3462 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m18371(__this, method) (bool)Enumerator_MoveNext_m18371_gshared((Enumerator_t3462 *)__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::get_Current()
 Object_t * Enumerator_get_Current_m18372_gshared (Enumerator_t3462 * __this, MethodInfo* method);
#define Enumerator_get_Current_m18372(__this, method) (Object_t *)Enumerator_get_Current_m18372_gshared((Enumerator_t3462 *)__this, method)
