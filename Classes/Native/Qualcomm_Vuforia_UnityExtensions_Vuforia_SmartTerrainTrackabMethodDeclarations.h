﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackableBehaviour
struct SmartTerrainTrackableBehaviour_t627;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t625;

// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableBehaviour::get_SmartTerrainTrackable()
 Object_t * SmartTerrainTrackableBehaviour_get_SmartTerrainTrackable_m2922 (SmartTerrainTrackableBehaviour_t627 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackableBehaviour::get_AutomaticUpdatesDisabled()
 bool SmartTerrainTrackableBehaviour_get_AutomaticUpdatesDisabled_m2923 (SmartTerrainTrackableBehaviour_t627 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::UpdateMeshAndColliders()
 void SmartTerrainTrackableBehaviour_UpdateMeshAndColliders_m459 (SmartTerrainTrackableBehaviour_t627 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::SetAutomaticUpdatesDisabled(System.Boolean)
 void SmartTerrainTrackableBehaviour_SetAutomaticUpdatesDisabled_m2924 (SmartTerrainTrackableBehaviour_t627 * __this, bool ___disabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::Start()
 void SmartTerrainTrackableBehaviour_Start_m460 (SmartTerrainTrackableBehaviour_t627 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::.ctor()
 void SmartTerrainTrackableBehaviour__ctor_m2925 (SmartTerrainTrackableBehaviour_t627 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
