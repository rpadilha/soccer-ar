﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t1138;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t1139;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t1136;

// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
 void PersistentCallGroup__ctor_m6547 (PersistentCallGroup_t1138 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
 void PersistentCallGroup_Initialize_m6548 (PersistentCallGroup_t1138 * __this, InvokableCallList_t1139 * ___invokableList, UnityEventBase_t1136 * ___unityEventBase, MethodInfo* method) IL2CPP_METHOD_ATTR;
