﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TargetFinder/TargetSearchResult
struct TargetSearchResult_t776;
struct TargetSearchResult_t776_marshaled;

void TargetSearchResult_t776_marshal(const TargetSearchResult_t776& unmarshaled, TargetSearchResult_t776_marshaled& marshaled);
void TargetSearchResult_t776_marshal_back(const TargetSearchResult_t776_marshaled& marshaled, TargetSearchResult_t776& unmarshaled);
void TargetSearchResult_t776_marshal_cleanup(TargetSearchResult_t776_marshaled& marshaled);
