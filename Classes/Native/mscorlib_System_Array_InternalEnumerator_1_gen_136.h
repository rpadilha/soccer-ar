﻿#pragma once
#include <stdint.h>
// System.Array
struct Array_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Array/InternalEnumerator`1<Player_Script/Player_State>
struct InternalEnumerator_1_t3097 
{
	// System.Array System.Array/InternalEnumerator`1<Player_Script/Player_State>::array
	Array_t * ___array_0;
	// System.Int32 System.Array/InternalEnumerator`1<Player_Script/Player_State>::idx
	int32_t ___idx_1;
};
