﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Resources
struct Resources_t195;
// UnityEngine.Object
struct Object_t120;
struct Object_t120_marshaled;
// System.String
struct String_t;
// System.Type
struct Type_t;
// UnityEngine.AsyncOperation
struct AsyncOperation_t952;
struct AsyncOperation_t952_marshaled;

// UnityEngine.Object UnityEngine.Resources::Load(System.String)
 Object_t120 * Resources_Load_m653 (Object_t * __this/* static, unused */, String_t* ___path, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
 Object_t120 * Resources_Load_m6068 (Object_t * __this/* static, unused */, String_t* ___path, Type_t * ___systemTypeInstance, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
 AsyncOperation_t952 * Resources_UnloadUnusedAssets_m5361 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
