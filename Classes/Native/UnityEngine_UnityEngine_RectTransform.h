﻿#pragma once
#include <stdint.h>
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t558;
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.RectTransform
struct RectTransform_t338  : public Transform_t74
{
};
struct RectTransform_t338_StaticFields{
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t558 * ___reapplyDrivenProperties_2;
};
