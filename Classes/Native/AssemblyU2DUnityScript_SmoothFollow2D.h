﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t74;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// SmoothFollow2D
struct SmoothFollow2D_t222  : public MonoBehaviour_t10
{
	// UnityEngine.Transform SmoothFollow2D::target
	Transform_t74 * ___target_2;
	// System.Single SmoothFollow2D::smoothTime
	float ___smoothTime_3;
	// UnityEngine.Transform SmoothFollow2D::thisTransform
	Transform_t74 * ___thisTransform_4;
	// UnityEngine.Vector2 SmoothFollow2D::velocity
	Vector2_t99  ___velocity_5;
};
