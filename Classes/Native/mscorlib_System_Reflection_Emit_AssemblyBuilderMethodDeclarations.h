﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1950;
// System.String
struct String_t;
// System.Reflection.Module[]
struct ModuleU5BU5D_t1951;
// System.Exception
struct Exception_t151;
// System.Reflection.AssemblyName
struct AssemblyName_t1952;

// System.String System.Reflection.Emit.AssemblyBuilder::get_Location()
 String_t* AssemblyBuilder_get_Location_m11112 (AssemblyBuilder_t1950 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module[] System.Reflection.Emit.AssemblyBuilder::GetModulesInternal()
 ModuleU5BU5D_t1951* AssemblyBuilder_GetModulesInternal_m11113 (AssemblyBuilder_t1950 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.AssemblyBuilder::get_IsCompilerContext()
 bool AssemblyBuilder_get_IsCompilerContext_m11114 (AssemblyBuilder_t1950 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.AssemblyBuilder::not_supported()
 Exception_t151 * AssemblyBuilder_not_supported_m11115 (AssemblyBuilder_t1950 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyName System.Reflection.Emit.AssemblyBuilder::UnprotectedGetName()
 AssemblyName_t1952 * AssemblyBuilder_UnprotectedGetName_m11116 (AssemblyBuilder_t1950 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
