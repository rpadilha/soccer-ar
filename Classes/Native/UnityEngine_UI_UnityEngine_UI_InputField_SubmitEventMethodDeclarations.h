﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t367;

// System.Void UnityEngine.UI.InputField/SubmitEvent::.ctor()
 void SubmitEvent__ctor_m1346 (SubmitEvent_t367 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
