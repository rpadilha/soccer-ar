﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<Vuforia.IVideoBackgroundEventHandler>
struct Comparison_1_t4508;
// System.Object
struct Object_t;
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t112;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m27239(__this, ___object, ___method, method) (void)Comparison_1__ctor_m14743_gshared((Comparison_1_t2833 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// System.Int32 System.Comparison`1<Vuforia.IVideoBackgroundEventHandler>::Invoke(T,T)
#define Comparison_1_Invoke_m27240(__this, ___x, ___y, method) (int32_t)Comparison_1_Invoke_m14744_gshared((Comparison_1_t2833 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.IAsyncResult System.Comparison`1<Vuforia.IVideoBackgroundEventHandler>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m27241(__this, ___x, ___y, ___callback, ___object, method) (Object_t *)Comparison_1_BeginInvoke_m14745_gshared((Comparison_1_t2833 *)__this, (Object_t *)___x, (Object_t *)___y, (AsyncCallback_t251 *)___callback, (Object_t *)___object, method)
// System.Int32 System.Comparison`1<Vuforia.IVideoBackgroundEventHandler>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m27242(__this, ___result, method) (int32_t)Comparison_1_EndInvoke_m14746_gshared((Comparison_1_t2833 *)__this, (Object_t *)___result, method)
