﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// UnityEngine.Object
struct Object_t120;
struct Object_t120_marshaled;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Object>
struct UnityAction_1_t4793  : public MulticastDelegate_t373
{
};
