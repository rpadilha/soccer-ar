﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct Comparer_1_t3293;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct Comparer_1_t3293  : public Object_t
{
};
struct Comparer_1_t3293_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.EventTrigger/Entry>::_default
	Comparer_1_t3293 * ____default_0;
};
