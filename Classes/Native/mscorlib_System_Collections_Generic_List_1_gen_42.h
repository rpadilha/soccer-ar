﻿#pragma once
#include <stdint.h>
// Vuforia.ImageTarget[]
struct ImageTargetU5BU5D_t920;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.ImageTarget>
struct List_1_t4425  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ImageTarget>::_items
	ImageTargetU5BU5D_t920* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ImageTarget>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ImageTarget>::_version
	int32_t ____version_3;
};
struct List_1_t4425_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ImageTarget>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.ImageTarget>::EmptyArray
	ImageTargetU5BU5D_t920* ___EmptyArray_4;
};
