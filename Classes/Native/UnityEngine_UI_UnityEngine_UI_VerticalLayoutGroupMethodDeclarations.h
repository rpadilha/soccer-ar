﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.VerticalLayoutGroup
struct VerticalLayoutGroup_t448;

// System.Void UnityEngine.UI.VerticalLayoutGroup::.ctor()
 void VerticalLayoutGroup__ctor_m1997 (VerticalLayoutGroup_t448 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputHorizontal()
 void VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1998 (VerticalLayoutGroup_t448 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputVertical()
 void VerticalLayoutGroup_CalculateLayoutInputVertical_m1999 (VerticalLayoutGroup_t448 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutHorizontal()
 void VerticalLayoutGroup_SetLayoutHorizontal_m2000 (VerticalLayoutGroup_t448 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutVertical()
 void VerticalLayoutGroup_SetLayoutVertical_m2001 (VerticalLayoutGroup_t448 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
