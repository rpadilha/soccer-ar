﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
struct InternalEnumerator_1_t3950;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m21814 (InternalEnumerator_1_t3950 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21815 (InternalEnumerator_1_t3950 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::Dispose()
 void InternalEnumerator_1_Dispose_m21816 (InternalEnumerator_1_t3950 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m21817 (InternalEnumerator_1_t3950 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m21818 (InternalEnumerator_1_t3950 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
