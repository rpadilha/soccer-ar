﻿#pragma once
#include <stdint.h>
// System.Text.EncoderFallback
struct EncoderFallback_t2203;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.EncoderFallback
struct EncoderFallback_t2203  : public Object_t
{
};
struct EncoderFallback_t2203_StaticFields{
	// System.Text.EncoderFallback System.Text.EncoderFallback::exception_fallback
	EncoderFallback_t2203 * ___exception_fallback_0;
	// System.Text.EncoderFallback System.Text.EncoderFallback::replacement_fallback
	EncoderFallback_t2203 * ___replacement_fallback_1;
	// System.Text.EncoderFallback System.Text.EncoderFallback::standard_safe_fallback
	EncoderFallback_t2203 * ___standard_safe_fallback_2;
};
