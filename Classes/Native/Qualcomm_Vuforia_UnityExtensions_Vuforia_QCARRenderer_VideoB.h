﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.QCARRenderer/VideoBackgroundReflection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB.h"
// Vuforia.QCARRenderer/VideoBackgroundReflection
struct VideoBackgroundReflection_t704 
{
	// System.Int32 Vuforia.QCARRenderer/VideoBackgroundReflection::value__
	int32_t ___value___1;
};
