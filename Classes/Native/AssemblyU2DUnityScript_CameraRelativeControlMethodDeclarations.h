﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraRelativeControl
struct CameraRelativeControl_t210;

// System.Void CameraRelativeControl::.ctor()
 void CameraRelativeControl__ctor_m734 (CameraRelativeControl_t210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraRelativeControl::Start()
 void CameraRelativeControl_Start_m735 (CameraRelativeControl_t210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraRelativeControl::FaceMovementDirection()
 void CameraRelativeControl_FaceMovementDirection_m736 (CameraRelativeControl_t210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraRelativeControl::OnEndGame()
 void CameraRelativeControl_OnEndGame_m737 (CameraRelativeControl_t210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraRelativeControl::Update()
 void CameraRelativeControl_Update_m738 (CameraRelativeControl_t210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraRelativeControl::Main()
 void CameraRelativeControl_Main_m739 (CameraRelativeControl_t210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
