﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.DataSet/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet_StorageType.h"
// Vuforia.DataSet/StorageType
struct StorageType_t628 
{
	// System.Int32 Vuforia.DataSet/StorageType::value__
	int32_t ___value___1;
};
