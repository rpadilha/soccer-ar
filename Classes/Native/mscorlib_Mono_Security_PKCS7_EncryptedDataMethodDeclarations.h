﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.PKCS7/EncryptedData
struct EncryptedData_t1831;
// Mono.Security.PKCS7/ContentInfo
struct ContentInfo_t1839;
// System.Byte[]
struct ByteU5BU5D_t653;
// Mono.Security.ASN1
struct ASN1_t1826;

// System.Void Mono.Security.PKCS7/EncryptedData::.ctor()
 void EncryptedData__ctor_m10369 (EncryptedData_t1831 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/EncryptedData::.ctor(Mono.Security.ASN1)
 void EncryptedData__ctor_m10370 (EncryptedData_t1831 * __this, ASN1_t1826 * ___asn1, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.PKCS7/ContentInfo Mono.Security.PKCS7/EncryptedData::get_EncryptionAlgorithm()
 ContentInfo_t1839 * EncryptedData_get_EncryptionAlgorithm_m10371 (EncryptedData_t1831 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/EncryptedData::get_EncryptedContent()
 ByteU5BU5D_t653* EncryptedData_get_EncryptedContent_m10372 (EncryptedData_t1831 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
