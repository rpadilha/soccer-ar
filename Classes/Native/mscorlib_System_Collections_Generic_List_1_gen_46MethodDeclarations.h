﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t811;
// System.Object
struct Object_t;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t812;
// System.Collections.Generic.IEnumerable`1<Vuforia.IUserDefinedTargetEventHandler>
struct IEnumerable_1_t4543;
// System.Collections.Generic.IEnumerator`1<Vuforia.IUserDefinedTargetEventHandler>
struct IEnumerator_1_t4544;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// System.Collections.Generic.ICollection`1<Vuforia.IUserDefinedTargetEventHandler>
struct ICollection_1_t4545;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>
struct ReadOnlyCollection_1_t4546;
// Vuforia.IUserDefinedTargetEventHandler[]
struct IUserDefinedTargetEventHandlerU5BU5D_t4542;
// System.Predicate`1<Vuforia.IUserDefinedTargetEventHandler>
struct Predicate_1_t4547;
// System.Comparison`1<Vuforia.IUserDefinedTargetEventHandler>
struct Comparison_1_t4548;
// System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_21.h"

// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
#define List_1__ctor_m5541(__this, method) (void)List_1__ctor_m14461_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m27426(__this, ___collection, method) (void)List_1__ctor_m14463_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m27427(__this, ___capacity, method) (void)List_1__ctor_m14465_gshared((List_1_t149 *)__this, (int32_t)___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::.cctor()
#define List_1__cctor_m27428(__this/* static, unused */, method) (void)List_1__cctor_m14467_gshared((Object_t *)__this/* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m27429(__this, method) (Object_t*)List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14469_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m27430(__this, ___array, ___arrayIndex, method) (void)List_1_System_Collections_ICollection_CopyTo_m14471_gshared((List_1_t149 *)__this, (Array_t *)___array, (int32_t)___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m27431(__this, method) (Object_t *)List_1_System_Collections_IEnumerable_GetEnumerator_m14473_gshared((List_1_t149 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m27432(__this, ___item, method) (int32_t)List_1_System_Collections_IList_Add_m14475_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m27433(__this, ___item, method) (bool)List_1_System_Collections_IList_Contains_m14477_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m27434(__this, ___item, method) (int32_t)List_1_System_Collections_IList_IndexOf_m14479_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m27435(__this, ___index, ___item, method) (void)List_1_System_Collections_IList_Insert_m14481_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m27436(__this, ___item, method) (void)List_1_System_Collections_IList_Remove_m14483_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m27437(__this, method) (bool)List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14485_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m27438(__this, method) (bool)List_1_System_Collections_ICollection_get_IsSynchronized_m14487_gshared((List_1_t149 *)__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m27439(__this, method) (Object_t *)List_1_System_Collections_ICollection_get_SyncRoot_m14489_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m27440(__this, method) (bool)List_1_System_Collections_IList_get_IsFixedSize_m14491_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m27441(__this, method) (bool)List_1_System_Collections_IList_get_IsReadOnly_m14493_gshared((List_1_t149 *)__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m27442(__this, ___index, method) (Object_t *)List_1_System_Collections_IList_get_Item_m14495_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m27443(__this, ___index, ___value, method) (void)List_1_System_Collections_IList_set_Item_m14497_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Add(T)
#define List_1_Add_m5536(__this, ___item, method) (void)List_1_Add_m14499_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m27444(__this, ___newCount, method) (void)List_1_GrowIfNeeded_m14501_gshared((List_1_t149 *)__this, (int32_t)___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m27445(__this, ___collection, method) (void)List_1_AddCollection_m14503_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m27446(__this, ___enumerable, method) (void)List_1_AddEnumerable_m14505_gshared((List_1_t149 *)__this, (Object_t*)___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m27447(__this, ___collection, method) (void)List_1_AddRange_m14506_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m27448(__this, method) (ReadOnlyCollection_1_t4546 *)List_1_AsReadOnly_m14508_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Clear()
#define List_1_Clear_m27449(__this, method) (void)List_1_Clear_m14510_gshared((List_1_t149 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Contains(T)
#define List_1_Contains_m27450(__this, ___item, method) (bool)List_1_Contains_m14512_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m27451(__this, ___array, ___arrayIndex, method) (void)List_1_CopyTo_m14514_gshared((List_1_t149 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m27452(__this, ___match, method) (Object_t *)List_1_Find_m14516_gshared((List_1_t149 *)__this, (Predicate_1_t2832 *)___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m27453(__this/* static, unused */, ___match, method) (void)List_1_CheckMatch_m14518_gshared((Object_t *)__this/* static, unused */, (Predicate_1_t2832 *)___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m27454(__this, ___startIndex, ___count, ___match, method) (int32_t)List_1_GetIndex_m14520_gshared((List_1_t149 *)__this, (int32_t)___startIndex, (int32_t)___count, (Predicate_1_t2832 *)___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::GetEnumerator()
 Enumerator_t945  List_1_GetEnumerator_m5538 (List_1_t811 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::IndexOf(T)
#define List_1_IndexOf_m27455(__this, ___item, method) (int32_t)List_1_IndexOf_m14522_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m27456(__this, ___start, ___delta, method) (void)List_1_Shift_m14524_gshared((List_1_t149 *)__this, (int32_t)___start, (int32_t)___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m27457(__this, ___index, method) (void)List_1_CheckIndex_m14526_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m27458(__this, ___index, ___item, method) (void)List_1_Insert_m14528_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m27459(__this, ___collection, method) (void)List_1_CheckCollection_m14530_gshared((List_1_t149 *)__this, (Object_t*)___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Remove(T)
#define List_1_Remove_m5537(__this, ___item, method) (bool)List_1_Remove_m14532_gshared((List_1_t149 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m27460(__this, ___match, method) (int32_t)List_1_RemoveAll_m14534_gshared((List_1_t149 *)__this, (Predicate_1_t2832 *)___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m27461(__this, ___index, method) (void)List_1_RemoveAt_m14536_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Reverse()
#define List_1_Reverse_m27462(__this, method) (void)List_1_Reverse_m14538_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Sort()
#define List_1_Sort_m27463(__this, method) (void)List_1_Sort_m14540_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m27464(__this, ___comparison, method) (void)List_1_Sort_m14542_gshared((List_1_t149 *)__this, (Comparison_1_t2833 *)___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::ToArray()
#define List_1_ToArray_m27465(__this, method) (IUserDefinedTargetEventHandlerU5BU5D_t4542*)List_1_ToArray_m14544_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::TrimExcess()
#define List_1_TrimExcess_m27466(__this, method) (void)List_1_TrimExcess_m14546_gshared((List_1_t149 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::get_Capacity()
#define List_1_get_Capacity_m27467(__this, method) (int32_t)List_1_get_Capacity_m14548_gshared((List_1_t149 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m27468(__this, ___value, method) (void)List_1_set_Capacity_m14550_gshared((List_1_t149 *)__this, (int32_t)___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::get_Count()
#define List_1_get_Count_m27469(__this, method) (int32_t)List_1_get_Count_m14552_gshared((List_1_t149 *)__this, method)
// T System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m27470(__this, ___index, method) (Object_t *)List_1_get_Item_m14554_gshared((List_1_t149 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m27471(__this, ___index, ___value, method) (void)List_1_set_Item_m14556_gshared((List_1_t149 *)__this, (int32_t)___index, (Object_t *)___value, method)
