﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>
struct ReadOnlyCollection_1_t4383;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t30;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>
struct IList_1_t4386;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t318;
// Vuforia.VirtualButtonAbstractBehaviour[]
struct VirtualButtonAbstractBehaviourU5BU5D_t773;
// System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>
struct IEnumerator_1_t914;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_0MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m26097(__this, ___list, method) (void)ReadOnlyCollection_1__ctor_m14564_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t*)___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26098(__this, ___item, method) (void)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14565_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m26099(__this, method) (void)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14566_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m26100(__this, ___index, ___item, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14567_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m26101(__this, ___item, method) (bool)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14568_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m26102(__this, ___index, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14569_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m26103(__this, ___index, method) (VirtualButtonAbstractBehaviour_t30 *)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14570_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m26104(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14571_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26105(__this, method) (bool)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14572_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m26106(__this, ___array, ___index, method) (void)ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14573_gshared((ReadOnlyCollection_1_t2836 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m26107(__this, method) (Object_t *)ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14574_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m26108(__this, ___value, method) (int32_t)ReadOnlyCollection_1_System_Collections_IList_Add_m14575_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m26109(__this, method) (void)ReadOnlyCollection_1_System_Collections_IList_Clear_m14576_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m26110(__this, ___value, method) (bool)ReadOnlyCollection_1_System_Collections_IList_Contains_m14577_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m26111(__this, ___value, method) (int32_t)ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14578_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m26112(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_Insert_m14579_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m26113(__this, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_Remove_m14580_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m26114(__this, ___index, method) (void)ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14581_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m26115(__this, method) (bool)ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14582_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m26116(__this, method) (Object_t *)ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14583_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m26117(__this, method) (bool)ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14584_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m26118(__this, method) (bool)ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14585_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m26119(__this, ___index, method) (Object_t *)ReadOnlyCollection_1_System_Collections_IList_get_Item_m14586_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m26120(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_set_Item_m14587_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Contains(T)
#define ReadOnlyCollection_1_Contains_m26121(__this, ___value, method) (bool)ReadOnlyCollection_1_Contains_m14588_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m26122(__this, ___array, ___index, method) (void)ReadOnlyCollection_1_CopyTo_m14589_gshared((ReadOnlyCollection_1_t2836 *)__this, (ObjectU5BU5D_t130*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m26123(__this, method) (Object_t*)ReadOnlyCollection_1_GetEnumerator_m14590_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m26124(__this, ___value, method) (int32_t)ReadOnlyCollection_1_IndexOf_m14591_gshared((ReadOnlyCollection_1_t2836 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Count()
#define ReadOnlyCollection_1_get_Count_m26125(__this, method) (int32_t)ReadOnlyCollection_1_get_Count_m14592_gshared((ReadOnlyCollection_1_t2836 *)__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m26126(__this, ___index, method) (VirtualButtonAbstractBehaviour_t30 *)ReadOnlyCollection_1_get_Item_m14593_gshared((ReadOnlyCollection_1_t2836 *)__this, (int32_t)___index, method)
