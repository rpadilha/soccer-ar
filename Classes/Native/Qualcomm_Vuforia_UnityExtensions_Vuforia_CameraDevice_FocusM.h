﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"
// Vuforia.CameraDevice/FocusMode
struct FocusMode_t600 
{
	// System.Int32 Vuforia.CameraDevice/FocusMode::value__
	int32_t ___value___1;
};
