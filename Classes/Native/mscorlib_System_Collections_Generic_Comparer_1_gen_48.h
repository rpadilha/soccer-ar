﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.IVirtualButtonEventHandler>
struct Comparer_1_t4593;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.IVirtualButtonEventHandler>
struct Comparer_1_t4593  : public Object_t
{
};
struct Comparer_1_t4593_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.IVirtualButtonEventHandler>::_default
	Comparer_1_t4593 * ____default_0;
};
