﻿#pragma once
#include <stdint.h>
// UnityEngine.Collider
struct Collider_t70;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.Collider>
struct CastHelper_1_t3147 
{
	// T UnityEngine.CastHelper`1<UnityEngine.Collider>::t
	Collider_t70 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.Collider>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
