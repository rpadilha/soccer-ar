﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct ObjectPool_1_t337;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct UnityAction_1_t343;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t345;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m2299(__this, ___actionOnGet, ___actionOnRelease, method) (void)ObjectPool_1__ctor_m16391_gshared((ObjectPool_1_t3208 *)__this, (UnityAction_1_t2763 *)___actionOnGet, (UnityAction_1_t2763 *)___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countAll()
#define ObjectPool_1_get_countAll_m18707(__this, method) (int32_t)ObjectPool_1_get_countAll_m16393_gshared((ObjectPool_1_t3208 *)__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m18708(__this, ___value, method) (void)ObjectPool_1_set_countAll_m16395_gshared((ObjectPool_1_t3208 *)__this, (int32_t)___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countActive()
#define ObjectPool_1_get_countActive_m18709(__this, method) (int32_t)ObjectPool_1_get_countActive_m16397_gshared((ObjectPool_1_t3208 *)__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m18710(__this, method) (int32_t)ObjectPool_1_get_countInactive_m16399_gshared((ObjectPool_1_t3208 *)__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Get()
#define ObjectPool_1_Get_m2307(__this, method) (List_1_t345 *)ObjectPool_1_Get_m16401_gshared((ObjectPool_1_t3208 *)__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Release(T)
#define ObjectPool_1_Release_m2311(__this, ___element, method) (void)ObjectPool_1_Release_m16403_gshared((ObjectPool_1_t3208 *)__this, (Object_t *)___element, method)
