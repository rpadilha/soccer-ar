﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogType.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo LogType_t959_il2cpp_TypeInfo;
// UnityEngine.Application/LogCallback
struct LogCallback_t1034  : public MulticastDelegate_t373
{
};
