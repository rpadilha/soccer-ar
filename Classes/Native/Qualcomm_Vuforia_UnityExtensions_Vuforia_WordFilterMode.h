﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.WordFilterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilterMode.h"
// Vuforia.WordFilterMode
struct WordFilterMode_t816 
{
	// System.Int32 Vuforia.WordFilterMode::value__
	int32_t ___value___1;
};
