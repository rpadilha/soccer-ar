﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GoalKeeperJump_Down
struct GoalKeeperJump_Down_t79;
// UnityEngine.Collider
struct Collider_t70;

// System.Void GoalKeeperJump_Down::.ctor()
 void GoalKeeperJump_Down__ctor_m125 (GoalKeeperJump_Down_t79 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoalKeeperJump_Down::Start()
 void GoalKeeperJump_Down_Start_m126 (GoalKeeperJump_Down_t79 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoalKeeperJump_Down::Update()
 void GoalKeeperJump_Down_Update_m127 (GoalKeeperJump_Down_t79 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoalKeeperJump_Down::OnTriggerEnter(UnityEngine.Collider)
 void GoalKeeperJump_Down_OnTriggerEnter_m128 (GoalKeeperJump_Down_t79 * __this, Collider_t70 * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
