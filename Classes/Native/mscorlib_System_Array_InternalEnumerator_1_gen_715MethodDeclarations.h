﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>
struct InternalEnumerator_1_t5261;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Runtime.InteropServices.ComInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ComInterfaceType.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31677 (InternalEnumerator_1_t5261 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31678 (InternalEnumerator_1_t5261 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::Dispose()
 void InternalEnumerator_1_Dispose_m31679 (InternalEnumerator_1_t5261 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31680 (InternalEnumerator_1_t5261 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31681 (InternalEnumerator_1_t5261 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
