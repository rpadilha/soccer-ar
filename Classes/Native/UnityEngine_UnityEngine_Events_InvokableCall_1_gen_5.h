﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>
struct UnityAction_1_t2795;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
struct InvokableCall_1_t2794  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::Delegate
	UnityAction_1_t2795 * ___Delegate_0;
};
