﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<SmoothFollow2D>
struct UnityAction_1_t3160;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<SmoothFollow2D>
struct InvokableCall_1_t3159  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<SmoothFollow2D>::Delegate
	UnityAction_1_t3160 * ___Delegate_0;
};
