﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// <PrivateImplementationDetails>/$ArrayType$1024
#pragma pack(push, tp, 1)
struct $ArrayType$1024_t2331 
{
	union
	{
	};
};
#pragma pack(pop, tp)
// Native definition for marshalling of: <PrivateImplementationDetails>/$ArrayType$1024
#pragma pack(push, tp, 1)
struct $ArrayType$1024_t2331_marshaled
{
	union
	{
	};
};
#pragma pack(pop, tp)
