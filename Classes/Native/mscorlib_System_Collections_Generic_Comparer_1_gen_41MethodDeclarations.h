﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<Vuforia.VirtualButtonAbstractBehaviour>
struct Comparer_1_t4390;
// System.Object
struct Object_t;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t30;

// System.Void System.Collections.Generic.Comparer`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor()
// System.Collections.Generic.Comparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_genMethodDeclarations.h"
#define Comparer_1__ctor_m26175(__this, method) (void)Comparer_1__ctor_m14672_gshared((Comparer_1_t2848 *)__this, method)
// System.Void System.Collections.Generic.Comparer`1<Vuforia.VirtualButtonAbstractBehaviour>::.cctor()
#define Comparer_1__cctor_m26176(__this/* static, unused */, method) (void)Comparer_1__cctor_m14673_gshared((Object_t *)__this/* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IComparer.Compare(System.Object,System.Object)
#define Comparer_1_System_Collections_IComparer_Compare_m26177(__this, ___x, ___y, method) (int32_t)Comparer_1_System_Collections_IComparer_Compare_m14674_gshared((Comparer_1_t2848 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.VirtualButtonAbstractBehaviour>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Default()
#define Comparer_1_get_Default_m26178(__this/* static, unused */, method) (Comparer_1_t4390 *)Comparer_1_get_Default_m14675_gshared((Object_t *)__this/* static, unused */, method)
