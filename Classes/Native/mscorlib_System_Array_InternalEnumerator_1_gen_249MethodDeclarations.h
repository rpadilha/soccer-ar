﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>
struct InternalEnumerator_1_t3612;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UI.InputField/InputType
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m19562 (InternalEnumerator_1_t3612 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19563 (InternalEnumerator_1_t3612 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::Dispose()
 void InternalEnumerator_1_Dispose_m19564 (InternalEnumerator_1_t3612 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m19565 (InternalEnumerator_1_t3612 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m19566 (InternalEnumerator_1_t3612 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
