﻿#pragma once
#include <stdint.h>
// Vuforia.Prop
struct Prop_t15;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Prop>
struct Comparison_1_t4315  : public MulticastDelegate_t373
{
};
