﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackableBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_117.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackableBehaviour>
struct CachedInvokableCall_1_t3884  : public InvokableCall_1_t3885
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackableBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
