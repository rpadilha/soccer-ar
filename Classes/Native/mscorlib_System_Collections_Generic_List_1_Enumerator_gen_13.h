﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>
struct List_1_t757;
// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t763;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>
struct Enumerator_t886 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::l
	List_1_t757 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::current
	Object_t * ___current_3;
};
