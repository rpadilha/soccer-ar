﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t961;

// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
 void WaitForFixedUpdate__ctor_m5584 (WaitForFixedUpdate_t961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
