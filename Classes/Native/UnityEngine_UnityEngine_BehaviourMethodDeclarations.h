﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Behaviour
struct Behaviour_t559;

// System.Void UnityEngine.Behaviour::.ctor()
 void Behaviour__ctor_m6111 (Behaviour_t559 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_enabled()
 bool Behaviour_get_enabled_m536 (Behaviour_t559 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
 void Behaviour_set_enabled_m547 (Behaviour_t559 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
 bool Behaviour_get_isActiveAndEnabled_m2147 (Behaviour_t559 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
