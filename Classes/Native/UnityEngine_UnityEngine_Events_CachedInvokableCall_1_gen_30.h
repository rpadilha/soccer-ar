﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_26.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>
struct CachedInvokableCall_1_t2961  : public InvokableCall_1_t2962
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
