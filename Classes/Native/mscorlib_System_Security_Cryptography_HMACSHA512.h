﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.HMAC
#include "mscorlib_System_Security_Cryptography_HMAC.h"
// System.Security.Cryptography.HMACSHA512
struct HMACSHA512_t2153  : public HMAC_t1721
{
	// System.Boolean System.Security.Cryptography.HMACSHA512::legacy
	bool ___legacy_11;
};
struct HMACSHA512_t2153_StaticFields{
	// System.Boolean System.Security.Cryptography.HMACSHA512::legacy_mode
	bool ___legacy_mode_10;
};
