﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ShieldMenu
struct ShieldMenu_t92;

// System.Void ShieldMenu::.ctor()
 void ShieldMenu__ctor_m150 (ShieldMenu_t92 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldMenu::Start()
 void ShieldMenu_Start_m151 (ShieldMenu_t92 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldMenu::Update()
 void ShieldMenu_Update_m152 (ShieldMenu_t92 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
