﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DefaultSmartTerrainEventHandler
struct DefaultSmartTerrainEventHandler_t14;
// Vuforia.Prop
struct Prop_t15;
// Vuforia.Surface
struct Surface_t16;

// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
 void DefaultSmartTerrainEventHandler__ctor_m13 (DefaultSmartTerrainEventHandler_t14 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
 void DefaultSmartTerrainEventHandler_Start_m14 (DefaultSmartTerrainEventHandler_t14 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
 void DefaultSmartTerrainEventHandler_OnDestroy_m15 (DefaultSmartTerrainEventHandler_t14 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
 void DefaultSmartTerrainEventHandler_OnPropCreated_m16 (DefaultSmartTerrainEventHandler_t14 * __this, Object_t * ___prop, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
 void DefaultSmartTerrainEventHandler_OnSurfaceCreated_m17 (DefaultSmartTerrainEventHandler_t14 * __this, Object_t * ___surface, MethodInfo* method) IL2CPP_METHOD_ATTR;
