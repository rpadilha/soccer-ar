﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t742;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t34;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.WordAbstractBehaviour>
struct Enumerator_t870 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.WordAbstractBehaviour>::l
	List_1_t742 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.WordAbstractBehaviour>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.WordAbstractBehaviour>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.WordAbstractBehaviour>::current
	WordAbstractBehaviour_t34 * ___current_3;
};
