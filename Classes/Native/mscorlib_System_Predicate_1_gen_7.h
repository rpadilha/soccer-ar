﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t234;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.EventSystems.BaseInputModule>
struct Predicate_1_t3182  : public MulticastDelegate_t373
{
};
