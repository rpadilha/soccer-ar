﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// UnityEngine.Material
struct Material_t64;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Material>
struct UnityAction_1_t4767  : public MulticastDelegate_t373
{
};
