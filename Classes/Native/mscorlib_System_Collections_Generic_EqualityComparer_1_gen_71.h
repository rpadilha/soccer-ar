﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Events.PersistentCall>
struct EqualityComparer_1_t5006;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Events.PersistentCall>
struct EqualityComparer_1_t5006  : public Object_t
{
};
struct EqualityComparer_1_t5006_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Events.PersistentCall>::_default
	EqualityComparer_1_t5006 * ____default_0;
};
