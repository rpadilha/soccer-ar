﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ChannelInfo
struct ChannelInfo_t2044;
// System.Object[]
struct ObjectU5BU5D_t130;

// System.Void System.Runtime.Remoting.ChannelInfo::.ctor()
 void ChannelInfo__ctor_m11627 (ChannelInfo_t2044 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.ChannelInfo::get_ChannelData()
 ObjectU5BU5D_t130* ChannelInfo_get_ChannelData_m11628 (ChannelInfo_t2044 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
