﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t130;
// UnityEngine.Events.InvokableCall`1<UnityEngine.CapsuleCollider>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_168.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.CapsuleCollider>
struct CachedInvokableCall_1_t4819  : public InvokableCall_1_t4820
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.CapsuleCollider>::m_Arg1
	ObjectU5BU5D_t130* ___m_Arg1_1;
};
