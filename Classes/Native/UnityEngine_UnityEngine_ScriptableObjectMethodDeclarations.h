﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ScriptableObject
struct ScriptableObject_t962;
struct ScriptableObject_t962_marshaled;
// System.String
struct String_t;
// System.Type
struct Type_t;

// System.Void UnityEngine.ScriptableObject::.ctor()
 void ScriptableObject__ctor_m5588 (ScriptableObject_t962 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
 void ScriptableObject_Internal_CreateScriptableObject_m5589 (Object_t * __this/* static, unused */, ScriptableObject_t962 * ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
 ScriptableObject_t962 * ScriptableObject_CreateInstance_m5590 (Object_t * __this/* static, unused */, String_t* ___className, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
 ScriptableObject_t962 * ScriptableObject_CreateInstance_m5591 (Object_t * __this/* static, unused */, Type_t * ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
 ScriptableObject_t962 * ScriptableObject_CreateInstanceFromType_m5592 (Object_t * __this/* static, unused */, Type_t * ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
void ScriptableObject_t962_marshal(const ScriptableObject_t962& unmarshaled, ScriptableObject_t962_marshaled& marshaled);
void ScriptableObject_t962_marshal_back(const ScriptableObject_t962_marshaled& marshaled, ScriptableObject_t962& unmarshaled);
void ScriptableObject_t962_marshal_cleanup(ScriptableObject_t962_marshaled& marshaled);
