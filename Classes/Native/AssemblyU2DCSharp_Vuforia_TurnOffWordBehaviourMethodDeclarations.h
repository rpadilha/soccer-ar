﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TurnOffWordBehaviour
struct TurnOffWordBehaviour_t54;

// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
 void TurnOffWordBehaviour__ctor_m89 (TurnOffWordBehaviour_t54 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
 void TurnOffWordBehaviour_Awake_m90 (TurnOffWordBehaviour_t54 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
