﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.MeshFilter>
struct UnityAction_1_t4642;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.MeshFilter>
struct InvokableCall_1_t4641  : public BaseInvokableCall_t1127
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.MeshFilter>::Delegate
	UnityAction_1_t4642 * ___Delegate_0;
};
