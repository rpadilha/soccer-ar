﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t111;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t250;
// System.AsyncCallback
struct AsyncCallback_t251;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t4987  : public MulticastDelegate_t373
{
};
