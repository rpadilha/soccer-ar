﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUISettings
struct GUISettings_t1013;

// System.Void UnityEngine.GUISettings::.ctor()
 void GUISettings__ctor_m5807 (GUISettings_t1013 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
