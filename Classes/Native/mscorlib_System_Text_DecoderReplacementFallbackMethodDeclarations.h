﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.DecoderReplacementFallback
struct DecoderReplacementFallback_t2200;
// System.String
struct String_t;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2195;
// System.Object
struct Object_t;

// System.Void System.Text.DecoderReplacementFallback::.ctor()
 void DecoderReplacementFallback__ctor_m12422 (DecoderReplacementFallback_t2200 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.DecoderReplacementFallback::.ctor(System.String)
 void DecoderReplacementFallback__ctor_m12423 (DecoderReplacementFallback_t2200 * __this, String_t* ___replacement, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.DecoderReplacementFallback::get_DefaultString()
 String_t* DecoderReplacementFallback_get_DefaultString_m12424 (DecoderReplacementFallback_t2200 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.DecoderReplacementFallback::CreateFallbackBuffer()
 DecoderFallbackBuffer_t2195 * DecoderReplacementFallback_CreateFallbackBuffer_m12425 (DecoderReplacementFallback_t2200 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.DecoderReplacementFallback::Equals(System.Object)
 bool DecoderReplacementFallback_Equals_m12426 (DecoderReplacementFallback_t2200 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.DecoderReplacementFallback::GetHashCode()
 int32_t DecoderReplacementFallback_GetHashCode_m12427 (DecoderReplacementFallback_t2200 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
