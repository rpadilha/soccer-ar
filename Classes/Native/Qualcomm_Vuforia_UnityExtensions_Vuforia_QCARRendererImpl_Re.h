﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Vuforia.QCARRendererImpl/RenderEvent
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl_Re.h"
// Vuforia.QCARRendererImpl/RenderEvent
struct RenderEvent_t708 
{
	// System.Int32 Vuforia.QCARRendererImpl/RenderEvent::value__
	int32_t ___value___1;
};
