﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RijndaelTransform
struct RijndaelTransform_t2163;
// System.Security.Cryptography.Rijndael
struct Rijndael_t1732;
// System.Byte[]
struct ByteU5BU5D_t653;
// System.UInt32[]
struct UInt32U5BU5D_t1594;

// System.Void System.Security.Cryptography.RijndaelTransform::.ctor(System.Security.Cryptography.Rijndael,System.Boolean,System.Byte[],System.Byte[])
 void RijndaelTransform__ctor_m12213 (RijndaelTransform_t2163 * __this, Rijndael_t1732 * ___algo, bool ___encryption, ByteU5BU5D_t653* ___key, ByteU5BU5D_t653* ___iv, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::.cctor()
 void RijndaelTransform__cctor_m12214 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::Clear()
 void RijndaelTransform_Clear_m12215 (RijndaelTransform_t2163 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::ECB(System.Byte[],System.Byte[])
 void RijndaelTransform_ECB_m12216 (RijndaelTransform_t2163 * __this, ByteU5BU5D_t653* ___input, ByteU5BU5D_t653* ___output, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Security.Cryptography.RijndaelTransform::SubByte(System.UInt32)
 uint32_t RijndaelTransform_SubByte_m12217 (RijndaelTransform_t2163 * __this, uint32_t ___a, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
 void RijndaelTransform_Encrypt128_m12218 (RijndaelTransform_t2163 * __this, ByteU5BU5D_t653* ___indata, ByteU5BU5D_t653* ___outdata, UInt32U5BU5D_t1594* ___ekey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::Encrypt192(System.Byte[],System.Byte[],System.UInt32[])
 void RijndaelTransform_Encrypt192_m12219 (RijndaelTransform_t2163 * __this, ByteU5BU5D_t653* ___indata, ByteU5BU5D_t653* ___outdata, UInt32U5BU5D_t1594* ___ekey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::Encrypt256(System.Byte[],System.Byte[],System.UInt32[])
 void RijndaelTransform_Encrypt256_m12220 (RijndaelTransform_t2163 * __this, ByteU5BU5D_t653* ___indata, ByteU5BU5D_t653* ___outdata, UInt32U5BU5D_t1594* ___ekey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
 void RijndaelTransform_Decrypt128_m12221 (RijndaelTransform_t2163 * __this, ByteU5BU5D_t653* ___indata, ByteU5BU5D_t653* ___outdata, UInt32U5BU5D_t1594* ___ekey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::Decrypt192(System.Byte[],System.Byte[],System.UInt32[])
 void RijndaelTransform_Decrypt192_m12222 (RijndaelTransform_t2163 * __this, ByteU5BU5D_t653* ___indata, ByteU5BU5D_t653* ___outdata, UInt32U5BU5D_t1594* ___ekey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelTransform::Decrypt256(System.Byte[],System.Byte[],System.UInt32[])
 void RijndaelTransform_Decrypt256_m12223 (RijndaelTransform_t2163 * __this, ByteU5BU5D_t653* ___indata, ByteU5BU5D_t653* ___outdata, UInt32U5BU5D_t1594* ___ekey, MethodInfo* method) IL2CPP_METHOD_ATTR;
